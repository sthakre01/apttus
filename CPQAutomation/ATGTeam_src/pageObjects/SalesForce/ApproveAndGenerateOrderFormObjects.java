package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ApproveAndGenerateOrderFormObjects {
	
	public static WebElement webElement = null;
	
	// To identify the EnterSearchItem
	public Select fn_selectApproval(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//tr/td[contains(text(),'Approval Stage')]//following::td[1]/div/span/select"))));
		return webList;
	}

	// To Click on CreateAgreementGMSA button
	public WebElement fn_clickSendForReviewBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@alt=\"Send For Review\"]")));
		return webElement;
	}
	
	// To Click on Select Generated GMSA
	public WebElement fn_clickAttachGMSA(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//th/div[contains(text(),\"Last Modified Date\")]//following::td/input")));
		return webElement;
	}
	
	// To Click on the Next Button
	public WebElement fn_clickNextBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value=\"Next\"]")));
		return webElement;
	}
	
	// To Click on Select NewOrderFormBtn
	public WebElement fn_clickNewOrderFormBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),\"GMSA/NDA/New Order Form Send For Customer Review\")]//preceding::span/input[@type=\"radio\"]")));
		return webElement;
	}
}
