package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProposalPageObjects {
	
public static WebElement webElement = null;
	
	

	// To identify the ProposalName Field
	public WebElement fn_clickProposalButton(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Create Quote/Proposal']")));
		return webElement;
	}

    // To identify the ProposalName Field
	public WebElement fn_setProposalName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/label[text()='Proposal Name']/following::td[1]/div/input")));
		return webElement;
	}
	
	// To identify the PrimaryContact Field
	public WebElement fn_setPrimaryContact(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/label[text()='Primary Contact']/following::td[1]/span/input")));
		return webElement;
	}

	// To identify the PriceList Field
	public WebElement fn_setPriceList(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/label[text()='Price List']/following::td[1]/div/span/input")));
		return webElement;
	}
	
	// To identify the Save Button
	public WebElement fn_clickSaveButton(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.name("save")));
		return webElement;
	}
	
	
}
