package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pageObjects.BasePageObject;

/**
 * Created by joe.gannon on 3/16/2016.
 */
public class OpportunityPageObject extends BasePageObject
{
//    public static By createQuoteBtn = By.name("apttus_proposal__createquoteproposalfromopportunity");
//    public static By createQuoteBtn = By.name("apttus_proposal__newquoteproposalfromopportunity");
    public static By createQuoteBtn = By.name("apttus_proposal__createquoteproposalfromopportunity");
    public static ProposalPageObject clickCreatePropBtn(WebDriver driver) throws Exception
    {
        waitForElement(driver, createQuoteBtn);
        Thread.sleep(2000);
        System.out.println("Clicking Quote Button");
        scrollToElement(driver,driver.findElement(createQuoteBtn));
        driver.findElement(createQuoteBtn).click();
        return new ProposalPageObject();
    }
}
