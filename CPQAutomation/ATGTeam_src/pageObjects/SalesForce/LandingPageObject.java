package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObjects.Apttus.ProductPageObject;
import pageObjects.BasePageObject;

import java.util.List;

/**
 * Created by joe.gannon on 2/9/2016.
 */
public class LandingPageObject extends BasePageObject
{
    public static By homeTab = By.id("home_Tab");
    public static By productTab = By.id("Product2_Tab");
    public static By navDropDown = By.id("tsidButton");
    public static By aptPropManagement = By.partialLinkText("Apttus Proposal Management");
    public static By searchField = By.id("phSearchInput");
    public static By searchBtn = By.id("phSearchButton");
    public static boolean isLoaded(WebDriver driver)
    {
        WebElement ele = waitForElement(driver,homeTab);
        if(ele.isDisplayed())
        {
            return true;
        }
        return false;
    }
    public static ProductPageObject clickProductTab(WebDriver driver)
    {
        waitForElement(driver,productTab);
        driver.findElement(productTab).click();
        return new ProductPageObject();
    }
    public static void clickDropDownItem(WebDriver driver, String linkText) throws Exception
    {
        waitForElement(driver,navDropDown);
        driver.findElement(navDropDown).click();
        forcePageScroll(driver);
        String dDownText = driver.findElement(navDropDown).getText();
        if(!(linkText.equals(dDownText)))
        {
            driver.findElement(By.partialLinkText(linkText)).click();
        }
        forcePageScrollUp(driver);

    }
    public static AccountPageObject searchForAccount(WebDriver driver, String searchItem) throws Exception
    {
        By testResult = By.partialLinkText(searchItem);
        AccountPageObject ap = new AccountPageObject();
        waitForElement(driver, searchField);
        driver.findElement(searchField).sendKeys(searchItem+"\n");
//        driver.findElement(searchBtn).click();
        waitForElement(driver,testResult);
//        By searchResult = By.partialLinkText("Automation - Apttus");
//        System.out.println("Finding search result");
        String newUrl = driver.findElement(testResult).getAttribute("href");
        driver.navigate().to(newUrl);
        return ap;
    }
}
