package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import pageObjects.BasePageObject;
import pageObjects.SalesForce.OpportunitesFormPageObject;

/**
 * Created by joe.gannon on 2/10/2016.
 */
public class OpportunitesPageObject extends BasePageObject
{
    public static By opportunityTab = By.id("Opportunity_Tab");
    public static By newOpportunityBtn = By.name("new");
    public static By continueBtn = By.xpath("//*[@id=\"bottomButtonRow\"]/input[1]");
    public static void createNewOpportunity(WebDriver driver)
    {
        driver.findElement(opportunityTab).click();
        waitForElement(driver,newOpportunityBtn);
        driver.findElement(newOpportunityBtn).click();
        waitForElement(driver,continueBtn);
        driver.findElement(continueBtn);
        Assert.assertTrue(OpportunitesFormPageObject.isLoaded(driver));

    }
}
