package pageObjects.SalesForce;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OfferingDataPO {
	
			
	public static WebElement webElement = null;
		
	// To identify the ServiceName Field
	public WebElement fn_clickNewOfferData(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@title='New Offering Data']")));
		return webElement;
	}

	// To click on the Image ofLook uP window of customer Reference	
	public WebElement fn_clickLookUpWindow(WebDriver driver, WebDriverWait webWait,String WindowName) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='"+WindowName+"']//following::img[1]")));
		return webElement;
	}
	
	// To click on the Go Button
	public WebElement fn_clickGoBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@title=\"Go!\"]")));
		return webElement;
	}
	
	// To click on the Go Button
	public WebElement fn_clickSrchItm(WebDriver driver, WebDriverWait webWait,String srchItem) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.partialLinkText("")));
		return webElement;
	}
	
	// To Enter the Billing Date
	public WebElement fn_setBillingDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/th/label[contains(text(),'Billing Date')]/following::td[1]/span/span/a")));
		return webElement;
	}
	
	// To Enter the Term
	public WebElement fn_setTerm(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Term']//following::input[1]")));
		return webElement;	}
	
	// To Enter the StartDate
	public WebElement fn_setStartDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Start Date']//following::a[1]")));
		return webElement;
	}
	
	// To Enter the FutureDate
	public WebElement fn_setExpirationDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/th/label[contains(text(),'Expiration')]/following::td[1]/span/input")));
		return webElement;
	}

	// To Enter the AnnualPriceIncrease
	public WebElement fn_setAnnualPI(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/th/label[contains(text(),'Annual Price Increase')]//following::td[1]/input")));
		return webElement;
	}
	
	// To Select the Billing Contact
	public Select fn_selBillingContact(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath("//tr/td[contains(text(),'Bill To Contact')]//following::td[1]/select")));
		return webList;
	}
	
	// To Select the Notification Contact
	public Select fn_selNotificationContact(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList =  new Select(driver.findElement(By.xpath("//tr/td[contains(text(),'Notification Contact')]//following::td[1]/select")));
		return webList;
	}
	
	// To Select the Covered Location
	public WebElement fn_selCoveredLocation(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/th/div[contains(text(),'Select')]//following::td[1]/input")));
		return webElement;
	}
	
	// To Click on Save Button
	public WebElement fn_clickSaveBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td/input[@title='Save']")));
		return webElement;
	}
	
	// To Click on Search Button on Popup window
	public WebElement fn_clickWindowSearchBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"lksrch\"]")));
		return webElement;
	}
	
	// To Click on Go Button on Popup window
	public WebElement fn_clickWindowGoBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/input[@title=\"Go!\"]")));
		return webElement;
	}
	
	// To get the link text on popup window
	public WebElement fn_clickWindowLinkText(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@class='list']/tbody/tr[2]/th/a")));
		return webElement;
	}
}
