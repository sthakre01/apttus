package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPageObjects {
	
public static WebElement webElement = null;
	
	// To identify the UserName Field
	public WebElement fn_EnterUserName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.id("username")));
		return webElement;
	}
	
	// To identify the Password Field
	public WebElement fn_EnterPassword(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.id("password")));
		return webElement;
	}
	
	// To identify the Login Button
	public WebElement fn_ClickLoginButton(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.id("Login")));
		return webElement;
	}
	

}
