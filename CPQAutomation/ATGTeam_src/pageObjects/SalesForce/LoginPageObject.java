package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pageObjects.BasePageObject;
import pageObjects.SalesForce.LandingPageObject;

/**
 * Created by joe.gannon on 2/9/2016.
 */
public class LoginPageObject extends BasePageObject
{
    public static By usernameField = By.id("username");
    public static By passwordField = By.id("password");
    public static By loginBtn = By.id("Login");
//    public static String username = "qa.ire_salesuser1@sungardas.com.apttusdev";
//    public static String password = "Welcome$10";
    public static String username = "joe.gannon@sungardas.com.apttus";
    public static String password = "ATGmsc12345";
    //"joe.gannon@sungardas.com.apttus"
    //ATGmsc12345
    public static LandingPageObject login(WebDriver driver)
    {
        LandingPageObject lp = new LandingPageObject();
        waitForElement(driver,usernameField);
//        driver.findElement(usernameField).sendKeys(username);
//        driver.findElement(passwordField).sendKeys(password);
        driver.findElement(usernameField).clear();
        driver.findElement(usernameField).sendKeys(username);
        driver.findElement(passwordField).clear();
        driver.findElement(passwordField).sendKeys(password);
        driver.findElement(loginBtn).click();
        return lp;
    }
}
