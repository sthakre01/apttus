package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pageObjects.BasePageObject;

/**
 * Created by joe.gannon on 3/16/2016.
 */
public class AccountPageObject extends BasePageObject
{
    public static By oppLink = By.xpath("//*[@id=\"001P000000eXCfz_RelatedOpportunityList_link\"]/span/text()");
    public static OpportunityPageObject clickOppLink(WebDriver driver, String oppName)
    {
        OpportunityPageObject op = new OpportunityPageObject();
//        waitForElement(driver, oppLink);
//        driver.findElement(oppLink).click();
        waitForElement(driver,By.partialLinkText(oppName));
        String linkText = driver.findElement(By.partialLinkText(oppName)).getAttribute("href");
        driver.navigate().to(linkText);

//        driver.findElement(By.partialLinkText(oppName)).click();
        return op;
    }
}
