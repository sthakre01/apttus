package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LeadCreationObjects {
	
	public static WebElement webElement = null;
		
	// To identify the EnterSearchItem
	public WebElement fn_SelectLeadsTab(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(("//ul/li/a[@title=\"Leads Tab\"]"))));
		return webElement;
	}
   
	//To click on New Button
	public WebElement fn_ClickNew(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td/input[@title=\"New\"]")));
		return webElement;
	}
	
	// To Select the Record type of new Record
	public Select fn_SelectRecord(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//td/label[contains(text(),\"Record Type of new record\")]//following::select[1]"))));
		return webList;
	}
   
	//To click on Continue Button
	public WebElement fn_ClickContinuebtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td/input[@value=\"Continue\"]")));
		return webElement;
	}
	
	//To enter input for lastname
	public WebElement fn_EnterLastName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td/label[contains(text(),'Last Name')]//following::td[1]/div/input")));
		return webElement;
	}
	//To enter input for Company
	public WebElement fn_EnterCompany(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td/label[contains(text(),'Company')]//following::td[1]/div/input")));
		return webElement;
	}
	//To enter input for Phone
	public WebElement fn_EnterPhone(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td/label[contains(text(),'Phone')]//following::td[1]/input")));
		return webElement;
	}
	
	// To Select the LeadCurrency
	public Select fn_SelectLeadCurrency(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//tr/td/label[contains(text(),'Lead Currency')]//following::td[1]/div/select"))));
		return webList;
	}
	
	// To Select the Lead Status
	public Select fn_LeadStatus(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//tr/td/label[contains(text(),'Lead Status')]//following::td[1]/div/span/select"))));
		return webList;
	}
	
	// To Select the Lead Stage
	public Select fn_LeadStage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//tr/td/label[contains(text(),'Lead Stage')]//following::td[1]/span/span/select"))));
		return webList;
	}
	
	//To click on Save Button
	public WebElement fn_ClickSaveBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@name='save']")));
		return webElement;
	}
	
	//To click on Convert Button
	public WebElement fn_ClickLdConvertBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td/input[@name=\"convert\"]")));
		return webElement;
	}
	
	
	//To click on Lookup window
	public WebElement fn_ClickAcctLookUpwndw(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/div/span/a/img[@alt=\" Lookup (New Window)\"]")));
		return webElement;
	}
	
	//Enter Acct Name
	public WebElement fn_EnterAcctName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td[contains(text(),\"Account Name\")]//following::td[1]/div/select")));
		return webElement;
	}
	
	//Click on Enter opptyName
	public WebElement fn_EnterOpptyName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td[contains(text(),\"Opportunity Name\")]//following::td[1]/div/input")));
		return webElement;
	}
	
	//Enter Value in subject Field
	public WebElement fn_EnterSubject(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/label[text()='Subject']//following::td[1]/div/input")));
		return webElement;
	}
	
	
	// To Select the LMRtasktype
	public Select fn_LMRtasktype(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//td/label[text()='LMR Task Type']//following::td[1]/div/span/select"))));
		return webList;
	}
	
	
	//Enter value for Duedate
	public WebElement fn_Enterduedate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/label[text()='Due Date']//following::td[1]/div/span/span/a")));
		return webElement;
	}
	
	//Select value for active currency
	public Select fn_ActiveCurrency(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//td/label[text()='Activity Currency']//following::td[1]/div/select"))));
		return webList;
	}
	
	//Click on Convert Button
	public WebElement fn_ClickConvertBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td/input[@title=\"Convert\"]")));
		return webElement;
	}
	
	//Select the competetors
	public Select fn_LeadContact(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//tr/td/label[contains(text(),'Contact Name')]//following::td[1]/div/select"))));
		return webList;
	}
	
	
	//Click on New Oppty Button
	public WebElement fn_ClickNewOpptyBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td/input[@name=\"newOpp\"]")));
		return webElement;
	}
	
	//Click on Continue button
	public WebElement fn_ClickOpptyContBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@title=\"Continue\"]")));
		return webElement;
	}
	
	//Enter the Oppty Name field
	public WebElement fn_EnterOpptunityyName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td/label[contains(text(),'Opportunity Name')]//following::td[1]/div/input")));
		return webElement;
	}
	
	//Enter the OpptyType
	public Select fn_SelectOpptyType(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//tr/td/span/label[contains(text(),'Opportunity Type')]//following::td[1]/div/span/select"))));
		return webList;
	}
	
	//select the Oppty Currency
	public Select fn_SelectOpptyCurrency(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//tr/td/label[contains(text(),'Opportunity Currency')]//following::td[1]/div/select"))));
		return webList;
	}
	//Select the competetors
	public Select fn_SelectOpptyComptrs(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(webWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(("//tr/td/label[contains(text(),'Competitors')]//following::td[1]/div/span/select")))));
		return webList;
	}
	
	//enter the close date
	public WebElement fn_EnterCloseDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td/label[contains(text(),'Close Date')]//following::td[1]/div/span/input")));
		return webElement;
	}
	
	//select the stage
	public Select fn_SelectStage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//tr/td/label[contains(text(),'Stage')]//following::td[1]/div/span/select"))));
		return webList;
	}
	
	//Enter the Billing Account
	public WebElement fn_EnterBillingAc(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td/label[contains(text(),'Billing Account')]//following::td[1]/span/input")));
		return webElement;
	}
	
	//Select the PrimaryProductInterest
	public Select fn_SelectPPI(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(webWait.until(ExpectedConditions.elementToBeClickable((By.xpath(("//tr/td/label[contains(text(),'Primary Product Interest')]//following::td[1]/div/span/select"))))));
		return webList;
	}
	
	//Click on Save Button
	public WebElement fn_ClickOpptySaveBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@title=\"Save\"]]")));
		return webElement;
	}
	
	
}
