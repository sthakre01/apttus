package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GenerateProposalDocPageObjects {
	
	public static WebElement webElement = null;
	
	// To Click on Generate Button on Proposal Page
	public WebElement fn_clickGenerate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[contains(@alt, 'Generate')]")));
		return webElement;
	}

	//To Click on the webElemention of Create Budgetary Proposal - To Present Doc
	public WebElement fn_selProposalDocument(WebDriver driver, WebDriverWait webWait,String strDocName) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/span[text()='"+strDocName+"']/preceding::td[1]/span/input")));
		return webElement;
	}
	
	//To Click on Generate Button
	public WebElement fn_clickGenerateBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[@class='pbButton']/span/input[@value='Generate']")));
		return webElement;
	}
	
	//To Click on LInk Click to View the file
	public WebElement fn_clickLinkViewFile(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText("Click here to view this file")));
		return webElement;
	}
	
	//To Click on Return Button
	public WebElement fn_ClickReturnBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@value='Return']")));
		return webElement;
	}
	
	//To Click on Return Button
	public WebElement fn_selDocumentType(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@value=\"DOC\"]")));
		return webElement;
	}
	
}
