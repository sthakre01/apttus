package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ConfigurationPageObjects {	

		
	public static WebElement webElement = null;
	
	
		//To identify the configure Button
		public WebElement fn_clickConfigureBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			//webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[contains(text(),'Configure Products')]/following::td[1]/div/a/img")));
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[contains(@alt, 'Configure Products')]")));
			return webElement;
		}

		// To identify the ServiceName Field
		public WebElement fn_clickBaseServiceName(WebDriver driver, WebDriverWait webWait,String strServicename) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.partialLinkText(strServicename)));
			return webElement;
		}
		
		// To identify the ServiceName Field
		public WebElement fn_clickServiceName(WebDriver driver, WebDriverWait webWait,String strComponentName) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(("//div//span[contains(@class,'aptSearchProductName') and text() ='"+strComponentName+"']/following::div[*]/span/a"))));
			return webElement;
		}
		
		//To identify the Offering Data Window
		public WebElement fn_getNDAAddressWindow(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@title='Address Lookup (New Window)']")));
			return webElement;
		}
		
		//To identify the NDA_Address  Data Window
		public WebElement fn_getOfferWindow(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/span/*/a/img[@alt=\"Offering Data Lookup (New Window)\"]")));
			return webElement;
		}
		
		// To identify the Next Field
		public WebElement fn_clickNext(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(("//*[@id=\"Next\"]/a"))));
			return webElement;
		}
		
		// To identify the Pricing Button
		public WebElement fn_clickPricingBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(("//div/ul/li[5]/a[contains(text(),'Go to Pricing')]"))));
			return webElement;
		}
		
		// To identify the Toggle button of show pricing
		public WebElement fn_clickShowPricing(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.className("showOptionButton")));
			return webElement;
		}		
		
		// To identify the Save button on pricing page
		public WebElement fn_clickSave(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//ul[2]/li[6]/a[contains(text(),'Update Line Items & Exit')]")));
			return webElement;
		}	
		
		
		
	}


