package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import pageObjects.Apttus.OptionConfigPageObject;
import pageObjects.BasePageObject;

import java.util.ArrayList;
import java.util.List;


public class ProposalPageObject extends BasePageObject
{
    public static By priceListField = By.id("CF00NP0000000h6CT");
    public static By propNameField = By.id("00NP0000000h5bA");
    public static By contactNameField = By.id("CF00NP0000000xJjh");
    public static By propSaveBtn = By.name("save");
//    public static By configProductBtn = By.xpath("//*[@id=\"00NP0000000h6CO_ileinner\"]/a");
    public static By configProductBtn = By.xpath("//*[@id=\"00NP0000000w2CF_ileinner\"]/a/img");
    public static By priceListDropDown = By.className("lookupInput");
    public static void setPriceList(WebDriver driver, String priceListName)
    {
        //waitForElement(driver,priceListDropDown).clear();
        //driver.findElement(priceListDropDown).click();

        //driver.findElement(priceListField).sendKeys(priceListName);

    }
    public static void setPropName(WebDriver driver, String propName)
    {
        waitForElement(driver, propNameField).clear();
        driver.findElement(propNameField).sendKeys(propName);
    }
    public static void setPrimaryContact(WebDriver driver, String contactName)
    {
        waitForElement(driver,contactNameField).clear();
        waitForElement(driver,contactNameField).sendKeys(contactName);
        waitForElement(driver,By.className("autocompleteMatch")).click();
//        driver.findElement(By.className("autocompleteMatch")).click();
    }
    public static void clickSaveBtn(WebDriver driver)
    {
        By saveBtn = By.xpath("/html/body/div[1]/div[2]/table/tbody/tr/td[2]/form/div/div[3]/table/tbody/tr/td[2]/input[1]");
        //html.ext-strict body.hasMotif.Custom29Tab.editPage.ext-gecko.ext-gecko3.sfdcBody.brandQuaternaryBgr div#contentWrapper div.bodyDiv.brdPalette.brandPrimaryBrd table#bodyTable.outer tbody tr td#bodyCell.oRight form#editPage div#ep.bPageBlock.brandSecondaryBrd.bEditBlock.secondaryPalette div.pbBottomButtons table tbody tr td#bottomButtonRow.pbButtonb input.btn
        waitForElement(driver,saveBtn);
//        simpleSleep(3000);
//        System.out.println("Clicking saveBtn");
//        WebElement mySaveBtn = driver.findElement(saveBtn);
//        System.out.println("SaveBtn className: "+mySaveBtn.getAttribute("name"));
//        WebElement testElement = driver.findElement(By.xpath("//*[@id=\"ep\"]/div[2]/div[5]/table/tbody/tr/td[2]/span/div/img"));
//        mySaveBtn.click();
//        mySaveBtn.click();
//        Actions action = new Actions(driver);
//        action.moveToElement(testElement).build().perform();
//        ((JavascriptExecutor) driver).executeScript("document.getElementsByName('save')[0].focus();");
//        System.out.println("Active Element name: "+driver.switchTo().activeElement().getAttribute("name"));
//        simpleSleep(10000);
//        driver.switchTo().activeElement().click();
        driver.findElement(By.name("save")).click();
//        simpleSleep(30000);
        //action.click().build().perform();

//        waitForElement(driver,saveBtn).click();
//        WebElement container = driver.findElement(By.className("pbButtonb"));
////        driver.findElement()
//        List<WebElement> btns = container.findElements(By.tagName("input"));
//        for(WebElement btn: btns)
//        {
//            if(btn.getAttribute("name").equals("save"))
//            {
//                System.out.println("Save Button Found!");
//                String blerg = driver.switchTo().activeElement().getAttribute("name");
//                System.out.println("Active Element: "+blerg);
//                btn.click();
//                String blergafter = driver.switchTo().activeElement().getAttribute("name");
//                System.out.println("Active Element: "+blergafter);
//                break;
//            }
//        }
//        for(WebElement btn : buttons)
//        {
//            if (btn.getAttribute("name").equals("save"))
//            {
//                System.out.println("Save Btn Found!");
//                try
//                {
//                    Thread.sleep(2000);
//                    scrollToElement(driver,btn);
//                    Thread.sleep(10000);
//                    btn.click();
//                    break;
//                }catch (Exception e)
//                {
//                    e.printStackTrace();
//                }
//
//            }
//        }
    }
    public static OptionConfigPageObject clickConfigProductBtn(WebDriver driver) throws Exception
    {
        System.out.println("Clicking configure products btn");
        forcePageScroll(driver);
//        Thread.sleep(20000);
        scrollToElementByLocator(driver,configProductBtn);
        waitForElement(driver, configProductBtn).click();
        return new OptionConfigPageObject();
    }

}
