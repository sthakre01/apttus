package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pageObjects.BasePageObject;

/**
 * Created by joe.gannon on 2/10/2016.
 */
public class OpportunitesFormPageObject extends BasePageObject
{
    private static By oppNameField = By.id("opp3");
    private static By oppAcctName = By.id("opp4");
    private static By oppType = By.id("opp5");
    private static By oppExistingBusiness = By.xpath("//*[@id=\"opp5\"]/option[2]");
    public static void fillOpportunityForm(WebDriver driver)
    {
        driver.findElement(oppNameField).sendKeys("Joe");
        driver.findElement(oppAcctName).sendKeys("Joe's Acct");
        driver.findElement(oppType).click();
        driver.findElement(oppExistingBusiness).click();

    }
    public static boolean isLoaded(WebDriver driver)
    {
        WebElement ele = waitForElement(driver,oppNameField);
        if(ele.isDisplayed())
        {
            return true;
        }
        return false;
    }
}
