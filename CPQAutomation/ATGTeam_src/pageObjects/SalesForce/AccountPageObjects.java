package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AccountPageObjects {
	
	
public static WebElement webElement = null;
	
	// To identify the EnterSearchItem
	public WebElement fn_setSearchItem(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.partialLinkText("searchItem")));
		return webElement;
	}

	// To identify the Search Field
	public WebElement fn_setSearchField(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.id("phSearchInput")));
		return webElement;
	}

	// To Click on SearchFeild Button
	public WebElement fn_clickSearchBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.id("phSearchButton")));
		return webElement;
	}

	//To click on CustomerReferences Tab
	public WebElement fn_clickCustomerReferenceTab(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//ul/li/a[@title='Customer References Tab']")));
		return webElement;
	}
	
	//To click on New Button on Customer References tab
	public WebElement fn_clickNewCustRefOnTab(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@title='New']")));
		return webElement;
	}
	
	//To click on NewCustomereReference Button
	public WebElement fn_clickNewCustReferenceBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@value='New Customer Reference']")));
		return webElement;
	}
	
	
	//To Enter Tier Name on Customer Reference page; 0 - Dev Instance, 1 - SandBox Instance.
	public WebElement fn_enterCsutomerReferenceName(WebDriver driver, WebDriverWait webWait, Integer intCustRefName) throws InterruptedException
	{
		//based on the environment, the XPath will be generated
		String strLabel = "";
		if(intCustRefName == 0)
			strLabel = "Customer Reference Name";
		else
			strLabel = "Tier Name";
		
		String strXPath = "//td/label[text()='"+strLabel+"']//following::td[1]/div/input";
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(strXPath)));
		return webElement;
	}
	
	//To Enter Tier Name on Customer Reference page
	public WebElement fn_clickSaveCustRefPage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@name='save' and @type='submit']")));
		return webElement;
	}
	
	//To Select the Currency Code on Customer Reference page
	public Select fn_selCurrency(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath("//td/label[text()='Currency']//following::td[1]/div/select")));
		return webList;
	}
	
	//To find the Customer Reference Object
	public WebElement fn_getCustomerReferenceTable(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='New Customer Reference']//following::table[1]/tbody/tr/th")));
		return webElement;
	}
	
}
