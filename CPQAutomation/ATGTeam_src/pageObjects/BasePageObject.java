package pageObjects;

import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Reporter;
import utility.Log;

/**
 * Created by joe.gannon on 2/9/2016.
 */
public class BasePageObject
{
    public static WebElement waitForElement(WebDriver driver, By element)
    {
        WebDriverWait wait = new WebDriverWait(driver, 40);

        try{
            WebElement ele = wait.until(ExpectedConditions.elementToBeClickable(element));
            return ele;
        }
        catch(Exception e)
        {
            Log.error("Element " + element.toString() + " did not load!");
            return null;
        }

    }
    public static void simpleSleep(int mili)
    {
        try
        {
            Thread.sleep(mili);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

    }
    public static void forcePageScroll(WebDriver driver)
    {
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollBy(0,250)", "");
    }
    public static void forcePageScrollUp(WebDriver driver)
    {
        JavascriptExecutor jse = (JavascriptExecutor)driver;
        jse.executeScript("window.scrollBy(0,-100)", "");
    }
    public static void scrollToElementByLocator(WebDriver driver, By locator)
    {
        WebElement element = waitForElement(driver, locator);
        Actions actions = new Actions(driver);
        actions.moveToElement(element);
// actions.click();
        actions.perform();

    }
    public static void scrollToElement(WebDriver driver, WebElement element) throws Exception
    {
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        Thread.sleep(500);
        forcePageScrollUp(driver);
//        Actions actions = new Actions(driver);
//        actions.moveToElement(element);
//        Thread.sleep(500);
//// actions.click();
//        actions.perform();
    }
    public static void waitForSpinner(WebDriver driver)
    {
        By spinnerLocator = By.className("spinnerImg-tree");
        WebDriverWait wait = new WebDriverWait(driver, 120);
        try{
            System.out.println("Waiting for spinner to disappear..");
            wait.until(ExpectedConditions.invisibilityOfElementLocated(spinnerLocator));
            System.out.println("Finished Waiting..");
        }
        catch(TimeoutException e)
        {
            System.out.println("Timed out waiting for spinner to disappear...");
        }
    }
    public static void waitForElementEditable(WebDriver driver, final WebElement element)
    {
        WebDriverWait wait = new WebDriverWait(driver, 40);

        try{
            wait.until(new ExpectedCondition<Boolean>() {
                public Boolean apply(WebDriver driver) {
                    return element.isEnabled();
                }
            });
        }
        catch(Exception e)
        {
            Log.error("Element " + element.toString() + " did not load!");
        }

    }

}
