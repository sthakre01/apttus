package pageObjects.Apttus.ConstraintValidation;

import appModules.Constraint;
import appModules.Product;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pageObjects.Apttus.ProductConfig.ProductConfigPageObject;

import java.util.ArrayList;
import java.util.List;


public class TwoFactorConstraintPageObject extends ProductConfigPageObject
{

    public static By warningMessageLocator = By.id("j_id0:idForm:idErrorMsg");
    public static By authenticationNodeQtyLocator = By.id("j_id0:idForm:j_id164:0:j_id173:0:j_id193:1:j_id226");
    public static By errorMessageLocator = By.id("j_id0:idForm:j_id164:0:j_id173:0:idOptionGroupErrorMessage");
    public static void verifyConstraints(WebDriver driver, Product prod) throws Exception
    {
        //TODO: verify initial warning message
        Constraint authNodeCon = prod.getConstraintRules().get(3);
        waitForElement(driver,warningMessageLocator);
        boolean authMsgAppears = verifyWarningMessage(driver,authNodeCon);
        System.out.println(authNodeCon.getMessage()+" : "+authMsgAppears);

        //TODO: verify error message for option selection
        //uncheck first two nodes
        clickCheckBoxItem(driver, 0);
        clickCheckBoxItem(driver,1);

        //after unchecking, click gotopricing button
        Thread.sleep(5000);
        clickGoToPricing(driver);
        driver.findElement(goToPricingBtn).click();
        waitForElement(driver,errorMessageLocator);

        System.out.println("ERROR MESSAGE: "+driver.findElement(errorMessageLocator).getText().trim());

        //TODO: Click Packaging and Delivery without selecting Hardware token
        Constraint hardwareConstraint = prod.getConstraintRules().get(1);
        clickCheckBoxItem(driver,9);
        System.out.println(hardwareConstraint.getConName()+" Validated: "+verifyWarningMessage(driver,hardwareConstraint));
        clickCheckBoxItem(driver,5);

        //TODO: click Passcode Deliveries without selecting software token
        Constraint softwareConstraint = prod.getConstraintRules().get(2);
        clickCheckBoxItem(driver,7);
        System.out.println(softwareConstraint.getConName()+" Validated: "+verifyWarningMessage(driver,softwareConstraint));
        clickCheckBoxItem(driver,4);

        //TODO: Make sure everything is clicked and proceed:
        new_clickAllCbs(driver,prod);


//        Thread.currentThread().interrupt();
        Thread.sleep(30000);

    }

    public static boolean verifyWarningMessage(WebDriver driver, Constraint con)
    {
        try {
            WebElement actualMsg = driver.findElement(warningMessageLocator).findElement(By.tagName("li"));
            System.out.println("WARNING MESSAGE FOUND: " + actualMsg.getText());
            return con.getMessage().equals(actualMsg.getText().trim());
        }
        catch(Exception e)
        {
            System.out.println(con.getConName()+" Message not found!");
            return false;
        }

    }

    public static void clickCheckBoxItem(WebDriver driver, int index) throws Exception
    {
        Thread.sleep(500);
        List<WebElement> allInputs = driver.findElements(By.tagName("input"));
        ArrayList<WebElement> allCbs = new ArrayList<>();
        try{
            for(WebElement input: allInputs) {
                if (input.getAttribute("type").equals("checkbox"))
                {
                    allCbs.add(input);
                }
            }


        scrollToElement(driver,allCbs.get(index));
        allCbs.get(index).click();

        }
        catch(StaleElementReferenceException e)
        {
            clickCheckBoxItem(driver,index);
        }
    }
}
