package pageObjects.Apttus.ConstraintValidation;

import appModules.Constraint;
import appModules.Product;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pageObjects.Apttus.ProductConfig.ProductConfigPageObject;


public class ContentDeliveryConstraintPageObject extends ProductConfigPageObject
{
    public static By warningMessageLocator = By.id("j_id0:idForm:idErrorMsg");
    public static By authenticationNodeQtyLocator = By.id("j_id0:idForm:j_id164:0:j_id173:0:j_id193:1:j_id226");
    public static By errorMessageLocator = By.id("j_id0:idForm:j_id164:0:j_id173:0:idOptionGroupErrorMessage");
    public static void verifyConstraints(WebDriver driver, Product prod) throws Exception {
        //TODO: verify initial warning message
        Constraint authNodeCon = prod.getConstraintRules().get(0);
        waitForElement(driver,warningMessageLocator);
        boolean authMsgAppears = verifyWarningMessage(driver,authNodeCon);
        System.out.println("Message: "+authNodeCon.getMessage()+" : "+authMsgAppears);
    }
    public static boolean verifyWarningMessage(WebDriver driver, Constraint con)
    {
        try {
            WebElement actualMsg = driver.findElement(warningMessageLocator).findElement(By.tagName("li"));
            System.out.println("WARNING MESSAGE FOUND: " + actualMsg.getText());
            return con.getMessage().equals(actualMsg.getText().trim());
        }
        catch(Exception e)
        {
            System.out.println(con.getConName()+" Message not found!");
            return false;
        }

    }

}
