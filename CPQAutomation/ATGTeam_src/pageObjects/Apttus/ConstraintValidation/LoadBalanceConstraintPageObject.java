package pageObjects.Apttus.ConstraintValidation;

import appModules.Product;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pageObjects.Apttus.ProductConfig.ProductConfigPageObject;

/**
 * Created by joe.gannon on 5/15/2016.
 */
public class LoadBalanceConstraintPageObject extends ProductConfigPageObject
{
    public boolean verifyingConstraints(WebDriver driver, Product prod) throws Exception
    {

        By firstErrorMsg = By.id("j_id0:idForm:j_id164:1:j_id173:0:j_id189");
        //*[@id="j_id0:idForm:j_id164:1:j_id173:0:j_id189"]/ul/li/text()
        By secondErrorMsg = By.xpath("//*[@id=\"j_id0:idForm:j_id164:3:j_id173:0:j_id189\"]/ul/li");
        //Verify Error Messages
        ProductConfigPageObject phyServ = new ProductConfigPageObject();
        phyServ.new_clickAllCbs(driver, prod);
        waitForSpinner(driver);
        clickGoToPricing(driver);

        //Verify error messages appear
        waitForElement(driver,firstErrorMsg);
        if(driver.findElement(firstErrorMsg).isDisplayed())
        {
            System.out.println("[PASS] 1st Validation Message is displayed");

        }else
        {
            System.out.println("[FAIL] 1st Validation Message is NOT displayed");
        }

        if(driver.findElement(secondErrorMsg).isDisplayed())
        {
            System.out.println("[PASS] 2nd Validation Message is displayed");

        }else
        {
            System.out.println("[FAIL] 2nd Validation Message is NOT displayed");
        }

        ProductConfigPageObject.clickSpecificCheckBox(driver,"Managed Load Balancer Add Ons Not Available in this market");
        waitForSpinner(driver);
        ProductConfigPageObject.clickSpecificCheckBox(driver,"Managed High Availability Load Balancer Not Available in this market");
        return false;
    }
}
