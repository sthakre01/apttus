package pageObjects.Apttus.ConstraintValidation;

import appModules.Option;
import appModules.Product;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pageObjects.Apttus.ProductConfig.PricingPageObject;
import pageObjects.Apttus.ProductConfig.ProductConfigPageObject;

import java.util.List;

public class WafConstraintPageObject extends ProductConfigPageObject
{
        public boolean verifyingConstraints(WebDriver driver, Product prod) throws Exception
        {
            System.out.println("Checking WAF Constraints");
            int numTables = driver.findElements(By.className("aptLeafProductsTableContainer")).size();
            System.out.println("Number of Tables: "+numTables);
            for(int i=0;i<numTables;i++)
            {
                this.verifyConstraintsTable(driver,prod,i);

            }
            return false;

        }

    public void verifyConstraintsTable(WebDriver driver, Product prod, int tableIndex) throws Exception
    {
        Thread.sleep(10000);
        System.out.println("NumTablesSize: "+driver.findElements(By.className("aptLeafProductsTableContainer")).size());
        WebElement container = driver.findElements(By.className("aptLeafProductsTableContainer")).get(tableIndex);
        WebElement tableBody = container.findElement(By.tagName("tbody"));
        List<WebElement> rows = tableBody.findElements(By.className("lineItem-"));
        System.out.println("NumberOfOptionRows: "+rows.size());
        if(rows.size()<=1)
        {
            System.out.println("Waiting for second table to populate");
            Thread.sleep(10000);
            container = driver.findElements(By.className("aptLeafProductsTableContainer")).get(tableIndex);
            tableBody = container.findElement(By.tagName("tbody"));
            rows = tableBody.findElements(By.className("lineItem-"));

        }
        //For each row, click radio and then click gotopricing and verify price
        int numRows =  tableBody.findElements(By.className("lineItem-")).size();
        for(int i=0;i<numRows;i++)
        {
            waitForElement(driver,By.className("aptLeafProductsTableContainer"));
            container = driver.findElements(By.className("aptLeafProductsTableContainer")).get(tableIndex);
            tableBody = container.findElement(By.tagName("tbody"));
            WebElement row = tableBody.findElements(By.className("lineItem-")).get(i);
            List<WebElement> inputs = row.findElements(By.tagName("input"));
            WebElement inp = inputs.get(0);
            List<WebElement> cells = row.findElements(By.tagName("td"));
            if (inp.getAttribute("type").equals("radio"))
            {

                String optionText = cells.get(2).getText();
                if(optionText.trim().equals("None"))
                {
                    System.out.println("Radio Button found for : " + optionText);
                    scrollToElement(driver,inp);
                    inp.click();
                    //Waiting for the next table to populate
                    Thread.sleep(10000);
                }
                else
                {
                    System.out.println("Radio Button found for : " + optionText);
                    inp.click();
                    Option currentOpt = prod.getProductPriceMap().get(optionText.trim());
                    if (currentOpt == null) {
                        System.out.println("CURRENT OPTION IS NULL");
                    } else {
                        System.out.println("OPTION FOUND!: " + currentOpt.getOptionName());
                        //Click Go To Pricing
                        clickGoToPricing(driver);

                        //Verify Option pricing
                        PricingPageObject.clickToggleBtn(driver);
                        PricingPageObject.getPricingTable(driver, prod);

                        //Check the product & family codes
                        System.out.println("008_Product name appears on shopping cart page: " + PricingPageObject.validateProductNameOnPricing(driver, prod.getProductName()));
                        System.out.println("009_Product Code matches current Product: " + PricingPageObject.validateProductCode(driver, prod));
                        PricingPageObject.validateBundleCodes(driver, prod);

                        driver.navigate().back();

//                        break; //REMOVE THIS
                    }
                }


            }else
            {
                System.out.println("RADIO BUTTON NOT FOUND!");
            }
        }
    }
}
