package pageObjects.Apttus;


import appModules.Product;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageObjects.Apttus.ProductConfig.ProductConfigPageObject;
import pageObjects.BasePageObject;

import java.util.HashMap;
import java.util.List;

public class OptionConfigPageObject extends BasePageObject
{
    public static By physServerConfigBtn = By.id("j_id0:idForm:j_id178:2:j_id221");
    public static By breadCrumbContainer = By.id("breadCrumbContainer");
    public static By offeringNextBtn = By.id("j_id0:idLineItemSO:j_id322:4:j_id325");
    public static void selectProduct(WebDriver driver, String productName) throws Exception
    {
        Thread.sleep(5000);
        driver.findElement(By.partialLinkText("Expand All")).click();
        waitForElement(driver, By.partialLinkText(productName));
        scrollToElementByLocator(driver,By.partialLinkText(productName));
        System.out.println("Trying to click link: "+productName);
        boolean isDisplayed = driver.findElement(By.partialLinkText(productName)).isDisplayed();
        scrollToElementByLocator(driver,By.partialLinkText(productName));
        forcePageScroll(driver);
        driver.findElement(By.partialLinkText(productName)).click();
        //product.click();


        System.out.println("001_Hierarchy levels are displayed on Configuration Window : "+isDisplayed);
        Assert.assertTrue(isDisplayed);

    }
    public static ProductConfigPageObject clickPhysServerConfigBtn(WebDriver driver) throws Exception
    {
        Thread.sleep(2000);
        forcePageScroll(driver);
        waitForElement(driver, physServerConfigBtn).click();
        return new ProductConfigPageObject();
    }

    public static boolean getProductBtn(WebDriver driver, Product prod) throws Exception
    {
        String productName = prod.getProductName();
        waitForElement(driver, breadCrumbContainer);
        Thread.sleep(5000);
        List<WebElement> products = driver.findElements(By.className("aptProductCatalogLine"));
        System.out.println("Number of Products found:"+products.size());
        boolean prodFound = false;
        for(WebElement product : products)
        {
            //aptProductInfoContainer
            String prodName = product.findElement(By.className("aptProductInfoFields")).getText().split("-")[0].trim();

            if(productName.contains(prodName) || prodName.contains(productName))
            {
                System.out.println("Product found: "+prodName);
                List<WebElement> btns = product.findElements(By.className("aptListButton"));
                for(WebElement btn: btns)
                {
                    System.out.println("BtnText: "+btn.getText());
                    if(btn.getText().equals("Configure"))
                    {
                        System.out.println("Actual Configure Button Found!");
                        scrollToElement(driver,btn);
                        forcePageScrollUp(driver);
                        Thread.sleep(500);
                        prodFound = true;
                        Assert.assertTrue(btn.isDisplayed());
                        System.out.println("006_Product name appears on Configuration page: "+btn.isDisplayed());
                        scrollToElement(driver,btn);
                        btn.click();
                        System.out.println("002_Configuration Button is clicked! ");
                        break;
                    }

                }

            }
        }
        if(!prodFound)
        {
            HashMap<String,String> prodList = prod.getItemMap("Step 2 Setup Categories","Product Name","Product Code");
            for(String optKey: prodList.keySet())
            {
                products = driver.findElements(By.className("aptProductCatalogLine"));
                System.out.println("Number of Products found:"+products.size());
                productName = optKey;
                for(WebElement product : products) {
                    //aptProductInfoContainer
                    String prodName = product.findElement(By.className("aptProductInfoFields")).getText().split("-")[0].trim();

                    if (productName.contains(prodName) || prodName.contains(productName)) {
                        int currentIndex = products.indexOf(product);
                        System.out.println("Product found: " + prodName);
                        WebElement btn = product.findElement(By.className("aptProductButttons"));
                        scrollToElement(driver, btn);
                        forcePageScrollUp(driver);
                        Thread.sleep(500);
                        prodFound = true;
//                        Assert.assertTrue(btn.isDisplayed());
                        System.out.println("006_Product name appears on Configuration page: " + btn.isDisplayed());
                        scrollToElement(driver,btn);
                        driver.findElements(By.className("aptProductCatalogLine")).get(currentIndex).findElement(By.className("aptProductButttons")).click();
                        //product.findElement(By.className("aptProductButttons")).click();
                        //btn.click();
                        System.out.println("002_Configuration Button is clicked! ");
                        break;
                    }
                    if(prodFound)
                    {
                        break;
                    }
                }
            }
        }
        return prodFound;
//        Assert.assertTrue(prodFound, "Product not found in on the catlog page");

    }
    public static void clickOfferingNextBtn(WebDriver driver)
    {
        waitForElement(driver,offeringNextBtn).click();
    }
    public static boolean verifyProductAttributes(WebDriver driver, Product prod)
    {
        if(prod.getProductData().get("Step 1b Attributes") == null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
