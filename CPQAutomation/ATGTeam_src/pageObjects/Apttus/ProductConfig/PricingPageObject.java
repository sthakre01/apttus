package pageObjects.Apttus.ProductConfig;

import appModules.Option;
import appModules.Product;
import appModules.TierPrice;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pageObjects.BasePageObject;
import testCases.ValidationTestCases.BaseValidationTestCase;

import java.util.HashMap;
import java.util.List;

/**
 * Created by joe.gannon on 3/18/2016.
 */
public class PricingPageObject extends BasePageObject
{
    public static By optionToggleBtn = By.className("showOptionButton");
    public static By optionPricingTable = By.className("aptLineItemOptionsTable");
    public static By productNameField = By.xpath("//*[@id=\"lookup01tP0000001QOYe00NP0000000h5u1\"]");
    public static By productCodeField = By.id("ProductCode_ileinner");
    public static void clickToggleBtn(WebDriver driver) throws Exception
    {
        try{
//            scrollToElementByLocator(driver,optionToggleBtn);
            waitForElement(driver,optionToggleBtn).click();

        }
        catch (NullPointerException e)
        {
            waitForSpinner(driver);
            ProductConfigPageObject.clickGoToPricing(driver);
            clickToggleBtn(driver);


        }
    }

    public static void getPricingTable(WebDriver driver, Product prod) throws Exception
    {
        HashMap<String,Option> optionMap =  prod.getProductPriceMap();
        boolean optionIsTierPrice = false;
        waitForElement(driver, optionPricingTable);
        WebElement table = driver.findElement(optionPricingTable);
        WebElement body = table.findElement(By.tagName("tbody"));
        List<WebElement> rows = body.findElements(By.tagName("tr"));
        System.out.println("NumRows: "+ rows.size());
        boolean allPass = true;

        if(prod.getTierPrices() != null)
        {
            for(TierPrice tp: prod.getTierPrices())
            {
                if(optionMap.get(tp.getOptionName()) != null)
                {
                    System.out.println("Option appears in Tier Price List");
                    optionMap.remove(tp.getOptionName());
                }
            }
        }


        Thread.sleep(2000);
        for(WebElement row: rows)
        {
            List<WebElement> cells = row.findElements(By.tagName("td"));
            String prodName = cells.get(1).getText().split("\\(New")[0].trim();
            String price = cells.get(5).getText().split("\\(")[0].trim().split(" ")[1].replaceAll(",","");
            String chargeType = cells.get(4).getText().trim();
            Option opt = optionMap.get(prodName);
            try{
                System.out.println("CHECKING OPTION: "+opt.getOptionName());

            }
            catch(NullPointerException e)
            {
                opt = new Option();
                opt.setOptionName("null");
            }

            String optFee;
            //If the option appears in the option map, remove it and move on

            if(chargeType.equals("Monthly Fee"))
            {
                optFee = opt.getMonthlyFee();
                if(optFee == null)
                {
                    optFee = "0.00";
                }
            }
            else
            {
                try
                {
                    optFee = opt.getOneTimeFee();
                }
                catch(NullPointerException e)
                {
                    optFee = "0";
                }


            }
            optFee = BaseValidationTestCase.round(optFee)+"";
            System.out.println("optFee: "+optFee+ " price: "+price);
            //Round optFee

            boolean feeMatches = (price.equals(optFee) || price.contains(optFee));
            if(!feeMatches)
            {
                allPass = false;
                System.out.println("[FAIL]: "+opt.getOptionName()+" Does not match!: SpreadSheetValue: "+optFee+" || WebValue: "+price);
            }
            else
            {
                System.out.println("[PASS] "+opt.getOptionName()+" || Pricing Matches: SpreadSheetValue: "+optFee+" || "+"WebValue: "+price);
            }
        }
        System.out.println("017//018_All Product Pricing matches: "+allPass);
        Assert.assertTrue(allPass);
    }

    public static boolean validateProductNameOnPricing(WebDriver driver, String productName)
    {
        //TODO: Fix the product name validation
//        return (productName.contains(driver.findElement(By.partialLinkText(productName)).getText()) || driver.findElement(By.partialLinkText(productName)).getText().contains(productName));
        return true;
    }
    public static void validateTierPrice(WebDriver driver, TierPrice tp)
    {
        waitForElement(driver, optionPricingTable);
        WebElement table = driver.findElement(optionPricingTable);
        WebElement body = table.findElement(By.tagName("tbody"));
        List<WebElement> rows = body.findElements(By.tagName("tr"));
        boolean optionFound = false;

        for(WebElement row: rows)
        {

            List<WebElement> cells = row.findElements(By.tagName("td"));
            String prodName = cells.get(1).getText().split("\\(New")[0].trim();
            String price = cells.get(5).getText().split("\\(")[0].trim().split(" ")[1].replaceAll(",","");
            String chargeType = cells.get(4).getText().trim();
            System.out.println("Tier: Checking "+prodName+" || TierName: "+tp.getOptionName());
            if(prodName.equals(tp.getOptionName()))
            {
                optionFound=true;
                System.out.println("Tier Option Found: "+tp.getOptionName());
                //convert both to floats
                String foundPrice = String.format("%.2f",Float.parseFloat(price));
                String existingPrice = String.format("%.2f",Float.parseFloat(tp.getTierPrice()));
                if(foundPrice.equals(existingPrice))
                {
                    System.out.println("[PASS] "+tp.getOptionName()+" || Value: "+tp.getTierValue()+ " || SheetPrice: "+existingPrice+ " || WebPrice: "+foundPrice);
                }
                else
                {
                    System.out.println("[FAIL] "+tp.getOptionName()+" || Value: "+tp.getTierValue()+ " || SheetPrice: "+existingPrice+ " || WebPrice: "+foundPrice);
                }
                break;
            }
        }
        if(!optionFound)
        {
            System.out.println("[ERROR] "+tp.getOptionName()+" not found!");
        }

    }
    public static boolean validateProductCode(WebDriver driver, Product prod) throws Exception
    {

        String productCode = prod.getProductCode();
//        String prodName = prod.getProductName();
        String prodName = prod.getBaseProdName();
        if(prod.getProductName().contains("GLB"))
        {
            prodName = prodName.split(("GLB"))[0].trim();
        }
//        prodName = prodName.split(" IRE")[0];
        System.out.println("Trying to click: "+ prod.getProductName());
        productNameField = By.partialLinkText(prodName);
        WebElement el = driver.findElement(productNameField);
        scrollToElement(driver,el);
        forcePageScrollUp(driver);
        driver.findElement(productNameField).click();
        String prodCodeFound = waitForElement(driver,productCodeField).getText();
        driver.navigate().back();
        return productCode.equals(prodCodeFound);

    }

    public static boolean validateBundleCodes(WebDriver driver, Product prod) throws Exception
    {
        //populate BundleCodeMap
        HashMap<String,String> bundleCodeMap = prod.getItemMap("Step 3 Setup Bundles","Bundle","Bundle Code");
        HashMap<String,String> prodCodeMap = prod.getItemMap("Step 1 Setup Products", "Product Name", "Product Code");

        waitForElement(driver,optionToggleBtn);
        scrollToElementByLocator(driver,optionToggleBtn);
        forcePageScrollUp(driver);
        waitForElement(driver,optionToggleBtn).click();

        Thread.sleep(10000);
        List<WebElement> rows = driver.findElements(By.className("aptOptionRow"));
        System.out.println("rowSize: "+rows.size());
        for(int i=0; i<rows.size();i++)
        {
            try {
                WebElement row = driver.findElements(By.className("aptOptionRow")).get(i);
                String rowClassName = row.getAttribute("className");
                String optName = row.findElements(By.tagName("td")).get(1).getText().split("\\(New")[0].trim();
                WebElement rowLink = row.findElements(By.tagName("td")).get(1);
                scrollToElement(driver, rowLink);
                forcePageScrollUp(driver);
                Thread.sleep(1000);
                row.findElements(By.tagName("a")).get(1).click();
                Thread.sleep(5000);
                String foundProdCode = waitForElement(driver, productCodeField).getText();
                System.out.println("010_ " + optName + " Bundle Code & Product Code Matches: " + prodCodeMap.get(optName).equals(foundProdCode));
                System.out.println("021_ " + optName + " Family Code: " + prodCodeMap.get(optName).equals(foundProdCode));
                driver.navigate().back();
                Thread.sleep(3000);
                scrollToElementByLocator(driver, optionToggleBtn);
                forcePageScrollUp(driver);
                waitForElement(driver, optionToggleBtn).click();
                waitForElement(driver, By.className(rowClassName));
            }
            catch(NullPointerException e)
            {
                System.out.println("Option Name is Null");
                e.printStackTrace();
            }
        }

        return true;
    }

    public static void clickToggleButton(WebDriver driver)
    {
        waitForElement(driver,optionToggleBtn);
        driver.findElement(optionToggleBtn).click();
    }


}
