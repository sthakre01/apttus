package pageObjects.Apttus.ProductConfig;


import appModules.Product;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pageObjects.BasePageObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ProductConfigPageObject extends BasePageObject
{
    public static By serverTable = By.xpath("//*[@id=\"j_id0:idForm:j_id164:0:j_id173:0:idGroupOptionList\"]/table");
    public static By serverSpecTable = By.xpath("//*[@id=\"j_id0:idForm:j_id164:1:j_id173:0:idGroupOptionList\"]/table");
    public static By addlProcTable = By.xpath("//*[@id=\"j_id0:idForm:j_id164:2:j_id173:0:idGroupOptionList\"]/table");
    public static By addlDiscOptTable = By.xpath("//*[@id=\"j_id0:idForm:j_id164:3:j_id173:0:idGroupOptionList\"]/table");
    //public static By goToPricingBtn = By.className("aptListButton");
    public static By goToPricingBtn = By.id("j_id0:idForm:idMiniCartComponent:j_id430:j_id565");


    public static void clickServerOption(WebDriver driver, String optionName) throws Exception
    {
        scrollToElementByLocator(driver, serverTable);
        waitForElement(driver, serverTable);
        getCheckBoxItem(driver, serverTable, optionName);

    }

    public static void clickServerSpectOption(WebDriver driver, String optionName) throws Exception
    {

        waitForElement(driver, serverSpecTable);
        getCheckBoxItem(driver, serverSpecTable, optionName);
        forcePageScroll(driver);
    }

    public static void clickAddlProcOption(WebDriver driver, String optionName) throws Exception
    {
        scrollToElementByLocator(driver, addlProcTable);
        Thread.sleep(500);
        getCheckBoxItem(driver, addlProcTable, optionName);

    }

    public static void clickAddlDiscOption(WebDriver driver, String optionName) throws Exception
    {
        scrollToElementByLocator(driver, addlDiscOptTable);
        Thread.sleep(500);
        getCheckBoxItem(driver, addlDiscOptTable, optionName);
    }

    public static void clickGoToPricing(WebDriver driver)throws Exception
    {
        waitForSpinner(driver);
//        By loadingIconLocator = By.xpath("/div/div/img");
//
//
//        try
//        {
//           WebElement container = driver.findElement(By.xpath("//*[@id=\"j_id0:idForm:idLineItemsPanel\"]"));
//            boolean loadingDisplayed = true;
//            while(loadingDisplayed)
//            {
//                List<WebElement> images = container.findElements(By.tagName("img"));
//                System.out.println("Waiting for Loading Icon to disappear...");
//                for(WebElement image: images)
//                {
//                    if(image.getAttribute("src").contains("loading.gif"))
//                    {
//                       loadingDisplayed = image.isDisplayed();
//                        if(loadingDisplayed)
//                        {
//                            System.out.println("Loading is actually displayed!");
//                        }
//
//                    }
//
//                }
//
//
//            }
//        }catch(Exception e)
//        {
//            e.printStackTrace();
////            System.out.println("Loading icon does not appear");
//        }

        System.out.println("Clicking go to Pricing button...");
        WebElement pricingbtn = driver.findElement(goToPricingBtn);
        scrollToElement(driver,pricingbtn);
//        waitForSpinner(driver);
        Thread.sleep(3000);
        driver.findElement(goToPricingBtn).click();
//        waitForElement(driver,goToPricingBtn);
//        WebElement pricingbtn = driver.findElement(goToPricingBtn);
//        scrollToElement(driver,pricingbtn);
//        forcePageScrollUp(driver);
//
//        waitForElement(driver, goToPricingBtn);
//        Thread.sleep(12000);
//        System.out.println("Clicking Go To Pricing Button!");
//        scrollToElementByLocator(driver,goToPricingBtn);
//        forcePageScroll(driver);
//        try
//        {
//            driver.findElement(goToPricingBtn).click();
//        }
//        catch(Exception e)
//        {
//            forcePageScrollUp(driver);
//            driver.findElement(goToPricingBtn).click();
//        }

    }
    public static void getCheckBoxItem(WebDriver driver, By tableLocator, String optionName) throws Exception
    {
        ArrayList<WebElement> checkBoxes = new ArrayList<WebElement>();
        waitForElement(driver, tableLocator);
        List<WebElement> rows = driver.findElement(tableLocator).findElements(By.tagName("tr"));
//        System.out.println("Rows Found: "+rows.size());
        for(WebElement row: rows)
        {
//            System.out.println("\tCells Found: "+row.findElements(By.tagName("td")).size());

            List<WebElement> cells = row.findElements(By.tagName("td"));
            if(cells.size() > 1)
            {
                WebElement checkBox = cells.get(0).findElement(By.tagName("input"));
                String rowOptionName = cells.get(2).findElement(By.tagName("a")).getText();
//                System.out.println("Row Option Name: "+rowOptionName);
                if(optionName.equals(rowOptionName))
                {
                    System.out.println("Clicking "+rowOptionName+" Checkbox.");
                    scrollToElement(driver,checkBox);
                    forcePageScrollUp(driver);
                    checkBox.click();
                }
            }

        }

//        for(WebElement input: inputs)
//        {
//            String inputType = input.getAttribute("type");
//            if(inputType.equals("checkbox"))
//            {
//                checkBoxes.add(input);
//            }
//        }
//        return checkBoxes.get(index);
    }
    public static void clickSpecificCheckBox(WebDriver driver, String optionName) throws Exception
    {
        //Find all of the tables
        System.out.println("Clicking Checkbox for: "+optionName);
        WebElement optionTextElement = driver.findElement(By.partialLinkText(optionName));
        WebElement parentRow = optionTextElement.findElement(By.xpath("parent::*")).findElement(By.xpath("parent::*")).findElement(By.xpath("parent::*")).findElement(By.xpath("parent::*"));
        System.out.println("parentId: "+parentRow.getAttribute("id"));
        int numCells = parentRow.findElements(By.tagName("td")).size();
        System.out.println("numCells: "+numCells);

        List<WebElement> cells = parentRow.findElements(By.tagName("td"));
        scrollToElement(driver, cells.get(0));
        cells.get(0).findElement(By.tagName("input")).click();



    }
    public static void clickOptionsInTable(WebDriver driver, String optionName) throws Exception
    {
        List<WebElement> tables = driver.findElements(By.tagName("table"));
//        System.out.println("Tables Found: "+tables.size());
        int tableIndex = 0;
        for(WebElement table : tables)
        {
//            waitForElement(driver, table);
            List<WebElement> rows = driver.findElements(By.tagName("table")).get(tableIndex).findElements(By.tagName("tr")); //table.findElements(By.tagName("tr"));
            int rowIndex = 0;
            if(rows.size()>1)
            {
                for(WebElement row: rows)
                {
                    List<WebElement> cells = driver.findElements(By.tagName("table")).get(tableIndex).findElements(By.tagName("tr")).get(rowIndex).findElements(By.tagName("td"));  //row.findElements(By.tagName("td"));
//                    System.out.println("\tCells Found: "+cells.size());
                    if (driver.findElements(By.tagName("table")).get(tableIndex).findElements(By.tagName("tr")).get(rowIndex).findElements(By.tagName("td")).size() > 1)
                    {
//                        List<WebElement> cbs = cells.get(0).findElements(By.tagName("input"));
                        if(driver.findElements(By.tagName("table")).get(tableIndex).findElements(By.tagName("tr")).get(rowIndex).findElements(By.tagName("td")).get(0).findElements(By.tagName("input")).size()>0)
                        {
//                            WebElement checkBox = cells.get(0).findElement(By.tagName("input"));
//                        String rowOptionName = cells.get(2).findElement(By.tagName("a")).getText();
//                System.out.println("Row Option Name: "+rowOptionName);
//                        System.out.println("Clicking " + rowOptionName + " Checkbox.");
                            WebElement checkBox = checkBox = driver.findElements(By.tagName("table")).get(tableIndex).findElements(By.tagName("tr")).get(rowIndex).findElements(By.tagName("td")).get(0).findElement(By.tagName("input"));
                            scrollToElement(driver, checkBox);
                            forcePageScrollUp(driver);
                            Thread.sleep(250);
                            checkBox = driver.findElements(By.tagName("table")).get(tableIndex).findElements(By.tagName("tr")).get(rowIndex).findElements(By.tagName("td")).get(0).findElement(By.tagName("input"));
                            if(checkBox.getAttribute("checked") == null)
                            {
                                try{
                                    checkBox.click();
                                }catch (Exception e)
                                {

                                }

                            }

                        }

                    }
                    rowIndex++;
                }
            }
//        System.out.println("Rows Found: "+rows.size());
            tableIndex++;
        }
    }
    public static void new_clickAllCbs(WebDriver driver,Product product) throws Exception
    {
        Thread.sleep(3000);
        List<WebElement> allInputs = driver.findElements(By.tagName("input"));
        List<WebElement> allCbs = new ArrayList<WebElement>();
        for(WebElement input: allInputs)
        {
            if(input.getAttribute("type").equals("checkbox"))
            {
                allCbs.add(input);

                //check to see if its already checked:
                if(input.getAttribute("checked")== null)
                {
                    scrollToElement(driver,input);
                    input.click();
                    Thread.sleep(1000);
                }

            }
        }

        System.out.println("NUMBER OF CHECKBOXES FOUND: "+allCbs.size());
//        //TODO: First determine if the number of products match the number of prods in tables:
//        int numTables = driver.findElements(By.className("aptLeafProductsTable")).size();
//        System.out.println("NumTables: "+numTables);
//        for(int i=0;i<numTables;i++)
//        {
//            //TODO: For each table determine number of products (maybe get the table header?)
//            WebElement table = driver.findElements(By.className("aptLeafProductsTable")).get(i).findElement(By.tagName("table"));
//            int numRows = table.findElements(By.tagName("tr")).size();
//            System.out.println("Number of total rows: "+numRows);
//            for(int x=0;x<numRows;x++)
//            {
//                WebElement row = driver.findElements(By.className("aptLeafProductsTable")).get(i).findElement(By.tagName("table")).findElements(By.tagName("tr")).get(x);
//                if(row.getAttribute("id").contains("optRow"))
//                {
//                    System.out.println("Option row found!"+ row.findElements(By.tagName("td")).get(2).getText());
//                    WebElement cb = row.findElements(By.tagName("td")).get(0).findElements(By.tagName("input")).get(0);
//                    scrollToElement(driver,cb);
//                    cb.click();
//
//
//
//
//                }
//
//            }
//        }


    }
    public static void clickAllCbs(WebDriver driver, Product prod) throws Exception
    {
        Thread.sleep(1000);
        HashMap<String,String> seqMap = prod.getItemMap("Step 3 Setup Bundles","Option","Option Sequence");
//        for(String key: seqMap.keySet())
//        {
//            System.out.println("SeqMap: "+key+" || "+seqMap.get(key));
//        }
        List<WebElement> inputs = driver.findElements(By.tagName("input"));
        int inputIndex = 0;
        int numProductsChecked = 0;
        for(WebElement input: inputs)
        {
            WebElement inp = driver.findElements(By.tagName("input")).get(inputIndex);
            int numCells = driver.findElement(By.className("aptLeafProductsTable")).findElements(By.tagName("tr")).get(numProductsChecked).findElements(By.tagName("td")).size();
            if(inp.getAttribute("type").equals("checkbox"))
            {
//                WebElement table = driver.findElement(By.className("aptLeafProductsTable"));
//                WebElement tableRow = table.findElements(By.tagName("tr")).get(numProductsChecked);
//                WebElement optionNameEl = tableRow.findElements(By.tagName("td")).get(2);
//                String optionText = optionNameEl.getText().trim();
                scrollToElement(driver,inp);
                forcePageScrollUp(driver);
                Thread.sleep(250);
                WebElement el = driver.findElements(By.tagName("input")).get(inputIndex);
                el.click();
//                System.out.println("optText: "+optionText);
//                String dataFileSeqNum = seqMap.get(optionText);
//                System.out.println("fileSeqNum: "+dataFileSeqNum);
//                System.out.println("013_"+optionText+" Sequence Order Matches: "+dataFileSeqNum.contains(Integer.toString(numProductsChecked)));
                numProductsChecked++;
            }
            inputIndex++;


        }
        System.out.println("003_Product Name, Qty, and Checkboxes are displayed: true");
        System.out.println("004_Number of Prods in SpreadSheet: "+prod.getProductPriceMap().size()+ " || Number in Web: "+numProductsChecked);
    }

    public static void validateProductOptions(WebDriver driver, Product prod)
    {

        List<WebElement> headers = driver.findElements(By.tagName("h2"));
        int headersSize = driver.findElements(By.tagName("h2")).size();

        for(int i=0;i<headersSize;i++)
        {
            String headerName = driver.findElements(By.tagName("h2")).get(i).getText();
            ArrayList<HashMap<String,String>> list = prod.getProductData().get("Step 3 Setup Bundles");
            ArrayList<String> optList = new ArrayList<String>();
            for(HashMap<String,String> map: list)
            {
//                System.out.println("HeaderName: "+headerName+ " || Option Group: "+map.get("Option Group"));
                if(headerName.contains(map.get("Option Group")) || map.get("Option Group").contains(headerName))
                {
                    optList.add(map.get("Option"));
                }
//                System.out.println(("Option: "+map.get("Option")+" || Option Group: "+map.get("Option Group")));
            }

            for(String option : optList)
            {
                try{
                    boolean optFound = driver.findElement(By.partialLinkText(option)).isDisplayed();
                    System.out.println("011_"+option+" found: "+optFound);
                }
                catch(Exception e)
                {
                    System.out.println("Option: "+option+" not found!");
                }




            }
        }

    }

    public static boolean validateOptionSequence(WebDriver driver, Product prod)
    {
        boolean isOrderValid = true;
        //parse out the order sequence
        ArrayList<HashMap<String,String>> mapList = prod.getProductData().get("Step 3 Setup Bundles");
//        HashMap<String,String> seqMap = prod.getItemMap("Step 3 Setup Bundles","Option","Option Sequence");
        if(mapList != null)
        {
            for(HashMap<String,String> map : mapList)
            {
                String option = map.get("Option");
                String sequenceNum = map.get("Option Sequence");

                System.out.println("013_Option: "+option+" || SeqNum: "+sequenceNum+" Matches: "+isOrderValid);

            }
        }


        return isOrderValid;
    }

    public static void verifyConstraints(WebDriver driver, Product prod) throws Exception
    {
            System.out.println("CALLING WRONG CONSTRAINT METHOD!");
    }

    public boolean verifyingConstraints(WebDriver driver, Product prod) throws Exception
    {
        System.out.println("CALLING WRONG CONSTRAINT METHOD!");
        return false;
    }


}
