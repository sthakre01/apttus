package pageObjects.Apttus;

import appModules.Attribute;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pageObjects.BasePageObject;

import java.util.List;


public class OfferingDataPageObject extends BasePageObject
{
    private static By productFamily = By.id("j_id0:j_id2:j_id3:j_id35:j_id40");
    private static By startDate = By.id("j_id0:j_id2:j_id3:j_id35:j_id43");
    private static By termField = By.id("j_id0:j_id2:j_id3:j_id35:j_id47");
    private static By customerRef = By.id("j_id0:j_id2:j_id3:j_id35:j_id39");
    private static By newOfferingDataBtn = By.id("//*[@id=\"a47P00000004Rlk_00NP0000000nHwZ\"]/div[1]/div/div[1]/table/tbody/tr/td[2]/input");
    private static By dateFormat = By.className("dateFormat");
    private static By deliverLocation = By.id("j_id0:j_id2:j_id3:j_id35:j_id41");
    private static By saveBtn = By.xpath("//*[@id=\"j_id0:j_id2:j_id3:j_id30\"]/input[1]");
    private static By offeringDataTable = By.xpath("//*[@id=\"a47P00000004Rrs_00NP0000000nHwZ_body\"]/table");
    ////*[@id="a47P00000004RpI_00NP0000000nHwZ"]/div[1]/div/div[1]/table/tbody/tr/td[2]/input

    public static void clickNewOfferingdataButton(WebDriver driver) throws Exception
    {
        System.out.println("Clicking on New Offering button.");
//        WebElement el = driver.findElement(By.xpath("//*[@id=\"a47P00000004Rpm_00NP0000000nHwZ\"]/div[1]"));
        Thread.sleep(12000);
        List<WebElement> lists = driver.findElements(By.className("bRelatedList"));
        System.out.println("NumLists: "+lists.size());
        try {
        for(WebElement list: lists)
        {
            List<WebElement> inputs = list.findElements(By.tagName("input"));
            System.out.println("NumInputs: "+inputs.size());



                for (WebElement input : inputs) {
                    String value = input.getAttribute("value");
                    if (value == null) {
                        System.out.println("is this null");
                    }
                    if (input.getAttribute("value").equals("New Offering Data")) {
                        input.click();
                        break;
                    }
                }
            }
        }
        catch(StaleElementReferenceException e)
            {
                System.out.println("Stale element for some reason");
            }

    }

    public static String fillOutOfferingData(WebDriver driver, Attribute attrib) throws Exception
    {
        attrib.print();
        driver.findElement(customerRef).sendKeys("AutomationCustRef");
//        driver.findElement(productFamily).sendKeys("GLB  - Network Services");
        String currDate = driver.findElement(dateFormat).getText().replaceAll("\\[","").replaceAll("\\]","");
        driver.findElement(startDate).sendKeys(currDate);
        driver.findElement(deliverLocation).sendKeys("Austin, TX");
        driver.findElement(saveBtn).click();

        //Get the offering data name
        By offeringDataName = By.xpath("//*[@id=\"a47P00000004V5G_00NP0000000nHwZ_body\"]/table/tbody/tr[2]/th/a");

        By tables = By.className("pbBody");
        waitForElement(driver,tables);
//        WebElement table = driver.findElement(offeringDataTable);
//        List<WebElement> rows = table.findElements(By.tagName("tr"));
//        System.out.println("NumberOfOfferingRows: "+rows.size());
//        WebElement lastRow = rows.get(rows.size()-1);
//        String odName = lastRow.findElements(By.tagName("th")).get(0).getText();
        ////*[@id="a47P00000004RxH_00NP0000000nHwZ_body"]/table/tbody/tr[2]/th/a
            simpleSleep(3000);
            List<WebElement> cells = driver.findElements(By.tagName("th"));
            for(WebElement cell: cells)
            {
                if(cell.getText().equals("Offering Data Name"))
                {
                    System.out.println("odTable Found!");
                    WebElement parentElement = driver.findElement(By.xpath("./.."));
                    List<WebElement> rows = parentElement.findElements(By.tagName("tr"));
                    System.out.println("TableRows: "+rows.size());

                }
            }

//        System.out.println("NumOdTables: "+odTables.size());
        simpleSleep(3000);
        String odName = driver.findElement(offeringDataName).getText();

        System.out.println("offering data name: "+odName);

        return odName;

    }



}
