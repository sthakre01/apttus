package pageObjects.Apttus;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ApproveAndGenerateGMSAPageObjects {
	
	public static WebElement webElement = null;
	
	// To identify the EnterSearchItem
	public Select fn_SelectApproval(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//tr/td[contains(text(),'Approval Stage')]//following::td[1]/div/span/select"))));
		return webList;
	}

	// To Click on CreateAgreementGMSA button
	public WebElement fn_CreateGMSA(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td[contains(text(),\"Create GMSA Agreement\")]//following::td[1]/div/a/img")));
		return webElement;
	}
	
	//To click on draft element
	public WebElement fn_ClickDraft(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td[contains(text(),\"Approval Stage\")]//following::td[1]/div")));
		return webElement;
	}
	
	//To click on Save
	public WebElement fn_ClickSave(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td/input[@name=\"inlineEditSave\"]")));
		return webElement;
	}
	
	//To Select the GMSA
	public Select fn_SelectGMSA(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath("//div/label[contains(text(),\"Record Type of new record\")]//following::select[1]")));
		return webList;
	}
	
	//To click on Continue button
	public WebElement fn_ClickContinueBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td/input[@value=\"Continue\"]")));
		return webElement;
	}
	
}
