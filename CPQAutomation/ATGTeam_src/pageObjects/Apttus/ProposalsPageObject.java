package pageObjects.Apttus;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pageObjects.BasePageObject;

/**
 * Created by joe.gannon on 2/10/2016.
 */
public class ProposalsPageObject extends BasePageObject
{
    public static By propTab = By.id("01rP00000000O2I_Tab");
    public static ProposalFormPageObject clickProposalsTab(WebDriver driver)
    {
        driver.findElement(propTab).click();
        return new ProposalFormPageObject();
    }

}
