package appModules;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pageObjects.Apttus.ProductConfig.PricingPageObject;
import pageObjects.Apttus.ProductConfig.ProductConfigPageObject;
import pageObjects.BasePageObject;
import sun.plugin.dom.exception.InvalidStateException;
import utility.EnvironmentDetails;
import utility.ExcelHandler;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValidationModule extends BasePageObject
{
    //private static By saveBtn = By.id("j_id0:idForm:j_id581:11:j_id584");
    private static By saveBtn = By.xpath("//*[@id=\"j_id0:idForm:j_id581:12:j_id584\"]");
    public static ArrayList<Product> LoadProducts()
    {
        Option o = new Option();
        File directory = new File(EnvironmentDetails.WORKSHEETS_PATH);
        File[] files = directory.listFiles();
        System.out.println(files.length+" Files Found!");
        ArrayList<Product> productList = new ArrayList<Product>();
        List<Map<String,String>> products = new ArrayList<Map<String,String>>();
        Map<String,Map<String,String>> fullMap = new HashMap<String,Map<String,String>>();
        for(File file : files)
        {
            String fullName = file.getName();
            String productName = fullName.split("-")[0];

            //TODO: Find number of dashes
            int numDashes =fullName.split("-").length;

            String type = fullName.split("-")[numDashes-1];
            if(type.contains("Pricing"))
            {
                System.out.println("Pricing Type Found!: "+ file.getAbsolutePath());
                String pricePath = file.getAbsolutePath();
                if(fullMap.containsKey(productName))
                {
                    Map<String,String> existingMap = fullMap.get(productName);
                    existingMap.put("pricePath", pricePath);
                    fullMap.put(productName,existingMap);
                }
                else
                {
                    Map<String,String> newMap = new HashMap<String,String>();
                    newMap.put("pricePath", pricePath);
                    fullMap.put(productName,newMap);
                }

            }
            else
            {
                System.out.println("Product Type Found!: "+ file.getAbsolutePath());
                String prodPath = file.getAbsolutePath();
                if(fullMap.containsKey(productName))
                {
                    Map<String,String> existingMap = fullMap.get(productName);
                    existingMap.put("prodPath", prodPath);
                    fullMap.put(productName,existingMap);
                }
                else
                {
                    Map<String,String> newMap = new HashMap<String,String>();
                    newMap.put("prodPath", prodPath);
                    fullMap.put(productName,newMap);
                }
            }
        }

        for(String key : fullMap.keySet())
        {
            Map<String,String> map = fullMap.get(key);
            Product prod = new Product(key);
            prod.setProductPath(map.get("prodPath"));
            prod.setProductPricePath(map.get("pricePath"));
            productList.add(prod);

        }
        for(Product prod : productList){
//            prod.printProduct();
            ExcelHandler ex = new ExcelHandler();
            prod.setProductData(ex.readDataSheet(prod.getProductPath(),"Test"));
            prod.setProductName(prod.getProductData().get("Step 2 Setup Categories").get(0).get("Product Name"));

            if(prod.getProductPricePath() != null)
            {
                System.out.println("Loading in Products and Prices");
                prod.setPricingData(ex.readDataSheet(prod.getProductPricePath(), "placeholder"));
                HashMap<String, Option> priceMap = prod.getProductPriceMap();
                for(String key : priceMap.keySet())
                {
                    System.out.println("Product: "+key+" || Monthly: "+priceMap.get(key).getMonthlyFee()+ " || One-Time: "+priceMap.get(key).getOneTimeFee());
                }

            }
        }

        return productList;
    }
    public static void changeOptionQty(WebDriver driver, String qty, Product prod)
    {
        List<WebElement> tBodies = driver.findElements(By.className("aptGroupHeader"));
        System.out.println("QtyBodies: "+tBodies.size());
        int numBodies = tBodies.size();
        int bodyInd = 0;

        for(int x=0;x<numBodies;x++)
        {
            int rowInd = 0;
            WebElement body = driver.findElements(By.className("aptGroupHeader")).get(bodyInd);
            //TODO: get the root tbody for the child element
            WebElement td = body.findElement(By.xpath("./.."));
            WebElement tr = td.findElement(By.xpath("./.."));
            WebElement tBody = tr.findElement(By.xpath("./.."));
            List<WebElement> rows = tBody.findElements(By.tagName("tr"));
            int numRows = rows.size();


            if(rows.size()>1)
            {
                System.out.println("QtyRowSizeGrtrThan1");
                for(int i=0;i<numRows;i++)
                {
                    body = driver.findElements(By.className("aptGroupHeader")).get(bodyInd);
                    td = body.findElement(By.xpath("./.."));
                    tr = td.findElement(By.xpath("./.."));
                    tBody = tr.findElement(By.xpath("./.."));
                    //List<WebElement> rows = tBody.findElements(By.tagName("tr"));
                    WebElement row = tBody.findElements(By.tagName("tr")).get(rowInd);
                    if(row.getAttribute("class").contains("lineItem-"))
                    {
                        //Get the specific cell values
                        System.out.println("QtyCellFound");
                        List<WebElement> cells = row.findElements(By.tagName("td"));
                        String optText = cells.get(2).getText().trim();
                        System.out.println("QtyTxt: "+optText);
                        Option opt = prod.getProductPriceMap().get(optText);
                        if(opt == null)
                        {
                            System.out.println("OPTION IS NULL!, Checking Tier Prices");
                            //look for the option in the TierList
                            for(TierPrice tp: prod.getTierPrices())
                            {
                                if(tp.getOptionName().equals(optText))
                                {
                                    opt = new Option();
                                    System.out.println("TierPrice found for Qty");
                                    opt.setOptionName(tp.getOptionName());
                                    opt.setMonthlyFee(tp.getTierPrice());
                                    opt.setMaxQty(tp.getTierValue());
                                    //adding option to option list in product
                                    prod.addOptionToProduct(opt);
                                    break;

                                }
                            }
                        }
//                        WebElement qtyCell = cells.get(5).findElement(By.tagName("input"));
                        //TODO: Wait for input to become available
                        //Thread.sleep(10000); //Might need to uncomment
                        try{

                            String optMaxQuantity = opt.getMaxQty();
                            if(cells.get(5).findElements(By.tagName("input")).size()<=0)
                            {
                                //skip it
                            }else if(!(optMaxQuantity.equals("NO_MAX_QTY"))){
//                                WebElement qtyCell = cells.get(5).findElement(By.tagName("input"));
//                                waitForElementEditable(driver, qtyCell);
//                                qtyCell.clear();
//                                waitForElementEditable(driver, qtyCell);
//                                qtyCell.sendKeys(opt.getMaxQty().replaceAll("\\.0*$", ""));
                                waitForSpinner(driver);
                                simpleSleep(2000);
                                getQuantityCheckBox(driver,opt.getOptionName()).click();
                                waitForSpinner(driver);
                                getQuantityCheckBox(driver,opt.getOptionName()).clear();
                                waitForSpinner(driver);
                                getQuantityCheckBox(driver,opt.getOptionName()).sendKeys(qty);
                                System.out.println("QtyMaxQty" + opt.getMaxQty());
                            }
                        }catch(InvalidStateException e)
                        {
                            WebElement bdy = driver.findElements(By.className("aptGroupHeader")).get(bodyInd);
                            WebElement tdd = bdy.findElement(By.xpath("./.."));
                            WebElement trr = tdd.findElement(By.xpath("./.."));
                            WebElement tBodyy = trr.findElement(By.xpath("./.."));
                            List<WebElement> rowss = tBodyy.findElements(By.tagName("tr"));
                            WebElement roww = rowss.get(rowInd);
                            if(roww.getAttribute("class").contains("lineItem-") && !(opt.getMaxQty().equals("NO_MAX_QTY")))
                            {
                                List<WebElement> cellss = roww.findElements(By.tagName("td"));
                                WebElement inputCell = cellss.get(5).findElement(By.tagName("input"));
                                waitForElementEditable(driver,inputCell);
                                inputCell.clear();
                                waitForElementEditable(driver,inputCell);
                                bdy = driver.findElements(By.className("aptGroupHeader")).get(bodyInd);
                                tdd = bdy.findElement(By.xpath("./.."));
                                trr = tdd.findElement(By.xpath("./.."));
                                tBodyy = trr.findElement(By.xpath("./.."));
                                rowss = tBodyy.findElements(By.tagName("tr"));
                                roww = rowss.get(rowInd);
                                cellss = roww.findElements(By.tagName("td"));
                                inputCell = cellss.get(5).findElement(By.tagName("input"));
                                inputCell.sendKeys(qty);

                                System.out.println("Setting max qty to: "+opt.getMaxQty());
                            }
                        }

                    }
                    rowInd++;
                }
            }

            bodyInd++;
        }
        driver.findElement(saveBtn).click();
        try
        {
            Thread.sleep(3000);
        }catch(Exception e)
        {
            e.printStackTrace();
        }
    }
    public static void setOptionQty(WebDriver driver, Product prod) throws Exception
    {
        //Thread.sleep(15000);  //Might need to uncomment
        List<WebElement> tBodies = driver.findElements(By.className("aptGroupHeader"));
        System.out.println("QtyBodies: "+tBodies.size());
        int numBodies = tBodies.size();
        int bodyInd = 0;

        for(int x=0;x<numBodies;x++)
        {
            int rowInd = 0;
            WebElement body = driver.findElements(By.className("aptGroupHeader")).get(bodyInd);
             //TODO: get the root tbody for the child element
            WebElement td = body.findElement(By.xpath("./.."));
            WebElement tr = td.findElement(By.xpath("./.."));
            WebElement tBody = tr.findElement(By.xpath("./.."));
            List<WebElement> rows = tBody.findElements(By.tagName("tr"));
            int numRows = rows.size();


            if(rows.size()>1)
            {
                System.out.println("QtyRowSizeGrtrThan1");
                for(int i=0;i<numRows;i++)
                {
                    body = driver.findElements(By.className("aptGroupHeader")).get(bodyInd);
                    td = body.findElement(By.xpath("./.."));
                    tr = td.findElement(By.xpath("./.."));
                    tBody = tr.findElement(By.xpath("./.."));
                    //List<WebElement> rows = tBody.findElements(By.tagName("tr"));
                    WebElement row = tBody.findElements(By.tagName("tr")).get(rowInd);
                    if(row.getAttribute("class").contains("lineItem-"))
                    {
                        //Get the specific cell values
                        System.out.println("QtyCellFound");
                        List<WebElement> cells = row.findElements(By.tagName("td"));
                        String optText = cells.get(2).getText().trim();
                        System.out.println("QtyTxt: "+optText);
                        Option opt = prod.getProductPriceMap().get(optText);
                        if(opt == null)
                        {
                            System.out.println("OPTION IS NULL!, Checking Tier Prices");
                            //look for the option in the TierList
                            if(prod.getTierPrices() != null)
                            {
                                for(TierPrice tp: prod.getTierPrices())
                                {
                                    if(tp.getOptionName().equals(optText))
                                    {
                                        opt = new Option();
                                        System.out.println("TierPrice found for Qty");
                                        opt.setOptionName(tp.getOptionName());
                                        opt.setMonthlyFee(tp.getTierPrice());
                                        opt.setMaxQty(tp.getTierValue());
                                        //adding option to option list in product
                                        prod.addOptionToProduct(opt);
                                        break;

                                    }
                                }
                            }

                        }else {

//                        WebElement qtyCell = cells.get(5).findElement(By.tagName("input"));
                            //TODO: Wait for input to become available
                            //Thread.sleep(10000); //Might need to uncomment
                            if (opt.getMaxQty() != null) {
                                try {

                                    String optMaxQuantity = opt.getMaxQty();
                                    if (cells.get(5).findElements(By.tagName("input")).size() <= 0) {
                                        //skip it
                                    } else if (!(optMaxQuantity.equals("NO_MAX_QTY"))) {
//                                WebElement qtyCell = cells.get(5).findElement(By.tagName("input"));
//                                waitForElementEditable(driver, qtyCell);
//                                qtyCell.clear();
//                                waitForElementEditable(driver, qtyCell);
//                                qtyCell.sendKeys(opt.getMaxQty().replaceAll("\\.0*$", ""));
                                        waitForSpinner(driver);
                                        Thread.sleep(1000);
                                        getQuantityCheckBox(driver, opt.getOptionName()).click();
                                        waitForSpinner(driver);
                                        getQuantityCheckBox(driver, opt.getOptionName()).clear();
                                        waitForSpinner(driver);
                                        getQuantityCheckBox(driver, opt.getOptionName()).sendKeys(opt.getMaxQty().replaceAll("\\.0*$", ""));
                                        System.out.println("QtyMaxQty" + opt.getMaxQty());
                                    }
                                } catch (InvalidStateException e) {
                                    WebElement bdy = driver.findElements(By.className("aptGroupHeader")).get(bodyInd);
                                    WebElement tdd = bdy.findElement(By.xpath("./.."));
                                    WebElement trr = tdd.findElement(By.xpath("./.."));
                                    WebElement tBodyy = trr.findElement(By.xpath("./.."));
                                    List<WebElement> rowss = tBodyy.findElements(By.tagName("tr"));
                                    WebElement roww = rowss.get(rowInd);
                                    if (roww.getAttribute("class").contains("lineItem-") && !(opt.getMaxQty().equals("NO_MAX_QTY"))) {
                                        List<WebElement> cellss = roww.findElements(By.tagName("td"));
                                        WebElement inputCell = cellss.get(5).findElement(By.tagName("input"));
                                        waitForElementEditable(driver, inputCell);
                                        inputCell.clear();
                                        waitForElementEditable(driver, inputCell);
                                        bdy = driver.findElements(By.className("aptGroupHeader")).get(bodyInd);
                                        tdd = bdy.findElement(By.xpath("./.."));
                                        trr = tdd.findElement(By.xpath("./.."));
                                        tBodyy = trr.findElement(By.xpath("./.."));
                                        rowss = tBodyy.findElements(By.tagName("tr"));
                                        roww = rowss.get(rowInd);
                                        cellss = roww.findElements(By.tagName("td"));
                                        inputCell = cellss.get(5).findElement(By.tagName("input"));
                                        inputCell.sendKeys(opt.getMaxQty());

                                        System.out.println("Setting max qty to: " + opt.getMaxQty());
                                    }
                                }
                            }
                        }


                    }
                    rowInd++;
                }
            }

            bodyInd++;
        }
        driver.findElement(saveBtn).click();
        try
        {
            Thread.sleep(3000);
        }catch(Exception e)
        {
            e.printStackTrace();
        }

    }
    public static void validateNonIntegerQty(WebDriver driver)
    {

    }
    public static void validateTierPrices(WebDriver driver, Product prod) throws Exception
    {
        System.out.println("Validating Tier Prices...");
//        driver.navigate().back();
        for(TierPrice tp: prod.getTierPrices())
        {
            //Navigate Back
            System.out.println("Checking Tier rate for: "+tp.getTierValue());
            System.out.println("Navigating back");
            driver.navigate().back();
            String tierName = tp.getOptionName();

            waitForElement(driver,By.className("aptLeafProductsTable"));
            WebElement container = driver.findElement(By.className("aptLeafProductsTable"));
            WebElement table = container.findElement(By.tagName("tbody"));
            int numRows = table.findElements(By.tagName("tr")).size();
            System.out.println("TierRowSize: "+numRows);
            for(int x=0; x<numRows;x++)
            {
                container = driver.findElement(By.className("aptLeafProductsTable"));
                table = container.findElement(By.tagName("tbody"));
                WebElement row = table.findElements(By.tagName("tr")).get(x);
                if(row.getAttribute("class").contains("lineItem-"))
                {
                    List<WebElement> cells = row.findElements(By.tagName("td"));
                    String optText = cells.get(2).getText().trim();
                    if(optText.trim().equals(tierName))
                    {
                        System.out.println("Tier Option Found: "+tierName);
                        //Get the quantyity input cell and enter the tier qty type
                        zxc\ty,.\
                        
                        getQuantityCheckBox(driver,tierName).click();
                        waitForSpinner(driver);
                        getQuantityCheckBox(driver,tierName).clear();
                        waitForSpinner(driver);
                        getQuantityCheckBox(driver,tierName).sendKeys(tp.getTierValue());
                        //wait for cells to refresh
//                        waitForElementEditable(driver,inputCell);
//                        getQuantityCheckBox(driver,tierName).clear();
//                        inputCell = getQuantityCheckBox(driver,tierName);
//                        waitForElementEditable(driver,inputCell);
//                        getQuantityCheckBox(driver,tierName).sendKeys(tp.getTierValue());
//                        inputCell = getQuantityCheckBox(driver,tierName);
//                        waitForElementEditable(driver,inputCell);

                        //Click gotopricing
                        Thread.sleep(3000);
                        ProductConfigPageObject.clickGoToPricing(driver);

                        //Click ToggleButton
                        PricingPageObject.clickToggleBtn(driver);

                        //Validate Product for option name
                        System.out.println("Checking Price for Tier: "+tp.getOptionName()+" || "+tp.getTierValue());
                        PricingPageObject.validateTierPrice(driver,tp);
                        break;



                    }
                }

            }



            //Find the existing qty box for the given Tier Price
            System.out.println("Checking TierPrice for: "+tierName);
            //Set the qty for the Tier Price
            System.out.println("Setting Quantity to: "+tp.getTierValue());
            //Go to Pricing

            //Validate Pricing
        }
    }
//    public static void waitForLoadingToComplete(WebDriver driver)
    {
        try
        {
            WebElement spinner = driver.findElement(By.className("spinnerImg-tree"));
            boolean spinnerDisplayed = spinner.isDisplayed();
            System.out.println("Waiting for Spinner to disappear..."+spinnerDisplayed);
            while(spinnerDisplayed)
            {
                spinnerDisplayed = driver.findElement(By.className("spinnerImg-tree")).isDisplayed();
                System.out.println("Spinner Displayed: "+spinnerDisplayed);
            }
        }catch(Exception e)
        {
            e.printStackTrace();
//            System.out.println("Loading icon does not appear");
        }
    }
    public static WebElement getQuantityCheckBox(WebDriver driver, String optionName)
    {
        waitForElement(driver,By.className("aptLeafProductsTable"));
        int numContainers = driver.findElements(By.className("aptLeafProductsTable")).size();

        for(int i=0;i<numContainers;i++) {
            WebElement container = driver.findElements(By.className("aptLeafProductsTable")).get(i);
            WebElement table = container.findElement(By.tagName("tbody"));
            List<WebElement> rows = table.findElements(By.tagName("tr"));

            //For each row, cycle through and grab the input element
            for (WebElement row : rows) {
                if (row.getAttribute("class").contains("lineItem-")) {
                    List<WebElement> cells = row.findElements(By.tagName("td"));
                    String optText = cells.get(2).getText().trim();

                    if (optText.equals(optionName)) {
                        //Grab the quantity cell and return it;
                        return row.findElement(By.className("aptQuantity"));
                    }

                }
            }
        }
        System.out.println("[ERROR] No Quantity Box found with Option Name: "+optionName);
        return null;
    }


}
