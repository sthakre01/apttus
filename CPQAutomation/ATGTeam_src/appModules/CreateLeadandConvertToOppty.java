package appModules;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.ConfigurationPageObjects;
import pageObjects.SalesForce.LeadCreationObjects;
import pageObjects.SalesForce.OfferingDataPO;
import utility.Log;
import utility.Utils;

public class CreateLeadandConvertToOppty {
	
	public boolean CreateLeadAndConvertToOppty(WebDriver driver,WebDriverWait webWait,String LeadCreationData,String ConversionData,String OpptyData) throws Exception
    {

		String[] arrLeadData= LeadCreationData.split("#");
		String[] arrConvData=ConversionData.split("#");
		String[] arrOpptData=OpptyData.split("#");
		WebElement webElement = null;
		Utils ObjUtils = new Utils();
		LeadCreationObjects ObjctsLead = new LeadCreationObjects();	
		//++++++++++++++CreateLeadAndConvertToOppty+++++++++++++++++++++++++++++++++++++++++++++++++++++++
	/*	//Click on Leads tab
		ObjctsLead.fn_SelectLeadsTab(driver, webWait).click();
		//click on New button
		ObjctsLead.fn_ClickNew(driver, webWait).click();
		//select the Record type
		//ObjctsLead.fn_SelectRecord(driver, webWait).selectByVisibleText("text");
		//click on Continue Button
		ObjctsLead.fn_ClickContinuebtn(driver, webWait).click();
		//Enter value for LastName
		ObjctsLead.fn_EnterLastName(driver, webWait).sendKeys(arrLeadData[0]);
		//enter value for Company Name
		ObjctsLead.fn_EnterCompany(driver, webWait).sendKeys(arrLeadData[1]);
		//enter value for Phone
		ObjctsLead.fn_EnterPhone(driver, webWait).sendKeys(arrLeadData[2]);
		//enter value for LeadCurrency
		//ObjctsLead.fn_SelectLeadCurrency(driver, webWait).selectByVisibleText(arrLeadData[3]);
		//Select value for Lead status
		ObjctsLead.fn_LeadStatus(driver, webWait).selectByVisibleText(arrLeadData[4]);
		//Select value for Lead stage
		//ObjctsLead.fn_LeadStage(driver, webWait).selectByVisibleText(arrLeadData[5]);
		//click on save button
		ObjctsLead.fn_ClickSaveBtn(driver, webWait).click();
		//click on convert Button
		ObjctsLead.fn_ClickLdConvertBtn(driver, webWait).click();	
		//Enter Account Name
		//ObjctsLead.fn_EnterAcctName(driver, webWait).sendKeys(ConvData[0]);
        boolean blnAccthandle = HandleLookUpWindow(driver,webWait,arrConvData[0]);
        if (blnAccthandle==true)
        	Log.info("Data is entered successfully in Account Lookup (New Window)");
        else
        	Log.info("Unable to enter data in Account Lookup (New Window)");*/
		//Enter Oppty Name
		ObjctsLead.fn_EnterOpptyName(driver, webWait).sendKeys(arrConvData[1]);
		//Enter subject
		ObjctsLead.fn_EnterSubject(driver, webWait).sendKeys(arrConvData[2]);
		// To Select the LMRtasktype
		ObjctsLead.fn_LMRtasktype(driver, webWait).selectByVisibleText(arrConvData[3]);
		//To Enter Due Date
		ObjctsLead.fn_Enterduedate(driver, webWait).click();
		//To click on Convert Button
		ObjctsLead.fn_ClickConvertBtn(driver, webWait).click();
		//++++++++++++++Create New Contact page+++++++++++++++++++++++++++++++++++++++++++++++++++++++
		ObjctsLead.fn_LeadContact(driver, webWait).selectByVisibleText("Create New Contact: ApptusAuto");
		//To click on Convert Button
		ObjctsLead.fn_ClickConvertBtn(driver, webWait).click();
		//++++++++++++++Enter fields of Oppty page+++++++++++++++++++++++++++++++++++++++++++++++++++++++
		//click on new oppty button
		ObjctsLead.fn_ClickNewOpptyBtn(driver, webWait).click();
		//click on continue button
		ObjctsLead.fn_ClickOpptyContBtn(driver, webWait).click();
		//Enter the Oppty Name field
		ObjctsLead.fn_EnterOpptunityyName(driver, webWait).sendKeys(arrOpptData[0]);
		//Enter the Oppty Type
		ObjctsLead.fn_SelectOpptyType(driver, webWait).selectByVisibleText(arrOpptData[1]);
		//select the Oppty Currency
		ObjctsLead.fn_SelectOpptyCurrency(driver, webWait).selectByVisibleText(arrOpptData[2]);
		//Primary product of interest
		WebElement SelectPPI = driver.findElement(By.id("00NG0000009A1OX"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",SelectPPI);
		Select select = new Select(SelectPPI);
		select.selectByVisibleText(arrOpptData[3]);
     	//enter the close date
		ObjctsLead.fn_EnterCloseDate(driver, webWait).sendKeys(arrOpptData[4]);
		ObjctsLead.fn_EnterOpptunityyName(driver, webWait).click();
		//select the stage
		ObjctsLead.fn_SelectStage(driver, webWait).selectByVisibleText(arrOpptData[5]);
		//Select the competetors	
		WebElement SelectCompetitors= driver.findElement(By.id("00NA00000034WuG_unselected"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",SelectCompetitors);
		Thread.sleep(1000);
		Actions action = new Actions(driver);
		action.moveToElement(SelectCompetitors).doubleClick().perform();
		//Click on Save Button
		WebElement ClickSave = driver.findElement(By.name("save"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",ClickSave);
		ClickSave.click();
		return true;
		
		
    }
	
	
	public boolean HandleLookUpWindow(WebDriver driver,WebDriverWait webWait,String Data)
	{
		

		String strParentWindow = driver.getWindowHandle();
		String strchildWindow = null;
		LeadCreationObjects ObjctLeads = new LeadCreationObjects();
		//Click on LookupWindow
		try 
		{
	    
		ObjctLeads.fn_ClickAcctLookUpwndw(driver,webWait).click();
		Thread.sleep(6000);
		//TODO: get the frame for the Child window
		Set<String> strWindowHandles = driver.getWindowHandles();   		
		strWindowHandles.remove(strParentWindow);
		strchildWindow = strWindowHandles.iterator().next();		
		driver.switchTo().window(strchildWindow);	// setting the window driver to the newly launched Print Frame Window.     
		WebElement rootTree = null;
		rootTree = driver.findElement(By.tagName("FRAMESET"));
		WebElement parentFrame = rootTree.findElement(By.name("searchFrame"));
		driver.switchTo().frame(parentFrame);
		driver.findElement(By.xpath("//*[@id=\"lksrch\"]")).sendKeys(Data);
		Thread.sleep(3000);
		driver.findElement(By.xpath("//div/input[@title=\"Go!\"]")).click();
		Thread.sleep(5000);//Click on the search item
		driver.switchTo().defaultContent();
		WebElement ResultsFrame = driver.findElement(By.name("resultsFrame"));
		driver.switchTo().frame(ResultsFrame);
		String strLen = driver.findElement(By.xpath("//table[@class='list']/tbody/tr[2]/th/a")).getText();
		if (strLen.length()>0)
			Log.info("Got the text from search window");
		else
			Log.info("Unable to Get the text from search window");		
		driver.findElement(By.xpath("//table[@class='list']/tbody/tr[2]/th/a")).click();
		driver.switchTo().window(strParentWindow);
		}
		
		catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();}
		
		return true;
	}
	
}
