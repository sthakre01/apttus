package appModules;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utility.Log;
import utility.Utils;
import pageObjects.SalesForce.ConfigurationPageObjects;
import pageObjects.SalesForce.OfferingDataPO;

public class CreateOfferingData {
	
	public Boolean fn_CreateOfferingData(WebDriver driver,WebDriverWait webWait,String strOfferingData) throws Exception
    {
		Utils objUtils = new Utils();
		String[] arrOfferData = strOfferingData.split("#");
		OfferingDataPO objOfferingData = new OfferingDataPO();
		//Click on Button NewOffering Data
		objOfferingData.fn_clickNewOfferData(driver, webWait).click();	
		Thread.sleep(5000);
		
        Boolean blnCustReferenceHandle = HandleLookUpWindow(driver,webWait,"Customer Reference",arrOfferData[0]);
        if (blnCustReferenceHandle==true)
        	Log.info("Data is entered successfully in Customer Reference Lookup (New Window)");
        else
        	Log.info("Unable to enter data in Customer Reference Lookup (New Window)");
        
        Boolean blnDeliveryLocationHandle = HandleLookUpWindow(driver,webWait,"Delivery Location",arrOfferData[1]);
        if (blnDeliveryLocationHandle==true)
        	Log.info("Data is entered successfully in Delivery Location Lookup (New Window)");
        else
        	Log.info("Unable to eneter data in Delivery Location Lookup (New Window)");
        
        //TODO:: Enter the Billing Date
        objOfferingData.fn_setBillingDate(driver, webWait).click(); 
               
        //TODO:: enter the Term
        objOfferingData.fn_setTerm(driver, webWait).clear();
        objOfferingData.fn_setTerm(driver, webWait).sendKeys("12");
               
        Boolean blnProductFamilyHandle = HandleLookUpWindow(driver,webWait,"Product Family",arrOfferData[2]);
        if (blnProductFamilyHandle==true)
        	Log.info("Data is entered successfully in Product Family Lookup (New Window)");
        else
        	Log.info("Unable to eneter data in Product Family Lookup (New Window)");
        
        //TODO:: Enter the Start Date
        objOfferingData.fn_setStartDate(driver, webWait).click();    
              
        String futureDate = objUtils.fn_getFutureDate(12);
        
        //TODO:: Enter the ExpirationDate
        objOfferingData.fn_setExpirationDate(driver, webWait).sendKeys(futureDate);
        
        //change the focus to Term
        objOfferingData.fn_setTerm(driver, webWait).click();
            
        //TODO:: Enter the AnnualPriceIncrease
        objOfferingData.fn_setAnnualPI(driver, webWait).sendKeys("3");
        
        Boolean blnPricelistHandle = HandleLookUpWindow(driver,webWait,"Price List",arrOfferData[3]);
        if (blnPricelistHandle==true)
        	Log.info("Data is entered successfully in Price List Lookup (New Window)");
        else
        	Log.info("Unable to enter data in Price List Lookup (New Window)");
        
        //TODO:: Select the Bill To Contact
        objOfferingData.fn_selBillingContact(driver, webWait).selectByIndex(1);
        
        //TODO:: Select the Notification Contact
        objOfferingData.fn_selNotificationContact(driver, webWait).selectByIndex(1);
        
        //TODO:: Select the Covered Location
        objOfferingData.fn_selCoveredLocation(driver, webWait).click();
        
        //click on Save button
        objOfferingData.fn_clickSaveBtn(driver, webWait).click();
        Thread.sleep(10000);
        
		return true;
		
    }
	
	public Boolean HandleLookUpWindow(WebDriver driver,WebDriverWait webWait,String strWindowName,String strData)
	{		
		OfferingDataPO objOffering = new OfferingDataPO();
		ConfigurationPageObjects objConfig = new ConfigurationPageObjects();
		String strFirstWindow = driver.getWindowHandle();

		//Click on LookupWindow
		try 
		{	    
		    if (strWindowName.contains("Offering"))
		    	objConfig.fn_getOfferWindow(driver, webWait).click();
		    else if(strWindowName.contains("NDA_Address")){
		    	objConfig.fn_getNDAAddressWindow(driver, webWait).click();
			    System.out.println(driver.getWindowHandles().size());
		        Set<String> windows = driver.getWindowHandles();
		        windows.remove(strFirstWindow);
		        Iterator<String> iter = windows.iterator();
			    driver.switchTo().window(iter.next());
			    driver.switchTo().window(iter.next());
			    System.out.println("the selected window is "+driver.getTitle());    
		    }
		    else
		    	objOffering.fn_clickLookUpWindow(driver,webWait,strWindowName).click();
			Thread.sleep(6000);
			WebElement rootTree = null;
			rootTree = driver.findElement(By.tagName("FRAMESET"));
			WebElement parentFrame = rootTree.findElement(By.name("searchFrame"));
			driver.switchTo().frame(parentFrame);
			objOffering.fn_clickWindowSearchBtn(driver, webWait).sendKeys(strData);
			Thread.sleep(3000);
			objOffering.fn_clickWindowGoBtn(driver, webWait).click();
			Thread.sleep(5000);//Click on the search item
			driver.switchTo().defaultContent();
			WebElement ResultsFrame = driver.findElement(By.name("resultsFrame"));
			driver.switchTo().frame(ResultsFrame);
			String strlen = objOffering.fn_clickWindowLinkText(driver, webWait).getText();
			if (strlen.length()>0)
				Log.info("Got the text from search window");
			else
				Log.info("Unable to Get the text from search window");		
			objOffering.fn_clickWindowLinkText(driver, webWait).click();
			driver.switchTo().window(strFirstWindow);
	
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return true;
	}
}