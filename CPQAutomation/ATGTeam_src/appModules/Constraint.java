package appModules;

/**
 * Created by joe.gannon on 5/4/2016.
 */
public class Constraint
{
    private String conName;
    private String prodCode;
    private String conType;
    private String typeRule;
    private String intent;
    private String disposition;
    private String relatedProd;
    private String assoc;
    private String message;
    public Constraint()
    {

    }

    public String getConName() {
        return conName;
    }

    public void setConName(String conName) {
        this.conName = conName;
    }

    public String getProdCode() {
        return prodCode;
    }

    public void setProdCode(String prodCode) {
        this.prodCode = prodCode;
    }

    public String getConType() {
        return conType;
    }

    public void setConType(String conType) {
        this.conType = conType;
    }

    public String getTypeRule() {
        return typeRule;
    }

    public void setTypeRule(String typeRule) {
        this.typeRule = typeRule;
    }

    public String getIntent() {
        return intent;
    }

    public void setIntent(String intent) {
        this.intent = intent;
    }

    public String getDisposition() {
        return disposition;
    }

    public void setDisposition(String disposition) {
        this.disposition = disposition;
    }

    public String getRelatedProd() {
        return relatedProd;
    }

    public void setRelatedProd(String relatedProd) {
        this.relatedProd = relatedProd;
    }

    public String getAssoc() {
        return assoc;
    }

    public void setAssoc(String assoc) {
        this.assoc = assoc;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void print()
    {
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~Constraint~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("\tConName: "+this.conName);
        System.out.println("\tProdCode: "+this.prodCode);
        System.out.println("\tType: "+this.conType);
        System.out.println("\tType Rule: "+this.typeRule);
        System.out.println("\tDisposition: "+this.disposition);
        System.out.println("\tRelatedProd: "+this.relatedProd);
        System.out.println("\tAssociation: "+this.assoc);
        System.out.println("\tMessage: "+this.message);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~Constraint~~~~~~~~~~~~~~~~~~~~~~~");

    }
}
