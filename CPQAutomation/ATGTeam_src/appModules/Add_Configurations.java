package appModules;

import java.io.File;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.BasePageObject;
import pageObjects.SalesForce.ConfigurationPageObjects;
import pageObjects.SalesForce.ProposalPageObjects;
import utility.EnvironmentDetailsSAS;
import utility.ExcelHandler;
import utility.Log;
import utility.Utils;

public class Add_Configurations {
	
	public boolean fn_AddConfigurations(WebDriver driver,WebDriverWait webWait,String strBaseServiceName,String strComponentName,String strTestCaseName) throws Exception
    {
			
		ConfigurationPageObjects objConfig = new ConfigurationPageObjects();
		EnvironmentDetailsSAS   objEnvDetails = new EnvironmentDetailsSAS();
		CreateOfferingData   objOfferingData    = new CreateOfferingData();
		
        try
        {   
        	      	
        	//Click on Configure button
        	objConfig.fn_clickConfigureBtn(driver, webWait).click();      	           
        	//click the Baseservice name
        	objConfig.fn_clickBaseServiceName(driver, webWait, strBaseServiceName).click();
        	//Click the ServiceName
        	objConfig.fn_clickServiceName(driver, webWait, strComponentName).click();   	
        	Thread.sleep(6000);     	
        	//Handle Search Lookup offering Data
            boolean blnofferingDataWindowhandle = objOfferingData.HandleLookUpWindow(driver,webWait,"Offering Data Lookup (New Window))","Apptus");
            if (blnofferingDataWindowhandle==true)
            	Log.info("Data is entered successfully in Offering Data Lookup (New Window)");
            else
            	Log.info("Unable to eneter data in Offering Data Lookup (New Window)");
            
        	//click on Next Link
        	objConfig.fn_clickNext(driver, webWait).click();
        	Thread.sleep(5000);
        	File fileConfigValidations = new File(objEnvDetails.dataSheetPath);	
  		    //TODO : Read Test Excel
 		    ArrayList<String> arrlistFileContents = ExcelHandler.readExcelFileAsArray(fileConfigValidations,"Vaulting_Config");		   
 		    String strOptionName=null;
 		    String strXpath = null;
		    int intRowtraverser=0;

		    //Utils.takeScreenshot(driver, strTestCaseName);
		    Log.info("++++++++++++++++++++++++++++++++++++Start of Configuration of Services+++++++++++++++++++++++++++++++++++++++++++++");
			//TODO: Now Traverse the ArrayList for Excel File Read to find the item from data table.
			for(int intCnt = 0 ; intCnt < arrlistFileContents.size(); intCnt +=2)
			{	
				intRowtraverser = intCnt;
				strOptionName = arrlistFileContents.get(intRowtraverser);
				strXpath = "//a[text()='"+strOptionName+"']/ancestor::tr[1]/td[1]//input";	
				try
				{					
                    //Identify the checkbox/radio button control
					WebElement webElement = driver.findElement(By.xpath(strXpath));					
					Thread.sleep(2000);					
					if (intCnt>1){
					  ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", webElement);
					 }					
					webElement.click();	// to click the checkbox
					Thread.sleep(10000);
					
					// to identify back the checked check-box/radio button webelement.
					WebElement webElement1 = driver.findElement(By.xpath(strXpath));					
					Boolean blnResult = webElement1.isSelected();
					if (blnResult==true)
						Log.info("Component of the Configuration is selected successfully::"+strOptionName); 	
					else
						Log.info("Component of the Configuration is not selected "+strOptionName); 
				}
			     catch (Exception e)
		        {
		            Log.error("Unable to click the element");
		            Log.error(e.getMessage());
		            e.printStackTrace();

		        }
			
			}
		    Log.info("++++++++++++++++++++++++++++++++++++End of Configuration of Services+++++++++++++++++++++++++++++++++++++++++++");
		    Thread.sleep(30000);   
			//Utils.takeScreenshot(driver, strTestCaseName);
        	//TODO:: click on Pricing Button
			objConfig.fn_clickPricingBtn(driver, webWait).click();
			Thread.sleep(10000); 
			//TODO :: click on toggle button
			objConfig.fn_clickShowPricing(driver, webWait).click();
			//Utils.takeScreenshot(driver, strTestCaseName);
        }
       catch (Exception e)
        {
            Log.error("ERROR: Unable to Add the Configuration");
            Log.error(e.getMessage());
            e.printStackTrace();

        }
		return true;
    }
        
 
    }

