package appModules;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.GenerateProposalDocPageObjects;
import utility.Log;
import utility.ReportUtils;
import utility.WordDocsHandler;

public class ProposalDoc_Creation_And_Validation {
	
	public boolean fn_ProposalDocCreationValidation(WebDriver driver,WebDriverWait webWait,String strDocName) throws Throwable
	{
		
		GenerateProposalDocPageObjects objProposalPO = new GenerateProposalDocPageObjects();
		ProposalDoc_Creation_And_Validation objDoccreation = new ProposalDoc_Creation_And_Validation();
		WordDocsHandler objWordhandler = new WordDocsHandler();
	  	String strParentWindow = driver.getWindowHandle();
    	String strFilename=null;
    	String strFileType=null;
  		
		//TO click on Generate Button on Proposal Page
		objProposalPO.fn_clickGenerate(driver, webWait).click();
		Thread.sleep(2000);
		//To select the doc type	
		objProposalPO.fn_selDocumentType(driver, webWait).click();
		//To click on Budgetary Proposal doc Generation
		objProposalPO.fn_selProposalDocument(driver, webWait,strDocName).click();
		Thread.sleep(2000);
		//To click on Generate Button on Proposal Generation Page
		objProposalPO.fn_clickGenerateBtn(driver, webWait).click();
		Thread.sleep(80000);
		//To Click on LInk Click to View the file
		objProposalPO.fn_clickLinkViewFile(driver, webWait).click();
		Thread.sleep(3000);	
		strFileType=".doc";  
		strFilename=objDoccreation.fn_downloadFile(driver, webWait, strDocName,strFileType);
		Thread.sleep(2000);
		if (strDocName.contains("Budget"))
		objWordhandler.fn_validateText(strFilename, "doc","BudgetProp_Validations");
		else
		objWordhandler.fn_validateText(strFilename, "doc","Proposal_Validations");	
		Thread.sleep(2000);
		//driver.close();
		Thread.sleep(2000);
	    driver.switchTo().window(strParentWindow);  
		
		//To click on Return Button to return to Proposal page
		//objProposalPO.fn_ClickReturnBtn(driver, webWait).click();
		
		return true;

	}

	public String fn_downloadFile(WebDriver driver,WebDriverWait webWait,String strDocName,String strFileType) throws InterruptedException 
	{
		   ReportUtils objReportutils= new ReportUtils();
		   
			//TODO: get the frame for the View & Print and verify the PO Number on it.
			//Switch to new window opened
			/*for(String winHandle : driver.getWindowHandles()){
			    driver.switchTo().window(winHandle);
			}*/
			Thread.sleep(1000);	
		   //Creating the File Name to download and rename with filename
			Date date = new Date() ;
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
			String strFilename = System.getProperty("user.dir")
						+ System.getProperty("file.separator")+"trunk"
						+ System.getProperty("file.separator")+"src"+System.getProperty("file.separator")+ "testData"
						+ System.getProperty("file.separator") +strDocName+dateFormat.format(date);
			Log.info("The filename generated is "+strFilename);
			strFilename = strFilename+strFileType;
		    Thread.sleep(2000);
		     
	        //TODO: Once the Print Frame is launched, Save the file and use PDF Stripper to Read it & Validate Data.
	      /* driver.switchTo().activeElement().sendKeys(Keys.TAB);      // traverse to the Print Control.
	        driver.switchTo().activeElement().sendKeys(Keys.TAB);      // traverse to the Pagination Control.
	        driver.switchTo().activeElement().sendKeys(Keys.TAB);      // traverse to the Rotate View Control.
	        driver.switchTo().activeElement().sendKeys(Keys.RETURN);        
	        Thread.sleep(2000);*/
	        
	
			//To Download the Proposal Doc
			objReportutils.fn_ReportsFileDownload(driver, strFilename);
			return strFilename;
	}
	
}
