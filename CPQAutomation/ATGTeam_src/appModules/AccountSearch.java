package appModules;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.AccountPageObjects;
import pageObjects.SalesForce.ProposalPageObjects;
import utility.Log;
import utility.Utils;

public class AccountSearch {
	
	public boolean fn_AccountSearch(WebDriver driver,WebDriverWait webWait,String strSearchItem,String strOpptyName,String strTestCaseName,String strCurrency) throws Exception
    {
			
  		AccountPageObjects objAcctpage = new AccountPageObjects(); 
  		ProposalPageObjects objProposal = new ProposalPageObjects();
		
        try
        {
        	
        	WebElement webElement = null;
        	String strTabletext=null;
            //Entering the Search item
        	objAcctpage.fn_setSearchField(driver, webWait).sendKeys(strSearchItem);       	
        	//Clicking on the search button
        	objAcctpage.fn_clickSearchBtn(driver, webWait).click();
        	Thread.sleep(3000);
        	//clicking on the search item link(oppty link)
        	if (driver.findElement(By.partialLinkText(strSearchItem)).isDisplayed()){
         		driver.findElement(By.partialLinkText(strSearchItem)).click();
        		Log.info("Opportunity link is clicked Successfully"+strSearchItem);}       	
        	else{          
        		Log.info("Unable to click the Opportunity link "+strSearchItem);}
        	  	
        	//to find the Customer Reference Object
        	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", objAcctpage.fn_getCustomerReferenceTable(driver, webWait));
			Thread.sleep(1000);
			strTabletext = objAcctpage.fn_getCustomerReferenceTable(driver, webWait).getText();
			//Validating whether the customer Reference table has Records or not
        	if (strTabletext.contains("No records to display")){
        		Log.info("Customer Reference is not linked to Account so creating a new one !!");
        		//To click on NewCustomereReference Button
        		objAcctpage.fn_clickNewCustReferenceBtn(driver, webWait).click();
        		//To enter the Tier name While creating Customer Reference
	        		try {
	        			
	            		objAcctpage.fn_enterCsutomerReferenceName(driver, webWait, 0).sendKeys("ApptusCustRefAutomation");            		
	        		}
	        		catch(Exception excpEnvironment){
	        			
	        			objAcctpage.fn_enterCsutomerReferenceName(driver, webWait, 1).sendKeys("ApptusCustRefAutomation");
	        		}
        		//To Select the Currency
        		objAcctpage.fn_selCurrency(driver, webWait).selectByVisibleText(strCurrency);
        		//To click on Save Button
        		objAcctpage.fn_clickSaveCustRefPage(driver, webWait).click();
                //Entering the Search item
            	objAcctpage.fn_setSearchField(driver, webWait).sendKeys(strSearchItem);       	
            	//Clicking on the search button
            	objAcctpage.fn_clickSearchBtn(driver, webWait).click();
            	Thread.sleep(3000);           	
            	//clicking on the search item link(oppty link)
            	if (driver.findElement(By.partialLinkText(strSearchItem)).isDisplayed()){
             		driver.findElement(By.partialLinkText(strSearchItem)).click();
            		Log.info("Opportunity link is clicked Successfully"+strSearchItem);}       	
            	else{          
            		Log.info("Unable to click the Opportunity link "+strSearchItem);}}

        	else{
        		Log.info("Customer Reference is already linked to Account "+strTabletext);
        	    }
 
            //click on the Opportunity displayed on oppty
  			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.partialLinkText(strOpptyName)));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", webElement);
			Thread.sleep(1000);	
			webElement.click();
			Thread.sleep(3000);	
        	if ((objProposal.fn_clickProposalButton(driver, webWait)).isDisplayed())
        	Log.info("OpportunityName link is clicked Successfully"+strOpptyName);
        	else
           	Log.info("Unable to click on the OpportunityName link "+strOpptyName);
        	Utils.takeScreenshot(driver, strTestCaseName);
        }
        catch (Exception e)
        {
            Log.error("Unable to Perform Account Search");
            Log.error(e.getMessage());

        }
		return true;
    }

}
