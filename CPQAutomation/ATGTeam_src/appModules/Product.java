package appModules;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by joe.gannon on 3/2/2016.
 */
public class Product
{
    private String productPath;
    private String productPricePath;
    private String productName;
    private String productCode;
    private String l2Name;
    private HashMap<String,ArrayList<HashMap<String,String>>> productData;
    private HashMap<String,ArrayList<HashMap<String,String>>> pricingData;
    private HashMap<String,Option> productPriceMap;
    private ArrayList<Constraint> constraints;
    private ArrayList<TierPrice> tierPrices;
    private ArrayList<Option> options;
    private ArrayList<Attribute> attributes;
    public Product(String name)
    {
        constraints = new ArrayList<Constraint>();
        productName = name;
        productPriceMap = new HashMap<String,Option>();
        options = new ArrayList<>();
        attributes = new ArrayList<>();
    }

    public void setProductPath(String newPath)
    {
        this.productPath = newPath;
    }

    public void setProductPricePath(String newPath)
    {
        this.productPricePath = newPath;
    }

    public String getProductPath() {
        return productPath;
    }

    public String getProductPricePath() {
        return productPricePath;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public HashMap<String, ArrayList<HashMap<String, String>>> getProductData() {
        return productData;
    }

    public void setProductData(HashMap<String, ArrayList<HashMap<String, String>>> productData) {
        this.productData = productData;
        this.setConstraintRules();
    }
    public String getBaseProdName()
    {
        return this.productName.split("\\s+")[0];
    }

    public HashMap<String, ArrayList<HashMap<String, String>>> getPricingData() {
        return pricingData;
    }

    public HashMap<String, Option> getProductPriceMap() {
        return productPriceMap;
    }

    public void setPricingData(HashMap<String, ArrayList<HashMap<String, String>>> pricingData) {
        this.pricingData = pricingData;
        System.out.println("Loading Options for Product: "+this.getProductName());
        ArrayList<HashMap<String,String>> priceList = this.getPriceSheetData("Step 3 Setup Base Prices");
        HashMap<String,String> qtyMap = this.getItemMap("Step 3 Setup Bundles","Option","Max qty");
        for(HashMap<String,String> prodPrice : priceList)
        {
            Option opt = new Option();
            //if product exists update the monthly fee or recurring fee
            if(this.productPriceMap.containsKey(prodPrice.get("Product Name")))
            {
                Option currProd = this.productPriceMap.get(prodPrice.get(("Product Name")));
//                System.out.println("Product already exists: "+prodPrice.get("Product Name"));

                //determine fee that already exists for product
                if(prodPrice.get(("Charge Type")).contains("Monthly") && prodPrice.get("List Price") != "NA")
                {
                    //Monthly fee Charge Type Found
//                    System.out.println()
                    opt.setOptionName(currProd.getOptionName());
                    opt.setMonthlyFee(prodPrice.get("List Price"));
                    opt.setOneTimeFee(currProd.getOneTimeFee());
                }
                else
                {
                    //One Time fee Charge Type Found
                    opt.setOptionName(currProd.getOptionName());
                    opt.setOneTimeFee(prodPrice.get("List Price"));
                    opt.setMonthlyFee(currProd.getMonthlyFee());
                }

            }
            else
            {
//                System.out.println("Product does not exist: "+prodPrice.get("Product Name"));
                opt.setOptionName(prodPrice.get("Product Name"));

                //Determine Fee current fee type
                if(prodPrice.get(("Charge Type")).contains("Monthly"))
                {
                    opt.setMonthlyFee(prodPrice.get("List Price"));
                }
                else
                {
                    opt.setOneTimeFee(prodPrice.get("List Price"));
                }
            }
            opt.setMaxQty(qtyMap.get(opt.getOptionName()));
           System.out.println("Adding option: "+opt.getOptionName()+" || Monthly: "+opt.getMonthlyFee()+ " || One-Time: "+opt.getOneTimeFee()+ " || "+opt.getMaxQty());
            this.options.add(opt);
            this.productPriceMap.put(opt.getOptionName(),opt);
//            this.productPriceMap.put(prodPrice.get("Product Name"),prodPrice.get("List Price"));
        }

        //remove options with pricing set to NA
        List<Option> dupeList = new ArrayList<Option>(this.getOptions());
        System.out.println("Initial Size: "+this.options.size());
        for(Option opt : dupeList)
        {
            if(opt.getMonthlyFee() != null)
            {
                if(opt.getMonthlyFee().equals("NA"))
                {
                    this.options.remove(opt);
                }
            }else {

                if (opt.getOneTimeFee() != null) {
                    if (opt.getOneTimeFee().equals("NA")) {
                        this.options.remove(opt);
                    }
                }
            }
        }
        System.out.println("Post Size: "+this.options.size());
        this.loadTierPrices();

    }

    public ArrayList<HashMap<String,String>> getSheetData(String sheetName)
    {
        return this.productData.get(sheetName);
    }

    public ArrayList<HashMap<String,String>> getPriceSheetData(String sheetName)
    {
        return this.pricingData.get(sheetName);
    }

    public void printProduct()
    {
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
        System.out.println("Name: "+this.productName);
        System.out.println("Prod Path: "+this.productPath);
        System.out.println("Price Path: "+this.productPricePath);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    }
    public ArrayList<HashMap<String,String>> getOptionGroup(String optionGroup)
    {
        ArrayList<HashMap<String,String>> optionGroupList = new ArrayList<HashMap<String,String>>();
        for(HashMap<String,String> map:  this.getProductData().get("Step 3 Setup Bundles"))
        {
            if(map.get("Option Group").equals(optionGroup))
            {
                optionGroupList.add(map);
            }
        }
        return optionGroupList;
    }
    public void printPricingData()
    {
        for(String sheetKey : pricingData.keySet())
        {
            ArrayList<HashMap<String,String>> sheet = pricingData.get(sheetKey);
            System.out.println("SHEET: "+sheetKey);
            for(HashMap<String,String> row: sheet)
            {
                for(String rowKey: row.keySet())
                {
                    System.out.println("\tKey: "+rowKey+" || Value: "+row.get(rowKey));
                }
                System.out.println("");
            }
        }
    }

    public String getProductCode()
    {
        return this.productData.get("Step 1 Setup Products").get(0).get("Product Code");
    }

    public HashMap<String,String> getItemMap(String sheetKey, String key, String valueKey)
    {
        HashMap<String,String> returnMap = new HashMap<String,String>();
        try
        {
            for(HashMap<String,String> map : this.productData.get(sheetKey))
            {
                returnMap.put(map.get(key),map.get(valueKey));
            }
            return returnMap;
        }
        catch (Exception e)
        {
            System.out.println("Sheet: "+sheetKey+ " || Key: "+key+" || ValueKey: "+valueKey+" Not Found!");
            return returnMap;
        }

    }

    public void setConstraintRules()
    {

        //for each row in the product workbook/ constraint add in a new constraint rule
        ArrayList<HashMap<String,String>> constraintSheet = productData.get("Step 4 Constraint Rules");
       try{
            System.out.println("Number of Constraints: " + constraintSheet.size());
            for (HashMap<String, String> map : constraintSheet) {
                Constraint con = new Constraint();
                con.setConName(map.get("Constraint Name"));
                con.setProdCode(map.get("Product Code/Grouping/Family/Category"));
                con.setConType(map.get("Type"));
                con.setTypeRule(map.get("Type Rules"));
                con.setIntent(map.get("Intent"));
                con.setDisposition(map.get("Disposition"));
                con.setRelatedProd(map.get("Related Product Code/Grouping/Family/Category"));
                con.setAssoc(map.get("Association"));
                con.setMessage(map.get("Message"));
                this.constraints.add(con);

            }

            for (Constraint con : this.constraints) {
                con.print();
            }
        }
       catch(NullPointerException e)
       {
           System.out.println("NO CONSTRAINT RULES FOUND");
       }


    }
    public void loadTierPrices()
    {
        ArrayList<TierPrice> tierPriceHashMap = new ArrayList<>();
        if(!(this.pricingData.get("Step 4 Setup Tier Prices")== null))
        {
            ArrayList<HashMap<String,String>> tierMap = this.pricingData.get("Step 4 Setup Tier Prices");
            //TODO: For each row in the Tier Prices Spreadsheet
            for(HashMap<String,String> map: tierMap)
            {
                //TODO: For each 'cell' in the Tier Prices Spreadsheet
                TierPrice tp = new TierPrice();
                tp.setOptionName(map.get("Product Name"));
                tp.setTierSelection(map.get("Dimension 1 Type"));
                tp.setTierType(map.get("Dimension 1"));
                tp.setTierValue(map.get("Dimension 1 Value"));
                tp.setTierPrice(map.get("Adjustment Amount"));
                tierPriceHashMap.add(tp);

            }
        }
        this.setTierPrices(tierPriceHashMap);
        for(TierPrice tp: tierPriceHashMap)
        {
            tp.print();
        }
    }
    public ArrayList<Constraint> getConstraintRules()
    {
        return this.constraints;
    }
    public ArrayList<TierPrice> getTierPrices() {
        return tierPrices;
    }

    public void setTierPrices(ArrayList<TierPrice> tierPrices) {
        this.tierPrices = tierPrices;
    }

    public void addOptionToProduct(Option opt)
    {
        this.productPriceMap.put(opt.getOptionName(),opt);
        this.options.add(opt);
    }

    public ArrayList<Option> getOptions()
    {
        return this.options;
    }
    public String getL2Name()
    {
        return this.l2Name;

    }
    public void setL2Name()
    {
        if (getProductData().get("Step 2 Setup Categories").get(0).get("Level 2 Name").equals(null))
        {
//            System.out.println("No L2 Name Found");
            this.l2Name = "NO_L2_NAME";

        }
        else
        {
//            System.out.println("L2 Name Found: "+getProductData().get("Step 2 Setup Categories").get(0).get("Level 2 Name"));
            this.l2Name = getProductData().get("Step 2 Setup Categories").get(0).get("Level 2 Name").trim();
        }
    }

    public ArrayList<Attribute> getAttributes() {
        return attributes;
    }

    public void setAttributes() {

        ArrayList<HashMap<String,String>> attributesList = this.getProductData().get("Step 1b Attributes");
        System.out.println("Adding "+attributesList.size()+" to Product");
        for(HashMap<String,String> map: attributesList)
        {
            Attribute attrib = new Attribute();
            attrib.setAttributeProductName(map.get("Product Name"));
            attrib.setAttributeValue(map.get("Value"));
            attrib.setAttributeName(map.get("Attribute Name"));
            attrib.setAttributeFieldType(map.get("Field Type"));
            this.attributes.add(attrib);
        }

    }
}
