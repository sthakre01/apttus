package appModules;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.ConfigurationPageObjects;
import pageObjects.SalesForce.PricingPageObjects;
import utility.EnvironmentDetailsSAS;
import utility.ExcelHandler;
import utility.Log;


public class PricingValidation {
	
	
	public boolean fn_PricingValidation(WebDriver driver,WebDriverWait webWait) throws Exception
	{
	
		EnvironmentDetailsSAS   objEnvDetails = new EnvironmentDetailsSAS();
		ConfigurationPageObjects objConfig = new ConfigurationPageObjects();
		PricingPageObjects       objPricing = new PricingPageObjects(); 
		try
		{
			
		WebElement webElement = null;
		 	 
	 	File fileConfigValidations = new File(objEnvDetails.dataSheetPath);	
	 	
		//TODO : Read Test Excel
		ArrayList<String> arrlistFileContents = ExcelHandler.readExcelFileAsArray(fileConfigValidations,"Vault_Pricing");	
	
		//initializing internal Loop variables
		int introwtraverser=0;
		int row=1;
		String strServiceName=null;
		String strChargeType=null;
		String strPrice=null;
		String strXpath1=null;
		String strFinalXpath=null;
		strXpath1 = "//table[@class='aptLineItemOptionsTable']/tbody/tr[";
		
		//TODO: Now Traverse the ArrayList for Excel File Read to find the item from data table.
		for(int intCnt = 0 ; intCnt < arrlistFileContents.size();intCnt+=3)
			{		
				
			introwtraverser = intCnt;	
				
		    //Fetching the Rates table to read the pricing components
			WebElement Webtable = objPricing.fn_getRatestbl(driver, webWait);
			
			//Fetch the rows of the table
			List<WebElement> TotalRowCount1 = Webtable.findElements(By.tagName("tr"));
			
			int rowsize=TotalRowCount1.size()-1;
		
			L1: for(WebElement rows : TotalRowCount1)
				{
					
					 strFinalXpath = strXpath1+row+"]";
					 
					//Fetching the service Name from excel
					 webElement  = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(strFinalXpath+"/td[2]")));
					 strServiceName = webElement.getText();
					 strServiceName = strServiceName.substring(0,strServiceName.length()-5).trim().toString();
					 
					//Fetching the Charge Type from application
					 webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(strFinalXpath+"/td[5]")));
					 strChargeType = webElement.getText().trim().toString();
					
					//checking the condition of matching the Service Name and its strChargeType
					if (strServiceName.contains(arrlistFileContents.get(introwtraverser))  && (strChargeType.contains(arrlistFileContents.get(introwtraverser+1))))
					{	
						
						Log.info("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
						Log.info("Start of Validations for the Service ::  "+strServiceName+"strChargeType :: "+strChargeType); 
						
							//Fetching the strprice Value from application
							 webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(strFinalXpath+"/td[6]"))); 
							 strPrice = webElement.getText();	
							 String[] arrprice =  strPrice.split(Pattern.quote("("));
							 
							   //TODO :: Verify the Service Name displayed on the Pricing Page
								if (arrlistFileContents.get(introwtraverser).trim().contains(strServiceName))
								{
									Log.info("Match Found for Service Name : "+strServiceName);
									Log.info("Application Value for Service Name : "+strServiceName);
									Log.info("Excel Data Value for Service Name : "+arrlistFileContents.get(introwtraverser));
									Log.info("-----------------------------------------------------------------------------------");
								}
								else
								{
									Log.info("Match Not Found for Service Name : "+strServiceName);
									Log.info("Application Value for Service Name : "+strServiceName);
									Log.info("Excel Data Value for Service Name : "+arrlistFileContents.get(introwtraverser));
									Log.info("-----------------------------------------------------------------------------------");
								}
								
							    //TODO :: Verify the Service Name displayed on the strChargeType
								if (arrlistFileContents.get(introwtraverser+1).trim().contains(strChargeType))
								{
									Log.info("Match Found for ChargeType : "+strChargeType);
									Log.info("Application Value for ChargeType : "+strChargeType);
									Log.info("Excel Data Value for ChargeType : "+arrlistFileContents.get(introwtraverser+1));
									Log.info("-----------------------------------------------------------------------------------");
								}
								else
								{
									Log.info("Match Not Found for ChargeType : "+strChargeType);
									Log.info("Application Value for ChargeType : "+strChargeType);
									Log.info("Excel Data Value for ChargeType : "+arrlistFileContents.get(introwtraverser+1));
									Log.info("-----------------------------------------------------------------------------------");
								}
								
							    //TODO :: Verify the Service Name displayed on the strprice
								if (arrlistFileContents.get(introwtraverser+2).trim().contains(arrprice[0].toString().trim()))
								{
									Log.info("Match Found for price : "+arrprice[0]);
									Log.info("Application Value for price : "+arrprice[0]);
									Log.info("Excel Data Value for price : "+arrlistFileContents.get(introwtraverser+2));
									Log.info("-----------------------------------------------------------------------------------");
								}
								else
								{
									Log.info("Match Not Found for price : "+arrprice[0]);
									Log.info("Application Value for price : "+arrprice[0]);
									Log.info("Excel Data Value for price : "+arrlistFileContents.get(introwtraverser+2));
									Log.info("-----------------------------------------------------------------------------------");
								}
								
								Log.info("End of Validations for the Service ::  "+strServiceName+"strChargeType :: "+strChargeType); 
								Log.info("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
			             
						break L1;		
				
				           }
					if (row<rowsize)
					row+=1;	
					else if (row==rowsize)
					row=1;	
						
					   }	
				
			     }	
		            //To click on UpdateLineItems&Exit Button after pricing Validation
		            objConfig.fn_clickSave(driver, webWait).click();
	       }
		
	    catch (Exception e)
	    {
	        Log.error("ERROR: Unable to complete the Pricing Validations");
	        Log.error(e.getMessage());
	
	    }
		return true;
		
	}
	
}
