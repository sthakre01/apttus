package appModules;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.LoginPageObjects;
import pageObjects.SalesForce.ProposalPageObjects;
import utility.Log;
import utility.Utils;

public class CreateProposal {
	
	public boolean fn_CreateProposal(WebDriver driver,WebDriverWait webWait,String strProposalData) throws Exception
    {
			
    	ProposalPageObjects objProposal = new ProposalPageObjects();
		String strProposaldata[]=strProposalData.split("#");
    	
        try
        {   
        	objProposal.fn_clickProposalButton(driver, webWait).click();
        	//clear the Proposal Name Field
        	objProposal.fn_setProposalName(driver, webWait).clear();
        	//Enter the User given ProposalName
        	objProposal.fn_setProposalName(driver, webWait).sendKeys(strProposaldata[0]);
        	//clear the PrimaryContact
        	objProposal.fn_setPrimaryContact(driver, webWait).clear();
        	//Enter the User given PrimaryContactlName
        	objProposal.fn_setPrimaryContact(driver, webWait).sendKeys(strProposaldata[1]);
        	//clear the PriceList
        	//Objproposal.fn_EnterPriceList(driver, webWait).clear();
        	//Enter the User given PriceListName
        	//Objproposal.fn_EnterPriceList(driver, webWait).sendKeys(PriceList);
        	//Click on Save button
        	objProposal.fn_clickSaveButton(driver, webWait).click();
        	//Utils.takeScreenshot(driver, strTestCaseName);
        	
        }
        catch (Exception e)
        {
            Log.error("ERROR: Unable to Create the Proposal");
            Log.error(e.getMessage());

        }
		return true;
    }

}
