package appModules;

import java.util.Set;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObjects.Apttus.ApproveAndGenerateGMSAPageObjects;


public class ApproveAndGenerateGMSA {
	

	public boolean fn_ApproveCreateGMSA(WebDriver driver,WebDriverWait webWait) throws InterruptedException
    {   

		String strParentWindow=null;
		String strchildWindow=null;
		ApproveAndGenerateGMSAPageObjects ObjctApprve = new ApproveAndGenerateGMSAPageObjects();
		//Select the approval status as Accepted

		Actions action = new Actions(driver);
		action.moveToElement(ObjctApprve.fn_ClickDraft(driver, webWait)).doubleClick().perform();
		Thread.sleep(2000);
		ObjctApprve.fn_SelectApproval(driver, webWait).selectByVisibleText("Accepted");
		//Click on save button
		ObjctApprve.fn_ClickSave(driver, webWait).click();
		Thread.sleep(1000);
		strParentWindow = driver.getWindowHandle();
		//Click on the Button CreteAgreementGMSA
		ObjctApprve.fn_CreateGMSA(driver, webWait).click();
		Thread.sleep(3000);
		//TODO: get the frame for the Child window
		Set<String> strWindowHandles = driver.getWindowHandles();   		
		strWindowHandles.remove(strParentWindow);
		strchildWindow = strWindowHandles.iterator().next();		
		driver.switchTo().window(strchildWindow);
		Thread.sleep(1000);
		//Select the Type as GMSA
		ObjctApprve.fn_SelectGMSA(driver, webWait).selectByVisibleText("GMSA");
		//Click on Continue Button
		ObjctApprve.fn_ClickContinueBtn(driver, webWait).click();
		return false;
		
    }		
}
