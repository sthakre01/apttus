package appModules;

import java.io.IOException;
import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.ApproveAndGenerateGMSAPageObjects;
import pageObjects.SalesForce.ApproveAndGenerateOrderFormObjects;
import utility.Log;
import utility.WordDocsHandler;

public class ValidateOrderFormDoc {
	

	public Boolean fn_ValidateOrderFormDoc(WebDriver driver,WebDriverWait webWait,String strDocName,String strAddress) throws IOException, Throwable
    {   

		String strParentWindow=null;
		String strchildWindow=null;
		ApproveAndGenerateGMSAPageObjects objctApprve = new ApproveAndGenerateGMSAPageObjects();
		ApproveAndGenerateOrderFormObjects objctOrderForm = new ApproveAndGenerateOrderFormObjects();
		WordDocsHandler objWordhandler = new WordDocsHandler();
		String strFilename=null;
		
		try{
              
			//To Click on the Send For Review Btn on Agremment Page
			objctOrderForm.fn_clickSendForReviewBtn(driver, webWait).click();
			Thread.sleep(5000);
			//To select the Generated GMSA
			objctOrderForm.fn_clickAttachGMSA(driver, webWait).click();
			//To click on the Next Button
			objctOrderForm.fn_clickNextBtn(driver, webWait).click();
			//To select the Order Form To generate
			objctOrderForm.fn_clickNewOrderFormBtn(driver, webWait).click();
			
		}
		catch (Exception e) {
			// TODO: handle exception
			Log.info("ERROR: problem while Validating the FunctionalDocs");
			e.printStackTrace();
		}	
		return true;
    }		
}
