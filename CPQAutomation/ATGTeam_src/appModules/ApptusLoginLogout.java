package appModules;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.*;
import utility.EnvironmentDetailsSAS;
import utility.Log;

public class ApptusLoginLogout {
	
	public static void appLogin(WebDriver driver,WebDriverWait webWait,String username, String password) throws Exception
    {
		LoginPageObjects LogObjts = new LoginPageObjects();
		
        try
        {
        	LogObjts.fn_EnterUserName(driver, webWait).clear();
        	LogObjts.fn_EnterUserName(driver, webWait).sendKeys(username);
        	LogObjts.fn_EnterPassword(driver, webWait).clear();
        	LogObjts.fn_EnterPassword(driver, webWait).sendKeys(password);
        	LogObjts.fn_ClickLoginButton(driver, webWait).click();
            //utility.Utils.takeScreenshot(driver, "ariaLogin_BeforeAssert");
        }
        catch (Exception e)
        {
            Log.error("Unable to login to APPTUS");
            Log.error(e.getMessage());
         }
    }

    /*public static void appLogout(WebDriver driver) {

        try
        {
            Log.info("Logging out");
            driver.findElement(AriaDashboardPageObject.logout()).click();
            Log.info("User is successfully logged out of APPTUS");
        }
        catch (Exception e)
        {
            Log.error("Unable to logout from application");
            Log.error(e.getMessage());
        }*/
    }


