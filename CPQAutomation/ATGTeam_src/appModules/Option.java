package appModules;

/**
 * Created by joe.gannon on 3/18/2016.
 */
public class Option
{
    private String optionName;
    private String monthlyFee;
    private String oneTimeFee;
    private String maxQty;
    public Option()
    {

    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public String getMonthlyFee() {
        return monthlyFee;
    }

    public void setMonthlyFee(String monthlyFee) {
        this.monthlyFee = monthlyFee;
    }

    public String getOneTimeFee() {
        return oneTimeFee;
    }

    public void setOneTimeFee(String oneTimeFee) {
        this.oneTimeFee = oneTimeFee;
    }

    public String getMaxQty() {
        return this.maxQty;
    }

    public void setMaxQty(String maxQty) {
        if(maxQty == null)
        {
            this.maxQty = "NO_MAX_QTY";
        }
        else
        {
            this.maxQty = maxQty;
        }

    }

    public void print()
    {
        System.out.println("Option Name: "+this.optionName);
        System.out.println("Option Monthly: "+this.monthlyFee);
        System.out.println("Option One-Time: "+this.oneTimeFee);
    }
}
