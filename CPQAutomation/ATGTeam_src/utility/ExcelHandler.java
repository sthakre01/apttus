package utility;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.Reporter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;

/**
 * Created by joe.gannon on 3/3/2016.
 */
public class ExcelHandler
{
    public HashMap<String,ArrayList<HashMap<String,String>>> readDataSheet(String filePath, String sheetName)
    {
        HashMap<String,ArrayList<HashMap<String,String>>> entireMapList = new HashMap<String,ArrayList<HashMap<String,String>>>();
        try
        {
            File excelFile = new File(filePath);
            FileInputStream fis = new FileInputStream(excelFile);
            HSSFWorkbook workbook = new HSSFWorkbook(fis);
            System.out.println("~~~~~~~~~~~~~~~"+excelFile.getName()+"~~~~~~~~~~~~~~~");
            for(int i=0;i < workbook.getNumberOfSheets(); i++)
            {
                ArrayList<HashMap<String,String>> sheetMap = new ArrayList<HashMap<String,String>>();
                String sheetTitle = workbook.getSheetName(i);
//                System.out.println("\tSheet Name: "+workbook.getSheetName(i));
                HSSFSheet sheet = workbook.getSheetAt(i);
                Iterator<Row> rowIterator = sheet.iterator();
                rowIterator.next(); // skip first row
                Row headerRow = rowIterator.next();
                int numHeaders = headerRow.getPhysicalNumberOfCells();
//                System.out.println("numHeaders: "+numHeaders);
                while(rowIterator.hasNext())
                {
                    Row currentRow = rowIterator.next();
                    if(currentRow.getCell(0) != null)
                    {
                        HashMap<String,String> rowMap = new HashMap<String,String>();
                        for(int x = 0; x < numHeaders; x++)
                        {

                            String header = headerRow.getCell(x,currentRow.CREATE_NULL_AS_BLANK).toString();
                            String value = currentRow.getCell(x, currentRow.CREATE_NULL_AS_BLANK).toString();
                            rowMap.put(header,value);
//                            System.out.println("\t|--> Header: "+headerRow.getCell(x)+ " || Value: "+currentRow.getCell(x));
                        }
                        sheetMap.add(rowMap);
//                        System.out.println("\t~~~~~~~~~~~~~~~~~~~~~~~End Row~~~~~~~~~~~~~~~~~~~~~~~~~");
                    }
                    entireMapList.put(sheetTitle,sheetMap);

                }

//                System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
                //map<Sheet, List<map<header,data>>>
            }
//
//            System.out.println("NumSheets: "+entireMapList.size());



        }catch(Exception e)
        {
            System.out.println("File not Found! "+filePath);
            e.printStackTrace();
        }
        return entireMapList;
    }
    
    public static ArrayList<String> readExcelFileAsArray(File file, String strWorksheet) throws FileNotFoundException
    {
        Log.info("Reading file: "+file.getAbsolutePath()+" and worksheet: "+strWorksheet.toString());
        try
        {
            FileInputStream fis = new FileInputStream(file);
            XSSFWorkbook workbook = new XSSFWorkbook(fis);
            //XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = workbook.getSheet(strWorksheet);
            //XSSFSheet sheet = workbook.getSheet(strWorksheet);
            Iterator<Row> rowIterator = sheet.iterator();

            //TODO: Create headers and cellValues objects to read the file contents.
            List<String> headers = new ArrayList<String>();
            ArrayList<String> cellValues = new ArrayList<String>();

            //set headers flag
            boolean areHeadersLoaded = false;

            //Iterate through excel and get cells
            while(rowIterator.hasNext())
            {
            	//TODO: Define another objects for row &cell iterators to save read data.
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();

                //TODO: loading the headers first.
                if(!areHeadersLoaded)
                {
                    while(cellIterator.hasNext())
                    {
                        Cell cell = cellIterator.next();
                        headers.add(cell.getStringCellValue());
                    }
                    areHeadersLoaded = true;
                }
                else
                {
                    //TODO: Iterating through rows to retrieve cell data.
                    while(cellIterator.hasNext())
                    {
                        Cell cell = cellIterator.next();
                        String strCellValue;
                        switch(cell.getCellType())
                        {
                            case Cell.CELL_TYPE_STRING:
                                strCellValue = cell.getStringCellValue();
                                cellValues.add(strCellValue);
                                break;
                            case Cell.CELL_TYPE_NUMERIC:
                                strCellValue = Integer.toString((int) cell.getNumericCellValue());
                                cellValues.add(strCellValue);
                                break;
                            case Cell.CELL_TYPE_BOOLEAN:
                                strCellValue = Boolean.toString(cell.getBooleanCellValue());
                                cellValues.add(strCellValue);
                                break;
                            default:
                        }
                    }
                }
            }
            Log.info("File has been read successfully !");
            return cellValues;
        }
        catch (Exception exception)
        {
            Log.error("ERROR: Error reading excel file.");
            Reporter.log("ERROR: Error reading excel file");
            exception.printStackTrace();
            return null;
        }
    }
    
}
