/* 
==============================================================
 Author     		:	Umesh Kute
 Class Name		 	: 	EnvironmentDetails
 Purpose     		: 	Declaring the environment variables that will be used for WordPress application.
 Date       		:	02/06/2015
 Version Information:	Version1.0
 PreCondition 		:	None
 Test Steps 		:	1. Enable the appropriate values for application URL and browser type for application
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
 #======================================================================
 */

package utility;

import java.io.File;
import java.util.Date;
import java.text.SimpleDateFormat;

//import org.apache.log4j.xml.DOMConfigurator;
//import org.openqa.selenium.WebDriver;


/*
This class contains the declaration for all environment variables used
@version: 1.0
@author: Umesh Kute
*/
public class EnvironmentDetails

{
	// ****** Environments details ******
	// Provide your Web application URL
	public static final String URL = "https://test.salesforce.com/";
	public static String username = "joe.gannon@sungardas.com.apttus";
	public static String password = "ATGmsc12345";
//	public static final String URL = "https://sungardas--ApttusDev.cs4.my.salesforce.com";
//	public static final String URL = "https://sungardas--apttus.cs4.my.salesforce.com/";
//	public static final String URL = "https://login.salesforce.com";

	// This parameter is to configure detaultbrowser for application
	// If no value is specified in variable browser, then default browser will be used. 
//	public static final String defaultbrowser = "firefox";
	
//	public static final String browser  = "firefox";
	public static final String browser = "Chrome";
	//public static final String browser = "IE";
	// public static final String browser = "Headless";
	// public static final String browser = "Grid";


	// Drivers details
	public static final String Path_ChromDriver = new File("Brwser_Driverssrc").getAbsolutePath() + "\\chromedriver.exe";
	public static final String Path_IeDriver = new File("Brwser_Driverssrc").getAbsolutePath() + "\\IEDriverServer.exe";
	public static final String Path_FirefoxDriver = "";
	public static final String WORKSHEETS_PATH = System.getProperty("user.dir")+"/src/testData/Products_Pricing/";

	// This parameter refers to the path where screenshot of page,
	// during the test case execution is taken.  
	public static SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy-hhmm");//("ddMMyyyy-hhmmss")
	public static Date curDate = new Date();
	public static String strDate = sdf.format(curDate);
	public static String Path_ScreenShot = new File("Screenshots")
			.getAbsolutePath() + "/Screenshots"+ "_" +strDate + "/";
	public static String CHROME_DRIVER_PATH = System.getProperty("user.dir")
			+ System.getProperty("file.separator") + "Brwser_Driverssrc" +System.getProperty("file.separator") + "chromedriver.exe";
	public static String FIREFOX_DRIVER_PATH = System.getProperty("user.dir")
			+ System.getProperty("file.separator") + "Brwser_Driverssrc" +System.getProperty("file.separator") + "chromedriver.exe";

	}