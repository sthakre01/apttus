/* 
==============================================================
 Author     		:	Umesh Kute
 Class Name		 	: 	Utils
 Purpose     		: 	Utility functions to use across the application [Reusable functions]
 						1. Take screenshot during test script execution
 						2. Read data from excel file
 Date       		:	02/06/2015
 Version Information:	Version1.0
 PreCondition 		:	None
 Test Steps 		:	None
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
 #======================================================================
 */

package utility;

import utility.EnvironmentDetailsSAS;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFTable;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Options;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.os.WindowsUtils;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.Reporter;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

//import com.sun.javafx.collections.MappingChange.Map;








import org.openqa.selenium.firefox.internal.ProfilesIni;

import atu.ddf.file.ExcelFile;
import atu.ddf.selenium.SeleniumTestNGHelper;

/*
This class contains the reusable function which can be used for 
taking screenshots during test script execution and reading data from excel file
@version: 1.0
@author: Umesh Kute
*/
public class Utils {
		public static WebDriver driver = null;

		
	 public static void takeScreenshot(WebDriver driver, String sTestCaseName) throws Exception{
			try{
				Log.info(EnvironmentDetailsSAS.Path_ScreenShot);
				File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
				FileUtils.copyFile(scrFile, new File(EnvironmentDetailsSAS.Path_ScreenShot + sTestCaseName +".jpg"));
				//Reporter.setEscapeHtml(true);
			
				Reporter.log("<a href=\""+EnvironmentDetailsSAS.Path_ScreenShot+"\"> Clickhere </a>");
	            Reporter.log("<a href=\""+ EnvironmentDetailsSAS.Path_ScreenShot + sTestCaseName +".jpg" +"\"><img src=\"file:///"  
	                        + EnvironmentDetailsSAS.Path_ScreenShot + sTestCaseName +".jpg" + "\" alt=\"\""+ "height='800' width='1200'/> "+"<br />"); 
	            
	            Reporter.setCurrentTestResult(null); 
	            
			} catch (Exception e){
				Log.error("Class Utils | Method takeScreenshot | Exception occured while capturing ScreenShot : "+e.getMessage());
				throw new Exception();
			}
		}

	 	/// Function to read data from  excel file
		public Object[][] data(String strSheetName,String strTestcaseName) throws atu.ddf.exceptions.DataDrivenFrameworkException 
		{
		String excelResource = System.getProperty("user.dir")
		+ System.getProperty("file.separator") + "src" +System.getProperty("file.separator") + "testData"
		+ System.getProperty("file.separator") + "TestData.xlsx";
		// provide your excel file path here
		ExcelFile excelFile = new ExcelFile(excelResource);
		// provide the Sheet name
		excelFile.setSheetName(strSheetName);
		// provide the Column Name where Test Case names will be given
		excelFile.setTestCaseHeaderName("TestCaseNameColumn");
		// provide the Test Case Name
		List<List<String>> data = excelFile
		.getDataUsingTestCaseName(strTestcaseName);
		// SeleniumTestNGHelper is a utility class that will help in converting
		// the Test data into TestNG Data Provider supported format
		// One such format here is a 2-Dimensional Object Array
		return SeleniumTestNGHelper.toObjectArray(data);
	}	
		

		public static WebDriver fn_SetChromeDriverProperties()
		{
			//WebDriver driver;

			EnvironmentDetailsSAS objEnv= new EnvironmentDetailsSAS();
			
			//FirefoxProfile profile = new FirefoxProfile(new File("C:\\Users\\madhavi.jn\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\u5qp8rnq.default"));                  
			//WebDriver driver = new FirefoxDriver(profile);

		  /*  ProfilesIni profile = new ProfilesIni();	 
			FirefoxProfile myprofile = profile.getProfile("default");
			//myprofile.setPreference("network.cookie.alwaysAcceptSessionCookies",true);
			//myprofile.setPreference(key, value);
		
			WebDriver driver = new FirefoxDriver(myprofile);

			 driver.get(objEnv.STRURL);	

			 return driver;*/
			
			
	    	
	    /*	String strFFProfilePath = "C:\\Users\\madhavi.jn\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\s6fkibr3.default";
	    	File fileFFProfileDirectory = new File(strFFProfilePath);
	    	FirefoxProfile ffProfile = new FirefoxProfile(fileFFProfileDirectory);
	    	
	    	driver = new FirefoxDriver(ffProfile);*/

		
			
			DesiredCapabilities chromeCapabilities = DesiredCapabilities.chrome();
            Map<String, Object> chromePrefs = new HashMap<String, Object>();           
            chromePrefs.put("download.prompt_for_download", true);
            
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("prefs", chromePrefs);
            chromeCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
			
            String Totalpath=WindowsUtils.getLocalAppDataPath()+"\\Google\\Chrome\\User Data\\Default";

			options.addArguments("user-data-dir="+Totalpath);
            chromeCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
            
			options.addArguments("--start-maximized");
            chromeCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
			
			System.setProperty("webdriver.chrome.driver",EnvironmentDetailsSAS.CHROME_DRIVER_PATH);
			driver = new ChromeDriver(chromeCapabilities);
			driver.get(objEnv.STRURL);
			return driver;
			
			
			
			
			
		}
		
		/// Function to read data from  excel file -- TestData.xls, Sheet - TestCaseDetails
		//This function is included to refer TestData.xls file
		public Object[][] data1(String strSheetName,String strTestcaseName) throws atu.ddf.exceptions.DataDrivenFrameworkException
		{
			String excelResource = System.getProperty("user.dir")
					+ System.getProperty("file.separator") +"trunk"+System.getProperty("file.separator")+"src" +System.getProperty("file.separator") + "testData"
					+ System.getProperty("file.separator") + "TestData.xlsx";
			// provide your excel file path here
			ExcelFile excelFile = new ExcelFile(excelResource);
			// provide the Sheet name
			excelFile.setSheetName(strSheetName);
			// provide the Column Name where Test Case names will be given
			excelFile.setTestCaseHeaderName("TestCaseNameColumn");
			// provide the Test Case Name
			List<List<String>> data = excelFile
					.getDataUsingTestCaseName(strTestcaseName);

			System.out.println(data);			
			return SeleniumTestNGHelper.toObjectArray(data);

		}
		
		
		//Function to Read Values from Excel By Condidtion as "Y"
		//Input Parameters:- File path and Sheet Name
		public Map<String, String> readexcel_ExecEnvironment(String destFile,String vSheetName)
		{
			String Value = null;
			//Create Excel data map Object
			Map<String, String> excelDataMap = new HashMap<String, String>();
			//creating the Try Block			
			try
			{
				//Creating the new File object by passing the destination file				
				//File file = new  File(destFile);
				//passing the created file object as a param to Input stream				
				FileInputStream fileipstream = new FileInputStream(new File(destFile));
				//creating a workbook object by fileipstream				
				XSSFWorkbook wb = new XSSFWorkbook(fileipstream);
				//creating a sheet object by vSheetName				
				XSSFSheet sheet = wb.getSheet(vSheetName);
                //getting the Row Count into Rows variables
				 int rows = sheet.getLastRowNum();
				 
                 //For Loop for iterating from zero to last row of the Excel
				 for(int rowno =0; rowno <=rows;rowno++)
				 {
                     //capturing the rowno to row variable as rowno cant be used further
					 XSSFRow row = sheet.getRow(rowno);
					 //capturing the initial row header value
					 XSSFRow rowheader = sheet.getRow(0);
					 //Capturing the value to validate the condition below
					 XSSFCell cell  = row.getCell(0);
					 
					 if(cell!=null)
					 {
						 switch (cell.getCellType())
		                	{
		                    case HSSFCell.CELL_TYPE_STRING:
		                       	Value =cell.getRichStringCellValue().getString();
		                          break;
		                    case HSSFCell.CELL_TYPE_NUMERIC:
		                    	Value = Integer.toString((int) cell.getNumericCellValue());
		                          break;
		                    case HSSFCell.CELL_TYPE_BOOLEAN:
		                    	Value = Boolean.toString(cell.getBooleanCellValue());
		                         break;
		                    case HSSFCell.CELL_TYPE_FORMULA:
		                    	Value = cell.getCellFormula();
		                        break;                        
		                    }
					 }
                     
					 String data = Value;
					 
					 if ("Y".equals(data))
					 {
	                    //parsing the column content for respective row	
						 for(int column =0; column < row.getLastCellNum() ;column++)
						{
							 //for putting all the values fetched into Map 
							 excelDataMap.put(rowheader.getCell(column).getStringCellValue(),row.getCell(column).getStringCellValue());

						 }

					 }
				 }
                 //returning the Map				 
				 return excelDataMap;


			}

			catch(Exception ioe)
			{
				ioe.printStackTrace();
				return excelDataMap;
			}

		}
		
		/*
		Author: Aashish Bhutkar
		Function fn_getPreviousDate: Returns the date of previous month(s).
		Inputs:	intDate - which date of the month to be returned.
				intMonthsBack - How many Months Back.
		Outputs: formated date string for the date requested.    
*/
		 public String fn_getFutureDate(Integer intMonthsFuture)
		 {	
				String PATTERN = "MM-dd-yyyy";
				SimpleDateFormat dateFormat = new SimpleDateFormat();
				dateFormat.applyPattern(PATTERN);

				//TODO: Get current date and then trace it back by 5 days for loading data.
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.MONTH, +intMonthsFuture);				
				String date1 = dateFormat.format(cal.getTime());
				
				return date1;
		 }
		
   public static void readExcelastable(String destFile) throws FileNotFoundException	
   {
		
	   try {
		   
	    FileInputStream fileipstream = new FileInputStream(new File(destFile));
		XSSFWorkbook workbook = new XSSFWorkbook(fileipstream);
	    int numberOfSheets = workbook.getNumberOfSheets();
	    
	    for (int sheetIdx = 0; sheetIdx < numberOfSheets; sheetIdx++) 
	    {
	        XSSFSheet sheet = workbook.getSheetAt(sheetIdx);
	        List<XSSFTable> tables = sheet.getTables();
	        for (XSSFTable t : tables) {
	            System.out.println(t.getDisplayName());
	            System.out.println(t.getName());
	            System.out.println(t.getNumerOfMappedColumns());

	            int startRow = t.getStartCellReference().getRow();
	            int endRow = t.getEndCellReference().getRow();
	            System.out.println("startRow = " + startRow);
	            System.out.println("endRow = " + endRow);

	            int startColumn = t.getStartCellReference().getCol();
	            int endColumn = t.getEndCellReference().getCol();

	            System.out.println("startColumn = " + startColumn);
	            System.out.println("endColumn = " + endColumn);

	            for (int i = startRow; i <= endRow; i++) {
	                String cellVal = "";

	                for (int j = startColumn; j <= endColumn; j++) {
	                    XSSFCell cell = sheet.getRow(i).getCell(j);
	                    if (cell != null) {
	                        cellVal = cell.getStringCellValue();
	                    }
	                    System.out.print(cellVal + "\t");
	                }
	                
	                System.out.println();
	            }

	        }
	    }

	    //workbook.

		
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
   }
	}
