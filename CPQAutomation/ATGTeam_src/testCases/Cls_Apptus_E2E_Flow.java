package testCases;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pageObjects.SalesForce.AccountPageObjects;
import appModules.AccountSearch;
import appModules.Add_Configurations;
import appModules.ValidateFunctionalDocs;
import appModules.ApptusLoginLogout;
import appModules.CreateOfferingData;
import appModules.CreateProposal;
import appModules.PricingValidation;
import appModules.ProposalDoc_Creation_And_Validation;
import atu.ddf.exceptions.DataDrivenFrameworkException;
import utility.EnvironmentDetailsSAS;
import utility.Log;
import utility.Utils;
import utility.VerificationMethods;
import utility.WordDocsHandler;

public class Cls_Apptus_E2E_Flow extends VerificationMethods{
	
	
		public WebDriver driver;
		public WebDriverWait webWait;
	    public static String EnvironmentDetails.strTestCaseName = "Apptus_E2E_Flow";
	
		 @BeforeTest
		 public void beforeMethod()throws Exception
		 {	
			//TODO: Initialize the Logger.
	    	DOMConfigurator.configure("log4j.xml");	
	    	
	    	Log.startTestCase("Login to application");
	    	
	    	//TODO: Set Chrome driver as the driver to launch the test.
	    	driver = utility.Utils.fn_SetChromeDriverProperties();
	    	    	
			//TODO: initialize the webWait
			webWait = new WebDriverWait(driver,120);
			EnvironmentDetailsSAS objEnv = new EnvironmentDetailsSAS();
	    	
			Log.startTestCase("Login to application");
			//Logs in to the Apptus application with the appropriate credentials	C:\CPQ_Automation\src\testData\Budgetary Proposal - To Present2016-07-07 12-24-13.pdf
			ApptusLoginLogout.appLogin(driver,webWait,objEnv.STRUSERNAME,objEnv.STRPASSWORD);
			
		 }
	 
		 @Test(dataProvider = "objectTestData", description = "Apptus_E2E_Flow")
		 public void fn_E2E_Apptus_E2E_Flow(String strSearchItem,String strOpportunityName,String strCurrency,String strProposalData,String strOfferingData,String strBaseServiceName,String strComponentName,String strAddress) throws Throwable 
		 {
			 //AccountSearch  ObjAcctSrch = new  AccountSearch();
			 //CreateLeadandConvertToOpportunity objLead = new CreateLeadandConvertToOpportunity();
			 AccountSearch objSearch = new AccountSearch();
			 CreateProposal objProposal = new  CreateProposal();
			 CreateOfferingData objOfferingData = new CreateOfferingData();
			 Add_Configurations objConfiguration = new  Add_Configurations();
			 PricingValidation  objPriceValidation = new  PricingValidation();
			 ValidateFunctionalDocs objValidateFunctionalDocs = new ValidateFunctionalDocs();
			 WordDocsHandler objWordhandler = new WordDocsHandler();	 
			 ProposalDoc_Creation_And_Validation objPropdocval= new ProposalDoc_Creation_And_Validation();
			
				
			 try {
				 
				 //objWordhandler.fn_validateText("C:\\Users\\madhavi.jn\\Desktop\\apptusdocs\\APT_Financial_Services_Limited-GMSA_Original_GMSA_Dynamic.pdf","pdf","GMSA_Validations");
				 //String LeadCreationData,String ConversionData,String OpportunityData
	            //Account search-open, click on the userdefined Opportunity-open
			/*	Boolean blnCreateLead = ObjLead.CreateLeadAndConvertToOpportunity(driver, webWait, LeadCreationData, ConversionData, OpportunityData);
				if (blnCreateLead==true){
					 Log.info("Lead has created successfully and convert to Opportunity");}
				else{
					 Log.info("ERROR: Unable to Create Lead and convt to Opportunity");
					 assertTrue(false, "ERROR: Unable to Create Lead and convt to Opportunity");}	*/
				 
			    //Search a Valid Account,and then its Opportunity and Create a new proposal from Opportunity 
			/*    Boolean blnAcctSearch=objSearch.fn_AccountSearch(driver, webWait, strSearchItem, strOpportunityName, strBaseServiceName, strCurrency);
				if (blnAcctSearch==true){
					 Log.info("Proposal has been created successfully from Opportunity");}
				else{
					 Log.info("ERROR: Unable to create the Proposal from Opportunity");
					 assertTrue(false, "ERROR: Unable to create the Proposal from Opportunity");}	
							
			    //Click on Create Proposal -Fill out Proposal details -Create it and  click on config button
				Boolean blnCreateProposal  = objProposal.fn_CreateProposal(driver, webWait, strProposalData);
				if (blnCreateProposal==true){
					 Log.info("Proposal Data has been entered and created successfully");}
				else{
					 Log.info("ERROR: Unable to enter the data and create the Proposal");
					 assertTrue(false, "ERROR: Unable to enter data and create the Proposal");}	*/
				 
					strSearchItem="Q-00002115";
					AccountPageObjects objacctpage = new AccountPageObjects();
					objacctpage.fn_setSearchField(driver, webWait).sendKeys(strSearchItem); 
					objacctpage.fn_clickSearchBtn(driver, webWait).click();
					Thread.sleep(1000);
					driver.findElement(By.partialLinkText(strSearchItem)).click();
				
	            //Create the offering data
			/*	Boolean blnOffrngData = objOfferingData.fn_CreateOfferingData(driver, webWait,strOfferingData);	
				if (blnOffrngData==true){
					 Log.info("offeringdata has been Created successfully");}
				else{
					 Log.info("ERROR: Unable to Create the offeringdata");}*/
				
			
				//Configuring the Services
				//Boolean blnAddConfig = objConfiguration.fn_AddConfigurations(driver, webWait, strBaseServiceName, strComponentName,EnvironmentDetails.strTestCaseName);
			/*	if (blnAddConfig==true){
					 Log.info("Configuration has been created successfully");}
				else{
					 Log.info("ERROR: Unable to add the configuration");
					 assertTrue(false, "ERROR: Unable to add the configuration");}	
				
				//Pricing Validation for the Services
				Boolean blnValpricing  = objPriceValidation.fn_PricingValidation(driver, webWait);
				if (blnValpricing==true){
					 Log.info("Pricing has been Validated successfully");}
				else{
					 Log.info("ERROR: Unable to validate the pricing");
					 assertTrue(false, "ERROR: Unable to validate the pricing");}	*/
		
				//Budgetary Proposal Document creation and Validation
			  /*  Boolean blnValiadateBudgPropCreation = objPropdocval.fn_ProposalDocCreationValidation(driver, webWait,"Budgetary Proposal - To Present");
				if (blnValiadateBudgPropCreation==true){
					 Log.info("BudgetaryProposal document has been created and Validated successfully");}
				else{
					 Log.info("ERROR: Unable to Create the BudgetaryProposal document");}*/
				
				
				//Proposal Document creation and Validation
			  /* Boolean blnValiadatePropCreation = objPropdocval.fn_ProposalDocCreationValidation(driver, webWait,"Proposal - To Present");
				if (blnValiadatePropCreation==true){
					 Log.info("Proposal document has been created and Validated successfully");}
				else{
					 Log.info("ERROR: Unable to Create the Proposal document");}*/
				
				//Approve the Proposal and Generate the GMSA document
				Boolean blnValAgrmntApproval  = objValidateFunctionalDocs.fn_ValidateFunctionalDocs(driver, webWait, "GMSA", strAddress);
			    if(blnValAgrmntApproval==true){
					 Log.info("GMSA DOC has been Generated and Validated successfully");}
				else{
					 Log.info("ERROR: Unable to Generate and Validate the Document");
					 assertTrue(false, "ERROR: Unable to Generate and Validate the Document");}	
				
				
				//Approve the Proposal and Generate the NDA document
			   Boolean blnValNDAApproval  = objValidateFunctionalDocs.fn_ValidateFunctionalDocs(driver, webWait, "NDA", strAddress);
			   if(blnValNDAApproval==true){
					 Log.info("NDA DOC has been Generated and Validated successfully");}
				else{
					 Log.info("ERROR: Unable to Generate and Validate the Document");
					 assertTrue(false, "ERROR: Unable to Generate and Validate the Document");}	
				     		
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Log.info("ERROR: problem while Creating the Apptus-E2E flow");
				e.printStackTrace();
			}
		 }
		 
		//Reading data for the test    
		@DataProvider(name = "objectTestData")
		public Object[][] testExcelData() throws DataDrivenFrameworkException
		{
		    Utils objUtils=new Utils();
		    return objUtils.data1("TestCaseDetails", "Apptus_E2E_Flow");
		}
	 
	/*	@AfterTest
		 public void afterMethod() throws Exception
		{			 
		 //TODO: Logs out of the application & quit the driver		
		 ApptusLoginLogout.appLogout(driver, webWait);
		 driver.quit(); 
		 }*/

}
