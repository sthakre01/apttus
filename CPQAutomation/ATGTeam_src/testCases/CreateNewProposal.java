package testCases;

import org.testng.annotations.Test;
import pageObjects.Apttus.ProposalFormPageObject;
import pageObjects.Apttus.ProposalsPageObject;
import pageObjects.SalesForce.LandingPageObject;

/**
 * Created by joe.gannon on 2/10/2016.
 */
public class CreateNewProposal extends BaseTestCase
{
    @Test
    public static void createNewProposal()
    {
        LandingPageObject lp = login();
        ProposalsPageObject.clickProposalsTab(driver);
        ProposalFormPageObject.fillNewProposalForm(driver);
    }

}
