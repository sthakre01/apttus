package testCases;

import org.testng.Assert;
import org.testng.annotations.Test;
import pageObjects.SalesForce.LandingPageObject;
import pageObjects.SalesForce.OpportunitesFormPageObject;
import pageObjects.SalesForce.OpportunitesPageObject;

/**
 * Created by joe.gannon on 2/10/2016.
 */
public class CreateNewOpportunity extends BaseTestCase
{


    @Test
    public static void createNewOpportunity() throws Exception
    {
        LandingPageObject lp = login();
        Assert.assertTrue(lp.isLoaded(driver));
        OpportunitesPageObject.createNewOpportunity(driver);
        OpportunitesFormPageObject.fillOpportunityForm(driver);
        Thread.sleep(10000);
    }
}
