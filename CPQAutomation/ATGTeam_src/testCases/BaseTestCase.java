package testCases;


import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.os.WindowsUtils;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import pageObjects.SalesForce.LandingPageObject;
import pageObjects.SalesForce.LoginPageObject;
import utility.EnvironmentDetails;
import utility.Log;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by joe.gannon on 2/9/2016.
 */

public class BaseTestCase
{
    public static WebDriver driver;
    @BeforeClass()
    public void beforeClass() throws Exception
    {
        System.setProperty("webdriver.chrome.driver", EnvironmentDetails.CHROME_DRIVER_PATH);
        ChromeOptions options = new ChromeOptions();
//        Map prefs = new HashMap();
//        prefs.put("profile.default_content_settings.cookies", 0);
//        options.setExperimentalOption("prefs", prefs);
        
         String Totalpath=WindowsUtils.getLocalAppDataPath()+"\\Google\\Chrome\\User Data\\Default";

		options.addArguments("user-data-dir="+Totalpath);
        
        //options.addArguments("user-data-dir=C:\\Users\\joe.gannon\\AppData\\Local\\Google\\Chrome\\User Data\\Default");
        options.addArguments("--start-maximized");
        driver = new ChromeDriver(options);
//        driver = new FirefoxDriver();
//        Cookie sidClientCookie = new Cookie("sid_Client","00000011uuv0000003oZUU");
//        Cookie sidCookie = new Cookie("sid","00DP0000003oZUU!ARMAQNF30aL4FiAyYT4t5etiluNHD6vY1ziLxzKlDZSXQQwV3yFyiHaYibHJPWMeTw5Yt4k4T.NX2orXIQQ6yjq1wXygePvS");
//        Cookie oidCookie = new Cookie("oid", "00DP0000003oZUU");
//        Cookie testCookie = new Cookie("testc", "testcookie");
//        Cookie umps_clientId = new Cookie("umps_clientId", "e2b95538ef439c2b55d146d3107a23a77ef31670");

        driver.navigate().to(EnvironmentDetails.URL);

//        driver.manage().addCookie(sidClientCookie);
//        driver.manage().addCookie(sidCookie);
//        driver.manage().addCookie(oidCookie);
//        driver.manage().addCookie(umps_clientId);
//        driver.manage().addCookie(testCookie);

    }
//    @Test
    public static LandingPageObject login()
    {
        LandingPageObject lp = LoginPageObject.login(driver);
        Log.info("Verifying Logged in: "+lp.isLoaded(driver));
        return lp;
    }

//    @AfterClass()
//    public void afterClass()
//    {
//        driver.quit();
//    }
}
