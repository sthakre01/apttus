package testCases.ValidationTestCases;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.Test;
import pageObjects.Apttus.ConstraintValidation.ContentDeliveryConstraintPageObject;
import pageObjects.Apttus.ProductConfig.ProductConfigPageObject;
import testCases.BaseTestCase;
import testCases.ValidationTestCases.BaseValidationTestCase;

/**
 * Created by joe.gannon on 5/18/2016.
 */
public class ContentDeliveryValidation extends BaseValidationTestCase
{
    @Test
    public void executeTest()
    {
        BaseValidationTestCase.validateProduct(driver,new ContentDeliveryConstraintPageObject());

    }
}
