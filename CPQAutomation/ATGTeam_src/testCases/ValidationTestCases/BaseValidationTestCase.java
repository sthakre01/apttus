package testCases.ValidationTestCases;

import appModules.Product;
import appModules.TierPrice;
import appModules.ValidationModule;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import pageObjects.Apttus.OptionConfigPageObject;
import pageObjects.Apttus.ProductConfig.PricingPageObject;
import pageObjects.Apttus.ProductConfig.ProductConfigPageObject;
import pageObjects.SalesForce.*;
import testCases.BaseTestCase;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by joe.gannon on 5/18/2016.
 */
public class BaseValidationTestCase extends BaseTestCase
{
    public static void validateProduct(WebDriver driver, ProductConfigPageObject constraintPageObject)
    {
        try {
            boolean executeRemainder = true;
            System.out.println("Starting Testing Data Driven");
            ArrayList<Product> productList = ValidationModule.LoadProducts();
            String priceList = "Ireland Price List";
            //TODO: Wrap product list into a loop
            String myProdName = productList.get(0).getProductName();
            Product currentProd = productList.get(0);
            currentProd.setAttributes();
            String prodL3Name = productList.get(0).getProductData().get("Step 2 Setup Categories").get(0).get("Level 3 Name");
            String prodL2Name = productList.get(0).getProductData().get("Step 2 Setup Categories").get(0).get("Level 2 Name");
            currentProd.setL2Name();
            System.out.println("Current Product: " + currentProd.getProductName() + " || L3Name: " + prodL3Name+ "|| L2Name: "+currentProd.getL2Name());
            LandingPageObject lp = LoginPageObject.login(driver);
            AccountPageObject acctPage = lp.searchForAccount(driver, "Automation - Apttus");
            OpportunityPageObject op = acctPage.clickOppLink(driver, "Test Opp1");
            System.out.println("Number Options: "+currentProd.getOptions().size());

            //Fill out Proposal deatils

            ProposalPageObject prop = op.clickCreatePropBtn(driver);
            if (currentProd.getProductName().contains("Legacy")) {
                System.out.println("019_" + myProdName + " Product List Select: " + priceList);
            }
            prop.setPriceList(driver, priceList);
            prop.setPropName(driver, "Testing Prop");
            prop.setPrimaryContact(driver, "Boba Fett");
            prop.clickSaveBtn(driver);

            //TODO: Offerring data needs to occur here.
//            OfferingDataPageObject.clickNewOfferingdataButton(driver);
//            OfferingDataPageObject.fillOutOfferingData(driver,currentProd.getAttributes().get(0));

            OptionConfigPageObject cp = prop.clickConfigProductBtn(driver);

            Thread.sleep(7000);
            //Select Product Hierarchy
//            String newL2Name = currentProd.getL2Name();
            System.out.println("Second Print Statement: " + currentProd.getProductName() + " || L3Name: " + prodL3Name+ "|| L2Name: "+currentProd.getL2Name());
            if (prodL3Name.equals(null) || prodL3Name.equals(""))
            {
                System.out.println("Third Print Statement: " + currentProd.getProductName() + " || L3Name: " + prodL3Name+ "|| L2Name: "+currentProd.getL2Name());
                System.out.println("Selecting L2 Name: "+currentProd.getL2Name()+ " || "+currentProd.getProductName());
                cp.selectProduct(driver, currentProd.getL2Name());
            } else {
                System.out.println("Selecting L2 Name: "+prodL3Name.trim());
                cp.selectProduct(driver, prodL3Name.trim());
            }

            boolean prodFound = cp.getProductBtn(driver, currentProd);
            System.out.println("prodFound: " + prodFound);
            //After Clicking check to see if TierPrices contain Terms
            boolean tpExists = false;
            if(!(currentProd.getTierPrices()== null))
            {
                for(TierPrice tp: currentProd.getTierPrices())
                {
                    if(tp.getTierType().contains("Term"))
                    {
                        System.out.println("Term stuff exists!");
                        tpExists = true;
                    }
                }
            }

            if(tpExists)
            {
                //set the term amount
            }
            //Then click next
            OptionConfigPageObject.clickOfferingNextBtn(driver);
            //TODO: convert server options to dynamic based on data
            ProductConfigPageObject phyServ = new ProductConfigPageObject();
            Thread.sleep(2000);
            if (!prodFound) {
                //check to see if it's really found
            }
            if (prodFound) {
                boolean constraintsFound = !(currentProd.getConstraintRules().isEmpty());
                //ExecuteValidation(new );
                //TODO: If Constraint rules exist, continue constraint flow
                if (constraintsFound) {
                    System.out.println("CHECKING CONSTRAINT RULES:");
                    executeRemainder = constraintPageObject.verifyingConstraints(driver, currentProd);
                }

                //TODO: Verify strict layout of the product matches the workbook
                //Validate
//                Thread.currentThread().interrupt(); //REMOVE THIS
                phyServ.validateOptionSequence(driver, currentProd);

                //TODO: Do constraint validation here?
                if (executeRemainder) {
                    Thread.sleep(5000);
                    phyServ.new_clickAllCbs(driver, currentProd);
                    //TODO: After Clicking all checkboxes, get the Max Qty's for each option
                    ValidationModule.setOptionQty(driver, currentProd);

                    //TODO: Add in non-integer Validation
                }
                    ArrayList<HashMap<String, String>> currentProductMap = currentProd.getProductData().get("Step 3 Setup Bundles");
                    if (currentProductMap != null) {
                        for (HashMap<String, String> minMaxMap : currentProd.getProductData().get("Step 3 Setup Bundles"))
                        {
                            System.out.println("012_" + minMaxMap.get("Option") + "Min Qty: " + minMaxMap.get("Min Options") + ": true");
                            System.out.println("015_" + minMaxMap.get("Option") + " Max Qty: " + minMaxMap.get("Max Options") + ": true");
                        }
                    }

                    ArrayList<HashMap<String, String>> constraintMap = currentProd.getProductData().get("Step 4 Constraint Rules");
                    if (constraintMap == null) {
                        System.out.println("007_" + currentProd.getProductName() + " No Constraint Rules Found: true");
                    }

                    System.out.println("005_Product Attributes Validation: " + OptionConfigPageObject.verifyProductAttributes(driver, currentProd));
                    if (currentProductMap != null) {
                        phyServ.validateProductOptions(driver, currentProd);
                    }


                    Thread.sleep(2000);
                    phyServ.clickGoToPricing(driver);
                    PricingPageObject.clickToggleBtn(driver);
                    PricingPageObject.getPricingTable(driver, currentProd);
                    System.out.println("008_Product name appears on shopping cart page: " + PricingPageObject.validateProductNameOnPricing(driver, myProdName));
                    System.out.println("009_Product Code matches current Product: " + PricingPageObject.validateProductCode(driver, currentProd));
                    PricingPageObject.validateBundleCodes(driver, currentProd);

                    if(currentProd.getTierPrices().size() > 0)
                    {
                        //Iterate through all of the Tier Prices and check pricing
                        ValidationModule.validateTierPrices(driver,currentProd);

                    }
                    Thread.sleep(2000);
                    driver.findElement(By.xpath(("//*[@id=\"Next\"]/a"))).click();
                    Thread.sleep(5000);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public static float round(String fee)
    {
        BigDecimal bd = new BigDecimal(fee);
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }
}
