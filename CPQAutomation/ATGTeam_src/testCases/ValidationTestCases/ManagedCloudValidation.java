package testCases.ValidationTestCases;

import org.testng.annotations.Test;
import pageObjects.Apttus.ConstraintValidation.ContentDeliveryConstraintPageObject;
import pageObjects.Apttus.ConstraintValidation.ManagedCloudConstraintPageObject;

/**
 * Created by joe.gannon on 6/14/2016.
 */
public class ManagedCloudValidation extends BaseValidationTestCase
{
    @Test
    public void executeTest()
    {
        BaseValidationTestCase.validateProduct(driver,new ManagedCloudConstraintPageObject());

    }
}
