package testCases.ValidationTestCases;

import org.testng.annotations.Test;
import pageObjects.Apttus.ConstraintValidation.OneLineDataConstraintPageObject;

public class OneLineDataValidation extends BaseValidationTestCase
{
    @Test
    public void executeTest()
    {
        BaseValidationTestCase.validateProduct(driver,new OneLineDataConstraintPageObject());

    }
}
