package testCases.ValidationTestCases;

import org.testng.annotations.Test;
import pageObjects.Apttus.ConstraintValidation.ContentDeliveryConstraintPageObject;
import pageObjects.Apttus.ConstraintValidation.WafConstraintPageObject;

public class WafValidation extends BaseValidationTestCase
{
    @Test
    public void executeTest()
    {
        BaseValidationTestCase.validateProduct(driver,new WafConstraintPageObject());

    }
}
