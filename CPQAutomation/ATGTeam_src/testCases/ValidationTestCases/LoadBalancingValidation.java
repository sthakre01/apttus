package testCases.ValidationTestCases;

import org.bouncycastle.jcajce.provider.symmetric.ARC4;
import org.testng.annotations.Test;
import pageObjects.Apttus.ConstraintValidation.ContentDeliveryConstraintPageObject;
import pageObjects.Apttus.ConstraintValidation.LoadBalanceConstraintPageObject;

/**
 * Created by joe.gannon on 6/22/2016.
 */
public class LoadBalancingValidation extends BaseValidationTestCase
{
    @Test
    public void executeTest()
    {
        BaseValidationTestCase.validateProduct(driver,new LoadBalanceConstraintPageObject());

    }
}
