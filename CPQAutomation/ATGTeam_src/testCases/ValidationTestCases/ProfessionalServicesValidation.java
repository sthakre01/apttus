package testCases.ValidationTestCases;

import org.testng.annotations.Test;
import pageObjects.Apttus.ConstraintValidation.ContentDeliveryConstraintPageObject;
import pageObjects.Apttus.ConstraintValidation.ProfServiceConstraintPageObject;

public class ProfessionalServicesValidation extends BaseValidationTestCase
{
    @Test
    public void executeTest()
    {
        BaseValidationTestCase.validateProduct(driver,new ProfServiceConstraintPageObject());

    }
}
