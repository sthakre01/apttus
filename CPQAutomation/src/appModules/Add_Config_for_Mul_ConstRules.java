 /*==============================================================
 Author     		:	Madhavi JN
 Class Name		 	: 	Add_Config_for_MultiProducts
 Purpose     		: 	Configuring the Multiple Products
 Date       		:	13/02/2017
 Version Information:	Version1.0
 PreCondition 		:	None
 Test Steps 		:	Configuring the Line Items for specific product
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
 #======================================================================
 */
package appModules;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.ConfigurationPageObjects;
import utility.API_SOAP;
import utility.EnvironmentDetails;
import utility.ExcelHandler;
import utility.Log;
import utility.Utils;
import utility.VerificationMethods;

public class Add_Config_for_Mul_ConstRules extends VerificationMethods{
	
	public Boolean fn_Config_for_Mul_CRules(WebDriver driver,WebDriverWait webWait, String strComponentName, 
			List<List<Map<String, String>>> lstmapResponse, String strLineItems) throws Exception{

	String strLineItemName=null;
	String strXpath = null;
 	String[] arrLineItemData=null;
 	String strEquipRefName="";
 	String strApttusQuery="";
    Boolean blnIsAllLineItemConfig=false;
    Boolean blnAddAllLineItems=false;
    Boolean blnAddSpecificLineItems=false;
//	Boolean blnIsProductConfigured = false;
	Boolean blnConfiguredSuccessful=false;
    Integer intCounter=0;
		
	ConfigurationPageObjects objConfig = new ConfigurationPageObjects();
	API_SOAP objSOAP = new API_SOAP();
	ExcelHandler objExcel = new ExcelHandler();
	EnvironmentDetails objEnvironment = new EnvironmentDetails();
    ConfigurationPageObjects objConfiguration = new ConfigurationPageObjects();
	Add_CRules_for_Mul objCRules = new Add_CRules_for_Mul();
	Add_Config_for_Mul_ConstRules objConfigMul = new Add_Config_for_Mul_ConstRules();
		
    try{
       	objConfig.fn_SearchProducts(driver, webWait).sendKeys(strComponentName); 
    	objConfig.fn_clicksearchBtn(driver, webWait).click();
    	Thread.sleep(8000);  
    	strComponentName=" "+strComponentName;
    	objConfig.fn_clickServiceName(driver, webWait, strComponentName).click();   	
    	Thread.sleep(6000); 
    	strEquipRefName = strComponentName+"_Configuration";
    	objConfig.fn_setEquipReference(driver, webWait).sendKeys(strEquipRefName);
    	Thread.sleep(3500); 
    	objConfig.fn_clickNext(driver, webWait).click();
    	Thread.sleep(6000); 
    	Map<String, Object> mapTestCases = new HashMap<String, Object>();
    	
	    //TODO:: Handling the specific condition to add all the Line items 
	    if (strLineItems.equalsIgnoreCase("ALL")){	    
	    	blnAddAllLineItems=objConfigMul.fn_Add_CR_AllLineItems(lstmapResponse, driver, webWait);
	    	if (blnAddAllLineItems==true)
	    		Log.debug("DEBUG: All Line Items have been successfully configured.");
	    	else
	    		Log.debug("Error: problem occoured while configuring All Line Items.");
	     }
	 	else{
 		    //TODO:: Handling the specific condition to add specific Line items which are user defined	    	
 		    arrLineItemData = strLineItems.toString().trim().split("##");	    	
 		   	for(Integer i=0;i<arrLineItemData.length;i++){ 		    	
 		   		strLineItemName = arrLineItemData[i].toString().trim();		    		
 		   		if ((lstmapResponse.toString().toLowerCase()).contains((strLineItemName.toLowerCase()))){
	   		        //Step10 
				    strApttusQuery = objExcel.fn_ReadApttusQuery("ApttusQuery", "ApttusQuerySheet", "Fetching_ConstraintRules_Product");					
				    Log.debug("DEBUG: Query 'To Fetch Constraint Rules for Product' Before concatenating is: "+strApttusQuery);
				    strApttusQuery = strApttusQuery.replace("'strProductName'","'"+strLineItemName+"'");
				    Log.debug("DEBUG: Query 'To Fetch Constraint Rules for Product' after concatenating is "+strApttusQuery);
				    List<List<Map<String, String>>> lstmapConstrRules = new ArrayList<List<Map<String, String>>>();
					
				   //Step11 
					mapTestCases.put("CallType", "query");
					mapTestCases.put("CallingData", strApttusQuery); 
				    lstmapConstrRules = objSOAP.fn_SOAP_GET(objEnvironment, mapTestCases, null); 					
				    String strMapRepose=objConfigMul.fn_CR_fetchResponse(lstmapConstrRules);		
                    if (strMapRepose.contains("No result returned for the SOAP response..")){                                                	
                    	Log.debug("DEBUG: There is not constraint rule assigned for this Line item ::"+strLineItemName);
                    	strXpath = "//a[text()='"+strLineItemName+"']/ancestor::tr[1]/td[1]//input";
						blnIsAllLineItemConfig=objConfigMul.fn_CR_ConfigureProducts(driver, webWait, strXpath, strLineItemName);	
						if (blnIsAllLineItemConfig) {
							Log.debug("DEBUG: Configure product successfully");
						}
						 
                        }
                    else{                       
                       	Log.debug("DEBUG: There is a constraint rule assigned for this Line item proceeding further for validation of the Rule ::"+strLineItemName);
                       	//Step12 proceed with CRule Validation of Lineitem
                       	objCRules.fn_CRules_for_Mul(driver, webWait, lstmapConstrRules,strLineItemName);
                    }    
	 		    	intCounter++;
 		    		}		
 		    	else {   		
 		    		Log.debug("ERROR: The user defined Line Item '"+strLineItemName+"' is not available in the system!");
 		     }
 		  } 
    	//TODO:: Handling the specific condition if none of the User defined line items are not matching with the system data
    	 if (intCounter==0){
    	 	// here, we forcefully configure all line items since none of the user defined items are available.
    		blnAddSpecificLineItems=objConfigMul.fn_Add_CR_Specific_LineItems(lstmapResponse, driver, webWait);
	    	if (blnAddSpecificLineItems==true)
	    		Log.debug("DEBUG: Specific Line Items have been successfully configured.");
	    	else
	    		Log.debug("Error: problem occoured while configuring Specific Line Items.");
    		Log.debug("DEBUG: Configuring the Line Items with the data present in the system and proceeding further !");
    	 }
 	 }   
 	blnConfiguredSuccessful = objConfiguration.fn_clickAddMorePrdctsBtn(driver, webWait).isDisplayed();   
    }
	catch (Exception e){
    	StringWriter stack = new StringWriter();
		e.printStackTrace(new PrintWriter(stack));
		Log.debug("DEBUG: The exception in Validating the condition for Adding Multiple products is: "+stack);
        }	
      
      return blnConfiguredSuccessful;
    }
	  
	//Purpose :- function to add all the line items present in the system
	//Input:- driver,wait,List of Response->lstmapResponse
    //Output:- Parsed Response value
    public Boolean fn_Add_CR_AllLineItems(List<List<Map<String, String>>> lstmapResponse,
    		WebDriver driver,WebDriverWait webWait){
 
	String strApttusQuery="";
	Boolean blnIsAllLineItemConfig = false;
	Map<String, Object> mapTestCases = new HashMap<String, Object>();
	Add_Config_for_Mul_ConstRules objConfigMultiProducts = new Add_Config_for_Mul_ConstRules();
	API_SOAP objSOAP = new API_SOAP();
	ExcelHandler objExcel = new ExcelHandler();
	EnvironmentDetails objEnvironment = new EnvironmentDetails();
	Add_CRules_for_Mul objCRules = new Add_CRules_for_Mul();
	Add_Config_for_Mul_ConstRules objMulPrdctsCRules = new Add_Config_for_Mul_ConstRules();
    	
	try
	{   	
	   	for(List<Map<String, String>> lstmap: lstmapResponse){    	
    	    for(Map<String, String> map : lstmap){    	    	    	        		 
    	        for (Map.Entry<String, String> entry : map.entrySet()){    	          
    	        	String strLineItemName = entry.getValue().toString().trim();    	        	
					//Step10 
					strApttusQuery = objExcel.fn_ReadApttusQuery("ApttusQuery", "ApttusQuerySheet", "Fetching_ConstraintRules_Product");					
					Log.debug("DEBUG: Query 'To Fetch Constraint Rules for Product' Before concatenating is: "+strApttusQuery);			
					strApttusQuery = strApttusQuery.replace("'strProductName'","'"+strLineItemName+"'");								 
					Log.debug("DEBUG: Query 'To Fetch Constraint Rules for Product' after concatenating is "+strApttusQuery);						
					List<List<Map<String, String>>> lstmapConstrRules = new ArrayList<List<Map<String, String>>>();						
					//Step11 
					mapTestCases.put("CallType", "query");
					mapTestCases.put("CallingData", strApttusQuery); 
					lstmapConstrRules = objSOAP.fn_SOAP_GET(objEnvironment, mapTestCases, null);
					String strMapRepose=objMulPrdctsCRules.fn_CR_fetchResponse(lstmapConstrRules);
					if (strMapRepose.contains("No result returned for the SOAP response..")){
                    	Log.debug("DEBUG: There is not constraint rule assigned for this Line item ::"+strLineItemName);
						String strXpath = "//a[text()='"+strLineItemName+"']/ancestor::tr[1]/td[1]//input";
						blnIsAllLineItemConfig=objConfigMultiProducts.fn_CR_ConfigureProducts(driver, webWait, strXpath, strLineItemName);	
                     }
                    else{
                		Log.debug("DEBUG: There is a constraint rule assigned for this Line item proceeding further for validation of the Rule ::"+strLineItemName);
                    	//Step12 proceed with CRule Validation of Lineitem
                    	objCRules.fn_CRules_for_Mul(driver, webWait, lstmapConstrRules,strLineItemName);
                     }
			    }
    	    }
    	}      
    	
	}
	catch (Exception exceptionResponse){
    	StringWriter stack = new StringWriter();
    	exceptionResponse.printStackTrace(new PrintWriter(stack));
		Log.debug("DEBUG: There is exception in adding all Line Items recorded as: "+stack);   		
    }
	return blnIsAllLineItemConfig;
 }


	//Purpose :- function to add Limited number of line items present in the system
	//Input:- driver,wait,List of Response->lstmapResponse
    //Output:- Parsed Response value
    public Boolean fn_Add_CR_Specific_LineItems(List<List<Map<String, String>>> lstmapResponse,WebDriver driver,WebDriverWait webWait){
        	
	String strApttusQuery="";
	Integer intcounter=0;
	Boolean blnIsSpecificLineItemConfig = false;   	
    	
	Add_Config_for_Mul_ConstRules objConfigMultiProducts = new Add_Config_for_Mul_ConstRules();
	ExcelHandler objExcel = new ExcelHandler();
	EnvironmentDetails objEnvironment = new EnvironmentDetails();
	Add_CRules_for_Mul objCRules = new Add_CRules_for_Mul();
	Add_Config_for_Mul_ConstRules objMulPrdctsCRules = new Add_Config_for_Mul_ConstRules();
	API_SOAP objSOAP = new API_SOAP();
	Map<String, Object> mapTestCases = new HashMap<String, Object>();
  	
    try{
    	   	
    	for(List<Map<String, String>> lstmap: lstmapResponse){
	    	for(Map<String, String> map : lstmap){
	    	    for (Map.Entry<String, String> entry : map.entrySet()){
    	        	if (intcounter<2){
	    	        	String strLineItemName = entry.getValue().toString().trim();						
						//Step10 
						strApttusQuery = objExcel.fn_ReadApttusQuery("ApttusQuery", "ApttusQuerySheet", "Fetching_ConstraintRules_Product");
						Log.debug("DEBUG: Query 'To Fetch Constraint Rules for Product' Before concatenating is: "+strApttusQuery);		
						strApttusQuery = strApttusQuery.replace("'strProductName'","'"+strLineItemName+"'");										 
						Log.debug("DEBUG: Query 'To Fetch Constraint Rules for Product' after concatenating is "+strApttusQuery);
						List<List<Map<String, String>>> lstmapConstrRules = new ArrayList<List<Map<String, String>>>();	
						//Step11
						mapTestCases.put("CallType", "query");
						mapTestCases.put("CallingData", strApttusQuery); 
						lstmapConstrRules = objSOAP.fn_SOAP_GET(objEnvironment, mapTestCases, null);							
						String strMapRepose=objMulPrdctsCRules.fn_CR_fetchResponse(lstmapConstrRules);
	                    if (strMapRepose.contains("No result returned for the SOAP response..")){                       	
                        	Log.debug("DEBUG: There is not constraint rule assigned for this Line item ::"+strLineItemName);
    						String strXpath = "//a[text()='"+strLineItemName+"']/ancestor::tr[1]/td[1]//input";
    						blnIsSpecificLineItemConfig=objConfigMultiProducts.fn_CR_ConfigureProducts(driver, webWait, strXpath, strLineItemName);	
    						intcounter++;                      	
		                 }
		                else{
		                    Log.debug("DEBUG: There is a constraint rule assigned for this Line item proceeding further for validation of the Rule ::"+strLineItemName);
		                    //Step12 proceed with CRule Validation of Lineitem
		                    objCRules.fn_CRules_for_Mul(driver, webWait, lstmapConstrRules,strLineItemName);
		                 }
				     }
				 }
	    	 }
	     }      	    	
      }
     catch (Exception exceptionResponse){
 	    	StringWriter stack = new StringWriter();
	    	exceptionResponse.printStackTrace(new PrintWriter(stack));
			Log.debug("DEBUG: There is exception in adding all Line Items recorded as: "+stack);
			blnIsSpecificLineItemConfig=false;
      }   	
    return blnIsSpecificLineItemConfig;
  }
    
	//Purpose :- function to fetch the Response value
	//Input:- List of Response->lstmapResponse
    //Output:- Parsed Response value
    public String fn_CR_fetchResponse(List<List<Map<String, String>>> lstmapResponse)
    {
    	String strResponseValue="";
    	
    	try
    	{   	
	    	for(List<Map<String, String>> lstmap: lstmapResponse)
	    	{    	        	 
	    	    for(Map<String, String> map : lstmap)
	    	    {    	        		 
	    	        for (Map.Entry<String, String> entry : map.entrySet())
	    	        {          	        	        	               						
	    	        	strResponseValue = entry.getValue().toString().trim();   		
				    }
	    	    }
	    	}
	    	
    	}
    	catch (Exception exceptionResponse)
    	{
 	    	StringWriter stack = new StringWriter();
	    	exceptionResponse.printStackTrace(new PrintWriter(stack));
			Log.debug("DEBUG: The exception in fetching the Response value is: "+stack);   		
    	}
    
		return strResponseValue;
    }
    
	//Purpose :- function to Configure the Line items of a product
	//Input:- driver,wait,constructed Xpath->strXpath,Name of the Line item->strOptionName
    //Output:- Parsed Response value
    public Boolean fn_CR_ConfigureProducts(WebDriver driver,WebDriverWait webWait,String strXpath,String strOptionName) throws InterruptedException
    {
    	
    	Boolean blnIsProductConfigured = false;
    	
		 try
		 {
			Log.debug("++++++++++++++++++++++++++++++++++++Start of Configuration of Services+++++++++++++++++++++++++++++++++++++++++++++");
			 
            //Identify the checkbox/radio button control
			WebElement webIdentifyChkBox = driver.findElement(By.xpath(strXpath));					
			Thread.sleep(5000);
			Boolean blnIsChecked = webIdentifyChkBox.isSelected();
				if (blnIsChecked==false)
				   {
					webIdentifyChkBox.click();	// to click the checkbox
					Thread.sleep(10000);
					Utils.fn_waitForSpinner(driver);
				    // to identify back the checked check-box/radio button webelement.
					WebElement webSelectChkBox = driver.findElement(By.xpath(strXpath));					
					Boolean blnResult = webSelectChkBox.isSelected();
						if (blnResult==true) {
							
							blnIsProductConfigured = true;
							Log.debug("DEBUG: Component of the Configuration '"+strOptionName+"' is selected successfully !");
						}
						else {
							
							blnIsProductConfigured = false;
							Log.debug("ERROR: Component of the Configuration '"+strOptionName+"' is not selected !");
						}							
					}
				else{
					
					blnIsProductConfigured = true;
					Log.debug("DEBUG: Component of the Configuration is Automatically selected by the System OR it is required and selected Automatically ::"+strOptionName);
				    }
				}
		    catch (Exception e)
	        {
		    	StringWriter stack = new StringWriter();
				e.printStackTrace(new PrintWriter(stack));
				Log.debug("DEBUG: The exception encountered while configuring product is: "+stack);
				blnIsProductConfigured = false;
				return blnIsProductConfigured;
	        }	

	        Log.debug("++++++++++++++++++++++++++++++++++++End of Configuration of Services+++++++++++++++++++++++++++++++++++++++++++");
	        Thread.sleep(5000);
	        
	        return blnIsProductConfigured;
 	     }
}
