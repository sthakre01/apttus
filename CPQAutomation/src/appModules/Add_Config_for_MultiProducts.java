 /*==============================================================
 Author     		:	Madhavi JN
 Class Name		 	: 	Add_Config_for_MultiProducts
 Purpose     		: 	Configuring the Multiple Products
 Date       		:	13/02/2017
 Version Information:	Version1.0
 PreCondition 		:	None
 Test Steps 		:	Configuring the Line Items for specific product
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
 #======================================================================
 */
package appModules;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.ConfigurationPageObjects;
import pageObjects.SalesForce.Configuration_NA;
import utility.Log;
import utility.Utils;
import utility.VerificationMethods;

public class Add_Config_for_MultiProducts extends VerificationMethods{
	
	public Boolean fn_Config_for_MultiProducts(WebDriver driver,WebDriverWait webWait, String strComponentName, 
			List<List<Map<String, String>>> lstmapResponse, String strLineItem,Map<String, String> mapProduct) throws Exception {  	
		
		//Purpose:- For Configuring Multiple Products
		//Inputs:- lstmapResponse,strLineItem and product map
		//============================================================================
	    //Step1:- Handling the specific condition to add all the Line items
		//Step2:- Handling the specific condition to add items specified in the Excel
		//Step3:- Capturing the Number of tables on the configuration page and checking for the presence of Lineitems
		//Step4:- Traversing all the Rows in a table and finding out the Lineitem
		//Step5:- Handling the Lineitems which are having Images to navigate further for configuration
		//Step6:- Comparing the text of both Lineitem and Rowtext
		//Step7:- Configuring the Attributes of the respective Lineitem
		//Step9:- Else condition to handle lineitems which are not present in the Apttus system
		//============================================================================
		
		String strXpath = null;
		Integer intCounter=0;
		Boolean blnAddAllLineItems=false;
		Boolean blnIsProductConfigured = false;		
	
    try {    
    	
    	Utils objUtils = new Utils();
		Add_Config_for_MultiProducts objConfigMultiProducts = new Add_Config_for_MultiProducts();
	    	
	   //Step1
	    if (mapProduct.get("LineItem_Name").equalsIgnoreCase("ALL")){   
	    	blnAddAllLineItems=objConfigMultiProducts.fn_Add_AllLineItems(lstmapResponse, driver, webWait);
	    	if (blnAddAllLineItems==true)
	    		Log.debug("DEBUG: All Line Items have been successfully configured.");
	    	else
	    		Log.debug("Error: problem occoured while configuring All Line Items.");
	    }
	    else{
	    	//Step2
	    	if ((lstmapResponse.toString().toLowerCase()).contains((mapProduct.get("LineItem_Name").trim().toLowerCase()))) {
			    String strBaseXpath="//div[@class='aptLeafProductsTable']/table";
			    List<WebElement>lstwebTables = driver.findElements(By.xpath(strBaseXpath));
			    //Step3
			    tableIteration:
			    for(Integer intTableNum=0; intTableNum<lstwebTables.size(); intTableNum++){
			   		List<WebElement>lstwebTableRows=lstwebTables.get(intTableNum).findElement(By.tagName("tbody"))
							.findElements(By.tagName("tr"));
			   	     //Step4
			   		for(Integer intRowNum=0; intRowNum<lstwebTableRows.size(); intRowNum++){ 
	    				lstwebTables = driver.findElements(By.xpath(strBaseXpath));
	        			lstwebTableRows=lstwebTables.get(intTableNum).findElement(By.tagName("tbody"))
	        															.findElements(By.tagName("tr"));
	        			if(!lstwebTableRows.get(intRowNum).getAttribute("innerText").isEmpty()){
	        				//Step5
		        			if(lstwebTableRows.get(intRowNum).getAttribute("innerHTML").
			        					contains("apttus_config2__Image_Configure")){
		        				    //splitting the lineitem name to match with group line item Name
			        				String[] arrsubLineItem = mapProduct.get("LineItem_Name").split("[()]");
			        				//Replacing the underscores in the string of Lineitem
									String strFinalLineitem = arrsubLineItem[1].replaceAll("-","").trim().toString();
									//Replacing the empty spaces in the string to nothing
									strFinalLineitem = strFinalLineitem.replaceAll(" ","").trim().toString();
									//Getting the row text of the respective Row where lineitem is found
									String[] arrRowtext = (lstwebTableRows.get(intRowNum).getAttribute("innerText")).trim().split("\\t");
									//Replacing with the Regular expression for removing special characters from the captured row text
									String strRowtext = arrRowtext[0].trim().replaceAll("[^(a-zA-Z0-9)]","").toString();
									//Replacing the underscores in the Rowtext
									strRowtext = strRowtext.replaceAll("-","").trim().toString();
									//Replacing the Keyword selection in the rowtext to match with the Lineitem
									strRowtext = strRowtext.replaceAll("Selections","").trim().toString();
									//Trimming the final string contained in the Rowtext
									strRowtext=strRowtext.trim();
									//Step6
				        			if((strFinalLineitem.trim().toLowerCase().contains(strRowtext.toLowerCase()))){
				        				
										strXpath = strBaseXpath+"/tbody/tr["+(intRowNum+1)+"]"+"//span/a";
										String strInnertext = driver.findElement(By.xpath(strXpath)).getAttribute("text");
										strInnertext = strInnertext.replaceAll("-","").trim().toString();
										strInnertext = strInnertext.replaceAll(" ","").trim().toString();
										strXpath = strBaseXpath+"/tbody/tr["+(intRowNum+1)+"]"+"//img[contains(@src,'apttus_config2__Image_Configure')]";
			   	    	    			WebElement webImgConfigure = driver.findElement(By.xpath(strXpath));
			   	    	    			webImgConfigure.click();
			   	    	    			Utils.fn_waitForSpinner(driver);
			   	    	    			strXpath = "//a[text()='"+mapProduct.get("LineItem_Name").trim()+"']/ancestor::tr[1]/td[1]//input";
										blnIsProductConfigured = objConfigMultiProducts.fn_ConfigureProducts(driver, webWait, strXpath, mapProduct.get("LineItem_Name"));
										//Step7
										objConfigMultiProducts.fn_ConfigureAttributes_IRE(driver, webWait, mapProduct.get("LineItem_Name"), lstmapResponse,mapProduct);
										strXpath = "//a[text()='"+mapProduct.get("Product_Name").trim()+"']";
										driver.findElement(By.xpath(strXpath)).click();
										Utils.fn_waitForSpinner(driver);
							    		intCounter++;
										if(blnIsProductConfigured == true)
							    			Log.debug("DEBUG: Line Item '"+mapProduct.get("LineItem_Name")+"' is available in the system and is successfully configured.");
							    		else
							    			Log.debug("ERROR: Line Item '"+mapProduct.get("LineItem_Name")+"' configuration is not successfull as reported earlier.");				    		
							        	objUtils.fn_takeScreenshotFullPage(driver);
							        	break tableIteration;							 
				        		  }
			        		  }	
		        		//Step8	
		        		else{
							strXpath = "//a[text()='"+mapProduct.get("LineItem_Name")+"']/ancestor::tr[1]/td[1]//input";	
							blnIsProductConfigured = objConfigMultiProducts.fn_ConfigureProducts(driver, webWait, strXpath, mapProduct.get("LineItem_Name"));
				    		intCounter++;
							if(blnIsProductConfigured == true)
				    			Log.debug("DEBUG: Line Item '"+mapProduct.get("LineItem_Name")+"' is available in the system and is successfully configured.");
				    		else
				    			Log.debug("ERROR: Line Item '"+mapProduct.get("LineItem_Name")+"' configuration is not successfull as reported earlier.");
							//Step7
							objConfigMultiProducts.fn_ConfigureAttributes_IRE(driver, webWait, mapProduct.get("LineItem_Name"), lstmapResponse,mapProduct);
				        	objUtils.fn_takeScreenshotFullPage(driver);	
				        	break tableIteration;
		        			}

	        			}
	        		}
			    	
			    }    		
	    	}
	    	//Step9
	    	else
	    	 Log.debug("ERROR: The user defined Line Item '"+mapProduct.get("LineItem_Name")+"' is not available in the system!");        	
	    }
     
    }
     catch (Exception e) {     
	    	StringWriter stack = new StringWriter();
			e.printStackTrace(new PrintWriter(stack));
			Log.debug("DEBUG: The exception in Validating the condition for Adding Multiple products is: "+stack);
        }
	return blnIsProductConfigured;	    
    }
	

    
	//Purpose :- function to add all the line items present in the system
	//Input:- driver,wait,List of Response->lstmapResponse
    //Output:- Parsed Response value
    public Boolean fn_Add_AllLineItems(List<List<Map<String, String>>> lstmapResponse,WebDriver driver,WebDriverWait webWait)
    {
    	Add_Config_for_MultiProducts objConfigMultiProducts = new Add_Config_for_MultiProducts();
    	Boolean blnIsAllLineItemConfig = false;
    	
    	try{ 
    	  	
    	   	for(List<Map<String, String>> lstmap: lstmapResponse){    	
	    	    for(Map<String, String> map : lstmap){    	    	    	        		 
	    	        for (Map.Entry<String, String> entry : map.entrySet()){    	          
	    	        	String strLineItemName = entry.getValue().toString().trim();
						String strXpath = "//a[text()='"+strLineItemName+"']/ancestor::tr[1]/td[1]//input";
						blnIsAllLineItemConfig=objConfigMultiProducts.fn_ConfigureProducts(driver, webWait, strXpath, strLineItemName);	  	
				    }
	    	    }
	    	}          	
    	}
    	catch (Exception exceptionResponse){  	
 	    	StringWriter stack = new StringWriter();
	    	exceptionResponse.printStackTrace(new PrintWriter(stack));
			Log.debug("DEBUG: There is exception in adding all Line Items recorded as: "+stack);   		
    	}  
    	return blnIsAllLineItemConfig;
    }  
    
	//Purpose :- function to add Limited number of line items present in the system
	//Input:- driver,wait,List of Response->lstmapResponse
    //Output:- Parsed Response value
    public Boolean fn_Add_Specific_LineItems(List<List<Map<String, String>>> lstmapResponse,WebDriver driver,WebDriverWait webWait)
    {
    	Add_Config_for_MultiProducts objConfigMultiProducts = new Add_Config_for_MultiProducts();
    	Integer intcounter=0;
    	Boolean blnIsSpecificLineItemConfig = false;
    	
    	try{
    	   	
    	   	for(List<Map<String, String>> lstmap: lstmapResponse){    	
			    for(Map<String, String> map : lstmap) {		   	    	        		 
					for (Map.Entry<String, String> entry : map.entrySet()){ 				 
						if (intcounter<2){							
						String strLineItemName = entry.getValue().toString().trim();
						String strXpath = "//a[text()='"+strLineItemName+"']/ancestor::tr[1]/td[1]//input";
						blnIsSpecificLineItemConfig=objConfigMultiProducts.fn_ConfigureProducts(driver, webWait, strXpath, strLineItemName);
						intcounter++;
						}
					}
			    }
	    	}      
	    	
    	}
    	catch (Exception exceptionResponse){   	
 	    	StringWriter stack = new StringWriter();
	    	exceptionResponse.printStackTrace(new PrintWriter(stack));
			Log.debug("DEBUG: There is exception in adding all Line Items recorded as: "+stack);
			blnIsSpecificLineItemConfig=false;
    	}   	
    return blnIsSpecificLineItemConfig;
    }
    
	//Purpose :- function to fetch the Response value
	//Input:- List of Response->lstmapResponse
    //Output:- Parsed Response value
    public String fn_fetchResponse(List<List<Map<String, String>>> lstmapResponse){
    
    	String strResponseValue="";
    	
    	try{ 
    	  	
	    	for(List<Map<String, String>> lstmap: lstmapResponse){    	    	        	 
	    	    for(Map<String, String> map : lstmap){     	       	        		 
	    	        for (Map.Entry<String, String> entry : map.entrySet()) {	    	                 	        	        	               						
	    	        	strResponseValue = entry.getValue().toString().trim();   		
				     }
	    	      }
	    	  }  	
    	   }
    	catch (Exception exceptionResponse){   	
 	    	StringWriter stack = new StringWriter();
	    	exceptionResponse.printStackTrace(new PrintWriter(stack));
			Log.debug("DEBUG: The exception in fetching the Response value is: "+stack);   		
    	 }   
		return strResponseValue;
     }
    
    
  //Purpose :- function to Configure the Line items of a product
  	//Input:- driver,wait,constructed Xpath->strXpath,Name of the Line item->strOptionName
      //Output:- Parsed Response value
  	public Boolean fn_ConfigureProducts(WebDriver driver,WebDriverWait webWait,String strXpath,String strOptionName) throws InterruptedException {
  	   	
  	   Boolean blnIsProductConfigured = false;
  	   Utils objUtils=new Utils();
  	   
  		 try{
  		 
  			Log.debug("++++++++++++++++++++++++++++++++++++Start of Configuration of Services+++++++++++++++++++++++++++++++++++++++++++++");		 
  	        //Identify the checkbox/radio button control			
  			Thread.sleep(5000);
  			WebElement webIdentifyInput = webWait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath(strXpath))));
  			Boolean blnIsSelected = webIdentifyInput.isSelected();	
  			if (blnIsSelected==false){	
  				try{
  					webIdentifyInput.click();	// to click the checkbox/radio button
  				}
  				catch (Exception exceptionElementNotClickable) {     
  					Utils.fn_scrollToElement(driver, webIdentifyInput);
  					objUtils.fn_takeScreenshot(driver);
  					webIdentifyInput.click();
  					StringWriter stack = new StringWriter();
  					exceptionElementNotClickable.printStackTrace(new PrintWriter(stack));
  					Log.debug("DEBUG: The exception encountered while configuring product is: "+stack);
  				 }	
  				Thread.sleep(10000);
  				Utils.fn_waitForSpinner(driver);
  				// to identify back the checked check-box/radio button webelement.
  				WebElement webSelectChkBox = driver.findElement(By.xpath(strXpath));					
  				Boolean blnResult = webSelectChkBox.isSelected();
  				if (blnResult==true){						
  					blnIsProductConfigured = true;
  					Log.debug("DEBUG: Component of the Configuration '"+strOptionName+"' is selected successfully !");
  				}
  				else{						
  					blnIsProductConfigured = false;
  					Log.debug("ERROR: Component of the Configuration '"+strOptionName+"' is not selected !");
  				  }							
  			}
  		else{				
  			blnIsProductConfigured = true;
  			Log.debug("DEBUG: Component of the Configuration is Automatically selected by the System OR it is required and selected Automatically ::"+strOptionName);
  		}
  	 }
      catch (Exception e) {     
      	StringWriter stack = new StringWriter();
  		e.printStackTrace(new PrintWriter(stack));
  		Log.debug("DEBUG: The exception encountered while configuring product is: "+stack);
  		blnIsProductConfigured = false;
  		return blnIsProductConfigured;
       }	
          Log.debug("++++++++++++++++++++++++++++++++++++End of Configuration of Services+++++++++++++++++++++++++++++++++++++++++++");
          Thread.sleep(5000);        
  	  return blnIsProductConfigured;
  	}
  	
    
	//Purpose :- function to Configure the Line items of a product
	//Input:- driver,wait,constructed Xpath->strXpath,Name of the Line item->strOptionName
    //Output:- Parsed Response value
	public Boolean fn_ConfigureProductsNA(WebDriver driver,WebDriverWait webWait,String strXpath,String strOptionName) throws InterruptedException {
	   	
	   Boolean blnIsProductConfigured = false;
	   Utils objUtils=new Utils();
	   
		 try{
		 
			Log.debug("++++++++++++++++++++++++++++++++++++Start of Configuration of Services+++++++++++++++++++++++++++++++++++++++++++++");		 
	        //Identify the checkbox/radio button control			
			Thread.sleep(5000);    
			WebElement webIdentifyInput = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(strXpath)));
			Utils.fn_scrollToElement(driver, webIdentifyInput);
			//WebElement webIdentifyInput = webWait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath(strXpath))));
			Boolean blnIsSelected = driver.findElement(By.xpath(strXpath+"/child::input")).isSelected();	
			if (blnIsSelected==false){	
				try{
					Utils.fn_scrollToElement(driver, webIdentifyInput);
					webIdentifyInput.click();	// to click the checkbox/radio button
					
				}
				catch (Exception exceptionElementNotClickable) {     
					Utils.fn_scrollToElement(driver, webIdentifyInput);
					objUtils.fn_takeScreenshot(driver);
					webIdentifyInput.click();
					StringWriter stack = new StringWriter();
					exceptionElementNotClickable.printStackTrace(new PrintWriter(stack));
					Log.debug("DEBUG: The exception encountered while configuring product is: "+stack);
				 }	
				Thread.sleep(10000);
				Utils.fn_waitForSpinner(driver);
				// to identify back the checked check-box/radio button webelement.
				WebElement webSelectChkBox = driver.findElement(By.xpath(strXpath+"/child::input"));					
				Boolean blnResult = webSelectChkBox.isSelected();
				if (blnResult==true){						
					blnIsProductConfigured = true;
					Log.debug("DEBUG: Component of the Configuration '"+strOptionName+"' is selected successfully !");
				}
				else{						
					blnIsProductConfigured = false;
					Log.debug("ERROR: Component of the Configuration '"+strOptionName+"' is not selected !");
				  }							
			}
		else{				
			blnIsProductConfigured = true;
			Log.debug("DEBUG: Component of the Configuration is Automatically selected by the System OR it is required and selected Automatically ::"+strOptionName);
		}
	 }
    catch (Exception e) {     
    	StringWriter stack = new StringWriter();
		e.printStackTrace(new PrintWriter(stack));
		Log.debug("DEBUG: The exception encountered while configuring product is: "+stack);
		blnIsProductConfigured = false;
		return blnIsProductConfigured;
     }	
        Log.debug("++++++++++++++++++++++++++++++++++++End of Configuration of Services+++++++++++++++++++++++++++++++++++++++++++");
        Thread.sleep(5000);        
	  return blnIsProductConfigured;
	}
   
    
	//Purpose :- function to Configure the Attributes of the Lineitem
	//Input:- driver,wait,LineitemName,Responsemap
    //Output:- Parsed Response value
    public Boolean fn_ConfigureAttributes_IRE(WebDriver driver,WebDriverWait webWait,String strLineitemName,
    		List<List<Map<String, String>>> lstmapResponse,Map<String, String> mapProduct) throws Exception{
    	
    	String strBaseXpath="";
    	Boolean blnAttributeConfigured=false;
      	
    	//TODO: Creating a Map which will contain ONLY Attributes & their Values for specific Product
    	Map<String, String> mapAttributeValue = new HashMap<String, String>();    	
    	Set<String> setKeys = mapProduct.keySet();
    	for(String key: setKeys) {   		
    		if(key.contains("_Validation") && !mapProduct.get(key).equals("NA")) {   			
    			//assuming that the 'Product_Information' holds the data for product as AttribName##Value
    			String[] arrAtrribValue = mapProduct.get(key).trim().split("##");
    			mapAttributeValue.put(arrAtrribValue[0], arrAtrribValue[1]);
    		}    			
    	}

    	strBaseXpath="//div[@class='aptLeafProductsTable']/table";
    	List<WebElement>lstwebTables = driver.findElements(By.xpath(strBaseXpath));
    	AttributeIteration:
    	for(Integer intTableNum=0; intTableNum<lstwebTables.size(); intTableNum++){
  		  
    		List<WebElement>lstwebTableRows=lstwebTables.get(intTableNum).findElement(By.tagName("tbody"))
    													.findElements(By.tagName("tr"));
    		for(Integer intRowNum=0; intRowNum<lstwebTableRows.size(); intRowNum++){ 
    		
    			if(blnAttributeConfigured==true){
    				
    				lstwebTables = driver.findElements(By.xpath(strBaseXpath));
        			lstwebTableRows=lstwebTables.get(intTableNum).findElement(By.tagName("tbody"))
        															.findElements(By.tagName("tr"));
    			}
    			if (lstwebTableRows.get(intRowNum).getAttribute("innerText").trim().toLowerCase()
	    			 						   .contains(strLineitemName.toLowerCase())){
    				//TODO:to handle the attributes which contains Image
        			if(lstwebTableRows.get(intRowNum).getAttribute("outerHTML").
        					contains("apttus_config2__Image_Attributes")){
        				String strImgXpath = strBaseXpath+"/tbody/tr["+(intRowNum+1)+"]//img[contains(@src,'apttus_config2__Image_Attributes')]";
        				driver.findElement(By.xpath(strImgXpath)).click();
        				Utils.fn_waitForSpinner(driver);
        				Thread.sleep(15000);
        				driver.switchTo().frame(driver.findElement(By.id("idAttributesIframe")));
        			    Boolean blhandleAttribwndw=fn_handleAttributesWindow(driver,webWait,mapAttributeValue);
        			    if (blhandleAttribwndw==true)
        			    	Log.debug("The Attributes for the Lineitem has been successfully configured ::"+strLineitemName);
        			    else
        			    	Log.debug("The Attributes for the Lineitem has not been configured ::"+strLineitemName);
        			    break AttributeIteration;
        			}
	    			 
	    			 //the below for loop identifies whether the attribute available has to be configured or otherwise. 
	    			 for (Map.Entry<String, String> attribute : mapAttributeValue.entrySet()) {
	    				 
	    				 if(blnAttributeConfigured==true){
	    					
	    					 lstwebTables = driver.findElements(By.xpath(strBaseXpath));
		   	    			 lstwebTableRows=lstwebTables.get(intTableNum).findElement(By.tagName("tbody"))
		   	    					 						.findElements(By.tagName("tr"));
	    				 }	   	    			 	    				 
	    				 //the below if loop is just-in-case loop to avoid iterations for misspelled attributes. 
	    				 if (lstwebTableRows.get(intRowNum).getAttribute("innerText").trim().toLowerCase()
	    						 						.contains(attribute.getKey().toLowerCase())) {

	    					 List<WebElement>lstwebTableRowCols = lstwebTableRows.get(intRowNum).findElements(By.tagName("td"));
    	   	    			 columnIteration:
	    	   	    		 for(Integer intColNum=0; intColNum<lstwebTableRowCols.size(); intColNum++){
	    	   	    			 
	    	   	    			 //governs if the webelements are to be refreshed; Yes ONLY if Attribute is configured.
	    	   	    			 if(blnAttributeConfigured==true){
	    	   	    				
	    	   	    				lstwebTables = driver.findElements(By.xpath(strBaseXpath));
	    	   	    				lstwebTableRows=lstwebTables.get(intTableNum).findElement(By.tagName("tbody"))
	    	   	    											.findElements(By.tagName("tr"));
	    	   	    				lstwebTableRowCols = lstwebTableRows.get(intRowNum).findElements(By.tagName("td"));
	    	   	    			 }
	    	   	    			 //this if loop identifies the attribute 'td' location to add the custom data.
	    	   	    			 if (lstwebTableRowCols.get(intColNum).getText().toLowerCase().contains(attribute.getKey().toLowerCase())){
	    		    		        	  				    	    		   		    
	    	   	    				 
	    	   	    				 String strInputBoxXpath = "((//div[@class='aptLeafProductsTable']/table)["+(intTableNum+1)+"]/tbody"
	    	   	    						 					+"/tr["+(intRowNum+1)+"]/td["+(intColNum+1)+"]//following::input)[1]";	    	   	    				 
	    	    	    			 WebElement webInputBox = driver.findElement(By.xpath(strInputBoxXpath));
	    	    	    			 webInputBox.clear();
	    	    	    			 Utils.fn_waitForSpinner(driver);
	    	    	    			 webInputBox = driver.findElement(By.xpath(strInputBoxXpath));
	    	    	    			 webInputBox.sendKeys(attribute.getValue());	    	   	    				
	    	    	    			 blnAttributeConfigured=true;
	    	    	    			 WebElement webIdentifyHeaderTag = webWait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//div[@class='title']"))));
									 //To identify the header tag when it is scrolled down
	    	    	    			 try{
										 webIdentifyHeaderTag.click();//to click the header tag to generate the spinner
									  }
									 catch (Exception e) {  
									//return to initial page position
										 JavascriptExecutor jsExecutor = (JavascriptExecutor)driver;
										 String strScrollPossition = "window.scrollTo(0,0)";
										 jsExecutor.executeScript(strScrollPossition);
										 Thread.sleep(3500);
										 webIdentifyHeaderTag = webWait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.xpath("//div[@class='title']"))));
										 webIdentifyHeaderTag.click();//to click the header tag to generate the spinner
									 }
	    	    	    			 //driver.findElement(By.xpath("//div[@class='title']")).click();
	    	    	    			 Utils.fn_waitForSpinner(driver);
	    	    	    			 break columnIteration;
	    	    	    			 //fallback logic for exiting this if loop.
	    	    	    			 //intcol = lstwebTableRowCols.size();
	    	   	    			 }
	    	   	    		 }
	    				 }
	    			 }   	    			
	    		 }
	    		 else {
	    			 
	    			 //TODO: Can be extended to have any DEFAULT values to be set as per need be!
	    		 }
    		}
    	}
    	//taking the snapshot of the page after the attribute values are added.
    	return blnAttributeConfigured;
    }
    
	//Purpose :- function to Configure the Attributes of the Lineitem which contains Image and opens window when clicked
	//Input:- driver,wait,mapAttributeValue->contained the attribute name and value
    //Output:- Boolean after entering the attribute values
    public Boolean fn_handleAttributesWindow(WebDriver driver,WebDriverWait webWait,Map<String, String> mapAttributeValue){

	 	Boolean blnIsEntered=false;
    	ConfigurationPageObjects objConfig = new ConfigurationPageObjects();

        try{
              
        	objConfig.fn_clickAttSaveBtn(driver, webWait).isEnabled();
        	for (Map.Entry<String, String> attribute : mapAttributeValue.entrySet()) {
        		String strKey = attribute.getKey();
        		//handle input and select box
        		try {
        			String strXpath = "//label[contains(text(),"+"'"+strKey+"'"+")]/following::input[1]";
            		driver.findElement(By.xpath(strXpath)).clear();
            		driver.findElement(By.xpath(strXpath)).sendKeys(attribute.getValue());
				} catch (Exception e) {
					String strXpath = "//label[contains(text(),'"+strKey+"')]/following::select[1]";
					new Select(driver.findElement(By.xpath(strXpath))).selectByValue(attribute.getValue());
				
				}
        		          		
        	}
        	objConfig.fn_clickAttSaveBtn(driver, webWait).click();
        	blnIsEntered=objConfig.fn_clickPricingBtn(driver, webWait).isEnabled();
        	Utils.fn_waitForSpinner(driver);
        }
	    catch (Exception e) { 
	    	blnIsEntered=false;
	    	StringWriter stack = new StringWriter();
			e.printStackTrace(new PrintWriter(stack));
			Log.debug("DEBUG: The exception in Entering the data for Attrbutes on the Window is: "+stack);
	    }   	
		return blnIsEntered;     
    }
  
	//Purpose :- function to Search the products
	//Input:- driver,wait,componentname->Name of the product to be searched
    //Output:- Searched response value
    public Boolean fn_SearchProducts(WebDriver driver,WebDriverWait webWait,String strComponentName){
    	
	 	String strEquipRefName="";	
	 	Boolean blnIsSearched=false;
    	ConfigurationPageObjects objConfig = new ConfigurationPageObjects();

        try{
         	objConfig.fn_SearchProducts(driver, webWait).sendKeys(strComponentName); 
        	objConfig.fn_clicksearchBtn(driver, webWait).click();
        	strComponentName=" "+strComponentName;
        	objConfig.fn_clickServiceName(driver, webWait, strComponentName).isEnabled();
        	objConfig.fn_clickServiceName(driver, webWait, strComponentName).click();   	
        	Thread.sleep(6000); 
        	strEquipRefName = strComponentName+"_Configuration";
        	objConfig.fn_setEquipReference(driver, webWait).sendKeys(strEquipRefName);
        	Thread.sleep(3500); 
        	objConfig.fn_clickNext(driver, webWait).click();
        	blnIsSearched=objConfig.fn_clickPricingBtn(driver, webWait).isDisplayed();
        	Thread.sleep(6000);  
        }
	    catch (Exception exceptionProductSearch) { 
	    	blnIsSearched=false;
	    	StringWriter stack = new StringWriter();
	    	exceptionProductSearch.printStackTrace(new PrintWriter(stack));
			Log.debug("DEBUG: The exception in Search and Adding Multiple products is: "+stack);
	    }   	
		return blnIsSearched;     
    }
    
	//Purpose :- function to Search the products
	//Input:- driver,wait,componentname->Name of the product to be searched
    //Output:- Searched response value
    public Boolean fn_SearchProductsNA(WebDriver driver,WebDriverWait webWait,String strComponentName){

	 	Boolean blnIsSearched=false;
	 	String strEquipRefName="";	
		Configuration_NA objConfigNA = new Configuration_NA();

        try{
        	objConfigNA.fn_SearchProducts(driver, webWait).sendKeys(strComponentName); 
        	objConfigNA.fn_SearchProducts(driver, webWait).sendKeys(Keys.ENTER);
        	Utils.fn_waitForProgressBar(driver); 
        	Utils.fn_waitForProgressBar(driver);
        	objConfigNA.fn_clickServiceName(driver, webWait, strComponentName).click();   	
        	Utils.fn_waitForProgressBar(driver); 
        	Utils.fn_waitForProgressBar(driver);
        	strEquipRefName = strComponentName+"_Configuration";
        	objConfigNA.fn_setEquipReference(driver, webWait).sendKeys(strEquipRefName);
        	Utils.fn_waitForProgressBar(driver); 
        	Thread.sleep(3500); 
        	blnIsSearched=objConfigNA.fn_clickGoToPricing(driver, webWait).isDisplayed();
        	Thread.sleep(6000);  
        }
	    catch (Exception exceptionProductSearch) { 
	    	blnIsSearched=false;
	    	StringWriter stack = new StringWriter();
	    	exceptionProductSearch.printStackTrace(new PrintWriter(stack));
			Log.debug("DEBUG: The exception in Search and Adding Multiple products is: "+stack);
	    }   	
		return blnIsSearched;     
    }
    
    
    public Boolean fn_ConfigureAttributes_NA(WebDriver driver,WebDriverWait webWait,String strLineitemName,
    		List<List<Map<String, String>>> lstmapResponse,Map<String, String> mapProduct) throws Exception{
    	
    	String strBaseXpath="";
    	Boolean blnAttributeConfigured=false;
    	Configuration_NA objConfigNA = new Configuration_NA();
		
		//TODO: Creating a Map which will contain ONLY Attributes & their Values for specific Product
    	Map<String, String> mapAttributeValue = new HashMap<String, String>();    	
    	Set<String> setKeys = mapProduct.keySet();
    	for(String key: setKeys) {   		
    		if(key.contains("_Validation") && !mapProduct.get(key).equals("NA")) {   			
    			//assuming that the 'Product_Information' holds the data for product as AttribName##Value
    			String[] arrAtrribValue = mapProduct.get(key).trim().split("##");
    			mapAttributeValue.put(arrAtrribValue[0], arrAtrribValue[1]);
    		}    			
    	}
    	
    	 for (Map.Entry<String, String> attribute : mapAttributeValue.entrySet()) {
			 if (attribute.getKey().equalsIgnoreCase("Monthly Commitment") || attribute.getKey().equalsIgnoreCase("Vendor Quote Reference") || attribute.getKey().equalsIgnoreCase("Sungard AS Monthly Cost") || attribute.getKey().equalsIgnoreCase("Sungard AS One Time Cost")
					 || attribute.getKey().equalsIgnoreCase("Total CapEx Cost") || attribute.getKey().equalsIgnoreCase("Total Monthly OpEx") || attribute.getKey().equalsIgnoreCase("Total One-Time Cost") || attribute.getKey().equalsIgnoreCase("User VPN Tunnels") || attribute.getKey().equalsIgnoreCase("Site-to-Site VPN Tunnels")
					 || attribute.getKey().equalsIgnoreCase("Total Rack Units Required for Eqpt")
					 ) {
				 objConfigNA.fn_setattribute(driver, webWait, attribute.getKey()).clear();
				 objConfigNA.fn_setattribute(driver, webWait, attribute.getKey()).sendKeys(attribute.getValue());
			}else if (attribute.getKey().equalsIgnoreCase("User Override")) {
				objConfigNA.fn_clickUserOverride(driver, webWait).click();
				Utils.fn_waitForProgressBar(driver);
				objConfigNA.fn_selectOverRide(driver, webWait, attribute.getValue()).click();
				Utils.fn_waitForProgressBar(driver);
			}
			else if (attribute.getKey().equalsIgnoreCase("Qty")) {
				objConfigNA.fn_setLineIteamQty(driver, webWait, strLineitemName).clear();
				objConfigNA.fn_setLineIteamQty(driver, webWait, strLineitemName).sendKeys(attribute.getValue());
				Utils.fn_waitForProgressBar(driver);
				
			}
			
    	 }
			 
    	
    	return blnAttributeConfigured;
    }
    
	public Boolean fn_Config_for_MultiProductsNA(WebDriver driver,WebDriverWait webWait, String strComponentName, 
			List<List<Map<String, String>>> lstmapResponse, String strLineItem,Map<String, String> mapProduct) throws Exception {  	
		
		//Purpose:- For Configuring Multiple Products
		//Inputs:- lstmapResponse,strLineItem and product map
		//============================================================================
	    //Step1:- Handling the specific condition to add all the Line items
		//Step2:- Handling the specific condition to add items specified in the Excel
		//Step3:- Capturing the Number of tables on the configuration page and checking for the presence of Lineitems
		//Step4:- Traversing all the Rows in a table and finding out the Lineitem
		//Step5:- Handling the Lineitems which are having Images to navigate further for configuration
		//Step6:- Comparing the text of both Lineitem and Rowtext
		//Step7:- Configuring the Attributes of the respective Lineitem
		//Step9:- Else condition to handle lineitems which are not present in the Apttus system
		//============================================================================
		
		String strXpath = null;
		Integer intCounter=0;
		Boolean blnAddAllLineItems=false;
		Boolean blnIsProductConfigured = false;
		Configuration_NA objConfigNA = new Configuration_NA();
	
    try {    
    	
    	Utils objUtils = new Utils();
		Add_Config_for_MultiProducts objConfigMultiProducts = new Add_Config_for_MultiProducts();
		Utils.fn_waitForProgressBar(driver);
		Utils.fn_scrollToElement(driver, objConfigNA.fn_clickProductOptions(driver, webWait));
		Utils.fn_waitForProgressBar(driver);
		objConfigNA.fn_clickProductOptions(driver, webWait).click(); 
		Utils.fn_waitForProgressBar(driver);	
		Utils.fn_waitForProgressBar(driver);
		Utils.fn_waitForProgressBar(driver);
		//For AWS products where LineItem selection is not required
		if ((mapProduct.get("LineItem_Name").contentEquals("NotRequired")) && 
				(mapProduct.get("Product_Name")).contains("AWS")){
			Utils.fn_waitForProgressBar(driver);
			/*Utils.fn_scrollToElement(driver, objConfigNA.fn_clickProductOptions(driver, webWait));
			Utils.fn_waitForProgressBar(driver);
			objConfigNA.fn_clickProductOptions(driver, webWait).click(); 
			Utils.fn_waitForProgressBar(driver);	
			Utils.fn_waitForProgressBar(driver);
			Utils.fn_waitForProgressBar(driver);*/
			if (mapProduct.get("Product_Name").contentEquals("Managed Cloud - AWS")){
				//Configure attribute of line items
				objConfigMultiProducts.fn_ConfigureAttributes_NA(driver, webWait, mapProduct.get("LineItem_Name"), lstmapResponse,mapProduct);
							
			}
			
			Thread.sleep(5000); 
			blnIsProductConfigured = objConfigNA.fn_clickAddMorePrdctsBtn(driver, webWait).isDisplayed();
		}
		else{
			   //Step1
		    if (mapProduct.get("LineItem_Name").equalsIgnoreCase("ALL")){   
		    	blnAddAllLineItems=objConfigMultiProducts.fn_Add_AllLineItems(lstmapResponse, driver, webWait);
		    	if (blnAddAllLineItems==true)
		    		Log.debug("DEBUG: All Line Items have been successfully configured.");
		    	else
		    		Log.debug("Error: problem occoured while configuring All Line Items.");
		    }
		    else{
		    	//Step2
		    	if ((lstmapResponse.toString().toLowerCase()).contains((mapProduct.get("LineItem_Name").trim().toLowerCase()))) {
		    		objConfigNA.fn_clickProductOptions(driver, webWait).click();
		    		strXpath = "//span[text()='"+mapProduct.get("LineItem_Name")+"']/preceding::input[1]/..";
		    		blnIsProductConfigured = objConfigMultiProducts.fn_ConfigureProductsNA(driver, webWait, strXpath, mapProduct.get("LineItem_Name"));
		    		intCounter++;
					if(blnIsProductConfigured == true)
		    			Log.debug("DEBUG: Line Item '"+mapProduct.get("LineItem_Name")+"' is available in the system and is successfully configured.");
		    		else
		    			Log.debug("ERROR: Line Item '"+mapProduct.get("LineItem_Name")+"' configuration is not successfull as reported earlier.");
					//Step7
					objConfigMultiProducts.fn_ConfigureAttributes_NA(driver, webWait, mapProduct.get("LineItem_Name"), lstmapResponse,mapProduct);
		        		
	    		}
		    	//Step9
		    	else
		    	 Log.debug("ERROR: The user defined Line Item '"+mapProduct.get("LineItem_Name")+"' is not available in the system!");        	
		    }
			
		}
    
    }
     catch (Exception ConfiguringMulprdctsNA) { 
    	    blnIsProductConfigured=false;
	    	StringWriter stack = new StringWriter();
	    	ConfiguringMulprdctsNA.printStackTrace(new PrintWriter(stack));
			Log.debug("DEBUG: The exception in Validating the condition for Adding Multiple products is: "+stack);
        }
	return blnIsProductConfigured;	    
    }
	

    
 }
	