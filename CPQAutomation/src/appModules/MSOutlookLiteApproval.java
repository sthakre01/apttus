package appModules;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.OutlookApproval;
import pageObjects.SalesForce.AutomateOutlookLiteApprovalProcessPO;
import utility.EnvironmentDetails;
import utility.Log;
import utility.VerificationMethods;


public class MSOutlookLiteApproval extends VerificationMethods{
	
	public boolean fn_AutomateOutlookApprovalProcess(WebDriver driver,WebDriverWait webWait,String strEmailDetails,String strTypeofFlow) throws Exception{
		
		boolean blnAutomateGmailApprovalProcess = false;
		OutlookApproval objOutlook = new OutlookApproval();
		AutomateOutlookLiteApprovalProcessPO objOutlookLite = new AutomateOutlookLiteApprovalProcessPO();
		
		
		String strchildWindow = null;
		String strParentWindow = null;
		String strEmailWindow = null;
		String strReviewWindow = null;
		String strNewproposalName = EnvironmentDetails.mapGlobalVariable.get("strNewproposalName").toString();

		webWait = new WebDriverWait(driver,30);
	    strParentWindow = driver.getWindowHandle();  // get the current window handle
	    //Gets the new window handle	  
	    Robot robot = new Robot();
	    robot.keyPress(KeyEvent.VK_CONTROL);
	    robot.keyPress(KeyEvent.VK_T);
	    robot.keyRelease(KeyEvent.VK_CONTROL);
	    robot.keyRelease(KeyEvent.VK_T);
	    Thread.sleep(2000);
		//get the frame for the Child window
		Set<String> strWindowHandles = driver.getWindowHandles();   		
		strWindowHandles.remove(strParentWindow);
		strchildWindow = strWindowHandles.iterator().next();		
		driver.switchTo().window(strchildWindow);
		driver.manage().deleteAllCookies();
		driver.navigate().to(EnvironmentDetails.strOutlookURL);
		
		try {    			
			objOutlookLite.fn_clickAcctLink(driver, webWait).click();
			
		} catch (Exception ExceptionNOAcctLink) {
			
				
					//Enter Outlook login Credential 
    				objOutlookLite.fn_setEmail(driver, webWait).sendKeys("suhas.thakre@sungardas.com");
        			objOutlookLite.fn_setEmail(driver, webWait).sendKeys(Keys.ENTER);
				
		}
		
		Thread.sleep(60000);
		//common code for both the approval and Docusign process
	    objOutlook.fn_clickSignoutImg(driver, webWait).click();
		Thread.sleep(5000);
		objOutlook.fn_clickSignoutBtn(driver, webWait).click();
		Thread.sleep(5000);
		
		
		//driver.navigate().to("https://outlook.office.com/owa/sungardas.com/");
		Thread.sleep(5000);	
		driver.findElement(By.xpath("(//div[@class='sch'])[5]/a/img")).click();
		Thread.sleep(5000);	
		driver.navigate().to("https://outlook.office.com/owa/sungardas.com/");
		Thread.sleep(5000);	
		
		
        if (strTypeofFlow.contentEquals("Approval")){       	
        	
        	Thread.sleep(30000);	
        	//Set IE driver
        	//System.setProperty("webdriver.ie.driver", EnvironmentDetails.IE_DRIVER_PATH);
        	WebDriver IEdriver = new InternetExplorerDriver();
        	IEdriver.manage().deleteAllCookies();
    		IEdriver.get(EnvironmentDetails.strOutlookURL);
    		
    		//Handle Automatic Outlook login 
    		try {    			
    			objOutlookLite.fn_clickAcctLink(IEdriver, webWait).click();
    			
    		} catch (Exception ExceptionNOAcctLink) {
    			
    				try {
    					//Enter Outlook login Credential 
	    				objOutlookLite.fn_setEmail(IEdriver, webWait).sendKeys("suhas.thakre@sungardas.com");
	        			objOutlookLite.fn_setEmail(IEdriver, webWait).sendKeys(Keys.ENTER);
					} catch (Exception e) {
						// Not Outlook lite version  
						Log.error("ERROR: Unable to Detect outlook lite version.");
						 assertTrue(false, "ERROR: Unable to Detect outlook lite version.");
					}
    		}
        	
    		Thread.sleep(10000);
    		
    		//Search proposal number 
    		objOutlookLite.fn_setSearchBar(IEdriver, webWait).clear();
    		objOutlookLite.fn_setSearchBar(IEdriver, webWait).sendKeys(strNewproposalName);
    		objOutlookLite.fn_clickSearchBtn(IEdriver, webWait).click();
    		Thread.sleep(3500);
    		
    		        	/*objOutlook.fn_clickMailListLink(driver, webWait).click();
			Thread.sleep(5000);
			//Search mail with given text 
			objOutlook.fn_clickSearchBar(driver, webWait).click();
			objOutlook.fn_setSearchBar(driver, webWait).sendKeys(strNewproposalName);;
			Thread.sleep(3500);
			objOutlook.fn_clickSearchBtn(driver, webWait).click();
			Thread.sleep(3500);
			
			//Get search mail list
			List<WebElement> lstSearchMail = driver.findElements(By.xpath("//div[@aria-label='Mail list']//div[@role='option']"));
			System.out.println("================================== Mail Count ========="+lstSearchMail.size());
				for (WebElement webElement : lstSearchMail) {
					
					//select each mail and approve 
					webElement.click();
					Thread.sleep(3500);
					objOutlook.fn_clickReplyallBtn(driver, webWait).click();
					Thread.sleep(3500);
					objOutlook.fn_setMsgBody(driver, webWait).sendKeys("APPROVE");
					Thread.sleep(3500);
					objOutlook.fn_clickSendBtn(driver, webWait).click();
					Thread.sleep(3500);
				}*/
       	}
		else{
			
			objOutlook.fn_clickMailListLink(driver, webWait).click();
			Thread.sleep(5000);
			//Search mail with given text 
			objOutlook.fn_clickSearchBar(driver, webWait).click();
			objOutlook.fn_setSearchBar(driver, webWait).sendKeys("Request for eSignatures. Please review and sign.");;
			Thread.sleep(3500);
			objOutlook.fn_clickSearchBtn(driver, webWait).click();
			Thread.sleep(3500);
			objOutlook.fn_clickMailListLink(driver, webWait).click();
			
			strEmailWindow = driver.getWindowHandle();
			//Docusign Process
			objOutlook.fn_clickReviewBtn(driver, webWait).click();
			
			//Capture review window id
			Thread.sleep(5000);		
		      for (String handle : driver.getWindowHandles()) {
	                 driver.switchTo().window(handle);}		
			Thread.sleep(5000);
			//objGmail.fn_clickIAcceptBtn(driver, webWait).click();
			Thread.sleep(500);		
			strReviewWindow = driver.getWindowHandle();
			Thread.sleep(5000);
			
			//Accept and Continue to sign document 
			if (strTypeofFlow.contentEquals("DocuSignProcessForOrder")){
				
				try {
					Thread.sleep(5000);
					objOutlook.fn_clickAgreeAndAccept(driver, webWait).click(); }          		
				catch(Exception excpEnvironment1){
					Log.debug("DEBUG: Continue Button is not present in the Docusign of Order");}		
			Thread.sleep(5000);
			objOutlook.fn_clickContinueBtn(driver, webWait).click();
			Thread.sleep(5000);}
			
			if (strTypeofFlow.contentEquals("DocuSignProcessFORGMSA")){
				
				// If AgreeAndAccept button is visible  
				try{
					
					objOutlook.fn_clickAgreeAndAccept(driver, webWait).click();
					Thread.sleep(5000);
					objOutlook.fn_clickContinueBtn(driver, webWait).click();
					Thread.sleep(5000);
					objOutlook.fn_clickNavigateBtn(driver, webWait).click();
					Thread.sleep(5000);
					WebElement svgObject = objOutlook.fn_clickSignBtn(driver, webWait);
					Actions builder = new Actions(driver);
					builder.click(svgObject).build().perform();
					//objGmail.fn_clickSignBtn(driver, webWait).click();
					Thread.sleep(5000);
				    }
			    catch(Exception excpContinueBtn){
			    
					try {			
						objOutlook.fn_clickContinueBtn(driver, webWait).click(); }          		
					catch(Exception excpContinueBtn1){
						Log.debug("DEBUG: Continue Button is not present in the Docusign of GMSA");}	
				Thread.sleep(5000);	
				objOutlook.fn_clickNavigateBtn(driver, webWait).click();
				Thread.sleep(3000);
				WebElement svgObject = objOutlook.fn_clickSignBtn(driver, webWait);
				Actions builder = new Actions(driver);
				builder.click(svgObject).build().perform();
				//objGmail.fn_clickSignBtn(driver, webWait).click();
				Thread.sleep(3000);			
			    }}	
			
			if (strTypeofFlow.contentEquals("DocuSignProcessForOrder")){
			driver.switchTo().frame(driver.findElement(By.id("signingIframe")));		
			objOutlook.fn_clickDragFrom(driver, webWait).click();
			WebElement webElementDragFrom = objOutlook.fn_clickDragFrom(driver, webWait);
			
			//Add signature on document 
			if (webElementDragFrom.isDisplayed())
				Log.debug("DEBUG: the frame is identified and reached to dragfrom button");
			else
				Log.debug("DEBUG: the frame is not identified and not reached to dragfrom button");
					
			WebElement webElementDragTo = objOutlook.fn_clickDragTo(driver, webWait);
			Actions builder = new Actions(driver);
			builder.dragAndDrop(webElementDragFrom, webElementDragTo).build().perform();
			Thread.sleep(10000);}
			
			//String strDocusignWindow = driver.getWindowHandle();	
			//To Handle Multiple windows
		    for (String handle : driver.getWindowHandles()) {
	                 driver.switchTo().window(handle);}	    
		   Thread.sleep(10000);   
		   objOutlook.fn_clickAdoptandSign(driver, webWait).click();   	   
		   driver.switchTo().defaultContent();   
		   driver.switchTo().window(strReviewWindow);  
		   Thread.sleep(5000);
		   objOutlook.fn_clickFinish(driver, webWait).click();
		   driver.close();
		   Thread.sleep(5000);
		   driver.switchTo().window(strEmailWindow);
			
		}
        
        //common code for both the approval and Docusign process
	    objOutlook.fn_clickSignoutImg(driver, webWait).click();
		Thread.sleep(5000);
		if (objOutlook.fn_clickSignoutBtn(driver, webWait).isDisplayed())
			blnAutomateGmailApprovalProcess=true;	
		objOutlook.fn_clickSignoutBtn(driver, webWait).click();
		Thread.sleep(5000);
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(strParentWindow);
   		return blnAutomateGmailApprovalProcess ;}
     

     }

	

