/*
 Author     		:	Madhavi JN.
 Class Name			: 	CreateLeadandConvertToOppty 
 Purpose     		: 	Purpose of this file is :
						1. To perform  CreateLeadandConvertToOppty functions.

 Date       		:	04/10/2016 
 Modified Date		:	27/12/2016	
 Version Information:	Version 2.0
 
 Version Changes 2.0:	1. Modified the Billing Infomration and Option Values in adding Multiple Options in LOV.
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/

package appModules;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.LeadCreationObjects;
import utility.EnvironmentDetails;
import utility.ExcelHandler;
import utility.Log;
import utility.Utils;
import utility.WriteExcel;
//import utility.WriteTextFiles;

public class CreateLeadandConvertToMultipleAcct {
	
	//TODO:: Multiple Account Creation
	
	public String fn_CreateLead_ConvertToMultipleAcct(WebDriver driver,WebDriverWait webWait,Map<String, Object> mapTcData,ArrayList<String> arrlistFileContents,int intRowtraverser) throws Exception
    {

		String strBillingInformation = arrlistFileContents.get(intRowtraverser+5);
		String strLeadCreationData = mapTcData.get("LeadCreationData").toString();
		String strConversionData = mapTcData.get("ConversionData").toString();
		String strAccountAddress = mapTcData.get("AccountAddress").toString();
		 
		String[] arrLeadData = strLeadCreationData.split("#");
		String[] arrConvData = strConversionData.split("#");
		String[] arrBillingData = strBillingInformation.split("#");
		String[] arrAddressData = strAccountAddress.split("#");
		
		String strEntityName ="";
		String strParentWindow="";
		String strEmailID="";
        String strShipTo="";
        String strBillTo="";
		
		String strCRname="";
		Utils objUtils = new Utils();
		String strLastName ="";
		String strFirstName="";
		
		Log.debug("DEBUG: Execution of function 'fn_CreateLead_ConvertToMultipleAcct' Started.");
		
		LeadCreationObjects objLeadCreation = new LeadCreationObjects();	
		CreateLeadandConvertToMultipleAcct objMultipleAccts = new CreateLeadandConvertToMultipleAcct();
		strShipTo=arrlistFileContents.get(intRowtraverser+7);
		strBillTo=arrlistFileContents.get(intRowtraverser+8);

		//++++++++++++++CreateLeadAndConvertToOppty+++++++++++++++++++++++++++++++++++++++++++++++++++++++
		objLeadCreation.fn_selectLeadsTab(driver, webWait).click();
		objLeadCreation.fn_clickNew(driver, webWait).click();
		objLeadCreation.fn_clickContinuebtn(driver, webWait).click();
		
		
		Date date = new Date() ;
		SimpleDateFormat dateFormat = new SimpleDateFormat("mmss");  
		
		strFirstName = (arrlistFileContents.get(intRowtraverser+1)+"_1_"+dateFormat.format(date));
		
		objLeadCreation.fn_selectFirstName(driver, webWait).selectByValue("Ms.");
		
		objLeadCreation.fn_setFirstName(driver, webWait).sendKeys(strFirstName);
		
		strLastName = (arrlistFileContents.get(intRowtraverser+2)+"_1_"+dateFormat.format(date));	
		
		objLeadCreation.fn_setLastName(driver, webWait).sendKeys(strLastName);
		
    	//TODO:: generating the New proposal name with timestamp to make the name unique for further validations
    	String strCompanyName = arrlistFileContents.get(intRowtraverser+3)+"_"+dateFormat.format(date);	
    	
		objLeadCreation.fn_setCompany(driver, webWait).sendKeys(strCompanyName);
	
		objLeadCreation.fn_setPhone(driver, webWait).sendKeys(arrLeadData[2]);
		
		//Environment Exception catched for if Country present and Selected.
		try {			
			objLeadCreation.fn_selectCountry(driver, webWait).selectByValue(arrLeadData[3]); }          		
		catch(Exception excpEnvironment){
			Log.debug("DEBUG: The country field is not present on the UI page of Leads");}	
		
		objLeadCreation.fn_selectLeadStatus(driver, webWait).selectByVisibleText(arrLeadData[4]);
		
		//Environment Exception catched for if Country present and Selected.
		try {			
			objLeadCreation.fn_selecApprovalStatus(driver, webWait).selectByVisibleText(arrLeadData[5]); }          		
		catch(Exception excpEnvironment){
			Log.debug("DEBUG: The Approval Status is not present on the UI page of Leads");}	
		
		objLeadCreation.fn_selectAcctType(driver, webWait).selectByVisibleText(arrlistFileContents.get(intRowtraverser));
		objLeadCreation.fn_selectIndustry(driver, webWait).selectByVisibleText(arrLeadData[7]);
		objLeadCreation.fn_clickSaveBtn(driver, webWait).click();
		objLeadCreation.fn_clickLeadConvertBtn(driver, webWait).click();
		Thread.sleep(8000);
		driver.switchTo().activeElement().sendKeys(Keys.ESCAPE);
		
		objLeadCreation.fn_selectAccountName(driver, webWait).selectByVisibleText("Create New Account: "+strCompanyName);
		String strOpptyName = arrlistFileContents.get(intRowtraverser+4)+"_"+dateFormat.format(date);
		objLeadCreation.fn_setOpptyName(driver, webWait).sendKeys(strOpptyName);
		objLeadCreation.fn_setSubject(driver, webWait).sendKeys(arrConvData[2]);
		objLeadCreation.fn_setDuedate(driver, webWait).click();
		objLeadCreation.fn_selectStatus(driver, webWait).selectByVisibleText(arrConvData[3]);
		objLeadCreation.fn_clickConvertBtn(driver, webWait).click();
		Thread.sleep(8000);
		
		//TODO :: Entity Name check
		strEntityName = objLeadCreation.fn_getEntityName(driver, webWait).getText();
		if (strEntityName.contentEquals("Sungard Availability Services (Ireland) Limited"))
			Log.debug("DEBUG: The entity Name is displayed on the Accounts page and matches with the expected Result :: "+strEntityName);
		else
			Log.debug("DEBUG: The entity Name displayed doesnt matches with the expected Result ::"+strEntityName);
     
	    strParentWindow = driver.getWindowHandle();	
		
		//TODO :: Enter the Address Information on Accounts page
		Actions actionStreetAddrs = new Actions(driver).doubleClick(objLeadCreation.fn_clickStreetAdress(driver, webWait));
		actionStreetAddrs.build().perform();
		Thread.sleep(5000);
		
		//TODO:: handle multiple windows
		for (String handle : driver.getWindowHandles()) {
               driver.switchTo().window(handle);}		
		Thread.sleep(5000);
		
		objLeadCreation.fn_setStreetAdress(driver, webWait).sendKeys(arrAddressData[0]);
		Thread.sleep(5000);
		
		objLeadCreation.fn_clickOkBtn(driver, webWait).click();
	            
	    driver.switchTo().window(strParentWindow);
		
		Actions actionCity = new Actions(driver).doubleClick(objLeadCreation.fn_clickcity(driver, webWait));
		actionCity.build().perform();
		Thread.sleep(5000);
		
		objLeadCreation.fn_setcity(driver, webWait).sendKeys(arrAddressData[1]);
		
		Actions actionCounty = new Actions(driver).doubleClick(objLeadCreation.fn_clickcounty(driver, webWait));
		actionCounty.build().perform();
		Thread.sleep(5000);
		
		objLeadCreation.fn_setcounty(driver, webWait).sendKeys(arrAddressData[2]);
		
		Actions actionPostCode = new Actions(driver).doubleClick(objLeadCreation.fn_clickPostCode(driver, webWait));
		actionPostCode.build().perform();
		Thread.sleep(5000);
		
		objLeadCreation.fn_setPostCode(driver, webWait).sendKeys(arrAddressData[3]);
		
		//Enter the Billing Information on Acct page which will be further used by Aria 
		Actions actionPaymntMthd = new Actions(driver).doubleClick(objLeadCreation.fn_clickPaymentMethod(driver, webWait));
		actionPaymntMthd.build().perform();
		Thread.sleep(3500);
		objLeadCreation.fn_selectPaymentMethod(driver, webWait).selectByValue(arrBillingData[0]);
		Thread.sleep(3500);
		Actions actionPaymnterm = new Actions(driver).doubleClick(objLeadCreation.fn_clickPaymentTerm(driver, webWait));
		actionPaymnterm.build().perform();
		Thread.sleep(3500);
		objLeadCreation.fn_selectPaymentTerm(driver, webWait).selectByValue(arrBillingData[1]);
		Thread.sleep(3500);
		Actions actionTaxExemptLevel = new Actions(driver).doubleClick(objLeadCreation.fn_clickTaxExemptLevel(driver, webWait));
		actionTaxExemptLevel.build().perform();
		Thread.sleep(3500);
		objLeadCreation.fn_selectTaxExemptLevel(driver, webWait).selectByValue(arrBillingData[2]);
		Thread.sleep(3500);
		Actions actionTaxExemptCertNumber = new Actions(driver).doubleClick(objLeadCreation.fn_clickTaxExemptCertificateNumber(driver, webWait));
		actionTaxExemptCertNumber.build().perform();
		Thread.sleep(3500);
		objLeadCreation.fn_setTaxExemptCertificateNumber(driver, webWait).sendKeys(arrBillingData[3]);
		Thread.sleep(3500);
		Actions actionTaxPayerID = new Actions(driver).doubleClick(objLeadCreation.fn_clickTaxPayerID(driver, webWait));
		actionTaxPayerID.build().perform();
		Thread.sleep(3500);
		objLeadCreation.fn_setTaxPayerID(driver, webWait).sendKeys(arrBillingData[4]);
		Thread.sleep(3500);
		Actions actionTaxPayerEndDate = new Actions(driver).doubleClick(objLeadCreation.fn_clickTaxPayerEndDate(driver, webWait));
		actionTaxPayerEndDate.build().perform();
		Thread.sleep(3500);
		objLeadCreation.fn_setTaxPayerEndDate(driver, webWait).sendKeys(objUtils.fn_getFutureDate(12, "dd/MM/yyyy"));
		Thread.sleep(3500);	
		objLeadCreation.fn_clickAcctDetailSave(driver, webWait).click();
		Thread.sleep(5000);
		
	
		
		//Edit the contact
		objLeadCreation.fn_clickEditContactLink(driver, webWait).click();
		Thread.sleep(5000);
		strEmailID = arrlistFileContents.get(intRowtraverser+6);
		
		objMultipleAccts.fn_EnterContactdetails(driver, webWait,strEmailID,arrAddressData);
		
		Thread.sleep(3500);
		//TODO:: Add New Address
		objLeadCreation.fn_clickNewAddressBtn(driver, webWait).click();
		Thread.sleep(5000);
		
		objMultipleAccts.fn_createContactandLinkAdress(driver, webWait,"TestAutoaddress_1",strEmailID,strShipTo,strBillTo,strLastName);
		
        //Add New customer reference
        objLeadCreation.fn_clickNewCustomerReferenceBtn(driver, webWait).click();
        strCRname = strCompanyName+"_CR";
        objLeadCreation.fn_setTierName(driver, webWait).sendKeys(strCRname);
        objLeadCreation.fn_clickSaveOnCustomerEditPage(driver, webWait).click();
        objLeadCreation.fn_clickAccountLink(driver, webWait).click();
        Thread.sleep(5000);
		
		//TODO:: Add new Contact and then link to address	
        objLeadCreation.fn_clickNewContact(driver, webWait).click();
        Thread.sleep(5000);
        objLeadCreation.fn_clickOpptyContBtn(driver, webWait).click();
        Thread.sleep(5000);
		objLeadCreation.fn_selectFirstName(driver, webWait).selectByValue("Ms.");
		strFirstName = (arrlistFileContents.get(intRowtraverser+1)+"_2_"+dateFormat.format(date));
		objLeadCreation.fn_setFirstName(driver, webWait).sendKeys(strFirstName);
        strLastName = (arrlistFileContents.get(intRowtraverser+2)+"_2_"+dateFormat.format(date));	
        objLeadCreation.fn_setContactLastName(driver, webWait).sendKeys(strLastName);
        
        objMultipleAccts.fn_EnterContactdetails(driver, webWait,strEmailID,arrAddressData);
        
		Thread.sleep(5000);
		
		//TODO:: Add New Address
		objLeadCreation.fn_clickNewAddressBtn(driver, webWait).click();
		Thread.sleep(5000);
		
		objMultipleAccts.fn_createContactandLinkAdress(driver, webWait,"TestAutoaddress_2",strEmailID,strShipTo,strBillTo,strLastName);
		
		Thread.sleep(8000);
		Log.debug("DEBUG: Execution of function 'fn_CreateLead_ConvertToMultipleAcct' Completed.");
		return strCompanyName;
    }
	
	//TODO:: Create Multiple contacts and link the contacts with address
	public void fn_createContactandLinkAdress(WebDriver driver,WebDriverWait webWait,String strAddressType,String strEmailID,String strShipTo,String strBillTo,String strLastName) throws InterruptedException{
		
		LeadCreationObjects objctsLead = new LeadCreationObjects();	
		Utils objUtils = new Utils();
		String strOptions="";
		String[] arrShipTo = strShipTo.split("#");
		String[] arrBillTo = strBillTo.split("#");
	
		Log.debug("DEBUG: Execution of function 'fn_createContactandLinkAdress' Started.");
		
		//Environment Exception catched for if Country present and Selected.
		try {
			objctsLead.fn_selectAddressStatus(driver, webWait).selectByVisibleText("Active"); }       		
		catch(Exception excpEnvironment){
			Log.debug("DEBUG: The Approval Status is not present on the UI page of Address Details page");}
		
		//TODO:: fillup the Adress feilds on Adress edit page
		objctsLead.fn_setAddress1(driver, webWait).sendKeys(strAddressType);
		objctsLead.fn_setCity(driver, webWait).sendKeys("Dublin");
		
		//Environment Exception catched for if Country present and Selected.
		try {			
			objctsLead.fn_selectAddressCounty(driver, webWait).selectByVisibleText("Dublin 2"); }       		
		catch(Exception excpEnvironment){
			Log.debug("DEBUG: The Address County is not present on the UI page of Address Details page");}
		
		objctsLead.fn_selectCountryOnAddressEditpage(driver, webWait).selectByVisibleText("Ireland");
		objctsLead.fn_setPostalCodeOnAddressEditpage(driver, webWait).sendKeys("D2");
		
		if (strAddressType.contains("TestAutoaddress_1"))
		{
		//Code to Add Multiple Options
		List<WebElement> lstAllOptionsAdd  = objctsLead.fn_selectMultipleOnAddressEditpage(driver, webWait).getOptions(); 
		for(WebElement webElement : lstAllOptionsAdd ){
			strOptions = webElement.getText();
			for (int i=0;i<arrShipTo.length;i++)
			     if (strOptions.equalsIgnoreCase(arrShipTo[i]))
			    	 webElement.click();}
		}
		else
		{
		//Code to Add Multiple Options
		List<WebElement> lstAllOptionsAdd  = objctsLead.fn_selectMultipleOnAddressEditpage(driver, webWait).getOptions(); 
		for(WebElement webElement : lstAllOptionsAdd ){
			strOptions = webElement.getText();
			for (int i=0;i<arrBillTo.length;i++)
			     if (strOptions.equalsIgnoreCase(arrBillTo[i]))
			    	 webElement.click();}
			
		}
		
		objctsLead.fn_clickImgAddOnAddressEditPage(driver, webWait).click();
		
		Thread.sleep(500);
        objctsLead.fn_clickSaveAddOnAddressEditPage(driver, webWait).click();
        Thread.sleep(5000);
       
        //TODO::New address Contact page      
        objctsLead.fn_clickContactOnAddressBtnOnContactEditPage(driver, webWait).click();
        Thread.sleep(5000);
        
        //TODO::fill up the GMSA Form details
	    objUtils.fn_HandleLookUpWindow(driver, webWait,"ContactLukUpWindwOnAdCtctEdtPage",strLastName);
	    
		if (strAddressType.contains("TestAutoaddress_1"))
		{
		List<WebElement> lstAllOptionsAddUser  = objctsLead.fn_selectUsedForOnAddressContactEditpage(driver, webWait).getOptions(); 
		for(WebElement webElement : lstAllOptionsAddUser ){
			strOptions = webElement.getText();
			for (int i=0;i<arrShipTo.length;i++)
			    if (strOptions.equalsIgnoreCase(arrShipTo[i]))
			      webElement.click();}
		}
		else
	    {
		List<WebElement> lstAllOptionsAddUser  = objctsLead.fn_selectUsedForOnAddressContactEditpage(driver, webWait).getOptions(); 
		for(WebElement webElement : lstAllOptionsAddUser ){
			strOptions = webElement.getText();
			for (int i=0;i<arrBillTo.length;i++)
			    if (strOptions.equalsIgnoreCase(arrBillTo[i]))
			      webElement.click();}
		}
		
        //Code to remove the Multiple Options from LOV
		objctsLead.fn_clickImgAddOnAddressEditPage(driver, webWait).click();
		
		Thread.sleep(500);
        objctsLead.fn_setEmailOnAdCtctEdtPage(driver, webWait).sendKeys(strEmailID);
        objctsLead.fn_clickSaveOnAdCtctEdtPage(driver, webWait).click();  
        objctsLead.fn_clickContactLnkSaveOnAdCtctEdtPage(driver, webWait).click(); 
        Thread.sleep(5000);
        objctsLead.fn_clickAccountNameLink(driver, webWait).click();
        Thread.sleep(15000);
        
        Log.debug("DEBUG: Execution of function 'fn_createContactandLinkAdress' Completed.");
		
	}
	
	public void fn_EnterContactdetails(WebDriver driver,WebDriverWait webWait,String strEmailID,String[] arrAddressData) throws InterruptedException{
			
			Log.debug("DEBUG: Execution of function 'fn_EnterContactdetails' Started.");
		    LeadCreationObjects objLeadCreation = new LeadCreationObjects();
	        objLeadCreation.fn_setCntactDecisionRole(driver, webWait).sendKeys(arrAddressData[5]);
			objLeadCreation.fn_setEmailOnContactPage(driver, webWait).sendKeys(strEmailID);
			objLeadCreation.fn_setCntactStreetAdress(driver, webWait).sendKeys(arrAddressData[0]);
			objLeadCreation.fn_setCntactCity(driver, webWait).sendKeys(arrAddressData[1]);
			objLeadCreation.fn_selectCntactCounty(driver, webWait).selectByValue(arrAddressData[2]);
			objLeadCreation.fn_setCntactPostalCode(driver, webWait).sendKeys(arrAddressData[3]);
			objLeadCreation.fn_selectCntactCountry(driver, webWait).selectByValue(arrAddressData[4]);
			objLeadCreation.fn_clickSaveOnContactPage(driver, webWait).click();
			Thread.sleep(2000);
			objLeadCreation.fn_clickAccountNameLink(driver, webWait).click();
			
			try {			
				objLeadCreation.fn_clickAccountHierarchyLink(driver, webWait).click(); 
				Thread.sleep(1000);}          		
			catch(Exception excpEnvironment){
				Log.debug("DEBUG: The AccountHierarchy Link is not present on the UI page of Accounts");}
			Log.debug("DEBUG: Execution of function 'fn_EnterContactdetails' Completed.");
	}
	
	
	public Boolean fn_MultipleAccountCreation(WebDriver driver,WebDriverWait webWait, Map<String, Object> mapMultipleAcct, String strAccountCreationSheet) throws InterruptedException{
		
		CreateLeadandConvertToMultipleAcct objLeadMultipleAcctCreation = new CreateLeadandConvertToMultipleAcct();
		//EnvironmentDetails objEnvironment = new EnvironmentDetails();
		//WriteTextFiles objTxtFile = new WriteTextFiles();
		Utils objUtils = new Utils();
	    WriteExcel objWriteExcel = new WriteExcel();
		Boolean blnResult=false;	
	    int intRowtraverser=0;
	    String strAcctName="";
	    Map<String, Object> mapOutputData = new HashMap<String, Object>();
	    List<Map<String, Object>> lisOutputData = new ArrayList<>();
	    List<List<Map<String, Object>>> lstmapOutputData = new ArrayList<>();
		
		ArrayList<String> lstAcctNames = new ArrayList<String>();						
		try{

			Log.debug("DEBUG: Execution of function 'fn_MultipleAccountCreation' Started.");
			
			//TODO : Read Test Excel
			//String strPathOfFile = mapMultipleAcct.get("PathOfFile").toString();			 
			File fileConfigValidations = new File(EnvironmentDetails.Path_DataSheet);
			ArrayList<String> arrlistFileContents = ExcelHandler.readExcelFileAsArray(fileConfigValidations,strAccountCreationSheet);		   
			
			//TODO: Now Traverse the ArrayList for Excel File Read to find the item from product Information
				for(int intCnt = 0 ; intCnt < arrlistFileContents.size(); intCnt +=9)
				{				 
					
					intRowtraverser=intCnt;
					
					strAcctName=objLeadMultipleAcctCreation.fn_CreateLead_ConvertToMultipleAcct(driver, webWait, mapMultipleAcct,arrlistFileContents,intRowtraverser);
					
					lstAcctNames.add(strAcctName);
					        
				} 
				
				
			 //blnResult=objTxtFile.fn_writeAcctName(lstAcctNames,strPathOfFile);
			
			
			 //Save Account name in global variable map 
			 EnvironmentDetails.mapGlobalVariable.put("strAcctName", strAcctName);
			 Log.debug("DEBUG: Account Name save in global variable map with key 'strAcctName'.");
			 
			//TODO:: Capture Screenshot for Account 
			 objUtils.fn_takeScreenshotFullPage(driver);
			 
			//TODO:: Write account name in output file 
			 mapOutputData.put("Account Name", strAcctName);
			 lisOutputData.add(mapOutputData);
			 lstmapOutputData.add(lisOutputData);
			 objWriteExcel.fn_WriteExcel(lstmapOutputData, EnvironmentDetails.strFQNFileNameWrite, "Account_Name");
			 
		}catch (Exception e)
           {					    	 
	    	StringWriter stack = new StringWriter();
			e.printStackTrace(new PrintWriter(stack));
			Log.debug("DEBUG: The exception in Generating Multiple Accounts is: "+stack);
	        }
		
		Log.debug("DEBUG: Execution of function 'fn_MultipleAccountCreation' Completed.");
		blnResult = true;
		return blnResult;
	
	}


	
}
