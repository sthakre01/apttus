package appModules;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.Account;
import pageObjects.SalesForce.Proposal;
import utility.Log;


public class AccountSearch {
	
	public boolean fn_AccountSearch(WebDriver driver,WebDriverWait webWait,String strSearchItem
										,String strOpptyName,String strTestCaseName,String strCurrency) throws Exception
    {
			
  		Account objAcctpage = new Account(); 
  		Proposal objProposal = new Proposal();
  		boolean blnAccountSearch        = false;   
		
        try
        {
        	
        	//WebElement webElement = null;
        	String strTabletext=null;
        	objAcctpage.fn_setSearchField(driver, webWait).sendKeys(strSearchItem);       	
        	objAcctpage.fn_clickSearchBtn(driver, webWait).click();
        	Thread.sleep(3000);
        	if (driver.findElement(By.partialLinkText(strSearchItem)).isDisplayed()){
         		driver.findElement(By.partialLinkText(strSearchItem)).click();
         		blnAccountSearch=true;
        		Log.info("Opportunity link is clicked Successfully"+strSearchItem);}       	
        	else{          
        		Log.info("Unable to click the Opportunity link "+strSearchItem);}      	  	
        	//to find the Customer Reference Object
        	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", objAcctpage.fn_getCustomerReferenceTable(driver, webWait));
			Thread.sleep(1000);
			strTabletext = objAcctpage.fn_getCustomerReferenceTable(driver, webWait).getText();
			//Validating whether the customer Reference table has Records or not
        	if(strTabletext.contains("No records to display")){
				Log.info("Customer Reference is not linked to Account so creating a new one !!");
				objAcctpage.fn_clickNewCustReferenceBtn(driver, webWait).click();
				try {        			
					objAcctpage.fn_setCustomerReferenceName(driver, webWait, 0).sendKeys("ApptusCustRefAutomation");}
				catch(Exception excpEnvironment){	        			
					objAcctpage.fn_setCustomerReferenceName(driver, webWait, 1).sendKeys("ApptusCustRefAutomation");}

        		objAcctpage.fn_selCurrency(driver, webWait).selectByVisibleText(strCurrency);
        		objAcctpage.fn_clickSaveCustRefPage(driver, webWait).click();
            	objAcctpage.fn_setSearchField(driver, webWait).sendKeys(strSearchItem);       	
            	objAcctpage.fn_clickSearchBtn(driver, webWait).click();
            	Thread.sleep(3000);           	
	            	if (driver.findElement(By.partialLinkText(strSearchItem)).isDisplayed()){
	             		driver.findElement(By.partialLinkText(strSearchItem)).click();
	            		Log.info("Opportunity link is clicked Successfully"+strSearchItem);}       	
	               else{          
	            		Log.info("Unable to click the Opportunity link "+strSearchItem);}}
        	else{
        		Log.info("Customer Reference is already linked to Account "+strTabletext);}
 
            //click on the Opportunity displayed on oppty
  			WebElement elemntOpptyName= webWait.until(ExpectedConditions.elementToBeClickable(By.partialLinkText(strOpptyName)));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elemntOpptyName);
			Thread.sleep(1000);	
			elemntOpptyName.click();
			Thread.sleep(3000);	
        	if ((objProposal.fn_clickProposalButton(driver, webWait)).isDisplayed())
        	Log.info("OpportunityName link is clicked Successfully"+strOpptyName);
        	else
           	Log.info("Unable to click on the OpportunityName link "+strOpptyName);
        	//Utils.takeScreenshot(driver, strTestCaseName);
        }
        catch (Exception ExceptionSearchAcct)
        {
            
            Log.error("ERROR: Unable to Perform Account Search with exception reported as: "+ExceptionSearchAcct.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionSearchAcct.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_AccountSearch method is: "+stack);

        }
		return blnAccountSearch;
    }

}
