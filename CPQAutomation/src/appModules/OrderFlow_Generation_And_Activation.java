/*
 Author     		:	Madhavi
 Class Name			: 	OrderFlow_Generation_And_Activation 
 Purpose     		: 	Purpose of this file is :
						1. TO Generate and activate order agreement. 

 Date       		:	26/12/2016 
 Modified Date		:		
 Modified By		: 	
 Version Information:	Version 1.0
 						
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/
package appModules;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.AfterApproval;
import pageObjects.SalesForce.OrderFlow;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import pageObjects.SalesForce.Account;
import utility.EnvironmentDetails;
import utility.Log;
import utility.Utils;
import utility.VerificationMethods;
import utility.WriteExcel;


public class OrderFlow_Generation_And_Activation extends VerificationMethods {
	
	/*
	Author: Suhas Thakre
	Function fn_OrderFlow_Generation_And_Activation: Generate and activate order agreement.
	Inputs: 
	Outputs: Boolean response to check success or failure of function .
	 */
	public boolean fn_OrderFlow_Generation_And_Activation(WebDriver driver,WebDriverWait webWait)throws Exception
	{

		//TODO:: Generate and activate order agreement.  
		/************************************************************************************************/
			//Step 1: Activate GMSA agreement. 
			//Step 2: Generate, present and accept proposal.
			//Step 3: Create Order line agreement.
			//Step 4: Generate supporting document.
			//Step 5: Generate Order agreement.
			//Step 6: Send order for review.
			//Step 7: Send order for esignature.

		/************************************************************************************************/
		Utils objUtils = new Utils();
		WriteExcel objWriteExcel = new WriteExcel();
		Boolean blnIsAlertPresent=false;
		Boolean blnOrderFlwGenerationAndActivation=false;
		//String strAgmntNumber ="";
		//String strMSAStatus="";
		OrderFlow objOrderFlow = new OrderFlow();
		AfterApproval objAfterPO = new AfterApproval();
		//Account objacctpage = new Account();
		//strAgmntNumber= EnvironmentDetails.mapGlobalVariable.get("strMSA_AgrmntNumber").toString();
		//String strMSA_AgrmntName = EnvironmentDetails.mapGlobalVariable.get("strMSA_AgrmntName").toString();
		//String strNewproposalName = EnvironmentDetails.mapGlobalVariable.get("strNewproposalName").toString();
		//strAgmntNumber=strAgmntNumber.replace(".", "#");
		//String[] arrFinalAgrmntNumber= strAgmntNumber.split("#");
		String strAgrmntName ="";
		String strAgrmntNum = "";
		String strUniqueKey = "";
		Map<String, Object> mapOutputData = new HashMap<String, Object>();
		List<Map<String, Object>> lisOutputData = new ArrayList<>();
		List<List<Map<String, Object>>> lstmapOutputData = new ArrayList<>();
		MSAGeneration_And_Activation objMSAGeneration = new MSAGeneration_And_Activation();
		
		
		try
		{
		Log.debug("DEBUG: Execution of function 'fn_OrderFlow_Generation_And_Activation' Started.");
		
		
		//Step 1
		fn_ActivateGMSA(driver, webWait);
				
		
		for (int i=0; i<=2;i++ ){
		driver.navigate().refresh();
		Thread.sleep(2000);
		}
		
		//Step 2
		objMSAGeneration.fn_GenerateAndPresentProposal(driver, webWait);
		
		//Step 3
		Thread.sleep(3500);
	    objOrderFlow.fn_clickCrtAgmntLinItmsBtn(driver, webWait).click();
	    Thread.sleep(4000);
	    objOrderFlow.fn_clickContinueBtn(driver, webWait).click();
	    Thread.sleep(1000);
	    objUtils.fn_HandleLookUpWindow(driver, webWait,"Address Lookup (New Window)","LAC");
	    objOrderFlow.fn_clickOrderFrmSaveBtn(driver, webWait).click();
	    Thread.sleep(4000);
	    
	    //Step 4
	    objOrderFlow.fn_clickGenerateSpprtDocsBtn(driver, webWait).click();
	    Thread.sleep(1000);
	    objOrderFlow.fn_clickWaterMark(driver, webWait).click();
	    Thread.sleep(1000);
	    
	    
		    //Handle template
		    try {
		    	//select new aptus test template
			    objOrderFlow.fn_clickSpecificTempOnOrderFrm(driver, webWait, "New Apttus Test").click();
			} catch (Exception e) {
				// Ireland Order Form
				objOrderFlow.fn_clickSpecificTempOnOrderFrm(driver, webWait, "Ireland Order Form").click();
				
			}
	    
	    Thread.sleep(4000);
	    
	    //Step 5
	    objOrderFlow.fn_clickOrdrFrmGnrtBtn(driver, webWait).isEnabled();
	    objOrderFlow.fn_clickOrdrFrmGnrtBtn(driver, webWait).click();
	    Thread.sleep(80000);
	    
	    try{   
		    Alert alert = driver.switchTo().alert();
		    Log.info("the message thrown from alert is :: "+alert.getText());
		    alert.accept();
		    blnIsAlertPresent=true;
	    	  }catch(Exception e){ 
	    		  Log.info("unexpected alert is not not present");   
	    	  }
	    
	    if (blnIsAlertPresent==false){
		WebElement elemntReturnBtn1= webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@value='Return' and @type='submit']")));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elemntReturnBtn1);
		Thread.sleep(1000);	
		elemntReturnBtn1.click();}
		
	    //capture Agreement Name
	    strAgrmntName = objAfterPO.fn_getAgrmntName(driver, webWait).getText();
	    strAgrmntNum = objAfterPO.fn_getAgrmntNumber(driver, webWait).getText();
	    
	    //Save Agreement Name in global variable map 
	    EnvironmentDetails.mapGlobalVariable.put("strAgrmntName", strAgrmntName);
	    Log.debug("DEBUG: Agreement Name save in global variable map with key 'strAgrmntName'.");
	    
	    //Save Agreement Number in global variable map 
	    EnvironmentDetails.mapGlobalVariable.put("strAgrmntNum", strAgrmntNum);
	    Log.debug("DEBUG: Agreement Name save in global variable map with key 'strAgrmntNum'.");
	    
	    //TODO:: Capture Screenshot for New offering 
	    objUtils.fn_takeScreenshotFullPage(driver);
	    
	    //TODO:: Write Agreement Details in output file 
	    mapOutputData.put("Agreement Name", strAgrmntName);
	    mapOutputData.put("Agreement Number", strAgrmntNum);
	    lisOutputData.add(mapOutputData);
	    lstmapOutputData.add(lisOutputData);
	    objWriteExcel.fn_WriteExcel(lstmapOutputData, EnvironmentDetails.strFQNFileNameWrite, "Agreement_Details");
	    
        Thread.sleep(8000);
        //Step 6
	    objOrderFlow.fn_clickSendForReview(driver, webWait).click();
	    Thread.sleep(4000);
	    objOrderFlow.fn_clickSelectAttachment(driver, webWait).click();
	    objOrderFlow.fn_clickNextBtn(driver, webWait).click();
	    //Select Email Template
	    objOrderFlow.fn_clickEmailTemplate(driver, webWait).click();
	    Thread.sleep(4000);
	    //objOrderFlow.fn_clickNextBtnEmailTemplate(driver, webWait).click();
	    
		WebElement elemntNextBtn1= objOrderFlow.fn_clickNextBtnEmailTemplate(driver, webWait);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elemntNextBtn1);
		Thread.sleep(1000);	
		elemntNextBtn1.click();
	    
	    Thread.sleep(4000);
	    //Send email with sungardas template
	    objUtils.fn_HandleLookUpWindow(driver, webWait,"Reports To Lookup (New Window)","Apptus");
	    Thread.sleep(1000);
	    objOrderFlow.fn_clickEmailSendBtn(driver, webWait).click();
	    
	    //Step 7
	    objOrderFlow.fn_clickESignatureBtn(driver, webWait).click();
	    objOrderFlow.fn_clickAddRecipientsBtn(driver, webWait).isEnabled();
	    objOrderFlow.fn_clickAddRecipientsBtn(driver, webWait).click();
	    Thread.sleep(1000);
	    objOrderFlow.fn_setNameForRecipient(driver, webWait).sendKeys("madhavi");
	    Thread.sleep(500);
	    objOrderFlow.fn_setEmailForRecipient(driver, webWait).sendKeys(EnvironmentDetails.mapEnvironmentDetails.get("Outlook_UserID"));
	    Thread.sleep(500);
	    objOrderFlow.fn_clickFinalizeDocuSignBtn(driver, webWait).click();
	    new WebDriverWait(driver,240).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//span[text()='Processing eSignature request']")));
	    objOrderFlow.fn_clickSendBtn(driver, webWait).isEnabled();
	    strUniqueKey = driver.getCurrentUrl().trim();
	    strUniqueKey = strUniqueKey.replace("https://appdemo.docusign.com/prepare/", "");
	    strUniqueKey = strUniqueKey.replace("/add-fields", "");
	    String[] arrUniqueKey = strUniqueKey.split("-");
	    //Save GMSA email request UniqueKey in global variable map 
	    EnvironmentDetails.mapGlobalVariable.put("strOrderUniqueKey", arrUniqueKey[0].trim());
	    Log.debug("DEBUG: Order email request UniqueKey save in global variable map with key 'strOrderUniqueKey' is: "+arrUniqueKey[0].trim());
	    Thread.sleep(5000);
	    objOrderFlow.fn_clickSendBtn(driver, webWait).click();
	    	try {
		    	objOrderFlow.fn_clickWithoutFieldsBtn(driver, new WebDriverWait(driver,15)).isEnabled();
		    	objOrderFlow.fn_clickWithoutFieldsBtn(driver, new WebDriverWait(driver,15)).click();
			} catch (Exception e) {
				Log.debug("DEBUG: Send Without Fields button not present.");
			}
	    	blnOrderFlwGenerationAndActivation= true;
		}
	    catch (Exception ExceptionOrderFlow) {
			Log.error("ERROR: Unable to Generate Order with exception reported as: "+ExceptionOrderFlow.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionOrderFlow.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_OrderFlow_Generation_And_Activation method is: "+stack);
           
		}
	    Log.debug("DEBUG: Execution of function 'fn_OrderFlow_Generation_And_Activation' Completed.");
	    return blnOrderFlwGenerationAndActivation;
		
	}

	
	/*
	Author: Suhas Thakre
	Function fn_OrderFlowGenerateActivateAppl: Generate and activate order agreement.
	Inputs: 
	Outputs: Boolean response to check success or failure of function .
	 */
	public boolean fn_OrderFlowGenerateActivateAppl(WebDriver driver,WebDriverWait webWait, String strSignFrom)throws Exception
	{

		//TODO:: Generate and activate order agreement.  
		/************************************************************************************************/
			//Step 1: Generate, present and accept proposal.
			//Step 2: Create Order line agreement.
			//Step 3: Generate supporting document.
			//Step 4: Set GMSA singed document.
			

		/************************************************************************************************/
		//Utils objUtils = new Utils();
		//WriteExcel objWriteExcel = new WriteExcel();
		MSAGeneration_And_Activation objMSAGeneration_And_Activation = new MSAGeneration_And_Activation();
		Boolean blnIsAlertPresent=false;
		Boolean blnOrderFlwGenerationAndActivation=false;
		//String strAgmntNumber ="";
		//String strMSAStatus="";
		OrderFlow objOrderFlow = new OrderFlow();
		AfterApproval objAfterPO = new AfterApproval();
		//Account objacctpage = new Account();
		//strAgmntNumber= EnvironmentDetails.mapGlobalVariable.get("strMSA_AgrmntNumber").toString();
		//String strMSA_AgrmntName = EnvironmentDetails.mapGlobalVariable.get("strMSA_AgrmntName").toString();
		//String strNewproposalName = EnvironmentDetails.mapGlobalVariable.get("strNewproposalName").toString();
		String strAcctAddrID = EnvironmentDetails.mapGlobalVariable.get("strAcctAddrID").toString();
		//strAgmntNumber=strAgmntNumber.replace(".", "#");
		//String[] arrFinalAgrmntNumber= strAgmntNumber.split("#");
		String strAgrmntName ="";
		String strAgrmntNum = "";
		//String strUniqueKey = "";
		//Map<String, Object> mapOutputData = new HashMap<String, Object>();
		//List<Map<String, Object>> lisOutputData = new ArrayList<>();
		//List<List<Map<String, Object>>> lstmapOutputData = new ArrayList<>();
		MSAGeneration_And_Activation objMSAGeneration = new MSAGeneration_And_Activation();
		
		
		try
		{
		Log.debug("DEBUG: Execution of function 'fn_OrderFlow_Generation_And_Activation' Started.");
		
		//Step 1: Generate, present and accept proposal.
		objMSAGeneration.fn_GenerateAndPresentProposal(driver, webWait);
		
		//Step 2: Create Order line agreement.
		objOrderFlow.fn_clickCrtAgmntLinItmsBtn(driver, webWait).isEnabled();
		objOrderFlow.fn_clickCrtAgmntLinItmsBtn(driver, webWait).click();
		objOrderFlow.fn_clickContinueBtn(driver, webWait).isEnabled();
		objOrderFlow.fn_selAgreementRecordType(driver, webWait).selectByVisibleText("Order Form");
		objOrderFlow.fn_clickContinueBtn(driver, webWait).isEnabled();
	    objOrderFlow.fn_clickContinueBtn(driver, webWait).click();
	    Thread.sleep(1000);
	    try { objOrderFlow.fn_clickEditBtn(driver, new WebDriverWait(driver,30)).isEnabled();
	    objOrderFlow.fn_clickEditBtn(driver, webWait).click(); 		} 
	    catch (Exception e) {  Log.debug("DEBUG: Order alrady open in edit mode.");		}

	    //Edit agreement if it's not open in edit mode
		try {
			objOrderFlow.fn_clickOrderFrmSaveBtn(driver, webWait).isEnabled();
		} catch (Exception e) {
			objOrderFlow.fn_clickEditBtn(driver, webWait).click(); 
		}
	    
	    objOrderFlow.fn_clickOrderFrmSaveBtn(driver, webWait).isEnabled();
	    objOrderFlow.fn_setOrderAddr(driver, webWait).isDisplayed();
	    objOrderFlow.fn_setOrderAddr(driver, webWait).sendKeys(strAcctAddrID);
	    objOrderFlow.fn_clickOrderFrmSaveBtn(driver, webWait).click();
	    objAfterPO.fn_getAgrmntName(driver, webWait).isDisplayed();
	    //capture Agreement Name
	    strAgrmntName = objAfterPO.fn_getAgrmntName(driver, webWait).getText();
	    strAgrmntNum = objAfterPO.fn_getAgrmntNumber(driver, webWait).getText();
	    //Save Agreement Name in global variable map 
	    EnvironmentDetails.mapGlobalVariable.put("strAgrmntNum", strAgrmntNum);
	    EnvironmentDetails.mapGlobalVariable.put("strAgrmntName", strAgrmntName);
	    Log.debug("DEBUG: Agreement Name save in global variable map with key 'strAgrmntName'.");
	    
	    
	    
	    //Step 2
		//Generate Order agreement form 
	    objOrderFlow.fn_clickGenerateBtnOnAgrementFrm(driver, webWait).isEnabled();
		objOrderFlow.fn_clickGenerateBtnOnAgrementFrm(driver, webWait).click();
		objOrderFlow.fn_clickOrdrFrmReturnBtn(driver, webWait).isEnabled();
		objOrderFlow.fn_clickWaterMark(driver, webWait).click();
		objOrderFlow.fn_clickOrdrFrmReturnBtn(driver, webWait).isEnabled();
		objOrderFlow.fn_clickOrderToPresent(driver, webWait).click();
		objOrderFlow.fn_clickOrdrFrmGnrtBtn(driver, webWait).isEnabled();
		Thread.sleep(4000);
		objOrderFlow.fn_clickOrdrFrmGnrtBtn(driver, webWait).click();
		try{  
	    	new WebDriverWait(driver,10).until(ExpectedConditions.alertIsPresent());
		    Alert alert = driver.switchTo().alert();
		    Thread.sleep(2000);
		    Log.info("the message thrown from alert is :: "+alert.getText());
		    alert.accept();
		    blnIsAlertPresent=true;
	    	  }catch(Exception e){ 
	    		  Log.info("unexpected alert is not not present");   
	    	  }
		Thread.sleep(3500);
		if (blnIsAlertPresent==false){
			WebElement elemntReturnBtn1= webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@value='Return' and @type='submit']")));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elemntReturnBtn1);
			Thread.sleep(1000);	
			elemntReturnBtn1.click();}
	    
	    //Step 3: Generate supporting document.
	    objOrderFlow.fn_clickGenerateSpprtDocsBtn(driver, webWait).click();
	    objOrderFlow.fn_clickWaterMark(driver, webWait).isEnabled();
	    objOrderFlow.fn_clickWaterMark(driver, webWait).click();
	    //Handle template
		    try {
		    	//select new aptus test template
			    objOrderFlow.fn_clickSpecificTempOnOrderFrm(driver, new WebDriverWait(driver,4), "New Apttus Test").click();
			} catch (Exception e) {
				// Ireland Order Form
				objOrderFlow.fn_clickSpecificTempOnOrderFrm(driver, webWait, "Order Form").click();
				
			}
		objOrderFlow.fn_clickOrdrFrmGnrtBtn(driver, webWait).isEnabled();
		Thread.sleep(5000);
		objOrderFlow.fn_clickOrdrFrmGnrtBtn(driver, webWait).click();
			try{ 
		    	new WebDriverWait(driver,35).until(ExpectedConditions.alertIsPresent());
			    Alert alert = driver.switchTo().alert();
			    Log.info("the message thrown from alert is :: "+alert.getText());
			    Thread.sleep(2000);
			    alert.accept();
			    blnIsAlertPresent=true;
		    	  }catch(Exception e){ 
		    		  Log.info("unexpected alert is not not present");   
		    	  }
		    
		    if (blnIsAlertPresent==false){
			WebElement elemntReturnBtn1= webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@value='Return' and @type='submit']")));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elemntReturnBtn1);
			Thread.sleep(1000);	
			elemntReturnBtn1.click();}
		    objOrderFlow.fn_clickEditBtn(driver, webWait).isEnabled();


		  //refrsh and wait until status change to In Authoring
		    
		    while (! objOrderFlow.fn_getStatusCategory(driver, webWait).getText().equalsIgnoreCase("In Authoring")) {
		    	Thread.sleep(5000);
				driver.navigate().refresh();
			}
		    
			/*Thread.sleep(5000);
			driver.navigate().refresh();
			if (objOrderFlow.fn_getStatusCategory(driver, webWait).getText().equalsIgnoreCase("In Authoring")) {
				Log.debug("DEBUG: status change to In Authoring.");
			} else {
				Thread.sleep(5000);
				driver.navigate().refresh();
			}*/
			assertEquals(objOrderFlow.fn_getStatusCategory(driver, webWait).getText(), "In Authoring");
			assertEquals(objOrderFlow.fn_getStatus(driver, webWait).getText(), "Generated");
		  
		  
		  if (strSignFrom.equalsIgnoreCase("API")) {
				new MSAGeneration_And_Activation().fn_DocSignAPI(driver, webWait, "strAgrmntName");			}
			else if (strSignFrom.equalsIgnoreCase("Application")) {
				//Step 4: Set GMSA singed document.
				  objMSAGeneration_And_Activation.fn_SetGMSADoc(driver, webWait, "strAgrmntName");			}
		  
		  
	    
	    	blnOrderFlwGenerationAndActivation= true;
		}
	    catch (Exception ExceptionOrderFlow) {
			Log.error("ERROR: Unable to Generate Order with exception reported as: "+ExceptionOrderFlow.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionOrderFlow.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_OrderFlow_Generation_And_Activation method is: "+stack);
           
		}
	    Log.debug("DEBUG: Execution of function 'fn_OrderFlow_Generation_And_Activation' Completed.");
	    return blnOrderFlwGenerationAndActivation;
		
	}
	
	
	/*
	Author: Suhas Thakre
	Function fn_ActivateGMSA: Activate GMSA agreement.
	Inputs: 
	Outputs: Boolean response to check success or failure of function .
	 */
	public void fn_ActivateGMSA(WebDriver driver,WebDriverWait webWait)throws Exception
	{
		//TODO:: Activate GMSA agreement.  
		/************************************************************************************************/
			//Step 1: Open GMSA agreement page. 
			//Step 2: Check e signature status.
			//Step 3: Activate GMSA agreement.
			
		/************************************************************************************************/
		
		Account objacctpage = new Account();
		AfterApproval objAfterPO = new AfterApproval();
		String strNewproposalName = EnvironmentDetails.mapGlobalVariable.get("strNewproposalName").toString();
		String strMSA_AgrmntName = EnvironmentDetails.mapGlobalVariable.get("strMSA_AgrmntName").toString();
		String strMSAStatus="";
		//Boolean blnIsAlertPresent=false;
		
		try
		{
		Log.debug("DEBUG: Execution of function 'fn_ActivateGMSA' Started.");
		
		//Step 1
		objacctpage.fn_setSearchField(driver, webWait).sendKeys(strNewproposalName); 
		objacctpage.fn_clickSearchBtn(driver, webWait).click();
		Thread.sleep(1000);
		driver.findElement(By.partialLinkText(strNewproposalName)).click();
		driver.findElement(By.linkText(strMSA_AgrmntName)).click();
		
		for (int i=0; i<=2;i++ ){
		driver.navigate().refresh();
		Thread.sleep(2000);
		}
		Thread.sleep(1000);
		
		//Step 2
		objAfterPO.fn_clickEsignatureStatus(driver, webWait).click();
		Thread.sleep(60000);
		
		for (int i=0; i<=2;i++ ){
		driver.navigate().refresh();
		Thread.sleep(2000);
		}
		
		//Step 3
	    objAfterPO.fn_clickGMSAFrmActivate(driver, webWait).click();
	    Thread.sleep(8000);
	    
		//Selecting the all Requireddocs 
		List<WebElement> allOptions  = objAfterPO.fn_selDocument(driver, webWait).getOptions(); 
		for(WebElement WebElement : allOptions ){
			WebElement.click();}
		objAfterPO.fn_clickNextButton(driver, webWait).click();
		Thread.sleep(5000);
		objAfterPO.fn_clickActivateOrderForm(driver, webWait).click();	
		Thread.sleep(10000);	
		//Accept alert if present 
		try{   
		    Alert alert = driver.switchTo().alert();
		    Log.info("the message thrown from alert is :: "+alert.getText());
		    alert.accept();
		    //blnIsAlertPresent=true;
	    	  }catch(Exception e){ 
	    		  Log.info("unexpected alert is not not present");   
	    	  }
		
		Thread.sleep(3500);
		//Getting the Final activated status of the Order
		strMSAStatus=objAfterPO.fn_getStatus(driver, webWait).getText();
		if 	(strMSAStatus.contains("Activated"))
			Log.info("The Generated MSA status is activated ::"+strMSAStatus);
		else
			 Log.info("Status of MSA is not Activated :: "+strMSAStatus);
	    
		
	}
    catch (Exception ExceptionActivateGMSA) {
		Log.error("ERROR: Unable to Generate Order with exception reported as: "+ExceptionActivateGMSA.getMessage());
        StringWriter stack = new StringWriter();
        ExceptionActivateGMSA.printStackTrace(new PrintWriter(stack));
        Log.debug("DEBUG: The exception in fn_ActivateGMSA method is: "+stack);
       
	}
    Log.debug("DEBUG: Execution of function 'fn_ActivateGMSA' Completed.");
    }
	
	/*
	Author: Suhas Thakre
	Function fn_OrderFormGenerateChangeOrder: Generate order form for change order.
	Inputs: 
	Outputs: Boolean response to check success or failure of function .
	 */
	public boolean fn_OrderFormGenerateChangeOrder(WebDriver driver,WebDriverWait webWait, String strSignFrom)throws Exception
	{
		

		//TODO:: Generate and activate order agreement.  
		/************************************************************************************************/
			//Step 1: Generate, present and accept proposal.
			//Step 2: To perform Renewal or Amendment.
			//Step 3: Generate agreement.
			//Step 4: Generate supporting document.
			//Step 5: Set GMSA singed document.
			

		/************************************************************************************************/
		Boolean blnOrderFormGenerateChangeOrder = false;
		MSAGeneration_And_Activation objMSAGeneration = new MSAGeneration_And_Activation();
		OrderFlow objOrderFlow = new OrderFlow();
		AfterApproval objAfterPO = new AfterApproval();
		String strProposalType = EnvironmentDetails.mapGlobalVariable.get("strProposalType").toString();
		String strAcctAddrID = EnvironmentDetails.mapGlobalVariable.get("strAcctAddrID").toString();
		String strAgrmntName ="";
		String strAgrmntNum = "";
		Boolean blnIsAlertPresent=false;
		try
		{
			Log.debug("DEBUG: Execution of function 'fn_OrderFormGenerateChangeOrder' Started.");
			
			//Step 1: Generate, present and accept proposal.
			objMSAGeneration.fn_GenerateAndPresentProposal(driver, webWait);
			
			//Step 2: To perform Renewal or Amendment.
			if (strProposalType.equalsIgnoreCase("Renewal")) {
				objOrderFlow.fn_clickRenewalBtn(driver, webWait).click();
			}else if (strProposalType.equalsIgnoreCase("Amendment")) {
				objOrderFlow.fn_clickAmendBtn(driver, webWait).click();
			}
			Thread.sleep(3500);
			objOrderFlow.fn_clickContinueBtn(driver, webWait).click();
		    Thread.sleep(3500);
		    objOrderFlow.fn_setOrderAddr(driver, webWait).clear();
		    objOrderFlow.fn_setOrderAddr(driver, webWait).sendKeys(strAcctAddrID);
		    Thread.sleep(3500);
		    Date date = new Date() ;
    		SimpleDateFormat dateFormat = new SimpleDateFormat("mmss");  		
        	strAgrmntName = "AutoOrder"+dateFormat.format(date);
        	objAfterPO.fn_getAgrmntNameInputBox(driver, webWait).clear();
        	objAfterPO.fn_getAgrmntNameInputBox(driver, webWait).sendKeys(strAgrmntName);
        	Thread.sleep(3500);
		    objOrderFlow.fn_clickOrderFrmSaveBtn(driver, webWait).click();
		    Thread.sleep(4000);
		    objAfterPO.fn_getAgrmntName(driver, webWait).isDisplayed();
		    //capture Agreement Name
		    strAgrmntName = objAfterPO.fn_getAgrmntName(driver, webWait).getText();
		    strAgrmntNum = objAfterPO.fn_getAgrmntNumber(driver, webWait).getText();
		    //Save Agreement Name in global variable map 
		    EnvironmentDetails.mapGlobalVariable.put("strAgrmntNum", strAgrmntNum);
		    EnvironmentDetails.mapGlobalVariable.put("strAgrmntName", strAgrmntName);
		    Log.debug("DEBUG: Agreement Name save in global variable map with key 'strAgrmntName'.");
		    
		    //Step 3: Generate agreement.
		    objOrderFlow.fn_clickGenerateBtnOnAgrementFrm(driver, webWait).isEnabled();
			objOrderFlow.fn_clickGenerateBtnOnAgrementFrm(driver, webWait).click();
			objOrderFlow.fn_clickOrdrFrmReturnBtn(driver, webWait).isEnabled();
			objOrderFlow.fn_clickWaterMark(driver, webWait).click();
			objOrderFlow.fn_clickOrdrFrmReturnBtn(driver, webWait).isEnabled();
			objOrderFlow.fn_clickOrderToPresent(driver, webWait).click();
			objOrderFlow.fn_clickOrdrFrmGnrtBtn(driver, webWait).isEnabled();
			Thread.sleep(4000);
			objOrderFlow.fn_clickOrdrFrmGnrtBtn(driver, webWait).click();
			try{  
		    	new WebDriverWait(driver,10).until(ExpectedConditions.alertIsPresent());
			    Alert alert = driver.switchTo().alert();
			    Thread.sleep(2000);
			    Log.info("the message thrown from alert is :: "+alert.getText());
			    alert.accept();
			    blnIsAlertPresent=true;
		    	  }catch(Exception e){ 
		    		  Log.info("unexpected alert is not not present");   
		    	  }
			Thread.sleep(3500);
			if (blnIsAlertPresent==false){
				WebElement elemntReturnBtn1= webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@value='Return' and @type='submit']")));
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elemntReturnBtn1);
				Thread.sleep(1000);	
				elemntReturnBtn1.click();}
		    
			//to handle support document condition : for amendment support document will not be generated 
			try {
					//Step 4: Generate supporting document.
					objOrderFlow.fn_clickGenerateSpprtDocsBtn(driver, new WebDriverWait(driver,10)).isEnabled();
				    objOrderFlow.fn_clickGenerateSpprtDocsBtn(driver, webWait).click();
				    objOrderFlow.fn_clickWaterMark(driver, webWait).isEnabled();
				    objOrderFlow.fn_clickWaterMark(driver, webWait).click();
				    //Handle template
					    try {
					    	//select new aptus test template
						    objOrderFlow.fn_clickSpecificTempOnOrderFrm(driver, new WebDriverWait(driver,4), "New Apttus Test").click();
						} catch (Exception e) {
							// Ireland Order Form
							objOrderFlow.fn_clickSpecificTempOnOrderFrm(driver, webWait, "Order Form").click();
							
						}
					objOrderFlow.fn_clickOrdrFrmGnrtBtn(driver, webWait).isEnabled();
					Thread.sleep(5000);
					objOrderFlow.fn_clickOrdrFrmGnrtBtn(driver, webWait).click();
						try{ 
					    	new WebDriverWait(driver,35).until(ExpectedConditions.alertIsPresent());
						    Alert alert = driver.switchTo().alert();
						    Log.info("the message thrown from alert is :: "+alert.getText());
						    Thread.sleep(2000);
						    alert.accept();
						    blnIsAlertPresent=true;
					    	  }catch(Exception e){ 
					    		  Log.info("unexpected alert is not not present");   
					    	  }
					    
					    if (blnIsAlertPresent==false){
						WebElement elemntReturnBtn1= webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@value='Return' and @type='submit']")));
						((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elemntReturnBtn1);
						Thread.sleep(1000);	
						elemntReturnBtn1.click();}
			} catch (Exception e) {
				Log.info("for amendment support document will not be generated");
			}
		    
			    
			    
			    objOrderFlow.fn_clickEditBtn(driver, webWait).isEnabled();
			    //refresh and wait until status change to In Authoring
			    
			    while (! objOrderFlow.fn_getStatusCategory(driver, webWait).getText().equalsIgnoreCase("In Authoring")) {
			    	Thread.sleep(5000);
					driver.navigate().refresh();
				}
			    
				
				assertEquals(objOrderFlow.fn_getStatusCategory(driver, webWait).getText(), "In Authoring");
				assertEquals(objOrderFlow.fn_getStatus(driver, webWait).getText(), "Generated");
			  
			  
			  if (strSignFrom.equalsIgnoreCase("API")) {
					new MSAGeneration_And_Activation().fn_DocSignAPI(driver, webWait, "strAgrmntName");			}
				else if (strSignFrom.equalsIgnoreCase("Application")) {
					//Step 4: Set GMSA singed document.
					objMSAGeneration.fn_SetGMSADoc(driver, webWait, "strAgrmntName");			}
			
			  blnOrderFormGenerateChangeOrder =true;
		}
		catch (Exception ExceptionOrderFlow) {
			Log.error("ERROR: Unable to Generate Order with exception reported as: "+ExceptionOrderFlow.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionOrderFlow.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_OrderFormGenerateChangeOrder method is: "+stack);
           
		}
	    return blnOrderFormGenerateChangeOrder;
	}


}
