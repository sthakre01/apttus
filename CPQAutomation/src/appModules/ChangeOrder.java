/*
 Author     		:	Suhas Thakre
 Class Name			: 	CreateUpgDwg 
 Purpose     		: 	Purpose of this file is :
						1. To Create Proposal

 Date       		:	21/02/2017 
 Modified Date		:		
 Modified By		: 	
 Version Information:	Version 1.0
 						
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/
package appModules;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.Configuration;
import pageObjects.SalesForce.ConfigurationPageObjects;
import pageObjects.SalesForce.Configuration_NA;
import pageObjects.SalesForce.LeadCreation;
import pageObjects.SalesForce.Pricing;
import pageObjects.SalesForce.Proposal;
import utility.EnvironmentDetails;
import utility.ExcelHandler;
import utility.Log;
import utility.Utils;
import utility.WriteExcel;


public class ChangeOrder  {
	
	
	/*
	Author: Suhas Thakre
	Function fn_ChangeOrder : To perform Upgrade, Downgrade and delete action.
	Inputs: 
	Outputs: Boolean response to check success or failure of function .
	 */
	public boolean fn_ChangeOrder(WebDriver driver,WebDriverWait webWait) throws Throwable
    {
		//TODO:: Select Product family  
		/************************************************************************************************/
			//Step 1: Read Change_Order Data sheet. 
			//Step 2: Delete existing product.
			//Step 3: Add LineItem to product.
			//Step 4: Remove LineItem of product.
			//Step 5: Update LineItem of product.
		
		/************************************************************************************************/
		ExcelHandler objExcel = new ExcelHandler();
		Configuration objConfig = new Configuration();
		Boolean blnChangeOrder = false;
		String strChangeOrderWB = "TestData.xlsx";
		String strChangeOrderSheet = "Change_Order";
		List<Map<String, String>> lstmapChangeOrderData = new ArrayList<Map<String,String>>();
		String strAction = "";
		String strProductName = "";
		String strLineItemName = "";
		String strAttribute = "";
		
    	
        try
        {   
        	String excelResource = EnvironmentDetails.Path_ExternalFiles
					+ System.getProperty("file.separator") + strChangeOrderWB;
			File fileLeadCreationData = new File(excelResource);
			lstmapChangeOrderData = objExcel.fn_ReadExcelAsMap(fileLeadCreationData,strChangeOrderSheet); 
			for (Map<String, String> mapChangeOrderData : lstmapChangeOrderData) {
				strAction = mapChangeOrderData.get("Action").toString().trim();
				strProductName = mapChangeOrderData.get("Product_Name").toString().trim();
				strLineItemName = mapChangeOrderData.get("LineItem_Name").toString().trim();
				strAttribute = mapChangeOrderData.get("Attribute").toString().trim();
				
				
				//Change order for Angular js UI
				//Step 2: Delete existing product.
				if (strAction.equalsIgnoreCase("Delete") && strLineItemName.equalsIgnoreCase("NA")) {
					fn_DeleteProductAJ(driver, webWait, strProductName);
				}
				//Step 3: Add LineItem to product.
				else if (strAction.equalsIgnoreCase("Add") && !strLineItemName.equalsIgnoreCase("NA")) {
					fn_AddRemoveLineItemAj(driver, webWait, strProductName, strLineItemName, strAttribute, strAction);
				}
				//Step 4: Remove LineItem to product.
				else if (strAction.equalsIgnoreCase("Delete") && !strLineItemName.equalsIgnoreCase("NA")) {
					fn_AddRemoveLineItemAj(driver, webWait, strProductName, strLineItemName, strAttribute, strAction);
				}
				//Step 6: Set Adjustment.
				else if (strAction.equalsIgnoreCase("Update") && strLineItemName.equalsIgnoreCase("Adjustment") && !strAttribute.equalsIgnoreCase("NA")) {
					fn_SetAdjustmentAj(driver, webWait, strProductName, strAttribute);
				}
				//Step 5: Update LineItem to product.
				else if (strAction.equalsIgnoreCase("Update") && !strLineItemName.equalsIgnoreCase("NA") && !strAttribute.equalsIgnoreCase("NA")) {
					fn_UpdateLineItemAj(driver, webWait, strProductName, strLineItemName, strAttribute);
				}
				
				Utils.fn_waitForSpinner(driver);
				objConfig.fn_clickOfferingMgmntNABtn(driver, new WebDriverWait(driver,30)).isEnabled();
				
									
				
				
								
			}
        	
        	blnChangeOrder = true;
    		
        }
        catch (Exception ExceptionChangeOrder) {
			Log.error("ERROR: Unable to Change Order with exception reported as: "+ExceptionChangeOrder.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionChangeOrder.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_ChangeOrder method is: "+stack);
           
		}
        return blnChangeOrder;
    }
	
	
	
	/*
	Author: Suhas Thakre
	Function fn_SetAdjustment : Set discount or markup on product.
	Inputs: strProductName - Product tobe Configure.
			
	Outputs: Boolean response to check success or failure of function .
	 */
	public void fn_SetAdjustment(WebDriver driver,WebDriverWait webWait,String strProductName, String strAttribute)
	{
		//TODO:: Set discount or markup on product.  
				/************************************************************************************************/
					//Step 1: Navigate to Reconfig page.
					//Step 2: Set Adjustment.
					//Step 3: Reprice proposal.
					
				
				/************************************************************************************************/
		
		Configuration objConfig = new Configuration();
		Pricing objPricing = new Pricing();
		pageObjects.SalesForce.ChangeOrder objChangeOrder = new pageObjects.SalesForce.ChangeOrder();
		String[] arrAttribute = strAttribute.split("##");
		try
        {
			//Step 1: Navigate to Reconfig page.
			Utils.fn_waitForSpinner(driver);
			objConfig.fn_clickOfferingMgmntBtn(driver, new WebDriverWait(driver,60)).isEnabled();
			
			//Step 2: Set Adjustment.
			objChangeOrder.fn_selectAdjustmentType(driver, webWait, strProductName).selectByValue(arrAttribute[0]);
			objChangeOrder.fn_setAdjustmentAmount(driver, webWait, strProductName).clear();
			objChangeOrder.fn_setAdjustmentAmount(driver, webWait, strProductName).sendKeys(arrAttribute[1]);
			
			//Step 3: Reprice proposal.
			objPricing.fn_RepriceBtn(driver, webWait).click();
			new WebDriverWait(driver,15).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(objChangeOrder.fn_textUpdatingPrice(driver, webWait, strProductName)));
	        webWait.until(ExpectedConditions.invisibilityOfElementLocated(objChangeOrder.fn_textUpdatingPrice(driver, webWait, strProductName)));
			Utils.fn_waitForSpinner(driver);
			objConfig.fn_clickOfferingMgmntBtn(driver, new WebDriverWait(driver,30)).isEnabled();
			Utils.fn_waitForSpinner(driver);
			
			
			
        }
		catch (Exception ExceptionSetAdjustment)
	    {
	            Log.error("ERROR: Unable to Set Adjustment with exception reported as: "+ExceptionSetAdjustment.getMessage());
	            StringWriter stack = new StringWriter();
	            ExceptionSetAdjustment.printStackTrace(new PrintWriter(stack));
	            Log.debug("DEBUG: The exception in fn_SetAdjustment method is: "+stack);
	
	     }
		
	}
	
	
	/*
	Author: Suhas Thakre
	Function fn_SetAdjustmentAj : Set discount or markup on product.
	Inputs: strProductName - Product tobe Configure.
			
	Outputs: Boolean response to check success or failure of function .
	 */
	public void fn_SetAdjustmentAj(WebDriver driver,WebDriverWait webWait,String strProductName, String strAttribute)
	{
		//TODO:: Set discount or markup on product.  
				/************************************************************************************************/
					//Step 1: Navigate to Reconfig page.
					//Step 2: Set Adjustment.
					//Step 3: Reprice proposal.
					
				
				/************************************************************************************************/
		
		Configuration objConfig = new Configuration();
		Configuration_NA objConfigNA = new Configuration_NA();
		Pricing objPricing = new Pricing();
		pageObjects.SalesForce.ChangeOrder objChangeOrder = new pageObjects.SalesForce.ChangeOrder();
		String[] arrAttribute = strAttribute.split("##");
		try
        {
			//Step 1: Navigate to Reconfig page.
			Thread.sleep(5000);
			Utils.fn_waitForSpinner(driver);
			objConfig.fn_clickOfferingMgmntNABtn(driver, webWait).isEnabled();
			
			//Step 2: Set Adjustment.
			Utils.fn_scrollToElement(driver, objChangeOrder.fn_clickAdjustmentTypeNA(driver, webWait, strProductName));
			objChangeOrder.fn_clickAdjustmentTypeNA(driver, webWait, strProductName).click();
			objChangeOrder.fn_selectAdjustmentTypeNA(driver, webWait, arrAttribute[0]).isEnabled();
			objChangeOrder.fn_selectAdjustmentTypeNA(driver, webWait, arrAttribute[0]).click();
			
			Utils.fn_scrollToElement(driver, objChangeOrder.fn_setAdjustmentAmountNA(driver, webWait, strProductName));
			objChangeOrder.fn_setAdjustmentAmountNA(driver, webWait, strProductName).clear();
			objChangeOrder.fn_setAdjustmentAmountNA(driver, webWait, strProductName).sendKeys(arrAttribute[1]);
			
			//Step 3: Reprice proposal.
			objConfigNA.fn_clickRepriceNABtn(driver, webWait).isEnabled();
			objConfigNA.fn_clickRepriceNABtn(driver, webWait).click();
			Utils.fn_waitForSpinner(driver);
			objConfig.fn_clickOfferingMgmntNABtn(driver, webWait).isEnabled();
			
			
			
			
        }
		catch (Exception ExceptionSetAdjustment)
	    {
	            Log.error("ERROR: Unable to Set Adjustment with exception reported as: "+ExceptionSetAdjustment.getMessage());
	            StringWriter stack = new StringWriter();
	            ExceptionSetAdjustment.printStackTrace(new PrintWriter(stack));
	            Log.debug("DEBUG: The exception in fn_SetAdjustmentAj method is: "+stack);
	
	     }
		
	}

	
	
	/*
	Author: Suhas Thakre
	Function fn_UpdateLineItem : Update Quantity of lineItem to existing product.
	Inputs: strProductName - Product tobe Configure.
			strLineItemName - LineItem tobe Configure.
			strAttribute - Attributes tobe update.
	Outputs: Boolean response to check success or failure of function .
	 */
	public void fn_UpdateLineItem(WebDriver driver,WebDriverWait webWait,String strProductName, String strLineItemName, String strAttribute)
	{
		//TODO:: Update Quantity of lineItem to existing product.  
				/************************************************************************************************/
					//Step 1: Navigate to Reconfig page.
					//Step 2: Update LineItem Attribute.
					//Step 3: Navigate Back To Pricing Screen.
					
				
				/************************************************************************************************/
		
		Configuration objConfig = new Configuration();
		ConfigurationPageObjects objConfigi = new ConfigurationPageObjects();
		pageObjects.SalesForce.ChangeOrder objChangeOrder = new pageObjects.SalesForce.ChangeOrder();
		String[] arrAttribute = strAttribute.split("##");
		try
        {
			//Step 1: Navigate to Reconfig page.
			objConfig.fn_clickOfferingMgmntBtn(driver, new WebDriverWait(driver,30)).isEnabled();
			objChangeOrder.fn_clickProductConfigureBtn(driver, webWait, strProductName).isEnabled();
			objChangeOrder.fn_clickProductConfigureBtn(driver, webWait, strProductName).click();
			objConfig.fn_clickNext(driver, webWait).isEnabled();
			objConfig.fn_clickNext(driver, webWait).click();
			objConfig.fn_clickPricingBtn(driver, new WebDriverWait(driver,30)).isEnabled();
			//Step 2: Update LineItem Attribute.
			//Update line item on configure window or Attribute window 
			try {
				Utils.fn_scrollToElement(driver, objChangeOrder.fn_setLineItemAttribute(driver, new WebDriverWait(driver,5), strLineItemName, arrAttribute[0]));
				objChangeOrder.fn_setLineItemAttribute(driver, webWait, strLineItemName, arrAttribute[0]).clear();
				Utils.fn_waitForSpinner(driver);
				objChangeOrder.fn_setLineItemAttribute(driver, webWait, strLineItemName, arrAttribute[0]).sendKeys(arrAttribute[1]);;
			} catch (Exception e) {
				// Update line item on Attribute window 
				objChangeOrder.fn_clickImageAttributes(driver, webWait, strLineItemName).click();
				Utils.fn_waitForSpinner(driver);
				Thread.sleep(15000);
				driver.switchTo().frame(driver.findElement(By.id("idAttributesIframe")));
				objChangeOrder.fn_clickQuickSaveBtn(driver, webWait).isEnabled();
				objChangeOrder.fn_selectImgAttribute(driver, webWait, arrAttribute[0]).selectByValue(arrAttribute[1]);
				objConfigi.fn_clickAttSaveBtn(driver, webWait).click();
				objConfig.fn_clickPricingBtn(driver, new WebDriverWait(driver,30)).isEnabled();
	        	Utils.fn_waitForSpinner(driver);
			}
			
			//Step 3: Navigate Back To Pricing Screen.
			objConfig.fn_clickHeader(driver, webWait).click();
			Utils.fn_waitForSpinner(driver);
			objConfig.fn_clickPricingBtn(driver, new WebDriverWait(driver,30)).isEnabled();
			objConfig.fn_clickPricingBtn(driver, new WebDriverWait(driver,30)).click();
			objConfig.fn_clickOfferingMgmntBtn(driver, new WebDriverWait(driver,30)).isEnabled();
			
			
        }
		catch (Exception Exceptionfn_AddLineItem)
	    {
	            Log.error("ERROR: Unable to Add LineItem with exception reported as: "+Exceptionfn_AddLineItem.getMessage());
	            StringWriter stack = new StringWriter();
	            Exceptionfn_AddLineItem.printStackTrace(new PrintWriter(stack));
	            Log.debug("DEBUG: The exception in fn_AddLineItem method is: "+stack);
	
	     }
		
	}
	
	
	/*
	Author: Suhas Thakre
	Function fn_UpdateLineItemAj : Update Quantity of lineItem to existing product.
	Inputs: strProductName - Product tobe Configure.
			strLineItemName - LineItem tobe Configure.
			strAttribute - Attributes tobe update.
	Outputs: Boolean response to check success or failure of function .
	 */
	public void fn_UpdateLineItemAj(WebDriver driver,WebDriverWait webWait,String strProductName, String strLineItemName, String strAttribute)
	{
		//TODO:: Update Quantity of lineItem to existing product.  
				/************************************************************************************************/
					//Step 1: Navigate to Reconfig page.
					//Step 2: Update LineItem Attribute.
					//Step 3: Navigate Back To Pricing Screen.
					
				
				/************************************************************************************************/
		
		Configuration objConfig = new Configuration();
		Configuration_NA objConfigNA = new Configuration_NA();
		pageObjects.SalesForce.ChangeOrder objChangeOrder = new pageObjects.SalesForce.ChangeOrder();
		String[] arrAttribute = strAttribute.split("##");
		try
        {
			//Step 1: Navigate to Reconfig page.
			objConfig.fn_clickOfferingMgmntNABtn(driver, webWait).isEnabled();
			objChangeOrder.fn_clickProductConfigureNABtn(driver, webWait, strProductName).isEnabled();
			objChangeOrder.fn_clickProductConfigureNABtn(driver, webWait, strProductName).click();
			objConfig.fn_clickProductOptionsbtn(driver, webWait).isEnabled();
			objConfig.fn_clickProductOptionsbtn(driver, webWait).click();
			objConfigNA.fn_clickQuickSaveNABtn(driver, webWait).isEnabled();
			
			//Step 2: Update LineItem Attribute.
			if (arrAttribute[0].equalsIgnoreCase("Monthly Commitment")) {
				 objConfigNA.fn_setMonthlyCommitment(driver, webWait).clear();
				 objConfigNA.fn_setMonthlyCommitment(driver, webWait).sendKeys(arrAttribute[1]);
			}else if (arrAttribute[0].equalsIgnoreCase("User Override")) {
				objConfigNA.fn_clickUserOverride(driver, webWait).click();
				Utils.fn_waitForProgressBar(driver);
				objConfigNA.fn_selectOverRide(driver, webWait, arrAttribute[1]).click();
				Utils.fn_waitForProgressBar(driver);
			}
			else if (arrAttribute[0].equalsIgnoreCase("Qty")) {
				objConfigNA.fn_setLineIteamQty(driver, webWait, strLineItemName).clear();
				objConfigNA.fn_setLineIteamQty(driver, webWait, strLineItemName).sendKeys(arrAttribute[1]);
				Utils.fn_waitForProgressBar(driver);
				
			}
			
			//Step 3: Navigate Back To Pricing Screen.
			objConfigNA.fn_clickGotoPricingNABtn(driver, webWait).isEnabled();
			objConfigNA.fn_clickGotoPricingNABtn(driver, webWait).click();
			objConfig.fn_clickOfferingMgmntNABtn(driver, webWait).isEnabled();
			
			
        }
		catch (Exception Exceptionfn_AddLineItem)
	    {
	            Log.error("ERROR: Unable to Add LineItem with exception reported as: "+Exceptionfn_AddLineItem.getMessage());
	            StringWriter stack = new StringWriter();
	            Exceptionfn_AddLineItem.printStackTrace(new PrintWriter(stack));
	            Log.debug("DEBUG: The exception in fn_UpdateLineItemAj method is: "+stack);
	
	     }
		
	}

	
	
	/*
	Author: Suhas Thakre
	Function fn_AddLineItem : Add or Remove lineItem to existing product.
	Inputs: strProductName - Product tobe remove
	Outputs: Boolean response to check success or failure of function .
	 */
	public void fn_AddRemoveLineItem(WebDriver driver,WebDriverWait webWait,String strProductName, String strLineItemName, String strAction)
	{
		//TODO:: Add or Remove lineItem to existing product.  
				/************************************************************************************************/
					//Step 1: Navigate to Reconfig page.
					//Step 2: Select LineItem.
					//Step 3: Navigate Back To Pricing Screen.
					
				
				/************************************************************************************************/
		
		Configuration objConfig = new Configuration();
		Utils objUtils = new Utils();
		pageObjects.SalesForce.ChangeOrder objChangeOrder = new pageObjects.SalesForce.ChangeOrder();
		try
        {
			//Step 1: Navigate to Reconfig page.
			objConfig.fn_clickOfferingMgmntBtn(driver, new WebDriverWait(driver,30)).isEnabled();
			objChangeOrder.fn_clickProductConfigureBtn(driver, webWait, strProductName).isEnabled();
			objChangeOrder.fn_clickProductConfigureBtn(driver, webWait, strProductName).click();
			objConfig.fn_clickNext(driver, webWait).isEnabled();
			objConfig.fn_clickNext(driver, webWait).click();
			objConfig.fn_clickPricingBtn(driver, new WebDriverWait(driver,30)).isEnabled();
			
			Utils.fn_scrollToElement(driver, objChangeOrder.fn_clickAddLineItem(driver, webWait, strLineItemName));
			//Step 2: Select LineItem.
			Boolean blnLineItemSelected = objChangeOrder.fn_clickAddLineItem(driver, webWait, strLineItemName).isSelected();
			if (blnLineItemSelected && strAction.equalsIgnoreCase("Delete")) {
				objChangeOrder.fn_clickAddLineItem(driver, webWait, strLineItemName).click();
			}
			else if ((!blnLineItemSelected) && strAction.equalsIgnoreCase("Add")) {
				objChangeOrder.fn_clickAddLineItem(driver, webWait, strLineItemName).click();
			}
			Utils.fn_waitForSpinner(driver);
			//TODO:: Capture Screenshot for Agreement after activation 
			objUtils.fn_takeScreenshotFullPage(driver);
			
			//Step 3: Navigate Back To Pricing Screen.
			objConfig.fn_clickPricingBtn(driver, new WebDriverWait(driver,30)).isEnabled();
			objConfig.fn_clickPricingBtn(driver, new WebDriverWait(driver,30)).click();
			objConfig.fn_clickOfferingMgmntBtn(driver, new WebDriverWait(driver,30)).isEnabled();
			
			
        }
		catch (Exception Exceptionfn_AddLineItem)
	    {
	            Log.error("ERROR: Unable to Add LineItem with exception reported as: "+Exceptionfn_AddLineItem.getMessage());
	            StringWriter stack = new StringWriter();
	            Exceptionfn_AddLineItem.printStackTrace(new PrintWriter(stack));
	            Log.debug("DEBUG: The exception in fn_AddLineItem method is: "+stack);
	
	     }
		
	}
	
	
	
	
	/*
	Author: Suhas Thakre
	Function fn_AddRemoveLineItemAj : Add or Remove lineItem to existing product.
	Inputs: strProductName - Product tobe remove
	Outputs: Boolean response to check success or failure of function .
	 */
	public void fn_AddRemoveLineItemAj(WebDriver driver,WebDriverWait webWait,String strProductName, String strLineItemName, String strAttribute, String strAction)
	{
		//TODO:: Add or Remove lineItem to existing product.  
				/************************************************************************************************/
					//Step 1: Navigate to Reconfig page.
					//Step 2: Select LineItem.
					//Step 3: Navigate Back To Pricing Screen.
					
				
				/************************************************************************************************/
		
		Configuration objConfig = new Configuration();
		Configuration_NA objConfigNA = new Configuration_NA();
		pageObjects.SalesForce.ChangeOrder objChangeOrder = new pageObjects.SalesForce.ChangeOrder();
		try
        {
			//Step 1: Navigate to Reconfig page.
			//Navigate to Reconfig page and remove product
			objConfig.fn_clickOfferingMgmntNABtn(driver, webWait).isEnabled();
			objChangeOrder.fn_clickProductConfigureNABtn(driver, webWait, strProductName).isEnabled();
			objChangeOrder.fn_clickProductConfigureNABtn(driver, webWait, strProductName).click();
			objConfig.fn_clickProductOptionsbtn(driver, webWait).isEnabled();
			objConfig.fn_clickProductOptionsbtn(driver, webWait).click();
			objConfigNA.fn_clickQuickSaveNABtn(driver, webWait).isEnabled();
			
			Utils.fn_waitForSpinner(driver);
			Utils.fn_scrollToElement(driver, objChangeOrder.fn_clickLineItemNA(driver, webWait, strLineItemName));
			
						
			//Step 2: Select LineItem.
			Boolean blnLineItemSelected = objChangeOrder.fn_clickAddLineItemNA(driver, webWait, strLineItemName).isSelected();
			if (blnLineItemSelected && strAction.equalsIgnoreCase("Delete")) {
				objChangeOrder.fn_clickAddLineItemNA(driver, webWait, strLineItemName).click();
			}
			else if ((!blnLineItemSelected) && strAction.equalsIgnoreCase("Add")) {
				objChangeOrder.fn_clickAddLineItemNA(driver, webWait, strLineItemName).click();
				Utils.fn_waitForProgressBar(driver);
				//Configure the attributes 
				if (!strAttribute.equalsIgnoreCase("NA")) {
					String[] arrAttribute = strAttribute.split("##");
					//Step 2: Update LineItem Attribute.
					if (arrAttribute[0].equalsIgnoreCase("Monthly Commitment")) {
						 objConfigNA.fn_setMonthlyCommitment(driver, webWait).clear();
						 objConfigNA.fn_setMonthlyCommitment(driver, webWait).sendKeys(arrAttribute[1]);
					}else if (arrAttribute[0].equalsIgnoreCase("User Override")) {
						objConfigNA.fn_clickUserOverride(driver, webWait).click();
						Utils.fn_waitForProgressBar(driver);
						objConfigNA.fn_selectOverRide(driver, webWait, arrAttribute[1]).click();
						Utils.fn_waitForProgressBar(driver);
					}
					else if (arrAttribute[0].equalsIgnoreCase("Qty")) {
						objConfigNA.fn_setLineIteamQty(driver, webWait, strLineItemName).clear();
						objConfigNA.fn_setLineIteamQty(driver, webWait, strLineItemName).sendKeys(arrAttribute[1]);
						Utils.fn_waitForProgressBar(driver);
						
					}
					
				}
			}
			
			
			
			Utils.fn_waitForSpinner(driver);
			
			//Step 3: Navigate Back To Pricing Screen.
			objConfigNA.fn_clickGotoPricingNABtn(driver, webWait).isEnabled();
			objConfigNA.fn_clickGotoPricingNABtn(driver, webWait).click();
			objConfig.fn_clickOfferingMgmntNABtn(driver, webWait).isEnabled();
			
			
        }
		catch (Exception Exceptionfn_AddLineItem)
	    {
	            Log.error("ERROR: Unable to Add LineItem with exception reported as: "+Exceptionfn_AddLineItem.getMessage());
	            StringWriter stack = new StringWriter();
	            Exceptionfn_AddLineItem.printStackTrace(new PrintWriter(stack));
	            Log.debug("DEBUG: The exception in fn_AddRemoveLineItemAj method is: "+stack);
	
	     }
		
	}

	
	/*
	Author: Suhas Thakre
	Function fn_DeleteProduct : Delete existing product.
	Inputs: strProductName - Product tobe remove
	Outputs: Boolean response to check success or failure of function .
	 */
	public void fn_DeleteProduct(WebDriver driver,WebDriverWait webWait,String strProductName)
	{
		
		Configuration objConfig = new Configuration();
		pageObjects.SalesForce.ChangeOrder objChangeOrder = new pageObjects.SalesForce.ChangeOrder();
		try
        {
			//Navigate to Reconfig page and remove product
			objConfig.fn_clickOfferingMgmntBtn(driver, new WebDriverWait(driver,30)).isEnabled();
			objChangeOrder.fn_clickProductConfigureBtn(driver, webWait, strProductName).isEnabled();
			objChangeOrder.fn_clickProductConfigureBtn(driver, webWait, strProductName).click();
			objConfig.fn_clickNext(driver, webWait).isEnabled();
			objConfig.fn_clickNext(driver, webWait).click();
			objConfig.fn_clickPricingBtn(driver, new WebDriverWait(driver,30)).isEnabled();
			objChangeOrder.fn_clickProductRemoveBtn(driver, webWait, strProductName).isEnabled();
			objChangeOrder.fn_clickProductRemoveBtn(driver, webWait, strProductName).click();
			objChangeOrder.fn_clickYesBtn(driver, webWait).isEnabled();
			objChangeOrder.fn_clickYesBtn(driver, webWait).click();
			webWait.until(ExpectedConditions.invisibilityOfElementLocated(objChangeOrder.fn_imgloading(driver, webWait)));
			//objConfig.fn_clickUpdatePriceBtn(driver, new WebDriverWait(driver,30)).isEnabled();
			//objConfig.fn_clickPricingBtn(driver, new WebDriverWait(driver,30)).click();
			objConfig.fn_clickOfferingMgmntBtn(driver, new WebDriverWait(driver,30)).isEnabled();
			
			
        }
		catch (Exception ExceptionRemoveProd)
	    {
	            Log.error("ERROR: Unable to Delete Product with exception reported as: "+ExceptionRemoveProd.getMessage());
	            StringWriter stack = new StringWriter();
	            ExceptionRemoveProd.printStackTrace(new PrintWriter(stack));
	            Log.debug("DEBUG: The exception in fn_DeleteProduct method is: "+stack);
	
	     }
		
	}

	
	/*
	Author: Suhas Thakre
	Function fn_DeleteProductAWS : Delete existing product.
	Inputs: strProductName - Product tobe remove
	Outputs: Boolean response to check success or failure of function .
	 */
	public void fn_DeleteProductAJ(WebDriver driver,WebDriverWait webWait,String strProductName)
	{
		
		Configuration objConfig = new Configuration();
		Configuration_NA objConfigNA = new Configuration_NA();
		pageObjects.SalesForce.ChangeOrder objChangeOrder = new pageObjects.SalesForce.ChangeOrder();
		try
        {
			//Navigate to Reconfig page and remove product
			objConfig.fn_clickOfferingMgmntNABtn(driver, webWait).isEnabled();
			objChangeOrder.fn_clickProductConfigureNABtn(driver, webWait, strProductName).isEnabled();
			objChangeOrder.fn_clickProductConfigureNABtn(driver, webWait, strProductName).click();
			//objConfig.fn_clickProductOptionsbtn(driver, webWait).isEnabled();
			//objConfig.fn_clickProductOptionsbtn(driver, webWait).click();
			objConfigNA.fn_clickGotoPricingNABtn(driver, webWait).isEnabled();
			objConfigNA.fn_clickRemoveItemNABtn(driver, webWait).isEnabled();
			objConfigNA.fn_clickRemoveItemNABtn(driver, webWait).click();
			objConfigNA.fn_clickGotoPricingNABtn(driver, webWait).isEnabled();
			objConfigNA.fn_clickGotoPricingNABtn(driver, webWait).click();
			objConfig.fn_clickOfferingMgmntNABtn(driver, webWait).isEnabled();
			
			
        }
		catch (Exception ExceptionRemoveProd)
	    {
	            Log.error("ERROR: Unable to Delete Product with exception reported as: "+ExceptionRemoveProd.getMessage());
	            StringWriter stack = new StringWriter();
	            ExceptionRemoveProd.printStackTrace(new PrintWriter(stack));
	            Log.debug("DEBUG: The exception in fn_DeleteProductAJ method is: "+stack);
	
	     }
		
	}

	
}
