/*
 Author     		:	Suhas Thakre
 Class Name			: 	CreateUpgDwg 
 Purpose     		: 	Purpose of this file is :
						1. To perform  upgrade and downgrade existing order 

 Date       		:	26/12/2016 
 Modified Date		:		
 Modified By		: 	
 Version Information:	Version 1.0
 						
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/


package appModules;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.Account;
import pageObjects.SalesForce.AfterApproval;
import pageObjects.SalesForce.OfferingData;
import pageObjects.SalesForce.OpportunitesPageObject;
import pageObjects.SalesForce.TerminationLog;
import utility.API_SOAP;
import utility.EnvironmentDetails;
import utility.ExcelHandler;
import utility.Log;
import utility.Utils;
import utility.VerificationMethods;
import utility.WriteExcel;


public class Termination extends VerificationMethods{
	
	/*
	Author: Suhas Thakre
	Function fn_CreateOpportunity: Create new Proposal from Renewal/Amendment.
	Inputs: strAccountName - Account name.
			strApttusQueryName - Apptus query to fetch offering details.
	Outputs: Boolean response to check success or failure of function .
	 */
	//Create proposal from upgrade and downgrade 
	public Boolean fn_CreateOpportunity(WebDriver driver,WebDriverWait webWait, String strAccountName, String strApttusQueryName, String strOffetingToTerminate)  throws Exception
    {
		
		//TODO:: generating the proposal from Offering data
		/************************************************************************************************/
			//Step 1: Open Account Page
			//Step 2: Fetch offering related to active order with help of SOQL
			//Step 3: Create opportunity through offering management.
		
		/************************************************************************************************/
		
		
		ExcelHandler objExcelHandler = new ExcelHandler();
		Utils objUtils = new Utils();
		WriteExcel objWriteExcel = new WriteExcel();
		Account objAcctpage = new Account(); 
		OfferingData objOfferingData= new OfferingData();
		EnvironmentDetails objEnv = (EnvironmentDetails) EnvironmentDetails.mapGlobalVariable.get("objEnv");
		OpportunitesPageObject objOpportunites = new OpportunitesPageObject();
		API_SOAP objAPI_SOAP = new API_SOAP();
		List<String> arrstrOfferingName = new ArrayList<>();
		List<String> arrstrOfferingSel = new ArrayList<>();
		String[] arrNoOffetingToSelect = null;
		String strOpportunityName = "";
		Boolean blnCreateOpportunity = false;
		
		
		Map<String, Object> mapOutputData = new HashMap<String, Object>();
		List<Map<String, Object>> lisOutputData = new ArrayList<>();
		List<List<Map<String, Object>>> lstmapOutputData = new ArrayList<>();
		Map<String, Object> mapTestCases = new HashMap<String, Object>();
		mapTestCases.put("CallType", "query");
		
		
		//Read query from workbook ApttusQuery.xlsx and sheet ApttusQuerySheet
		String strApttusQuery = objExcelHandler.fn_ReadApttusQuery("ApttusQuery", "ApttusQuerySheet", strApttusQueryName);
		strApttusQuery = strApttusQuery.replace("AccountNameInput", strAccountName);
		
		try {
			
			//Step 1: Open Account Page
			objAcctpage.fn_setSearchField(driver, webWait).sendKeys(strAccountName); 
			objAcctpage.fn_clickSearchBtn(driver, webWait).click();
			Thread.sleep(3500);
			driver.findElement(By.partialLinkText(strAccountName)).click();
			
			/*//Step 2: Fetch offering related to active order with help of SOQL 
			mapTestCases.put("CallingData", strApttusQuery);
			List<List<Map<String, String>>> lstmapSOAPRes = objAPI_SOAP.fn_SOAP_GET(objEnv, mapTestCases, null);
			//Add result to list 
				for (List<Map<String, String>> listmap : lstmapSOAPRes) {
					 
					 for (Map<String, String> map : listmap) {
						 
						 arrstrOfferingName.add(map.get("Name").toString());
					}
					
				}
			
			
				
				
			//list of opportunity to select	
			if (strOffetingToTerminate.equalsIgnoreCase("ALL")) {
				arrstrOfferingSel = arrstrOfferingName;
			} else {
				int intOffetingToTerminate = Integer.parseInt(strOffetingToTerminate);
				for (int i = 0; i < intOffetingToTerminate; i++) {
					arrstrOfferingSel.add(arrstrOfferingName.get(i));
					
				}
				
			}	
			*/
			
			//Step 3: Create opportunity through offering management.
			objAcctpage.fn_clickOfferingManagementBtn(driver, webWait).isEnabled();
			objAcctpage.fn_clickOfferingManagementBtn(driver, webWait).click();
			Thread.sleep(3500);
			//Select all offering realted to active order
			
			arrNoOffetingToSelect = strOffetingToTerminate.split("#");	
			for (int i = 0; i < arrNoOffetingToSelect.length; i++) {
				webWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'"+arrNoOffetingToSelect[i]+"')]/ancestor::tr[1]/td[1]//input"))).click();
					
			}
			
			//Save Offering to Select  in global variable map
			EnvironmentDetails.mapGlobalVariable.put("arrNoOffetingToSelect", arrNoOffetingToSelect);
			Log.debug("DEBUG: Offering to Select save in global variable map with key 'arrstrOfferingSel'.");
			
			/*for (String strOfferingName : arrstrOfferingSel) {
				webWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='"+strOfferingName+"']/ancestor::tr[1]/td[1]//input"))).click();
			}*/
			//TODO:: Capture Screenshot for number of offering selected
			objUtils.fn_takeScreenshotFullPage(driver);
				
			objOfferingData.fn_clickCreateUpgDwgBtn(driver, webWait).click();
			Thread.sleep(3500);	
			objOfferingData.fn_clickCreateQuotechkbox(driver, webWait, "No").click();
			Thread.sleep(3500);
			objOfferingData.fn_clickConfirmUpgDwgBtn(driver, webWait).click();
			
			//Save Opportunity name in global variable map
			strOpportunityName = objOpportunites.fn_getOpportunityName(driver, webWait).getText();
			EnvironmentDetails.mapGlobalVariable.put("strOpportunityName", strOpportunityName);
			Log.debug("DEBUG: Opportunity name save in global variable map with key 'strOpportunityName'.");
			
			//TODO:: Capture Screenshot for Opportunity
			objUtils.fn_takeScreenshotFullPage(driver);
			
			//write data in output file 
			mapOutputData.put("Opportunity Name", strOpportunityName);
			lisOutputData.add(mapOutputData);
			lstmapOutputData.add(lisOutputData);
			objWriteExcel.fn_WriteExcel(lstmapOutputData, EnvironmentDetails.strFQNFileNameWrite, "Opportunity_Details");
			
			
			Thread.sleep(3500);	
			blnCreateOpportunity = true;
		} catch (Exception ExceptionCreateUpgDwg) {
			Log.error("ERROR: Unable to Create proposal from upgrade and downgrade with exception reported as: "+ExceptionCreateUpgDwg.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionCreateUpgDwg.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_CreateUpgDwg method is: "+stack);
           
		}
		return blnCreateOpportunity ;		
    }
	
	
	/*
	Author: Suhas Thakre
	Function fn_RemoveAutoRenew: Perform Early Termination and Auto Renew Off by legal Sales User.
	Inputs: strAccountName - Account name.
			strApttusQueryName - Apptus query to fetch offering details.
	Outputs: Boolean response to check success or failure of function .
	 */
	public Boolean fn_RemoveAutoRenew(WebDriver driver,WebDriverWait webWait, String strAccountName, String strApttusQueryName)  throws Exception
    {
		
		
		//TODO:: Perform Early Termination and Auto Renew Off by legal Sales User.
		/************************************************************************************************/
			//Step 1: Logout from existing User
			//Step 2: Login With legal Sales User.
			//Step 3: Perform Auto Renew Off.
			//Step 4: Perform Early Termination.
		
		/************************************************************************************************/
		Boolean blnRemoveAutoRenew = false;
		try {
			
			//Step 1: Logout from existing User
			ApptusLoginLogout.appLogout(driver, webWait);
			
			//Step 2: Login With legal Sales User.
			ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("LegalSalesUserName"), EnvironmentDetails.mapEnvironmentDetails.get("LegalSalesPassWord"));
			
			//Step 3: Perform Auto Renew Off.
			fn_RenewOffAndTermination(driver, webWait, strAccountName, strApttusQueryName, "Auto Renew Off");
			
			//Step 4: Perform Early Termination.
			fn_RenewOffAndTermination(driver, webWait, strAccountName, strApttusQueryName, "Early Termination");
			
			blnRemoveAutoRenew = true;
		}
		catch (Exception ExceptionRemoveAutoRenew) {
					Log.error("ERROR: Unable to Remove AutoRenew with exception reported as: "+ExceptionRemoveAutoRenew.getMessage());
		            StringWriter stack = new StringWriter();
		            ExceptionRemoveAutoRenew.printStackTrace(new PrintWriter(stack));
		            Log.debug("DEBUG: The exception in fn_RemoveAutoRenew method is: "+stack);
		           
		}
		
		return blnRemoveAutoRenew;
    }
	
	
	/*
	Author: Suhas Thakre
	Function fn_RenewOffAndTermination: To perform Auto Renew Off and Early Termination.
	Inputs: strAccountName - Account name.
			strApttusQueryName - Apptus query to fetch offering details.
			strTerminationType - To perform Auto Renew Off or Early Termination.
	Outputs: Boolean response to check success or failure of function .
	 */
	//Create proposal from upgrade and downgrade 
	public Boolean fn_RenewOffAndTermination(WebDriver driver,WebDriverWait webWait, String strAccountName, String strApttusQueryName, String strTerminationType)  throws Exception
    {
		
		//TODO:: To perform Auto Renew Off or Early Termination.
		/************************************************************************************************/
			//Step 1: Open Account Page
			//Step 2: Fetch offering related to active order with help of SOQL
			//Step 3: Perform Termination asper Termination Type
		
		/************************************************************************************************/
		
		
		ExcelHandler objExcelHandler = new ExcelHandler();
		Utils objUtils = new Utils();
		Account objAcctpage = new Account(); 
		OfferingData objOfferingData= new OfferingData();
		EnvironmentDetails objEnv = (EnvironmentDetails) EnvironmentDetails.mapGlobalVariable.get("objEnv");
		String[] arrNoOffetingToSelect  =  (String[]) EnvironmentDetails.mapGlobalVariable.get("arrNoOffetingToSelect");
		API_SOAP objAPI_SOAP = new API_SOAP();
		List<String> arrstrOfferingName = new ArrayList<>();
		Boolean blnRenewOffAndTermination = false;
		Map<String, Object> mapTestCases = new HashMap<String, Object>();
		
		//Read query from workbook ApttusQuery.xlsx and sheet ApttusQuerySheet
		String strApttusQuery = objExcelHandler.fn_ReadApttusQuery("ApttusQuery", "ApttusQuerySheet", "Offerings for Amendment");
		strApttusQuery = strApttusQuery.replace("AccountNameInput", strAccountName);
		
		try {
			
			//Step 1: Open Account Page
			objAcctpage.fn_setSearchField(driver, webWait).sendKeys(strAccountName); 
			objAcctpage.fn_clickSearchBtn(driver, webWait).click();
			Thread.sleep(3500);
			driver.findElement(By.partialLinkText(strAccountName)).click();
			
			/*//Step 2: Fetch offering related to active order with help of SOQL
			mapTestCases.put("CallType", "query");
			mapTestCases.put("CallingData", strApttusQuery);
			List<List<Map<String, String>>> lstmapSOAPRes = objAPI_SOAP.fn_SOAP_GET(objEnv, mapTestCases, null);
			//Add result to list 
				for (List<Map<String, String>> listmap : lstmapSOAPRes) {
					 
					 for (Map<String, String> map : listmap) {
						 
						 arrstrOfferingName.add(map.get("Name").toString());
					}
					
				}*/
			
			
			//handle change order for Ireland and non-Ireland 
			try {
				//executing change order for non Ireland 
				objAcctpage.fn_clickUpgDwgOpptyBtn(driver, new WebDriverWait(driver,10)).isEnabled();
				objAcctpage.fn_clickUpgDwgOpptyBtn(driver, webWait).click();
				objAcctpage.fn_clickOfferingManagementBtn(driver, webWait).isEnabled();
				objAcctpage.fn_clickOfferingManagementBtn(driver, webWait).click();
			} catch (Exception e) {
				//executing change order for Ireland
				objAcctpage.fn_clickOfferingManagementBtn(driver, webWait).isEnabled();
				objAcctpage.fn_clickOfferingManagementBtn(driver, webWait).click();
			}
			
			Thread.sleep(3500);
			objOfferingData.fn_clickRemoveAutoRenewBtn(driver, webWait).isEnabled();
			//Select all offering realted to active order
			for (int i = 0; i < arrNoOffetingToSelect.length; i++) {
				webWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'"+arrNoOffetingToSelect[i]+"')]/ancestor::tr[1]/td[1]//input"))).click();
					
			}
			
			/*for (String strOfferingName : arrstrOfferingName) {
				webWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[text()='"+strOfferingName+"']/ancestor::tr[1]/td[1]//input"))).click();
			}*/
			
			//TODO:: Capture Screenshot for number of offering selected
			objUtils.fn_takeScreenshotFullPage(driver);
				
			//Perform Auto Renew Off or Early Termination
			if (strTerminationType.equalsIgnoreCase("Auto Renew Off")) {
				
				objOfferingData.fn_clickRemoveAutoRenewBtn(driver, webWait).click();
				Thread.sleep(3500);
			}
			else if (strTerminationType.equalsIgnoreCase("Early Termination")) {
				
				objOfferingData.fn_clickEarlyTerminateBtn(driver, webWait).click();
				Thread.sleep(3500);
			}
			
			
			Thread.sleep(3500);	
			blnRenewOffAndTermination = true;
		} catch (Exception ExceptionRenewOffAndTermination) {
			Log.error("ERROR: Unable to Terminate Offerings with exception reported as: "+ExceptionRenewOffAndTermination.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionRenewOffAndTermination.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_RenewOffAndTermination method is: "+stack);
           
		}
		return blnRenewOffAndTermination ;		
    }
	
	
	/*
	Author: Suhas Thakre
	Function fn_UPDTerminationLogs: Accept Termination Logs.
	Inputs: strAccountName - Account name.
	Outputs: Boolean response to check success or failure of function .
	 */
	public Boolean fn_UPDTerminationLogs(WebDriver driver,WebDriverWait webWait, String strAccountName)  throws Exception
    {
		
		
		//TODO:: Accept Termination Logs.
		/************************************************************************************************/
			//Step 1: Capture All Termination Logs ID.
			//Step 2: Open Each Termination Logs.
			//Step 3: Accept Termination Logs.
			
		/************************************************************************************************/
		
		TerminationLog objTerminationLog= new TerminationLog();
		Boolean blnUPDTerminationLogs = false;
		String strTerminationLogIDs = "";
		List<String> lstTerminationLogIDs = new ArrayList<>();
		try {
			
			//Step 1: Capture All Termination Logs ID.
			List<WebElement> lsteleTerminationLogIDs = driver.findElements(By.xpath("//td[text()='Early Termination']//ancestor::tr[1]/th/a"));
			
			for (WebElement eleTerminationLogID : lsteleTerminationLogIDs) {
				strTerminationLogIDs = eleTerminationLogID.getAttribute("innerText");
				lstTerminationLogIDs.add(strTerminationLogIDs);
			}
			
			for (String strTerminationLogID : lstTerminationLogIDs) {
				//Step 2: Open Each Termination Logs.
				driver.findElement(By.linkText(strTerminationLogID)).click();
				Thread.sleep(3500);
				//Step 3: Accept Termination Logs.
				objTerminationLog.fn_clickEditBtn(driver, webWait).click();
				Thread.sleep(3500);
				objTerminationLog.fn_setTerminationDate(driver, webWait).click();
				Thread.sleep(3500);
				objTerminationLog.fn_selTerminationStatus(driver, webWait).selectByValue("Accepted");
				Thread.sleep(3500);
				objTerminationLog.fn_clickPaymentReceiveBtn(driver, webWait).click();
				Thread.sleep(3500);
				objTerminationLog.fn_clickSaveBtn(driver, webWait).click();
				Thread.sleep(3500);
				driver.findElement(By.linkText(strAccountName)).click();
				Thread.sleep(10000);
				
				
			}
			
			blnUPDTerminationLogs = true;
		}
		catch (Exception ExceptionUPDTerminationLogs) {
					Log.error("ERROR: Unable to Accept Termination Logs with exception reported as: "+ExceptionUPDTerminationLogs.getMessage());
		            StringWriter stack = new StringWriter();
		            ExceptionUPDTerminationLogs.printStackTrace(new PrintWriter(stack));
		            Log.debug("DEBUG: The exception in fn_UPDTerminationLogs method is: "+stack);
		           
		}
		
		
		return blnUPDTerminationLogs;
    }
	
	
	/*
	Author: Suhas Thakre
	Function fn_TerminateOpp: To Terminate Opportunity by Sales User 1.
	Inputs: strAccountName - Account name.
	Outputs: Boolean response to check success or failure of function .
	 */
	public Boolean fn_TerminateOpp(WebDriver driver,WebDriverWait webWait, String strAccountName)  throws Exception
    {
		
		
		//TODO:: To Terminate Opportunity.
		/************************************************************************************************/
			//Step 1: Logout from existing User
			//Step 2: Login With legal Sales User 1.
			//Step 3: Search Account and Open Opportunity Page.
			//Step 4: Terminate Opportunity.
		
		/************************************************************************************************/
		
		Account objAcctpage = new Account();
		OpportunitesPageObject objOpportunites= new OpportunitesPageObject();
		String strOpportunityName = EnvironmentDetails.mapGlobalVariable.get("strOpportunityName").toString();
		Boolean blnTerminateOpp = false;
		try {
			
			//Step 1: Logout from existing User
			ApptusLoginLogout.appLogout(driver, webWait);
			
			//Step 2: Login With legal Sales User.
			ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("LegalSalesUserName"), EnvironmentDetails.mapEnvironmentDetails.get("LegalSalesPassWord"));
			
			//Step 3: Search Account and Open Opportunity Page.
			objAcctpage.fn_setSearchField(driver, webWait).sendKeys(strAccountName); 
			objAcctpage.fn_clickSearchBtn(driver, webWait).click();
			Thread.sleep(3500);
			driver.findElement(By.linkText(strAccountName)).click();
			Thread.sleep(3500);
			driver.findElement(By.linkText(strOpportunityName)).click();
			Thread.sleep(3500);
			
			//Step 4: Terminate Opportunity.
			objOpportunites.fn_clickTerminatebtn(driver, webWait).click();
			Thread.sleep(3500);
		 	Alert alert = driver.switchTo().alert();
		 	alert.accept();
		 	Thread.sleep(3500);
		 	blnTerminateOpp = true;
		}
		catch (Exception ExceptionTerminateOpp) {
					Log.error("ERROR: Unable to Terminate Opportunity with exception reported as: "+ExceptionTerminateOpp.getMessage());
		            StringWriter stack = new StringWriter();
		            ExceptionTerminateOpp.printStackTrace(new PrintWriter(stack));
		            Log.debug("DEBUG: The exception in fn_TerminateOpp method is: "+stack);
		           
		}
		
		
		return blnTerminateOpp;
    }

	/*
	Author: Suhas Thakre
	Function fn_LostOpp: Update Opportunity with lost Stage.
	Inputs: strAccountName - Account name.
	Outputs: Boolean response to check success or failure of function .
	 */
	public Boolean fn_LostOpp(WebDriver driver,WebDriverWait webWait, String strAccountName,String strOppDetails)  throws Exception
    {
		
		
		//TODO:: Update Opportunity with lost Stage.
		/************************************************************************************************/
			//Step 1: Logout from existing User
			//Step 2: Login With legal Sales User 7.
			//Step 3: Search Account and Open Opportunity Page.
			//Step 4: Update Stage to Lost.
		
		/************************************************************************************************/
		
		Account objAcctpage = new Account();
		AfterApproval objAfterPO = new AfterApproval();
		String strOpportunityName = EnvironmentDetails.mapGlobalVariable.get("strOpportunityName").toString();
		String[] arrOppDetails = strOppDetails.split("#");
		Boolean blnLostOpp = false;
		try {
			
			//Step 1: Logout from existing User
			ApptusLoginLogout.appLogout(driver, webWait);
			
			//Step 2: Login With legal Sales User.
			ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("SalesUser7UserName"), EnvironmentDetails.mapEnvironmentDetails.get("SalesUser7PassWord"));
			
			//Step 3: Search Account and Open Opportunity Page.
			objAcctpage.fn_setSearchField(driver, webWait).sendKeys(strAccountName); 
			objAcctpage.fn_clickSearchBtn(driver, webWait).click();
			Thread.sleep(3500);
			driver.findElement(By.linkText(strAccountName)).click();
			Thread.sleep(3500);
			driver.findElement(By.linkText(strOpportunityName)).click();
			Thread.sleep(3500);
			
			//Step 4: Update Stage to Lost.
			objAfterPO.fn_clickEditBtn(driver, webWait).click();
			Thread.sleep(3500);
			objAfterPO.fn_selStage(driver, webWait).selectByVisibleText(arrOppDetails[0]);
			Thread.sleep(3500);
			//Code to Add Multiple Options for Competitors
			String strOptions="";
			List<WebElement> lstwebAllOptionsAdd  = objAfterPO.fn_selectMultipleOnCompetitors(driver, webWait).getOptions(); 
			for(WebElement webOptions : lstwebAllOptionsAdd ){
				 strOptions = webOptions.getText();
				
				if (strOptions.contains(arrOppDetails[1]))
				      webOptions.click();}
			objAfterPO.fn_ClickImgAddOnAddressEditPage(driver, webWait).click();
			objAfterPO.fn_clickOpptySaveBtn(driver, webWait).click();
			Thread.sleep(3500);
			blnLostOpp = true;
		}
		catch (Exception ExceptionTerminateOpp) {
					Log.error("ERROR: Unable to Terminate Opportunity with exception reported as: "+ExceptionTerminateOpp.getMessage());
		            StringWriter stack = new StringWriter();
		            ExceptionTerminateOpp.printStackTrace(new PrintWriter(stack));
		            Log.debug("DEBUG: The exception in fn_TerminateOpp method is: "+stack);
		           
		}
		
		
		return blnLostOpp;
    }


	
	/*
	Author: Suhas Thakre
	Function fn_VerifyLineItems: Verify All Asset Line Items Status.
	Inputs: strAccountName - Account name.
	Outputs: Boolean response to check success or failure of function .
	 */
	public Boolean fn_VerifyLineItems(WebDriver driver,WebDriverWait webWait, String strAccountName, String strApttusQueryName)  throws Exception
    {
		
		
		//TODO:: Verify All Asset Line Items Status.
		/************************************************************************************************/
			//Step 1: Open Account Page.
			//Step 2: Verify All Asset Line Items Status..
		
		/************************************************************************************************/
		
		Account objAcctpage = new Account();
		//ExcelHandler objExcelHandler = new ExcelHandler();
		//API_SOAP objAPI_SOAP = new API_SOAP();
		//EnvironmentDetails objEnv = (EnvironmentDetails) EnvironmentDetails.mapGlobalVariable.get("objEnv");
		String[] arrNoOffetingToSelect  =  (String[]) EnvironmentDetails.mapGlobalVariable.get("arrNoOffetingToSelect");
		String strOrderAgreement = "";
		List<WebElement> lstAssetLineItems = new ArrayList<>() ;
		//List<String> arrstrOfferingNames = new ArrayList<>();
		Boolean blnVerifyLineItems = false;
		
		try {
			
			//Step 1: Logout from existing User
			ApptusLoginLogout.appLogout(driver, webWait);
			
			//Step 2: Login With legal Sales User.
			ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("LegalSalesUserName"), EnvironmentDetails.mapEnvironmentDetails.get("LegalSalesPassWord"));
			
			//Step 1: Search Account.
			objAcctpage.fn_setSearchField(driver, webWait).sendKeys(strAccountName); 
			objAcctpage.fn_clickSearchBtn(driver, webWait).click();
			Thread.sleep(3500);
			driver.findElement(By.linkText(strAccountName)).click();
			Thread.sleep(3500);
			
			strOrderAgreement = objAcctpage.fn_GetOrderAgreement(driver, webWait).getText();
			
			//Click on show more records 
			try {
				objAcctpage.fn_ClickShowMoreLineItems(driver, new WebDriverWait(driver,15), strOrderAgreement).click();
				Thread.sleep(3500);
			} catch (Exception e) {
				Log.debug("DEBUG: No more records present.");
			}
			
			/*	//Get all asset line iteam
			lstAssetLineItems = objAcctpage.fn_GetLineItems(driver, webWait, strOrderAgreement);
			
			for (WebElement AssetLineItem : lstAssetLineItems) {
				
				assertEquals(AssetLineItem.getText().trim(),"Early Terminated");
				
			}*/
			
		/*	//Get Offering names with help of SOQL
			String strApttusQuery = objExcelHandler.fn_ReadApttusQuery("ApttusQuery", "ApttusQuerySheet", strApttusQueryName);
			strApttusQuery = strApttusQuery.replace("AccountNameInput", strAccountName);
			//Step 1 
			List<List<Map<String, String>>> lstmapSOAPRes = objAPI_SOAP.fn_SOAP_GET(objEnv, strApttusQuery);
			//Add result to list 
				for (List<Map<String, String>> listmap : lstmapSOAPRes) {
					 
					 for (Map<String, String> map : listmap) {
						 
						 arrstrOfferingNames.add(map.get("Name").toString());
					}
					
				}
			*/
			
			
			
			//Read Asset line for each offering 
			for (int i = 0; i < arrNoOffetingToSelect.length; i++) {
				lstAssetLineItems = objAcctpage.fn_GetLineItemsOffering(driver, webWait, arrNoOffetingToSelect[i]);
				for (WebElement AssetLineItem : lstAssetLineItems) {
					
					assertEquals(AssetLineItem.getText().trim(),"Terminated");
					
				}
					
			}
			
			/*for (String strOfferingNames : arrstrOfferingSel) {
				lstAssetLineItems = objAcctpage.fn_GetLineItemsOffering(driver, webWait, strOfferingNames);
					for (WebElement AssetLineItem : lstAssetLineItems) {
						
						assertEquals(AssetLineItem.getText().trim(),"Terminated");
						
					}
			}	
			*/
			blnVerifyLineItems = true;
		}
		catch (Exception ExceptionTerminateOpp) {
					Log.error("ERROR: Unable to Verify All Asset Line Items Status with exception reported as: "+ExceptionTerminateOpp.getMessage());
		            StringWriter stack = new StringWriter();
		            ExceptionTerminateOpp.printStackTrace(new PrintWriter(stack));
		            Log.debug("DEBUG: The exception in fn_VerifyLineItems method is: "+stack);
		}
		
		
		return blnVerifyLineItems;
    }

	
}
