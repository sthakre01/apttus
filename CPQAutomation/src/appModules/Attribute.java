package appModules;

/**
 * Created by joe.gannon on 6/23/2016.
 */
public class Attribute
{
    private String attributeProductName;
    private String attributeValue;
    private String attributeName;
    private String attributeFieldType;

    public String getAttributeProductName() {
        return attributeProductName;
    }

    public void setAttributeProductName(String attributeProductName) {
        this.attributeProductName = attributeProductName;
    }

    public String getAttributeValue() {
        return attributeValue;
    }

    public void setAttributeValue(String attributeValue) {
        this.attributeValue = attributeValue;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    public String getAttributeFieldType() {
        return attributeFieldType;
    }

    public void setAttributeFieldType(String attributeFieldType) {
        this.attributeFieldType = attributeFieldType;
    }

    public void print()
    {
        System.out.println("~~~~~~~~~~~~~~~~~~~Attribute~~~~~~~~~~~~~~~~~~~");
        System.out.println("\tProduct Name: "+this.attributeProductName);
        System.out.println("\tValue: "+this.attributeValue);
        System.out.println("\tAttribute Name: "+this.attributeName);
        System.out.println("\tField Type: "+this.attributeFieldType);
    }
}
