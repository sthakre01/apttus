/*
 Author     		:	Madhavi JN.
 Class Name			: 	CreateOfferingData 
 Purpose     		: 	Purpose of this file is :
						1. To perform  CreateOfferingData functions.
 Date       		:	04/10/2016 

 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/


package appModules;


import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utility.API_SOAP;
import utility.EnvironmentDetails;
import utility.EnvironmentDetailsSAS;
import utility.ExcelHandler;
import utility.Log;
import utility.Utils;
import utility.WriteExcel;
import pageObjects.SalesForce.Account;
import pageObjects.SalesForce.Configuration;
import pageObjects.SalesForce.LeadCreation;
import pageObjects.SalesForce.OfferingData;
import pageObjects.SalesForce.OpportunitesFormPageObject;
import pageObjects.SalesForce.OpportunitesPageObject;
import pageObjects.SalesForce.Proposal;

public class CreateOfferingData {
	
	/*
	Author: Suhas Thakre
	Function fn_CreateOfferingData: Create new Offerings.
	Inputs: strOfferingDataSheetName - sheet name where input data is present.
			intNewOfferingCount - Number of offering to create.
	Outputs: Boolean response to check success or failure of function .
	 */
	public Boolean fn_CreateOfferingData(WebDriver driver,WebDriverWait webWait,String strOfferingDataSheetName) throws Exception
    {
		
		//TODO:: Create multiple new offering 
		/************************************************************************************************/
			//Step 1: Generate unique offering name 
			//Step 2: Enter data in mandatory fields
			//Step 3: Save name of generated new offering in  global variable map 
		
		/************************************************************************************************/
		
		Utils objUtils = new Utils();
		ExcelHandler objExcel = new ExcelHandler();
		EnvironmentDetailsSAS   objEnvDetails = new EnvironmentDetailsSAS();
		OfferingData objOfferingData = new OfferingData();
		Configuration objConfig = new Configuration();
		boolean blnCreateOfferingData=false;
		List<String> arrNewOfferingNames = new ArrayList<>();		
		Map<String, Object> mapOutputData = new HashMap<String, Object>();
		List<Map<String, Object>> lisOutputData = new ArrayList<>();
		List<List<Map<String, Object>>> lstmapOutputData = new ArrayList<>();
		WriteExcel objWriteExcel = new WriteExcel();
		String strCreateOfferingWB = "TestData.xlsx";
		String strCreateOfferingSheet = "New_Offering_Data";
		List<Map<String, String>> lstmapCreateOfferingData = new ArrayList<Map<String,String>>();
		String strCustReference= EnvironmentDetails.mapGlobalVariable.get("strCustReference").toString();
		Integer intNewOfferingCount = Integer.parseInt(EnvironmentDetails.mapGlobalVariable.get("strNewOfferingCount").toString().trim());
		String strDeliveryLocation = "" ;
		String strOfferingDataType = "" ;
		String strTerm = "" ;
		String strEndDate = "";
		String strStartDate = "";
		String strBillingDate = "";
		String strBF = "";
		
		//Read Offering Data from Excel 
		String excelResource = EnvironmentDetails.Path_ExternalFiles
				+ System.getProperty("file.separator") + strCreateOfferingWB;
		File fileLeadCreationData = new File(excelResource);
		lstmapCreateOfferingData = objExcel.fn_ReadExcelAsMap(fileLeadCreationData,strCreateOfferingSheet); 
		for (Map<String, String> mapCreateOfferingData : lstmapCreateOfferingData) {
			strDeliveryLocation = mapCreateOfferingData.get("Delivery Location").toString().trim();
			strOfferingDataType = mapCreateOfferingData.get("Offering Data Type").toString().trim();
			strTerm = mapCreateOfferingData.get("Term").toString().trim();
			strStartDate = mapCreateOfferingData.get("Start_Date").toString().trim();
			strBillingDate = mapCreateOfferingData.get("Billing_Date").toString().trim();
			strBF = mapCreateOfferingData.get("Billing Frequency").toString().trim();
		}
		
		//change date format as per country 
		SimpleDateFormat USformat = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat NonUSformat = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat Inputformat = new SimpleDateFormat("dd/MMM/yyyy");
		Date dateStartDate = Inputformat.parse(strStartDate);
		Date dateBillingDate = Inputformat.parse(strBillingDate);
		//Date dateEndDate = Inputformat.parse(strEndDate);
		if (EnvironmentDetails.mapGlobalVariable.get("strCountry").toString().equalsIgnoreCase("United States")) {
			strStartDate = USformat.format(dateStartDate);
			strBillingDate = USformat.format(dateBillingDate);
			//strEndDate = USformat.format(dateEndDate);
		} else {
			strStartDate = NonUSformat.format(dateStartDate);
			strBillingDate = NonUSformat.format(dateBillingDate);
			//strEndDate = NonUSformat.format(dateEndDate);
		}
			
		//No new offering created 	
		if (intNewOfferingCount == 0) {
			EnvironmentDetails.mapGlobalVariable.put("arrNewOfferingNames", null);
		}
		
		//loop to create multiple new offering 
		for (int i = 0; i < intNewOfferingCount; i++) {
				
					if (strOfferingDataType !=null && strOfferingDataType.equalsIgnoreCase("New Offering Data")) {
	
					//Click on Button NewOffering Data/edit the existing offering data
					objOfferingData.fn_clickNewOfferData(driver, webWait).click();	
					
					
					//Customer Reference data not available in lookup
					//objOfferingData.fn_setOfferingDataName(driver, webWait).sendKeys("Test_Automation");
					/*Thread.sleep(2000);	
			       Boolean blnCustReferenceHandle = objUtils.fn_HandleLookUpWindow(driver,webWait,"Customer Reference",arrOfferData[0]);
			        if (blnCustReferenceHandle==true)
			        	Log.info("Data is entered successfully in Customer Reference Lookup (New Window)");
			        else
			        	Log.info("Unable to enter data in Customer Reference Lookup (New Window)");*/
			        
			        Thread.sleep(2000);	
			        //Enter Offering data name and save in global variable map 
			        
			        int outdatacount = 1;
			        //Step 1
			        Date date = new Date();
					SimpleDateFormat dateFormat = new SimpleDateFormat("mmss");  		
					String strNewOfferingDataName = "New_Off_"+dateFormat.format(date);
					mapOutputData.put("New Offering Data Name "+outdatacount, strNewOfferingDataName);
					outdatacount++;
					
					//Step 3
					arrNewOfferingNames.add(strNewOfferingDataName);
					EnvironmentDetails.mapGlobalVariable.put("arrNewOfferingNames", arrNewOfferingNames);
					Log.debug("DEBUG: Saved New Offering Name in global variable map with Key arrNewOfferingNames.");
					
					//Enter new offering data name
					objOfferingData.fn_setOfferingDataName(driver, webWait).sendKeys(strNewOfferingDataName);
					
					//Enter the Customer Reference
			        objOfferingData.fn_setCustomerReference(driver, webWait).clear();
			        objOfferingData.fn_setCustomerReference(driver, webWait).sendKeys(strCustReference);
					
					//Select Delivery Location
			        Boolean blnDeliveryLocationHandle = objUtils.fn_HandleLookUpWindow(driver,webWait,"Delivery Location",strDeliveryLocation);
			        if (blnDeliveryLocationHandle==true)
			        	Log.debug("DEBUG: Data is entered successfully in Delivery Location Lookup (New Window)");
			        else
			        	Log.debug("DEBUG: Unable to eneter data in Delivery Location Lookup (New Window)");
			        Thread.sleep(2000);	
			        
			        
			        
			        //Enter the Start Date
			        objOfferingData.fn_setOfferingStartDate(driver, webWait).sendKeys(strStartDate);;
			      //change the focus to Term
			        objOfferingData.fn_setTerm(driver, webWait).click();
			        
			        
			               
			        //Enter the Term
			        //objOfferingData.fn_setTerm(driver, webWait).clear();
			        //objOfferingData.fn_setTerm(driver, webWait).sendKeys("12");    
			        
			        //Enter the Billing Date
			        objOfferingData.fn_setBillingDate(driver, webWait).sendKeys(strBillingDate);; 
			        //change the focus to Term
			        objOfferingData.fn_setTerm(driver, webWait).click();
			        
			        //Enter the Renewal Term
			        /*objOfferingData.fn_setAutoRenewalTerm(driver, webWait).sendKeys(strTerm);
			        objOfferingData.fn_setTerm(driver, webWait).sendKeys(strTerm);*/
			        
			        	        
			        
			        /*//Product Family field  not available on page
			        Boolean blnProductFamilyHandle = objUtils.fn_HandleLookUpWindow(driver,webWait,"Product Family",arrOfferData[2]);
			        if (blnProductFamilyHandle==true)
			        	Log.info("Data is entered successfully in Product Family Lookup (New Window)");
			        else
			        	Log.info("Unable to eneter data in Product Family Lookup (New Window)");*/
			        
			       
			         
			        //AnnualPriceIncrease field not available on UI
			        //Enter the AnnualPriceIncrease
			        //objOfferingData.fn_setAnnualPI(driver, webWait).sendKeys("3");
			        
			        /*//Price List field not available on UI
			        //Boolean blnPricelistHandle = objUtils.fn_HandleLookUpWindow(driver,webWait,"Price List",arrOfferData[3]);
			       // if (blnPricelistHandle==true)
			        //	Log.info("Data is entered successfully in Price List Lookup (New Window)");
			       // else
			        	//Log.info("Unable to enter data in Price List Lookup (New Window)");
			         */					
				
				} else {
					objOfferingData.fn_clickExistingOfferingDataLink(driver, webWait).click();
					Thread.sleep(8000); }          		
				
				Thread.sleep(2000);	
				//Select the Billing Frequency	
				objOfferingData.fn_selBillingFrequency(driver, webWait).selectByValue(strBF);
		        Thread.sleep(1000);
		                
		        objOfferingData.fn_selAnnualIncreasePL(driver, webWait).selectByIndex(1);
		        Thread.sleep(1000);
		        
		        //Select the Bill To Contact
		        objOfferingData.fn_selBillingContact(driver, webWait).selectByIndex(1);
		        
		        //Select the Notification Contact
		        objOfferingData.fn_selNotificationContact(driver, webWait).selectByIndex(1);
		        
		      //Enter End Date
		        //change date format as per country
		        String futureDate = objUtils.fn_getFutureDate(12, "dd/MMM/yyyy");
		        Date datea = Inputformat.parse(futureDate);
				if (EnvironmentDetails.mapGlobalVariable.get("strCountry").toString().equalsIgnoreCase("United States")) {
					strEndDate = USformat.format(datea);
				} else {
					strEndDate = NonUSformat.format(datea);
				}
		        Thread.sleep(3500);
				try {        			
			        objOfferingData.fn_setEndDate(driver, webWait).sendKeys(strEndDate);}
				catch(Exception excpEnvironment1)	{        			
					objOfferingData.fn_setExpirationDate(driver, webWait).sendKeys(strEndDate);
				    }
		        //change the focus to Term
		        objOfferingData.fn_setTerm(driver, webWait).click();
		        Thread.sleep(3500);
		        
		        //Select the Covered Location
		        objOfferingData.fn_selCoveredLocation(driver, webWait).click();
		        
		        Actions act = new Actions(driver);
				objOfferingData.fn_setChangeRenewTerm(driver, webWait).isDisplayed();
				objOfferingData.fn_setChangeRenewTerm(driver, webWait).clear();
				objOfferingData.fn_setChangeRenewTerm(driver, webWait).sendKeys(strTerm);
				act.moveToElement(objOfferingData.fn_setChangeTerm(driver, webWait)).doubleClick().build().perform();
				objOfferingData.fn_setChangeTerm(driver, webWait).sendKeys(strTerm);
				//change the focus to Term
		        objOfferingData.fn_setTerm(driver, webWait).click();
		        
		        //click on Save button
		        objOfferingData.fn_clickSaveBtn(driver, webWait).click();
		        Thread.sleep(10000);
		        
		        //TODO:: Capture Screenshot for New offering 
				objUtils.fn_takeScreenshotFullPage(driver);
		}
		
		//write data in output file 
		//objUtils.fn_WriteExcelApttus("New_Offering_Data_Details"+EnvironmentDetails.mapGlobalVariable.get("struniquekey").toString(), mapOutputData);
		
		
		
		blnCreateOfferingData=objConfig.fn_clickConfigureBtn(driver, webWait).isDisplayed();
		return blnCreateOfferingData;
		
    }
		
	/*
	Author: Suhas Thakre
	Function fn_fn_UpdOfferingData : Open existing offering and update field required for new order.
	Inputs: mapOfferingNames - Map of existing offerings name.
	Outputs: Boolean response to check success or failure of function .
	 */
	public static Boolean fn_UpdOfferingData(WebDriver driver,WebDriverWait webWait, Map<String,String> mapOfferingNames, Map<String, List<String>> mapUPDOffering ) throws Throwable
	{
		//TODO:: Update Existing Offering  
		/************************************************************************************************/
			//Step 1: iterate to all offering and open it. 
			//Step 2: Update Offering Data.
			
		
		/************************************************************************************************/
		
		WriteExcel objWriteExcel = new WriteExcel();
		Utils objUtils = new Utils();
		String strOfferingName = "";
		
		//Map<String, Integer> map = new HashMap<String, Integer>();
		String strProposalNumber = EnvironmentDetails.mapGlobalVariable.get("strNewproposalName").toString();
		String strBillingDate = "";
		Boolean blnUpdOffering = false;
		Map<String, Object> mapOutputData = new HashMap<String, Object>();
		List<Map<String, Object>> lisOutputData = new ArrayList<>();
		List<List<Map<String, Object>>> lstmapOutputData = new ArrayList<>();

		//Save Offering Names in global variable map 
	    EnvironmentDetails.mapGlobalVariable.put("mapOfferingNames", mapOfferingNames);
	    Log.debug("DEBUG: Offering Name save in global variable map with key 'mapOfferingNames'.");
	    
	    
		
		try
		{
		Log.debug("DEBUG: Execution of function 'fn_UpdOfferingData' Started.");
		
		//Step 1
	    for(Entry<String, String> mapOfferingName: mapOfferingNames.entrySet()) {
	    	
	    	
	    	//Step 2
	    	strOfferingName = mapOfferingName.getValue();
	    	
	    	List<String> lstUPDOffering = mapUPDOffering.get(strOfferingName);
	    	
	    	OfferingData objOfferingData = new OfferingData();
			objOfferingData.fn_clickExistingMulOfferingLink(driver, webWait, strOfferingName).click();
			Thread.sleep(8000);           		
			
			objOfferingData.fn_clickEditbtn(driver, webWait).click();
			Thread.sleep(3500);
			
			//Select the Billing Frequency	
		    objOfferingData.fn_selBillingFrequency(driver, webWait).selectByValue(lstUPDOffering.get(2));
		    Thread.sleep(1000);
		     
		    //Select the Annual Price Increase PL
		    objOfferingData.fn_selAnnualIncreasePL(driver, webWait).selectByValue(lstUPDOffering.get(1));
		    Thread.sleep(1000);
		    
		    //Select the Bill To Contact
		    objOfferingData.fn_selBillingContact(driver, webWait).selectByIndex(1);
		    
		    //Select the Notification Contact
		    objOfferingData.fn_selNotificationContact(driver, webWait).selectByIndex(1);
		    
		    //Select the Covered Location
		    objOfferingData.fn_selCoveredLocation(driver, webWait).click();
		    
		    //Set Billing Date
		    objOfferingData.fn_setBillingDate(driver, webWait).clear();
		    Thread.sleep(1500);
		    
		    //change date format as per country 
			SimpleDateFormat USformat = new SimpleDateFormat("MM/dd/yyyy");
			SimpleDateFormat NonUSformat = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat Inputformat = new SimpleDateFormat("dd/MMM/yyyy");
			Date date = Inputformat.parse(lstUPDOffering.get(0));
			if (EnvironmentDetails.mapGlobalVariable.get("strCountry").toString().equalsIgnoreCase("United States")) {
				strBillingDate = USformat.format(date);
			} else {
				strBillingDate = NonUSformat.format(date);
			}
		    
		    objOfferingData.fn_setBillingDate(driver, webWait).sendKeys(strBillingDate);
		    
		    //click on Save button
		    objOfferingData.fn_clickSaveBtn(driver, webWait).click();
		    Thread.sleep(5000);
		    
		    //TODO:: Capture Screenshot for offering 
		    objUtils.fn_takeScreenshotFullPage(driver);
		    driver.findElement(By.linkText(strProposalNumber)).click();
		    Thread.sleep(3500);
		    
		    //Data for Output file
		    mapOutputData.put(mapOfferingName.getKey(), mapOfferingName.getValue());
		    
	    }
	    
	    //TODO:: Write Offering name in output file 
	    objUtils.fn_WriteExcelApttus("Offering_Data_Details"+EnvironmentDetails.mapGlobalVariable.get("struniquekey").toString(), mapOutputData);
	    
	    
		}
	    catch (Exception ExceptionUpdOffering) {
			Log.error("ERROR: Unable to Update Offering with exception reported as: "+ExceptionUpdOffering.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionUpdOffering.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_fn_UpdOfferingData method is: "+stack);
           
		}
	    Log.debug("DEBUG: Execution of function 'fn_fn_UpdOfferingData' Completed.");
	    blnUpdOffering = true;
		return blnUpdOffering;
	
	}
	
	/*
	Author: Suhas Thakre
	Function fn_SelectProductFamily: Select Product family to create multiple offering.
	Inputs: strApttusQueryName - SOQL to fetch product family name with the help of product name.
			strProductInfoSheet - Data sheet name containing product name to be configure.
			strProductsDataSheet - Workbook name containing ProductInfo Sheet.
	Outputs: Boolean response to check success or failure of function .
	 */
	public Boolean fn_SelectProductFamily(WebDriver driver,WebDriverWait webWait, String strApttusQueryName, String strProductInfoSheet
										, String strCreateOfferingDataSheetName, String strProductInfoWBName, String strCreateOfferingWBName ) throws Throwable
	{
		
		//TODO:: Select Product family  
		/************************************************************************************************/
			//Step 1: Open Opportunity page. 
			//Step 2: Read Product Name from Product_Information sheet.
			//Step 3: Fetch Product family from Product name with help of Apttus query.
			//Step 4: Add Data to Map with key as product name and value as family
			//Step 4: Search and select Product family.
			//Step 5: Enter Customer reference for non US country
		
		/************************************************************************************************/
		
		Account objAccount = new Account();
		OpportunitesPageObject objOpp = new OpportunitesPageObject();
		OpportunitesFormPageObject objOppForm = new OpportunitesFormPageObject();
		LeadCreation ObjLead = new LeadCreation();
		ExcelHandler objExcel = new ExcelHandler();
		//AfterApproval objAfterPO = new AfterApproval();
		API_SOAP objAPI_SOAP = new API_SOAP();
		WriteExcel objWriteExcel = new WriteExcel();
		Utils objUtils = new Utils();
		EnvironmentDetails objEnv = (EnvironmentDetails) EnvironmentDetails.mapGlobalVariable.get("objEnv");
		String strCountry = EnvironmentDetails.mapGlobalVariable.get("strCountry").toString();
		String excelResource = EnvironmentDetails.Path_ExternalFiles
								+ System.getProperty("file.separator") + strProductInfoWBName;
		File fileProductInfo = new File(excelResource);
		List<Map<String, String>> lstProductMap = new ArrayList<Map<String,String>>();
		Set<String> setProductFamily = new HashSet<String>(); 
		Set<String> setstrProductName = new HashSet<String>(); 
		String strOpportunityName= "";
		String strToBeConfigured= "";
		//String strProductName= "";
		String strCustReference= "";
		String strContact= "";
		Map<String, String> mapProductAndFamily = new HashMap<String, String>();
		Map<String, Object> mapOutputData = new HashMap<String, Object>();
		List<Map<String, Object>> lisOutputData = new ArrayList<>();
		List<List<Map<String, Object>>> lstmapOutputData = new ArrayList<>();
		Map<String, Object> mapTestCases = new HashMap<String, Object>();
		mapTestCases.put("CallType", "query");
		
		
		try {	
			
			//Close reminder window
	        objUtils.fn_CloseRemainder(driver, webWait);
			
			Log.debug("DEBUG: Execution of function 'fn_SelectProductFamily' Started.");
			//Read query from workbook ApttusQuery.xlsx and sheet ApttusQuerySheet
			String strApttusQuery = objExcel.fn_ReadApttusQuery("ApttusQuery", "ApttusQuerySheet", strApttusQueryName);
			
			//Step 1
			Thread.sleep(5000);
			
			//Save Customer reference in global variable map
			strCustReference = objAccount.fn_GetCustReference(driver, webWait).getText();
			EnvironmentDetails.mapGlobalVariable.put("strCustReference", strCustReference);
			Log.debug("DEBUG: Customer reference save in global variable map with key 'strCustReference'.");
			
			//Save Contacts name in global variable map
				//handle Dev and INT env.
				try {
					strContact = objAccount.fn_GetActiveContacts(driver, new WebDriverWait(driver,15), "Contacts").getText();
				} catch (Exception e) {
					strContact = objAccount.fn_GetActiveContacts(driver, webWait, "Active Contacts").getText();
				}
			
			EnvironmentDetails.mapGlobalVariable.put("strContact", strContact);
			Log.debug("DEBUG: Contacts name save in global variable map with key 'strContact'.");
					
			//Save Opportunity name in global variable map
			strOpportunityName = objAccount.fn_clickOpportunityName(driver, webWait).getText();
			EnvironmentDetails.mapGlobalVariable.put("strOpportunityName", strOpportunityName);
			Log.debug("DEBUG: Opportunity name save in global variable map with key 'strOpportunityName'.");
			
			
			objAccount.fn_clickOpportunityName(driver, webWait).isEnabled();
			objAccount.fn_clickOpportunityName(driver, webWait).click();
			//TODO:: Capture Screenshot for Opportunity 
			objUtils.fn_takeScreenshotFullPage(driver);
			
			//TODO:: Write Opportunity Name in output file
			mapOutputData.put("Opportunity Name", strOpportunityName);
			objUtils.fn_WriteExcelApttus("Opportunity_Names"+EnvironmentDetails.mapGlobalVariable.get("struniquekey").toString(), mapOutputData);
			
			
			
			//Click Add product button 
			ObjLead.fn_clickAddProductBtn(driver, webWait).click();
			
			try {
				//Select price book for oppertunity 
				if (ObjLead.fn_clickChoosePriceBook(driver, new WebDriverWait(driver,3)).isDisplayed()) {
					ObjLead.fn_clickSaveBtn(driver, webWait).isEnabled();
					
					//Select price book for UAT depending on country 
					if (EnvironmentDetails.mapEnvironmentDetails.get("Environment").equalsIgnoreCase("UAT") 
						&& EnvironmentDetails.mapGlobalVariable.get("strCountry").toString().equalsIgnoreCase("United States")) {
						ObjLead.fn_selectPricebook(driver, webWait).selectByVisibleText("North America");
					}
					
					
					ObjLead.fn_clickSaveBtn(driver, webWait).click();
				}
			} catch (Exception e) {
				Log.debug("DEBUG: No need to select price book.");
			}
			
			
			
			//Step 2: Read Product Name from Product_Information sheet.
			lstProductMap = objExcel.fn_ReadExcelAsMap(fileProductInfo,strProductInfoSheet); 
			//Search yes condition in all row of Product_Information sheet
			for (Map<String, String> mapProduct : lstProductMap) {
				if (mapProduct.get("Uniquekey").toString().trim().equalsIgnoreCase(EnvironmentDetails.mapGlobalVariable.get("struniquekey").toString())) {
					strToBeConfigured = mapProduct.get("To_Be_configured").toString().trim();
					if (strToBeConfigured.equalsIgnoreCase("Yes"))
					{	 
						setstrProductName.add(mapProduct.get("Product_Name").toString().trim());
						
					}
				}
			}
			
			//Step 3: Fetch Product family from Product name with help of Apttus query.
			for (String strProductNamel : setstrProductName) {
				
				String strUpdApttusQuery = strApttusQuery;
				//Update SOQL for each product
				strUpdApttusQuery = strUpdApttusQuery.replace("ProductNameInput", strProductNamel);
				
				//Step 3 
				mapTestCases.put("CallingData", strUpdApttusQuery);
				List<List<Map<String, String>>> lstmapSOAPRes = objAPI_SOAP.fn_SOAP_GET(objEnv, mapTestCases, null);
				//Add result to list 
					for (List<Map<String, String>> listmap : lstmapSOAPRes) {
						 
						 for (Map<String, String> map : listmap) {
							 
							 try {
								 setProductFamily.add(map.get("Family").toString());
								 
								 
								 
								//Step 4
								 //Update Map to get Product And Family Relationship 
								 mapProductAndFamily.put(strProductNamel, map.get("Family").toString());
								 
							} catch (Exception ExceptionDuplicateValue) {
								Log.debug("DEBUG: Value already exist in set.");
							}
							 
						}
						
					}
			}
			
			//Step 5
			for (String strProductFamilyName : setProductFamily) {
				ObjLead.fn_setKeywordTxt(driver, webWait).sendKeys(strProductFamilyName);
				ObjLead.fn_clickPrdSearchBtn(driver, webWait).click();
				Thread.sleep(5000);
				//Select product family
				ObjLead.fn_clickPrdSearched(driver, webWait, strProductFamilyName).isEnabled();
				ObjLead.fn_clickPrdSearched(driver, webWait, strProductFamilyName).click();
				Thread.sleep(3500);
				ObjLead.fn_clickSelectBtn(driver, webWait).click();
				Thread.sleep(3500);
				fn_SetProductFamilyDetails(driver, webWait, strProductFamilyName, strCreateOfferingDataSheetName, strCreateOfferingWBName);
			}
			
			EnvironmentDetails.mapGlobalVariable.put("mapProductAndFamily", mapProductAndFamily);
			Log.debug("DEBUG: Saved Product And Family relationship map in global variable map with Key mapProductAndFamily.");
			
			ObjLead.fn_clickCancelBtn(driver, webWait).click();
			
			objOpp.fn_getEditCRLink(driver, webWait).isEnabled();
			List<WebElement> lstEleEditCRLinks = objOpp.fn_getEditCRLinks(driver, webWait);
			//Step 5: Enter Customer reference for non US country
			if (!strCountry.equalsIgnoreCase("Ireland")) {
				
				for (int i = 0; i < lstEleEditCRLinks.size(); i++) {
					objOpp.fn_getEditCRLink(driver, webWait).isEnabled();
					lstEleEditCRLinks = objOpp.fn_getEditCRLinks(driver, webWait);
					lstEleEditCRLinks.get(i).click();
					ObjLead.fn_clickSaveBtn(driver, webWait).isEnabled();
					objOppForm.fn_setCustRef(driver, webWait).clear();
					objOppForm.fn_setCustRef(driver, webWait).sendKeys(strCustReference);
					ObjLead.fn_clickSaveBtn(driver, webWait).click();
	      			
	      			
				}
			}
			objOpp.fn_getEditCRLink(driver, webWait).isEnabled();
		}
		catch (Exception ExceptionSelectProductFamily) {
			Log.error("ERROR: Unable to Create proposal from upgrade and downgrade with exception reported as: "+ExceptionSelectProductFamily.getMessage());
	        StringWriter stack = new StringWriter();
	        ExceptionSelectProductFamily.printStackTrace(new PrintWriter(stack));
	        Log.debug("DEBUG: The exception in fn_CreateUpgDwg method is: "+stack);
	       
		}
		
		Log.debug("DEBUG: Execution of function 'fn_SelectProductFamily' Completed.");
		return true;

	}

	/*
	Author: Suhas Thakre
	Function fn_SetProductFamilyDetails : Set Product details to create offering.
	Inputs: strProductFamily - Product family name for witch data needs to be enter.
			strProductsDataSheet - ProductsDataSheet containing data to create offering.
	Outputs: Boolean response to check success or failure of function .
	 */
	public static void fn_SetProductFamilyDetails(WebDriver driver,WebDriverWait webWait, String strProductFamily
							, String strCreateOfferingDataSheetName, String strCreateOfferingWBName ) throws Throwable
	{
		//TODO:: Set Product details  
		/************************************************************************************************/
			//Step 1: Read Offering Data from CreateOfferingData sheet. 
			//Step 2: Search for Product Family and fetch data.
			//Step 3: Set Product Family Data.
			
		
		/************************************************************************************************/
		
		LeadCreation ObjLead = new LeadCreation();
		ExcelHandler objExcel = new ExcelHandler();
		//Utils objUtils = new Utils();
		//EnvironmentDetails objEnv = (EnvironmentDetails) EnvironmentDetails.mapGlobalVariable.get("objEnv");
		String excelResource = EnvironmentDetails.Path_ExternalFiles
								+ System.getProperty("file.separator") + strCreateOfferingWBName;
		File fileCreateOffering = new File(excelResource);
		List<Map<String, String>> lstProductMap = new ArrayList<Map<String,String>>();
		String strProductFamilyData ="";
		String strInvoiceTerm ="";
		String strSite ="";
		String strStartDate ="";
		String strAnnualFee ="";
		String strOneTimeFee ="";
		String strCustReference= EnvironmentDetails.mapGlobalVariable.get("strCustReference").toString();
		Boolean blnCreateOffering = false ;
		
	try 
	{
		Log.debug("DEBUG: Execution of function 'fn_SetProductFamilyDetails' Started.");
		//Step 1
		lstProductMap = objExcel.fn_ReadExcelAsMap(fileCreateOffering,strCreateOfferingDataSheetName); 
		
		//Step 2
		//Search Product Family in all row of Add_Products_Data sheet
				for (Map<String, String> mapProduct : lstProductMap) {
					
					if (mapProduct.get("Uniquekey").toString().trim().equalsIgnoreCase(EnvironmentDetails.mapGlobalVariable.get("struniquekey").toString())) {
						strProductFamilyData = mapProduct.get("Product_Family").toString().trim();
						if (strProductFamilyData.equalsIgnoreCase(strProductFamily.trim()))
						{	
							//Step 3
							strInvoiceTerm = mapProduct.get("Invoice_Term").toString().trim();
							strSite = mapProduct.get("Site").toString().trim();
							strStartDate = mapProduct.get("Start_Date").toString().trim();
							
							//change date format as per country 
							SimpleDateFormat USformat = new SimpleDateFormat("MM/dd/yyyy");
							SimpleDateFormat NonUSformat = new SimpleDateFormat("dd/MM/yyyy");
							SimpleDateFormat Inputformat = new SimpleDateFormat("dd/MMM/yyyy");
							Date date = Inputformat.parse(strStartDate);
							if (EnvironmentDetails.mapGlobalVariable.get("strCountry").toString().equalsIgnoreCase("United States")) {
								strStartDate = USformat.format(date);
							} else {
								strStartDate = NonUSformat.format(date);
							}
							
							
							
							
							//Save Start Date in global variable map 
							EnvironmentDetails.mapGlobalVariable.put("strStartDate", strStartDate);
							Log.debug("DEBUG: Start Date save in global variable map with key 'strStartDate'.");
							
							strAnnualFee = mapProduct.get("Annual_Fee").toString().trim();
							strOneTimeFee = mapProduct.get("One_Time_Fee").toString().trim();
							
							ObjLead.fn_enterInvTerm(driver, webWait).sendKeys(strInvoiceTerm);
							Thread.sleep(3500);
							ObjLead.fn_selectSite(driver, webWait).selectByValue(strSite);
							Thread.sleep(3500);
							ObjLead.fn_setStartDate(driver, webWait).sendKeys(strStartDate);
							
							try {
								ObjLead.fn_setAnnualFee(driver, new WebDriverWait(driver,3)).click();
								Thread.sleep(3500);
								ObjLead.fn_setAnnualFee(driver, webWait).clear();
								ObjLead.fn_setAnnualFee(driver, webWait).sendKeys(strAnnualFee);
							} catch (Exception e) {
								Log.debug("DEBUG: Annual free is not editable for Add products page.");
							}
							ObjLead.fn_setOneTimeFee(driver, webWait).click();
							Thread.sleep(3500);
							ObjLead.fn_setOneTimeFee(driver, webWait).clear();
							ObjLead.fn_setOneTimeFee(driver, webWait).sendKeys(strOneTimeFee);
					    	Thread.sleep(3500);
					    	try {
					    		ObjLead.fn_setCustomerRef(driver, new WebDriverWait(driver,2)).clear();
						    	ObjLead.fn_setCustomerRef(driver, webWait).sendKeys(strCustReference);
							} catch (Exception e) {
								Log.debug("DEBUG: Customer Referance is not present for Add products page.");
							}
					    	
					    	//objUtils.fn_HandleLookUpWindow(driver, webWait,"Customer Reference Lookup (New Window)",strCustReference);
						    Thread.sleep(3500);
						    ObjLead.fn_clickPrdctSaveMoreBtn(driver, webWait).click();
							Thread.sleep(8000);
							blnCreateOffering = true;
						}
						
					}
					else {
						Log.debug("DEBUG: Product Family '"+strProductFamily+"' Not Present in data sheet");
					}
				}
				
				
				//if product family is not available then no need to create offering 
				if (blnCreateOffering==false) {
					ObjLead.fn_clickOfferingCancelBtn(driver, webWait).click();
					Thread.sleep(3500);
					//Click Add product button 
					ObjLead.fn_clickAddProductBtn(driver, webWait).click();
					Thread.sleep(3500);
				}
	    }
		catch (Exception ExceptionSetProductFamily) {
			Log.error("ERROR: Unable to Create proposal from upgrade and downgrade with exception reported as: "+ExceptionSetProductFamily.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionSetProductFamily.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_CreateUpgDwg method is: "+stack);
           
		}
				Log.debug("DEBUG: Execution of function 'fn_SetProductFamilyDetails' Completed.");
	}

	/*
	Author: Suhas Thakre
	Function fn_CaptureOfferingData : Capture Offering name against product family.
	Inputs: strApttusQueryName - SOQL to fetch SiteCode with help of offering name.
			strProductsDataSheet - ProductsDataSheet containing data to create offering.
	Outputs: Boolean response to check success or failure of function .
	 */
	public Boolean fn_CaptureOfferingData(WebDriver driver,WebDriverWait webWait,String strProductsDataSheet, String strApttusQueryName, String strCreateOfferingWBName ) throws Throwable
	{
		//TODO:: Capture Offering Details  
		/************************************************************************************************/
			//Step 1: Prepare map with Offering key and site values. 
			//Step 2: Read Family name and SiteCode from data sheetL.
			//Step 3: Read offering name with help of SiteCode from mapOfferingAndLoc.
			//Step 4: Remove LocCode entry from mapOfferingAndLoc to avoid duplicate loc.
			//Step 5: Update each offering data
		/************************************************************************************************/
		
		ExcelHandler objExcel = new ExcelHandler();
		API_SOAP objAPI_SOAP = new API_SOAP();
		Proposal objProposal= new Proposal();
		EnvironmentDetails objEnv = (EnvironmentDetails) EnvironmentDetails.mapGlobalVariable.get("objEnv");
		List<Map<String, String>> lstProductMap = new ArrayList<Map<String,String>>();
		Map<String,String> mapFamilyAndOffering = new HashMap<String,String>();
		Map<String,String> mapOfferingAndLoc = new HashMap<String,String>();
		Map<String, List<String>> mapUPDOffering = new HashMap<String, List<String>>();
		Map<String, Object> mapTestCases = new HashMap<String, Object>();
		mapTestCases.put("CallType", "query");
		
		String excelResource = EnvironmentDetails.Path_ExternalFiles + System.getProperty("file.separator") + strCreateOfferingWBName;
		File fileCreateOffering = new File(excelResource);
		String strSite ="";
		String strLocCode = "";
		String strProductFamily ="";
		String strBillingDate = "";
		String strIncreasePL = "";
		String strBillingFrequency = "";
		
		Boolean blnCaptureOfferingData = false;
		
		try
		{
		Log.debug("DEBUG: Execution of function 'fn_CaptureOfferingData' Started.");
		
		//Read query from workbook ApttusQuery.xlsx and sheet ApttusQuerySheet
		String strApttusQuery = objExcel.fn_ReadApttusQuery("ApttusQuery", "ApttusQuerySheet", strApttusQueryName);
		
		//Step 1: Prepare map with Offering key and siteCode values.
		//Step a: Read all offering. 
		//Step b: Get sideCode from offering name with help of SOQL.
		//Wait for Offering
		webWait.until(ExpectedConditions.elementToBeClickable(objProposal.fn_getOfferingNames(driver, webWait).get(0))).isDisplayed();
		//Step a: Read all offering.
		List<WebElement> lstOfferings = objProposal.fn_getOfferingNames(driver, webWait);
		for (WebElement eleOfferings : lstOfferings) {
			String strOfferingName = eleOfferings.getAttribute("innerHTML").trim();
			//Step b: Get sideCode from offering name with help of SOQL.
			String strUpdApttusQuery = strApttusQuery;
			strUpdApttusQuery = strUpdApttusQuery.replace("OfferingNameInput", strOfferingName);
			mapTestCases.put("CallingData", strUpdApttusQuery);
			List<List<Map<String, String>>> lstmapSOAPRes = objAPI_SOAP.fn_SOAP_GET(objEnv, mapTestCases, null);
			//Add result to list 
				for (List<Map<String, String>> listmap : lstmapSOAPRes) {
					 
					 for (Map<String, String> map : listmap) {
						 
						 strLocCode= map.get("Data_Center_Code__c").toString();
					}
					
				}
				//Create map with offering as key and location as value
				mapOfferingAndLoc.put(strOfferingName, strLocCode);
		}
		
		
		//Step 2: Read Family name and SiteCode from data sheetL.
		lstProductMap = objExcel.fn_ReadExcelAsMap(fileCreateOffering,strProductsDataSheet); 
		String strOfferingName = "";
		for (Map<String, String> mapProduct : lstProductMap) {
			//Select data depends on unique key 
			if (mapProduct.get("Uniquekey").toString().trim().equalsIgnoreCase(EnvironmentDetails.mapGlobalVariable.get("struniquekey").toString())) {
					List<String> lstUPDOffering = new ArrayList<String>();
					//Read Site Name
					strSite = mapProduct.get("Site").toString().trim();
					strProductFamily = mapProduct.get("Product_Family").toString().trim();
					strBillingDate = mapProduct.get("Billing_Date").toString().trim();
					strIncreasePL = mapProduct.get("Annual_Price_Increase_PL").toString().trim();
					strBillingFrequency = mapProduct.get("Billing Frequency").toString().trim();
					lstUPDOffering.add(strBillingDate);
					lstUPDOffering.add(strIncreasePL);
					lstUPDOffering.add(strBillingFrequency);
						//Step 3: Read offering name with help of SiteCode from mapOfferingAndLoc.
						for ( Entry<String, String> entry : mapOfferingAndLoc.entrySet()) {
						    String OfferingNamekey = entry.getKey();
						    String strLocCodevalue = entry.getValue();
						    
						    if (strLocCodevalue.equalsIgnoreCase(strSite)) {
								
						    	strOfferingName = OfferingNamekey;
						    	//System.out.println("========= "+strProductFamily+" ========= "+strOfferingName);
						    	mapOfferingAndLoc.remove(OfferingNamekey);
						    	mapFamilyAndOffering.put(strProductFamily, strOfferingName);
						    	break;
							}
						    
						   
						}
					
						
					//mapFamilyAndOffering.put(strProductFamily, strOfferingName);
					mapUPDOffering.put(strOfferingName, lstUPDOffering);
					EnvironmentDetails.mapGlobalVariable.put("mapOfferingNames", mapFamilyAndOffering);
			}
		}
		
		//Save Product Family And Offering relationship
		EnvironmentDetails.mapGlobalVariable.put("mapFamilyAndOffering", mapFamilyAndOffering);
		Log.debug("DEBUG: Saved Family And Offering relationship map in global variable map with Key mapFamilyAndOffering.");
		
		//Step 5: Update each offering data
		//TODO:: Update Offering Data
		fn_UpdOfferingData(driver, webWait, mapFamilyAndOffering, mapUPDOffering);
		
		}
		catch (Exception ExceptionCaptureOffering) {
			Log.error("ERROR: Unable to Capture Offering Data with exception reported as: "+ExceptionCaptureOffering.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionCaptureOffering.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_CaptureOfferingData method is: "+stack);
           
		}
		Log.debug("DEBUG: Execution of function 'fn_CaptureOfferingData' Completed.");
		blnCaptureOfferingData = true;
		return blnCaptureOfferingData;
		
		
	}
}