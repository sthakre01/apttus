
/*
 Author     		:	Madhavi
 Class Name			: 	ApprovalValidation 
 Purpose     		: 	Purpose of this file is :
						1. To Approval trigger successfully. 

 Date       		:	26/12/2016 
 Modified Date		:		
 Modified By		: 	
 Version Information:	Version 1.0
 						
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/
package appModules;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import utility.Log;
import utility.Utils;
import pageObjects.SalesForce.Approval;

public class ApprovalValidation {
	
	public Boolean fn_ApprovalValidation(WebDriver driver,WebDriverWait webWait) throws Exception{
		
		
		//GenerateProposalDoc objProposalPO = new GenerateProposalDoc();
		Approval objApproval = new Approval();
		Utils objUtils = new Utils();
		
		String strMsgtext="";
		Boolean blnApprovalValidation = false;
		
		objApproval.fn_clickSubmitWthAttchmentsBtn(driver, webWait).isEnabled();
		//TODO:: Capture Screenshot for Approval details 
		objUtils.fn_takeScreenshotFullPage(driver);
		
		objApproval.fn_clickSubmitWthAttchmentsBtn(driver, webWait).click();
		Thread.sleep(10000);
		
		//Handle Occurrence without attachments
		try{
			objApproval.fn_clickSubmit(driver, webWait).isEnabled();
			objApproval.fn_clickSelect(driver, webWait).click();
			objApproval.fn_clickSubmit(driver, webWait).isEnabled();
			objApproval.fn_clickSubmit(driver, webWait).click();
			objApproval.fn_getMsgText(driver, webWait).isDisplayed();
			
			strMsgtext = objApproval.fn_getMsgText(driver, webWait).getText();
			if (strMsgtext.contains("Your request(s) have been submitted for approval. You will be notified when approvals are complete."))
				Log.info("Proposal has been successfully submitted for approval with notification.");
			else
				Log.debug("DEBUG: Proposal has not been successfully submitted for approval with notification.");
			}
		catch(Exception exceptionApprovalSbmt){
			Log.error("ERROR: Approval has not occoured with selected of attachments.");}
		
		//Handle yes button
		try{
			 objApproval.fn_clickyesBtn(driver, new WebDriverWait(driver,5)).click();
		    }
		catch(Exception exceptionYesBtn){
			Log.debug("DEBUG: Yes button is not available.");}
		
		objApproval.fn_clickReturnBtn(driver, webWait).isEnabled();
		objApproval.fn_clickReturnBtn(driver, webWait).click();
		Thread.sleep(3500);
		
		blnApprovalValidation = true;
		return blnApprovalValidation;
	
	}

}

