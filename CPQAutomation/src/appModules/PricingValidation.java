/*
 Author     		:	Suhas Thakre
 Class Name			: 	PricingValidation 
 Purpose     		: 	Purpose of this file is :
						1. To Validate pricing of line items on pricing screen 

 Date       		:	04/06/2017
 Modified Date		:		
 Modified By		: 	
 Version Information:	Version 1.0
 						
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/

package appModules;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.Approval;
import pageObjects.SalesForce.Configuration;
import pageObjects.SalesForce.Pricing;
import pageObjects.SalesForce.Pricing_NA;
import utility.API_SOAP;
import utility.EnvironmentDetails;
import utility.ExcelHandler;
import utility.Log;
import utility.Utils;
import utility.VerificationMethods;
import utility.WriteExcel;


public class PricingValidation extends VerificationMethods{
	
	
	public boolean fn_PricingValidation(WebDriver driver,WebDriverWait webWait,String strPricingSheet_Name) throws Exception
	{
	
		//EnvironmentDetailsSAS   objEnvDetails = new EnvironmentDetailsSAS();
		Configuration objConfig = new Configuration();
		//Pricing       objPricing = new Pricing(); 
		boolean blnPricingValidation=false;
		
		try
		{
			
		//WebElement webElement = null;
		 	 
	 	//File fileConfigValidations = new File(objEnvDetails.dataSheetPath);	
	 	Approval objApproval = new Approval();
	 	
		//TODO : Read Test Excel
		//ArrayList<String> arrlistFileContents = ExcelHandler.readExcelFileAsArray(fileConfigValidations,strPricingSheet_Name);	
	
		//initializing internal Loop variables
		//int introwtraverser=0;
		//int row=1;
		//String strServiceName=null;
		//String strChargeType=null;
		//String strPrice=null;
		//String strXpath1=null;
		//String strFinalXpath=null;
		//strXpath1 = "//table[@class='aptLineItemOptionsTable']/tbody/tr[";
		
	/*	//TODO: Now Traverse the ArrayList for Excel File Read to find the item from data table.
		for(int intCnt = 0 ; intCnt < arrlistFileContents.size();intCnt+=3)
			{		
				
					introwtraverser = intCnt;	
						
				    //Fetching the Rates table to read the pricing components
					WebElement Webtable = objPricing.fn_getRatestbl(driver, webWait);
					
					//Fetch the rows of the table
					List<WebElement> TotalRowCount1 = Webtable.findElements(By.tagName("tr"));
			
			         int rowsize=TotalRowCount1.size()-1;
						
					 strFinalXpath = strXpath1+row+"]";
					 
					//Fetching the service Name from excel
					 webElement  = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(strFinalXpath+"/td[2]")));
					 strServiceName = webElement.getText();
					 strServiceName = strServiceName.substring(0,strServiceName.length()-5).trim().toString();
					 
					//Fetching the Charge Type from application
					 webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(strFinalXpath+"/td[5]")));
					 strChargeType = webElement.getText().trim().toString();
					
					//checking the condition of matching the Service Name and its strChargeType
					if (strServiceName.contains(arrlistFileContents.get(introwtraverser))  && (strChargeType.contains(arrlistFileContents.get(introwtraverser+1))))
					{	
						
						Log.info("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
						Log.info("Start of Validations for the Service ::  "+strServiceName+"strChargeType :: "+strChargeType); 
						
							//Fetching the strprice Value from application
							 webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(strFinalXpath+"/td[6]"))); 
							 strPrice = webElement.getText();	
							 String[] arrprice =  strPrice.split(Pattern.quote("("));
							 
							   //TODO :: Verify the Service Name displayed on the Pricing Page
								if (arrlistFileContents.get(introwtraverser).trim().contains(strServiceName))
								{
									Log.info("Match Found for Service Name : "+strServiceName);
									Log.info("Application Value for Service Name : "+strServiceName);
									Log.info("Excel Data Value for Service Name : "+arrlistFileContents.get(introwtraverser));
									Log.info("-----------------------------------------------------------------------------------");
								}
								else
								{
									Log.info("Match Not Found for Service Name : "+strServiceName);
									Log.info("Application Value for Service Name : "+strServiceName);
									Log.info("Excel Data Value for Service Name : "+arrlistFileContents.get(introwtraverser));
									Log.info("-----------------------------------------------------------------------------------");
								}
								
							    //TODO :: Verify the Service Name displayed on the strChargeType
								if (arrlistFileContents.get(introwtraverser+1).trim().contains(strChargeType))
								{
									Log.info("Match Found for ChargeType : "+strChargeType);
									Log.info("Application Value for ChargeType : "+strChargeType);
									Log.info("Excel Data Value for ChargeType : "+arrlistFileContents.get(introwtraverser+1));
									Log.info("-----------------------------------------------------------------------------------");
								}
								else
								{
									Log.info("Match Not Found for ChargeType : "+strChargeType);
									Log.info("Application Value for ChargeType : "+strChargeType);
									Log.info("Excel Data Value for ChargeType : "+arrlistFileContents.get(introwtraverser+1));
									Log.info("-----------------------------------------------------------------------------------");
								}
								
							    //TODO :: Verify the Service Name displayed on the strprice
								if (arrlistFileContents.get(introwtraverser+2).trim().contains(arrprice[0].toString().trim()))
								{
									Log.info("Match Found for price : "+arrprice[0]);
									Log.info("Application Value for price : "+arrprice[0]);
									Log.info("Excel Data Value for price : "+arrlistFileContents.get(introwtraverser+2));
									Log.info("-----------------------------------------------------------------------------------");
								}
								else
								{
									Log.info("Match Not Found for price : "+arrprice[0]);
									Log.info("Application Value for price : "+arrprice[0]);
									Log.info("Excel Data Value for price : "+arrlistFileContents.get(introwtraverser+2));
									Log.info("-----------------------------------------------------------------------------------");
								}
								
								Log.info("End of Validations for the Service ::  "+strServiceName+"strChargeType :: "+strChargeType); 
								Log.info("++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");

					if (row<rowsize)
						row+=1;	
					else if (row==rowsize)
						row=1;	
						
					   }	
				
			     }	*/
		            //To click on UpdateLineItems&Exit Button after pricing Validation
		            //objConfig.fn_clickSave(driver, webWait).click();
	 	
	 		//sleep to apply discount
	 		Thread.sleep(40000);
		
			try {
				objConfig.fn_createPropAndSubmitBtn(driver, new WebDriverWait(driver,30)).isDisplayed();
	            objConfig.fn_createPropAndSubmitBtn(driver, webWait).click(); 	} 
			catch (Exception e) {
				objConfig.fn_createPropAndSubmitNABtn(driver, webWait).click();   }	
			objApproval.fn_clickSubmitWthAttchmentsBtn(driver, webWait).isEnabled();
		
		            blnPricingValidation = true;
		    		
	       }
		
	    catch (Exception e)
	    {
	        Log.error("ERROR: Unable to complete the Pricing Validations");
	        Log.error(e.getMessage());
	
	    }
		
		return blnPricingValidation;
		
	}
	
	/*
	Author: Suhas Thakre
	Function fn_PricingValidationIRE: Validate line items price for each product.
	Inputs: 
	Outputs: Boolean response to check success or failure of function .
	 */
	public boolean fn_PricingValidationIRE(WebDriver driver,WebDriverWait webWait) throws Exception
	{
		//TODO:: Validate product pricing  
		/************************************************************************************************/
			//Step 1: Get Currency code of record.
			//Step 2: Validate pricing for Ireland and Non-Ireland.
			
		/************************************************************************************************/
		
		Configuration objConfig = new Configuration();
		ExcelHandler objExcel = new ExcelHandler();
		API_SOAP objAPI_SOAP = new API_SOAP();
		EnvironmentDetails objEnv = (EnvironmentDetails) EnvironmentDetails.mapGlobalVariable.get("objEnv");
		String strNewproposalName = EnvironmentDetails.mapGlobalVariable.get("strNewproposalName").toString();
		Map<String, Object> mapTestCases = new HashMap<String, Object>();
		String strCurrencyCodeSOQL = "";
		String strCurrencyCode = "";
		Boolean blnPricingValidation = false;
		Boolean blnNonAngJs = false;
		
		try
		{
			//Verify Angular or Non-Angular js application.
			try {
				objConfig.fn_clickOfferingMgmntBtn(driver, new WebDriverWait(driver,5)).isEnabled(); 	
				blnNonAngJs = true; } 
			catch (Exception e) {
				Log.debug("DEBUG: Validating pricing for Angular js application.");			}
			
			//Step 1: Get Currency code of record.
			strCurrencyCodeSOQL = objExcel.fn_ReadApttusQuery("ApttusQuery", "ApttusQuerySheet", "CurrencyCode");
			strCurrencyCodeSOQL = strCurrencyCodeSOQL.replace("ProposalNameInput", strNewproposalName);
			mapTestCases.put("CallType", "query");
			mapTestCases.put("CallingData", strCurrencyCodeSOQL);
			List<List<Map<String, String>>> lstmapMSAID = objAPI_SOAP.fn_SOAP_GET(objEnv, mapTestCases, null);
			for (List<Map<String, String>> listmap : lstmapMSAID) {
				 for (Map<String, String> map : listmap) {
					 strCurrencyCode = map.get("opportunityCurrency__c").toString();
				}
			}
			
			
			if (blnNonAngJs ==false) {
				fn_PricingValidationNA(driver, webWait, strCurrencyCode);
			}else {
				fn_PricingValidationIRE(driver, webWait, strCurrencyCode);
			}
			
			blnPricingValidation = true;
		}
		catch (Exception ExceptionPricingValidation) {
			Log.error("ERROR: Unable to Validate Pricing with exception reported as: "+ExceptionPricingValidation.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionPricingValidation.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_PricingValidationIRE method is: "+stack);
           
		}
		
		return blnPricingValidation;
		
	}
	
	
	
	/*
	Author: Suhas Thakre
	Function fn_PricingValidationIRE: Validate line items price for each product.
	Inputs: strCurrencyCode - Currency code for country.
	Outputs: Boolean response to check success or failure of function .
	 */
	public void fn_PricingValidationNA(WebDriver driver,WebDriverWait webWait, String strCurrencyCode) throws Exception
	{
		//TODO:: Validate product pricing  
		/************************************************************************************************/
			//Step 2: Get list of product available.
			//Step 3: Get line items for each product.
			//Step 4: Get line items price from database.
			//Step 5: Compare result with database.
			

		/************************************************************************************************/
		//Pricing objPricing = new Pricing();
		Pricing_NA objPricingNA = new Pricing_NA();
		Utils objUtils=new Utils();
		Configuration objConfig = new Configuration();
		API_SOAP objAPI_SOAP = new API_SOAP();
		EnvironmentDetails objEnv = (EnvironmentDetails) EnvironmentDetails.mapGlobalVariable.get("objEnv");
		String strLineItemPriceSOQL = "";
		String strLineItemPriceUPDSOQL = "";
		String strLineItemPriceDb = "";
		ExcelHandler objExcel = new ExcelHandler();
		Map<String, Object> mapTestCases = new HashMap<String, Object>();
		
		try
		{
			objConfig.fn_clickOfferingMgmntNABtn(driver, webWait).isEnabled();
			
			//Get line items price SOQL
			strLineItemPriceSOQL = objExcel.fn_ReadApttusQuery("ApttusQuery", "ApttusQuerySheet", "Line Item Price");
			mapTestCases.put("CallType", "query");
			
			//Step 2: Get list of product available.
			List<WebElement> lsteleProdOption = objPricingNA.fn_getProdOptions(driver, webWait);
			//Run loop for All product
			for (WebElement eleProdOption : lsteleProdOption) {
				Utils.fn_scrollToElement(driver, eleProdOption);
				eleProdOption.click();
				//Thread.sleep(2000);
				objUtils.fn_waitForLoader(driver);
				objPricingNA.fn_getLineItems(driver, webWait).get(0).isEnabled();
				
				//Step 3: Get line items for each product.
				List<WebElement> lsteleLineItems = objPricingNA.fn_getLineItems(driver, webWait);
				//Run Loop for all line items of each product 
				for (int i = 1; i <= lsteleLineItems.size(); i++) {
					String strLineItemName = objPricingNA.fn_getLineItem(driver, webWait, i).getAttribute("title").trim();
					
					List<WebElement> lsteleChargeType = objPricingNA.fn_getChargeType(driver, webWait, i);
					//Run Loop for all charge type of each line items
					for (int j = 0; j < lsteleChargeType.size(); j++) {
						
						String strLineItemChargeType = lsteleChargeType.get(j).getText().trim();
						String strLineItemBasePrice = objPricingNA.fn_getBasePrice(driver, webWait, i+1, j+1).getText().trim();
						String strLineItemQuantity = "";
						String strLineItemAdjType = "";
						//Get Quantity from text field or input box.
						try {
							strLineItemQuantity = objPricingNA.fn_getQuantity(driver, new WebDriverWait(driver,2), i+1, j+1).getText().trim();	}
						catch (Exception e) {
							strLineItemQuantity = objPricingNA.fn_getQuantityInputbox(driver, webWait, i+1, j+1).getAttribute("value").trim();	}
						String strLineItemTerm = objPricingNA.fn_getTerm(driver, webWait,  i+1, j+1).getText().trim();
						String strLineItemMRR = objPricingNA.fn_getMRR(driver, webWait, i+1, j+1).getText().trim();
						String strLineItemGCR = objPricingNA.fn_getGCR(driver, webWait, i+1, j+1).getText().trim();
						String strLineItemNetPrice = objPricingNA.fn_getNetPrice(driver, webWait, i+1, j+1).getText().trim();
						String strLineItemAdjAmt = objPricingNA.fn_getAdjAmt(driver, webWait, i+1, j+1).getAttribute("value").trim();
						//Get Adjustment type from text field or select box.
						try {
							strLineItemAdjType = objPricingNA.fn_getAdjTypeSel(driver, new WebDriverWait(driver,2), i+1, j+1).getText().trim();	}
						catch (Exception e) {
							strLineItemAdjType = objPricingNA.fn_getAdjType(driver, webWait, i+1, j+1).getText().trim();	}
						
						//Step 4: Get line items price from database.
						strLineItemPriceUPDSOQL = strLineItemPriceSOQL;
						strLineItemPriceUPDSOQL = strLineItemPriceUPDSOQL.replace("LineItemNameInput", strLineItemName);
						strLineItemPriceUPDSOQL = strLineItemPriceUPDSOQL.replace("ChargeTypeInput", strLineItemChargeType);
						strLineItemPriceUPDSOQL = strLineItemPriceUPDSOQL.replace("CurrencyCodeInput", strCurrencyCode);
						mapTestCases.put("CallingData", strLineItemPriceUPDSOQL);
						List<List<Map<String, String>>> lstmapLineItemPrice = objAPI_SOAP.fn_SOAP_GET(objEnv, mapTestCases, null);
						for (List<Map<String, String>> listmap : lstmapLineItemPrice) {
							 for (Map<String, String> map : listmap) {
								 strLineItemPriceDb = map.get("Apttus_Config2__ListPrice__c").toString();
								 //strLineItemPriceDb = strCurrencyCode + " "+strLineItemPriceDb;
							}
						}
						
						if (strLineItemName.equalsIgnoreCase("Cloud Recovery - AWS (Annual Tests per Contract Year)") && strLineItemQuantity.equalsIgnoreCase("2.00")) {
							strLineItemPriceDb = "0.00";
						}
						else if (strLineItemName.equalsIgnoreCase("Managed Cloud - AWS: Infrastructure Commitment") || strLineItemName.equalsIgnoreCase("Managed Cloud - AWS: Management") ) {
							strLineItemPriceDb = fn_CalBasePriceProductAttribute(driver, webWait, strLineItemName);
						}
						//Step 5: Compare result with database.
						fn_CompareValue(strLineItemBasePrice.replace(",", ""), strCurrencyCode + " "+strLineItemPriceDb);	
						String strMRRCal = fn_CalculateMRR(strLineItemPriceDb,strLineItemAdjAmt,strLineItemAdjType,strLineItemChargeType);
						fn_CompareValue(strLineItemMRR.replace(",", ""), strCurrencyCode + " "+strMRRCal);
						String strGCRCal = fn_CalculateGCR(strLineItemPriceDb, strLineItemAdjAmt, strLineItemAdjType, strLineItemTerm, strLineItemQuantity);
						fn_CompareValue(strLineItemGCR.replace(",", ""), strCurrencyCode + " "+strGCRCal);
						fn_CompareValue(strLineItemNetPrice.replace(",", ""), strCurrencyCode + " "+strGCRCal);
						
						//System.out.println("=============================strLineItemName===================  "+strLineItemName+"  "+lsteleChargeType.get(j).getText()+"  "+strLineItemBasePrice);
						//break;
					}
					//break;
				}
				
				
				//eleProdOption.click();
				objUtils.fn_waitForLoader(driver);
				
				//break;
			}
			
			
		}
		catch (Exception ExceptionPricingValidation) {
			Log.error("ERROR: Unable to Validate Pricing with exception reported as: "+ExceptionPricingValidation.getMessage());
	        StringWriter stack = new StringWriter();
	        ExceptionPricingValidation.printStackTrace(new PrintWriter(stack));
	        Log.debug("DEBUG: The exception in fn_PricingValidationNA method is: "+stack);
	       
		}
	}
	
	
	
	/*
	Author: Suhas Thakre
	Function fn_CalBasePriceProductAttribute: Calculate base price depend on Product attributes .
	Inputs: strLineItemName - Line Items name.
	Outputs: Boolean response to check success or failure of function .
	 */
	public String fn_CalBasePriceProductAttribute(WebDriver driver,WebDriverWait webWait, String strLineItemName) throws Exception
	{
		//TODO:: Calculate base price depend on Product attributes.  
		/************************************************************************************************/
			//Step 1: Get Product attributes from database.
			//Step 2: Calculate base price depend on Product attributes.
			
		/************************************************************************************************/
		Map<String, Object> mapTestCases = new HashMap<String, Object>();
		ExcelHandler objExcel = new ExcelHandler();
		WriteExcel objWriteExcel = new WriteExcel();
		Map<String, Object> mapOutputData = new HashMap<String, Object>();
		List<Map<String, Object>> lisOutputData = new ArrayList<>();
		List<List<Map<String, Object>>> lstmapOutputData = new ArrayList<>();
		EnvironmentDetails objEnv = (EnvironmentDetails) EnvironmentDetails.mapGlobalVariable.get("objEnv");
		String strNewproposalName = EnvironmentDetails.mapGlobalVariable.get("strNewproposalName").toString();
		API_SOAP objAPI_SOAP = new API_SOAP();
		String strLineItemPriceSOQL = "";
		String strLineItemPriceUPDSOQL = "";
		String strUserOverride = "";
		String strMonthlyCommitment = "";
		String strLineItemPriceDb = "";
		Double dblMonthlyCommitment=null;
		Double dblUserOverride=null;
		Double dblLineItemPriceDb=null;
		try {
			
			//Step 1: Get Product attributes from database.
			strLineItemPriceSOQL = objExcel.fn_ReadApttusQuery("ApttusQuery", "ApttusQuerySheet", "Product Attribute Value");
			mapTestCases.put("CallType", "query");
			strLineItemPriceUPDSOQL = strLineItemPriceSOQL;
			strLineItemPriceUPDSOQL = strLineItemPriceUPDSOQL.replace("ProposalNameInput", strNewproposalName);
			strLineItemPriceUPDSOQL = strLineItemPriceUPDSOQL.replace("LineItemNameInput", strLineItemName);
			mapTestCases.put("CallingData", strLineItemPriceUPDSOQL);
			List<List<Map<String, String>>> lstmapLineItemPrice = objAPI_SOAP.fn_SOAP_GET(objEnv, mapTestCases, null);
			for (List<Map<String, String>> listmap : lstmapLineItemPrice) {
				 for (Map<String, String> map : listmap) {
					 strMonthlyCommitment = map.get("MgdAWS_Monthly_Commitment_Value__c").toString();
					 strUserOverride = map.get("MgdAWS_Management_User_Entry__c").toString();
					 
				}
			}
			
								//TODO:: Write account name in output file 
								mapOutputData.put("Monthly Commitment", strMonthlyCommitment);
								mapOutputData.put("User Override", strUserOverride);
								lisOutputData.add(mapOutputData);
								lstmapOutputData.add(lisOutputData);
								objWriteExcel.fn_WriteExcel(lstmapOutputData, EnvironmentDetails.strFQNFileNameWrite, "New_Offering_Data_Details");

			if (strLineItemName.equalsIgnoreCase("Managed Cloud - AWS: Infrastructure Commitment") ) {
				strLineItemPriceDb = strMonthlyCommitment;
			}
			//Step 2: Calculate base price depend on Product attributes.
			else if (strLineItemName.equalsIgnoreCase("Managed Cloud - AWS: Management") ) {
				dblMonthlyCommitment = Double.parseDouble(strMonthlyCommitment);
				dblUserOverride = Double.parseDouble(strUserOverride);
				dblLineItemPriceDb = dblMonthlyCommitment*(dblUserOverride/100);
				//Format double to round of 2 decimal place.
				strLineItemPriceDb = String.format("%.2f", dblLineItemPriceDb);
			}
			
			
		}
		catch (Exception ExceptionCalBasePrice) {
					Log.error("ERROR: Unable to Calculate base price with exception reported as: "+ExceptionCalBasePrice.getMessage());
		            StringWriter stack = new StringWriter();
		            ExceptionCalBasePrice.printStackTrace(new PrintWriter(stack));
		            Log.debug("DEBUG: The exception in fn_CalBasePriceProductAttribute method is: "+stack);
		}
		return strLineItemPriceDb;
		
	}
	
	/*
	Author: Suhas Thakre
	Function fn_PricingValidationIRE: Validate line items price for each product.
	Inputs: strCurrencyCode - Currency code for country.
	Outputs: Boolean response to check success or failure of function .
	 */
	public void fn_PricingValidationIRE(WebDriver driver,WebDriverWait webWait, String strCurrencyCode) throws Exception
	{
		//TODO:: Validate product pricing  
		/************************************************************************************************/
			//Step 2: Get list of product available.
			//Step 3: Get line items for each product.
			//Step 4: Get line items price from database.
			//Step 4: Add Data to Map with key as product name and value as family
			//Step 4: Search and select Product family.

		/************************************************************************************************/
		Pricing objPricing = new Pricing();
		Utils objUtils=new Utils();
		API_SOAP objAPI_SOAP = new API_SOAP();
		Configuration objConfig = new Configuration();
		EnvironmentDetails objEnv = (EnvironmentDetails) EnvironmentDetails.mapGlobalVariable.get("objEnv");
		String strLineItemPriceSOQL = "";
		String strLineItemPriceUPDSOQL = "";
		String strLineItemPriceDb = "";
		ExcelHandler objExcel = new ExcelHandler();
		Map<String, Object> mapTestCases = new HashMap<String, Object>();
		try
		{
			objConfig.fn_clickOfferingMgmntBtn(driver, new WebDriverWait(driver,5)).isEnabled();
			
			//Get line items price SOQL
			strLineItemPriceSOQL = objExcel.fn_ReadApttusQuery("ApttusQuery", "ApttusQuerySheet", "Line Item Price");
			mapTestCases.put("CallType", "query");
			
			//Step 2: Get list of product available.
			List<WebElement> lsteleProdOption = objPricing.fn_getProdOptions(driver, webWait);
			for (WebElement eleProdOption : lsteleProdOption) {
				Utils.fn_scrollToElement(driver, eleProdOption);
				eleProdOption.click();
				objUtils.fn_waitForloadingBar(driver);
				objPricing.fn_getLineItemOptionsTable(driver, webWait).isDisplayed();
				
				//Step 3: Get line items for each product.
				List<WebElement> lsteleLineItems = objPricing.fn_getLineItems(driver, webWait);
				
				for (int i = 1; i <= lsteleLineItems.size(); i++) {
					String strLineItemName = objPricing.fn_getLineItem(driver, webWait, i).getText().trim();
					String strLineItemChargeType = objPricing.fn_geChargeType(driver, webWait, i).getText().trim();
					String strLineItemBasePrice = objPricing.fn_getBasePrice(driver, webWait, i).getText().trim();
					String strLineItemQuantity = objPricing.fn_getQuantity(driver, webWait, i).getText().trim();
					String strLineItemTerm = objPricing.fn_getTerm(driver, webWait, i).getText().trim();
					String strLineItemMRR = objPricing.fn_getMRR(driver, webWait, i).getText().trim();
					String strLineItemGCR = objPricing.fn_getGCR(driver, webWait, i).getText().trim();
					String strLineItemNetPrice = objPricing.fn_getNetPrice(driver, webWait, i).getText().trim();
					String strLineItemAdjAmt = objPricing.fn_getAdjAmt(driver, webWait, i).getAttribute("value");
					String strLineItemAdjType = objPricing.fn_getAdjType(driver, webWait, i).getFirstSelectedOption().getText();
					
						//Step 4: Get line items price from database.
						strLineItemPriceUPDSOQL = strLineItemPriceSOQL;
						strLineItemPriceUPDSOQL = strLineItemPriceUPDSOQL.replace("LineItemNameInput", strLineItemName);
						strLineItemPriceUPDSOQL = strLineItemPriceUPDSOQL.replace("ChargeTypeInput", strLineItemChargeType);
						strLineItemPriceUPDSOQL = strLineItemPriceUPDSOQL.replace("CurrencyCodeInput", strCurrencyCode);
						mapTestCases.put("CallingData", strLineItemPriceUPDSOQL);
						List<List<Map<String, String>>> lstmapLineItemPrice = objAPI_SOAP.fn_SOAP_GET(objEnv, mapTestCases, null);
						for (List<Map<String, String>> listmap : lstmapLineItemPrice) {
							 for (Map<String, String> map : listmap) {
								 strLineItemPriceDb = map.get("Apttus_Config2__ListPrice__c").toString();
								 //strLineItemPriceDb = strCurrencyCode + " "+strLineItemPriceDb;
							}
						}
					fn_CompareValue(strLineItemBasePrice.replace(",", ""), strCurrencyCode + " "+strLineItemPriceDb);	
					String strMRRCal = fn_CalculateMRR(strLineItemPriceDb,strLineItemAdjAmt,strLineItemAdjType,strLineItemChargeType);
					fn_CompareValue(strLineItemMRR.replace(",", ""), strCurrencyCode + " "+strMRRCal);
					String strGCRCal = fn_CalculateGCR(strLineItemPriceDb, strLineItemAdjAmt, strLineItemAdjType, strLineItemTerm, strLineItemQuantity);
					fn_CompareValue(strLineItemGCR.replace(",", ""), strCurrencyCode + " "+strGCRCal);
					fn_CompareValue(strLineItemNetPrice.replace(",", ""), strCurrencyCode + " "+strGCRCal);
				}
			}
		
		}
		catch (Exception ExceptionPricingValidation) {
			Log.error("ERROR: Unable to Validate Pricing with exception reported as: "+ExceptionPricingValidation.getMessage());
	        StringWriter stack = new StringWriter();
	        ExceptionPricingValidation.printStackTrace(new PrintWriter(stack));
	        Log.debug("DEBUG: The exception in fn_PricingValidationIRE method is: "+stack);
	       
		}
	}
	
	
	/*
	Author: Suhas Thakre
	Function fn_CalculateGCR: Calculate GCR for line items.
	Inputs: strLineItemBasePrice - Line Item BasePrice.
			strLineItemTerm - Term of line items.
			strLineItemQuantity - Quantity of line items.
			strLineItemAdjAmt - Amount need tobe Adjust.
			strLineItemAdjType - Adjustment type (ie % discount, discount amount, % markup, markup amount) as MRR calculation is different for each type. 
	Outputs: String GCR after Calculation .
	 */
	
	public String fn_CalculateGCR(String strLineItemBasePrice, String strLineItemAdjAmt,String strLineItemAdjType 
				, String strLineItemTerm , String strLineItemQuantity ) throws Exception
	{
		//TODO:: Calculate GCR for line items.  
		/************************************************************************************************/
			//Step 1: Calculate GSR depending on  Term, Quantity and BasePrice
			//Step 2: Calculate GSR for different adjustment type
		
		/************************************************************************************************/
		String strGCRCal = "";
		Double dblTerm=null;
		Double dblQuantity=null;
		Double dblBasePrice=null;
		Double dblGCRCal=null;
		
		
		try {
			dblBasePrice = Double.parseDouble(strLineItemBasePrice);
			dblQuantity = Double.parseDouble(strLineItemQuantity);
			dblTerm = Double.parseDouble(strLineItemTerm);
			
			//Step 1: Calculate GSR depending on  Term, Quantity and BasePrice
			dblGCRCal = dblBasePrice * dblQuantity * dblTerm ;
			strGCRCal = String.format("%.2f", dblGCRCal);
			
			
			//Step 2: Calculate GSR for different adjustment type
			if (!strLineItemAdjAmt.isEmpty()) {
				strGCRCal = fn_CalculateAdjustment(strLineItemAdjAmt, strLineItemAdjType, strGCRCal);
			}
		}
		catch (Exception ExceptionCalGCR) {
			Log.error("ERROR: Unable to Calculate MRR with exception reported as:: "+ExceptionCalGCR.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionCalGCR.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_CalculateGCR method is: "+stack);
		           
		}
		
		return strGCRCal;
	}
	
	
	/*
	Author: Suhas Thakre
	Function fn_CalculateAdjustment: Adjust Amount as per Adjustment type.
	Inputs: strLineItemAdjAmt - Amount need tobe Adjust.
			strLineItemAdjType - Adjustment type (ie % discount, discount amount, % markup, markup amount) as MRR calculation is different for each type. 
			strAdjValue - Value tobe Adjust
	Outputs: String Value After Adjustment .
	 */
	public String fn_CalculateAdjustment(String strLineItemAdjAmt,String strLineItemAdjType, String strAdjValue) throws Exception
	{
		
		
		//TODO:: Adjust Amount as per Adjustment type.  
		/************************************************************************************************/
			//Step 1: Change Adjustment amount as per adjustment type  
			
		/************************************************************************************************/
		//String strGCRCal = "";
		Double dblAdjAmt=null;
		Double dblAdjValue=null;
		
		
		try {
			//Step 1: Change Adjustment amount as per adjustment type
			
			dblAdjAmt = Double.parseDouble(strLineItemAdjAmt);
			dblAdjValue = Double.parseDouble(strAdjValue);
			//dblOriLineItemAdjAmt = dblMRRCal;
			
			switch (strLineItemAdjType.toLowerCase().trim()) {
			 //format Double to 2 decimal place 
		     case "% discount":
		    	 	//calculate expected MRR 
		    	 	dblAdjValue = dblAdjValue - ( dblAdjValue * (dblAdjAmt/100) );
					break;
	         case "discount amount":
	        	 	//calculate expected MRR 
	        	 dblAdjValue = dblAdjValue - dblAdjAmt ;
					break;
	         case "% markup":
	        	 	//calculate expected MRR 
	        	 	dblAdjValue = dblAdjValue + ( dblAdjValue * (dblAdjAmt/100) );
					break;
	         case "markup amount":
	        	 	//calculate expected MRR 
		        	 dblAdjValue = dblAdjValue + dblAdjAmt ;
		             break;
	         default:
	        	 Log.debug("DEBUG: Unable to find Adjustment Type");
	     }
		//Format double to round of 2 decimal place.
		strAdjValue = String.format("%.2f", dblAdjValue);
		
		}
		catch (Exception ExceptionCalAdj) {
			Log.error("ERROR: Unable to Calculate Adjustment with exception reported as:: "+ExceptionCalAdj.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionCalAdj.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_CalculateAdjustment method is: "+stack);
		           
		}
		
		return strAdjValue;
	}
	
	
	
	/*
	Author: Suhas Thakre
	Function fn_CalculateMRR: Calculate MRR for line items.
	Inputs: strLineItemBasePrice - Line Item BasePrice.
			strLineItemAdjAmt - Amount need tobe Adjust.
			strLineItemAdjType - Adjustment type (ie % discount, discount amount, % markup, markup amount) as MRR calculation is different for each type. 
			strLineItemChargeType - Charge Type (Monthly or onetime) to select MRR
	Outputs: String MRR after Calculation .
	 */
	public String fn_CalculateMRR(String strLineItemBasePrice, String strLineItemAdjAmt,String strLineItemAdjType, String strLineItemChargeType ) throws Exception
	{
		
		//TODO:: Calculate MRR for line items.  
		/************************************************************************************************/
			//Step 1: Set default MRR 
			//Step 2: Set MRR for OneTime fee charge type 
			//Step 3: Set MRR for different adjustment type
		
		/************************************************************************************************/
		String strMRRCal = "";
		//Double dblMRRCal=null;
		//Double dblLineItemAdjAmt=null;
		//Double dblOriLineItemAdjAmt=null;
		//DecimalFormat df = new DecimalFormat("#.00");
		try {
			
			//Step 1: Set default MRR ( MRR is same as BasePrice when no adjustment is required)
			strMRRCal = strLineItemBasePrice;
			
			//Step 2: Set MRR for OneTime fee charge type( MRR is zero when charge type is onetime)
			if (strLineItemChargeType.equalsIgnoreCase("One-time Fee")) {
				strMRRCal = "0.00";
			}
			else if (strLineItemChargeType.equalsIgnoreCase("Overage Fee")) {
				strMRRCal = "0.00";
			}
			
			//Step 3: Set MRR for different adjustment type
			if (!strLineItemAdjAmt.isEmpty()) {
				strMRRCal = fn_CalculateAdjustment(strLineItemAdjAmt, strLineItemAdjType, strMRRCal);
			}
		}
		catch (Exception ExceptionCalculateMRR) {
			Log.error("ERROR: Unable to Calculate MRR with exception reported as:: "+ExceptionCalculateMRR.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionCalculateMRR.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_CalculateMRR method is: "+stack);
		           
		}
		
		return strMRRCal;
	}
	
	
	/*
	Author: Suhas Thakre
	Function fn_CompareValue: Verify Actual value contains Expected Value.
	Inputs: strActual - Actual Value.
			strExpected - Expected Value
	Outputs: Boolean response to check success or failure of function .
	 */
	
	public void fn_CompareValue(String strActual, String strExpected) throws Exception
	{
		try {    
			strActual.contains(strExpected);  // "+strExpected+" not available in "+strActual
			//Verify Actual value contains Expected Value.
			if (strActual.contains(strExpected)) {
				 Log.debug("DEBUG: Actual "+strActual+" Expected "+strExpected);
			} 
			//Throw error when value mismatch.
			else {
				Log.error("ERROR: Actual "+strActual+" Expected "+strExpected);
				assertTrue(false, "ERROR: Comparison Fail with Actual "+strActual+" Expected "+strExpected);
			}
			
		}
		catch (Exception ExceptionCreateUpgDwg) {
					Log.error("ERROR: "+strExpected+" not available in "+strActual+ ": "+ExceptionCreateUpgDwg.getMessage());
		            StringWriter stack = new StringWriter();
		            ExceptionCreateUpgDwg.printStackTrace(new PrintWriter(stack));
		            Log.debug("DEBUG: The exception in fn_CompareValue method is: "+stack);
		           
				}
	}
}
