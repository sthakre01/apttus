/*
 Author     		:	Madhavi JN.
 Class Name			: 	CreateLeadandConvertToOppty 
 Purpose     		: 	Purpose of this file is :
						1. To perform  CreateLeadandConvertToOppty functions.

 Date       		:	04/10/2016 
 Modified Date		:	27/12/2016	
 Version Information:	Version 2.0
 
 Version Changes 2.0:	1. Modified the Billing Infomration and Option Values in adding Multiple Options in LOV.
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/

package appModules;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import pageObjects.SalesForce.Account;
import pageObjects.SalesForce.LeadCreationObjects;
import pageObjects.SalesForce.OpportunitesFormPageObject;
import utility.EnvironmentDetails;
import utility.ExcelHandler;
import utility.Log;
import utility.Utils;
import utility.WriteExcel;
//import utility.WriteTextFiles;

public class CreateLeadandConvertToMultipleAcctNA {
	
	//TODO:: Multiple Account Creation
	
	public String fn_CreateLead_ConvertToMultipleAcct(WebDriver driver,WebDriverWait webWait,Map<String, Object> mapMultipleAcct) throws Throwable
    {
		
		//TODO:: Create Account 
		/************************************************************************************************/
			//Step 1: Read Lead Creation data. 
			//Step 2: Read Account Creation data.
			//Step 3: Create Lead.
			//Step 4: Convert Lead to account.
			//Step 5: Enter the Address Information on Accounts page.
			//Step 6: Enter the Billing Information on Acct page which will be further used by Aria.
			//Step 7: Update contact address.
			//Step 8: Add New customer reference.
			//Step 9: Add new Contact and then link to address.
			//Step 10: Update Opportunity details.
		
		/************************************************************************************************/

		LeadCreationObjects objLeadCreation = new LeadCreationObjects();	
		Account objAcctpage = new Account();
		Utils objUtils=new Utils();
		CreateLeadandConvertToMultipleAcctNA objMultipleAccts = new CreateLeadandConvertToMultipleAcctNA();
		ExcelHandler objExcel = new ExcelHandler();
		List<Map<String, String>> lstmapLeadCreationData = new ArrayList<Map<String,String>>();
		List<Map<String, String>> lstmapfileAcctCreationData = new ArrayList<Map<String,String>>();
		Map<String,String> mapAcctAddrData = new HashMap<String,String>();
		
		String strBillingInformation = "";
		String[] arrBillingData = null;
		
		//String strAcctName = "";
		String strEntityName ="";
		//String strParentWindow="";
		String strEmailID="";
        String strShipTo="";
        String strBillTo="";
        String strPhone = "";
        String strCountry = "";
        String strLeadStatus = "";
        String strLeadStage = "";
        String strApprovalStatus = "";
        String strIndustry = "";
        String strSubject = "";
        String strConversionStatus = "";
        String strStreetAddress = "";
        String strCity = "";
        String strState = "";
        String strCountyCode = "";
        String strPostCode = "";
        String strContactCountry = "";
        String strDecisionRole = "";
        String strQuoteType = "";
        String strStateCode ="";
        strShipTo="";
		strBillTo="";
		
		String strCRname="";
		String strLastName ="";
		String strFirstName="";
		String strLastNameOri ="";
		String strFirstNameOri="";
		String strCompanyName = "";
		String strAccountType = "";
		String strOpptyName = "";
		String strAcctAddrID = "";
		
		String strMulAcctCreationSheet = mapMultipleAcct.get("strMulAcctCreationSheet").toString();
		 String strMulAcctCreationWB = mapMultipleAcct.get("strMulAcctCreationWB").toString();
		 String strLeadCreationWB = mapMultipleAcct.get("strLeadCreationWB").toString();
		 String strLeadToAccountSheet = mapMultipleAcct.get("strLeadToAccountSheet").toString();
		
		Log.debug("DEBUG: Execution of function 'fn_CreateLead_ConvertToMultipleAcct' Started.");
		
		//Step 1: Read Lead Creation data.
		String excelResource = EnvironmentDetails.Path_ExternalFiles
				+ System.getProperty("file.separator") + strLeadCreationWB;
		File fileLeadCreationData = new File(excelResource);
		lstmapLeadCreationData = objExcel.fn_ReadExcelAsMap(fileLeadCreationData,strLeadToAccountSheet); 
		for (Map<String, String> mapLeadCreationData : lstmapLeadCreationData) {
			//Select data depends on unique key 
			if (mapLeadCreationData.get("Uniquekey").toString().trim().equalsIgnoreCase(EnvironmentDetails.mapGlobalVariable.get("struniquekey").toString())) {
				strPhone = mapLeadCreationData.get("Phone").toString().trim();
				strCountry = mapLeadCreationData.get("Country").toString().trim();
				EnvironmentDetails.mapGlobalVariable.put("strCountry", strCountry);
				strLeadStatus = mapLeadCreationData.get("Lead Status").toString().trim();
				strLeadStage = mapLeadCreationData.get("Lead Stage").toString().trim();
				strApprovalStatus = mapLeadCreationData.get("Approval Status").toString().trim();
				strIndustry = mapLeadCreationData.get("Industry").toString().trim();
				strSubject = mapLeadCreationData.get("Subject").toString().trim();
				strConversionStatus = mapLeadCreationData.get("Conversion Status").toString().trim();
				strStreetAddress = mapLeadCreationData.get("Street Address").toString().trim();
				strCity = mapLeadCreationData.get("City").toString().trim();
				strState = mapLeadCreationData.get("State/Province").toString().trim();
				strStateCode = mapLeadCreationData.get("State Code").toString().trim();			
				strCountyCode = mapLeadCreationData.get("County Code").toString().trim();
				strPostCode = mapLeadCreationData.get("Post Code").toString().trim();
				strContactCountry = mapLeadCreationData.get("Contact Country").toString().trim();
				strDecisionRole = mapLeadCreationData.get("Contact Decision Role").toString().trim();
				strEntityName = mapLeadCreationData.get("EntityName").toString().trim();
				strQuoteType = mapLeadCreationData.get("Quote Type").toString().trim();
				
				mapAcctAddrData.put("strStreetAddress", strStreetAddress);
				mapAcctAddrData.put("strCity", strCity);
				mapAcctAddrData.put("strCountyCode", strCountyCode);
				mapAcctAddrData.put("strPostCode", strPostCode);
				mapAcctAddrData.put("strContactCountry", strContactCountry);
				mapAcctAddrData.put("strDecisionRole", strDecisionRole);
				mapAcctAddrData.put("strState", strState);
			}
			
			
			
		}
		
		
		//Step 2: Read Account Creation data.
		String excelAcctCreation = EnvironmentDetails.Path_ExternalFiles
				+ System.getProperty("file.separator") + strMulAcctCreationWB;
		File fileAcctCreationData = new File(excelAcctCreation);
		lstmapfileAcctCreationData = objExcel.fn_ReadExcelAsMap(fileAcctCreationData,strMulAcctCreationSheet); 
		for (Map<String, String> mapfileAcctCreationData : lstmapfileAcctCreationData) {
			//Select data depends on unique key 
			if (mapfileAcctCreationData.get("Company_Name").toString().trim().equalsIgnoreCase(EnvironmentDetails.mapGlobalVariable.get("struniquekey").toString())) {
				strFirstName = mapfileAcctCreationData.get("Contact_FirstName").toString().trim();
				strLastName = mapfileAcctCreationData.get("Contact_LastName").toString().trim();
				strCompanyName = mapfileAcctCreationData.get("Company_Name").toString().trim();
				strAccountType = mapfileAcctCreationData.get("Account_Type").toString().trim();
				strOpptyName = mapfileAcctCreationData.get("Oppty_Name").toString().trim();
				strBillingInformation = mapfileAcctCreationData.get("BillingInformation").toString().trim();
				arrBillingData = strBillingInformation.split("#");
				strEmailID = mapfileAcctCreationData.get("EmailID").toString().trim();
				strShipTo = mapfileAcctCreationData.get("ShipTo").toString().trim();
				strBillTo = mapfileAcctCreationData.get("BillTo").toString().trim();
				EnvironmentDetails.mapGlobalVariable.put("strGMSAStartDate", mapfileAcctCreationData.get("GMSA Start Date").toString().trim());
				strFirstNameOri = strFirstName;
				strLastNameOri = strLastName;
			}
		}

		//Step 3: Create Lead.
		objLeadCreation.fn_selectLeadsTab(driver, webWait).click();
		objLeadCreation.fn_clickNew(driver, webWait).click();
		try {
			objLeadCreation.fn_clickContinuebtn(driver, new WebDriverWait(driver,3)).click();
		} catch (Exception e) {
			Log.debug("Continue button not present for new lead creation.");
		}
		Date date = new Date() ;
		SimpleDateFormat dateFormat = new SimpleDateFormat("mmss");  
		strFirstName = strFirstNameOri+"_1_"+dateFormat.format(date);
		objLeadCreation.fn_selectFirstName(driver, webWait).selectByValue("Ms.");
		objLeadCreation.fn_setFirstName(driver, webWait).sendKeys(strFirstName);
		strLastName = strLastNameOri+"_1_"+dateFormat.format(date);	
		objLeadCreation.fn_setLastName(driver, webWait).sendKeys(strLastName);
		//TODO:: generating the New proposal name with timestamp to make the name unique for further validations
    	strCompanyName = strCompanyName+"_"+dateFormat.format(date);	
    	objLeadCreation.fn_setCompany(driver, webWait).sendKeys(strCompanyName);
    	objLeadCreation.fn_setPhone(driver, webWait).sendKeys(strPhone);
    	try {
    		objLeadCreation.fn_selQuoteType(driver, webWait).selectByValue(strQuoteType);		} 
    	catch (Exception e) {
    		Log.debug("DEBUG: The Quote Type field is not present on the UI page of Leads");	}
    	  

    	//Set Lead Address
    	try {
			objLeadCreation.fn_setLeadCountry(driver, new WebDriverWait(driver,1)).clear();
			objLeadCreation.fn_setLeadCountry(driver, webWait).sendKeys(strContactCountry);		} 
    	catch (Exception e) {
			objLeadCreation.fn_selLeadCountryUK(driver, new WebDriverWait(driver,1)).selectByValue(strContactCountry);;		}
    	try {
    		objLeadCreation.fn_setLeadStreetAdress(driver, new WebDriverWait(driver,1)).clear();
    		objLeadCreation.fn_setLeadStreetAdress(driver, webWait).sendKeys(strStreetAddress); 		} 
    	catch (Exception e) {
			objLeadCreation.fn_setLeadStreetAdressUK(driver, new WebDriverWait(driver,1)).clear();
    		objLeadCreation.fn_setLeadStreetAdressUK(driver, webWait).sendKeys(strStreetAddress);		}
    	try {
    		objLeadCreation.fn_setLeadCity(driver, new WebDriverWait(driver,1)).clear();
    		objLeadCreation.fn_setLeadCity(driver, webWait).sendKeys(strCity);		} 
    	catch (Exception e) {
    		Log.debug("DEBUG: The City field is not present on the UI page of Leads");	}
    	try {
			objLeadCreation.fn_setLeadState(driver, new WebDriverWait(driver,1)).clear();
			objLeadCreation.fn_setLeadState(driver, webWait).sendKeys(strStateCode);		} 
    	catch (Exception e) {
			objLeadCreation.fn_selLeadStateUK(driver, new WebDriverWait(driver,1)).selectByValue(strState);;		}
		try {
			objLeadCreation.fn_setLeadPostalCode(driver, new WebDriverWait(driver,1)).clear();
			objLeadCreation.fn_setLeadPostalCode(driver, webWait).sendKeys(strPostCode);	} 
    	catch (Exception e) {
    		objLeadCreation.fn_setLeadPostalCodeUK(driver, new WebDriverWait(driver,1)).clear();
			objLeadCreation.fn_setLeadPostalCodeUK(driver, webWait).sendKeys(strPostCode);		}
		
		
		
		

		
    	
		try {			
			objLeadCreation.fn_selectCountry(driver, new WebDriverWait(driver,2)).selectByValue(strCountry); }          		
		catch(Exception excpEnvironment){
			Log.debug("DEBUG: The country field is not present on the UI page of Leads");}	
		objLeadCreation.fn_selectLeadStatus(driver, webWait).selectByVisibleText(strLeadStatus);
		try {objLeadCreation.fn_selectLeadStage(driver, new WebDriverWait(driver,1)).selectByVisibleText(strLeadStage); }          		
		catch(Exception excpEnvironment){Log.debug("DEBUG: The Lead Stage is not present on the UI page of Leads");}
		try {objLeadCreation.fn_selectLeadTactic(driver, new WebDriverWait(driver,1)).selectByIndex(1);; }          		
		catch(Exception excpEnvironment){Log.debug("DEBUG: The Lead Tactic is not present on the UI page of Leads");}
		try {			
			objLeadCreation.fn_selecApprovalStatus(driver, new WebDriverWait(driver,2)).selectByVisibleText(strApprovalStatus); }          		
		catch(Exception excpEnvironment){
			Log.debug("DEBUG: The Approval Status is not present on the UI page of Leads");}	
		try {			
			objLeadCreation.fn_selectAcctType(driver, new WebDriverWait(driver,2)).selectByVisibleText(strAccountType); }          		
		catch(Exception excpEnvironment){
			Log.debug("DEBUG: The Account Type is not present on the UI page of Leads");}
		try {objLeadCreation.fn_selDataSource(driver, new WebDriverWait(driver,1)).selectByIndex(1);; }          		
		catch(Exception excpEnvironment){Log.debug("DEBUG: The Data Source is not present on the UI page of Leads");}
		try {objLeadCreation.fn_selGroupemployee(driver, new WebDriverWait(driver,1)).selectByIndex(1);; }          		
		catch(Exception excpEnvironment){Log.debug("DEBUG: The Group employee is not present on the UI page of Leads");}
		try {objLeadCreation.fn_setAnnualRevenue(driver, new WebDriverWait(driver,1)).clear();; }          		
		catch(Exception excpEnvironment){Log.debug("DEBUG: The Annual Revenue is not present on the UI page of Leads");}
		objLeadCreation.fn_selectIndustry(driver, webWait).selectByVisibleText(strIndustry);
		objLeadCreation.fn_clickSaveBtn(driver, webWait).click();
		
		//Close reminder window
        objUtils.fn_CloseRemainder(driver, webWait);
		
		//For UAT Approve Lead Using finance User
		if (EnvironmentDetails.mapEnvironmentDetails.get("Environment").toString().equalsIgnoreCase("UAT")) {
			objLeadCreation.fn_clickLeadConvertBtn(driver, webWait).isEnabled();
			String strLeadURL = driver.getCurrentUrl();
			//Step 1: Logout from existing User
      		ApptusLoginLogout.appLogout(driver, webWait);
      		//Step 2: Login With Finance User.
      		ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("FinanceUserName"), EnvironmentDetails.mapEnvironmentDetails.get("FinancePassWord"));
      		Thread.sleep(2000);
      		driver.navigate().to(strLeadURL);
      		objLeadCreation.fn_clickEditBtn(driver, webWait).isEnabled();
      		objLeadCreation.fn_clickEditBtn(driver, webWait).click();
      		objLeadCreation.fn_clickSaveBtn(driver, webWait).isEnabled();
      		objLeadCreation.fn_selecApprovalStatus(driver, new WebDriverWait(driver,2)).selectByVisibleText(strApprovalStatus);
      		objLeadCreation.fn_clickSaveBtn(driver, webWait).click();
      		//Step 1: Logout from existing User
      		ApptusLoginLogout.appLogout(driver, webWait);
      		if (EnvironmentDetails.mapGlobalVariable.get("struniquekey").toString().contains("US")) {
				ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("USAUserName"), EnvironmentDetails.mapEnvironmentDetails.get("USAPassWord"));
			}
			else if (EnvironmentDetails.mapGlobalVariable.get("struniquekey").toString().contains("CAN")) {
				ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("CANUserName"), EnvironmentDetails.mapEnvironmentDetails.get("CANPassWord"));
			}
			else if (EnvironmentDetails.mapGlobalVariable.get("struniquekey").toString().contains("UK")) {
				ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("UKUserName"), EnvironmentDetails.mapEnvironmentDetails.get("UKPassWord"));
			}
			else if (EnvironmentDetails.mapGlobalVariable.get("struniquekey").toString().contains("IRE")) {
				ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("IREUserName"), EnvironmentDetails.mapEnvironmentDetails.get("IREPassWord"));
			}
			else if (EnvironmentDetails.mapGlobalVariable.get("struniquekey").toString().contains("LUX")) {
				ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("LUXUserName"), EnvironmentDetails.mapEnvironmentDetails.get("LUXPassWord"));
			}
      		Thread.sleep(2000);
      		driver.navigate().to(strLeadURL);
      		objLeadCreation.fn_clickLeadConvertBtn(driver, webWait).isEnabled();
		
		}
		
		//Step 4: Convert Lead to account.
		objLeadCreation.fn_clickLeadConvertBtn(driver, webWait).click();
		Thread.sleep(8000);
		driver.switchTo().activeElement().sendKeys(Keys.ESCAPE);
		objLeadCreation.fn_selectAccountName(driver, webWait).selectByVisibleText("Create New Account: "+strCompanyName);
		strOpptyName = strOpptyName+"_"+dateFormat.format(date);
		objLeadCreation.fn_setOpptyName(driver, webWait).sendKeys(strOpptyName);
		objLeadCreation.fn_setSubject(driver, webWait).sendKeys(strSubject);
		objLeadCreation.fn_setDuedate(driver, webWait).click();
		objLeadCreation.fn_selectStatus(driver, webWait).selectByVisibleText(strConversionStatus);
		objLeadCreation.fn_clickConvertBtn(driver, webWait).click();
		objLeadCreation.fn_getEntityName(driver, webWait).isDisplayed();
		
		//TODO :: Entity Name check
		if (objLeadCreation.fn_getEntityName(driver, webWait).getText().contentEquals(strEntityName))
			Log.debug("DEBUG: The entity Name is displayed on the Accounts page and matches with the expected Result :: "+objLeadCreation.fn_getEntityName(driver, webWait).getText());
		else
			Log.debug("DEBUG: The entity Name displayed doesnt matches with the expected Result ::"+objLeadCreation.fn_getEntityName(driver, webWait).getText());
     
	    //strParentWindow = driver.getWindowHandle();	
	    
	   /* objLeadCreation.fn_clickEditBtn(driver, webWait).click();
		//Step 5: Enter the Address Information on Accounts page.
	    objLeadCreation.fn_setStreetAdress(driver, webWait).clear();
  		objLeadCreation.fn_setStreetAdress(driver, webWait).sendKeys(strStreetAddress);
  		objLeadCreation.fn_setcity(driver, webWait).clear();
  		objLeadCreation.fn_setcity(driver, webWait).sendKeys(strCity);
  		objLeadCreation.fn_setState(driver, webWait).clear();
  		objLeadCreation.fn_setState(driver, webWait).sendKeys(strState);
  		objLeadCreation.fn_setcounty(driver, webWait).clear();
  		objLeadCreation.fn_setcounty(driver, webWait).sendKeys(strCountyCode);
  		objLeadCreation.fn_setPostCode(driver, webWait).clear();
  		objLeadCreation.fn_setPostCode(driver, webWait).sendKeys(strPostCode);
	  	objLeadCreation.fn_clickSaveBtn(driver, webWait).click();*/
	  		
		
		 
	  	//Step 7: Update contact address.
		try {
			objLeadCreation.fn_clickEditContactLink(driver, new WebDriverWait(driver,10)).click();
		} catch (Exception e) {
			objLeadCreation.fn_clickEditContactDevLink(driver, webWait).click();
		}
		objMultipleAccts.fn_EnterContactdetails(driver, webWait,"svc.mule.integration.int@sungardas.com",mapAcctAddrData);
		
		
		
		//TODO:: Add New Address
		objLeadCreation.fn_clickNewAddressBtn(driver, webWait).click();		
		objMultipleAccts.fn_createContactandLinkAdress(driver, webWait,"TestAutoaddress_1",strEmailID,strShipTo,strBillTo,strLastName, mapAcctAddrData);
		
		//Step 8: Add New customer reference.
        objLeadCreation.fn_clickNewCustomerReferenceBtn(driver, webWait).click();
        strCRname = strCompanyName+"_CR";
        objLeadCreation.fn_setTierName(driver, webWait).sendKeys(strCRname);
        objLeadCreation.fn_clickSaveOnCustomerEditPage(driver, webWait).click();
        objLeadCreation.fn_clickAccountLink(driver, webWait).click();
        
        //Step 9: Add new Contact and then link to address.	
        objLeadCreation.fn_clickNewContact(driver, webWait).click();
        try {			
        	objLeadCreation.fn_clickOpptyContBtn(driver, new WebDriverWait(driver,2)).click();  }        		
		catch(Exception excpEnvironment){
			Log.debug("DEBUG: The Continue button is not present on the UI page of Contact");}	
        
        //--//
        if (EnvironmentDetails.mapEnvironmentDetails.get("Environment").toString().equalsIgnoreCase("DEV") || EnvironmentDetails.mapEnvironmentDetails.get("Environment").toString().equalsIgnoreCase("UAT")) {
        	objLeadCreation.fn_setContactAcctName(driver, webWait).clear();
        	objLeadCreation.fn_setContactAcctName(driver, webWait).sendKeys(strCompanyName);;
		}
        objLeadCreation.fn_selectFirstName(driver, webWait).selectByValue("Ms.");
		strFirstName = strFirstNameOri+"_2_"+dateFormat.format(date);
		objLeadCreation.fn_setFirstName(driver, webWait).sendKeys(strFirstName);
        strLastName = strLastNameOri+"_2_"+dateFormat.format(date);	
        objLeadCreation.fn_setContactLastName(driver, webWait).sendKeys(strLastName);
        objMultipleAccts.fn_EnterContactdetails(driver, webWait,"Email"+dateFormat.format(date)+"@sungardas.com",mapAcctAddrData);  
        //objMultipleAccts.fn_EnterContactdetails(driver, webWait,"svc.mule.integration.int@sungardas.com",mapAcctAddrData);
        
        //Close reminder window
        objUtils.fn_CloseRemainder(driver, webWait);
		
		//TODO:: Add New Address
		objLeadCreation.fn_clickNewAddressBtn(driver, webWait).click();
		objMultipleAccts.fn_createContactandLinkAdress(driver, webWait,"TestAutoaddress_2",strEmailID,strShipTo,strBillTo,strLastName, mapAcctAddrData);
		
		//Close reminder window
        objUtils.fn_CloseRemainder(driver, webWait);
		
		//Get Account Address ID Name
		objLeadCreation.fn_getEditAddeLink(driver, webWait).isEnabled();
		try {
  			strAcctAddrID = objLeadCreation.fn_getAcctAddrID(driver, new WebDriverWait(driver,2)).getText();
      		//Save Account Address ID name in global variable map 
			 EnvironmentDetails.mapGlobalVariable.put("strAcctAddrID", strAcctAddrID);
			 Log.debug("DEBUG: Account Address ID Name save in global variable map with key 'strAcctAddrID'.");
		} catch (Exception e) {
			Log.debug("DEBUG: Account Address ID Name Not present on Account page");
		}
		
        //Step 10: Update Opportunity details.
        objLeadCreation.fn_clickEditOppLink(driver, webWait).isEnabled();
        Thread.sleep(3500);
       objLeadCreation.fn_clickEditOppLink(driver, webWait).click();
        OpportunitesFormPageObject objOpportunitesFormPageObject = new OpportunitesFormPageObject();
       try {			
        	objOpportunitesFormPageObject.fn_selQuoteType(driver, new WebDriverWait(driver,3)).selectByValue(strQuoteType); }          		
		catch(Exception excpEnvironment){
			Log.debug("DEBUG: The Quote Type field is not present on the UI page of Opportunity form");}	
        try {			
        	 objOpportunitesFormPageObject.fn_selOppType(driver, new WebDriverWait(driver,3)).selectByValue("New Logo");;; }          		
		catch(Exception excpEnvironment){
			Log.debug("DEBUG: The Opportunity Type field is not present on the UI page of Opportunity form");}
        try {			
       	 objOpportunitesFormPageObject.fn_selLeadTactic(driver, new WebDriverWait(driver,3)).selectByIndex(1);; }          		
		catch(Exception excpEnvironment){
			Log.debug("DEBUG: The Lead Tactic field is not present on the UI page of Opportunity form");}
        
        
        //Code to Add Multiple Options for Competitors
		//String strOptions="";
		List<WebElement> lstwebAllOptionsAdd  = objOpportunitesFormPageObject.fn_selectMultipleOnCompetitors(driver, webWait).getOptions(); 
		lstwebAllOptionsAdd.get(1).click();
		objOpportunitesFormPageObject.fn_ClickImgAddOnAddressEditPage(driver, webWait).click();
		if (strCountry.equalsIgnoreCase("Ireland")) {
			objOpportunitesFormPageObject.fn_setPriceBook(driver, webWait).clear();
			objOpportunitesFormPageObject.fn_setPriceBook(driver, webWait).sendKeys(strCountry);;
		}
		objLeadCreation.fn_clickSaveBtn(driver, webWait).click();
        
        
      //Step 6: Enter the Billing Information on Acct page which will be further used by Aria.
      		objLeadCreation.fn_clickNewAddressBtn(driver, webWait).isEnabled();
      		//Step 1: Logout from existing User
      		ApptusLoginLogout.appLogout(driver, webWait);
      		//Step 2: Login With Finance User.
      		ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("FinanceUserName"), EnvironmentDetails.mapEnvironmentDetails.get("FinancePassWord"));
      		objAcctpage.fn_setSearchField(driver, webWait).sendKeys(strCompanyName); 
      		objAcctpage.fn_clickSearchBtn(driver, webWait).click();
      		Thread.sleep(3500);
      		//Some time Account visibility for finance user take time
      		try {
      			driver.findElement(By.linkText(strCompanyName)).click();
			} catch (Exception e) {
				Thread.sleep(10000);
				objAcctpage.fn_setSearchField(driver, webWait).clear();
				objAcctpage.fn_setSearchField(driver, webWait).sendKeys(strCompanyName); 
	      		objAcctpage.fn_clickSearchBtn(driver, webWait).click();
	      		Thread.sleep(3500);
	      		driver.findElement(By.linkText(strCompanyName)).click();
			}
      		
      		
      		//Step 2a: activate each address.
      		objLeadCreation.fn_getEditAddeLink(driver, webWait).isEnabled();
      		List<WebElement> lstEleEditAddeLinks = objLeadCreation.fn_getEditAddeLinks(driver, webWait);
      		
      		
      		
      		for (int i = 0; i < lstEleEditAddeLinks.size(); i++) {
      			objLeadCreation.fn_getEditAddeLink(driver, webWait).isEnabled();
      			lstEleEditAddeLinks = objLeadCreation.fn_getEditAddeLinks(driver, webWait);
      			lstEleEditAddeLinks.get(i).click();
      			objLeadCreation.fn_clickSaveBtn(driver, webWait).isEnabled();
      			objLeadCreation.fn_selAddressStatus(driver, webWait).selectByValue("Active");
      			objLeadCreation.fn_clickSaveBtn(driver, webWait).click();
			}
      		
      		/*for (WebElement eleEditAddeLink : lstEleEditAddeLinks) {
      			objLeadCreation.fn_getEditAddeLink(driver, webWait).isEnabled();
      			eleEditAddeLink.click();
      			objLeadCreation.fn_clickSaveBtn(driver, webWait).isEnabled();
      			objLeadCreation.fn_selAddressStatus(driver, webWait).selectByValue("Active");
      			objLeadCreation.fn_clickSaveBtn(driver, webWait).click();
			}*/
      		
      		objLeadCreation.fn_clickEditBtn(driver, webWait).isEnabled();
      		objLeadCreation.fn_clickEditBtn(driver, webWait).click();
      		
      		//try catch -- for billing information not available in Dev
      		try{
	      		objLeadCreation.fn_setTaxExemptCertificateNumber(driver, new WebDriverWait(driver,3)).isDisplayed();
	      		objLeadCreation.fn_selectPaymentMethod(driver, webWait).selectByValue(arrBillingData[0]);
	    		objLeadCreation.fn_selectPaymentTerm(driver, webWait).selectByValue(arrBillingData[1]);
	    		/*if (objLeadCreation.fn_clickBillingApprovalchk(driver, webWait).isSelected()) {
				objLeadCreation.fn_clickBillingApprovalchk(driver, webWait).click();}*/
	    		Assert.assertEquals(objLeadCreation.fn_clickBillingApprovalchk(driver, webWait).isSelected(), true);
	    		objLeadCreation.fn_selectTaxExemptLevel(driver, webWait).selectByValue(arrBillingData[2]);
	    		if (arrBillingData[2].equalsIgnoreCase("Tax-Exempt")) {
	    			objLeadCreation.fn_setTaxExemptCertificateNumber(driver, webWait).sendKeys(arrBillingData[3]);
	        		objLeadCreation.fn_setTaxPayerID(driver, webWait).sendKeys(arrBillingData[4]);
	        		objLeadCreation.fn_setTaxPayerEndDate(driver, webWait).sendKeys(objUtils.fn_getFutureDate(12, "M/d/yyyy"));
				}
      		}
      		catch(Exception excpEnvironment){
    			Log.debug("DEBUG: billing information not present.");}
    		
    		try {			
    			objLeadCreation.fn_selIndustryRollup(driver, new WebDriverWait(driver,1)).selectByIndex(1);; }          		
    		catch(Exception excpEnvironment){
    			Log.debug("DEBUG: The Industry Rollup field is not present on the UI page of Account");}	
    		objLeadCreation.fn_clickSaveBtn(driver, webWait).click();
    		ApptusLoginLogout.appLogout(driver, webWait);
    		
    		//Login As per Country 
    		if (strCountry.equalsIgnoreCase("United States")) {
    			ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("USAUserName"), EnvironmentDetails.mapEnvironmentDetails.get("USAPassWord"));
			}
    		else if (strCountry.equalsIgnoreCase("Canada")) {
    			ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("CANUserName"), EnvironmentDetails.mapEnvironmentDetails.get("CANPassWord"));
			}
    		else if (strCountry.equalsIgnoreCase("United Kingdom")) {
				ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("UKUserName"), EnvironmentDetails.mapEnvironmentDetails.get("UKPassWord"));
			}
			else if (strCountry.equalsIgnoreCase("Ireland")) {
				ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("IREUserName"), EnvironmentDetails.mapEnvironmentDetails.get("IREPassWord"));
			}
			else if (strCountry.equalsIgnoreCase("Luxembourg")) {
				ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("LUXUserName"), EnvironmentDetails.mapEnvironmentDetails.get("LUXPassWord"));
			}
      		objAcctpage.fn_setSearchField(driver, webWait).sendKeys(strCompanyName); 
      		objAcctpage.fn_clickSearchBtn(driver, webWait).click();
      		Thread.sleep(3500);
      		driver.findElement(By.linkText(strCompanyName)).click();
      		objLeadCreation.fn_clickNewAddressBtn(driver, webWait).isEnabled();
      		
      		
      		
      		
      		
		
		Log.debug("DEBUG: Execution of function 'fn_CreateLead_ConvertToMultipleAcct' Completed.");
		return strCompanyName;
    }
	
	//TODO:: Create Multiple contacts and link the contacts with address
	public void fn_createContactandLinkAdress(WebDriver driver,WebDriverWait webWait,String strAddressType,String strEmailID,String strShipTo,String strBillTo,String strLastName, Map<String, String> mapAcctAddrData) throws InterruptedException{
		
		LeadCreationObjects objctsLead = new LeadCreationObjects();	
		Utils objUtils = new Utils();
		String strOptions="";
		String[] arrShipTo = strShipTo.split("#");
		String[] arrBillTo = strBillTo.split("#");
		String strCity = mapAcctAddrData.get("strCity");
		String strCountry= mapAcctAddrData.get("strContactCountry");
		String strPostCode = mapAcctAddrData.get("strPostCode");
		String strState = mapAcctAddrData.get("strState");
	
		
		Log.debug("DEBUG: Execution of function 'fn_createContactandLinkAdress' Started.");
		
		//Environment Exception catched for if Country present and Selected.
		try {
			objctsLead.fn_selectAddressStatus(driver, webWait).selectByVisibleText("Active"); }       		
		catch(Exception excpEnvironment){
			Log.debug("DEBUG: The Approval Status is not present on the UI page of Address Details page");}
		
		//TODO:: fillup the Adress feilds on Adress edit page
		objctsLead.fn_setAddress1(driver, webWait).sendKeys(strAddressType);
		objctsLead.fn_setCity(driver, webWait).sendKeys(strCity);
		
		
		
		//Environment Exception catched for if Country present and Selected.
		try {			
			objctsLead.fn_selectAddressCounty(driver, webWait).selectByVisibleText(strCountry); }       		
		catch(Exception excpEnvironment){
			Log.debug("DEBUG: The Address County is not present on the UI page of Address Details page");}
		
		objctsLead.fn_selectCountryOnAddressEditpage(driver, webWait).selectByVisibleText(strCountry);
		objctsLead.fn_setPostalCodeOnAddressEditpage(driver, webWait).sendKeys(strPostCode);
		try {
			objctsLead.fn_selectState(driver, new WebDriverWait(driver,1)).selectByVisibleText(strState);		} 
    	catch (Exception e) {
    		try {
    			objctsLead.fn_selectAddressCounty(driver, new WebDriverWait(driver,1)).selectByValue(strState);;
			} catch (Exception e2) {
				objctsLead.fn_selectAddressCountyDev(driver, new WebDriverWait(driver,1)).selectByValue(strState);;
			}
		}
		
		
		
		if (strAddressType.contains("TestAutoaddress_1"))
		{
		//Code to Add Multiple Options
		List<WebElement> lstAllOptionsAdd  = objctsLead.fn_selectMultipleOnAddressEditpage(driver, webWait).getOptions(); 
		for(WebElement webElement : lstAllOptionsAdd ){
			strOptions = webElement.getText();
			for (int i=0;i<arrShipTo.length;i++)
			     if (strOptions.equalsIgnoreCase(arrShipTo[i]))
			    	 webElement.click();}
		}
		else
		{
		//Code to Add Multiple Options
		List<WebElement> lstAllOptionsAdd  = objctsLead.fn_selectMultipleOnAddressEditpage(driver, webWait).getOptions(); 
		for(WebElement webElement : lstAllOptionsAdd ){
			strOptions = webElement.getText();
			for (int i=0;i<arrBillTo.length;i++)
			     if (strOptions.equalsIgnoreCase(arrBillTo[i]))
			    	 webElement.click();}
			
		}
		
		objctsLead.fn_clickImgAddOnAddressEditPage(driver, webWait).click();
		
		Thread.sleep(500);
        objctsLead.fn_clickSaveAddOnAddressEditPage(driver, webWait).click();
        Thread.sleep(5000);
       
        //TODO::New address Contact page      
        objctsLead.fn_clickContactOnAddressBtnOnContactEditPage(driver, webWait).click();
        Thread.sleep(5000);
        
        //TODO::fill up the GMSA Form details
	    objUtils.fn_HandleLookUpWindow(driver, webWait,"ContactLukUpWindwOnAdCtctEdtPage",strLastName);
	    
		if (strAddressType.contains("TestAutoaddress_1"))
		{
		List<WebElement> lstAllOptionsAddUser  = objctsLead.fn_selectUsedForOnAddressContactEditpage(driver, webWait).getOptions(); 
		for(WebElement webElement : lstAllOptionsAddUser ){
			strOptions = webElement.getText();
			for (int i=0;i<arrShipTo.length;i++)
			    if (strOptions.equalsIgnoreCase(arrShipTo[i]))
			      webElement.click();}
		}
		else
	    {
		List<WebElement> lstAllOptionsAddUser  = objctsLead.fn_selectUsedForOnAddressContactEditpage(driver, webWait).getOptions(); 
		for(WebElement webElement : lstAllOptionsAddUser ){
			strOptions = webElement.getText();
			for (int i=0;i<arrBillTo.length;i++)
			    if (strOptions.equalsIgnoreCase(arrBillTo[i]))
			      webElement.click();}
		}
		
        //Code to remove the Multiple Options from LOV
		objctsLead.fn_clickImgAddOnAddressEditPage(driver, webWait).click();
		
		Thread.sleep(500);
        objctsLead.fn_setEmailOnAdCtctEdtPage(driver, webWait).sendKeys("svc.mule.integration.int@sungardas.com");
        objctsLead.fn_clickSaveOnAdCtctEdtPage(driver, webWait).click();  
        objctsLead.fn_clickContactLnkSaveOnAdCtctEdtPage(driver, webWait).click(); 
        Thread.sleep(5000);
        objctsLead.fn_clickAccountNameLink(driver, webWait).click();
        Thread.sleep(15000);
        
        Log.debug("DEBUG: Execution of function 'fn_createContactandLinkAdress' Completed.");
		
	}
	
	public void fn_EnterContactdetails(WebDriver driver,WebDriverWait webWait,String strEmailID,Map<String,String> mapAcctAddrData) throws InterruptedException{
			
			Log.debug("DEBUG: Execution of function 'fn_EnterContactdetails' Started.");
		    LeadCreationObjects objLeadCreation = new LeadCreationObjects();
	        objLeadCreation.fn_setCntactDecisionRole(driver, webWait).sendKeys(mapAcctAddrData.get("strDecisionRole"));
			objLeadCreation.fn_setEmailOnContactPage(driver, webWait).sendKeys(strEmailID);
		/*	try {
	    		objLeadCreation.fn_setCntactStreetAdress(driver, new WebDriverWait(driver,1)).clear();
	    		objLeadCreation.fn_setCntactStreetAdress(driver, webWait).sendKeys(mapAcctAddrData.get("strStreetAddress")); 		} 
	    	catch (Exception e) {
				objLeadCreation.fn_setLeadStreetAdressUK(driver, new WebDriverWait(driver,1)).clear();
	    		objLeadCreation.fn_setLeadStreetAdressUK(driver, webWait).sendKeys(mapAcctAddrData.get("strStreetAddress"));		}
			try {
	    		objLeadCreation.fn_setCntactCity(driver, new WebDriverWait(driver,1)).clear();
	    		objLeadCreation.fn_setCntactCity(driver, webWait).sendKeys(mapAcctAddrData.get("strCity")); 		} 
	    	catch (Exception e) {
				objLeadCreation.fn_setLeadCity(driver, new WebDriverWait(driver,1)).clear();
	    		objLeadCreation.fn_setLeadCity(driver, webWait).sendKeys(mapAcctAddrData.get("strCity"));		}
			try {
	    		objLeadCreation.fn_setCntactPostalCode(driver, new WebDriverWait(driver,1)).clear();
	    		objLeadCreation.fn_setCntactPostalCode(driver, webWait).sendKeys(mapAcctAddrData.get("strPostCode")); 		} 
	    	catch (Exception e) {
				objLeadCreation.fn_setLeadPostalCodeUK(driver, new WebDriverWait(driver,1)).clear();
	    		objLeadCreation.fn_setLeadPostalCodeUK(driver, webWait).sendKeys(mapAcctAddrData.get("strPostCode"));		}
			//objLeadCreation.fn_selectCntactCounty(driver, webWait).sendKeys(mapAcctAddrData.get("strCountyCode"));
			try {
	    		objLeadCreation.fn_setCntactCountry(driver, new WebDriverWait(driver,1)).clear();
	    		objLeadCreation.fn_setCntactCountry(driver, webWait).sendKeys(mapAcctAddrData.get("strContactCountry")); 		} 
	    	catch (Exception e) {
	    		objLeadCreation.fn_setLeadCountryUK(driver, new WebDriverWait(driver,2)).selectByValue(mapAcctAddrData.get("strContactCountry"));		}
		*/	
			objLeadCreation.fn_setJobTitle(driver, webWait).sendKeys("Test Job");
			objLeadCreation.fn_clickSaveOnContactPage(driver, webWait).click();
			Thread.sleep(2000);
			objLeadCreation.fn_clickAccountNameLink(driver, webWait).click();
			
			try {			
				objLeadCreation.fn_clickAccountHierarchyLink(driver, new WebDriverWait(driver,2)).click(); 
				Thread.sleep(1000);}          		
			catch(Exception excpEnvironment){
				Log.debug("DEBUG: The AccountHierarchy Link is not present on the UI page of Accounts");}
			Log.debug("DEBUG: Execution of function 'fn_EnterContactdetails' Completed.");
	}
	
	
	public Boolean fn_MultipleAccountCreation(WebDriver driver,WebDriverWait webWait, Map<String, Object> mapMultipleAcct) throws Throwable{
		
		CreateLeadandConvertToMultipleAcctNA objLeadMultipleAcctCreation = new CreateLeadandConvertToMultipleAcctNA();
		//EnvironmentDetails objEnvironment = new EnvironmentDetails();
		//WriteTextFiles objTxtFile = new WriteTextFiles();
		Utils objUtils = new Utils();
		ExcelHandler objExcel = new ExcelHandler();
		Boolean blnResult=false;	
		String strAcctName="";
	    Map<String, Object> mapOutputData = new HashMap<String, Object>();
	    List<Map<String, String>> lstAcctCreationDataMap = new ArrayList<Map<String,String>>();
	    ArrayList<String> lstAcctNames = new ArrayList<String>();	
		 String strMulAcctCreationSheet = mapMultipleAcct.get("strMulAcctCreationSheet").toString();
		 String strMulAcctCreationWB = mapMultipleAcct.get("strMulAcctCreationWB").toString();
		
		
		
		try{

			Log.debug("DEBUG: Execution of function 'fn_MultipleAccountCreation' Started.");
			
			//TODO : Read Test Excel
			String excelResource = EnvironmentDetails.Path_ExternalFiles+ System.getProperty("file.separator") + strMulAcctCreationWB;
			File fileProductInfo = new File(excelResource);
			lstAcctCreationDataMap = objExcel.fn_ReadExcelAsMap(fileProductInfo,strMulAcctCreationSheet); 
			
			//TODO: Create account of each data row in MulAcctCreationSheet
			//for (int i = 0; i < lstAcctCreationDataMap.size(); i++) {
			
			
				
				strAcctName=objLeadMultipleAcctCreation.fn_CreateLead_ConvertToMultipleAcct(driver, webWait, mapMultipleAcct);
				lstAcctNames.add(strAcctName);
			//}
			
				
			 //blnResult=objTxtFile.fn_writeAcctName(lstAcctNames,strPathOfFile);
			
			
			 //Save Account name in global variable map 
			 EnvironmentDetails.mapGlobalVariable.put("strAcctName", strAcctName);
			 Log.debug("DEBUG: Account Name save in global variable map with key 'strAcctName'.");
			 
			//TODO:: Capture Screenshot for Account 
			 objUtils.fn_takeScreenshotFullPage(driver);
			 
			//TODO:: Write account name in output file 
			mapOutputData.put("Account Name", strAcctName);
			objUtils.fn_WriteExcelApttus("Account_Name"+EnvironmentDetails.mapGlobalVariable.get("struniquekey").toString(), mapOutputData);
			
			
			 
			 
		}catch (Exception e)
           {					    	 
	    	StringWriter stack = new StringWriter();
			e.printStackTrace(new PrintWriter(stack));
			Log.debug("DEBUG: The exception in Generating Multiple Accounts is: "+stack);
	        }
		
		Log.debug("DEBUG: Execution of function 'fn_MultipleAccountCreation' Completed.");
		blnResult = true;
		return blnResult;
	
	}


	
}
