package appModules;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObjects.SalesForce.*;
import utility.Log;

public class ApptusLoginLogout {
	
	public static void appLogin(WebDriver driver,WebDriverWait webWait,String username, String password) throws Exception
    {
		Login LogObjts = new Login();
		
        try
        {
        	LogObjts.fn_EnterUserName(driver, webWait).clear();
        	LogObjts.fn_EnterUserName(driver, webWait).sendKeys(username);
        	LogObjts.fn_EnterPassword(driver, webWait).clear();
        	LogObjts.fn_EnterPassword(driver, webWait).sendKeys(password);
        	LogObjts.fn_ClickLoginButton(driver, webWait).click();
            //utility.Utils.takeScreenshot(driver, "ariaLogin_BeforeAssert");
        }
        catch (Exception e)
        {
            Log.error("Unable to login to APPTUS");
            Log.error(e.getMessage());
         }
    }

    public static void appLogout(WebDriver driver,WebDriverWait webWait) 
    {

    	Login objLoginLogout = new Login();
    	 
        try
        {
            Log.debug("DEBUG: Logging out");
            objLoginLogout.fn_clickNavigateArrowBtn(driver, webWait).isEnabled();
            objLoginLogout.fn_clickNavigateArrowBtn(driver, webWait).click();
            objLoginLogout.fn_clickLogOutButton(driver, webWait).isEnabled();
            objLoginLogout.fn_clickLogOutButton(driver, webWait).click();
            Log.debug("DEBUG: User is successfully logged out of APPTUS");
        }
        catch (Exception e)
        {
            Log.error("Unable to logout from APPTUS application");
            Log.error(e.getMessage());
        }
    }

}
