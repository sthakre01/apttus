package appModules;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import pageObjects.SalesForce.Configuration;
import utility.Log;

public class Add_Configurations_for_Multiple_Products {
	
	public Boolean fn_Add_Configurations_for_Multiple_Products(WebDriver driver,WebDriverWait webWait,String strComponentName,List<List<Map<String, Object>>> lstmapResponse) throws Exception
    {
			
		Configuration objConfig = new Configuration();
		boolean blnmultipleConfiguration =false;
		
        try
        {   
        	
        	Object OptionName=null;
 		    String strXpath = null;
 		    String strQuipRefName ="";
            
        	objConfig.fn_SearchProducts(driver, webWait).sendKeys(strComponentName); 
        	objConfig.fn_clicksearchBtn(driver, webWait).click();
        	Thread.sleep(8000);  
        	objConfig.fn_clickServiceName(driver, webWait, strComponentName).click();   	
        	Thread.sleep(6000); 
        	strQuipRefName = strComponentName+"_Configuration";
        	objConfig.fn_setEquipReference(driver, webWait).sendKeys(strQuipRefName);
        	Thread.sleep(3500); 
        	objConfig.fn_clickNext(driver, webWait).click();
        	Thread.sleep(6000); 
         
        	for(List<Map<String, Object>> lstmap: lstmapResponse){
        	        	 
        	    for(Map<String, Object> map : lstmap){
        	        		 
        	        for (Map.Entry<String, Object> entry : map.entrySet()){    
        	        	       	        	        	               						
        	        	 OptionName = entry.getValue();
  	                
					     //Utils.takeScreenshot(driver, strTestCaseName);
					     Log.info("++++++++++++++++++++++++++++++++++++Start of Configuration of Services+++++++++++++++++++++++++++++++++++++++++++++");
			
						 strXpath = "//a[text()='"+OptionName+"']/ancestor::tr[1]/td[1]//input";	
						 try
						 {					
		                    //Identify the checkbox/radio button control
							WebElement webidentifychkbox = driver.findElement(By.xpath(strXpath));					
							Thread.sleep(10000);
							boolean blnIsChecked = webidentifychkbox.isSelected();
							if (blnIsChecked==false){
								webidentifychkbox.click();	// to click the checkbox
								Thread.sleep(10000);
								fn_waitForSpinner(driver);
							    // to identify back the checked check-box/radio button webelement.
								WebElement webselchkbox = driver.findElement(By.xpath(strXpath));					
								Boolean blnResult = webselchkbox.isSelected();
									if (blnResult==true)
										Log.debug("Component of the Configuration is selected successfully::"+OptionName); 	
									else
										Log.debug("Component of the Configuration is not selected "+OptionName);}
							else{
								Log.debug("Component of the Configuration is Automatically selected by the System OR it is required and selected Automatically ::"+OptionName);
							    }
							}
						    catch (Exception e)
					        {
					            Log.error("ERROR: Unable to click the element");
					            Log.error(e.getMessage());
					            e.printStackTrace();}	
			
					        Log.info("++++++++++++++++++++++++++++++++++++End of Configuration of Services+++++++++++++++++++++++++++++++++++++++++++");
					        Thread.sleep(10000);  			    
        					}
					    
        	        	 }
					    
        	        }  
        	
        	blnmultipleConfiguration = objConfig.fn_clickOfferingMgmntBtn(driver, webWait).isDisplayed();
		
        }
       catch (Exception e)
        {
            Log.error("ERROR: Unable to Add the Configuration");
            Log.error(e.getMessage());
            e.printStackTrace();

        }
        return blnmultipleConfiguration;
    }
	
	
    public static void fn_waitForSpinner(WebDriver driver)
    {
        By spinnerLocator = By.className("spinnerImg-tree");
        WebDriverWait wait = new WebDriverWait(driver, 120);
        try{
            Log.debug("Waiting for spinner to disappear..");
            wait.until(ExpectedConditions.invisibilityOfElementLocated(spinnerLocator));
            Log.debug("Finished Waiting..");
        }
        catch(TimeoutException e)
        {
        	Log.debug("Timed out waiting for spinner to disappear...");
        }
    }
}
