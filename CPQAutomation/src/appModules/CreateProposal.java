/*
 Author     		:	Suhas Thakre
 Class Name			: 	CreateUpgDwg 
 Purpose     		: 	Purpose of this file is :
						1. To Create Proposal

 Date       		:	21/02/2017 
 Modified Date		:		
 Modified By		: 	
 Version Information:	Version 1.0
 						
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/
package appModules;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.LeadCreation;
import pageObjects.SalesForce.Proposal;
import utility.EnvironmentDetails;
import utility.Log;
import utility.Utils;
import utility.WriteExcel;


public class CreateProposal  {
	
	public static String strNewproposalName=null;
	public static String strProposalNumber=null;
	
	
	/*
	Author: Suhas Thakre
	Function fn_CreateProposal : Create new Proposal from Opportunity.
	Inputs: strProposalData - Mandatory field required to create proposal.
	Outputs: Boolean response to check success or failure of function .
	 */
	public boolean fn_CreateProposal(WebDriver driver,WebDriverWait webWait,String strProposalData) throws Throwable
    {
		//TODO:: Select Product family  
		/************************************************************************************************/
			//Step 1: Create proposal. 
			//Step 2: Set mandatory fields for proposal.
			
		
		/************************************************************************************************/
		Proposal objProposal = new Proposal();
		LeadCreation ObjLead = new LeadCreation();
		Utils objUtils = new Utils();
		//OfferingData objOfferingData = new OfferingData();
		//CreateOfferingData objCreateOfferingData = new CreateOfferingData();
    	String strProposaldata[]=strProposalData.split("#");
    	String strContact = EnvironmentDetails.mapGlobalVariable.get("strContact").toString();
    	String strProposalType = "";
    	Map<String, Object> mapOutputData = new HashMap<String, Object>();
    	Boolean blnCreateProposal = false;
    	
        try
        {   
        	Log.debug("DEBUG: Execution of function 'fn_CreateProposal' Started.");
        	
        	//Step 1
        	try {
        		ObjLead.fn_clickQuoteORProposalBtn(driver, new WebDriverWait(driver,5)).click();
			} catch (Exception e) {
				driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@title='SGSFA_QuotingButtons']")));	
				ObjLead.fn_clickQuoteORProposalBtn(driver, new WebDriverWait(driver,5)).click();
				driver.switchTo().defaultContent();
			}
        	
        	objProposal.fn_setProposalName(driver, webWait).isDisplayed();
        	
    		Date date = new Date() ;
    		SimpleDateFormat dateFormat = new SimpleDateFormat("mmss"); 
    		
    		//Step 2
        	objProposal.fn_setProposalName(driver, webWait).clear();
        	//TODO:: generating the New proposal name with timestamp to make the name unique for further validations
        	strNewproposalName = strProposaldata[0]+dateFormat.format(date);
        	objProposal.fn_setProposalName(driver, webWait).sendKeys(strNewproposalName);
        	objProposal.fn_setPrimaryContact(driver, webWait).clear();
        	objProposal.fn_setPrimaryContact(driver, webWait).sendKeys(strContact);
        	
        	if (EnvironmentDetails.mapGlobalVariable.get("strCountry").toString()!="Ireland") {
        		//objProposal.fn_setPriceList(driver, webWait).sendKeys("USA Price List");
            	//objProposal.fn_setEntityName(driver, webWait).sendKeys("Sungard Availability Services, LP");
			}
        	
        	objProposal.fn_clickSaveButton(driver, webWait).click();
        	Thread.sleep(5000);
        	
        	
        	strProposalNumber = objProposal.fn_getProposalNumber(driver, webWait).getText();
        	Thread.sleep(5000);
        	
        	//Saved Proposal Name in global variable map
        	EnvironmentDetails.mapGlobalVariable.put("strNewproposalName", strProposalNumber);
        	Log.debug("DEBUG: Proposal Name save in global variable map with key 'strNewproposalName'.");
        	
        	
        	//TODO:: Capture Screenshot for Proposal 
        	objUtils.fn_takeScreenshotFullPage(driver);
        	
        	//TODO:: Write account name in output file 
        	mapOutputData.put("Proposal Name", strProposalNumber);
        	objUtils.fn_WriteExcelApttus("Proposal_Name"+EnvironmentDetails.mapGlobalVariable.get("struniquekey").toString(), mapOutputData);
        	
        	
        	//Saved Proposal Type in global variable map
			strProposalType = objProposal.fn_getProposalType(driver, webWait).getAttribute("innerText").trim();
			EnvironmentDetails.mapGlobalVariable.put("strProposalType", strProposalType);
			Log.debug("DEBUG: Proposal Type save in global variable map with key 'strProposalType'.");
        	
        	//Utils.takeScreenshot(driver, strTestCaseName);
        	
        	
        }
        catch (Exception ExceptionCreateProposal) {
			Log.error("ERROR: Unable to Create proposal from Opportunity with exception reported as: "+ExceptionCreateProposal.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionCreateProposal.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_CreateProposal method is: "+stack);
           
		}
        
        Log.debug("DEBUG: Execution of function 'fn_CreateProposal' Completed.");
        blnCreateProposal = true;
		return blnCreateProposal;
    }

}
