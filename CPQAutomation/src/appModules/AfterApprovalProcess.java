/*
 Author     		:	Suhas Thakre
 Class Name			: 	CreateUpgDwg 
 Purpose     		: 	Purpose of this file is :
						1. To perform  upgrade and downgrade existing order 

 Date       		:	26/12/2016 
 Modified Date		:		
 Modified By		: 	
 Version Information:	Version 1.0
 						
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/
package appModules;


import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.Account;
import pageObjects.SalesForce.AfterApproval;
import utility.API_SOAP;
import utility.EnvironmentDetails;
import utility.ExcelHandler;
import utility.Log;
import utility.Utils;
import utility.VerificationMethods;
import utility.WriteExcel;


public class AfterApprovalProcess extends VerificationMethods{
	
	/*
	Author: Suhas Thakre
	Function fn_AfterApprovalProcess: Create Active Order.
	Inputs: strCloseOppDetials - data inputs required to close opportunity.
	Outputs: Boolean response to check success or failure of function .
	 */
	public Boolean fn_OrderFormActivation(WebDriver driver,WebDriverWait webWait, String strCloseOppDetials, String strActivationDate) throws Throwable{
		
		//TODO:: Create Active Order.  
		/************************************************************************************************/
			//Step 1: Open Order agreement page. 
			//Step 2: Set Order agreement activation date.
			//Step 3: Check e signature status and close opportunity.
			//Step 4: Activate Order agreement.
			//Step 5: Activate Order.  

		/************************************************************************************************/
			
			AfterApproval objAfterPO = new AfterApproval();
			ExcelHandler objExcel = new ExcelHandler();
			//Account objAcctPage = new Account();
			WriteExcel objWriteExcel = new WriteExcel();
			Utils objUtils = new Utils();
			Account objacctpage = new Account();
			Boolean blnAfterApprovalprocess = false;
			String strAgreementNum= EnvironmentDetails.mapGlobalVariable.get("strAgrmntNum").toString();
			String strAgrmntName= EnvironmentDetails.mapGlobalVariable.get("strAgrmntName").toString();
			String strNewproposalName = EnvironmentDetails.mapGlobalVariable.get("strNewproposalName").toString();
			String strCountry = EnvironmentDetails.mapGlobalVariable.get("strCountry").toString();
			List<Map<String, String>> lstProductMap = new ArrayList<Map<String,String>>();
			String strStatus="";
			//String strOrderStatus="";
			String strOrderNumber="";
			Map<String, Object> mapOutputData = new HashMap<String, Object>();
			List<Map<String, Object>> lstOutputData = new ArrayList<>();
			List<List<Map<String, Object>>> lstmapOutputData = new ArrayList<>();
			String[] arrstrCloseOppDetails = strCloseOppDetials.split("#");
			//Boolean blnIsAlertPresent=false;
			String strCreateOfferingWBName = "TestData.xlsx";
			String strCreateOfferingDataSheetName = "CreateOfferingData";
			String strProductFamilyData  = "";
			Boolean blnActivateAWSOrder=false;

			
			//change date format as per country 
			SimpleDateFormat USformat = new SimpleDateFormat("MM/dd/yyyy");
			SimpleDateFormat NonUSformat = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat Inputformat = new SimpleDateFormat("dd/MMM/yyyy");
			Date date = Inputformat.parse(strActivationDate);
			if (EnvironmentDetails.mapGlobalVariable.get("strCountry").toString().equalsIgnoreCase("United States")) {
				strActivationDate = USformat.format(date);
			} else {
				strActivationDate = NonUSformat.format(date);
			}
			
			//Step 1
			objacctpage.fn_setSearchField(driver, webWait).sendKeys(strNewproposalName); 
			objacctpage.fn_clickSearchBtn(driver, webWait).click();
			Thread.sleep(1000);
			driver.findElement(By.partialLinkText(strNewproposalName)).click();
			Thread.sleep(3500);
			driver.findElement(By.linkText(strAgrmntName)).click();
			
			
			
			try
			{
			
			Log.debug("DEBUG: Execution of function 'fn_AfterApprovalProcess' Started.");
			
			//Step 2
			//String strParentWindow = driver.getWindowHandle();		
			Actions action = new Actions(driver).doubleClick(objAfterPO.fn_clickActivatedDate(driver, webWait));
			action.build().perform();
			
			//Handle multiple windows
			/*for (String handle : driver.getWindowHandles()) {
	               driver.switchTo().window(handle);}		
			Thread.sleep(5000);
			objAfterPO.fn_clickCalendarTodayLink(driver, webWait).click();	   --   
		    driver.switchTo().window(strParentWindow);
			Thread.sleep(5000);	
			*/
			objAfterPO.fn_setActivationDate(driver, webWait).isDisplayed();	
			objAfterPO.fn_setActivationDate(driver, webWait).clear();
			objAfterPO.fn_setActivationDate(driver, webWait).sendKeys(strActivationDate); 
			objAfterPO.fn_clickSave(driver, webWait).isEnabled();
			objAfterPO.fn_clickSave(driver, webWait).click();
			//Thread.sleep(15000);
			fn_Pagerefresh(driver, webWait, 1);
			
			
				try {
					//Step 3
					objAfterPO.fn_clickEsignatureStatus(driver, new WebDriverWait(driver,5)).click();
					Thread.sleep(15000);
					
				} catch (Exception e) {
					Log.debug("DEBUG: esignature not required.");
				}
				
			fn_Pagerefresh(driver, webWait, 1);	
			objAfterPO.fn_getAgrmntNumber(driver, webWait).isDisplayed();
			strAgreementNum = objAfterPO.fn_getAgrmntNumber(driver, webWait).getText();
			strAgrmntName = objAfterPO.fn_getAgrmntName(driver, webWait).getText();
			Log.info("The Agreement Number is :: "+strAgreementNum);
			objAfterPO.fn_clickActivate(driver, webWait).click();
			//Thread.sleep(5000);
						
			//Close the corresponding Opportunity
			objAfterPO.fn_clickGoToOpptyBtn(driver, webWait).isEnabled();
			objAfterPO.fn_clickGoToOpptyBtn(driver, webWait).click();
			objAfterPO.fn_clickEditBtn(driver, webWait).isEnabled();
			objAfterPO.fn_clickEditBtn(driver, webWait).click();
			Thread.sleep(1000);
			objAfterPO.fn_selStage(driver, webWait).selectByVisibleText(arrstrCloseOppDetails[0]);
			Thread.sleep(1000);
			objAfterPO.fn_selOppType(driver, webWait).selectByIndex(0);
			Thread.sleep(1000);
			objAfterPO.fn_selWinReason(driver, webWait).selectByIndex(1);
			
			//Code to Add Multiple Options for Competitors
			String strOptions="";
			List<WebElement> lstwebAllOptionsAdd  = objAfterPO.fn_selectMultipleOnCompetitors(driver, webWait).getOptions(); 
			for(WebElement webOptions : lstwebAllOptionsAdd ){
				 strOptions = webOptions.getText();
				
				if (strOptions.contains(arrstrCloseOppDetails[2]))
				      webOptions.click();}
			objAfterPO.fn_ClickImgAddOnAddressEditPage(driver, webWait).click();
			objAfterPO.fn_clickOpptySaveBtn(driver, webWait).click();
			Thread.sleep(1000);
			
			//Query the agreement  to refresh the status of agreement
			objacctpage.fn_setSearchField(driver, webWait).sendKeys(strNewproposalName); 
			objacctpage.fn_clickSearchBtn(driver, webWait).click();
			Thread.sleep(1000);
			driver.findElement(By.partialLinkText(strNewproposalName)).click();
			Thread.sleep(3500);
			webWait.until(ExpectedConditions.elementToBeClickable(By.linkText(strAgrmntName))).isDisplayed();
			driver.findElement(By.linkText(strAgrmntName)).click();
			
			//Step 4
			objAfterPO.fn_clickActivate(driver, webWait).click();
			objAfterPO.fn_clickNextButton(driver, webWait).isEnabled();
			Thread.sleep(3500);
			//Selecting the all Required document 
			List<WebElement> lstwebAllOptions  = objAfterPO.fn_selDocument(driver, webWait).getOptions(); 
			for(WebElement webOptions : lstwebAllOptions ){
				webOptions.click();}
			objAfterPO.fn_clickNextButton(driver, webWait).click();
			Thread.sleep(5000);
			objAfterPO.fn_clickActivateOrderForm(driver, webWait).click();	
			

				//Accept alert if present 
				try{  
					new WebDriverWait(driver,20).until(ExpectedConditions.alertIsPresent());
				    Alert alert = driver.switchTo().alert();
				    Log.info("the message thrown from alert is :: "+alert.getText());
				    alert.accept();
				    //blnIsAlertPresent=true;
			    	  }catch(Exception e){ 
			    		  Log.info("unexpected alert is not not present");   
			    	  }
			
			//Getting the Final activated status of the Order
			strStatus=objAfterPO.fn_getStatus(driver, webWait).getText();
			if 	(strStatus.contains("Activated")){
				Log.debug("DEBUG: The Order is activated.");
			    blnAfterApprovalprocess = true;}
			else
				 Log.debug("DEBUG: Unable to validate the pricing.");
			
			
			
			while (! objAfterPO.fn_getOrderCount(driver, webWait).getText().equalsIgnoreCase("[1]")) {
				driver.navigate().refresh();
				Thread.sleep(5000);
			}
			
			
			//Open Agreement page
			objacctpage.fn_setSearchField(driver, webWait).sendKeys(strNewproposalName); 
			objacctpage.fn_clickSearchBtn(driver, webWait).click();
			driver.findElement(By.partialLinkText(strNewproposalName)).isEnabled();
			driver.findElement(By.partialLinkText(strNewproposalName)).click();
			Thread.sleep(3500);
			webWait.until(ExpectedConditions.elementToBeClickable(By.linkText(strAgrmntName))).isEnabled();
			driver.findElement(By.linkText(strAgrmntName)).click();
			
			objAfterPO.fn_getOrderNumber(driver, webWait).isDisplayed();
			strOrderNumber=objAfterPO.fn_getOrderNumber(driver, webWait).getText();
			if (strOrderNumber.isEmpty())
				Log.debug("DEBUG: The order number '"+strOrderNumber+"' is not generated.");
			else
				Log.info("The order number successfully generated is :: "+strOrderNumber);	
			
			
			//TODO:: Capture Screenshot for Agreement after activation 
			objUtils.fn_takeScreenshotFullPage(driver);
			
			//write data in output file 
			mapOutputData.put("Order Number", strOrderNumber);
			lstOutputData.add(mapOutputData);
			lstmapOutputData.add(lstOutputData);
			objWriteExcel.fn_WriteExcel(lstmapOutputData, EnvironmentDetails.strFQNFileNameWrite, "Order_Details");
			
			
			//Read the product family to decide order activation process 
			String excelResource = EnvironmentDetails.Path_ExternalFiles+ System.getProperty("file.separator") + strCreateOfferingWBName;
			File fileCreateOffering = new File(excelResource);
			lstProductMap = objExcel.fn_ReadExcelAsMap(fileCreateOffering,strCreateOfferingDataSheetName); 
			//Search Product Family in all row of Add_Products_Data sheet
			for (Map<String, String> mapProduct : lstProductMap) {
				if (mapProduct.get("Uniquekey").toString().trim().equalsIgnoreCase(EnvironmentDetails.mapGlobalVariable.get("struniquekey").toString())) {
					strProductFamilyData = mapProduct.get("Product_Family").toString().trim();
					if (strProductFamilyData.equalsIgnoreCase("GLB - Managed Cloud - AWS")) {
						blnActivateAWSOrder = true;
						break;
					}
				}
			}
			
			
			//Step 5: Activate Order.
			if (blnActivateAWSOrder) {
				fn_ActivateOrderAWS(driver, webWait);
			} else {
				fn_ActivateOrder(driver, webWait, strOrderNumber, strActivationDate);
			}
			
			
	
		
			
			
			}
			catch (Exception ExceptionAfterApproval) {
				Log.error("ERROR: Unable to perform After Approval Process with exception reported as: "+ExceptionAfterApproval.getMessage());
	            StringWriter stack = new StringWriter();
	            ExceptionAfterApproval.printStackTrace(new PrintWriter(stack));
	            Log.debug("DEBUG: The exception in fn_AfterApprovalProcess method is: "+stack);
	           
			}
			
			Log.debug("DEBUG: Execution of function 'fn_AfterApprovalProcess' Completed.");
			return blnAfterApprovalprocess;
	}
	
	public void fn_Pagerefresh(WebDriver driver,WebDriverWait webWait, Integer intRefreshCount) throws Throwable{
		for (int intRefresh=0; intRefresh<=intRefreshCount;intRefresh++ ){
			driver.navigate().refresh();
			Thread.sleep(1000);
		}
	}
	
	/*
	Author: Suhas Thakre
	Function fn_ActivateOrder: Activate Order.
	Inputs: strOrderNumber - Order tobe activated.
	Outputs: 
	 */
	public void fn_ActivateOrder(WebDriver driver,WebDriverWait webWait, String strOrderNumber, String strActivationDate) throws Throwable{
		
		//TODO:: Create Active Order.  
		/************************************************************************************************/
			//Step 1: Logout from existing user. 
			//Step 2: Login With legal Sales User.
			//Step 3: Open Order.
			//Step 4: Activate Order agreement.
			//Step 5: Activate Order.

		/************************************************************************************************/
		Account objAcctPage = new Account();
		AfterApproval objAfterPO = new AfterApproval();
		Utils objUtils = new Utils();
		try {
			
			/*//Step 1: Logout from existing user.
			ApptusLoginLogout.appLogout(driver, webWait);
			//Step 2: Login With legal Sales User.
			ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("InstUserName"), EnvironmentDetails.mapEnvironmentDetails.get("InstPassWord"));
			//Step 3: Open Order.
*/			objAcctPage.fn_setSearchField(driver, webWait).sendKeys(strOrderNumber); 
			objAcctPage.fn_clickSearchBtn(driver, webWait).click();
			webWait.until(ExpectedConditions.elementToBeClickable(driver.findElement(By.partialLinkText(strOrderNumber)))).isEnabled();
			driver.findElement(By.partialLinkText(strOrderNumber)).click();
			objAfterPO.fn_clickReadyForActivationDate(driver, webWait).isDisplayed();
			
			//Double click the activate by date to enter the date
			//String strParentWindow1 = driver.getWindowHandle();		
			Actions action1 = new Actions(driver).doubleClick(objAfterPO.fn_clickReadyForActivationDate(driver, webWait));
			action1.build().perform();
			objAfterPO.fn_setReadyForActivationDate(driver, webWait).sendKeys(strActivationDate+" 3:06");
			Thread.sleep(3500);
			objAfterPO.fn_clickOrderDetailSave(driver, webWait).click();
			Thread.sleep(3500);
			fn_Pagerefresh(driver, webWait, 1);
			
			//TODO:: Capture Screenshot for Order after activation 
			objUtils.fn_takeScreenshotFullPage(driver);
			
			assertEquals(objAfterPO.fn_getOrderStatus(driver, webWait).getText().trim(), "Activated");
			
			
		} catch (Exception ExceptionActivateOrder) {
			Log.error("ERROR: Unable to Activate Order with exception reported as: "+ExceptionActivateOrder.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionActivateOrder.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_ActivateOrder method is: "+stack);
           
		}
		
		
	}
	
	
	/*
	Author: Suhas Thakre
	Function fn_ActivateOrder: Activate Order.
	Inputs: strOrderNumber - Order tobe activated.
	Outputs: 
	 */
	public Boolean fn_ActivateOrderAWS(WebDriver driver,WebDriverWait webWait) throws Throwable{
		
		//TODO:: Create Active Order.  
		/************************************************************************************************/
			//Step 1: Logout from existing user.
			//Step 2: Read offering ID query.
			//Step 3: Get Offerings ID.
			//Step 4: Navigate to Operations/Implementation Form .
			//Step 5: Login With Operations User and add AWS ID.
			//Step 6: Read Usage mapping data and set AWS id for each line items.
			//Step 7: Send Order To PSO.
			//Step 8: Login with PSO and assign order to PM.
			//Step 9: Login with PM and activate order.
		
			/*EnvironmentDetails.mapGlobalVariable.put("strAcctName", "US0523_1_4634");
			EnvironmentDetails.mapGlobalVariable.put("struniquekey", "US0523_1");*/
		/************************************************************************************************/
		//Account objAcctPage = new Account();
		AfterApproval objAfterPO = new AfterApproval();
		ExcelHandler objExcel = new ExcelHandler();
		//Utils objUtils = new Utils();
		API_SOAP objAPI_SOAP = new API_SOAP();
		Map<String, Object> mapTestCases = new HashMap<String, Object>();
		EnvironmentDetails objEnv = (EnvironmentDetails) EnvironmentDetails.mapGlobalVariable.get("objEnv");
		String strApttusQuery = "";
		String strApttusUpdQuery = "";
		String strActivationDate = "";
		String strAcctName = EnvironmentDetails.mapGlobalVariable.get("strAcctName").toString();
		String strOfferingsID = "";
		String strUsageMappingWB = "TestData.xlsx";
		String strUsageMappingSheet = "Usage_Mapping";
		List<Map<String, String>> lstmapUsageMappingData = new ArrayList<Map<String,String>>();
		String strLineItemName = "";
		String strAWSID = "";
		Boolean blnActivateOrderAWS = false;
		Boolean blnTriggerActivateOrderAWS = false;
		try {
			
			//Step 1: Logout from existing user.
			ApptusLoginLogout.appLogout(driver, webWait);
			
			//Step 2: Read offering ID query.
			strApttusQuery = objExcel.fn_ReadApttusQuery("ApttusQuery", "ApttusQuerySheet", "Offerings ID");
			strApttusUpdQuery = strApttusQuery.replace("AccountNameInput", strAcctName);
			
			//Step 3: Get Offerings ID ID.
			mapTestCases.put("CallType", "query");
			mapTestCases.put("CallingData", strApttusUpdQuery);
			List<List<Map<String, String>>> lstmapAppReqId = objAPI_SOAP.fn_SOAP_GET(objEnv, mapTestCases, null);
			for (List<Map<String, String>> listmap : lstmapAppReqId) {
				 for (Map<String, String> map : listmap) {
					 strOfferingsID =  map.get("Id").toString();
				}
			}
			
			//Step 4: Navigate to Operations/Implementation Form .
			driver.navigate().to("https://sungardas--ebintb--c.cs44.visual.force.com/apex/Apttus_SG_Ops_AssetRelations?id="+strOfferingsID);
			
			//Step 5: Login With Operations User and add AWS ID.
			ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("OperationsUserName"), EnvironmentDetails.mapEnvironmentDetails.get("OperationsPassWord"));
			objAfterPO.fn_clickGotoAcct(driver, webWait).isEnabled();
			
			
			
			//Step 6: Read Usage mapping data and set AWS id for each line items.
			String excelResource = EnvironmentDetails.Path_ExternalFiles
					+ System.getProperty("file.separator") + strUsageMappingWB;
			File fileLeadCreationData = new File(excelResource);
			lstmapUsageMappingData = objExcel.fn_ReadExcelAsMap(fileLeadCreationData,strUsageMappingSheet); 
			for (Map<String, String> mapUsageMappingData : lstmapUsageMappingData) {
				//Select data depends on unique key 
				if (mapUsageMappingData.get("Uniquekey").toString().trim().equalsIgnoreCase(EnvironmentDetails.mapGlobalVariable.get("struniquekey").toString())) {
					blnTriggerActivateOrderAWS = true;
					strLineItemName = mapUsageMappingData.get("LineItem_Name").toString().trim();
					strAWSID = mapUsageMappingData.get("AWS_ID").toString().trim();
					strActivationDate = mapUsageMappingData.get("Activation_Date").toString().trim();
					
					//change date format as per country 
					SimpleDateFormat USformat = new SimpleDateFormat("MM/dd/yyyy");
					SimpleDateFormat NonUSformat = new SimpleDateFormat("MM/dd/yyyy");
					SimpleDateFormat Inputformat = new SimpleDateFormat("dd/MMM/yyyy");
					Date date = Inputformat.parse(strActivationDate);
					if (EnvironmentDetails.mapGlobalVariable.get("strCountry").toString().equalsIgnoreCase("United States")) {
						strActivationDate = USformat.format(date);
					} else {
						strActivationDate = NonUSformat.format(date);
					}
						//Add Usage for new order or update date for change order
						try {
							//Add AWS ID for each line items
							objAfterPO.fn_clickAddbtn(driver, new WebDriverWait(driver,15), strLineItemName).isEnabled();
							objAfterPO.fn_clickAddbtn(driver, webWait, strLineItemName).click();
							objAfterPO.fn_clickSaveEntrybtn(driver, webWait).isEnabled();
							objAfterPO.fn_setProductExternalId(driver, webWait).clear();
							objAfterPO.fn_setProductExternalId(driver, webWait).sendKeys(strAWSID);
							objAfterPO.fn_setProductInstallDate(driver, webWait).clear();
							objAfterPO.fn_setProductInstallDate(driver, webWait).sendKeys(strActivationDate);
							objAfterPO.fn_clickSaveEntrybtn(driver, webWait).click();
							objAfterPO.fn_getRecordsSavedtext(driver, webWait).isDisplayed();
						} catch (Exception e) {
							objAfterPO.fn_clickEditbtn(driver, webWait, strLineItemName).isEnabled();
							objAfterPO.fn_clickEditbtn(driver, webWait, strLineItemName).click();
							objAfterPO.fn_clickSaveEditsbtn(driver, webWait, strAWSID).isEnabled();
							objAfterPO.fn_setInstallDate(driver, webWait, strLineItemName, strAWSID).clear();
							objAfterPO.fn_setInstallDate(driver, webWait, strLineItemName, strAWSID).sendKeys(strActivationDate);
							objAfterPO.fn_setExternalId(driver, webWait, strLineItemName).click();
							objAfterPO.fn_clickSaveEditsbtn(driver, webWait, strAWSID).click();
							objAfterPO.fn_getRecordsSavedtext(driver, webWait).isDisplayed();
						}
				}
			}
			
			
			//Trigger activation process only when usage details are available 
			if (blnTriggerActivateOrderAWS) {
				
				//Step 7: Send Order To PSO.
				objAfterPO.fn_clickSendtoPSObtn(driver, webWait).isEnabled();
				objAfterPO.fn_clickSendtoPSObtn(driver, webWait).click();
				objAfterPO.fn_getSendtoPSOtext(driver, webWait).isDisplayed();
				
				
				//Step 8: Login with PSO and assign order to PM.
				ApptusLoginLogout.appLogout(driver, webWait);
				//Login With PSO User.
				ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("PSOUserName"), EnvironmentDetails.mapEnvironmentDetails.get("PSOPassWord"));
				Thread.sleep(3000);
				//Navigate to PSO Form .
				driver.navigate().to("https://sungardas--ebintb--c.cs44.visual.force.com/apex/Apttus_SG_Ops_AssetRelations?id="+strOfferingsID);
				objAfterPO.fn_clickSendtoPMbtn(driver, webWait).isEnabled();
				//set SNT Code and select PM
				objAfterPO.fn_setSNTCode(driver, webWait).sendKeys("123456789012");
				objAfterPO.fn_clickSavebtn(driver, webWait).click();
				objAfterPO.fn_selAssigntoPM(driver, webWait).selectByVisibleText("PMUSER1, PM1");
				objAfterPO.fn_clickSendtoPMbtn(driver, webWait).click();
				objAfterPO.fn_getSendtoPMtext(driver, webWait).isDisplayed();
				
				//Step 9: Login with PM and activate order.
				ApptusLoginLogout.appLogout(driver, webWait);
				//Login With PM User.
				ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("PMUserName").toString(), EnvironmentDetails.mapEnvironmentDetails.get("PMPassWord").toString());
				Thread.sleep(3000);
				//Navigate to PM Form .
				driver.navigate().to("https://sungardas--ebintb--c.cs44.visual.force.com/apex/Apttus_SG_Ops_AssetRelations?id="+strOfferingsID);
				objAfterPO.fn_clickActivatebtn(driver, webWait).isEnabled();
				//objAfterPO.fn_clickSavebtn(driver, webWait).isEnabled();
				//set GUID 
				objAfterPO.fn_setGUID(driver, webWait).sendKeys("123456789021");
				objAfterPO.fn_clickSavebtn(driver, webWait).click();
				objAfterPO.fn_clickActivatebtn(driver, webWait).isEnabled();
				objAfterPO.fn_clickActivatebtn(driver, webWait).click();
				objAfterPO.fn_getActivatedtext(driver, webWait).isDisplayed();
			}
			
				
			
			//assertEquals(objAfterPO.fn_getOrderStatus(driver, webWait).getText().trim(), "Activated");
			blnActivateOrderAWS = true;
			
		} catch (Exception ExceptionActivateOrder) {
			Log.error("ERROR: Unable to Activate Order with exception reported as: "+ExceptionActivateOrder.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionActivateOrder.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_ActivateOrder method is: "+stack);
           
		}
		return blnActivateOrderAWS;
		
		
	}
	
	
	
	
	
	
}
