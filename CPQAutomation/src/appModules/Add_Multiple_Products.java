/* 
==============================================================
 Author     		:	Madhavi JN
 Class Name		 	: 	Add_Multiple_Products
 Purpose     		: 	Adding the Multiple Products
 Date       		:	13/02/2017
 Version Information:	Version1.0
 PreCondition 		:	None
 Test Steps 		:	Created proposal and then click on Configure Products button
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
 #======================================================================
 */

package appModules;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import utility.API_SOAP;
import utility.EnvironmentDetails;
import utility.ExcelHandler;
import utility.Log;
import utility.Utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.ConfigurationPageObjects;
import pageObjects.SalesForce.Configuration_NA;



public class Add_Multiple_Products {
	
	public Boolean fn_Add_Multiple_Products(WebDriver driver,WebDriverWait webWait,Map<String, Object> mapTCData) throws Exception
	{
		//Purpose:- For adding Multiple products
		//Inputs:- mapTCData which contains the data driven through external sheet
		//============================================================================
	    //Step1:- To Fetch Data for Product Information from test data sheet
		//Step2:- Read query for Fetching_ProductCode
		//Step3:- Fire the Query for Fetching_ProductCode and Get the Response from Soap call
		//Step4:- Read query for Fetching_PriceList
		//Step5:- Fire the Query for Fetching_PriceList and get the response for the SOAP's GET call.
		//Step6:- Read query for Fetching_LineItems_of_Product
		//Step7:- Fire the Query for Fetching_LineItems_of_Product and get the response for the SOAP's GET call.
		//Step8:- Validation of the condition of product having Constraint rules.(No proceed with configuration,
		//(Yes proceed with validation of constraint rules)
		//Step9:- Validate whether Lineitem has CRule and proceed with validation of rules
		//Step10:- Click on AddMoreProducts Button
		//============================================================================
		
		String strFileProductCode=null;
		String strFilePriceListCode=null;
		String strProductName=null;
		Boolean blnAdd_Multiple_Products=false;
		Boolean blnMultiple_products=false;
		Boolean blnIsSearched=false;
		String strLineItems_Manual="";
		String strApttusQuery="";
		String strToBeConfigured="";
		String strValidate_Const_Rules="";

		MSOutlookApproval objMSOutlookApproval = new MSOutlookApproval();
		API_SOAP objSOAP = new API_SOAP();
		ExcelHandler objExcel = new ExcelHandler();
		Utils objUtils=new Utils();
    	EnvironmentDetails objEnvironment = new EnvironmentDetails();
		ConfigurationPageObjects objConfig = new ConfigurationPageObjects();
		Add_Config_for_MultiProducts objconfigPrdcts = new Add_Config_for_MultiProducts();
		//Add_Config_for_Mul_ConstRules objCRules = new Add_Config_for_Mul_ConstRules();
		Add_Config_for_MultiProducts objConfigMultiProducts = new Add_Config_for_MultiProducts();
		
		try{

			File fileConfigValidations = new File(EnvironmentDetails.Path_DataSheet);			
			List<Map<String,String>> lstMainProductMap = new ArrayList<Map<String,String>>();		
			List<Map<String,String>> lstCopyProductMap = new ArrayList<Map<String,String>>();		
			List<Map<String,String>> lstEachProductMap = new ArrayList<Map<String,String>>();		
			List<Map<String,String>> lstUpdateCopyProductMap = new ArrayList<Map<String,String>>();
			List<List<Map<String, String>>> lstmapResponse = new ArrayList<List<Map<String,String>>>();
			Map<String, Object> mapTestCases = new HashMap<String, Object>();
			Configuration_NA objConfigNA = new Configuration_NA();
			
			List<Map<String,String>> lstMainProducOldMap = new ArrayList<Map<String,String>>();
			
			// execution in progress for non angular js application 
			
			//block to handle Configure button and  Add New Product
        	try {
        		//Click on Configure button
            	objConfig.fn_clickConfigureBtn(driver, webWait).click();
            	//objConfig.fn_clickAddMorePrdctsBtn(driver, webWait).click();
			} catch (Exception e) {
				Log.debug("DEBUG: Configure button not present cont with Search Product");
			}
        	
        	//block to handle   Add New Product
        	try {
					objConfigNA.fn_clickAddMorePrdctsBtn(driver, new WebDriverWait(driver,15)).isEnabled();
					objConfigNA.fn_clickAddMorePrdctsBtn(driver, new WebDriverWait(driver,15)).click();
					Utils.fn_waitForProgressBar(driver);
				} catch (Exception e2) {
					Log.debug("DEBUG: Add New Product button not present cont with Search Product");
				}
			
        	/*//block to identify angular js  application 
        	try {
        		objConfigNA.fn_clickGotoPricingNABtn(driver, new WebDriverWait(driver,60)).isEnabled();
        		blnAngularJS = true;
			} catch (Exception e) {
				Log.debug("DEBUG: Execution inprogress for non-angular js application ");
			}*/
			
			//Step1
			lstMainProducOldMap = objExcel.fn_ReadExcelAsMap(fileConfigValidations,"Product_Information");
			
			for (Map<String, String> mapMainProducOld : lstMainProducOldMap) {
				//Select data depends on unique key 
				if (mapMainProducOld.get("Uniquekey").toString().trim().equalsIgnoreCase(EnvironmentDetails.mapGlobalVariable.get("struniquekey").toString())) {
					lstMainProductMap.add(mapMainProducOld);
				}
			}
			
			
			lstCopyProductMap.addAll(lstMainProductMap);		
			lstUpdateCopyProductMap.addAll(lstMainProductMap);		
			HashSet<String> hashsetProductName = new HashSet<String>();
			
			for (Map<String, String> mapTemp : lstMainProductMap) {								
				String strMainProdName = mapTemp.get("Product_Name");		
				if(!hashsetProductName.contains(strMainProdName)){			
					Boolean blnMap2Generated = false;					
					lstEachProductMap = new ArrayList<Map<String,String>>();			
					for (Map<String, String> mapCopy : lstCopyProductMap){													
						if (mapCopy.containsValue(strMainProdName)){					
							lstEachProductMap.add(mapCopy);
							blnMap2Generated = true;
							lstUpdateCopyProductMap.remove(mapCopy);
						 }						
					 }
						
				lstCopyProductMap = new ArrayList<Map<String,String>>();				
				lstCopyProductMap.addAll(lstUpdateCopyProductMap);
									
				if(blnMap2Generated == true) {			
					Boolean blnPrdctConfigured = false;					
					for (Map<String, String> mapProduct : lstEachProductMap){				
						if (!blnPrdctConfigured == true) {		       	
						 //Step2 
						strProductName = mapProduct.get("Product_Name").toString().trim();					
						strApttusQuery = objExcel.fn_ReadApttusQuery("ApttusQuery", "ApttusQuerySheet", "Fetching_ProductCode");						
						Log.debug("DEBUG: Query 'To Fetch Product Code for Product' Before concatenating is: "+strApttusQuery);					
						strApttusQuery = strApttusQuery.replace("'strProductName'","'"+strProductName+"'");														 
						Log.debug("DEBUG: Query 'To Fetch Product Code for Product' after concatenating is "+strApttusQuery);
						
						//Step3 
						mapTestCases.put("CallType", "query");
						mapTestCases.put("CallingData", strApttusQuery); 
						List<List<Map<String, String>>> lstmapProductCode = objSOAP.fn_SOAP_GET(objEnvironment, mapTestCases, null); 
						//Handle Object not supported issue in Dev
						if (lstmapProductCode.toString().contains("INVALID_TYPE: sObject")) {
							lstmapProductCode = objMSOutlookApproval.fn_WorkbenchSOQL(driver, webWait, strApttusQuery);
						}
						strFileProductCode=objconfigPrdcts.fn_fetchResponse(lstmapProductCode);
						
						//Step4 
						strApttusQuery = objExcel.fn_ReadApttusQuery("ApttusQuery", "ApttusQuerySheet", "Fetching_PriceList");						
						Log.debug("DEBUG: Query 'To Fetch PriceList Code for Product' Before concatenating is "+strApttusQuery);	
						String strInputPriceList = "";
						//slect price list as per country
						if (EnvironmentDetails.mapGlobalVariable.get("strCountry").toString().equalsIgnoreCase("Ireland")) {
							strInputPriceList = "Ireland Price List";
						}
						else if (EnvironmentDetails.mapGlobalVariable.get("strCountry").toString().equalsIgnoreCase("Canada")) {
							strInputPriceList = "Canada Price List";
						}
						else if (EnvironmentDetails.mapGlobalVariable.get("strCountry").toString().equalsIgnoreCase("United Kingdom")) {
							strInputPriceList = "UK Price List";
						}
						else if (EnvironmentDetails.mapGlobalVariable.get("strCountry").toString().equalsIgnoreCase("Luxembourg")) {
							strInputPriceList = "Luxembourg Price List";
						}
						else {
							strInputPriceList = "USA Price List";
						}
						strApttusQuery = strApttusQuery.replace("'strProductName'","'"+strProductName+"'");
						strApttusQuery = strApttusQuery.replace("'strInputPriceList'","'"+strInputPriceList+"'");
						Log.debug("DEBUG: Query 'To Fetch PriceList Code for Product' after concatenating is "+strApttusQuery);						
						
						//Step5 
						mapTestCases.put("CallType", "query");
						mapTestCases.put("CallingData", strApttusQuery); 
						List<List<Map<String, String>>> lstmapPriceListCode = objSOAP.fn_SOAP_GET(objEnvironment, mapTestCases, null);		
						//Handle Object not supported issue in Dev
						if (lstmapPriceListCode.toString().contains("INVALID_TYPE: sObject")) {
							lstmapPriceListCode = objMSOutlookApproval.fn_WorkbenchSOQL(driver, webWait, strApttusQuery);
						}
						strFilePriceListCode=objconfigPrdcts.fn_fetchResponse(lstmapPriceListCode);							
						strLineItems_Manual = mapProduct.get("LineItem_Name").toString().trim();						
						strToBeConfigured = mapProduct.get("To_Be_configured").toString().trim();						
						strValidate_Const_Rules = mapProduct.get("Validate_Const_Rules").toString().trim();
						
						//Step6 	
						strApttusQuery = objExcel.fn_ReadApttusQuery("ApttusQuery", "ApttusQuerySheet", "Fetching_LineItems_of_Product");										
						Log.debug("DEBUG: Query 'To Fetch Linetems for Product' Before concatenating is "+strApttusQuery);						
						strApttusQuery = strApttusQuery.replace("'strProductCode'","'"+strFileProductCode+"'");						
						strApttusQuery = strApttusQuery.replace("'strPriceListCode'","'"+strFilePriceListCode+"'");									 
						Log.debug("DEBUG: Query 'To Fetch Lineitems for Product' after concatenating is "+strApttusQuery);
						
						//Step7 
						mapTestCases.put("CallType", "query");
						mapTestCases.put("CallingData", strApttusQuery); 
						lstmapResponse = objSOAP.fn_SOAP_GET(objEnvironment, mapTestCases, null);
						//Handle Object not supported issue in Dev
						if (lstmapResponse.toString().contains("INVALID_TYPE: sObject")) {
							lstmapResponse = objMSOutlookApproval.fn_WorkbenchSOQL(driver, webWait, strApttusQuery);
						}
                        
						
                        //TODO:: Function for Searching the products before to add
                		objConfigNA.fn_SearchProducts(driver, new WebDriverWait(driver,45)).isDisplayed();
						blnIsSearched=objConfigMultiProducts.fn_SearchProductsNA(driver,webWait,strProductName);
				    	if (blnIsSearched==true)
				    		Log.debug("DEBUG: Searched the Product Successfully ::"+strProductName);
				        else
					    	Log.debug("Error: problem occoured while Searching the Product ::"+strProductName); 	 
						
						
					}
					
					//Configure product on angular and non-angular js page.
					//Step8 
					objConfigNA.fn_clickProductOptions(driver, new WebDriverWait(driver,2)).isDisplayed();
					if (strToBeConfigured.equalsIgnoreCase("Yes") && strValidate_Const_Rules.equalsIgnoreCase("No")){							
						blnMultiple_products = objconfigPrdcts.fn_Config_for_MultiProductsNA(driver, webWait,strProductName,lstmapResponse,strLineItems_Manual,mapProduct);							
						if (blnMultiple_products==true)
							Log.info("Product '"+strProductName+"' is successfully configured !");
						else
						Log.error("ERROR: Unable to configure the product: " +strProductName);
					}
					blnPrdctConfigured = true;						
				}
			}
				
				//Take product config screenshot 
				objUtils.fn_takeScreenshotFullPage(driver);
				
				hashsetProductName.add(strMainProdName);
				Thread.sleep(2000);	
					//Step10
					 if (!(lstUpdateCopyProductMap.isEmpty())){	
						//Add More product on angular and non-angular js page.
						 objConfigNA.fn_clickAddMorePrdctsBtn(driver, new WebDriverWait(driver,15)).isEnabled();
						 Utils.fn_scrollToElement(driver, objConfigNA.fn_clickAddMorePrdctsBtn(driver, webWait));
						 objConfigNA.fn_clickAddMorePrdctsBtn(driver, webWait).click();
						 Utils.fn_waitForProgressBar(driver);	
					}
		}
		
	 }
	    //Click on Pricing Button for angular and non-angular js page.
		//objConfigNA.fn_clickGoToPricing(driver, new WebDriverWait(driver,15)).isEnabled();
		objConfigNA.fn_clickGotoPricingNABtn(driver, webWait).isEnabled();
	 	Utils.fn_waitForProgressBar(driver);
		//objConfigNA.fn_clickGoToPricing(driver, webWait).click();
	 	objConfigNA.fn_clickGotoPricingNABtn(driver, webWait).click();
		objUtils.fn_waitForLoader(driver);
		Utils.fn_waitForProgressBar(driver);
		Thread.sleep(3500);
		blnAdd_Multiple_Products = objConfigNA.fn_clickOfferingMgmntBtn(driver, webWait).isDisplayed();  
		
		 
						
	  }	 catch (Exception MultiproductAddException) {      
		     StringWriter stack = new StringWriter();
			 MultiproductAddException.printStackTrace(new PrintWriter(stack));
			 Log.debug("DEBUG: The exception in Adding Multiple products is: "+stack);}
	
		return blnAdd_Multiple_Products;		
	}
	
}
