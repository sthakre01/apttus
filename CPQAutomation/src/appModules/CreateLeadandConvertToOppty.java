/*
 Author     		:	Madhavi JN.
 Class Name			: 	CreateLeadandConvertToOppty 
 Purpose     		: 	Purpose of this file is :
						1. To perform  CreateLeadandConvertToOppty functions.

 Date       		:	04/10/2016 
 Modified Date		:	27/12/2016	
 Modified By		: 	Aashish Bhutkar
 Version Information:	Version 2.0
 
 Version Changes 2.0:	1. Modified the Billing Infomration and Option Values in adding Multiple Options in LOV.
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/


package appModules;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.AfterApproval;
import pageObjects.SalesForce.LeadCreation;
import pageObjects.SalesForce.Proposal;
import utility.Log;
import utility.Utils;


public class CreateLeadandConvertToOppty {
	
	public boolean fn_CreateLeadAndConvertToOppty(WebDriver driver,WebDriverWait webWait,String strLeadCreationData
													,String strBillingInformation,String strOptionsData,String strConversionData,String strOpptyData) throws Exception
    {

		String[] arrLeadData = strLeadCreationData.split("#");
		String[] arrConvData = strConversionData.split("#");
		String[] arrOpptData = strOpptyData.split("#");
		String[] arrBillingData = strBillingInformation.split("#");
		String[] arrOptionsData = strOptionsData.split("#");
		
		String strEntityName ="";
		String strCRname="";
		String strOpptyName="";
		String strOptions="";
		Utils objUtils = new Utils();
		boolean blnConvtdToOppty = false;

		LeadCreation ObjctsLead = new LeadCreation();	
		Proposal objProposal = new Proposal();
		AfterApproval objAfterPO = new AfterApproval();

		//++++++++++++++CreateLeadAndConvertToOppty+++++++++++++++++++++++++++++++++++++++++++++++++++++++
		ObjctsLead.fn_selectLeadsTab(driver, webWait).click();
		ObjctsLead.fn_clickNew(driver, webWait).click();
		ObjctsLead.fn_clickContinuebtn(driver, webWait).click();
		ObjctsLead.fn_setLastName(driver, webWait).sendKeys(arrLeadData[0]);
		Date date = new Date() ;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");  		
    	//TODO:: generating the New proposal name with timestamp to make the name unique for further validations
    	String strCompanyName = arrLeadData[1]+dateFormat.format(date);	
		ObjctsLead.fn_setCompany(driver, webWait).sendKeys(strCompanyName);
		ObjctsLead.fn_setPhone(driver, webWait).sendKeys(arrLeadData[2]);
		
		//Environment Exception catched for if Country present and Selected.
		try {			
			ObjctsLead.fn_selectCountry(driver, webWait).selectByValue(arrLeadData[3]); }          		
		catch(Exception excpEnvironment){
			Log.info("The country field is not present on the UI page of Leads");}	
		
		ObjctsLead.fn_selectLeadStatus(driver, webWait).selectByVisibleText(arrLeadData[4]);
		
		//Environment Exception catched for if Country present and Selected.
		try {			
			ObjctsLead.fn_selecApprovalStatus(driver, webWait).selectByVisibleText(arrLeadData[5]); }          		
		catch(Exception excpEnvironment){
			Log.info("The Approval Status is not present on the UI page of Leads");}	
		
		ObjctsLead.fn_selectAcctType(driver, webWait).selectByVisibleText(arrLeadData[6]);
		ObjctsLead.fn_selectIndustry(driver, webWait).selectByVisibleText(arrLeadData[7]);
		ObjctsLead.fn_clickSaveBtn(driver, webWait).click();
		ObjctsLead.fn_clickLeadConvertBtn(driver, webWait).click();
		Thread.sleep(8000);
		driver.switchTo().activeElement().sendKeys(Keys.ESCAPE);
		
		//ObjctsLead.fn_selectAccountName(driver, webWait).selectByVisibleText(arrConvData[0]);
      /*  boolean blnAccthandle = ObjUtils.fn_HandleLookUpWindow(driver, webWait, "AccountLookupWndw", arrConvData[0]);
	        if (blnAccthandle==true)
	        	Log.info("Data is entered successfully in Account Lookup (New Window)");
	        else
	        	Log.info("Unable to enter data in Account Lookup (New Window)");*/
		
		ObjctsLead.fn_selectAccountName(driver, webWait).selectByVisibleText("Create New Account: "+strCompanyName);
		ObjctsLead.fn_setOpptyName(driver, webWait).sendKeys(arrConvData[1]);
		ObjctsLead.fn_setSubject(driver, webWait).sendKeys(arrConvData[2]);
		ObjctsLead.fn_setDuedate(driver, webWait).click();
		ObjctsLead.fn_selectStatus(driver, webWait).selectByVisibleText(arrConvData[3]);
		ObjctsLead.fn_clickConvertBtn(driver, webWait).click();
		Thread.sleep(8000);
		
		//TODO :: Entity Name check
		strEntityName = ObjctsLead.fn_getEntityName(driver, webWait).getText();
		if (strEntityName.contentEquals("Sungard Availability Services (Ireland) Limited"))
			Log.info("The entity Name is displayed on the Accounts page and matches with the expected Result :: "+strEntityName);
		else
			Log.info("The entity Name is displayed and doesnt matches with the expected Result ::"+strEntityName);
		
		
		//Enter the Billing Information on Acct page which will be further used by Aria 
		Actions actionPaymntMthd = new Actions(driver).doubleClick(ObjctsLead.fn_clickPaymentMethod(driver, webWait));
		actionPaymntMthd.build().perform();
		Thread.sleep(8000);
		ObjctsLead.fn_selectPaymentMethod(driver, webWait).selectByValue(arrBillingData[0]);
		Thread.sleep(8000);
		Actions actionPaymnterm = new Actions(driver).doubleClick(ObjctsLead.fn_clickPaymentTerm(driver, webWait));
		actionPaymnterm.build().perform();
		Thread.sleep(8000);
		ObjctsLead.fn_selectPaymentTerm(driver, webWait).selectByValue(arrBillingData[1]);
		Thread.sleep(8000);
		Actions actionTaxExemptLevel = new Actions(driver).doubleClick(ObjctsLead.fn_clickTaxExemptLevel(driver, webWait));
		actionTaxExemptLevel.build().perform();
		Thread.sleep(8000);
		ObjctsLead.fn_selectTaxExemptLevel(driver, webWait).selectByValue(arrBillingData[2]);
		Thread.sleep(8000);
		Actions actionTaxExemptCertNumber = new Actions(driver).doubleClick(ObjctsLead.fn_clickTaxExemptCertificateNumber(driver, webWait));
		actionTaxExemptCertNumber.build().perform();
		Thread.sleep(8000);
		ObjctsLead.fn_setTaxExemptCertificateNumber(driver, webWait).sendKeys(arrBillingData[3]);
		Thread.sleep(8000);
		Actions actionTaxPayerID = new Actions(driver).doubleClick(ObjctsLead.fn_clickTaxPayerID(driver, webWait));
		actionTaxPayerID.build().perform();
		Thread.sleep(8000);
		ObjctsLead.fn_setTaxPayerID(driver, webWait).sendKeys(arrBillingData[4]);
		Thread.sleep(8000);
		Actions actionTaxPayerEndDate = new Actions(driver).doubleClick(ObjctsLead.fn_clickTaxPayerEndDate(driver, webWait));
		actionTaxPayerEndDate.build().perform();
		Thread.sleep(8000);
		ObjctsLead.fn_setTaxPayerEndDate(driver, webWait).sendKeys(objUtils.fn_getFutureDate(12, "dd/MM/yyyy"));
		Thread.sleep(8000);	
		ObjctsLead.fn_clickAcctDetailSave(driver, webWait).click();
		Thread.sleep(5000);
		
		//Edit the contact
		ObjctsLead.fn_clickEditContactLink(driver, webWait).click();
		Thread.sleep(5000);
		ObjctsLead.fn_setEmailOnContactPage(driver, webWait).sendKeys(arrConvData[4]);
		Thread.sleep(500);
		ObjctsLead.fn_clickSaveOnContactPage(driver, webWait).click();
		
		//TODO:: Add New Address
		ObjctsLead.fn_clickNewAddressBtn(driver, webWait).click();
		Thread.sleep(5000);
		
		//Environment Exception catched for if Country present and Selected.
		try {			
			ObjctsLead.fn_selectAddressStatus(driver, webWait).selectByVisibleText("Active"); }       		
		catch(Exception excpEnvironment){
			Log.info("The Approval Status is not present on the UI page of Address Details page");}
		
		//TODO:: fillup the Adress feilds on Adress edit page
		ObjctsLead.fn_setAddress1(driver, webWait).sendKeys("TestAutomation");
		ObjctsLead.fn_setCity(driver, webWait).sendKeys("Dublin");
		
		//Environment Exception catched for if Country present and Selected.
		try {			
			ObjctsLead.fn_selectAddressCounty(driver, webWait).selectByVisibleText("Dublin 2"); }       		
		catch(Exception excpEnvironment){
			Log.info("The Address County is not present on the UI page of Address Details page");}
		
		ObjctsLead.fn_selectCountryOnAddressEditpage(driver, webWait).selectByVisibleText("Ireland");
		ObjctsLead.fn_setPostalCodeOnAddressEditpage(driver, webWait).sendKeys("D2");
		
		//Code to Add Multiple Options
		List<WebElement> allOptionsAdd  = ObjctsLead.fn_selectMultipleOnAddressEditpage(driver, webWait).getOptions(); 
		for(WebElement WebElement : allOptionsAdd ){
			strOptions = WebElement.getText();
			for (int i=0;i<arrOptionsData.length;i++)
			if (strOptions.contains(arrOptionsData[i]))
			      WebElement.click();}
		ObjctsLead.fn_clickImgAddOnAddressEditPage(driver, webWait).click();
		
        //Code to remove the Multi[le Options from LOV
		//ObjctsLead.fn_clickImgRemoveOnAddressEditPage(driver, webWait).click();
		Thread.sleep(500);
        ObjctsLead.fn_clickSaveAddOnAddressEditPage(driver, webWait).click();
        Thread.sleep(5000);
       
        //TODO::New address Contact page      
        ObjctsLead.fn_clickContactOnAddressBtnOnContactEditPage(driver, webWait).click();
        Thread.sleep(5000);
        
        //TODO::fill up the GMSA Form details
	    objUtils.fn_HandleLookUpWindow(driver, webWait,"ContactLukUpWindwOnAdCtctEdtPage","ApptusAuto");   
		List<WebElement> allOptionsAddUser  = ObjctsLead.fn_selectUsedForOnAddressContactEditpage(driver, webWait).getOptions(); 
		for(WebElement WebElement : allOptionsAddUser ){
			strOptions = WebElement.getText();
			for (int i=0;i<arrOptionsData.length;i++)
			if (strOptions.contains(arrOptionsData[i]))
			      WebElement.click();}
        //Code to remove the Multi[le Options from LOV
		ObjctsLead.fn_clickImgAddOnAddressEditPage(driver, webWait).click();
		Thread.sleep(500);
		//ObjctsLead.fn_clickImgRemoveOnAddressEditPage(driver, webWait).click();
		Thread.sleep(500);
        ObjctsLead.fn_setEmailOnAdCtctEdtPage(driver, webWait).sendKeys(arrConvData[4]);
        ObjctsLead.fn_clickSaveOnAdCtctEdtPage(driver, webWait).click();  
        ObjctsLead.fn_clickContactLnkSaveOnAdCtctEdtPage(driver, webWait).click(); 
        Thread.sleep(5000);
        ObjctsLead.fn_clickAccountNameLink(driver, webWait).click();
        Thread.sleep(15000);
       
        //Add New customer reference
        ObjctsLead.fn_clickNewCustomerReferenceBtn(driver, webWait).click();
        strCRname = strCompanyName+"_CR";
        ObjctsLead.fn_setTierName(driver, webWait).sendKeys(strCRname);
        ObjctsLead.fn_clickSaveOnCustomerEditPage(driver, webWait).click();
        ObjctsLead.fn_clickAccountLink(driver, webWait).click();
        Thread.sleep(5000);
        
        //TODO:edit the oppty on Account page
        ObjctsLead.fn_clickEditOpttyOnAcctPage(driver, webWait).click();
        ObjctsLead.fn_clickOpptyEditOnOpptyPage(driver, webWait).click();
        ObjctsLead.fn_selectOpptyType(driver, webWait).selectByVisibleText("New Logo");
        //ObjctsLead.fn_selectOpptyCurrency(driver, webWait).selectByVisibleText(arrOpptData[2]);
        ObjctsLead.fn_setCloseDate(driver, webWait).clear();
		ObjctsLead.fn_setCloseDate(driver, webWait).sendKeys(arrOpptData[4]);
		ObjctsLead.fn_setOpptunityName(driver, webWait).click();
		ObjctsLead.fn_selectStage(driver, webWait).selectByVisibleText(arrOpptData[5]);
		WebElement webSelectCompetitors= driver.findElement(By.xpath("//tr/td/span/select[@title='Competitors - Available']"));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",webSelectCompetitors);
		Thread.sleep(1000);
		Actions actioncomptr = new Actions(driver);
		actioncomptr.moveToElement(webSelectCompetitors).doubleClick().perform();
		ObjctsLead.fn_setPriceBook(driver, webWait).clear();
		Thread.sleep(100);
		ObjctsLead.fn_setPriceBook(driver, webWait).sendKeys(arrOpptData[6]);
	
    	//TODO:: generating the New proposal name with timestamp to make the name unique for further validations
    	strOpptyName = "TestAutomationOpportunity"+dateFormat.format(date);
		ObjctsLead.fn_setOpptunityName(driver, webWait).click();	
		ObjctsLead.fn_setOpptunityName(driver, webWait).clear();
		ObjctsLead.fn_setOpptunityName(driver, webWait).sendKeys(strOpptyName);
		Thread.sleep(1000);
		WebElement clickSave = ObjctsLead.fn_clickOpptySaveBtn(driver, webWait);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);",clickSave);
		clickSave.click();
		Thread.sleep(5000);
		ObjctsLead.fn_clickAddProductBtn(driver, webWait).click();
		Thread.sleep(8000);
		ObjctsLead.fn_selectFilter(driver, webWait).selectByVisibleText("Product Code");
		ObjctsLead.fn_selectSrchCriteria(driver, webWait).selectByVisibleText("contains");
		ObjctsLead.fn_setSrchTxt(driver, webWait).sendKeys("GLB");
		ObjctsLead.fn_clickPrdSearchBtn(driver, webWait).click();
		Thread.sleep(4000);
		ObjctsLead.fn_clickPrdSearched(driver, webWait, "strProductFamilyName").click();
		ObjctsLead.fn_clickSelectBtn(driver, webWait).click();
		Thread.sleep(9000);
		ObjctsLead.fn_enterInvTerm(driver, webWait).sendKeys("12");
		Thread.sleep(2000);
		ObjctsLead.fn_selectSite(driver, webWait).selectByValue("DUB2");
		Thread.sleep(4000);
		String strParentWindow = driver.getWindowHandle();
		ObjctsLead.fn_setStartDate(driver, webWait).click();
		
		//TODO:: handle multiple windows
		for (String handle : driver.getWindowHandles()) {
               driver.switchTo().window(handle);}		
		Thread.sleep(5000);
		objAfterPO.fn_clickCalendarTodayLink(driver, webWait).click();
		Thread.sleep(5000);
		driver.switchTo().window(strParentWindow);
		Thread.sleep(4000);
		ObjctsLead.fn_setAnnualFee(driver, webWait).click();
		Thread.sleep(4000);
		ObjctsLead.fn_setAnnualFee(driver, webWait).clear();
		ObjctsLead.fn_setAnnualFee(driver, webWait).sendKeys("100");
		Thread.sleep(400);
		ObjctsLead.fn_setOneTimeFee(driver, webWait).clear();
		ObjctsLead.fn_setOneTimeFee(driver, webWait).sendKeys("100");
    	Thread.sleep(1000);
    	
        //TODO::fill up the GMSA Form details
	    objUtils.fn_HandleLookUpWindow(driver, webWait,"Customer Reference Lookup (New Window)",strCRname);
	    Thread.sleep(6000);
		ObjctsLead.fn_clickPrdctSaveBtn(driver, webWait).click();
		Thread.sleep(8000);
		ObjctsLead.fn_clickQuoteORProposalBtn(driver, webWait).click();
		Thread.sleep(10000);
		blnConvtdToOppty=objProposal.fn_setProposalName(driver, webWait).isDisplayed();   
        
        
		//++++++++++++++Create New Contact page+++++++++++++++++++++++++++++++++++++++++++++++++++++++
		//Environment Exception catched for if Contact present and Selected an dmoving a head for conversion
	/*	try {		
			ObjctsLead.fn_selectLeadContact(driver, webWait).selectByVisibleText("Attach to Existing: ApptusAuto");
			//To click on Convert Button
			ObjctsLead.fn_clickConvertBtn(driver, webWait).click();
			Thread.sleep(5000);}
		catch(Exception excpEnvironment){	
			Log.info("The Contact field is not present on the UI Lead to ppty Conversion page");}
		//++++++++++++++Enter fields of Oppty page+++++++++++++++++++++++++++++++++++++++++++++++++++++++
		ObjctsLead.fn_clickNewOpptyBtn(driver, webWait).click();
		ObjctsLead.fn_setOpptunityName(driver, webWait).sendKeys(arrOpptData[0]);
		Thread.sleep(3000);
		ObjctsLead.fn_selectOpptyType(driver, webWait).selectByVisibleText(arrOpptData[1]);*/
		
		return blnConvtdToOppty;
    }
	
	

	
}
