 /*==============================================================
 Author     		:	Madhavi JN
 Class Name		 	: 	Add_ConstraintRules_for_MultiProducts
 Purpose     		: 	Validating the ConstraintRules for Multiple Products
 Date       		:	16/02/2017
 Version Information:	Version1.0
 PreCondition 		:	None
 Test Steps 		:	Validating the ConstraintRules for specific product
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
 #======================================================================
 */
package appModules;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import utility.Log;
import utility.VerificationMethods;

public class Add_ConstraintRules_for_MultiProducts extends VerificationMethods{
	
	public Boolean fn_ConstRules_for_MultiProducts(WebDriver driver,WebDriverWait webWait, 
			List<List<Map<String, String>>> lstmapConstrRules) throws Exception
    {
        Boolean blnConstValSuccessful=false;
		
        try
        {
    	   	for(List<Map<String, String>> lstmap: lstmapConstrRules)
	    	{
	    	    for(Map<String, String> map : lstmap)
	    	    {	    	        		 
	    	        for (Map.Entry<String, String> entry : map.entrySet())
	    	        {  
	    	        	String strKeyName = entry.getKey().toString().trim();
	    	        	
	    	        	String strValueName = entry.getValue().toString().trim();
					 }
	    	    }
	    	} 
           
        }    
       catch (Exception e)
        {
	    	StringWriter stack = new StringWriter();
			e.printStackTrace(new PrintWriter(stack));
			blnConstValSuccessful=false;
			Log.debug("DEBUG: The exception in Validating the Constraint rules for Multiple products is: "+stack);
        }	
      
        return blnConstValSuccessful;
    }
	

   }
