package appModules;

import java.io.File;
import java.util.ArrayList;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.Configuration;
import utility.EnvironmentDetails;
import utility.EnvironmentDetailsSAS;
import utility.ExcelHandler;
import utility.Log;

public class Add_Configurations {
	
	public boolean fn_AddConfigurations(WebDriver driver,WebDriverWait webWait,String strTestCaseName, String strConfigData) throws Exception
    {
			
		Configuration objConfig = new Configuration();
		EnvironmentDetailsSAS   objEnvDetails = new EnvironmentDetailsSAS();
		String[] arrstrConfigData = strConfigData.split("#");
		boolean blnConfiguration =false;
		//String strOfferingName = null;
		String strProposalType = EnvironmentDetails.mapGlobalVariable.get("strProposalType").toString();
		
		
        try
        {   
        	
        	//block to handle Configure button and  Add New Product
        	try {
        		//Click on Configure button
            	objConfig.fn_clickConfigureBtn(driver, webWait).click();
			} catch (Exception e) {
				Log.info("Configure button not present cont with Add New Product");
				objConfig.fn_AddMoreProductsBtn(driver, webWait).click();
			}
        	   
        	//Click on Add New Product for Amendment proposal 
        	if ((strProposalType !=null && strProposalType.equalsIgnoreCase("Amendment")) || (strProposalType !=null && strProposalType.equalsIgnoreCase("Renewal")) ) {
        		 objConfig.fn_AddMoreProductsBtn(driver, webWait).click();
			}
        	//click the Baseservice name
        	objConfig.fn_clickBaseServiceName(driver, webWait, arrstrConfigData[0]).click();
        	//Click the ServiceName
        	objConfig.fn_clickServiceName(driver, webWait, arrstrConfigData[1]).click();   	
        	Thread.sleep(6000);  
        	objConfig.fn_clickNext(driver, webWait).click();
        	Thread.sleep(6000); 
        	File fileConfigValidations = new File(objEnvDetails.dataSheetPath);	
  		    //TODO : Read Test Excel
 		    ArrayList<String> arrlistFileContents = ExcelHandler.readExcelFileAsArray(fileConfigValidations,arrstrConfigData[2]);		   
 		    String strOptionName=null;
 		    String strXpath = null;
		    int intRowtraverser=0;

		    //Utils.takeScreenshot(driver, strTestCaseName);
		    Log.info("++++++++++++++++++++++++++++++++++++Start of Configuration of Services+++++++++++++++++++++++++++++++++++++++++++++");
			//TODO: Now Traverse the ArrayList for Excel File Read to find the item from data table.
			for(int intCnt = 0 ; intCnt < arrlistFileContents.size(); intCnt +=2)
			{	
				intRowtraverser = intCnt;
				strOptionName = arrlistFileContents.get(intRowtraverser);
				strXpath = "//a[text()='"+strOptionName+"']/ancestor::tr[1]/td[1]//input";	
				try
				{					
                    //Identify the checkbox/radio button control
					WebElement webidentifychkbox = driver.findElement(By.xpath(strXpath));					
					Thread.sleep(10000);							
					webidentifychkbox.click();	// to click the checkbox
					Thread.sleep(10000);
					fn_waitForSpinner(driver);
				// to identify back the checked check-box/radio button webelement.
					WebElement webselchkbox = driver.findElement(By.xpath(strXpath));					
					Boolean blnResult = webselchkbox.isSelected();
					if (blnResult==true)
						Log.info("Component of the Configuration is selected successfully::"+strOptionName); 	
					else
						Log.info("Component of the Configuration is not selected "+strOptionName); 
				}
			     catch (Exception e)
		        {
		            Log.error("Unable to click the element");
		            Log.error(e.getMessage());
		            e.printStackTrace();

		        }
			
			}
		    Log.info("++++++++++++++++++++++++++++++++++++End of Configuration of Services+++++++++++++++++++++++++++++++++++++++++++");
		    Thread.sleep(10000);
		    //Utils.takeScreenshot(driver, strTestCaseName);
		    
		    /*
		    //ReConfigure Product for Amendment proposal  call by different way 
        	if (objCreateUpgDwg.strProposalType !=null && objCreateUpgDwg.strProposalType.equalsIgnoreCase("Amendment")) {
        		objCreateUpgDwg.fn_ReConfigureProduct(driver, webWait);
        		Thread.sleep(10000);
			}
			*/
        	//TODO:: click on Pricing Button
			objConfig.fn_clickPricingBtn(driver, webWait).click();
			Thread.sleep(10000); 
			//Utils.takeScreenshot(driver, strTestCaseName);
			/*//Attch Offering
			objConfig.fn_clickOfferingMgmntBtn(driver, webWait).click();
			Thread.sleep(10000); 
			WebElement webElementDragFrom = objConfig.fn_clickDragFrom(driver, webWait);
			//Add product to given offering name
			//Retrieve offering name base on key or directly assign offering name 
				if (arrstrConfigData[3].contains("Key")) {
					strOfferingName = EnvironmentDetails.mapGlobalVariable.get(arrstrConfigData[3]).toString();
				} else {
					strOfferingName = arrstrConfigData[3];
				}
			
			WebElement webElementDragTo = objConfig.fn_clickDragTo(driver, webWait, strOfferingName);
			Actions builder = new Actions(driver);
			builder.dragAndDrop(webElementDragFrom, webElementDragTo).build().perform();
			Thread.sleep(10000);
			//Pick Start date and Billing date for Amendment 
			if (strProposalType !=null && strProposalType.equalsIgnoreCase("Amendment")) {
       		 objConfig.fn_SelectChangeStartDatePicker(driver, webWait).click();
       		 objConfig.fn_SelectChangeBillingDatePicker(driver, webWait).click();
			}
			objConfig.fn_clickGoToCart(driver, webWait).click();
			Thread.sleep(10000);
			blnConfiguration= objConfig.fn_clickShowPricing(driver, webWait).isDisplayed();
			//TODO :: click on toggle button
			objConfig.fn_clickShowPricing(driver, webWait).click();
			Thread.sleep(10000);*/
			blnConfiguration= true;
        }
       catch (Exception e)
        {
            Log.error("ERROR: Unable to Add the Configuration");
            Log.error(e.getMessage());
            e.printStackTrace();

        }
        return blnConfiguration;
    }
	
	
    public void fn_waitForSpinner(WebDriver driver)
    {
        By spinnerLocator = By.className("spinnerImg-tree");
        WebDriverWait wait = new WebDriverWait(driver, 120);
        try{
           // Log.info("Waiting for spinner to disappear..");
            wait.until(ExpectedConditions.invisibilityOfElementLocated(spinnerLocator));
          //  Log.info("Finished Waiting..");
        }
        catch(TimeoutException e)
        {
        	Log.info("Timed out waiting for spinner to disappear...");
        }
    }
}
