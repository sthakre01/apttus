package appModules;

import java.io.IOException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

//import pageObjects.SalesForce.ApproveAndGenerateGMSAPageObjects;
import pageObjects.SalesForce.ApproveAndGenerateOrderForm;
import utility.Log;

public class ValidateOrderFormDoc {
	

	public Boolean fn_ValidateOrderFormDoc(WebDriver driver,WebDriverWait webWait,String strDocName,String strAddress) throws IOException, Throwable
    {   

		//ApproveAndGenerateGMSAPageObjects objctApprve = new ApproveAndGenerateGMSAPageObjects();
		ApproveAndGenerateOrderForm objctOrderForm = new ApproveAndGenerateOrderForm();
		//WordDocsHandler objWordhandler = new WordDocsHandler();

		
		try{
              
			//To Click on the Send For Review Btn on Agremment Page
			objctOrderForm.fn_clickSendForReviewBtn(driver, webWait).click();
			Thread.sleep(5000);
			//To select the Generated GMSA
			objctOrderForm.fn_clickAttachGMSA(driver, webWait).click();
			//To click on the Next Button
			objctOrderForm.fn_clickNextBtn(driver, webWait).click();
			//To select the Order Form To generate
			objctOrderForm.fn_clickNewOrderFormBtn(driver, webWait).click();
			
		}
		catch (Exception e) {
			// TODO: handle exception
			Log.info("ERROR: problem while Validating the FunctionalDocs");
			e.printStackTrace();
		}	
		return true;
    }		
}
