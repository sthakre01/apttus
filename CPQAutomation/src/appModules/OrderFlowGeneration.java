package appModules;

import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.Account;
import pageObjects.SalesForce.OrderFlow;
import utility.Log;
import utility.Utils;


public class OrderFlowGeneration{
	
	public boolean fn_OrderFlowGeneration(WebDriver driver,WebDriverWait webWait)throws Exception
	{
		String strApprovalStagetext="";
		String strParentWindow="";
		String strChildWindow;
		String strMSA_AgrmntNumber="";
		boolean blnIsAlertPresent=false;
		boolean blnOrderFlow = false;
		OrderFlow objOrderFlow = new OrderFlow();
		Utils objUtils = new Utils();
		Thread.sleep(8000);
		
		
		Account objacctpage = new Account();
		objacctpage.fn_setSearchField(driver, webWait).sendKeys(CreateProposal.strProposalNumber); 
		objacctpage.fn_clickSearchBtn(driver, webWait).click();
		Thread.sleep(1000);
		driver.findElement(By.partialLinkText(CreateProposal.strProposalNumber)).click();
		
		objOrderFlow.fn_clickGenerate(driver, webWait).click();
		Thread.sleep(2000);
		objOrderFlow.fn_clickWaterMark(driver, webWait).click();
		Thread.sleep(2000);
		objOrderFlow.fn_clickProposalToPresent(driver, webWait).click();
		Thread.sleep(8000);
		objOrderFlow.fn_clickProposalGenerateBtn(driver, webWait).click();
		Thread.sleep(10000);	
		
		WebElement elemntReturnBtn= webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@value='Return' and @type='submit']")));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elemntReturnBtn);
		Thread.sleep(1000);	
		elemntReturnBtn.click();

		//objOrderFlow.fn_clickReturnBtn(driver, webWait).click();
		Thread.sleep(5000);
		strApprovalStagetext=objOrderFlow.fn_getTextApprovalStage(driver, webWait).getText();
		if (strApprovalStagetext.contentEquals("Generated"))
			Log.info("Approval stage is successfully changed to Generated");
		else
			Log.info("Approval stage is not changed to Generated"+strApprovalStagetext);
		objOrderFlow.fn_clickPresent(driver, webWait).click();
		Thread.sleep(5000);
		
		Thread.sleep(1000);
		objUtils.fn_HandleLookUpWindow(driver, webWait,"Contact Lookup (New Window)","Apptus");
		Thread.sleep(4000);
		objOrderFlow.fn_setTextinSubject(driver, webWait).sendKeys("TestAutomation");
		Thread.sleep(1000);
		objOrderFlow.fn_setTextinEmailBody(driver, webWait).sendKeys("TestAutomation");
		Thread.sleep(1000);
		objOrderFlow.fn_clickPropAttchmnt(driver, webWait).click();
		Thread.sleep(1000);
		objOrderFlow.fn_clickSendEmailBtn(driver, webWait).click();
		Thread.sleep(8000);
	    objOrderFlow.fn_clickAccept(driver, webWait).click();
	    Thread.sleep(8000);
	    strParentWindow=driver.getWindowHandle();
	    objOrderFlow.fn_clickMSA_AGRMNT_Btn(driver, webWait).click();
	    Thread.sleep(8000);
	    Set<String>strWindowHandles = driver.getWindowHandles();
	    strWindowHandles.remove(strParentWindow);
	    strChildWindow=strWindowHandles.iterator().next();   
	    driver.switchTo().window(strChildWindow);
	    
        //TODO::fill up the GMSA Form details
	    objUtils.fn_HandleLookUpWindow(driver, webWait,"Address Lookup (New Window)","LAC");
	    Thread.sleep(4000);
	    objOrderFlow.fn_clickMSAStartDate(driver, webWait).click();
	    Thread.sleep(1000);
	    
	    webWait = new WebDriverWait(driver,10);
	    
		//Environment Exception catched for CreateAnthrBtn if present and Selected.
		try {			
			 objOrderFlow.fn_clickCreateAnthrBtn(driver, webWait).click(); }          		
		catch(Exception excpEnvironment){
			Log.info("The CreateAnthrBtn field is not present on the UI page of MSA Form");}	
	    
	    Thread.sleep(500);
	    objOrderFlow.fn_clickMSASaveBtn(driver, webWait).click();
	    Thread.sleep(4000);
	    
	    webWait = new WebDriverWait(driver,60);
	    
	    strMSA_AgrmntNumber = objOrderFlow.fn_captureMSAAgrmntNumber(driver, webWait).getText();
	    Log.info("MSA Agrement number is captured successfully the number generated is ::"+strMSA_AgrmntNumber);
	    Thread.sleep(1000);
	    driver.close();
	    driver.switchTo().window(strParentWindow);
	    Thread.sleep(1000);
		objacctpage.fn_setSearchField(driver, webWait).sendKeys(CreateProposal.strProposalNumber); 
		objacctpage.fn_clickSearchBtn(driver, webWait).click();
		Thread.sleep(1000);
		driver.findElement(By.partialLinkText(CreateProposal.strProposalNumber)).click();
	    
	    objOrderFlow.fn_clickCrtAgmntLinItmsBtn(driver, webWait).click();
	    Thread.sleep(4000);
	    objOrderFlow.fn_clickContinueBtn(driver, webWait).click();
	    Thread.sleep(1000);
	    objUtils.fn_HandleLookUpWindow(driver, webWait,"Address Lookup (New Window)","LAC");
	    objOrderFlow.fn_clickOrderFrmSaveBtn(driver, webWait).click();
	    Thread.sleep(4000);
	    objOrderFlow.fn_clickGenerateSpprtDocsBtn(driver, webWait).click();
	    Thread.sleep(1000);
	    objOrderFlow.fn_clickWaterMark(driver, webWait).click();
	    Thread.sleep(1000);
	    objOrderFlow.fn_clickSelectTempOnOrderFrm(driver, webWait).click();
	    Thread.sleep(4000);
	    objOrderFlow.fn_clickOrdrFrmGnrtBtn(driver, webWait).click();
	    Thread.sleep(55000);
	    
	    try{   
		    Alert alert = driver.switchTo().alert();
		    Log.info("the message thrown from alert is :: "+alert.getText());
		    alert.accept();
		    blnIsAlertPresent=true;
	    	  }catch(Exception e){ 
	    		  Log.info("unexpected alert is not not present");   
	    	  }
	    
	    Thread.sleep(5000);
	    if (blnIsAlertPresent==false){
		WebElement elemntReturnBtn1= webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@value='Return' and @type='submit']")));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elemntReturnBtn1);
		Thread.sleep(1000);	
		elemntReturnBtn1.click();}
		
        Thread.sleep(8000);
	    objOrderFlow.fn_clickSendForReview(driver, webWait).click();
	    Thread.sleep(4000);
	    objOrderFlow.fn_clickSelectAttachment(driver, webWait).click();
	    objOrderFlow.fn_clickNextBtn(driver, webWait).click();
	    //Select Email Template
	    objOrderFlow.fn_clickEmailTemplate(driver, webWait).click();
	    Thread.sleep(4000);
	    //objOrderFlow.fn_clickNextBtnEmailTemplate(driver, webWait).click();
	    
		WebElement elemntNextBtn= objOrderFlow.fn_clickNextBtnEmailTemplate(driver, webWait);
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elemntNextBtn);
		Thread.sleep(1000);	
		elemntNextBtn.click();
	    
	    Thread.sleep(4000);
	    //Send email with sungardas template
	    objUtils.fn_HandleLookUpWindow(driver, webWait,"Reports To Lookup (New Window)","QA Test");
	    Thread.sleep(1000);
	    objOrderFlow.fn_clickEmailSendBtn(driver, webWait).click();
	    
	    //Docusignprocess
	    objOrderFlow.fn_clickESignatureBtn(driver, webWait).click();
	    Thread.sleep(500);
	    objOrderFlow.fn_clickAddRecipientsBtn(driver, webWait).click();
	    Thread.sleep(1000);
	    objOrderFlow.fn_setNameForRecipient(driver, webWait).sendKeys("madhavi");
	    Thread.sleep(500);
	    objOrderFlow.fn_setEmailForRecipient(driver, webWait).sendKeys("madhavi.jn@sungardas.com");
	    Thread.sleep(500);
	    objOrderFlow.fn_clickSendForEsignatureBtn(driver, webWait).click();
	    Thread.sleep(55000);
	    blnOrderFlow = true;
		return blnOrderFlow;
		
	}



}
