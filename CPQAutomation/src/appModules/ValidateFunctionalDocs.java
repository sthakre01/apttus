package appModules;

import java.io.IOException;
import java.util.Set;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.ApproveAndGenerateGMSA;
import utility.Log;
import utility.Utils;
import utility.WordDocsHandler;

public class ValidateFunctionalDocs {
	

	public Boolean fn_ValidateFunctionalDocs(WebDriver driver,WebDriverWait webWait,String strDocName,String strAddress) throws IOException, Throwable
    {   

		String strParentWindow=null;
		String strchildWindow=null;
		ApproveAndGenerateGMSA objctApprve = new ApproveAndGenerateGMSA();
		ProposalDoc_Creation_And_Validation objDoccreation = new ProposalDoc_Creation_And_Validation();
		//CreateOfferingData objOffData = new CreateOfferingData();
		WordDocsHandler objWordhandler = new WordDocsHandler();
		Utils objUtils = new Utils();
		String strFilename=null;
		
		try{
		//Select the approval status as Accepted
		Actions action = new Actions(driver);
		action.moveToElement(objctApprve.fn_clickDraft(driver, webWait)).doubleClick().perform();
		Thread.sleep(2000);
		objctApprve.fn_selectApproval(driver, webWait).selectByVisibleText("Accepted");
		//Click on save button
		objctApprve.fn_clickSave(driver, webWait).click();
		Thread.sleep(1000);
		strParentWindow= driver.getWindowHandle();
		//Click on the Button CreteAgreementGMSA
		objctApprve.fn_createGMSA(driver, webWait).click();
		Thread.sleep(3000);
		//TODO: get the frame for the Child window
		Set<String> strWindowHandles = driver.getWindowHandles();   		
		strWindowHandles.remove(strParentWindow);
		strchildWindow = strWindowHandles.iterator().next();		
		driver.switchTo().window(strchildWindow);
		Thread.sleep(1000);
			if (strDocName.contains("GMSA")){
			//Select the Type as GMSA
			objctApprve.fn_selectDocType(driver, webWait).selectByVisibleText("GMSA");}
			else{
			objctApprve.fn_selectDocType(driver, webWait).selectByVisibleText("NDA");}	
		//Click on Continue Button
		objctApprve.fn_clickContinueBtn(driver, webWait).click();
		Thread.sleep(1000);
		//Click on Agreement start Date
		objctApprve.fn_clickStartDate(driver, webWait).click();
		//Filling the Address Field
			if (strDocName.contains("NDA")){
			//Select the Type as GMSA
				objUtils.fn_HandleLookUpWindow(driver, webWait,"NDA_Address",strAddress);}	
			if (strDocName.contains("GMSA")){
			//click on Save with Warning CheckBox
			objctApprve.fn_clickSaveWarning(driver, webWait).click();}
		//click on Savebtn
		objctApprve.fn_clickSaveBtn(driver, webWait).click();
		Thread.sleep(1000); 
		//click on Generate button
		objctApprve.fn_clickGenerateBtn(driver, webWait).click();
		//Select the Document tYpe
		//ObjctApprve.fn_selDocumentType(driver, webWait).click();
		//Select the GMSA Template
		//ObjctApprve.fn_selTemplateType(driver, webWait).click();	
		Thread.sleep(6000);   
		//Click on Generate Btn on GMSA Generate Page
		WebElement GenerateClick = objctApprve.fn_clickGenerateBtnOnTemplatepg(driver, webWait);
		((JavascriptExecutor)driver).executeScript("window.scrollTo(0,"+GenerateClick.getLocation().y+")");
		GenerateClick.click();
		Thread.sleep(60000);
		//Click on Link View the file
		WebElement LinkViewFileClick = objctApprve.fn_clickLinkViewFile(driver, webWait);
		((JavascriptExecutor)driver).executeScript("window.scrollTo(0,"+LinkViewFileClick.getLocation().y+")");
		LinkViewFileClick.click();
		Thread.sleep(60000);
		//Switch to new window opened if PDF file is opened in new browser
		/*for(String winHandle : driver.getWindowHandles()){
		    driver.switchTo().window(winHandle);
		}*/
		//Download the document and validate it
		strFilename=objDoccreation.fn_downloadFile(driver, webWait, strDocName,".doc");
			if (strDocName.contains("GMSA")){
			objWordhandler.fn_validateText(strFilename, "doc","GMSA_Validations");}
			else{
			objWordhandler.fn_validateText(strFilename, "doc","NDA_Validations");}	
		//Close the new window, if that window no more required
		driver.close();
		Thread.sleep(4000); 
		driver.switchTo().window(strParentWindow);
		}
		catch (Exception e) {
			// TODO: handle exception
			Log.info("ERROR: problem while Validating the FunctionalDocs");
			e.printStackTrace();
		}	
		return true;
    }		
}
