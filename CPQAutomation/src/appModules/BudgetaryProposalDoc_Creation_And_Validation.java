package appModules;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.GenerateProposalDoc;
import utility.Log;
import utility.ReportUtils;

public class BudgetaryProposalDoc_Creation_And_Validation {
	
	public boolean fn_BudgetaryProposalDocCreationValidation(WebDriver driver,WebDriverWait webWait) throws Throwable
	{
		
		GenerateProposalDoc objProposalPO = new GenerateProposalDoc();
		ReportUtils objReportutils= new ReportUtils();	
		//WordDocsHandler objWordhandler = new WordDocsHandler();
		//TO click on Generate Button on Proposal Page
		objProposalPO.fn_clickGenerateBtn(driver, webWait).click();
		Thread.sleep(2000);
		//To select the doc type	
		objProposalPO.fn_selDocumentType(driver, webWait).click();
		//To click on Budgetary Proposal doc Generation
		objProposalPO.fn_clickGenerateBdgtPrp(driver, webWait).click();
		Thread.sleep(2000);
		//To click on Generate Button on Proposal Generation Page
		objProposalPO.fn_clickGenerateBtn(driver, webWait).click();
		Thread.sleep(2000);
		//To Click on LInk Click to View the file
		objProposalPO.fn_clickLinkViewFile(driver, webWait).click();
		Thread.sleep(2000);
        //Creating the File Name to download and rename with filename
		Date date = new Date() ;
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
		String filename = System.getProperty("user.dir")
				+ System.getProperty("file.separator") + "trunk" +System.getProperty("file.separator") +"src"+System.getProperty("file.separator")+ "testData"
				+ System.getProperty("file.separator") +"ProposalDocument"+dateFormat.format(date);
	    Log.info("the filename generated is "+filename);
	    filename = filename+".doc";
	    Thread.sleep(2000);
		//To Download the Proposal Doc
		objReportutils.fn_ReportsFileDownload(driver, filename);
		Thread.sleep(2000);
		//objWordhandler.validateText(filename, "doc","BudgetProp_Validations");
		Thread.sleep(2000);
		//To click on Return Button to return to Proposal page
		objProposalPO.fn_ClickReturnBtn(driver, webWait).click();
		
		return true;

	}

}
