/*
 Author     		:	Suhas Thakre
 Class Name			: 	MSAGeneration_And_Activation 
 Purpose     		: 	Purpose of this file is :
						1. To Generate and activate GMSA. 

 Date       		:	21/02/2017 
 Modified Date		:		
 Modified By		: 	
 Version Information:	Version 1.0
 						
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/

package appModules;


import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.Account;
import pageObjects.SalesForce.AfterApproval;
import pageObjects.SalesForce.OrderFlow;
import pageObjects.SalesForce.Proposal;
import utility.API_SOAP;
import utility.EnvironmentDetails;
import utility.Log;
import utility.Utils;
import utility.VerificationMethods;
import utility.WriteExcel;


public class MSAGeneration_And_Activation extends VerificationMethods {
	
	public  String strMSA_AgrmntNumber;
	public  String strMSA_AgrmntName;
	
	/*
	Author: Suhas Thakre
	Function fn_MSAGeneration_And_Activation: Generate GMSA.
	Inputs: 
	Outputs: Boolean response to check success or failure of function .
	 */
	public boolean fn_MSAGeneration_And_Activation(WebDriver driver,WebDriverWait webWait)throws Exception
	{
		//TODO:: Generate, Send for Review and esignature GMSA.  
		/************************************************************************************************/
			//Step 1: Create GMSA. 
			//Step 2: Generate GMSA.
			//Step 3: Send GMSA for e signature.
			
		
		/************************************************************************************************/
		boolean blnMSAGenerationAndActivation = false;
		
		
		try
		{
			//Step 1
			fn_CreateGMSA(driver, webWait);
			
			//Step 2
			fn_GenerateGMSA(driver, webWait);
			
			//Step 3 
			fn_SendSignatureGMSA(driver, webWait);
			
		}
	    catch (Exception ExceptionMSAGeneration) {
			Log.error("ERROR: Unable to Generate  MSA with exception reported as: "+ExceptionMSAGeneration.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionMSAGeneration.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_MSAGeneration_And_Activation method is: "+stack);
           
		}
	    Log.debug("DEBUG: Execution of function 'fn_MSAGeneration_And_Activation' Completed.");
	    blnMSAGenerationAndActivation = true;
		return blnMSAGenerationAndActivation;
		
	}
	
	
	
	/*
	Author: Suhas Thakre
	Function fn_MSAGenerateActivateAppl: Generate GMSA and Activate.
	Inputs: 
	Outputs: Boolean response to check success or failure of function .
	 */
	public boolean fn_MSAGenerateActivateAppl(WebDriver driver,WebDriverWait webWait, String strSignFrom)throws Exception
	{
		//TODO:: Generate, Send for Review and esignature GMSA.  
		/************************************************************************************************/
			//Step 1: Create GMSA. 
			//Step 2: Generate GMSA.
			//Step 3: Set GMSA singed document.
			//Step 4: Activate GMSA. 
		
		/************************************************************************************************/
		boolean blnMSAGenerationAndActivation = false;
		
		
		try
		{
			//Step 1: Create GMSA.
			fn_CreateGMSA(driver, webWait);
			
			//Step 2: Generate GMSA.
			fn_GenerateGMSA(driver, webWait);
			
			if (strSignFrom.equalsIgnoreCase("API")) {
				fn_DocSignAPI(driver, webWait, "strMSA_AgrmntName");			}
			else if (strSignFrom.equalsIgnoreCase("Application")) {
				//Step 3: Set GMSA singed document.
				fn_SetGMSADoc(driver, webWait, "strMSA_AgrmntName");			}
			
			
			//Step 4: Activate GMSA.
			fn_ActivateGMSAAppl(driver, webWait);
			
		}
	    catch (Exception ExceptionMSAGeneration) {
			Log.error("ERROR: Unable to Generate  MSA with exception reported as: "+ExceptionMSAGeneration.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionMSAGeneration.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_MSAGeneration_And_Activation method is: "+stack);
           
		}
	    Log.debug("DEBUG: Execution of function 'fn_MSAGenerateActivateAppl' Completed.");
	    blnMSAGenerationAndActivation = true;
		return blnMSAGenerationAndActivation;
		
	}

	
	/*
	Author: Suhas Thakre
	Function fn_MSASignAPI: Sign GMSA using API.
	Inputs: 
	Outputs: Boolean response to check success or failure of function .
	 */
	public boolean fn_DocSignAPI(WebDriver driver,WebDriverWait webWait, String strAgrmntName)throws Exception
	{
		//TODO:: Generate, Send for Review and esignature GMSA.  
		/************************************************************************************************/
			//Step 1: Open agreement.
			//Step 2: Get GMSA ID. 
			//Step 3: Update Status using API.
			
		
		/************************************************************************************************/
		
		
		API_SOAP objAPI_SOAP = new API_SOAP();
		MSOutlookApproval objMSOutlookApproval = new MSOutlookApproval();
		//Account objacctpage = new Account();
		//Proposal objProposal= new Proposal();
		boolean blnExceptionMSASignAPI = false;
		Map<String, Object> mapTestCases = new HashMap<String, Object>();
		EnvironmentDetails objEnv = (EnvironmentDetails) EnvironmentDetails.mapGlobalVariable.get("objEnv");
		String strAgreementName = EnvironmentDetails.mapGlobalVariable.get(strAgrmntName).toString();
		//String strNewproposalName = EnvironmentDetails.mapGlobalVariable.get("strNewproposalName").toString();
		Map<String, String> mapSOAP = new HashMap<String, String>();
		mapSOAP.put("Apttus__Status_Category__c", "In Signatures");
		mapSOAP.put("Apttus__Status__c", "Fully Signed");
		String strApttusQuery = "";
		String Id = "";

		try
		{
			//Step 1: Open agreement. 
			/*objacctpage.fn_setSearchField(driver, webWait).sendKeys(strNewproposalName); 
			objacctpage.fn_clickSearchBtn(driver, webWait).click();
			driver.findElement(By.partialLinkText(strNewproposalName)).isEnabled();
			driver.findElement(By.partialLinkText(strNewproposalName)).click();
			objProposal.fn_clickAgreementsLink(driver, webWait, strAgreementName).isEnabled();
			objProposal.fn_clickAgreementsLink(driver, webWait, strAgreementName).click();*/
			
			//Step 2: Get GMSA ID.
			mapTestCases.put("CallType", "query");
			strApttusQuery = "SELECT Id FROM Apttus__APTS_Agreement__c WHERE Name = '"+strAgreementName+"'";
			mapTestCases.put("CallingData", strApttusQuery);
			List<List<Map<String, String>>> lstmapMSAID = objAPI_SOAP.fn_SOAP_GET(objEnv, mapTestCases, null);
			
			//Handle Object not supported issue in Dev
			if (lstmapMSAID.toString().contains("INVALID_TYPE: sObject")) {
				lstmapMSAID = objMSOutlookApproval.fn_WorkbenchSOQL(driver, webWait, strApttusQuery);
			}
			
			
			for (List<Map<String, String>> listmap : lstmapMSAID) {
				 for (Map<String, String> map : listmap) {
					 Id = map.get("Id").toString();
					 mapSOAP.put("Id", map.get("Id").toString());
				}
			}
			
			//Step 3: Update Status using API.
			mapTestCases.put("CallType", "update");
			mapTestCases.put("CallingData", "Apttus__APTS_Agreement__c");
			mapSOAP.put("UserID", "SOAP_UserID");
			mapSOAP.put("Password", "SOAP_Password");
			List<List<Map<String, String>>> lstmapUpdate = objAPI_SOAP.fn_SOAP_GET(objEnv, mapTestCases, mapSOAP);
			
			//Handle Object not supported issue in Dev
			if (EnvironmentDetails.mapEnvironmentDetails.get("Environment").equalsIgnoreCase("DEV") || EnvironmentDetails.mapEnvironmentDetails.get("Environment").equalsIgnoreCase("UAT")) {
				objMSOutlookApproval.fn_WorkbenchUpdate(driver, webWait, Id, "Apttus__APTS_Agreement__c");;
			}
			
			Thread.sleep(4000);
			driver.navigate().refresh();
			Thread.sleep(1000);
						
			
		}
	    catch (Exception ExceptionMSASignAPI) {
			Log.error("ERROR: Unable to Sign  MSA with exception reported as: "+ExceptionMSASignAPI.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionMSASignAPI.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_MSASignAPI method is: "+stack);
           
		}
	    Log.debug("DEBUG: Execution of function 'fn_MSASignAPI' Completed.");
	    blnExceptionMSASignAPI = true;
		return blnExceptionMSASignAPI;
		
	}

	
	/*
	Author: Suhas Thakre
	Function fn_ActivateGMSAAppl: Activate GMSA agreement.
	Inputs: 
	Outputs: Boolean response to check success or failure of function .
	 */
	public void fn_ActivateGMSAAppl(WebDriver driver,WebDriverWait webWait)throws Exception
	{
		//TODO:: Activate GMSA agreement.  
		/************************************************************************************************/
			//Step 3: Activate GMSA agreement.
			
		/************************************************************************************************/
		
		AfterApproval objAfterPO = new AfterApproval();
		Proposal objProposal= new Proposal();
		String strNewproposalName = EnvironmentDetails.mapGlobalVariable.get("strNewproposalName").toString();
		String strAcctName = EnvironmentDetails.mapGlobalVariable.get("strAcctName").toString();
		
		
		try
		{
		Log.debug("DEBUG: Execution of function 'fn_ActivateGMSA' Started.");
		
		//Step 3
	    objAfterPO.fn_clickGMSAFrmActivate(driver, webWait).click();
	    objAfterPO.fn_clickNextButton(driver, webWait).isEnabled();
	    
		//Selecting the all Requireddocs 
		List<WebElement> allOptions  = objAfterPO.fn_selDocument(driver, webWait).getOptions(); 
		for(WebElement WebElement : allOptions ){
			WebElement.click();}
		objAfterPO.fn_clickNextButton(driver, webWait).click();
		objAfterPO.fn_clickActivateOrderForm(driver, webWait).isEnabled();
		objAfterPO.fn_clickActivateOrderForm(driver, webWait).click();	
		//Accept alert if present 
		try{  
			
			new WebDriverWait(driver,30).until(ExpectedConditions.alertIsPresent());
		    Alert alert = driver.switchTo().alert();
		    Log.info("the message thrown from alert is :: "+alert.getText());
		    alert.accept();
		     }catch(Exception e){ 
	    		  Log.info("unexpected alert is not not present");   
	    	  }
		
		Thread.sleep(3500);
		
		//Step 4: Verify status.
		assertEquals(objProposal.fn_verifyStatus(driver, webWait).getText().trim(), "Activated");
		
		driver.findElement(By.linkText(strNewproposalName)).isEnabled();
		driver.findElement(By.linkText(strNewproposalName)).click();
		driver.findElement(By.linkText(strAcctName)).isEnabled();
		
		
		
	}
    catch (Exception ExceptionActivateGMSA) {
		Log.error("ERROR: Unable to Generate Order with exception reported as: "+ExceptionActivateGMSA.getMessage());
        StringWriter stack = new StringWriter();
        ExceptionActivateGMSA.printStackTrace(new PrintWriter(stack));
        Log.debug("DEBUG: The exception in fn_ActivateGMSA method is: "+stack);
       
	}
    Log.debug("DEBUG: Execution of function 'fn_ActivateGMSA' Completed.");
    
	
		
	}

	
	/*
	Author: Suhas Thakre
	Function fn_SetGMSADoc: Set GMSA Documents details.
	Inputs: 
	Outputs: Boolean response to check success or failure of function .
	 */
	public void fn_SetGMSADoc(WebDriver driver,WebDriverWait webWait, String strAgrmntName)throws Exception
	{
		
		
		Account objacctpage = new Account();
		OrderFlow objOrderFlow = new OrderFlow();
		Proposal objProposal= new Proposal();
		//EnvironmentDetails objEnv = (EnvironmentDetails) EnvironmentDetails.mapGlobalVariable.get("objEnv");
		String strDoc = EnvironmentDetails.Path_ExternalFiles
								+ System.getProperty("file.separator") + "Doc.txt";
		String strNewproposalName = EnvironmentDetails.mapGlobalVariable.get("strNewproposalName").toString();
		String strAgreementName = EnvironmentDetails.mapGlobalVariable.get(strAgrmntName).toString();
		//boolean blnIsAlertPresent=false;
		
		try
		{
			//TODO:: Activate GMSA agreement  
			/************************************************************************************************/
				//Step 1: Open GMSA agreement page. 
				//Step 2: Set Document Details.
				//Step 3: Import Offline Document.
				
			/************************************************************************************************/
			
			Log.debug("DEBUG: Execution of function 'fn_SetGMSADoc' Started.");	
		
				//Step 1 
				objacctpage.fn_setSearchField(driver, webWait).sendKeys(strNewproposalName); 
				objacctpage.fn_clickSearchBtn(driver, webWait).click();
				Thread.sleep(1000);
				driver.findElement(By.partialLinkText(strNewproposalName)).click();
				Thread.sleep(3500);
				objProposal.fn_clickAgreementsLink(driver, webWait, strAgreementName).click();
				
				//Step 2: Set Document Details.
				objOrderFlow.fn_clickEditBtn(driver, webWait).isEnabled();
				objOrderFlow.fn_clickEditBtn(driver, webWait).click();
				objOrderFlow.fn_clickSaveBtn(driver, webWait).isEnabled();
				objOrderFlow.fn_selAgreementType(driver, webWait).selectByValue("Signed Customer Paper");
				objOrderFlow.fn_selSource(driver, webWait).selectByValue("Other Party Paper");
				objOrderFlow.fn_clickSaveBtn(driver, webWait).click();
				objOrderFlow.fn_clickEditBtn(driver, webWait).isEnabled();
				
				//Step 3: Import Offline Document.
				objOrderFlow.fn_clickImportDocBtn(driver, webWait).isEnabled();
				objOrderFlow.fn_clickImportDocBtn(driver, webWait).click();
				objOrderFlow.fn_setFile(driver, webWait).isEnabled();
				objOrderFlow.fn_setFile(driver, webWait).sendKeys(strDoc);
				objOrderFlow.fn_clickAttachFileBtn(driver, webWait).isEnabled();
				objOrderFlow.fn_clickAttachFileBtn(driver, webWait).click();
				objOrderFlow.fn_clickContinueBtn(driver, webWait).isEnabled();
				objOrderFlow.fn_clickContinueBtn(driver, webWait).click();
				objOrderFlow.fn_getDocImportedtext(driver, webWait).isDisplayed();
				objOrderFlow.fn_clickReturnBtn(driver, webWait).isEnabled();
				objOrderFlow.fn_clickReturnBtn(driver, webWait).click();
				
				//Step 4: Verify Approval status.
				assertEquals(objProposal.fn_verifyStatus(driver, webWait).getText().trim(), "Fully Signed");
				
			    
			}
			catch (Exception ExceptionActivateGMSA) {
				Log.error("ERROR: Unable to Set GMSA Documents with exception reported as: "+ExceptionActivateGMSA.getMessage());
		        StringWriter stack = new StringWriter();
		        ExceptionActivateGMSA.printStackTrace(new PrintWriter(stack));
		        Log.debug("DEBUG: The exception in fn_SetGMSADoc method is: "+stack);
		       
			}
			Log.debug("DEBUG: Execution of function 'fn_SetGMSADoc' Completed.");	
						
	}
	
	
	/*
	Author: Suhas Thakre
	Function fn_GenerateAndPresentProposal: Generate, Present and accept proposal.
	Inputs: 
	Outputs: Boolean response to check success or failure of function .
	 */
	public void fn_GenerateAndPresentProposal(WebDriver driver,WebDriverWait webWait)throws Exception
	{
		
		OrderFlow objOrderFlow = new OrderFlow();
		Utils objUtils = new Utils();
		Account objacctpage = new Account();
		String strApprovalStagetext="";
		String strNewproposalName = EnvironmentDetails.mapGlobalVariable.get("strNewproposalName").toString();
		
		//TODO:: Select Product family  
		/************************************************************************************************/
			//Step 1: Open Proposal page. 
			//Step 2: Generate proposal.
			//Step 3: Accept Proposal.
			
		
		/************************************************************************************************/
		
		try
		{
		Log.debug("DEBUG: Execution of function 'fn_GenerateAndPresentProposal' Started.");
		
		
		//Step 1
		objacctpage.fn_setSearchField(driver, webWait).sendKeys(strNewproposalName); 
		objacctpage.fn_clickSearchBtn(driver, webWait).click();
		Thread.sleep(1000);
		driver.findElement(By.partialLinkText(strNewproposalName)).click();
		
		//Step 2
		objOrderFlow.fn_clickGenerate(driver, webWait).isEnabled();
		String strPrimaryContact = objOrderFlow.fn_getPrimaryContact(driver, webWait).getText();
		objOrderFlow.fn_clickGenerate(driver, webWait).click();
		objOrderFlow.fn_clickProposalCancelBtn(driver, webWait).isEnabled();
		objOrderFlow.fn_clickWaterMark(driver, webWait).click();
		objOrderFlow.fn_clickProposalCancelBtn(driver, webWait).isEnabled();
		objOrderFlow.fn_clickProposalToPresent(driver, webWait).click();
		objOrderFlow.fn_clickProposalGenerateBtn(driver, webWait).isEnabled();
		Thread.sleep(2000);
		objOrderFlow.fn_clickProposalGenerateBtn(driver, webWait).click();
		objOrderFlow.fn_clickReturnBtn(driver, webWait).isEnabled();
		Thread.sleep(2000);
		objOrderFlow.fn_clickReturnBtn(driver, webWait).click();
		
		/*WebElement elemntReturnBtn= webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@value='Return' and @type='submit']")));
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elemntReturnBtn);
		Thread.sleep(1000);	
		elemntReturnBtn.click();*/

		//objOrderFlow.fn_clickReturnBtn(driver, webWait).click();
		objOrderFlow.fn_clickPresent(driver, webWait).isEnabled();
		strApprovalStagetext=objOrderFlow.fn_getTextApprovalStage(driver, webWait).getText();
		if (strApprovalStagetext.contentEquals("Generated"))
			Log.info("Approval stage is successfully changed to Generated");
		else
			Log.info("Approval stage is not changed to Generated"+strApprovalStagetext);
		objOrderFlow.fn_clickPresent(driver, webWait).click();
		objOrderFlow.fn_clickSendEmailBtn(driver, webWait).isEnabled();
		//Get Primary contact if it's not available on proposal
		if (!strPrimaryContact.contains("FirstName")) {
			strPrimaryContact =  EnvironmentDetails.mapGlobalVariable.get("strPrimaryContact").toString();
		} 
		objUtils.fn_HandleLookUpWindow(driver, webWait,"Contact Lookup (New Window)",strPrimaryContact);
		objOrderFlow.fn_setTextinSubject(driver, webWait).sendKeys("TestAutomation");
		objOrderFlow.fn_setTextinEmailBody(driver, webWait).sendKeys("TestAutomation");
		objOrderFlow.fn_clickPropAttchmnt(driver, webWait).click();
		objOrderFlow.fn_clickSendEmailBtn(driver, webWait).click();
		objOrderFlow.fn_clickAccept(driver, webWait).isEnabled();
		
		//Step 3
		objOrderFlow.fn_clickAccept(driver, webWait).click();
		}
		catch (Exception ExceptionGenerateAndPresentProposal) {
			Log.error("ERROR: Unable to Generate  MSA with exception reported as: "+ExceptionGenerateAndPresentProposal.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionGenerateAndPresentProposal.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_GenerateAndPresentProposal method is: "+stack);
           
		}
		Log.debug("DEBUG: Execution of function 'fn_GenerateAndPresentProposal' Completed.");
		
	}
	

	/*
	Author: Suhas Thakre
	Function fn_CreateGMSA: Create GMSA agreement.
	Inputs: 
	Outputs: Boolean response to check success or failure of function .
	 */
	public void fn_CreateGMSA(WebDriver driver,WebDriverWait webWait)throws Exception
	{
		//TODO:: Select Product family  
		/************************************************************************************************/
			//Step 1: Create GMSA from proposal page. 
			//Step 2: fill up the GMSA Form details.
			//Step 3: Read GMSA star date from offering creation sheet.
			
		/************************************************************************************************/
		
		OrderFlow objOrderFlow = new OrderFlow();
		WriteExcel objWriteExcel = new WriteExcel();
		Utils objUtils = new Utils();
		Map<String, Object> mapOutputData = new HashMap<String, Object>();
		List<Map<String, Object>> lisOutputData = new ArrayList<>();
		List<List<Map<String, Object>>> lstmapOutputData = new ArrayList<>();
		//String strChildWindow;
		String strParentWindow="";
		String strStartDate = EnvironmentDetails.mapGlobalVariable.get("strStartDate").toString();
		
		
		try
		{
		
		Thread.sleep(3500);
		Log.debug("DEBUG: Execution of function 'fn_CreateGMSA' Started.");
		strParentWindow=driver.getWindowHandle();
		
		objOrderFlow.fn_clickMSA_AGRMNT_Btn(driver, webWait).isEnabled();
		
		//Step 1
	    objOrderFlow.fn_clickMSA_AGRMNT_Btn(driver, webWait).click();
	    Thread.sleep(3500);
	    for (String handle : driver.getWindowHandles()) {

            driver.switchTo().window(handle);
            if (driver.getTitle().equalsIgnoreCase("Salesforce - Unlimited Edition")) {
           	break;
			}
          }
	    
	   
	    //Step 2
	    objOrderFlow.fn_setGMSAAddr(driver, webWait).isDisplayed();
	    objOrderFlow.fn_setGMSAAddr(driver, webWait).sendKeys(EnvironmentDetails.mapGlobalVariable.get("strAcctAddrID").toString());
	    //objUtils.fn_HandleLookUpWindow(driver, webWait,"Address Lookup (New Window)","LAC");
	    
	    
	    
	  //Step 3
	    String strGMSAStartDate = "";
	    //change date format as per country 
		SimpleDateFormat USformat = new SimpleDateFormat("MM/dd/yyyy");
		SimpleDateFormat NonUSformat = new SimpleDateFormat("dd/MM/yyyy");
		SimpleDateFormat Inputformat = new SimpleDateFormat("dd/MMM/yyyy");
		Date date = Inputformat.parse(EnvironmentDetails.mapGlobalVariable.get("strGMSAStartDate").toString());
		if (EnvironmentDetails.mapGlobalVariable.get("strCountry").toString().equalsIgnoreCase("United States")) {
			strGMSAStartDate = USformat.format(date);
		} else {
			strGMSAStartDate = NonUSformat.format(date);
		}
		
	    objOrderFlow.fn_SetMSAStartDate(driver, webWait).isDisplayed();
	    objOrderFlow.fn_SetMSAStartDate(driver, webWait).sendKeys(strGMSAStartDate);
	    
	  
	    
		//Environment Exception catched for CreateAnthrBtn if present and Selected.
		try {			
			 objOrderFlow.fn_clickCreateAnthrBtn(driver, new WebDriverWait(driver,5)).click(); }          		
		catch(Exception excpEnvironment){
			Log.info("The CreateAnthrBtn field is not present on the UI page of MSA Form");}	
	    
		objOrderFlow.fn_clickMSASaveBtn(driver, webWait).isEnabled();
	    objOrderFlow.fn_clickMSASaveBtn(driver, webWait).click();
	    
	    objOrderFlow.fn_captureMSAAgrmntNumber(driver, webWait).isDisplayed();
	    
	    strMSA_AgrmntNumber = objOrderFlow.fn_captureMSAAgrmntNumber(driver, webWait).getText();
	    strMSA_AgrmntName = objOrderFlow.fn_captureMSAAgrmntName(driver, webWait).getText();
	    
	    
	    
	    
	    //Save Agreement name in global variable map 
	    EnvironmentDetails.mapGlobalVariable.put("strMSA_AgrmntName", strMSA_AgrmntName);
	    Log.debug("DEBUG: Agreement name save in global variable map with key 'strMSA_AgrmntName'.");
	    
	    //Save Agreement Number in global variable map 
	    EnvironmentDetails.mapGlobalVariable.put("strMSA_AgrmntNumber", strMSA_AgrmntNumber);
	    Log.debug("DEBUG: Agreement Number save in global variable map with key 'strMSA_AgrmntNumber'.");
	    
	    //TODO:: Capture Screenshot for New offering 
	    objUtils.fn_takeScreenshotFullPage(driver);
	    
	    //TODO:: Write MSA Agreement Name in output file 
	    mapOutputData.put("MSA Agreement name", strMSA_AgrmntName);
	    mapOutputData.put("MSA Agreement Number", strMSA_AgrmntNumber);
	    objUtils.fn_WriteExcelApttus("MSA_Agreement_Details"+EnvironmentDetails.mapGlobalVariable.get("struniquekey").toString(), mapOutputData);
	    
	    
	    //Save Agrement number and name
	    
	    Log.info("MSA Agrement number is captured successfully the number generated is ::"+strMSA_AgrmntNumber);
	    Thread.sleep(1000);
	    driver.close();
	    driver.switchTo().window(strParentWindow);
	    Thread.sleep(1000);
	    
		}
		catch (Exception ExceptionCreateGMSA) {
			Log.error("ERROR: Unable to Generate  MSA with exception reported as: "+ExceptionCreateGMSA.getMessage());
	        StringWriter stack = new StringWriter();
	        ExceptionCreateGMSA.printStackTrace(new PrintWriter(stack));
	        Log.debug("DEBUG: The exception in fn_CreateGMSA method is: "+stack);
	       
		}
		Log.debug("DEBUG: Execution of function 'fn_CreateGMSA' Completed.");
		
	    
	}
	
	
	/*
	Author: Suhas Thakre
	Function fn_GenerateGMSA: Generate GMSA agreement.
	Inputs: 
	Outputs: Boolean response to check success or failure of function .
	 */
	public void fn_GenerateGMSA(WebDriver driver,WebDriverWait webWait)throws Exception
	{
		
		
		Account objacctpage = new Account();
		OrderFlow objOrderFlow = new OrderFlow();
		Proposal objProposal= new Proposal();
		String strNewproposalName = EnvironmentDetails.mapGlobalVariable.get("strNewproposalName").toString();
		boolean blnIsAlertPresent=false;
		
		try
		{
			//TODO:: Activate GMSA agreement  
			/************************************************************************************************/
				//Step 1: Open GMSA agreement page. 
				//Step 2: Activate the GMSA Agreement.
				//Step 3: Fetch Product family from Product name with help of Apttus query.
				//Step 4: Add Data to Map with key as product name and value as family
				//Step 4: Search and select Product family.
			
			/************************************************************************************************/
			
			Log.debug("DEBUG: Execution of function 'fn_ActivateGMSA' Started.");	
		
				//Step 1 
				objacctpage.fn_setSearchField(driver, webWait).sendKeys(strNewproposalName); 
				objacctpage.fn_clickSearchBtn(driver, webWait).click();
				Thread.sleep(1000);
				driver.findElement(By.partialLinkText(strNewproposalName)).click();
				Thread.sleep(3500);
				objProposal.fn_clickAgreementsLink(driver, webWait, strMSA_AgrmntName).click();
				
				
			    //Step 2
				//Genarate GMSA
				Thread.sleep(6000);
				objOrderFlow.fn_clickGenerateBtnOnAgrementFrm(driver, webWait).click();
				Thread.sleep(6000);
				objOrderFlow.fn_clickWaterMark(driver, webWait).click();
				Thread.sleep(2000);
				objOrderFlow.fn_clickOrdrFrmGnrtBtn(driver, webWait).click();
				
			    try{  
			    	new WebDriverWait(driver,90).until(ExpectedConditions.alertIsPresent());
				    Alert alert = driver.switchTo().alert();
				    Log.info("the message thrown from alert is :: "+alert.getText());
				    Thread.sleep(2000);
				    alert.accept();
				    blnIsAlertPresent=true;
			    	  }catch(Exception e){ 
			    		  Log.info("unexpected alert is not not present");   
			    	  }
				Thread.sleep(3500);
				
				if (blnIsAlertPresent==false){
					WebElement elemntReturnBtn1= webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@value='Return' and @type='submit']")));
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elemntReturnBtn1);
					Thread.sleep(1000);	
					elemntReturnBtn1.click();}
				
				
				//refrsh and wait until status change to In Authoring
				
				while (!objOrderFlow.fn_getStatusCategory(driver, webWait).getText().equalsIgnoreCase("In Authoring")) {
					Thread.sleep(5000);
					driver.navigate().refresh();
				}
				
				/*Thread.sleep(5000);
				driver.navigate().refresh();
				if (objOrderFlow.fn_getStatusCategory(driver, webWait).getText().equalsIgnoreCase("In Authoring")) {
					Log.debug("DEBUG: status change to In Authoring.");
				} else {
					Thread.sleep(5000);
					driver.navigate().refresh();
				}*/
				assertEquals(objOrderFlow.fn_getStatusCategory(driver, webWait).getText(), "In Authoring");
				assertEquals(objOrderFlow.fn_getStatus(driver, webWait).getText(), "Generated");
				
			}
			catch (Exception ExceptionActivateGMSA) {
				Log.error("ERROR: Unable to Generate  MSA with exception reported as: "+ExceptionActivateGMSA.getMessage());
		        StringWriter stack = new StringWriter();
		        ExceptionActivateGMSA.printStackTrace(new PrintWriter(stack));
		        Log.debug("DEBUG: The exception in fn_ActivateGMSA method is: "+stack);
		       
			}
			Log.debug("DEBUG: Execution of function 'fn_ActivateGMSA' Completed.");	
						
	}
	
	
	/*
	Author: Suhas Thakre
	Function fn_SendSignatureGMSA: Send GMSA agreement for esignature.
	Inputs: 
	Outputs: Boolean response to check success or failure of function .
	 */
	public void fn_SendSignatureGMSA(WebDriver driver,WebDriverWait webWait)throws Exception
	{
		//TODO:: Send GMSA agreement for esignature.  
		/************************************************************************************************/
			//Step 1: Open GMSA agreement page. 
			//Step 2: Send GMSA for Review.
			//Step 3: Send GMSA for esignature.
			
		
		/************************************************************************************************/
		
		
		Account objacctpage = new Account();
		OrderFlow objOrderFlow = new OrderFlow();
		AfterApproval objAfterPO = new AfterApproval();
		Proposal objProposal= new Proposal();
		Utils objUtils = new Utils();
		String strUniqueKey = "";
		String strNewproposalName = EnvironmentDetails.mapGlobalVariable.get("strNewproposalName").toString();
		
		try
		{
			Log.debug("DEBUG: Execution of function 'fn_SendSignatureGMSA' Started.");
				//Step 1
				objacctpage.fn_setSearchField(driver, webWait).sendKeys(strNewproposalName); 
				objacctpage.fn_clickSearchBtn(driver, webWait).click();
				Thread.sleep(1000);
				driver.findElement(By.partialLinkText(strNewproposalName)).click();
				Thread.sleep(3500);
				objProposal.fn_clickAgreementsLink(driver, webWait, strMSA_AgrmntName).click();
				
				
				for (int i=0; i<=4;i++ ){
				driver.navigate().refresh();
				Thread.sleep(2000);
				}
				
				//TODO:: Capturing the StatusCategory and Status of Agreement		
				String strStatusCategory=objOrderFlow.fn_getStatusCategory(driver, webWait).getText();
				if (strStatusCategory.contentEquals("In Authoring"))
					Log.info("Status Category of the MSA Agrement status is changed to ::"+strStatusCategory);
				else
					Log.info("Status Category of the MSA Agrement status is not changed to"+strStatusCategory);
				
				String strStatus=objOrderFlow.fn_getStatus(driver, webWait).getText();
				if (strStatus.contentEquals("Generated"))
					Log.info("Status of the MSA Agrement is changed to ::"+strStatus);
				else
					Log.info("Status of the MSA Agrement  is not changed to"+strStatus);
				
				//Step 2
				objOrderFlow.fn_clickSendForReview(driver, webWait).click();
				Thread.sleep(10000);
				//Selecting the REquired Docs for MSA Agrmnt 
				objOrderFlow.fn_clickSelectDocForMSA(driver, webWait).click();
				Thread.sleep(2000);
				objAfterPO.fn_clickMSANextButton(driver, webWait).click();
				Thread.sleep(5000);
				objOrderFlow.fn_clickEmailTemplate(driver, webWait).click();
				Thread.sleep(30000);
				WebElement elemntNextBtn= objOrderFlow.fn_clickNextBtnEmailTemplate(driver, webWait);
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elemntNextBtn);
				elemntNextBtn.click();
				Thread.sleep(10000);
			    //Send email with sungardas template
			    objUtils.fn_HandleLookUpWindow(driver, webWait,"Reports To Lookup (New Window)","ApptusAuto");
			    Thread.sleep(3500);
			    objOrderFlow.fn_clickEmailSendBtn(driver, webWait).click();
			    //Docusignprocess
			    
			    //Step 3
			    objOrderFlow.fn_clickESignatureBtn(driver, webWait).click();
			    objOrderFlow.fn_clickAddRecipientsBtn(driver, webWait).isEnabled();
			    objOrderFlow.fn_clickAddRecipientsBtn(driver, webWait).click();
			    Thread.sleep(1000);
			    objOrderFlow.fn_setNameForRecipient(driver, webWait).sendKeys("madhavi");
			    Thread.sleep(500);
			    objOrderFlow.fn_setEmailForRecipient(driver, webWait).sendKeys(EnvironmentDetails.mapEnvironmentDetails.get("Outlook_UserID"));
			    Thread.sleep(500);
			    objOrderFlow.fn_clickFinalizeDocuSignBtn(driver, webWait).click();
			    new WebDriverWait(driver,240).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//span[text()='Processing eSignature request']")));
			    objOrderFlow.fn_clickSendBtn(driver, webWait).isEnabled();
			    strUniqueKey = driver.getCurrentUrl().trim();
			    strUniqueKey = strUniqueKey.replace("https://appdemo.docusign.com/prepare/", "");
			    strUniqueKey = strUniqueKey.replace("/add-fields", "");
			    String[] arrUniqueKey = strUniqueKey.split("-");
			    //Save GMSA email request UniqueKey in global variable map 
			    EnvironmentDetails.mapGlobalVariable.put("strGMSAUniqueKey", arrUniqueKey[0].trim());
			    Log.debug("DEBUG: GMSA email request UniqueKey save in global variable map with key 'UniqueKey' is: "+arrUniqueKey[0].trim());
			    Thread.sleep(5000);
			    objOrderFlow.fn_clickSendBtn(driver, webWait).click();
			    
			    try {
			    	objOrderFlow.fn_clickWithoutFieldsBtn(driver, new WebDriverWait(driver,15)).isEnabled();
			    	objOrderFlow.fn_clickWithoutFieldsBtn(driver, new WebDriverWait(driver,15)).click();
				} catch (Exception e) {
					Log.debug("DEBUG: Send Without Fields button not present.");
				}
			//    objOrderFlow.fn_clickSendForEsignatureBtn(driver, webWait).click();
			  //  Thread.sleep(60000);
			    
			}
			catch (Exception ExceptionSendSignatureGMSA) {
				Log.error("ERROR: Unable to Generate  MSA with exception reported as: "+ExceptionSendSignatureGMSA.getMessage());
		        StringWriter stack = new StringWriter();
		        ExceptionSendSignatureGMSA.printStackTrace(new PrintWriter(stack));
		        Log.debug("DEBUG: The exception in fn_SendSignatureGMSA method is: "+stack);
		       
			}
			Log.debug("DEBUG: Execution of function 'fn_SendSignatureGMSA' Completed.");	
	}

}
