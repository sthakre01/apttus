
/*
 Author     		:	Suhas Thakre
 Class Name			: 	MSOutlookApproval 
 Purpose     		: 	Purpose of this file is :
						1. To perform read, reply action related to outlook 

 Date       		:	06/012/2017 
 Modified Date		:	06/012/2017	
 Modified By		: 	
 Version Information:	Version 1.0
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/


package appModules;

import java.awt.Toolkit;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.swing.JDialog;
import javax.swing.JLabel;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.OutlookApproval;
import pageObjects.SalesForce.Proposal;
import pageObjects.SalesForce.workbench;
import utility.API_SOAP;
import utility.EnvironmentDetails;
import utility.ExcelHandler;
import utility.Log;
import utility.Utils;
import utility.VerificationMethods;


public class MSOutlookApproval extends VerificationMethods{
	
	
	/*
	Author: Suhas Thakre
	Function fn_OutlookLogin: User login to Microsoft Outlook 365.
	Inputs: 
	Outputs: Boolean response to check success or failure of function .
	 */
	public Boolean fn_OutlookLogin(WebDriver driver,WebDriverWait webWait) throws Exception{
		
		//TODO:: login to Microsoft Outlook 365
		/************************************************************************************************/
			//Step 0: Open new window tab for outlook.
			//Step 1: Click Use Another Account link.
			//Step 2: Enter password first to avoid auto login.
			//Step 3: Enter User name.
			//Step 4: Click Sign In button. 
		/************************************************************************************************/
		OutlookApproval objOutlook = new OutlookApproval();
		//EnvironmentDetails objEnv = (EnvironmentDetails) EnvironmentDetails.mapGlobalVariable.get("objEnv");
		Boolean blnOutlookLogin = false;
		String strEmailWindow = null;
		String strApplWindow = null;
		
		
		//Get application window id
		strApplWindow = driver.getWindowHandle();
		EnvironmentDetails.mapGlobalVariable.put("strApplWindowID", strApplWindow);
		Log.debug("DEBUG: Appication window id saved in global variable map with key name strApplWindowID.");
		//Step 0
		/*Robot robot = new Robot();
	    robot.keyPress(KeyEvent.VK_CONTROL);
	    robot.keyPress(KeyEvent.VK_T);
	    robot.keyRelease(KeyEvent.VK_CONTROL);
	    robot.keyRelease(KeyEvent.VK_T);*/
		((JavascriptExecutor)driver).executeScript("window.open('"+EnvironmentDetails.mapEnvironmentDetails.get("Outlook_URL")+"','_blank')");  
		Thread.sleep(2000);
	    
	    //Switch to outlook window 
	    Set<String> strWindowHandles = driver.getWindowHandles();
	    strWindowHandles.remove(strApplWindow);
	    strEmailWindow = strWindowHandles.iterator().next();
	    
	    //Step 2
	    driver.switchTo().window(strEmailWindow);
	    //driver.navigate().to(EnvironmentDetails.mapEnvironmentDetails.get("Outlook_URL"));
	    Thread.sleep(3500);
	    strEmailWindow = driver.getWindowHandle();
	    EnvironmentDetails.mapGlobalVariable.put("strEmailWindowID", strEmailWindow);
		Log.debug("DEBUG: Outlook window id saved in global variable map with key name strEmailWindowID.");
	    
		//webWait = new WebDriverWait(driver,15);
		try { 
			//Step 1
			objOutlook.fn_clickAnotherAccountLink(driver, new WebDriverWait(driver,30)).click();
			//Step 2
			objOutlook.fn_setPassword(driver, new WebDriverWait(driver,15)).sendKeys(EnvironmentDetails.mapEnvironmentDetails.get("Outlook_Password"));
			//Step 3
			objOutlook.fn_setEmail(driver, new WebDriverWait(driver,15)).sendKeys(EnvironmentDetails.mapEnvironmentDetails.get("Outlook_UserID"));
			//Step 4
			//objOutlook.fn_clickSignInBtn(driver, webWait).click();
			objOutlook.fn_setEmail(driver, new WebDriverWait(driver,15)).submit();
			Thread.sleep(3500);
			
		} catch (Exception ExceptionNOAcctLink) {
			
			
			try {
				//if  Use Another Account link is not present and directly login page displayed. 
				objOutlook.fn_setPassword(driver, new WebDriverWait(driver,15)).sendKeys(EnvironmentDetails.mapEnvironmentDetails.get("Outlook_Password"));
				objOutlook.fn_setEmail(driver, new WebDriverWait(driver,15)).sendKeys(EnvironmentDetails.mapEnvironmentDetails.get("Outlook_UserID"));
				objOutlook.fn_setEmail(driver, new WebDriverWait(driver,15)).submit();
				Thread.sleep(3500);
			} catch (Exception e) {
				Log.debug("DEBUG: User Login Already.");
			}
		}
		
	
		
		
		try{
		new WebDriverWait(driver,15).until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='password']"))).sendKeys("Sonu@4321");
		driver.findElement(By.xpath("//a[@title='Sign On']")).click();;
		Thread.sleep(3500);
		}catch (Exception e)
		{
			Log.debug("DEBUG: Not Required VPN.");
		}
		
		webWait = new WebDriverWait(driver,60);
		Thread.sleep(5000);
		blnOutlookLogin = objOutlook.fn_clickMailListLink(driver, webWait).isDisplayed();
		
		
		return blnOutlookLogin;
		
	}
	
	
	/*
	Author: Suhas Thakre
	Function fn_OutlookSignout: Sign out from Microsoft Outlook 365.
	Inputs: 
	Outputs: Boolean response to check success or failure of function .
	 */
	public Boolean fn_OutlookSignout(WebDriver driver,WebDriverWait webWait) throws Exception{
		
		//TODO:: Sign out from Microsoft Outlook 365.
		/************************************************************************************************/
			//Step 1: Click on Account link image.
			//Step 2: Click on Sign out button.
			//Step 3: Close the browser 
		/************************************************************************************************/
		OutlookApproval objOutlook = new OutlookApproval();
		Boolean blnOutlookSignout = false;
		String strEmailWindow = null;
		
		strEmailWindow = EnvironmentDetails.mapGlobalVariable.get("strEmailWindowID").toString();
		driver.switchTo().window(strEmailWindow);
		try { 
			//Step 1
		    objOutlook.fn_clickSignoutImg(driver, webWait).click();
			Thread.sleep(3500);
				if (objOutlook.fn_clickSignoutBtn(driver, webWait).isDisplayed())
					blnOutlookSignout=true;	
			//Step 2
			objOutlook.fn_clickSignoutBtn(driver, webWait).click();
			Thread.sleep(3500);
			driver.close();
			
		} catch (Exception ExceptionSignout) {
			Log.error("ERROR: Unable to Sign Out from Outlook 365 with exception reported as: "+ExceptionSignout.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionSignout.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_OutlookSignout method is: "+stack);
		}
		
		return blnOutlookSignout;
		
	}
	
	/*
	Author: Suhas Thakre
	Function fn_ApprovalProcess: Approve the mail Trigger from Apttus.
	Inputs: 
	Outputs: Boolean response to check success or failure of function .
	 */
	public Boolean fn_ApprovalProcess(WebDriver driver,WebDriverWait webWait) throws Exception{
		
		//TODO:: Approve the e-mail Trigger from Apttus.
		/************************************************************************************************/
			//Step 1: Open new window tab for outlook.
			//Step 2: Login to outlook 365.
			//Step 3: Search email with Proposal id. 
			//Step 4: Select each email and approve the same.
			//Step 5: Sign Out from Outlook 365.
		/************************************************************************************************/
		OutlookApproval objOutlook = new OutlookApproval();
		Utils objUtils = new Utils();
		Boolean blnApprovalProcess = false;
		String strApplWindow = null;
		String strNewproposalName = EnvironmentDetails.mapGlobalVariable.get("strNewproposalName").toString();
		
		JDialog jdlgPrintFrame  = fn_WinApplet(driver, webWait, "Approving proposal through Email Please wait.");
		Thread.sleep(120000);
		
		//Step 2
		fn_OutlookLogin(driver, webWait);
		
		driver.navigate().to(EnvironmentDetails.mapEnvironmentDetails.get("OutlookApttusMailBox_URL"));
		Thread.sleep(5000);
		
		try {
			objOutlook.fn_clickMailListLink(driver, webWait).isDisplayed();
		} catch (Exception e) {
			driver.navigate().refresh();
			objOutlook.fn_clickMailListLink(driver, webWait).isDisplayed();
		}
		
		
		try { 
				
			
        	objOutlook.fn_clickMailListLink(driver, webWait).click();
			Thread.sleep(3500);    // wait for inbounds mail
			//Step 3 
			objOutlook.fn_clickSearchBar(driver, webWait).click();
			objOutlook.fn_setSearchBar(driver, webWait).sendKeys(strNewproposalName);;
			Thread.sleep(3500);
			objOutlook.fn_clickSearchBtn(driver, webWait).click();
			Thread.sleep(3500);
			//Select all folder
			objOutlook.fn_clickAllfoldersBtn(driver, webWait).click();
			Thread.sleep(3500);
			
			
			//refresh mail box 4 times to get emails
			Integer intMailCount = 0;
			while (true) {
				
				if (intMailCount > 4) {
					break;
				}
				try {
					intMailCount++;
					driver.findElement(By.xpath("//div[@aria-label='Mail list']//div[@role='option']")).isDisplayed();
					break;
				} catch (Exception ExceptionMailNotFound) {
					objOutlook.fn_clickSearchBtn(driver, webWait).click();
					Thread.sleep(30000);
				}
			}
			
			//Get search mail list
			List<WebElement> lstSearchEMail = driver.findElements(By.xpath("//div[@aria-label='Mail list']//div[@role='option']"));
			//System.out.println("================================== Mail Count ========="+lstSearchMail.size());
				for (WebElement webEMail : lstSearchEMail) {
					//Step 4 
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", webEMail);
					webEMail.click();
					Thread.sleep(3500);
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", objOutlook.fn_clickReplyallBtn(driver, webWait));
					objOutlook.fn_clickReplyallBtn(driver, webWait).isEnabled();
					try {
						
						//((JavascriptExecutor) driver).executeScript("document.getElementById('"+objOutlook.fn_clickReplyallBtn(driver, webWait).getAttribute("id")+"').focus();");
						objOutlook.fn_clickReplyallBtn(driver, webWait).click();
					} catch (Exception e) {
						
						new WebDriverWait(driver,60).until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".o365cs-notifications-newMailPersonalImage")));
						Thread.sleep(3500);
						//objOutlook.fn_clickReplyallBtn(driver, webWait).sendKeys(Keys.ESCAPE);
						objOutlook.fn_clickReplyallBtn(driver, webWait).click();
					}
					Thread.sleep(3500);
					objOutlook.fn_setMsgBody(driver, webWait).sendKeys("APPROVE");
					
					//TODO:: Capture Screenshot for Approval details 
					objUtils.fn_takeScreenshot(driver);
					
					//Thread.sleep(3500);
					objOutlook.fn_clickSendBtn(driver, webWait).click();
					Thread.sleep(3500);
					
				}
			
				
			//To capture send email
			objOutlook.fn_clickExithBtn(driver, webWait).click();
			Thread.sleep(3500);
			objOutlook.fn_clickSentItemsBtn(driver, webWait).click();
			Thread.sleep(3500);
			//TODO:: Capture Screenshot After Approval email send details 
			objUtils.fn_takeScreenshot(driver);
			Thread.sleep(3500);
			//Reset Search option to all folder
			objOutlook.fn_clickMailListLink(driver, webWait).click();
			Thread.sleep(3500);
			objOutlook.fn_clickSearchBar(driver, webWait).click();
			objOutlook.fn_setSearchBar(driver, webWait).sendKeys(strNewproposalName);;
			Thread.sleep(3500);
			objOutlook.fn_clickSearchBtn(driver, webWait).click();
			Thread.sleep(3500);
			objOutlook.fn_clickMailListLink(driver, webWait).click();
			Thread.sleep(3500);
			//Select all folder
			objOutlook.fn_clickAllfoldersBtn(driver, webWait).click();
			Thread.sleep(3500);
			
			
			blnApprovalProcess = true;
		} catch (Exception ExceptionApprove) {
			Log.error("ERROR: Unable to Approve email with exception reported as: "+ExceptionApprove.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionApprove.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_ApprovalProcess method is: "+stack);
		}
		
		driver.navigate().to(EnvironmentDetails.mapEnvironmentDetails.get("Outlook_URL"));
		//Step 5
		fn_OutlookSignout(driver, webWait);
		strApplWindow = EnvironmentDetails.mapGlobalVariable.get("strApplWindowID").toString();
		driver.switchTo().window(strApplWindow);
		jdlgPrintFrame.dispose();
		return blnApprovalProcess;
		
	}
	
	
	/*
	Author: Suhas Thakre
	Function fn_OutlookDocuSignProcess: Provide esignature to document received from Apttus.
	Inputs: 
	Outputs: Boolean response to check success or failure of function .
	 */
	public Boolean fn_OutlookDocuSignProcess(WebDriver driver,WebDriverWait webWait, String strDocuSignType, String strUniqueKey ) throws Exception{
		
		//TODO:: Approve the email Trigger from Apttus.
		/************************************************************************************************/
			//Step 1: Open new window tab for outlook.
			//Step 2: Login to outlook 365.
			//Step 3: Search email for eSignatures. 
			//Step 4: Select Document to perform e signature.
			//Step 5: Accept and Continue to sign document.
			//Step 6: Add Signature to document.
			//Step 7: Sign Out from Outlook 365.
		/************************************************************************************************/
		OutlookApproval objOutlook = new OutlookApproval();
		Utils objUtils = new Utils();
		Boolean blnDocuSignProcess = false;
		String strApplWindow = null;
		String strEmailWindow = null;
		String strReviewWindow = null;
		String strKey = EnvironmentDetails.mapGlobalVariable.get(strUniqueKey).toString();
		
		JDialog jdlgPrintFrame  = fn_WinApplet(driver, webWait,"Signing Document through Email Please wait.");
		Thread.sleep(120000);	
		
		
		//Step 2
		fn_OutlookLogin(driver, webWait);
		try { 
			
			objOutlook.fn_clickMailListLink(driver, webWait).click();
			Thread.sleep(3500);
			//Step 3 
			objOutlook.fn_clickSearchBar(driver, webWait).click();
			objOutlook.fn_setSearchBar(driver, webWait).sendKeys("Request for eSignatures. Please review and sign.");;
			Thread.sleep(3500);
			objOutlook.fn_clickSearchBtn(driver, webWait).click();
			Thread.sleep(3500);
			objOutlook.fn_clickMailListLink(driver, webWait).click();
			Thread.sleep(6000);
			
			//TODO:: Select Correct request for esignature email
			fn_SelRequestMail(driver, webWait, strKey);
		
			
			//TODO:: Capture Screenshot for Approval details
			//objUtils.takeScreenshot(driver, EnvironmentDetails.strTestCaseName);
			
			//Step 4
			objOutlook.fn_clickReviewBtn(driver, webWait).click();
			
			//Capture review window id
			Thread.sleep(5000);		
		      for (String handle : driver.getWindowHandles()) {
	                 driver.switchTo().window(handle);}		
			strReviewWindow = driver.getWindowHandle();
			
			new WebDriverWait(driver,120).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[@class='spinner']")));
			objOutlook.fn_ReviewText(driver, new WebDriverWait(driver,90)).isDisplayed();
			objOutlook.fn_clickAgreeAndAccept(driver, webWait).isEnabled();
			Thread.sleep(5000);
			objOutlook.fn_clickAgreeAndAccept(driver, webWait).click();
			
			if (!objOutlook.fn_clickAgreeAndAccept(driver, webWait).isSelected()) {
				objOutlook.fn_clickAgreeAndAccept(driver, webWait).click();
			} else {
				Log.debug("DEBUG: I agree checkbox is selected.");
			}
			
			//Step 5 
		/*		try {
					Thread.sleep(5000);
					objOutlook.fn_clickAgreeAndAccept(driver, webWait).isDisplayed();
					objOutlook.fn_clickAgreeAndAccept(driver, webWait).click(); }          		
				catch(Exception excpEnvironment1){
					Log.debug("DEBUG: Continue Button is not present in the Docusign of Order.");}	*/	
			Thread.sleep(5000);
			objOutlook.fn_clickContinueBtn(driver, webWait).isEnabled();
			objOutlook.fn_clickContinueBtn(driver, webWait).click();
			Thread.sleep(5000);
			
			if (strDocuSignType.equalsIgnoreCase("MSA")) {
				
				objOutlook.fn_clickNavigateBtn(driver, webWait).isEnabled();
				objOutlook.fn_clickNavigateBtn(driver, webWait).click();
				Thread.sleep(5000);
				objOutlook.fn_clickSignBtn(driver, webWait).isDisplayed();
				WebElement svgObject = objOutlook.fn_clickSignBtn(driver, webWait);
				Actions builder = new Actions(driver);
				builder.click(svgObject).build().perform();
				//objGmail.fn_clickSignBtn(driver, webWait).click();
				Thread.sleep(5000);
				for (String handle : driver.getWindowHandles()) {
	                 driver.switchTo().window(handle);}	
				
				//handle adopt and sign button
					try {
						objOutlook.fn_clickAdoptandSign(driver, webWait).click();
					} catch (Exception e) {
						Log.debug("DEBUG: Adopt and Sign button not available.");
					}
				Thread.sleep(3500);
					//handle Not available object
					try {
						objOutlook.fn_setPrintName(driver, new WebDriverWait(driver,10)).sendKeys("Test");
						objOutlook.fn_setPrintTitle(driver, new WebDriverWait(driver,10)).sendKeys("Test");
						
					} catch (Exception e) {
						Log.debug("DEBUG: Print Name and Print Title textbox not available.");
					}
				
				Thread.sleep(3500);
				
			} 
			else if (strDocuSignType.equalsIgnoreCase("Order")) {
				
				//Step 6
				driver.switchTo().frame(driver.findElement(By.id("signingIframe")));		
				objOutlook.fn_clickDragFrom(driver, webWait).click();
				WebElement webElementDragFrom = objOutlook.fn_clickDragFrom(driver, webWait);
					if (webElementDragFrom.isDisplayed())
						Log.debug("DEBUG: The signing frame is identified and drag from button is displayed.");
					else
						Log.debug("ERROR: The signing frame is identified and drag from button is not displayed.");
				WebElement webElementDragTo = objOutlook.fn_clickDragTo(driver, webWait);
				Actions builder = new Actions(driver);
				builder.dragAndDrop(webElementDragFrom, webElementDragTo).build().perform();
				Thread.sleep(3500);
				
				for (String handle : driver.getWindowHandles()) {
	                 driver.switchTo().window(handle);}	    
				   Thread.sleep(10000);   
				   objOutlook.fn_clickAdoptandSign(driver, webWait).click();   	   
				   driver.switchTo().defaultContent(); 
			}
			
			
			
		   
		   //Finish the e signature 
		   driver.switchTo().window(strReviewWindow);  
		   Thread.sleep(5000);
		   
		   
		   //TODO:: Capture Screenshot for Document signed  
		   objUtils.fn_takeScreenshot(driver);
		   
		   objOutlook.fn_clickFinish(driver, webWait).click();
		   driver.close();
		   Thread.sleep(5000);
		   strEmailWindow = EnvironmentDetails.mapGlobalVariable.get("strEmailWindowID").toString();
			driver.switchTo().window(strEmailWindow);
		   driver.switchTo().window(strEmailWindow);
		   
		   blnDocuSignProcess = true;
			
		} catch (Exception ExceptionESignature) {
			Log.error("ERROR: Unable to Provide e-signature with exception reported as: "+ExceptionESignature.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionESignature.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_OutlookDocuSignProcess method is: "+stack);
		}
		//Step 7
		fn_OutlookSignout(driver, webWait);
		strApplWindow = EnvironmentDetails.mapGlobalVariable.get("strApplWindowID").toString();
		driver.switchTo().window(strApplWindow);
		jdlgPrintFrame.dispose();
		return blnDocuSignProcess;
		
	}

	
	/*
	Author: Suhas Thakre
	Function fn_SelRequestMail: Select request for esignature email.
	Inputs: 
	Outputs: Boolean response to check success or failure of function .
	 */
	public  void fn_SelRequestMail(WebDriver driver, WebDriverWait webWait, String strKey ) throws Exception{
		//TODO:: Select request for esignature email.
				/************************************************************************************************/
					//Step 1: Search for email with unique key. 
					//Step 2: if not found move to next mail.
					
				/************************************************************************************************/
		OutlookApproval objOutlook = new OutlookApproval();
		String strDocURL = "";
		Boolean blnMail = false;
		try{	
			 
			//Step 1: Search for email with unique key. 
			for (int i = 1; i < 10; i++) {
				
				objOutlook.fn_clickSignMailLink(driver, webWait, i).click();
				String strMailSub= objOutlook.fn_clickSignMailLink(driver, webWait, i).getText().trim();
				
				if (strMailSub.equalsIgnoreCase("Request for eSignatures")) {
					
					objOutlook.fn_clickReviewBtn(driver, webWait).isDisplayed();
					Thread.sleep(4000);
					strDocURL = objOutlook.fn_clickReviewBtn(driver, webWait).getAttribute("href");
					if (strDocURL.contains(strKey)) {
						Log.debug("DEBUG: Request for e signature found at position: "+i);
						blnMail = true;
						break;
					}
				}
				
				
			}
			
			if (blnMail == false) {
				Log.debug("DEBUG: Request for e signature not found till position: 5");
			}
			
		} catch (Exception ExceptionSelRequestMail) {
			Log.error("ERROR: Unable to Select request for esignature email with exception reported as: "+ExceptionSelRequestMail.getMessage());
		    StringWriter stack = new StringWriter();
		    ExceptionSelRequestMail.printStackTrace(new PrintWriter(stack));
		    Log.debug("DEBUG: The exception in fn_SelRequestMail method is: "+stack);
		}
	}
	
	public JDialog fn_WinApplet(WebDriver driver,WebDriverWait webWait, String strMessage ) throws Exception{
		String strPrintFrameMsg = "<html><div align='center'><font size='5' face='SANS_SERIF'>"+strMessage+"</font>"
                + "<br><br><br><font size='20' face='SANS_SERIF' color='red'>Please do not touch the browser; can cause TC to fail !</font>"
                + "<br><br><br><br><br><br><br><font size='5' face='SANS_SERIF'>Please Note: This Message Box will be auto closed !</font></div></html>";
		JDialog jdlgPrintFrame = new JDialog();
		jdlgPrintFrame.setTitle("WARNING !");
		jdlgPrintFrame.setModal(false);
		jdlgPrintFrame.add(new JLabel(strPrintFrameMsg, JLabel.CENTER));
		jdlgPrintFrame.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		jdlgPrintFrame.setAlwaysOnTop(true);
		jdlgPrintFrame.setMinimumSize(Toolkit.getDefaultToolkit().getScreenSize());
		jdlgPrintFrame.setResizable(false);
		jdlgPrintFrame.pack();
		jdlgPrintFrame.setVisible(true);
		//Thread.sleep(60000);
		//jdlgPrintFrame.dispose();
		return jdlgPrintFrame;
		// 

	}
	
	
	
	/*
	Author: Suhas Thakre
	Function fn_ApprovalProcessApplication: Approve proposal from application.
	Inputs: 
	Outputs: Boolean response to check success or failure of function .
	 */
	public Boolean fn_ApprovalProcessApplication(WebDriver driver,WebDriverWait webWait) throws Exception{
		
		//TODO:: Sign out from Microsoft Outlook 365.
		/************************************************************************************************/
			//Step 1: Open My Approvals page.
			//Step 2: Verify All approval selected.
			//Step 3: Approve proposal. 
			//Step 4: Verify Approval status.
		/************************************************************************************************/
		Proposal objProposal= new Proposal();
		Boolean blnApproved = false;
		List<WebElement> elesApprovalscheckbox = new ArrayList<WebElement>();
		try { 
			
			//Step 1: Open My Approvals page.
			objProposal.fn_clickMyApprovalsbtn(driver, webWait).isEnabled();
			objProposal.fn_clickMyApprovalsbtn(driver, webWait).click();
			objProposal.fn_clickAllApprovalsbtn(driver, webWait).isEnabled();
			objProposal.fn_clickAllApprovalsbtn(driver, webWait).click();
			
			//Step 2: Verify All approval selected.
			elesApprovalscheckbox = objProposal.fn_getApprovalscheckbox(driver, webWait);
			for (WebElement eleApprovalscheckbox : elesApprovalscheckbox) {
				if (eleApprovalscheckbox.isSelected()) {
					Log.debug("DEBUG: Approval Selected.");
				} else {
					eleApprovalscheckbox.click();
				}
			}
			
			//Step 3: Approve proposal.
			objProposal.fn_clickApprovebtn(driver, webWait).isEnabled();
			objProposal.fn_clickApprovebtn(driver, webWait).click();
			objProposal.fn_setApproveComment(driver, webWait).isDisplayed();
			objProposal.fn_setApproveComment(driver, webWait).sendKeys("APPROVE");
			objProposal.fn_clickSaveCommentbtn(driver, webWait).click();
			objProposal.fn_clickReturnbtn(driver, webWait).isEnabled();
			objProposal.fn_clickReturnbtn(driver, webWait).click();
			objProposal.fn_verifyApprovalStatus(driver, webWait).isDisplayed();
			driver.navigate().refresh();
			
			//Step 4: Verify Approval status.
			assertEquals(objProposal.fn_verifyApprovalStatus(driver, webWait).getText().trim(), "Approved");
			
			
			
			blnApproved = true;
			
		} catch (Exception ExceptionSignout) {
			Log.error("ERROR: Unable to Approve proposal with exception reported as: "+ExceptionSignout.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionSignout.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_ApprovalProcessApplication method is: "+stack);
		}
		
		return blnApproved;
		
	}
	
	
	/*
	Author: Suhas Thakre
	Function fn_WorkbenchUpdate: Execute Update using workbench.
	Inputs: 
	Outputs: Boolean response to check success or failure of function .
	 */
	public void fn_WorkbenchUpdate(WebDriver driver,WebDriverWait webWait , String strID, String strObject)throws Exception
	{
	
		//TODO:: Execute Update using workbench.  
		/************************************************************************************************/
			//Step 1: Open workbench page. 
			//Step 2: Switch to workbench window
			//Step 3: Login to workbench.
			//Step 4: Update SOQL.
			//Step 5: Get Result.

		/************************************************************************************************/
		
		workbench objworkbench = new workbench();
		List<Map<String, String>> lisMapOutputData = new ArrayList<>();
	    List<List<Map<String, String>>> lstlstmapOutputData = new ArrayList<>();
	    
		try {
		
			//Step 1: Open workbench page.
			((JavascriptExecutor)driver).executeScript("window.open('https://workbench.developerforce.com/query.php')");  
			Thread.sleep(2000);
			
			//Step 2: Switch to workbench window 
			String strApplWindow = driver.getWindowHandle();
		    Set<String> strWindowHandles = driver.getWindowHandles();
		    for (String Handle : strWindowHandles) {
		    	driver.switchTo().window(Handle);
			}
		    
		    //Handle re login
		    try {
		    	//Step 3: Login to workbench.
			    objworkbench.fn_clicktermsAccepted(driver, new WebDriverWait(driver,3)).isEnabled();
			    objworkbench.fn_selEnvironment(driver, webWait).selectByVisibleText("Sandbox");;
			    objworkbench.fn_clicktermsAccepted(driver, webWait).click();
			    objworkbench.fn_clickLoginbtn(driver, webWait).click();
			} catch (Exception e) {
				 Log.debug("DEBUG: Already login to workbench");
			}
		     
		    
		    //Step 4: Update SOQL.
		    objworkbench.fn_clickQuerybtn(driver, webWait).isEnabled();
		    objworkbench.fn_clickDatalink(driver, webWait).isEnabled();
		    objworkbench.fn_clickDatalink(driver, webWait).click();
		    objworkbench.fn_clickUpdatelink(driver, webWait).isEnabled();
		    objworkbench.fn_clickUpdatelink(driver, webWait).click();
		    objworkbench.fn_clickNextlink(driver, webWait).isEnabled();
		    objworkbench.fn_selObjectType(driver, webWait).selectByVisibleText(strObject);
		    objworkbench.fn_clicksingleRecord(driver, webWait).click();
		    objworkbench.fn_setId(driver, webWait).clear();
		    objworkbench.fn_setId(driver, webWait).sendKeys(strID);
		    objworkbench.fn_clickNextlink(driver, webWait).click();
		    objworkbench.fn_clickConfirmUpdatebtn(driver, webWait).isEnabled();
		    objworkbench.fn_UpdateField(driver, webWait, "Apttus__Status_Category__c").clear();
		    objworkbench.fn_UpdateField(driver, webWait, "Apttus__Status_Category__c").sendKeys("In Signatures");
		    objworkbench.fn_UpdateField(driver, webWait, "Apttus__Status__c").clear();
		    objworkbench.fn_UpdateField(driver, webWait, "Apttus__Status__c").sendKeys("Fully Signed");
		    objworkbench.fn_clickConfirmUpdatebtn(driver, webWait).click();
		    objworkbench.fn_clickDownload(driver, webWait).isEnabled();
		    
		    driver.close();
		    driver.switchTo().window(strApplWindow);
		    
		}
		catch (Exception ExceptionWorkbenchSOQL) {
					Log.error("ERROR: Unable to Update using workbench with exception reported as: "+ExceptionWorkbenchSOQL.getMessage());
		            StringWriter stack = new StringWriter();
		            ExceptionWorkbenchSOQL.printStackTrace(new PrintWriter(stack));
		            Log.debug("DEBUG: The exception in fn_WorkbenchUpdate method is: "+stack);
		}
		
	     
	}
	
	
	/*
	Author: Suhas Thakre
	Function fn_WorkbenchSOQL: Execute SOQL using workbench.
	Inputs: 
	Outputs: Boolean response to check success or failure of function .
	 */
	public List<List<Map<String, String>>> fn_WorkbenchSOQL(WebDriver driver,WebDriverWait webWait , String strSOQL)throws Exception
	{
	
		//TODO:: Execute SOQL using workbench.  
		/************************************************************************************************/
			//Step 1: Open workbench page. 
			//Step 2: Switch to workbench window
			//Step 3: Login to workbench.
			//Step 4: Execute SOQL.
			//Step 5: Get Result.

		/************************************************************************************************/
		
		workbench objworkbench = new workbench();
		List<Map<String, String>> lisMapOutputData = new ArrayList<>();
	    List<List<Map<String, String>>> lstlstmapOutputData = new ArrayList<>();
	    
		try {
		
			//Step 1: Open workbench page.
			((JavascriptExecutor)driver).executeScript("window.open('https://workbench.developerforce.com/query.php')");  
			Thread.sleep(2000);
			
			//Step 2: Switch to workbench window 
			String strApplWindow = driver.getWindowHandle();
		    Set<String> strWindowHandles = driver.getWindowHandles();
		    for (String Handle : strWindowHandles) {
		    	driver.switchTo().window(Handle);
			}
		    
		    //Handle re login
		    try {
		    	//Step 3: Login to workbench.
			    objworkbench.fn_clicktermsAccepted(driver, new WebDriverWait(driver,3)).isEnabled();
			    objworkbench.fn_selEnvironment(driver, webWait).selectByVisibleText("Sandbox");;
			    objworkbench.fn_clicktermsAccepted(driver, webWait).click();
			    objworkbench.fn_clickLoginbtn(driver, webWait).click();
			} catch (Exception e) {
				 Log.debug("DEBUG: Already login to workbench");
			}
		    
		    //Step 4: Execute SOQL.
		    objworkbench.fn_clickQuerybtn(driver, webWait).isEnabled();
		    objworkbench.fn_setSOQL(driver, webWait).clear();
		    objworkbench.fn_setSOQL(driver, webWait).sendKeys(strSOQL);
		    objworkbench.fn_clickQuerybtn(driver, webWait).click();
		    
		    //Step 5: Get Result.
		    objworkbench.fn_getQueryResults(driver, webWait).isDisplayed();
		    WebElement QueryResultsTable = objworkbench.fn_QueryResultsTable(driver, webWait);
		    List<WebElement> ResultRows = QueryResultsTable.findElements(By.tagName("tr"));
		    List<WebElement> TotalCol = ResultRows.get(1).findElements(By.tagName("td"));
		    
		    for (int i = 0; i < ResultRows.size()-1; i++) {
		    	Map<String, String> mapOutputData = new HashMap<String, String>();
		    	for (int j = 1; j < TotalCol.size(); j++) {
					String heders = ResultRows.get(0).findElements(By.tagName("th")).get(j).getText();
			    	String value = ResultRows.get(i+1).findElements(By.tagName("td")).get(j).getText();
			    	mapOutputData.put(heders, value);
			    }
		    	lisMapOutputData.add(mapOutputData);
			}
		    lstlstmapOutputData.add(lisMapOutputData);
		    
		    driver.close();
		    driver.switchTo().window(strApplWindow);
		    
		}
		catch (Exception ExceptionWorkbenchSOQL) {
					Log.error("ERROR: Unable to Execute SOQL using workbench with exception reported as: "+ExceptionWorkbenchSOQL.getMessage());
		            StringWriter stack = new StringWriter();
		            ExceptionWorkbenchSOQL.printStackTrace(new PrintWriter(stack));
		            Log.debug("DEBUG: The exception in fn_WorkbenchSOQL method is: "+stack);
		}
		return lstlstmapOutputData;
	     
	}
	
	/*
	Author: Suhas Thakre
	Function fn_ApprovalProcessAPI: Approve proposal using API call.
	Inputs: 
	Outputs: Boolean response to check success or failure of function .
	Function fn_SOAP_GET(apex): to trigger apex code using API.
	Inputs: mapTestCases - Key "CallType" define type of request ie query,update or apex. 
			mapSOAP  - key "AttribValue[i]" define attributes required for apex call.
			 		   key "NameSpace" define name space for apex call.
			 		   key "ClassName" define class name or webservice name for apex call.
			 		   key "SOAPRequest" define class API request name for apex call.
	 */
	public boolean fn_ApprovalProcessAPI(WebDriver driver,WebDriverWait webWait)throws Exception
	{
		//TODO:: Generate, Send for Review and esignature GMSA.  
		/************************************************************************************************/
			//Step 1: Read Approval Approval Request query.
			//Step 2: Get approval request ID. 
			//Step 3: Approve proposal using apex API call.
			//Step 4: Verify Approval status.
			
		
		/************************************************************************************************/
		
		
		API_SOAP objAPI_SOAP = new API_SOAP();
		//Account objacctpage = new Account();
		Proposal objProposal= new Proposal();
		ExcelHandler objExcel = new ExcelHandler();
		boolean blnExceptionMSASignAPI = false;
		Map<String, Object> mapTestCases = new HashMap<String, Object>();
		EnvironmentDetails objEnv = (EnvironmentDetails) EnvironmentDetails.mapGlobalVariable.get("objEnv");
		String strNewproposalName = EnvironmentDetails.mapGlobalVariable.get("strNewproposalName").toString();
		Map<String, String> mapSOAP = new HashMap<String, String>();
		Map<String, String> mapAppReq = new HashMap<String, String>();
		//List<String> lstAppReqId = new ArrayList<String>();
		String strApttusQuery = "";
		String strApttusUpdQuery = "";
		String QueueName = "";

		try
		{
			
			//Step 1: Read Approval Approval Request query.
			strApttusQuery = objExcel.fn_ReadApttusQuery("ApttusQuery", "ApttusQuerySheet", "Approval Approval Request");
			
			//strApttusQuery = "SELECT Id, Apttus_Approval__Assigned_To_Name__c FROM Apttus_Approval__Approval_Request__c WHERE Quote_Proposal_No__c = 'Q-00008966'";
			strApttusUpdQuery = strApttusQuery.replace("strproposalName", strNewproposalName);
			
			//Step 2: Get approval request ID.
			mapTestCases.put("CallType", "query");
			mapTestCases.put("CallingData", strApttusUpdQuery);
			mapSOAP.put("UserID", "SOAP_UserID");
			mapSOAP.put("Password", "SOAP_Password");
			
			List<List<Map<String, String>>> lstmapAppReqId = objAPI_SOAP.fn_SOAP_GET(objEnv, mapTestCases, mapSOAP);
			
			//Handle Object not supported issue in Dev
			if (lstmapAppReqId.toString().contains("INVALID_TYPE: sObject")) {
				lstmapAppReqId = fn_WorkbenchSOQL(driver, webWait, strApttusUpdQuery);
			} 
			
			
			for (List<Map<String, String>> listmap : lstmapAppReqId) {
				for (Map<String, String> map : listmap) {
					//handle null value for Apttus_Approval__Assigned_To_Name__c
					try {
						QueueName = map.get("Apttus_Approval__Assigned_To_Name__c").toString();  					} 
					catch (Exception e) {
						QueueName = "";					}
					mapAppReq.put(map.get("Id").toString(), QueueName);
				}
			}
			
			//Step 3: Approve proposal using apex API call.
			mapTestCases.put("CallType", "apex");
			mapSOAP.put("AttribValue1", "APPROVE DONE");
			mapSOAP.put("NameSpace", "Apttus_Approval");
			mapSOAP.put("ClassName", "ApprovalsWebService");
			mapSOAP.put("SOAPRequest", "approveRequest");
			mapSOAP.put("UserID", "SOAP_UserID");
			mapSOAP.put("Password", "SOAP_Password");
			for (Map.Entry<String, String> entry : mapAppReq.entrySet()){    	          
	        	if (entry.getValue().equalsIgnoreCase("Ireland Finance")) {
	        		mapSOAP.put("AttribValue2", entry.getKey());
	        		mapSOAP.put("UserID", "SOAP_Finance_UserID");
	    			mapSOAP.put("Password", "SOAP_Finance_Password");
				}
	        	else if (entry.getValue().equalsIgnoreCase("Ireland Operations")) {
	        		mapSOAP.put("AttribValue2", entry.getKey());
	        		mapSOAP.put("UserID", "SOAP_Operations_UserID");
	    			mapSOAP.put("Password", "SOAP_Operations_Password");
				}
	        	else if (entry.getValue().equalsIgnoreCase("Ireland Sales")) {
	        		mapSOAP.put("AttribValue2", entry.getKey());
	        		mapSOAP.put("UserID", "SOAP_Sales_UserID");
	    			mapSOAP.put("Password", "SOAP_Sales_Password");
				}
	        	else if (entry.getValue().equalsIgnoreCase("Solutions Engineering")) {
	        		mapSOAP.put("AttribValue2", entry.getKey());
	        		mapSOAP.put("UserID", "SOAP_Solution_UserID");
	    			mapSOAP.put("Password", "SOAP_Solution_Password");
				}
	        	else if (entry.getValue().equalsIgnoreCase("AWS - Pricing Desk")) {
	        		mapSOAP.put("AttribValue2", entry.getKey());
	        		mapSOAP.put("UserID", "SOAP_Pricing_UserID");
	    			mapSOAP.put("Password", "SOAP_Pricing_Password");
				}
	        	else if (entry.getValue().contains("GLB")) {
	        		mapSOAP.put("AttribValue2", entry.getKey());
	        		mapSOAP.put("UserID", "SOAP_Productmgr_UserID");
	    			mapSOAP.put("Password", "SOAP_Productmgr_Password");
				}
	        	else {
	        		mapSOAP.put("AttribValue2", entry.getKey());
	        		mapSOAP.put("UserID", "SOAP_UserID");
	    			mapSOAP.put("Password", "SOAP_Password");
				}
	        	objAPI_SOAP.fn_SOAP_GET(objEnv, mapTestCases, mapSOAP);
				Thread.sleep(3500);
		    }
			
			//Step 4: Verify Approval status.
			driver.navigate().refresh();
			Thread.sleep(1000);
			assertEquals(objProposal.fn_verifyApprovalStatus(driver, webWait).getText().trim(), "Approved");
			
			
		}
	    catch (Exception ExceptionApprovalProcessAPI) {
			Log.error("ERROR: Unable to Approved Proposal with exception reported as: "+ExceptionApprovalProcessAPI.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionApprovalProcessAPI.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_ApprovalProcessAPI method is: "+stack);
           
		}
	    Log.debug("DEBUG: Execution of function 'fn_ApprovalProcessAPI' Completed.");
	    blnExceptionMSASignAPI = true;
		return blnExceptionMSASignAPI;
		
	}
	
	
	


	public Boolean fn_AutomateOutlookApprovalProcess(WebDriver driver,WebDriverWait webWait,String strEmailDetails,String strTypeofFlow) throws Exception{
		
		/*
		
		OutlookApproval objOutlook = new OutlookApproval();
		EnvironmentDetails objEnv = new EnvironmentDetails();
		Boolean blnAutomateGmailApprovalProcess = false;
		String strchildWindow = null;
		String strApplWindow = null;
		String strEmailWindow = null;
		String strReviewWindow = null;
		//String strNewproposalName = EnvironmentDetails.mapGlobalVariable.get("strNewproposalName").toString();
		
		
		System.out.println("=================UserID===== "+EnvironmentDetails.mapEnvironmentDetails.get("Outlook_UserID"));
		System.out.println("=================UserID===== "+EnvironmentDetails.mapEnvironmentDetails.get("Outlook_Password"));

		webWait = new WebDriverWait(driver,30);
	    strApplWindow = driver.getWindowHandle();  // get the current window handle
	    //Gets the new window handle	  
	    Robot robot = new Robot();
	    robot.keyPress(KeyEvent.VK_CONTROL);
	    robot.keyPress(KeyEvent.VK_T);
	    robot.keyRelease(KeyEvent.VK_CONTROL);
	    robot.keyRelease(KeyEvent.VK_T);
	    Thread.sleep(2000);
		//get the frame for the Child window
		Set<String> strWindowHandles = driver.getWindowHandles();   		
		strWindowHandles.remove(strApplWindow);
		strchildWindow = strWindowHandles.iterator().next();		
		driver.switchTo().window(strchildWindow);
		driver.navigate().to(EnvironmentDetails.strOutlookURL);
		Thread.sleep(5000);	
		
		//Login to outlook OWA
		try { 
			//Select existing Account 
			objOutlook.fn_clickAcctLink(driver, webWait).click();
			
		} catch (Exception ExceptionNOAcctLink) {
			//Enter Outlook login Credential 
			objOutlook.fn_setEmail(driver, webWait).sendKeys(strEmailDetails);
			objOutlook.fn_setEmail(driver, webWait).sendKeys(Keys.ENTER);
				
		}
		
        if (strTypeofFlow.contentEquals("Approval")){
        	Thread.sleep(30000);	
        	objOutlook.fn_clickMailListLink(driver, webWait).click();
			Thread.sleep(5000);
			//Search mail with given text 
			objOutlook.fn_clickSearchBar(driver, webWait).click();
			objOutlook.fn_setSearchBar(driver, webWait).sendKeys(strNewproposalName);;
			Thread.sleep(3500);
			objOutlook.fn_clickSearchBtn(driver, webWait).click();
			Thread.sleep(3500);
			
			//Get search mail list
			List<WebElement> lstSearchMail = driver.findElements(By.xpath("//div[@aria-label='Mail list']//div[@role='option']"));
			//System.out.println("================================== Mail Count ========="+lstSearchMail.size());
				for (WebElement webElement : lstSearchMail) {
					
					//select each mail and approve 
					webElement.click();
					Thread.sleep(3500);
					objOutlook.fn_clickReplyallBtn(driver, webWait).click();
					Thread.sleep(3500);
					objOutlook.fn_setMsgBody(driver, webWait).sendKeys("APPROVE");
					Thread.sleep(3500);
					objOutlook.fn_clickSendBtn(driver, webWait).click();
					Thread.sleep(3500);
				}
       	}
		else{
			
			objOutlook.fn_clickMailListLink(driver, webWait).click();
			Thread.sleep(5000);
			//Search mail with given text 
			objOutlook.fn_clickSearchBar(driver, webWait).click();
			objOutlook.fn_setSearchBar(driver, webWait).sendKeys("Request for eSignatures. Please review and sign.");;
			Thread.sleep(3500);
			objOutlook.fn_clickSearchBtn(driver, webWait).click();
			Thread.sleep(3500);
			objOutlook.fn_clickMailListLink(driver, webWait).click();
			
			strEmailWindow = driver.getWindowHandle();
			//Docusign Process
			objOutlook.fn_clickReviewBtn(driver, webWait).click();
			
			//Capture review window id
			Thread.sleep(5000);		
		      for (String handle : driver.getWindowHandles()) {
	                 driver.switchTo().window(handle);}		
			Thread.sleep(5000);
			//objGmail.fn_clickIAcceptBtn(driver, webWait).click();
			Thread.sleep(500);		
			strReviewWindow = driver.getWindowHandle();
			Thread.sleep(5000);
			
			//Accept and Continue to sign document 
			if (strTypeofFlow.contentEquals("DocuSignProcessForOrder")){
				
				try {
					Thread.sleep(5000);
					objOutlook.fn_clickAgreeAndAccept(driver, webWait).click(); }          		
				catch(Exception excpEnvironment1){
					Log.debug("DEBUG: Continue Button is not present in the Docusign of Order.");}		
			Thread.sleep(5000);
			objOutlook.fn_clickContinueBtn(driver, webWait).click();
			Thread.sleep(5000);}
			
			if (strTypeofFlow.contentEquals("DocuSignProcessFORGMSA")){
				
				// If AgreeAndAccept button is visible  
				try{
					
					objOutlook.fn_clickAgreeAndAccept(driver, webWait).click();
					Thread.sleep(5000);
					objOutlook.fn_clickContinueBtn(driver, webWait).click();
					Thread.sleep(5000);
					objOutlook.fn_clickNavigateBtn(driver, webWait).click();
					Thread.sleep(5000);
					WebElement svgObject = objOutlook.fn_clickSignBtn(driver, webWait);
					Actions builder = new Actions(driver);
					builder.click(svgObject).build().perform();
					//objGmail.fn_clickSignBtn(driver, webWait).click();
					Thread.sleep(5000);
				    }
			    catch(Exception excpContinueBtn){
			    
					try {			
						objOutlook.fn_clickContinueBtn(driver, webWait).click(); }          		
					catch(Exception excpContinueBtn1){
						Log.debug("DEBUG: Continue Button is not present in the Docusign of GMSA.");}	
				Thread.sleep(5000);	
				objOutlook.fn_clickNavigateBtn(driver, webWait).click();
				Thread.sleep(3000);
				WebElement svgObject = objOutlook.fn_clickSignBtn(driver, webWait);
				Actions builder = new Actions(driver);
				builder.click(svgObject).build().perform();
				//objGmail.fn_clickSignBtn(driver, webWait).click();
				Thread.sleep(3000);			
			    }}	
			
			if (strTypeofFlow.contentEquals("DocuSignProcessForOrder")){
			driver.switchTo().frame(driver.findElement(By.id("signingIframe")));		
			objOutlook.fn_clickDragFrom(driver, webWait).click();
			WebElement webElementDragFrom = objOutlook.fn_clickDragFrom(driver, webWait);
			
			//Add signature on document 
			if (webElementDragFrom.isDisplayed())
				Log.debug("DEBUG: The signing frame is identified and drag from button is displayed.");
			else
				Log.debug("ERROR: The signing frame is identified and drag from button is not displayed.");
					
			WebElement webElementDragTo = objOutlook.fn_clickDragTo(driver, webWait);
			Actions builder = new Actions(driver);
			builder.dragAndDrop(webElementDragFrom, webElementDragTo).build().perform();
			Thread.sleep(10000);}
			
			//String strDocusignWindow = driver.getWindowHandle();	
			//To Handle Multiple windows
		    for (String handle : driver.getWindowHandles()) {
	                 driver.switchTo().window(handle);}	    
		   Thread.sleep(10000);   
		   objOutlook.fn_clickAdoptandSign(driver, webWait).click();   	   
		   driver.switchTo().defaultContent();   
		   driver.switchTo().window(strReviewWindow);  
		   Thread.sleep(5000);
		   objOutlook.fn_clickFinish(driver, webWait).click();
		   driver.close();
		   Thread.sleep(5000);
		   driver.switchTo().window(strEmailWindow);
			
		}
        
        //common code for both the approval and Docusign process
	    objOutlook.fn_clickSignoutImg(driver, webWait).click();
		Thread.sleep(5000);
		if (objOutlook.fn_clickSignoutBtn(driver, webWait).isDisplayed())
			blnAutomateGmailApprovalProcess=true;	
		objOutlook.fn_clickSignoutBtn(driver, webWait).click();
		Thread.sleep(5000);
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(strApplWindow);*/
   		return true ;}
     

     }

	

