package appModules;

/**
 * Created by joe.gannon on 5/18/2016.
 */
public class TierPrice
{
    private String optionName;
    private String tierSelection;
    private String tierType;
    private String tierValue;
    private String tierPrice;

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public String getTierSelection() {
        return tierSelection;
    }

    public void setTierSelection(String tierSelection) {
        this.tierSelection = tierSelection;
    }

    public String getTierType() {
        return tierType;
    }

    public void setTierType(String tierType) {
        this.tierType = tierType;
    }

    public String getTierValue() {
        return tierValue;
    }

    public void setTierValue(String tierValue) {
        this.tierValue = tierValue;
    }

    public String getTierPrice() {
        return tierPrice;
    }

    public void setTierPrice(String tierPrice) {
        this.tierPrice = tierPrice;
    }

    public void print()
    {
        System.out.println("~~~~~~~~~~~~~~~~"+this.optionName+"~~~~~~~~~~~~~~~~");
        System.out.println("\tTierSelection: "+this.tierSelection);
        System.out.println("\tTierType: "+this.tierType);
        System.out.println("\tTierValue: "+this.tierValue);
        System.out.println("\tTierPrice: "+this.tierPrice);
        System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
    }
}
