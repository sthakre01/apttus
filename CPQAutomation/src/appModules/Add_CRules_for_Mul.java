 /*==============================================================
 Author     		:	Madhavi JN
 Class Name		 	: 	Add_ConstraintRules_for_MultiProducts
 Purpose     		: 	Validating the ConstraintRules for Multiple Products
 Date       		:	16/02/2017
 Version Information:	Version1.0
 PreCondition 		:	None
 Test Steps 		:	Validating the ConstraintRules for specific product
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
 #======================================================================
 */
package appModules;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.ConfigurationPageObjects;
import pageObjects.SalesForce.ConstraintRuleVaildations;
import utility.API_SOAP;
import utility.EnvironmentDetails;
import utility.ExcelHandler;
import utility.Log;
import utility.VerificationMethods;

public class Add_CRules_for_Mul extends VerificationMethods{
	
	public Boolean fn_CRules_for_Mul(WebDriver driver,WebDriverWait webWait, 
        	List<List<Map<String, String>>> lstmapConstrRules,String strLineItemName) throws Exception
    {
        Boolean blnConstValSuccessful=false;
        String strApttusQuery="";
        String strLineItem="";
		ExcelHandler objExcel = new ExcelHandler();
		ConfigurationPageObjects objConfig = new ConfigurationPageObjects();
		API_SOAP objSOAP = new API_SOAP();
		EnvironmentDetails objEnvironment = new EnvironmentDetails();
		Add_CRules_for_Mul objCRules = new Add_CRules_for_Mul();
		Map<String, Object> mapTestCases = new HashMap<String, Object>();
		
        try {
        	
    	   	for(List<Map<String, String>> lstmap: lstmapConstrRules){   
    	   		
	    	    for(Map<String, String> map : lstmap){
	    	    	
	    	    	for (Map.Entry<String, String> entry : map.entrySet()){    	          					
		    	    	String strConstraintRuleId = entry.getValue().toString().trim();    	    		
						//Read query from workbook ApttusQuery.xlsx and sheet ApttusQuerySheet for Fetching_ProductCode
						strApttusQuery = objExcel.fn_ReadApttusQuery("ApttusQuery", "ApttusQuerySheet", "Fetching_ConstraintRules_Action");
						Log.debug("DEBUG: Query 'To Fetch Constraint Rules for Action' Before concatenating is: "+strApttusQuery);				
						strApttusQuery = strApttusQuery.replace("'strConstraintRuleId'","'"+strConstraintRuleId+"'");												 
						Log.debug("DEBUG: Query 'To Fetch Constraint Rules for Action' after concatenating is "+strApttusQuery);
						List<List<Map<String, String>>> lstmapConstrAction = new ArrayList<List<Map<String, String>>>();
						//get the response for the SOAP's GET call.
						mapTestCases.put("CallType", "query");
						mapTestCases.put("CallingData", strApttusQuery); 
						lstmapConstrAction = objSOAP.fn_SOAP_GET(objEnvironment, mapTestCases, null);				
						objCRules.fn_constraintRuleAction(driver,webWait,lstmapConstrAction,strLineItemName);  	        	
					}
	    	    }
	    	} 
           
        }    
       catch (Exception e)
        {
	    	StringWriter stack = new StringWriter();
			e.printStackTrace(new PrintWriter(stack));
			blnConstValSuccessful=false;
			Log.debug("DEBUG: The exception in Validating the Constraint rules for Multiple products is: "+stack);
        }	
      
        return blnConstValSuccessful;
    }
	
	
	public void fn_constraintRuleAction(WebDriver driver,WebDriverWait webWait,
			List<List<Map<String, String>>> lstmapActionInfmtn,String strLineItemName){
		
		String strActionDisposition="";
		String strMessage="";
		Boolean blnIsAllLineItemConfig=false;
		Add_Config_for_Mul_ConstRules objConfigMultiProducts = new Add_Config_for_Mul_ConstRules();
		ConstraintRuleVaildations objCRvalPo= new ConstraintRuleVaildations();
		
	  try{
		   for (List<Map<String, String>> list : lstmapActionInfmtn) {	   
			   for (Map<String, String> map : list) {		   
				   String strActionType = map.get("Apttus_Config2__ActionType__c").toString().trim();
				   
				   switch(strActionType){
				   
				   case "Validation":	
					   
					    //Including the LineiTem Specified
	                  	String strXpath = "//a[text()='"+strLineItemName+"']/ancestor::tr[1]/td[1]//input";
			    		blnIsAllLineItemConfig=objConfigMultiProducts.fn_CR_ConfigureProducts(driver, webWait, strXpath, strLineItemName);
			    		Thread.sleep(5000);
			    		
			    		//Fetching the ActionDisposition(Error/Warning) and Validating it
			    		strActionDisposition = objCRvalPo.fn_getMessageType(driver, webWait).getText();
			    		if (strActionDisposition.contains(map.get("Apttus_Config2__ActionDisposition__c").toString().trim()))
			    			Log.debug("DEBUG: Type of Error Message is displayed correctly as:: "+map.get("Apttus_Config2__ActionDisposition__c"));
			    		else
			    			Log.debug("DEBUG: Type of Error Message is displayed incorrectly as:: "+strActionDisposition);
			    		
			    		//Fetching the ActionMessage and Validating it
			    		strMessage = objCRvalPo.fn_getMessageType(driver, webWait).getText();
			    		if (strActionDisposition.contains(map.get("Apttus_Config2__Message__c").toString().trim()))
			    			Log.debug("DEBUG: Type of Message is displayed correctly as:: "+map.get("Apttus_Config2__Message__c"));
			    		else
			    			Log.debug("DEBUG: Type of Message is displayed incorrectly as:: "+strMessage);
			    		
			    		//Excluding the LineiTem Specified to Remove the Error
			    		if (map.get("Apttus_Config2__Message__c").toString().trim().equalsIgnoreCase("Error"))
			    		{
		                  	strXpath = "//a[text()='"+strLineItemName+"']/ancestor::tr[1]/td[1]//input";
				    		blnIsAllLineItemConfig=objConfigMultiProducts.fn_CR_ConfigureProducts(driver, webWait, strXpath, strLineItemName);
				    		Thread.sleep(5000);
			    		}
				   
				   case "Exclusion":
					   
					   
					   
					   
					     
				   
				   case "Inclusion":
					   
			    		if (!(map.get("Apttus_Config2__ActionIntent__c").toString().trim().equalsIgnoreCase("Auto Include")))
			    		{
					    //Including the LineiTem Specified
	                  	strXpath = "//a[text()='"+strLineItemName+"']/ancestor::tr[1]/td[1]//input";
			    		blnIsAllLineItemConfig=objConfigMultiProducts.fn_CR_ConfigureProducts(driver, webWait, strXpath, strLineItemName);
			    		Thread.sleep(5000);
			    		}
			    		
			    		//Fetching the ActionDisposition(Error/Warning) and Validating it
			    		strActionDisposition = objCRvalPo.fn_getMessageType(driver, webWait).getText();
			    		if (strActionDisposition.contains(map.get("Apttus_Config2__ActionDisposition__c").toString().trim()))
			    			Log.debug("DEBUG: Type of Error Message is displayed correctly as:: "+map.get("Apttus_Config2__ActionDisposition__c"));
			    		else
			    			Log.debug("DEBUG: Type of Error Message is displayed incorrectly as:: "+strActionDisposition);
			    		
			    		//Fetching the ActionMessage and Validating it
			    		strMessage = objCRvalPo.fn_getMessageType(driver, webWait).getText();
			    		if (strActionDisposition.contains(map.get("Apttus_Config2__Message__c").toString().trim()))
			    			Log.debug("DEBUG: Type of Message is displayed correctly as:: "+map.get("Apttus_Config2__Message__c"));
			    		else
			    			Log.debug("DEBUG: Type of Message is displayed incorrectly as:: "+strMessage);
			    		
			    		//Excluding the LineiTem Specified to Remove the Error
			    		if (!(map.get("Apttus_Config2__ActionIntent__c").toString().trim().equalsIgnoreCase("Auto Include")))
			    		{
		                  	strXpath = "//a[text()='"+strLineItemName+"']/ancestor::tr[1]/td[1]//input";
				    		blnIsAllLineItemConfig=objConfigMultiProducts.fn_CR_ConfigureProducts(driver, webWait, strXpath, strLineItemName);
				    		Thread.sleep(5000);
			    		}
			    		
				   case "Recommendation":
					   
					    //Including the LineiTem Specified
	                  	strXpath = "//a[text()='"+strLineItemName+"']/ancestor::tr[1]/td[1]//input";
			    		blnIsAllLineItemConfig=objConfigMultiProducts.fn_CR_ConfigureProducts(driver, webWait, strXpath, strLineItemName);
			    		Thread.sleep(5000);
			    		String strCRRuleMessage=objCRvalPo.fn_getCRRuleMessage(driver, webWait).getText();
			    		if (objCRvalPo.fn_clickCRcheckbox(driver, webWait).isDisplayed())
			    		{ 
				    		Log.debug("DEBUG: CR rule dialog box is displayed as Expected with message  :: "+strCRRuleMessage);
			    		    //click on CR rule Inputbox
			    		    objCRvalPo.fn_clickCRcheckbox(driver, webWait).click();
			    		}   
			    		else
			    			Log.debug("DEBUG: CR rule dialog box is not displayed for Lineitem:: "+strLineItemName);
			    		
			    		//Fetching the ActionMessage and Validating it
			    		strMessage = objCRvalPo.fn_getMessageType(driver, webWait).getText();
			    		if (strActionDisposition.contains(map.get("Apttus_Config2__Message__c").toString().trim()))
			    			Log.debug("DEBUG: Type of Message is displayed correctly as:: "+map.get("Apttus_Config2__Message__c"));
			    		else
			    			Log.debug("DEBUG: Type of Message is displayed incorrectly as:: "+strMessage);
			    		
				   }
				   
			   }
			   
		    }
		   
		  }
		  catch (Exception CRulesException)
		 {
		   StringWriter stack = new StringWriter();
		   CRulesException.printStackTrace(new PrintWriter(stack));
		   Log.debug("DEBUG: The exception in Adding Multiple products is: "+stack);
		 }
		
	}


	

   }
