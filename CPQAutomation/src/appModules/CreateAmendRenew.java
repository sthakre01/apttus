/*
 Author     		:	Suhas Thakre
 Class Name			: 	CreateUpgDwg 
 Purpose     		: 	Purpose of this file is :
						1. To perform  upgrade and downgrade existing order 

 Date       		:	26/12/2016 
 Modified Date		:		
 Modified By		: 	
 Version Information:	Version 1.0
 						
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/


package appModules;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import pageObjects.SalesForce.Account;
import pageObjects.SalesForce.AfterApproval;
import pageObjects.SalesForce.Configuration;
import pageObjects.SalesForce.LeadCreationObjects;
import pageObjects.SalesForce.OfferingData;
import pageObjects.SalesForce.OpportunitesPageObject;
import pageObjects.SalesForce.OrderFlow;
import pageObjects.SalesForce.Pricing;
import pageObjects.SalesForce.Proposal;
import utility.API_SOAP;
import utility.EnvironmentDetails;
import utility.ExcelHandler;
import utility.Log;
import utility.Utils;
import utility.VerificationMethods;
import utility.WriteExcel;


public class CreateAmendRenew extends VerificationMethods{
	
	 
	/*
	Author: Suhas Thakre
	Function fn_CreateUpgDwg: Create new Proposal from Renewal/Amendment.
	Inputs: strAccountName - Account name.
			strApttusQueryName - Apptus query to fetch offering details.
			intNoOffetingToSelect - Number of offering to select for Renewal/Amendment.
			strProposalFlow - Option to execute Renewal Amendment flow.
	Outputs: Boolean response to check success or failure of function .
	 */
	//Create proposal from upgrade and downgrade 
	public Boolean fn_CreateAmendRenewProposal(WebDriver driver,WebDriverWait webWait, String strApttusQueryName)  throws Throwable
    {
		
		//TODO:: generating the proposal from Offering data
		/************************************************************************************************/
			//Step 1: Fetch offering names with help of Apttus query
			//Step 2: Select multiple offering with help of radio button
			//Step 3: Get proposal name
			//Step 4: Get existing offering name with updated version 
		/************************************************************************************************/
		
		
		ExcelHandler objExcelHandler = new ExcelHandler();
		Utils objUtils=new Utils();
		Account objAccount = new Account();
		LeadCreationObjects objLeadCreation = new LeadCreationObjects();	
		ExcelHandler objExcel = new ExcelHandler();
		WriteExcel objWriteExcel = new WriteExcel();
		Account objAcctpage = new Account(); 
		OfferingData objOfferingData= new OfferingData();
		Proposal objProposal = new Proposal();
		EnvironmentDetails objEnv = (EnvironmentDetails) EnvironmentDetails.mapGlobalVariable.get("objEnv");
		OpportunitesPageObject objOpportunites = new OpportunitesPageObject();
		API_SOAP objAPI_SOAP = new API_SOAP();
		List<String> arrstrOfferingNameExist = new ArrayList<>();
		List<String> arrstrOfferingNameSel = new ArrayList<>();
		String strNewproposalName=null;
		String strProposalType = null;
		Boolean blnCreateUpgDwg = false;
		String strAmendRenewWB = "TestData.xlsx";
		String strAmendRenewSheet = "Amend_Renew";
		List<Map<String, String>> lstmapAmendRenewData = new ArrayList<Map<String,String>>();
		String strAccountName = "";
		String strProposalFlow = "";
		String strNewOfferingCount = "";
		String strProductAssignNewOfferingCount = "";
		Integer intNoOffetingToSelect = 0;
		String strNoOffetingToSelect = "";
		String strAcctAddrID = "";
		String strPrimaryContact = "";
		String strCustReference= "";
		String[] arrNoOffetingToSelect = null;
		String strCountry = "";
		
		Map<String, Object> mapOutputData = new HashMap<String, Object>();
		List<Map<String, Object>> lisOutputData = new ArrayList<>();
		List<List<Map<String, Object>>> lstmapOutputData = new ArrayList<>();
		Map<String, Object> mapTestCases = new HashMap<String, Object>();
		
		String excelResource = EnvironmentDetails.Path_ExternalFiles
				+ System.getProperty("file.separator") + strAmendRenewWB;
		File fileLeadCreationData = new File(excelResource);
		lstmapAmendRenewData = objExcel.fn_ReadExcelAsMap(fileLeadCreationData,strAmendRenewSheet); 
		for (Map<String, String> mapAmendRenewData : lstmapAmendRenewData) {
			strAccountName = mapAmendRenewData.get("AccountName").toString().trim();
			EnvironmentDetails.mapGlobalVariable.put("strAcctName", strAccountName);
			strProposalFlow = mapAmendRenewData.get("ProposalFlow").toString().trim();
			strCountry = mapAmendRenewData.get("Country").toString().trim();
			EnvironmentDetails.mapGlobalVariable.put("strCountry", strCountry);
			//intNoOffetingToSelect = Integer.parseInt(mapAmendRenewData.get("NoOffetingToSelect").toString().trim());
			strNoOffetingToSelect = mapAmendRenewData.get("OffetingToSelect").toString().trim();
			strNewOfferingCount = mapAmendRenewData.get("NewOfferingCount").toString().trim();
			EnvironmentDetails.mapGlobalVariable.put("strNewOfferingCount", strNewOfferingCount);
			strProductAssignNewOfferingCount = mapAmendRenewData.get("ProductAssignNewOfferingCount").toString().trim();
			EnvironmentDetails.mapGlobalVariable.put("strProductAssignNewOfferingCount", strProductAssignNewOfferingCount);
			
		}
		
		
		//Get count of number of offering to select
		arrNoOffetingToSelect = strNoOffetingToSelect.split("#");
		intNoOffetingToSelect = arrNoOffetingToSelect.length;
		EnvironmentDetails.mapGlobalVariable.put("intNoOffetingToSelect", intNoOffetingToSelect);
		
		mapTestCases.put("CallType", "query");
		//Read query from workbook ApttusQuery.xlsx and sheet ApttusQuerySheet
		String strApttusQuery = objExcelHandler.fn_ReadApttusQuery("ApttusQuery", "ApttusQuerySheet", strApttusQueryName);
		strApttusQuery = strApttusQuery.replace("AccountNameInput", strAccountName);
		
		try {
			
			//Step 1 
			
			//Search Account
			objAcctpage.fn_setSearchField(driver, webWait).sendKeys(strAccountName); 
			objAcctpage.fn_clickSearchBtn(driver, webWait).click();
			Thread.sleep(3500);
			driver.findElement(By.partialLinkText(strAccountName)).click();
			
			
			
			//Get Account Address ID Name
			objLeadCreation.fn_getEditAddeLink(driver, webWait).isEnabled();
			try {
	  			strAcctAddrID = objLeadCreation.fn_getAcctAddrID(driver, new WebDriverWait(driver,2)).getText();
	      		//Save Account Address ID name in global variable map 
				 EnvironmentDetails.mapGlobalVariable.put("strAcctAddrID", strAcctAddrID);
				 Log.debug("DEBUG: Account Address ID Name save in global variable map with key 'strAcctAddrID'.");
			} catch (Exception e) {
				Log.debug("DEBUG: Account Address ID Name Not present on Account page");
			}
			
			
			//Save Customer reference in global variable map
			strCustReference = objAccount.fn_GetCustReference(driver, webWait).getText();
			EnvironmentDetails.mapGlobalVariable.put("strCustReference", strCustReference);
			Log.debug("DEBUG: Customer reference save in global variable map with key 'strCustReference'.");
			
			//Get Primary Contact Name
			objLeadCreation.fn_getEditAddeLink(driver, webWait).isEnabled();
			try {
	  			strAcctAddrID = objLeadCreation.fn_gePrimaryContact(driver, new WebDriverWait(driver,2)).getText();
	      		//Save Account Address ID name in global variable map 
				 EnvironmentDetails.mapGlobalVariable.put("strPrimaryContact", strPrimaryContact);
				 Log.debug("DEBUG: Primary Contact Name save in global variable map with key 'strPrimaryContact'.");
			} catch (Exception e) {
				Log.debug("DEBUG: Primary Contact Name Not present on Account page");
			}
			
			//Close reminder window
	        objUtils.fn_CloseRemainder(driver, webWait);
			
			//handle change order for Ireland and non-Ireland 
			try {
				//executing change order for non Ireland 
				objAcctpage.fn_clickUpgDwgOpptyBtn(driver, new WebDriverWait(driver,10)).isEnabled();
				objAcctpage.fn_clickUpgDwgOpptyBtn(driver, webWait).click();
				objAcctpage.fn_clickOfferingManagementBtn(driver, webWait).isEnabled();
				objAcctpage.fn_clickOfferingManagementBtn(driver, webWait).click();
			} catch (Exception e) {
				//executing change order for Ireland
				objAcctpage.fn_clickOfferingManagementBtn(driver, webWait).isEnabled();
				objAcctpage.fn_clickOfferingManagementBtn(driver, webWait).click();
			}
			
			//Select existing Offering Radio button
			Thread.sleep(3500);
			//Step 2
			for (int i = 0; i < arrNoOffetingToSelect.length; i++) {
				webWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(),'"+arrNoOffetingToSelect[i]+"')]/ancestor::tr[1]/td[1]//input"))).click();
				
					/*//Update version of offering selected
					Integer intIndexOfLastChar = arrstrOfferingNameExist.get(i).toString().length();
					char chrversionno = arrstrOfferingNameExist.get(i).toString().charAt(intIndexOfLastChar-1);
					Integer intoldversion  = Integer.parseInt(chrversionno+"");
					int intNewversion = intoldversion + 1;
					String strNewversion = Integer.toString(intNewversion);
					String UpdatedOfferingName = arrstrOfferingNameExist.get(i).toString().substring(0, intIndexOfLastChar-1)+strNewversion;
					//Add Updated offering name to arrstrOfferingNameSel 
				    arrstrOfferingNameSel.add(UpdatedOfferingName.trim());*/
			}
			
			//TODO:: Capture Screenshot for number of offering selected
			objUtils.fn_takeScreenshotFullPage(driver);
				//click UpgDwg or Renewal button
				if (strProposalFlow.equalsIgnoreCase("Renewal")) {
					objOfferingData.fn_clickCreateRenewalBtn(driver, webWait).click();
					Thread.sleep(3500);	
					objOfferingData.fn_clickCreateRenewalsbt(driver, webWait).click();
				}else if (strProposalFlow.equalsIgnoreCase("Amendment")) {
					objOfferingData.fn_clickCreateUpgDwgBtn(driver, webWait).click();
					Thread.sleep(3500);	
					objOfferingData.fn_clickConfirmUpgDwgBtn(driver, webWait).click();
				}
			
			//click on proposal
			objOpportunites.fn_clickProposalNumber(driver, new WebDriverWait(driver,360)).isEnabled();
			objOpportunites.fn_clickProposalNumber(driver, webWait).click();
			Thread.sleep(3500);
			
			//TODO:: Capture Screenshot for Proposal
			objUtils.fn_takeScreenshotFullPage(driver);
			
			//Step 3 			
			strNewproposalName = objProposal.fn_getProposalNumber(driver, webWait).getAttribute("innerText").trim();
			Log.debug("DEBUG: Saved Proposal Number in global variable map.");
			Log.info("Proposal Number Generated is "+strNewproposalName);
			EnvironmentDetails.mapGlobalVariable.put("strNewproposalName", strNewproposalName);
			//CreateProposal.strNewproposalName = strNewproposalName;
			
			//write data in output file 
			mapOutputData.put("Proposal Number", strNewproposalName);
			lisOutputData.add(mapOutputData);
			lstmapOutputData.add(lisOutputData);
			objWriteExcel.fn_WriteExcel(lstmapOutputData, EnvironmentDetails.strFQNFileNameWrite, "Proposal_Details");
			mapOutputData = new HashMap<String, Object>();
			lisOutputData = new ArrayList<>();
			lstmapOutputData = new ArrayList<>();
			
			
			//Saved Proposal Type in global variable map
			strProposalType = objProposal.fn_getProposalType(driver, webWait).getAttribute("innerText").trim();
			Log.debug("DEBUG: Saved Proposal Type in global variable map with Key strProposalType.");
			EnvironmentDetails.mapGlobalVariable.put("strProposalType", strProposalType);
				//Verify Proposal Type
				if (strProposalFlow.equalsIgnoreCase("Renewal")) {
					Assert.assertEquals("Renewal", strProposalType);
				}else if (strProposalFlow.equalsIgnoreCase("Amendment")) {
					Assert.assertEquals("Amendment", strProposalType);
				}
			
			/*//Step 4 
			objAcctpage.fn_clickOfferingManagementBtn(driver, webWait).click();
			Thread.sleep(3500);	
			List<WebElement> eleExistOfferingNames = driver.findElements(By.xpath("//*[@id='objectWall']/div/span[1]"));
			int outdatacount = 1;
			for (WebElement webElement : eleExistOfferingNames) {
				
				String strExistOfferingNames = webElement.getAttribute("textContent");
				if (!strExistOfferingNames.equalsIgnoreCase("Products Unassigned to Offering Data Records")) {
					arrstrOfferingNameSel.add(strExistOfferingNames);
					mapOutputData.put("Existing Offering Name "+outdatacount, strExistOfferingNames);
					outdatacount++;
				}
			}*/
			
			//Add arrstrOfferingNameSel to global variable map 
			EnvironmentDetails.mapGlobalVariable.put("arrstrOfferingNameSel", arrNoOffetingToSelect);
			Log.debug("DEBUG: Saved Existing offering name in global variable map with Key arrstrOfferingNameSel.");
			
			//TODO:: Capture Screenshot for number of existing offerings
			//objUtils.fn_takeScreenshotFullPage(driver);
			
			//write data in output file			
			lisOutputData.add(mapOutputData);
			lstmapOutputData.add(lisOutputData);
			objWriteExcel.fn_WriteExcel(lstmapOutputData, EnvironmentDetails.strFQNFileNameWrite, "Existing_Offering_Details");
			
			//webWait.until(ExpectedConditions.elementToBeClickable(By.linkText(strNewproposalName))).click();
			Thread.sleep(3500);	
			blnCreateUpgDwg = true;
		} catch (Exception ExceptionCreateUpgDwg) {
			Log.error("ERROR: Unable to Create proposal from upgrade and downgrade with exception reported as: "+ExceptionCreateUpgDwg.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionCreateUpgDwg.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_CreateUpgDwg method is: "+stack);
           
		}
		return blnCreateUpgDwg ;		
    }
	
	//Change quantity of exiting product line
	/*public static void fn_ReConfigureProduct(WebDriver driver,WebDriverWait webWait, String strReConfigSheet_Name)
	{
		Configuration objConfig = new Configuration();
		EnvironmentDetailsSAS   objEnvDetails = new EnvironmentDetailsSAS();
		Add_Configurations objAddConfigurations= new Add_Configurations();
		
		try
        { 
		//click existing product configuration 
		driver.findElement(By.xpath("//a[text()='VytalVault Shared Services']/ancestor::tr[1]/td[5]//img[@alt='Configure']")).click();
		objConfig.fn_clickNext(driver, webWait).click();
    	Thread.sleep(6000); 
    	File fileConfigValidations = new File(objEnvDetails.dataSheetPath);	
		    ArrayList<String> arrlistFileContents = ExcelHandler.readExcelFileAsArray(fileConfigValidations,strReConfigSheet_Name);		   
		    String strOptionName=null;
		    //String strXpath = "";
		    String QuantityXpath = null;
	    int intRowtraverser=0;

	    //Utils.takeScreenshot(driver, strTestCaseName);
	    Log.info("++++++++++++++++++++++++++++++++++++Start of Configuration of Services+++++++++++++++++++++++++++++++++++++++++++++");
		for(int intCnt = 0 ; intCnt < arrlistFileContents.size(); intCnt +=2)
		{	
			intRowtraverser = intCnt;
			strOptionName = arrlistFileContents.get(intRowtraverser);
			//strXpath = "//a[text()='"+strOptionName+"']/ancestor::tr[1]/td[1]//input";	
			QuantityXpath = "//a[text()='"+strOptionName+"']/ancestor::tr[1]/td[6]//input";	
			try
			{					
				Thread.sleep(10000);
				objAddConfigurations.fn_waitForSpinner(driver);
				//Get the current quantity and update increased quantity by 1
				String CurrentQuantity = driver.findElement(By.xpath(QuantityXpath)).getAttribute("value");
				double UpdatedQuantityF = Float.parseFloat(CurrentQuantity);
				//update quantity by 1
				UpdatedQuantityF = UpdatedQuantityF +1.0 ;
				String UpdatedQuantityS = Double.toString(UpdatedQuantityF);
				System.out.println(" Updated Quantity = "+UpdatedQuantityS);
				driver.findElement(By.xpath(QuantityXpath)).clear();
				driver.findElement(By.xpath("//a[text()='Vaulting Services - VytalVault (GB Storage - Provisioned)']/ancestor::tr[1]/td[6]//span[text()='Qty']")).click();
				Thread.sleep(10000);
				objAddConfigurations.fn_waitForSpinner(driver);
				driver.findElement(By.xpath(QuantityXpath)).sendKeys(UpdatedQuantityS);
				driver.findElement(By.xpath("//a[text()='Vaulting Services - VytalVault (GB Storage - Provisioned)']/ancestor::tr[1]/td[6]//span[text()='Qty']")).click();
				objAddConfigurations.fn_waitForSpinner(driver);
			}
		     catch (Exception e)
	        {
	            Log.error("Unable to click the element");
	            Log.error(e.getMessage());
	            e.printStackTrace();

	        }
		
		}
		
        }
		catch (Exception ExceptionReConfig)
	    {
	            Log.error("ERROR: Unable to do ReConfiguration with exception reported as: "+ExceptionReConfig.getMessage());
	            StringWriter stack = new StringWriter();
	            ExceptionReConfig.printStackTrace(new PrintWriter(stack));
	            Log.debug("DEBUG: The exception in fn_ReConfigureProduct is: "+stack);
	
	     }		
	}
	*/
	
	/*
	Author: Suhas Thakre
	Function fn_RepriceProduct : Change offering price details.
	Inputs: strRepriceProductData - (Adjustment type, Adjustment Amount)
	Outputs: Boolean response to check success or failure of function.
	 */
	//Change Price of exiting product line
	public Boolean fn_RepriceProduct(WebDriver driver,WebDriverWait webWait, String strRepriceProductData) throws Throwable
	{
		
		//TODO:: Change Price of exiting offerings product
		/************************************************************************************************/
			//Step 1: Reprice existing offerings 
			//Step 2: Validate applied amount adjustment 
			
		/************************************************************************************************/
		
		Pricing objPricing = new Pricing();
		Configuration objConfig = new Configuration();
		Utils objUtils = new Utils();
		Boolean blnRepriceProduct =false;
		try
        { 
		
    	Double dblMonthlyAdjustment=null;
		    String strAdjXpath=null;
		    String strAmtXpath=null;
		    String strNetPriceXpath=null;
		    String strOriNetPrice=null;
		    Double dblOriNetPrice = null;
		    String strExpNetPrice=null;
		    Double dblExpNetPrice = null;
		    String strActNetPrice=null;
		    DecimalFormat df = new DecimalFormat("#.00");        
		    ArrayList<String> arrstrOfferingNameSel =   (ArrayList<String>) (EnvironmentDetails.mapGlobalVariable.get("arrstrOfferingNameSel"));
		    String[] arrRepriceProductData = strRepriceProductData.split("#");
		    List<String> arrOriNetPrice = new ArrayList<>();
		    Thread.sleep(3500);
		    objConfig.fn_createPropAndSubmitBtn(driver, webWait).isDisplayed();
		    Thread.sleep(3500);

		    //Step 1
		    for (String strOfferingNameSel : arrstrOfferingNameSel) {
		    	strAdjXpath = "//tr[ (td[5]//span[text()='"+strOfferingNameSel+"'])   and (td[6]//span[text()='Monthly Fee']) ]/td[13]//select";
				strAmtXpath = "//tr[  (td[5]//span[text()='"+strOfferingNameSel+"'])   and (td[6]//span[text()='Monthly Fee']) ]/td[14]//input";
				strNetPriceXpath = "//tr[ (td[5]//span[text()='"+strOfferingNameSel+"'])   and (td[6]//span[text()='Monthly Fee']) ]/td[17]/span";
				List<WebElement> eleAdjType = driver.findElements(By.xpath(strAdjXpath));
				List<WebElement> eleMonthAdj = driver.findElements(By.xpath(strAmtXpath));
				List<WebElement> elestrNetPrice = driver.findElements(By.xpath(strNetPriceXpath));
				//reprice all product in existing offering
				for (int i = 0; i < eleAdjType.size(); i++) {
					
					//Select Adjustment Type 	
					Select strStrAdjustmentType = new Select(eleAdjType.get(i));
					strStrAdjustmentType.selectByValue(arrRepriceProductData[0]);
					Thread.sleep(3500);
					
					//Enter Adjustment value
					eleMonthAdj.get(i).clear();
					eleMonthAdj.get(i).sendKeys(arrRepriceProductData[1]);
					
					//capture original net price and convert to Double
					strOriNetPrice = elestrNetPrice.get(i).getText().trim();
					strOriNetPrice = objUtils.fn_CleanCurrencyCode(strOriNetPrice);
					strOriNetPrice = strOriNetPrice.replace(",", "");
					arrOriNetPrice.add(strOriNetPrice);
					
				}
				
				
			}
		    
		    //TODO:: Capture Screenshot for Adjustment amount 
			objUtils.fn_takeScreenshotFullPage(driver);
		    
		    objPricing.fn_RepriceBtn(driver, webWait).click();
			Thread.sleep(3500);
			
			//wait for price get updated 
			fn_waitForPriceUpdate(driver);
			driver.navigate().refresh();
			Thread.sleep(3500);
			
			
			//Step 2
			//to retrieve old net price array
			int j = 0;
			//validate the applied adjustment 
			for (String strOfferingNameSel : arrstrOfferingNameSel) {
				strNetPriceXpath = "//tr[ (td[5]//span[text()='"+strOfferingNameSel+"'])   and (td[6]//span[text()='Monthly Fee']) ]/td[17]/span";
				List<WebElement> elestrNetPrice = driver.findElements(By.xpath(strNetPriceXpath));
				for (int i = 0; i < elestrNetPrice.size(); i++) {
					
					//capture Actual net price
					strActNetPrice = elestrNetPrice.get(i).getText().trim();
					strActNetPrice = objUtils.fn_CleanCurrencyCode(strActNetPrice);
					strActNetPrice = strActNetPrice.replace(",", "");
					//System.out.println( "========= strActNetPrice ========"+strActNetPrice);
					
					strOriNetPrice = arrOriNetPrice.get(j).toString();
					//System.out.println( " =========strOriNetPrice========= "+strOriNetPrice );
					dblOriNetPrice = Double.parseDouble(strOriNetPrice);
					j++;
					dblMonthlyAdjustment = Double.parseDouble(arrRepriceProductData[1]);
					
					
							switch (arrRepriceProductData[0].toLowerCase().trim()) {
							 //format Double to 2 decimal place 
						     case "% discount":
						    	 	//calculate expected net price 
									dblExpNetPrice = dblOriNetPrice - ( dblOriNetPrice * (dblMonthlyAdjustment/100) );
									dblExpNetPrice = Double.valueOf(df.format(dblExpNetPrice));
									strExpNetPrice = String.format("%.2f", dblExpNetPrice);
									//System.out.println( "========= strExpNetPrice ========"+strExpNetPrice);
					        	 	Thread.sleep(3500);
					        	 	//Compare net price 
					        	 	assertEquals(strActNetPrice, strExpNetPrice);
					        	 	
					             break;
					         case "discount amount":
					        	 	//calculate expected net price 
									dblExpNetPrice = dblOriNetPrice - (dblMonthlyAdjustment * 12);
									dblExpNetPrice = Double.valueOf(df.format(dblExpNetPrice));
									strExpNetPrice = String.format("%.2f", dblExpNetPrice);
									//System.out.println( "========= strExpNetPrice ========"+strExpNetPrice);
					        	 	Thread.sleep(3500);
					        	 	//Compare net price 
					        	 	assertEquals(strActNetPrice, strExpNetPrice);
					             break;
					         case "% markup":
					        	 	//calculate expected net price 
									dblExpNetPrice = dblOriNetPrice + ( dblOriNetPrice * (dblMonthlyAdjustment/100) );
									dblExpNetPrice = Double.valueOf(df.format(dblExpNetPrice));
									strExpNetPrice = String.format("%.2f", dblExpNetPrice);
									//System.out.println( "========= strExpNetPrice ========"+strExpNetPrice);
					        	 	Thread.sleep(3500);
					        	 	//Compare net price 
					        	 	assertEquals(strActNetPrice, strExpNetPrice);
					             break;
					         case "markup amount":
					        	 	//calculate expected net price 
									dblExpNetPrice = dblOriNetPrice + (dblMonthlyAdjustment * 12);
									dblExpNetPrice = Double.valueOf(df.format(dblExpNetPrice));
									strExpNetPrice = String.format("%.2f", dblExpNetPrice);
									//System.out.println( "========= strExpNetPrice ========"+strExpNetPrice);
					        	 	Thread.sleep(3500);
					        	 	//Compare net price 
					        	 	assertEquals(strActNetPrice, strExpNetPrice);
					             break;
					         default:
					        	 Log.debug("DEBUG: Unable to find Adjustment Type");
					     }
				}
			}
			
			Thread.sleep(3500);
			//TODO:: Capture Screenshot After Adjustment amount 
			objUtils.fn_takeScreenshotFullPage(driver);
			
			objConfig.fn_createPropAndSubmitBtn(driver, webWait).click();
			Thread.sleep(3500);
		
		blnRepriceProduct =true;
        }
		catch (Exception ExceptionRepriceProd)
	    {
	           
	            Log.error("ERROR: Unable to Change Price of exiting product with exception reported as: "+ExceptionRepriceProd.getMessage());
	            StringWriter stack = new StringWriter();
	            ExceptionRepriceProd.printStackTrace(new PrintWriter(stack));
	            Log.debug("DEBUG: The exception in fn_RepriceProduct method is: "+stack);
	
	     }
		
		return blnRepriceProduct;
	}
	
	
/*	//Delete Product from pricing screen 
	public Boolean fn_RemoveProduct(WebDriver driver,WebDriverWait webWait,Integer intNoProductToRemove)
	{
		Pricing objPricing= new Pricing();
		String strRemoveProductXpath=null;
		Boolean blnRemoveProduct =false;
		ArrayList<String> arrstrOfferingNameSel =(ArrayList<String>)EnvironmentDetails.mapGlobalVariable.get("arrstrOfferingNameSel");
		try
        {
			//TODO:: Delete existing offering products
			for (String strOfferingNameSel : arrstrOfferingNameSel) {
				strRemoveProductXpath = "//tr[(td[5]//span[text()='"+strOfferingNameSel+"']) ]/td[2]//img[@title='Remove']";
				List<WebElement> eleRemoveProduct = driver.findElements(By.xpath(strRemoveProductXpath));
				for (int i = 0; i < intNoProductToRemove; i++) {
					eleRemoveProduct.get(i).click();
					Thread.sleep(3500);
					//click yes button 
					objPricing.fn_clickYesBtn(driver, webWait).click();
					Thread.sleep(3500);
				}
			}
			blnRemoveProduct =true;
        }
		catch (Exception ExceptionRemoveProd)
	    {
	            Log.error("ERROR: Unable to Delete Product from pricing screen with exception reported as: "+ExceptionRemoveProd.getMessage());
	            StringWriter stack = new StringWriter();
	            ExceptionRemoveProd.printStackTrace(new PrintWriter(stack));
	            Log.debug("DEBUG: The exception in fn_RemoveProduct method is: "+stack);
	
	     }
		return blnRemoveProduct;
	}

	*/
	/*
	Author: Suhas Thakre
	Function fn_OfferingManagement : Assign Product to New and Existing offering.
	Inputs: intProductAssignNewOfferingCount - Number of product to be assign to new offering.
	Outputs: Boolean response to check success or failure of function.
	 */
	//Assign Product to offering  
	public Boolean fn_OfferingManagement(WebDriver driver,WebDriverWait webWait)
	{
		
		//TODO:: Assign Product to New and existing offering
		/************************************************************************************************/
			//Step 1: Get count of unassign product 
			//Step 2: Verify number of unassign product should be greater than number of product to be assign to offerings 
			//Step 3: Assign product to offerings 
			//Step 4: Select dates for existing offerings 

		/************************************************************************************************/
		
		Configuration objConfig = new Configuration();
		OfferingData objOfferingData = new OfferingData();
		ExcelHandler objExcel = new ExcelHandler();
		Utils objUtils = new Utils();
		Boolean blnOfferingManagement =false;
		int strNewOfferingCount;
		ArrayList<String> arrNewOfferingNames = null ;
		String strOfferingWB = "TestData.xlsx";
		String strOfferingSheet = "Amend_Renew";
		List<Map<String, String>> lstmapOfferingData = new ArrayList<Map<String,String>>();
		String strProposalType = EnvironmentDetails.mapGlobalVariable.get("strProposalType").toString();
		Integer intProductAssignNewOfferingCount = 0;
		String strProductAssignNewOfferingCount = "";
		String strStartDate = "";
		String strEndDate = "";
		String strBillingDate = "";
		String strChangeTerm = "";
		try
        {
			
			
			//Read Offering data and Change Dates.
			String excelResource = EnvironmentDetails.Path_ExternalFiles+ System.getProperty("file.separator") + strOfferingWB;
			File fileLeadCreationData = new File(excelResource);
			lstmapOfferingData = objExcel.fn_ReadExcelAsMap(fileLeadCreationData,strOfferingSheet); 
			for (Map<String, String> mapOfferingData : lstmapOfferingData) {
				strStartDate = mapOfferingData.get("Start_Date").toString().trim();
				strBillingDate = mapOfferingData.get("Billing_Date").toString().trim();
				//strEndDate = mapOfferingData.get("End_Date").toString().trim();
				strChangeTerm = mapOfferingData.get("Term").toString().trim();
				strProductAssignNewOfferingCount = mapOfferingData.get("ProductAssignNewOfferingCount").toString().trim();
				intProductAssignNewOfferingCount = Integer.parseInt(strProductAssignNewOfferingCount);
			}
			
			//Change Date only if dates need to be change
			if (!(strStartDate.equalsIgnoreCase("NA") && strBillingDate.equalsIgnoreCase("NA") )) {
				//change date format as per country 
				SimpleDateFormat USformat = new SimpleDateFormat("MM/dd/yyyy");
				SimpleDateFormat NonUSformat = new SimpleDateFormat("dd/MM/yyyy");
				SimpleDateFormat Inputformat = new SimpleDateFormat("dd/MMM/yyyy");
				Date dateStartDate = Inputformat.parse(strStartDate);
				Date dateBillingDate = Inputformat.parse(strBillingDate);
				//Date dateEndDate = Inputformat.parse(strEndDate);
				if (EnvironmentDetails.mapGlobalVariable.get("strCountry").toString().equalsIgnoreCase("United States")) {
					strStartDate = USformat.format(dateStartDate);
					strBillingDate = USformat.format(dateBillingDate);
					//strEndDate = USformat.format(dateEndDate);
				} else {
					strStartDate = NonUSformat.format(dateStartDate);
					strBillingDate = NonUSformat.format(dateBillingDate);
					//strEndDate = NonUSformat.format(dateEndDate);
				}
			}
			
			//For Ireland and Non-Ireland
			objConfig.fn_clickOfferingMgmntNABtn(driver, new WebDriverWait(driver,30)).click();
			
			Thread.sleep(3500); 
			
			//TODO:: Capture Screenshot for before assigning product to offering 
			objUtils.fn_takeScreenshotFullPage(driver);
			
			//Step 1
			int intUnassignProd = driver.findElements(By.xpath("//span[text()='Products Unassigned to Offering Data Records']/following::ul[1]/span")).size();
			Log.info("UnAssign product available is "+intUnassignProd);
			String strNewOfferingDataName = null ;
			
			//Get existing offering name selected 
			String[] arrstrOfferingNameSel  = (String[]) EnvironmentDetails.mapGlobalVariable.get("arrstrOfferingNameSel");
			WebElement webElementDragFrom = null;
			WebElement webElementDragTo = null;
			
			//if no new offering created then new offering count should be 0
			if (EnvironmentDetails.mapGlobalVariable.get("arrNewOfferingNames") != null) {
				
				//Get New offering name created 
				arrNewOfferingNames =(ArrayList)EnvironmentDetails.mapGlobalVariable.get("arrNewOfferingNames");
				strNewOfferingCount = arrNewOfferingNames.size();

			}
			else 
			{
				strNewOfferingCount = 0;
			}
			
						
			//Step 2 
			if (intUnassignProd >= (intProductAssignNewOfferingCount*strNewOfferingCount)) {
				Log.info("Number of product to be assign to new offering is "+(intProductAssignNewOfferingCount*strNewOfferingCount)
						+ " and Number of Unassign product is "+intUnassignProd);
			} else {
				Log.error("ERROR: Number of product to be assign to new offering is "+(intProductAssignNewOfferingCount*strNewOfferingCount)
							+ " and Number of Unassign product is "+intUnassignProd);
				assertTrue(false, "ERROR: Number of unassign product is less than number of product to be assign to offerings.");
			}
			
			
			//edit offering only when data is available for change
			if (!(strStartDate.equalsIgnoreCase("NA") &&  strBillingDate.equalsIgnoreCase("NA")
					&& strChangeTerm.equalsIgnoreCase("NA"))) {
				//Step 4: Set dates for existing offerings  
				for (String strOfferingNameSel : arrstrOfferingNameSel) {
					objOfferingData.fn_clickEditOfferingBtn(driver, webWait, strOfferingNameSel).isEnabled();
					objOfferingData.fn_clickEditOfferingBtn(driver, webWait, strOfferingNameSel).click();
					objOfferingData.fn_clickCloneBtn(driver, webWait).isEnabled();
					
					if (!strChangeTerm.equalsIgnoreCase("NA")) {
						Actions act = new Actions(driver);
						objOfferingData.fn_setChangeRenewTerm(driver, webWait).isDisplayed();
						objOfferingData.fn_setChangeRenewTerm(driver, webWait).clear();
						objOfferingData.fn_setChangeRenewTerm(driver, webWait).sendKeys(strChangeTerm);
						act.moveToElement(objOfferingData.fn_setChangeTerm(driver, webWait)).doubleClick().build().perform();
						objOfferingData.fn_setChangeTerm(driver, webWait).sendKeys(strChangeTerm);
						
						
						
					}
					
					if (!strStartDate.equalsIgnoreCase("NA")) {
						objOfferingData.fn_setOfferingChangeStartDate(driver, webWait).clear();
						objOfferingData.fn_setOfferingChangeStartDate(driver, webWait).sendKeys(strStartDate);
					}
					if (!strBillingDate.equalsIgnoreCase("NA")) {
						objOfferingData.fn_setOfferingChangeBillDate(driver, webWait).clear();
						objOfferingData.fn_setOfferingChangeBillDate(driver, webWait).sendKeys(strBillingDate);
					}
					/*if (!strEndDate.equalsIgnoreCase("NA")) {
						objOfferingData.fn_setOfferingEndDate(driver, webWait).clear();
						objOfferingData.fn_setOfferingEndDate(driver, webWait).sendKeys(strEndDate);
					}*/
					
					
					
					
					objOfferingData.fn_clickSaveBtn(driver, webWait).isEnabled();
					objOfferingData.fn_clickSaveBtn(driver, webWait).click();
					objOfferingData.fn_clickEditOffBtn(driver, webWait).isEnabled();
					//objConfig.fn_SelectChangeStartDatePicker(driver, webWait, strOfferingNameSel).click();
		       		//objConfig.fn_SelectChangeBillingDatePicker(driver, webWait, strOfferingNameSel).click();
				}
			}
			
			
			//Step 3
			int j = 0;
			int k = 0;
			for (int i = 0; i <intUnassignProd; i++) {
				webElementDragFrom = objConfig.fn_clickDragFrom(driver, webWait);
				//assign product to new offering 
				if (i < (intProductAssignNewOfferingCount*strNewOfferingCount)) {
					
					//Get new offering name from list
					strNewOfferingDataName = arrNewOfferingNames.get(k);
					k++;
					webElementDragTo = objConfig.fn_clickDragToOffering(driver, webWait, strNewOfferingDataName);
					//reset index of NewOfferingNames list 
					if (k >= strNewOfferingCount ) {
						
						k = 0;
					}
					//System.out.println( " ======= Offering add to new "+ i);
				}else {
					//assign product to existing offering 
					webElementDragTo = objConfig.fn_clickDragToOffering(driver, webWait, arrstrOfferingNameSel[j]);
					//System.out.println( " ======= Offering add to Exist "+ i);
					j++;
					//reset counter for existing offering 
					if (j >= arrstrOfferingNameSel.length) {
						j = 0;
					}
				}
				Actions builder = new Actions(driver);
				builder.dragAndDrop(webElementDragFrom, webElementDragTo).build().perform();
				Thread.sleep(5000);
			}
			
			if ((strProposalType !=null && strProposalType.equalsIgnoreCase("Amendment"))) {
				//Step 4: Select dates for existing offerings    //--//
				/*for (String strOfferingNameSel : arrstrOfferingNameSel) {
					objConfig.fn_SelectChangeStartDatePicker(driver, webWait, strOfferingNameSel).click();
		       		objConfig.fn_SelectChangeBillingDatePicker(driver, webWait, strOfferingNameSel).click();
				}*/
			}
			
			
			
			
			
			
			
			
			//TODO:: Capture Screenshot for after assigning product to offering 
			objUtils.fn_takeScreenshotFullPage(driver);
			
			objConfig.fn_clickSaveandReturn(driver, new WebDriverWait(driver,2)).click(); 	
			
			
			
			blnOfferingManagement =true;
        }
		catch (Exception ExceptionRemoveProd)
	    {
	            Log.error("ERROR: Unable to Manage Offering with exception reported as: "+ExceptionRemoveProd.getMessage());
	            StringWriter stack = new StringWriter();
	            ExceptionRemoveProd.printStackTrace(new PrintWriter(stack));
	            Log.debug("DEBUG: The exception in fn_OfferingManagement method is: "+stack);
	
	     }
		return blnOfferingManagement;
	}

	
	/*
	Author: Suhas Thakre
	Function fn_OfferingManagementNew : Assign product to respective product family.
	Inputs: strProductsDataSheet - Data related to assign product to respective offering.
	Outputs: Boolean response to check success or failure of function.
	 */
	public Boolean fn_OfferingManagementNew(WebDriver driver,WebDriverWait webWait ,String strProductsDataSheet) throws Throwable
	{
		
		//TODO:: Assign product to respective product family  
		/************************************************************************************************/
			//Step 1: Read Product Data and product family data from Add_Products_Data sheet. 
			//Step 2: Read offering name with help of product family.
			//Step 3: Assign product to Offering .
		/************************************************************************************************/
		
		ExcelHandler objExcel = new ExcelHandler();
		Configuration objConfig = new Configuration();
		Actions builder = new Actions(driver);
		Utils objUtils = new Utils();
		//EnvironmentDetails objEnv = (EnvironmentDetails) EnvironmentDetails.mapGlobalVariable.get("objEnv");
		List<Map<String, String>> lstProductMap = new ArrayList<Map<String,String>>();
		Map<String,String> mapOfferingNames = new HashMap<String,String>();
		File fileConfigValidations = new File(EnvironmentDetails.Path_DataSheet);
		String strProductFamily ="";
		String strProduct ="";
		String strOfferingName = "";
		WebElement webElementDragFrom ;
		WebElement webElementDragTo ;
		Boolean blnOfferingMang = false;
		
		try
		{
		Log.debug("DEBUG: Execution of function 'fn_OfferingManagementNew' Started.");
		objConfig.fn_clickOfferingMgmntBtn(driver, webWait).click();
		Thread.sleep(3500); 
		
		//Step 1
		lstProductMap = objExcel.fn_ReadExcelAsMap(fileConfigValidations,strProductsDataSheet); 
		
		//Step 2
		mapOfferingNames = (Map<String, String>) EnvironmentDetails.mapGlobalVariable.get("mapOfferingNames");
		
		
		//TODO:: Capture Screenshot Before adding product to offering
		objUtils.fn_takeScreenshotFullPage(driver);
		
		//Step 3
		for (Map<String, String> mapProduct : lstProductMap) {
			//Read Product Family
			strProductFamily = mapProduct.get("Product_Family").toString().trim();
			strProduct = mapProduct.get("Product_Name").toString().trim();
			strOfferingName = mapOfferingNames.get(strProductFamily);
			webElementDragFrom = objConfig.fn_clickDragProductFrom(driver, webWait, strProduct);
			webElementDragTo = objConfig.fn_clickDragToOffering(driver, webWait, strOfferingName);
			builder.dragAndDrop(webElementDragFrom, webElementDragTo).build().perform();
			Thread.sleep(3500);
			
		}

		//TODO:: Capture Screenshot after adding product to offering
		objUtils.fn_takeScreenshotFullPage(driver);
		
		objConfig.fn_clickGoToCart(driver, webWait).click();
		Thread.sleep(20000);
		}  
		catch (Exception ExceptionOfferingManagement) {
			Log.error("ERROR: Unable to Manage Offering  with exception reported as: "+ExceptionOfferingManagement.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionOfferingManagement.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_OfferingManagementNew method is: "+stack);
           
		}
		
		Log.debug("DEBUG: Execution of function 'fn_OfferingManagementNew' Completed.");
		blnOfferingMang = true;
		return blnOfferingMang;

	}
	
	
	/*
	Author: Suhas Thakre
	Function fn_OfferingManagementMul : Assign product to respective product family and Offering.
	Inputs: strProductsInfoDataSheet - Data related to Product names.
	Outputs: Boolean response to check success or failure of function.
	 */
	public Boolean fn_OfferingManagementMul(WebDriver driver,WebDriverWait webWait ,String strProductsInfoDataSheet, String strProductInfoWBName) throws Throwable
	{
		
		//TODO:: Assign product to respective product family  
		/************************************************************************************************/
			//Step 1: Read Product Names from Product_Information sheet. 
			//Step 2: Read Product family with help of Product name.
			//Step 3: Read Offering name with help of Product family.
			//Step 4: Assign product to respective offering.
		/************************************************************************************************/
		
		ExcelHandler objExcel = new ExcelHandler();
		Configuration objConfig = new Configuration();
		Utils objUtils = new Utils();
		File fileConfigValidations = new File(EnvironmentDetails.Path_DataSheet);
		List<Map<String, String>> lstProductInfoMap = new ArrayList<Map<String,String>>();
		List<Map<String, String>> lstFamilyInfoMap = new ArrayList<Map<String,String>>();
		Map<String,String> mapProductAndFamily = new HashMap<String,String>();
		Map<String,String> mapFamilyAndOffering = new HashMap<String,String>();
		Set<String> setstrProductName = new HashSet<String>(); 
		Actions builder = new Actions(driver);
		String strProductName = "";
		String strDefoultOfferingName = "";
		String strProductFamilyName = "";
		String strOfferingName = "";
		WebElement webElementDragFrom ;
		WebElement webElementDragTo ;
		Boolean blnOfferingMang = false;
		
		try
		{
		
		Log.debug("DEBUG: Execution of function 'fn_OfferingManagementMul' Started.");
	
		//Read mapProductAndFamily map from global variable map 
		mapProductAndFamily = (Map<String, String>) EnvironmentDetails.mapGlobalVariable.get("mapProductAndFamily");
		
		//Read mapFamilyAndOffering map from global variable map 
		mapFamilyAndOffering = (Map<String, String>) EnvironmentDetails.mapGlobalVariable.get("mapFamilyAndOffering");	
		
		//fetch default offering 
		lstFamilyInfoMap = objExcel.fn_ReadExcelAsMap(fileConfigValidations,"CreateOfferingData"); 	
		for (Map<String, String> mapFamily : lstFamilyInfoMap) {
			if (mapFamily.get("Uniquekey").toString().trim().equalsIgnoreCase(EnvironmentDetails.mapGlobalVariable.get("struniquekey").toString())) {
				String strDefoultProductFamilyName = mapFamily.get("Product_Family").toString().toString();
				strDefoultOfferingName = mapFamilyAndOffering.get(strDefoultProductFamilyName).toString();
			}	
				
		}
		//strDefoultOfferingName = mapFamilyAndOffering.get(lstFamilyInfoMap.get(0).get("Product_Family").toString()).toString();
		
		//Click on offering management button
		objConfig.fn_clickOfferingMgmntNABtn(driver, webWait).click();  
		objConfig.fn_clickAddNewOffering(driver, webWait).isEnabled();
		
		
		
		//Step 1: Read Product Name from Product_Information sheet.
		lstProductInfoMap = objExcel.fn_ReadExcelAsMap(fileConfigValidations,strProductsInfoDataSheet); 
		
		//TODO:: Capture Screenshot Before adding product to offering
		objUtils.fn_takeScreenshotFullPage(driver);
		
		//Search yes condition in all row of Product_Information sheet
		for (Map<String, String> mapProduct : lstProductInfoMap) {
			if (mapProduct.get("Uniquekey").toString().trim().equalsIgnoreCase(EnvironmentDetails.mapGlobalVariable.get("struniquekey").toString())) {
				String strToBeConfigured = mapProduct.get("To_Be_configured").toString().trim();
				if (strToBeConfigured.equalsIgnoreCase("Yes"))
				{	 
					setstrProductName.add(mapProduct.get("Product_Name").toString().trim());
					
				}
			}
		}
		
		for (String strProductNamel : setstrProductName) {
			
			strProductName = strProductNamel;
			//Step 2
			strProductFamilyName = mapProductAndFamily.get(strProductName).toString();
			
			//if offering not available for product
				try {
					//Step 3
					strOfferingName = mapFamilyAndOffering.get(strProductFamilyName).toString();
				} catch (Exception e) {
					// assign product to default offering
					strOfferingName = strDefoultOfferingName;
				}
			webElementDragFrom = objConfig.fn_clickDragProductFrom(driver, webWait, strProductName);
			webElementDragTo = objConfig.fn_clickDragToOffering(driver, webWait, strOfferingName);
			//Step 4
			builder.dragAndDrop(webElementDragFrom, webElementDragTo).build().perform();
			Thread.sleep(3500);
		}
		
	
		//TODO:: Capture Screenshot after adding product to offering
		objUtils.fn_takeScreenshotFullPage(driver);
		objConfig.fn_clickSaveandReturn(driver, new WebDriverWait(driver,2)).click(); 	
		objConfig.fn_clickOfferingMgmntNABtn(driver, webWait).isEnabled();  	
		
		}  
		catch (Exception ExceptionOfferingManagement) {
			Log.error("ERROR: Unable to Manage Offering  with exception reported as: "+ExceptionOfferingManagement.getMessage());
	        StringWriter stack = new StringWriter();
	        ExceptionOfferingManagement.printStackTrace(new PrintWriter(stack));
	        Log.debug("DEBUG: The exception in fn_OfferingManagementMul method is: "+stack);
	       
		}
		
		Log.debug("DEBUG: Execution of function 'fn_OfferingManagementMul' Completed.");
		blnOfferingMang = true;
		return blnOfferingMang;
	}
	
	/*
	Author: Suhas Thakre
	Function fn_GeneratePresentAcceptProposal : Generate Present Accept Proposal.
	Inputs: Null
	Outputs: Boolean response to check success or failure of function .
	 */
	//proposal Flow generation
	public Boolean fn_GeneratePresentAcceptProposal(WebDriver driver,WebDriverWait webWait)throws Exception
	{
		String strApprovalStagetext="";
		//Boolean blnIsAlertPresent=false;
		Boolean blnMSAGenerationAndActivation = false;
		OrderFlow objOrderFlow = new OrderFlow();
		//After_Approval_Process_PO objAfterPO = new After_Approval_Process_PO();
		Utils objUtils = new Utils();
		Account objacctpage = new Account();
		Thread.sleep(8000);
		String strNewproposalName=EnvironmentDetails.mapGlobalVariable.get("strNewproposalName").toString();

		try {
			objacctpage.fn_setSearchField(driver, webWait).sendKeys(strNewproposalName); 
			objacctpage.fn_clickSearchBtn(driver, webWait).click();
			Thread.sleep(3500);
			driver.findElement(By.partialLinkText(strNewproposalName)).click();
			driver.navigate().refresh();
			Thread.sleep(3500);
			
			//TODO:: Capture Screenshot for proposal status change to approved 
			objUtils.fn_takeScreenshot(driver);
			
			//Generate Agreement 
			objOrderFlow.fn_clickGenerate(driver, webWait).click();
			Thread.sleep(3500);
			objOrderFlow.fn_clickWaterMark(driver, webWait).click();
			Thread.sleep(3500);
			
			//Present Agreement 
			objOrderFlow.fn_clickProposalToPresent(driver, webWait).click();
			Thread.sleep(3500);
			objOrderFlow.fn_clickProposalGenerateBtn(driver, webWait).click();
			Thread.sleep(3500);	
			
			WebElement elemntReturnBtn= webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@value='Return' and @type='submit']")));
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elemntReturnBtn);
			Thread.sleep(3500);	
			elemntReturnBtn.click();

			//objOrderFlow.fn_clickReturnBtn(driver, webWait).click();
			Thread.sleep(5000);
			strApprovalStagetext=objOrderFlow.fn_getTextApprovalStage(driver, webWait).getText();
			if (strApprovalStagetext.contentEquals("Generated"))
				Log.debug("DEBUG: Approval stage is successfully changed to Generated.");
			else
				Log.debug("DEBUG: Approval stage is not changed to Generated"+strApprovalStagetext);
			objOrderFlow.fn_clickPresent(driver, webWait).click();
			Thread.sleep(5000);
			
			Thread.sleep(3500);
			objUtils.fn_HandleLookUpWindow(driver, webWait,"Contact Lookup (New Window)","Apptus");
			Thread.sleep(4000);
			objOrderFlow.fn_setTextinSubject(driver, webWait).sendKeys("TestAutomation");
			Thread.sleep(3500);
			objOrderFlow.fn_setTextinEmailBody(driver, webWait).sendKeys("TestAutomation");
			Thread.sleep(3500);
			objOrderFlow.fn_clickPropAttchmnt(driver, webWait).click();
			Thread.sleep(3500);
			objOrderFlow.fn_clickSendEmailBtn(driver, webWait).click();
			Thread.sleep(8000);
			
			//Accept Agreement
		    objOrderFlow.fn_clickAccept(driver, webWait).click();
		    Thread.sleep(8000);
		    blnMSAGenerationAndActivation = true;
		} catch (Exception ExceptionGntQuote) {
			
			Log.error("ERROR: Unable to generate proposal with exception reported as: "+ExceptionGntQuote.getMessage());
            StringWriter stack = new StringWriter();
            ExceptionGntQuote.printStackTrace(new PrintWriter(stack));
            Log.debug("DEBUG: The exception in fn_GeneratePresentAcceptProposal method is: "+stack);

		}
		return blnMSAGenerationAndActivation;
	}
	
	/*
	Author: Suhas Thakre
	Function fn_OrderFlow_Generate_Activate_Amend: Generate and Activate Order.
	Inputs: strEmailForRecipient - Email ID to send Document for signature.
	Outputs: Boolean response to check success or failure of function .
	 */
	////OrderFlowGeneration and Activation for Amendment
		public Boolean fn_OrderFlow_Generate_Activate_Amend(WebDriver driver,WebDriverWait webWait, String strEmailForRecipient) throws Throwable
		{
			OrderFlow objOrderFlow = new OrderFlow();
			AfterApproval objAfterPO = new AfterApproval();
			Utils objUtils = new Utils();
			Boolean blnIsAlertPresent=false;
			Boolean blnOrderFlwGenerationAndActivation=false;
			String strAgrmntName="";
			String strAgrmntNub="";
			Map<String, Object> mapOutputData = new HashMap<String, Object>();
			List<Map<String, Object>> lisOutputData = new ArrayList<>();
			List<List<Map<String, Object>>> lstmapOutputData = new ArrayList<>();
			WriteExcel objWriteExcel = new WriteExcel();
			String strProposalType = EnvironmentDetails.mapGlobalVariable.get("strProposalType").toString();
			
			try {
				
				
				//To perform Renewal or Amendment
				if (strProposalType.equalsIgnoreCase("Renewal")) {
					objOrderFlow.fn_clickRenewalBtn(driver, webWait).click();
				}else if (strProposalType.equalsIgnoreCase("Amendment")) {
					objOrderFlow.fn_clickAmendBtn(driver, webWait).click();
				}
				
				Thread.sleep(3500);
				objOrderFlow.fn_clickContinueBtn(driver, webWait).click();
				
			    Thread.sleep(3500);
			    objUtils.fn_HandleLookUpWindow(driver, webWait,"Address Lookup (New Window)","LAC");
			    Thread.sleep(3500);
			    Date date = new Date() ;
	    		SimpleDateFormat dateFormat = new SimpleDateFormat("mmss");  		
	        	strAgrmntName = "TestAutomationAcct"+dateFormat.format(date)+"_Order Form_Amended";
	        	objAfterPO.fn_getAgrmntNameInputBox(driver, webWait).clear();
	        	objAfterPO.fn_getAgrmntNameInputBox(driver, webWait).sendKeys(strAgrmntName);
	        	Thread.sleep(3500);
			    
			    objOrderFlow.fn_clickOrderFrmSaveBtn(driver, webWait).click();
			    Thread.sleep(4000);
			    //capture Agreement Name
			    strAgrmntName = objAfterPO.fn_getAgrmntName(driver, webWait).getText();
			    strAgrmntNub = objAfterPO.fn_getAgrmntNumber(driver, webWait).getText();
			    Log.debug("DEBUG: Saved Agreement Name in global variable map with key strAgrmntName");
				EnvironmentDetails.mapGlobalVariable.put("strAgrmntName", strAgrmntName);
				
				//TODO:: Capture Screenshot for Agreement details 
				objUtils.fn_takeScreenshotFullPage(driver);
				
				//write data in output file 
				mapOutputData.put("Agreement Number", strAgrmntNub);
				mapOutputData.put("Agreement Name", strAgrmntName);
				lisOutputData.add(mapOutputData);
				lstmapOutputData.add(lisOutputData);
				objWriteExcel.fn_WriteExcel(lstmapOutputData, EnvironmentDetails.strFQNFileNameWrite, "Agreement_Details");
				
				
			    objOrderFlow.fn_clickGenerateSpprtDocsBtn(driver, webWait).click();
			    Thread.sleep(3500);
			    objOrderFlow.fn_clickWaterMark(driver, webWait).click();
			    Thread.sleep(3500);
			    objOrderFlow.fn_clickSelectTempOnOrderFrm(driver, webWait).click();
			    Thread.sleep(4000);
			    objOrderFlow.fn_clickOrdrFrmGnrtBtn(driver, webWait).click();
			    Thread.sleep(50000);
			    
			    try{   
				    Alert alert = driver.switchTo().alert();
				    Log.debug("DEBUG: The message thrown from alert is :: "+alert.getText());
				    alert.accept();
				    blnIsAlertPresent=true;
			    	  }catch(Exception e){ 
			    		  Log.debug("DEBUG: Unexpected alert is not not present.");   
			    	  }
			    
			    Thread.sleep(80000);
			    if (blnIsAlertPresent==false){
			    	
					WebElement elemntReturnBtn1= webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@value='Return' and @type='submit']")));
					((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elemntReturnBtn1);
					Thread.sleep(1000);	
					elemntReturnBtn1.click();
				}
				Thread.sleep(8000);
		        driver.navigate().refresh();
		        objOrderFlow.fn_clickSendForReview(driver, webWait).click();
			    Thread.sleep(6000);
			    objOrderFlow.fn_clickSelectAttachment(driver, webWait).click();
			    Thread.sleep(3500);
			    objOrderFlow.fn_clickNextBtn(driver, webWait).click();
			    //Select Amendments and Renewals check box
			    objOrderFlow.fn_clickAmendmentsRenewalscheckbox(driver, webWait).click();
			    Thread.sleep(4000);
			    //Select attachment and proced 
			    try {
			    	objOrderFlow.fn_clickSelectAttachment(driver, webWait).click();
				} catch (Exception ExceptionAttachment) {
					Log.debug("DEBUG: No Attachments From available for Agreement.");
				}
			    Thread.sleep(3500);
			    objOrderFlow.fn_clickNextBtn(driver, webWait).click();
			    //Select Email Template
			    objOrderFlow.fn_clickEmailTemplateAmendement(driver, webWait).click();
			    Thread.sleep(4000);
			    //objOrderFlow.fn_clickNextBtnEmailTemplate(driver, webWait).click();
			    
				WebElement elemntNextBtn1= objOrderFlow.fn_clickNextBtnEmailTemplateAmendement(driver, webWait);
				((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", elemntNextBtn1);
				Thread.sleep(3500);	
				elemntNextBtn1.click();
			    
			    Thread.sleep(4000);
			    //Send email with Sungardas template
			    objUtils.fn_HandleLookUpWindow(driver, webWait,"Reports To Lookup (New Window)","Apptus");
			    Thread.sleep(3500);
			    objOrderFlow.fn_clickEmailSendBtn(driver, webWait).click();			    
			    //Docusignprocess to provide Recipient 
			    objOrderFlow.fn_clickESignatureBtn(driver, webWait).click();
			    Thread.sleep(3500);
			    objOrderFlow.fn_clickAddRecipientsBtn(driver, webWait).click();
			    Thread.sleep(3500);
			    objOrderFlow.fn_setNameForRecipient(driver, webWait).sendKeys("Suhas");
			    Thread.sleep(3500);
			    objOrderFlow.fn_setEmailForRecipient(driver, webWait).sendKeys(strEmailForRecipient);
			    Thread.sleep(10000);
			    
			    if (objOrderFlow.fn_clickSendForEsignatureBtn(driver, webWait).isDisplayed())
			    	blnOrderFlwGenerationAndActivation=true;
			    
			    objOrderFlow.fn_clickSendForEsignatureBtn(driver, webWait).click();
			    Thread.sleep(15000);
			    blnOrderFlwGenerationAndActivation = true;
			} catch (Exception ExceptionActiveOrd) {
				
				Log.error("ERROR: Unable to Activate Order with exception reported as: "+ExceptionActiveOrd.getMessage());
	            StringWriter stack = new StringWriter();
	            ExceptionActiveOrd.printStackTrace(new PrintWriter(stack));
	            Log.debug("DEBUG: The exception in fn_OrderFlow_Generate_Activate_Amend method is: "+stack);
			}	   
			
			return blnOrderFlwGenerationAndActivation;
			
			
		}
		
		
		public void fn_waitForPriceUpdate(WebDriver driver) throws Throwable
	    {
	        WebDriverWait wait = new WebDriverWait(driver, 120);
	        try{
	           // Log.info("Waiting for spinner to disappear..");
	            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//img[@src='/img/loading.gif']")));
	            Thread.sleep(3500);
	            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Updating Price, please wait...']")));
	            wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("(//img[contains(@src,'apttus_config2__Image_LoadingPage')])[2]")));
	            Thread.sleep(3500);
	          //  Log.info("Finished Waiting..");
	        }
	        catch(TimeoutException e)
	        {
	        	Log.info("Timed out waiting for spinner to disappear...");
	        }
	    }
	
}
