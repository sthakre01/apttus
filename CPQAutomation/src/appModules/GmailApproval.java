package appModules;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.SalesForce.AutomateGmailApprovalProcessPO;
import utility.EnvironmentDetails;
import utility.Log;


public class GmailApproval{
	
	public boolean fn_AutomateGmailApprovalProcess(WebDriver driver,WebDriverWait webWait,String strEmailDetails,String strTypeofFlow) throws Exception{
		
		boolean blnAutomateGmailApprovalProcess = false;
		AutomateGmailApprovalProcessPO objGmail = new AutomateGmailApprovalProcessPO();
		String arrEmailDetails[]       =   strEmailDetails.split("#");
		String strchildWindow = null;
		String strParentWindow = null;
		String strEmailWindow = null;
		String strReviewWindow = null;
		String strNewproposalName = EnvironmentDetails.mapGlobalVariable.get("strNewproposalName").toString();

		webWait = new WebDriverWait(driver,15);
	    strParentWindow = driver.getWindowHandle();  // get the current window handle
	    //Gets the new window handle	  
	    Robot robot = new Robot();
	    robot.keyPress(KeyEvent.VK_CONTROL);
	    robot.keyPress(KeyEvent.VK_T);
	    robot.keyRelease(KeyEvent.VK_CONTROL);
	    robot.keyRelease(KeyEvent.VK_T);
	    Thread.sleep(2000);
		//TODO: get the frame for the Child window
		Set<String> strWindowHandles = driver.getWindowHandles();   		
		strWindowHandles.remove(strParentWindow);
		strchildWindow = strWindowHandles.iterator().next();		
		driver.switchTo().window(strchildWindow);
		driver.navigate().to("https://mail.google.com");
		Thread.sleep(5000);	
		
		//Try Catch Block for handling the  Value setting of UserID Field
		try {        			
			objGmail.fn_setEmail(driver, webWait).sendKeys(arrEmailDetails[0]);
			Log.info("Value for UserID is not set and proceeding with setting up the UserID");}
		catch(Exception e){	 
			Log.error("Value for UserID is already set and proceeding with setting up the pwd");
			objGmail.fn_setPwd(driver, webWait).click();
			Thread.sleep(1000);
			objGmail.fn_setPwd(driver, webWait).sendKeys(arrEmailDetails[1]);}
		
		Thread.sleep(1000);
		objGmail.fn_clickSignIn(driver, webWait).click();	
		webWait = new WebDriverWait(driver,60);
		Thread.sleep(10000);
		driver.switchTo().activeElement().sendKeys(Keys.ESCAPE);
		Thread.sleep(5000);
		objGmail.fn_setSearchBar(driver, webWait).click();
		Thread.sleep(5000);
		
        if (strTypeofFlow.contentEquals("Approval")){
			objGmail.fn_setSearchBar(driver, webWait).sendKeys(strNewproposalName);
			Thread.sleep(5000);
			objGmail.fn_setSearchBar(driver, webWait).sendKeys(Keys.ENTER);
			Thread.sleep(1000);
		    WebElement webTable = objGmail.fn_tblInbox(driver, webWait);
			List<WebElement> TotalRowCount = webTable.findElements(By.tagName("tr"));	
			for(WebElement webrow : TotalRowCount)
	        {
			Thread.sleep(5000);
			int totalmailcount = TotalRowCount.size();
			webrow.click();
			Thread.sleep(5000);
				for(int i=1;i<=totalmailcount;i++ ){
					objGmail.fn_clickReplyBtn(driver, webWait).click();
					Thread.sleep(1500);
					objGmail.fn_setMsgBody(driver, webWait).sendKeys("APPROVE");
					Thread.sleep(1500);
					objGmail.fn_clickSendBtn(driver, webWait).click();
					Thread.sleep(5500);	
					objGmail.fn_clickOlderBtn(driver, webWait).click();
					Thread.sleep(1500);	}
				break;}	}
		else{
			objGmail.fn_setSearchBar(driver, webWait).sendKeys("Request for eSignatures. Please review and sign.");
			Thread.sleep(5000);
			objGmail.fn_setSearchBar(driver, webWait).sendKeys(Keys.ENTER);
			Thread.sleep(5000);
		    WebElement webTable = objGmail.fn_tblInbox(driver, webWait);
			List<WebElement> TotalRowCount = webTable.findElements(By.tagName("tr"));	
			for(WebElement webrow : TotalRowCount)
	        {
			Thread.sleep(5000);
			webrow.click();
			Thread.sleep(5000);
			break;}	
			strEmailWindow = driver.getWindowHandle();
			//Docusign Process
			objGmail.fn_clickReviewBtn(driver, webWait).click();
			Thread.sleep(5000);		
		      for (String handle : driver.getWindowHandles()) {
	                 driver.switchTo().window(handle);}		
			Thread.sleep(5000);
			//objGmail.fn_clickIAcceptBtn(driver, webWait).click();
			Thread.sleep(500);		
			strReviewWindow = driver.getWindowHandle();
			Thread.sleep(5000);
			if (strTypeofFlow.contentEquals("DocuSignProcessForOrder")){
				
				try {
					Thread.sleep(5000);
					objGmail.fn_clickAgreeAndAccept(driver, webWait).click(); }          		
				catch(Exception excpEnvironment1){
					Log.info("Continue Button is not present in the Docusign of Order");}		
			Thread.sleep(5000);
			objGmail.fn_clickContinueBtn(driver, webWait).click();
			Thread.sleep(5000);}
			
			if (strTypeofFlow.contentEquals("DocuSignProcessFORGMSA")){
				try{
					
					objGmail.fn_clickAgreeAndAccept(driver, webWait).click();
					Thread.sleep(5000);
					objGmail.fn_clickContinueBtn(driver, webWait).click();
					Thread.sleep(5000);
					objGmail.fn_clickNavigateBtn(driver, webWait).click();
					Thread.sleep(5000);
					WebElement svgObject = objGmail.fn_clickSignBtn(driver, webWait);
					Actions builder = new Actions(driver);
					builder.click(svgObject).build().perform();
					//objGmail.fn_clickSignBtn(driver, webWait).click();
					Thread.sleep(5000);
				    }
			    catch(Exception excpEnvironment){
			    
					try {			
						objGmail.fn_clickContinueBtn(driver, webWait).click(); }          		
					catch(Exception excpEnvironment1){
						Log.info("Continue Button is not present in the Docusign of GMSA");}	
				Thread.sleep(5000);	
				objGmail.fn_clickNavigateBtn(driver, webWait).click();
				Thread.sleep(3000);
				WebElement svgObject = objGmail.fn_clickSignBtn(driver, webWait);
				Actions builder = new Actions(driver);
				builder.click(svgObject).build().perform();
				//objGmail.fn_clickSignBtn(driver, webWait).click();
				Thread.sleep(3000);			
			    }}	
			
			if (strTypeofFlow.contentEquals("DocuSignProcessForOrder")){
			driver.switchTo().frame(driver.findElement(By.id("signingIframe")));		
			objGmail.fn_clickDragFrom(driver, webWait).click();
			WebElement webElementDragFrom = objGmail.fn_clickDragFrom(driver, webWait);
			
			if (webElementDragFrom.isDisplayed())
				Log.info("the frame is identified and reached to dragfrom button");
			else
				Log.info("the frame is not identified and not reached to dragfrom button");
					
			WebElement webElementDragTo = objGmail.fn_clickDragTo(driver, webWait);
			Actions builder = new Actions(driver);
			builder.dragAndDrop(webElementDragFrom, webElementDragTo).build().perform();
			Thread.sleep(10000);}
			
			//String strDocusignWindow = driver.getWindowHandle();	
			//TODO :: To Handle Multiple windows
		    for (String handle : driver.getWindowHandles()) {
	                 driver.switchTo().window(handle);}	    
		   Thread.sleep(10000);   
		   objGmail.fn_clickAdoptandSign(driver, webWait).click();   	   
		   driver.switchTo().defaultContent();   
		   driver.switchTo().window(strReviewWindow);  
		   Thread.sleep(5000);
		   objGmail.fn_clickFinish(driver, webWait).click();
		   driver.close();
		   Thread.sleep(5000);
		   driver.switchTo().window(strEmailWindow);
			
		}
        
        //TODO:: common code for both the approval and Docusign process
	    objGmail.fn_clickSignoutImg(driver, webWait).click();
		Thread.sleep(5000);
		if (objGmail.fn_clickSignoutBtn(driver, webWait).isDisplayed())
			blnAutomateGmailApprovalProcess=true;	
		objGmail.fn_clickSignoutBtn(driver, webWait).click();
		Thread.sleep(5000);
		driver.close();
		Thread.sleep(5000);
		driver.switchTo().window(strParentWindow);
   		return blnAutomateGmailApprovalProcess;}
     

     }

	

