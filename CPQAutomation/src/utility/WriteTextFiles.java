package utility;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

public class WriteTextFiles{

	//TODO:: write the Account Names to file:: notepad 
	//Input Parameters:: Path of the File,Finalist of Acctnmes
	public Boolean fn_writeAcctName(ArrayList<String> finallist,String strPathofFile) throws FileNotFoundException, UnsupportedEncodingException{

		FileOutputStream outputStream = new FileOutputStream(strPathofFile);
		
		OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-16");
		
		BufferedWriter bufferedWriter = new BufferedWriter(outputStreamWriter);		
	
		try {
			
				for (int i=0;i<finallist.size();i++) 
				{
					
					bufferedWriter.write(finallist.get(i));
					bufferedWriter.newLine();
									
				}
				
				bufferedWriter.flush();
				bufferedWriter.close();
				
		   } catch (IOException exception) 
		     {
		    	StringWriter stack = new StringWriter();
		    	exception.printStackTrace(new PrintWriter(stack));
				Log.debug("DEBUG: The exception in Generating Multiple Accounts is: "+stack);
			}
         return true;
	}
	
	
}