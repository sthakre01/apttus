package utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;


import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.text.PDFTextStripper;




public class PDFDocsHandler 
{
	
	public static void main(String[] arg) 
	{
		
		PDDocument document = null;
		JMenuItem jmiOpen;

		try {
			  
		      FileInputStream file = new FileInputStream(new File("C:\\CPQ_Automation\\APT_Financial_Services_Limited-GMSA_Original_GMSA_Dynamic.pdf"));
    	        document = PDDocument.load(file);
    	        JMenuBar mb = new JMenuBar();
    	        JMenu fileMenu = new JMenu("File");
    	        mb.add(fileMenu);
    	        fileMenu.add(jmiOpen = new JMenuItem("Open")); 
 

		        document.setAllSecurityToBeRemoved(true);
		        AccessPermission ap = document.getCurrentAccessPermission();
		           if( ! ap.canExtractContent() )
		           {
		               throw new IOException( "You do not have permission to extract text" );
		           }
		       // PDFTextStripper pdfStrip = new PDFTextStripper();
		       // String text = pdfStrip.getText(document); 
		        
		       		        
		        PDFTextStripper reader = new PDFTextStripper();
		        
		        //reader.showTextStrings(arg0);
		        reader.setStartPage(2);
		        reader.setEndPage(2);
		        reader.setParagraphStart("This Global Master");
		        
		        //String pageText = reader.getText(document);
		        
		        reader.beginText();
		        
		       //System.out.println("the page text is :: "+pageText);
		        //System.out.println("the text is ::"+text);
		        //PDDocumentInformation info = document.getDocumentInformation();
		        
		        document.save(new FileOutputStream("C:\\CPQ_Automation\\APT_Financial_Services_Limited-GMSA_Original_GMSA_Dynamic.docx"));
		        document.close();
		        
    	    
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
}

}
