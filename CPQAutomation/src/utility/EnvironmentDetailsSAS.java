/* 
==============================================================
 Author     		:	Umesh Kute
 Class Name		 	: 	EnvironmentDetails
 Purpose     		: 	Declaring the environment variables that will be used for WordPress application.
 Date       		:	02/06/2015
 Version Information:	Version1.0
 PreCondition 		:	None
 Test Steps 		:	1. Enable the appropriate values for application URL and browser type for application
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
 #======================================================================
 */

package utility;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

//import org.apache.log4j.xml.DOMConfigurator;
//import org.openqa.selenium.WebDriver;

/*
This class contains the declaration for all environment variables used
@version: 1.0
@author: Umesh Kute
*/
public class EnvironmentDetailsSAS

{

	public static final String browser = "Chrome";
	
	Utils objExecEnv = new Utils();

	public final String dataSheetPath = System.getProperty("user.dir")
			+System.getProperty("file.separator")+ "testData"+ System.getProperty("file.separator") + "TestData.xlsx";
	
	public List<Map<String, String>> lstmapExecEnv = objExecEnv.readexcel_ExecEnvironment(dataSheetPath,"ExecEnv");
	public String STRUSERNAME = "";
	public String STRPASSWORD = "";
	public String STRURL = "";

	
	//public final String STRUSERNAME = lstmapExecEnv.get("InstUserName");
	//public final String STRPASSWORD = lstmapExecEnv.get("InstPassWord");
	//public final String STRURL= lstmapExecEnv.get("InstanceURL");
	

	// Drivers details
	public static final String Path_ChromDriver = new File("Brwser_Driverssrc").getAbsolutePath() + "\\chromedriver.exe";
	public static final String Path_IeDriver = new File("Brwser_Driverssrc").getAbsolutePath() + "\\IEDriverServer.exe";
	public static final String Path_FirefoxDriver = "C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe";
	public static final String WORKSHEETS_PATH = "C:\\CPQ_Automation\\trunk\\src\\testData\\Products_Pricing";

	// This parameter refers to the path where screenshot of page,
	// during the test case execution is taken.  
	public static SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy-hhmm");//("ddMMyyyy-hhmmss")
	public static Date curDate = new Date();
	public static String strDate = sdf.format(curDate);
	public static String Path_ScreenShot = new File("Screenshots")
			.getAbsolutePath() + "/Screenshots"+ "_" +strDate + "/";
	public static String CHROME_DRIVER_PATH = System.getProperty("user.dir")
			+ System.getProperty("file.separator") + "Brwser_Driverssrc" +System.getProperty("file.separator") + "chromedriver.exe";
	
	public static String IE_DRIVER_PATH = System.getProperty("user.dir")
			+ System.getProperty("file.separator") + "Brwser_Driverssrc" +System.getProperty("file.separator") + "IEDriverServer.exe";


		
	//TODO: Default Constructor, which will always return the instance details with "Y" as value for TestCaseNameColumn.
     public EnvironmentDetailsSAS() {

		this("Y");
	}


	//TODO: Parameterized Constructor, which will return the Instance details based on Parameterized Instance.	
	public EnvironmentDetailsSAS(String strEnv) {
				
		for(Map<String, String> map : lstmapExecEnv) {
			
			if(map.get("Environment").equalsIgnoreCase("INT") && strEnv.equalsIgnoreCase("INT")) {
				
				Log.info("\nEnvironment selected for testing is: "+map.get("Environment"));
				STRUSERNAME = map.get("InstUserName");
				STRPASSWORD = map.get("InstPassWord");
				STRURL = map.get("InstanceURL");
				break;
			}			
			if(map.get("Environment").equalsIgnoreCase("UAT") && strEnv.equalsIgnoreCase("UAT")) {
				
				Log.info("\nEnvironment selected for testing is: "+map.get("Environment"));
				STRUSERNAME = map.get("InstUserName");
				STRPASSWORD = map.get("InstPassWord");
				STRURL = map.get("InstanceURL");
				break;
			}
			if(map.get("Environment").equalsIgnoreCase("DEV") && strEnv.equalsIgnoreCase("DEV")) {
				
				Log.info("\nEnvironment selected for testing is: "+map.get("Environment"));
				STRUSERNAME = map.get("InstUserName");
				STRPASSWORD = map.get("InstPassWord");
				STRURL = map.get("InstanceURL");
				break;
			}
			if(map.get("TestCaseNameColumn").equalsIgnoreCase("Y") && strEnv.equalsIgnoreCase("Y")) {
			
				Log.info("\nDefault Environment selected for testing is: "+map.get("Environment"));
				STRUSERNAME = map.get("InstUserName");
				STRPASSWORD = map.get("InstPassWord");
				STRURL = map.get("InstanceURL");
				break;
			}
		}
	}
	
}	