/* 
============================================================================================================================
 Author     		:	Aashish Bhutkar
 Modified By		:	Aashish Bhutkar
 
 Class Name		 	: 	API_SOAP
 
 Purpose     		: 	Enlist all SOAP call functions supported for testing.
 
 Date       		:	11/08/2016
 Modified Date		:	02/20/2017, 01/09/2017, 01/06/2017, 12/14/2016
 Version Information:	Version 3.0
 
 PreCondition 		:	Currently calls only SOAP calls for GET .
 Test Steps 		:	1. Based on the API under test for SOAP, call GET to execute the call.
 						2. Based on the requests for SQL / Self Validations, conduct those.
 						
 Version Change 1.1	:	1. Changed usage of HashMap to Map with generic Object class for it's values. 						
 Version Change 1.2	:	1. Modified function to refer to the "fn_GetNormalisedListMap" in Utils for 
 						   normalising responses.
 Version Change 1.3	:	1. Updated the signature for "fn_SOAPParser" as per changes in it's definition.
 Version Change 2.0	:	1. Updated the API call by handling special characters which are part of XML.
 						2. Created function "fn_ValidateSOAPResponse" to handle SOAP responses which don't have any
 						   fields requested as part of the call as it's response.
 Version Change 3.0	:	1. Updated the "fn_SOAPBuilder" signature to handle the Map called for Update calls.
  						2. Created a new function "fn_SOAP_POST" for update request using the SOAP protocol. 
 
 Copyright notice	:	Copyright(C) 2016 Sungard Availability Services - All Rights Reserved 
 ============================================================================================================================
 */

package utility;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.InputStreamEntity;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;


public class API_SOAP extends VerificationMethods {


/*
	Author: Aashish Bhutkar
	Function fn_SOAP_GET: SOAP HTTP for GET call.
	Inputs: objEnvironment - Environment Object which governs the execution environment; read from EnvironmentDetails class.
			hmapTestCases - Test Cases sheet read from the TestData workbook.
			strFQNFileNameWrite - FQN for the excel file to write the response.
			strWorksheetName - Specifies the Worksheet name from Results workbook @ run time to look for the data.
	Outputs: List of Map is the output which actually is the parsed response of the call made.
*/

	public List<List<Map<String, String>>> fn_SOAP_GET(EnvironmentDetails objEnvironment, Map<String, Object> mapTestCases 
										, Map<String, String> mapSOAP) throws Exception {
				
		SOAP_Builder objBuildSOAP = new SOAP_Builder();
		SOAP_Parser objSOAPParser =  new SOAP_Parser();
		
		List<List<Map<String, String>>> lstmapParsedResponse = new ArrayList<List<Map<String,String>>>();
		String strBuildSOAP_GETCall = "";
		
		try {			
			
			String strSessionID = "";
			String strCallType = ((String) mapTestCases.get("CallType")).trim();
			
			//Set Default SOAP login credential 
			if (mapSOAP == null) {
				mapSOAP = new HashMap<String, String>();
				mapSOAP.put("UserID", "SOAP_UserID");
    			mapSOAP.put("Password", "SOAP_Password");
			}
			String strBuildSOAP_Login = objBuildSOAP.fn_SOAPBuilder(objEnvironment, "login", "", "", mapSOAP);
			Log.debug("DEBUG: The generated XML is: "+strBuildSOAP_Login);
			HttpPost httpSOAP = new HttpPost(EnvironmentDetails.mapEnvironmentDetails.get("SOAP_Endpoint").trim());
			httpSOAP.setEntity(new InputStreamEntity(IOUtils.toInputStream(strBuildSOAP_Login, "UTF-8") , strBuildSOAP_Login.length()));
			httpSOAP.setHeader("Content-type", "text/xml; charset=UTF-8");
			httpSOAP.setHeader("SOAPAction", "Define");
			HttpClient client = HttpClients.createDefault();
			HttpResponse response = client.execute(httpSOAP);
			Log.debug("DEBUG: Pure Response: "+response);
			String res_xml = EntityUtils.toString(response.getEntity());
			Log.debug("DEBUG: Reponse as string: "+res_xml);
			lstmapParsedResponse = objSOAPParser.fn_SOAPParser(res_xml, "login");
			
			//now get the session ID to be used for the subsequent call.
			for(List<Map<String, String>> lst : lstmapParsedResponse)
				for(Map<String, String> map: lst)
					strSessionID = map.get("sessionId").toString();
			
			//get the SQL call for the SOAP call to be made.
			String strCallingData = ((String) mapTestCases.get("CallingData")).trim();
			//handling special characters during the XML creation.
			strCallingData = strCallingData.replaceAll("&", "&amp;");
			strCallingData = strCallingData.replaceAll("<", "&lt;");
			strCallingData = strCallingData.replaceAll(">", "&gt;");
			strCallingData = strCallingData.replaceAll("'", "\\'");
			
			if (strCallType.equalsIgnoreCase("query")) {
			 strBuildSOAP_GETCall = objBuildSOAP.fn_SOAPBuilder(objEnvironment, strCallType, strSessionID, strCallingData, null);
			 httpSOAP = new HttpPost(EnvironmentDetails.mapEnvironmentDetails.get("SOAP_Endpoint").trim());
			}else if (strCallType.equalsIgnoreCase("update")) {
			 strBuildSOAP_GETCall = objBuildSOAP.fn_SOAPBuilder(objEnvironment, strCallType, strSessionID, strCallingData, mapSOAP);	
			 httpSOAP = new HttpPost(EnvironmentDetails.mapEnvironmentDetails.get("SOAP_Endpoint").trim());
			}else if (strCallType.equalsIgnoreCase("apex")) {
				 strBuildSOAP_GETCall = objBuildSOAP.fn_SOAPBuilder(objEnvironment, strCallType, strSessionID, strCallingData, mapSOAP);
				 httpSOAP = new HttpPost(EnvironmentDetails.mapEnvironmentDetails.get("Apex_Endpoint").trim());
				// httpSOAP = new HttpPost("https://sungardas--ebintb.cs44.my.salesforce.com/services/Soap/s/39.0");
			}
			
			
			Log.debug("DEBUG: The generated XML is: "+strBuildSOAP_GETCall);
			//httpSOAP = new HttpPost(objEnvironment.mapEnvironmentDetails.get("SOAP_Endpoint").trim());
			httpSOAP.setEntity(new InputStreamEntity(IOUtils.toInputStream(strBuildSOAP_GETCall, "UTF-8") , strBuildSOAP_GETCall.length()));
			httpSOAP.setHeader("Content-type", "text/xml; charset=UTF-8");
			httpSOAP.setHeader("SOAPAction", "Define");
			client = HttpClients.createDefault();
			response = client.execute(httpSOAP);
			Log.debug("DEBUG: Pure Response: "+response);
			res_xml = EntityUtils.toString(response.getEntity());
			Log.info("Raw SOAP GET Reponse is: "+res_xml);
			lstmapParsedResponse = objSOAPParser.fn_SOAPParser(res_xml, "query");
			
			
		}
		catch (Exception exceptionSOAPGET) {

			StringWriter stack = new StringWriter();
			exceptionSOAPGET.printStackTrace(new PrintWriter(stack));
			Log.debug("DEBUG: The exception recorded in API SOAP class is: "+stack);
			assertFalse(true, "ERROR: Error while creating the SOAP Response with exception as: "+exceptionSOAPGET);
		}		
		
		return lstmapParsedResponse;
	}


	
	

/*
	Author: Aashish Bhutkar
	Function fn_ValidateSOAPResponse: Validate SOAP response for any missing fields queried for.
	Inputs: strCallingData - The actual SOAP call i.e. Query called for response.
			lstmapParsedResponse - The SOAP response received after Parsing.
	Outputs: List of Map which will have all field which were queried for.
*/
	public List<List<Map<String, Object>>> fn_ValidateSOAPResponse(String strCallingData, 
												List<List<Map<String, Object>>> lstmapParsedResponse) {
		
		List<List<Map<String, Object>>> lstmapValidatedResponse = new ArrayList<List<Map<String,Object>>>();
		Boolean blnInnerQueryExists = false;
				
		//TODO: Parse the Calling data i.e. query to know which fields have been asked for data.
		/*	
		 * Step 1: Create list of all the fields mentioned in the query.
		 * Step 2: Check if ONLY * is seen, if so, prepare the final map w/o Table alias and send back the Map List. 
		 * Step 3: Cleanse the list generated in Step 1 with keywords like Distinct and others. 
		 * Step 4: Parse the Response Map to identify if any field mentioned in Step1 is missing.
		 * 		   and if found missing, it will be added to the final as null value and the list Map returned.
		*/
		
		//Step 1:
		String strLocalCallingData = strCallingData/*.toLowerCase()*/;
		do {	//Manipulate the Inner Queries primarily and then find the fields from main query.
			
			if(strLocalCallingData.toLowerCase().contains("(sel")) {

				blnInnerQueryExists = true;
				String strBeforeOpenBracket = strLocalCallingData.substring(0, strLocalCallingData.toLowerCase().indexOf("(sel"));
				String strAfterOpenBracket = strLocalCallingData.substring(strLocalCallingData.indexOf(")"
																			, strLocalCallingData.toLowerCase().indexOf("(sel"))+1);
				
				/********************************************************************************************************************
				
					//since the inner query returns can't be guaranteed, we will ignore the aliases there after for the 
					//final list creation of actual fields to be identified.
					Steps:	1. In 'strAfterOpenBracket' check for the existence of immediate " , ".
								Step 1 enables to find the alias which will be overlooked in the final response parsing.
							 	if Step 1 yields into a find, substring the 'strAfterOpenBracket' for data after " , ".
							2. else to Step 1, we will eliminate text till the next " from".
							   Step 2 enables to eliminate the only aliasing sub-query.					
				
				********************************************************************************************************************/
				Integer intIndexOfAliasingComa = strAfterOpenBracket.indexOf(",");
				if(intIndexOfAliasingComa == -1) {	// indicates that there is no comma, so look for " form" now.
				
					Integer intIndexOfMainQueryFrom = strAfterOpenBracket.toLowerCase().indexOf(" from");
					strAfterOpenBracket = strAfterOpenBracket.substring(intIndexOfMainQueryFrom+1);
				}
				else {
					
					strAfterOpenBracket = strAfterOpenBracket.substring(intIndexOfAliasingComa+1);
				}
				
				strLocalCallingData = strBeforeOpenBracket + strAfterOpenBracket ;
			}			
			
		}while(strLocalCallingData.toLowerCase().contains("(sel"));
		Integer intStartIndex = strCallingData.toLowerCase().indexOf("select ");	//clause can be wither Select / SELECT / select
		if(intStartIndex == 0)
			intStartIndex = 7;
		else if(intStartIndex == -1) {
			
			intStartIndex = strLocalCallingData.toLowerCase().indexOf("sel ");	//clause can be wither Sel / SEL / sel
			if(intStartIndex == 0)
				intStartIndex = 4;
		}
		if (intStartIndex == 0) {
			
			Log.debug("ERROR: There is no Select clause in the Query; hence exiting with error !");
			return lstmapParsedResponse;
		}			
		Integer intEndIndex = strLocalCallingData.toLowerCase().indexOf(" from");
		if (intEndIndex == -1) {
			
			Log.debug("ERROR: There is no from clause in the Query; hence exiting with error !");
			return lstmapParsedResponse;
		}
		String strFieldList = strLocalCallingData.substring(intStartIndex, intEndIndex);
		String strFinalFieldList = "";
		Log.debug("DEBUG: The final list of SQL Fields is: "+strFieldList);
		
		//Step 2:
		if(strFieldList.contains("*")) {	//assumes all columns from the table are extracted; remove Table Name from parsed response
											//and return the cleaned response.
			for(List<Map<String, Object>> lst : lstmapParsedResponse) {
			
				List<Map<String, Object>> lstmapTemp = new ArrayList<Map<String,Object>>();
				for(Map<String, Object> map : lst) {
					
					Map<String, Object> mapTemp = new HashMap<String, Object>();
					Set<String> keys = map.keySet();
					for(String key : keys) {
						
						if(key.contains(".")) {
							
							String[] arrKeyName = key.toString().trim().split(".");
							mapTemp.put(arrKeyName[1].trim(), map.get(key));
							lstmapTemp.add(mapTemp);
						}
						else							
							lstmapTemp.add(map);
					}
				}
				lstmapValidatedResponse.add(lstmapTemp);
			}
			
			return lstmapValidatedResponse;
		}
		
		//Step 3:
		String[] arrKeywords = new String[]{"distinct", "(", "max", "min"};
		String[] arrFieldList = strFieldList.split(",");
		for(String strKeyword : arrKeywords) {
			
			do {
				
				//get the starting index of the keyword to be eliminated from the list.
				Integer intIndex = strFieldList.toLowerCase().indexOf(strKeyword);
				if(intIndex != -1) {
					
					strFinalFieldList = "";
					String strFinalFieldList2 = "";
					String strFinalFieldList1 = strFieldList.substring(0, intIndex);
					if(strKeyword.equals("(")) {
						
						Integer intIndex2 = strFieldList.toLowerCase().indexOf(")");
						strFinalFieldList2 = strFieldList.substring(intIndex2+1);
					}
					else
						strFinalFieldList2 = strFieldList.substring(strKeyword.length()+intIndex+1);
					strFinalFieldList = strFinalFieldList1 + strFinalFieldList2;
					strFieldList = strFinalFieldList;
				}
			}while(strFieldList.toLowerCase().contains(strKeyword));
		}
		if(strFinalFieldList.contentEquals(""))	//indicates that if there are no Keywords to be eliminated, retain field list as is.
			strFinalFieldList = strFieldList;
		Log.debug("DEBUG: The final cleansed list of SQL fields is: "+strFinalFieldList);
		arrFieldList = strFinalFieldList.split(",");
		
		//Step 4:		
		for(List<Map<String, Object>> lst : lstmapParsedResponse) {
			
			int intListCounter = 0;
			List<Map<String, Object>> lstmapTemp = new ArrayList<Map<String,Object>>();
			List<Map<String, Object>> lstmapRemainder = new ArrayList<Map<String,Object>>();			
			List<String> lstFieldList = new ArrayList<String>();	//create a list which will indicate which field is not returned in response.
			for(String strFieldName: arrFieldList)
				lstFieldList.add(strFieldName.trim());			
			lstmapRemainder.addAll(lst);
			//first loop the list for MATCHING Key with Expected Fields.
			for(Map<String, Object> map : lst) {
				
				intListCounter ++;
				Set<String> keys = map.keySet();
				for(String key : keys) {
					
					nextKeyEquals:
					//traversing the Field List to map it with the key being searched.
					for(int intFieldList = 0; intFieldList < arrFieldList.length; intFieldList ++) {
						
						Map<String, Object> tempMap = new HashMap<String, Object>();
						if(key.equals(arrFieldList[intFieldList].trim())) {
							
							tempMap.putAll(map);	//since the field names match to the key as is, add entire map.
							lstmapTemp.add(tempMap);
							lstFieldList.remove(arrFieldList[intFieldList].trim());
							lstmapRemainder.remove(map);	//remove map's whose values didn't match.
							break nextKeyEquals;
						}
					}
				}
			}
			Log.debug("DEBUG: The list of items remainder after first iteration is: "+lstFieldList);
			//secondly, loop the list for containing Key with Expected Fields.			
			String strContainsFieldList = lstFieldList.toString();
			strContainsFieldList = strContainsFieldList.replaceAll("\\[", "");
			strContainsFieldList = strContainsFieldList.replaceAll("\\]", "");
			String[] arrContainsFieldList = strContainsFieldList.split(",");
			List<Map<String, Object>> lstmapLeftOver = new ArrayList<Map<String,Object>>();
			lstmapLeftOver.addAll(lstmapRemainder);
			for(Map<String, Object> map : lstmapRemainder) {
				
				intListCounter ++;
				Set<String> keys = map.keySet();
				for(String key : keys) {
					
					nextKeyEquals:
					//traversing the Field List to map it with the key being searched.
					for(int intFieldList = 0; intFieldList < arrContainsFieldList.length; intFieldList ++) {
						
						Map<String, Object> tempMap = new HashMap<String, Object>();
						if(key.contains("."+arrContainsFieldList[intFieldList].trim())) {
							
							tempMap.put(arrContainsFieldList[intFieldList].trim(), map.get(key));	//add the Key from Field list and the value for it from parsed response.
							lstmapTemp.add(tempMap);
							lstmapLeftOver.remove(map);
							lstFieldList.remove(arrContainsFieldList[intFieldList].trim());
							break nextKeyEquals;
						}
					}
				}
			}
			Log.debug("DEBUG: The list of items remainder after second iteration is: "+lstmapLeftOver);

			if(lstmapLeftOver.size() != 0)	// even after finding exact map's key matches or otherwise, if there are still any leftovers, add those map's 
				lstmapTemp.addAll(lstmapLeftOver);	//to the final map.
			
			if(lstFieldList.size() != 0){	//now, since the entire parsed response is traversed and we have fields with no values,					
											//indicates that the SOAP response have returned null, so add these fields to the response with null value.
				for(int intFieldList = 0; intFieldList < lstFieldList.size(); intFieldList ++) {
				
					if(blnInnerQueryExists == false) {	//if there is any sub-query part of the call, we overlook the fields.
						
						Map<String, Object> tempMap = new HashMap<String, Object>();
						tempMap.put(lstFieldList.get(intFieldList).trim(), "");
						lstmapTemp.add(tempMap);
					}					
				}
			}			
			if(intListCounter >= lst.size())	//since we have completed working for the list, add it to the final List.
				lstmapValidatedResponse.add(lstmapTemp);
		}
		
		return lstmapValidatedResponse;
	}
}