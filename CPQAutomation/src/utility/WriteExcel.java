/* 
============================================================================================================================
 Author     		:	Aashish Bhutkar
 Modified By		:	
 
 Class Name		 	: 	WriteExcel
 
 Purpose     		: 	Write the responses from API calls / SQL executions.
 
 Date       		:	11/08/2016
 Modified Date		:	01/04/2017
 Version Information:	Version 1.1
 
 PreCondition 		:	Required parameters like response data & location of file to write response 
 						should be provided. (Currently Supports ONLY MySQL DB.)
 Test Steps 		:	1. Based on the data received, write the data to excel sheet.
 
 Version Change 1.1	:	1. Updated the logic for file writing to handle Decimal / BigDecimal & Timestamp data types.

 Copyright notice	:	Copyright(C) 2016 Sungard Availability Services - 
 						All Rights Reserved 
 ===========================================================================================================================
 */

package utility;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class WriteExcel extends VerificationMethods {

/*
	Author: Aashish Bhutkar
	Function fn_WriteExcel: REST Assured call for GET call.
	Inputs: lstmapActualData - List of hashmap reported by various functions to be written to excel file.
			strFQNFileNameWrite - FQN for the excel file to write the response.
			strWorksheetName - Specifies the Worksheet name from Results workbook @ run time to look for the data.
	Outputs: List of Hashmap is the output which actually is the parsed response of the call made.
*/

	public Boolean fn_WriteExcel(List<List<Map<String, Object>>>  lstmapActualData, String strFQNFileNameWrite
						, String strWorksheetName) throws Exception {
		
		Boolean blnFielWriteComplete = false;
		Integer intCurrentSize = 0;
		Integer intCurrentPosition = 0;
		
		//TODO: 1. Based on the List Received, identify the # of Columns to be created.
		//		2. Once the Columns are identified, add those to the newly (Object) created excel.
		//		3. Add the data from the list to the workbook and close the sheet.
		//		4. Once successful, close the workbook and return the boolean status to calling function.
		
		try {
		
			for(Integer intPos = 0; intPos < lstmapActualData.size(); intPos++) {			
				
				if(intCurrentSize < lstmapActualData.get(intPos).size()) {
					
					intCurrentSize = lstmapActualData.get(intPos).size();
					intCurrentPosition = intPos;
				}
			}
			Log.debug("DEBUG: The Maximum Size of a List is: "+intCurrentSize+" with the position as: "+intCurrentPosition);
			List<String> lstActualColumnNames = new ArrayList<String>();
			Log.debug("DEBUG: The file name for writing results is: "+strFQNFileNameWrite);
			File fileInputStream = new File(strFQNFileNameWrite);
			FileInputStream fisExcel = null;
			XSSFWorkbook excelWorkBook = null;
			XSSFSheet excelSheet = null;
			strWorksheetName = strWorksheetName.replaceAll("[/.*]", "");
			if(fileInputStream.exists()) {	//if file exists, open it in append mode.
				
				try {
					
					fisExcel = new FileInputStream(fileInputStream);
					excelWorkBook = (XSSFWorkbook) WorkbookFactory.create(fisExcel);
				}
				catch (InvalidFormatException e) {

					e.printStackTrace();
				} 
				excelSheet = excelWorkBook.createSheet(strWorksheetName);
			}
			else {	//since file doesn't exist, we will create one and proceed with it.
				
				excelWorkBook = new XSSFWorkbook();
				excelSheet = excelWorkBook.createSheet(strWorksheetName);
			}		
			
			//add the Header for the excel sheet.
			XSSFRow rowExcel = excelSheet.createRow(0);
			Integer intColumnIndex = 0;			
			for(List<Map<String, Object>> lstmap : lstmapActualData)
				for(Map<String, Object> map : lstmap) {
					
					Set<String> keys = map.keySet();
					for(String strKey : keys) {	//traverse through the various keys for the map to know the available keys / column names.							
						if(!lstActualColumnNames.contains(strKey)) {
						
							XSSFCell cellHeaderRow = rowExcel.createCell(intColumnIndex);
							lstActualColumnNames.add(strKey);
							cellHeaderRow.setCellValue(/*lstActualColumnNames.get(intColumnIndex)*/strKey);
							intColumnIndex ++;
						}
					}
				}
				
			//now add the data for the excel sheet.
			intColumnIndex = 0;
			Integer intRowIndex = 1;	// starting from 1 since the '0'th row was your Header Data.
			List<String> lstColumnArraysList = new ArrayList<String>();
			for(String str : lstActualColumnNames)
				lstColumnArraysList.add(str);
			rowExcel = excelSheet.createRow(intRowIndex);	//this logic is for writing data in straight line with multiple Keys in a list.
			for(List<Map<String, Object>> lstMAP : lstmapActualData) {
				
				if(intCurrentSize != 1 || (intCurrentSize == 1 && lstActualColumnNames.size() == 1)) {	//this logic is for writing data for collection of Keys from the data set.
					
					lstColumnArraysList.clear();
					for(String str : lstActualColumnNames)
						lstColumnArraysList.add(str);
					
					rowExcel = excelSheet.createRow(intRowIndex);
				}

				for(Map<String, Object> map : lstMAP) {
					
					String strColumnName = "";
					
					Set<String> keys = map.keySet();
					for(String strKey : keys) {	//traverse through the various keys for the map to know the available keys / column names.
						
						strColumnName = strKey;
						Log.debug("DEBUG: The column name for which the data to be written is: "+strColumnName);
						intColumnIndex = lstActualColumnNames.indexOf(strColumnName);
						//Log.debug("The index @ which the data to be written is: "+intColumnIndex);
						lstColumnArraysList.remove(strColumnName);
						//Log.debug("The length of reference list now is: "+lstColumnArraysList.size()+" and values: "+lstColumnArraysList);
						
						//get the Data type of the value of the hashmap.
						String strValueDataType = map.get(strColumnName).getClass().getSimpleName();
						Log.debug("DEBUG: The type of value to be written is: "+strValueDataType);
						XSSFCell cellRow = rowExcel.createCell(intColumnIndex);
						
						//based on the data type, create the cell and add data to it.
						if(strValueDataType.equalsIgnoreCase("string") || strValueDataType.contains("nteger") || strValueDataType.equalsIgnoreCase("bigdecimal")) {
							
							String strColumnValue = map.get(strColumnName).toString();
							cellRow.setCellType(Cell.CELL_TYPE_STRING);
							cellRow.setCellValue(strColumnValue);
							Log.debug("DEBUG: The value to be added is: "+strColumnValue+" at row #: "+intRowIndex+" in column #: "+intColumnIndex);
						}
						else if(strValueDataType.equalsIgnoreCase("decimal") || strValueDataType.equalsIgnoreCase("double") 
									|| strValueDataType.equalsIgnoreCase("float")) {
							
							Double dblColumnValue = (Double) map.get(strColumnName);
							cellRow.setCellType(Cell.CELL_TYPE_NUMERIC);
							cellRow.setCellValue(dblColumnValue);							
							Log.debug("DEBUG: The value to be added is: "+dblColumnValue+" at row #: "+intRowIndex+" in column #: "+intColumnIndex);
						}
/*						
 * 						This code has been commented since we see loss of data happening.
						else if(strValueDataType.equalsIgnoreCase("bigdecimal")) {
						
							BigDecimal bdeciColumnvalue = (BigDecimal) map.get(strColumnName); 
							Double dblColumnValue = bdeciColumnvalue.doubleValue();
							cellRow.setCellType(Cell.CELL_TYPE_NUMERIC);
							cellRow.setCellValue(dblColumnValue);							
							Log.debug("DEBUG: The value to be added is: "+dblColumnValue+" at row #: "+intRowIndex+" in column #: "+intColumnIndex);
						}*/
						else if(strValueDataType.equalsIgnoreCase("date") || strValueDataType.equalsIgnoreCase("timestamp")) {
							
							Date dateColumnValue = (Date) map.get(strColumnName);
							cellRow.setCellType(Cell.CELL_TYPE_STRING);
							cellRow.setCellValue(dateColumnValue);							
							Log.debug("DEBUG: The value to be added is: "+dateColumnValue+" at row #: "+intRowIndex+" in column #: "+intColumnIndex);
						}
						else if(strValueDataType.equalsIgnoreCase("boolean")) {
							
							Boolean blnColumnValue = (Boolean) map.get(strColumnName);
							cellRow.setCellType(Cell.CELL_TYPE_BOOLEAN);
							cellRow.setCellValue(blnColumnValue);							
							Log.debug("DEBUG: The value to be added is: "+blnColumnValue+" at row #: "+intRowIndex+" in column #: "+intColumnIndex);
						}
/*						else if() {
							
							Double dblColumnValue = Double.parseDouble(map.get(strColumnName).toString());
							cellRow.setCellValue(dblColumnValue);
							Log.debug("The value to be added is: "+dblColumnValue+" at row #: "+intRowIndex+" in column #: "+intColumnIndex);
						}*/
					}
				}
				//handling logic for the Lists read of XML response which don't have all the values in it.
				if(lstColumnArraysList.size() > 0 && intCurrentSize != 1){
					
					for(Integer intFinalArray = 0; intFinalArray < lstColumnArraysList.size(); intFinalArray ++) {
						
						intColumnIndex = lstActualColumnNames.indexOf(lstColumnArraysList.get(intFinalArray));	//Arrays.asList(lstActualColumnNames).indexOf(lstColumnArraysList.get(intFinalArray));
						//Log.info("The index @ which the data to be written is: "+intColumnIndex);
						XSSFCell cellRow = rowExcel.createCell(intColumnIndex);
						cellRow.setCellValue("NA");
						Log.debug("DEBUG: The value to be added is: 'NA' at row #: "+intRowIndex+" in column #: "+intColumnIndex);
					}
				}
				//here it is assumed that the data is to be written in a single row or in multiple rows for a Single Key.
				if(intCurrentSize != 1 || (intCurrentSize == 1 && lstActualColumnNames.size() == 1))
					intRowIndex ++;
			}
			
			if(fileInputStream.exists())
				fisExcel.close();
			FileOutputStream fosFinalOutput = new FileOutputStream(fileInputStream);
			excelWorkBook.write(fosFinalOutput);
			fosFinalOutput.flush();
			fosFinalOutput.close();
			Thread.sleep(10000);	//sleep added for the file to be written completely and then recognized for later reference. 
					
			if(new File(strFQNFileNameWrite).exists()) {
			
				blnFielWriteComplete = true;
				//assertTrue(true, "The Result file has been successfully created / updated !");
				Log.debug("DEBUG: The Result file has been successfully created / updated !");
			}
		}		
		catch (Exception exceptionWriteExcel){	
						
			assertFalse(false, "\nERROR: The Excel file couldn't be written due to exception: "+exceptionWriteExcel+" \n");
			StringWriter stack = new StringWriter();
			exceptionWriteExcel.printStackTrace(new PrintWriter(stack));
			Log.debug("DEBUG: The exception while writing the file is: "+stack);
			blnFielWriteComplete = false;
		}
		
		return blnFielWriteComplete;
	}
}