/* 
==============================================================
 Author     		:	Umesh Kute / Aashish Bhutkar
 Modified By		:	Aashish Bhutkar
 
 Class Name		 	: 	VerificationMethods
 
 Purpose     		: 	To handle all types of assertions.
 
 Date       		:	02/16/2015
 Modified Date		:	11/28/2016
 Version Information:	Version 2.0
 
 PreCondition 		:	None
 Test Steps 		:	None
 
 Version Change 1.0	:	1. Initial implementation. 
 Version Change 2.0	:	1. Updated logic for the inputing data to Extent Graphical Reports.
 
 Copyright notice	:	Copyright(C) 2016 Sungard Availability Services - 
 						All Rights Reserved 
 ======================================================================
 */

package utility;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.Reporter;

import com.relevantcodes.extentreports.LogStatus;

public class VerificationMethods {

	public static String chromeDriverPath = new File("Brwser_Driverssrc").getAbsolutePath() + "\\chromedriver.exe";
	public static String BASE_TESTDATA_PATH = new File("src\\testData\\").getAbsolutePath();
	private static Map<ITestResult, List<Throwable>> verificationFailuresMap = new HashMap<ITestResult, List<Throwable>>();

    public static void assertTrue(boolean condition) {
    	
    	Assert.assertTrue(condition);
    	Log.extentTestLogger.log(LogStatus.PASS, "ASSERT TRUE");
    }
    
    public static void assertTrue(boolean condition, String strMessage) {
    	
    	Log.info(strMessage);
    	Assert.assertTrue(condition, strMessage);
    	Log.extentTestLogger.log(LogStatus.PASS, strMessage);
    }
    
    public static void assertFalse(boolean condition) {
    	
    	Log.error("ASSERT FALSE");
    	Assert.assertFalse(condition);
    	Log.extentTestLogger.log(LogStatus.FAIL, "ASSERT FALSE");
    }
    
    public static void assertFalse(boolean condition, String strMessage) {
    	
    	Log.error(strMessage);
    	Assert.assertFalse(condition, strMessage);
    	Log.extentTestLogger.log(LogStatus.FAIL, strMessage);
    }
    
    public static void assertEquals(boolean actual, boolean expected) {

    	Log.info("ASSERT EQUALS - BOOLEAN");
    	Assert.assertEquals(actual, expected);
    	Log.extentTestLogger.log(LogStatus.INFO, "ASSERT EQUALS - BOOLEAN");
    }
    
    public static void assertEquals(Object actual, Object expected) {
    	
    	Log.debug("DEBUG: ASSERT EQUALS");
    	Assert.assertEquals(actual, expected);
    	//Log.extentTestLogger.log(LogStatus.INFO, "ASSERT EQUALS");
    }
    
    public static void assertEquals(Object[] actual, Object[] expected) {
    	
    	Log.info("ASSERT EQUALS");
    	Assert.assertEquals(actual, expected);
    	Log.extentTestLogger.log(LogStatus.INFO, "ASSERT EQUALS");
    }
    
    public static void assertEquals(Object actual, Object expected, String strMessage) {
    	
    	Log.info(strMessage);
    	Assert.assertEquals(actual, expected, strMessage);
    	Log.extentTestLogger.log(LogStatus.INFO, strMessage);
    }
    
    public static void verifyTrue(boolean condition) {
    	try {
    		
    		assertTrue(condition);
    	} catch(Throwable e) {
    		
    		addVerificationFailure(e);
    	}
    }
    
    public static void verifyTrue(boolean condition, String strMessage) {
    	try {
    	
    		assertTrue(condition, strMessage);
    	} catch(Throwable e) {
    		
    		addVerificationFailure(e);
    	}
    }
    
    public static void verifyFalse(boolean condition) {
    	try {
    		
    		assertFalse(condition);
		} catch(Throwable e) {
    		
			addVerificationFailure(e);
		}
    }
    
    public static void verifyFalse(boolean condition, String strMessage) {
    	try {
    		
    		assertFalse(condition, strMessage);
    	} catch(Throwable e) {
    		
    		addVerificationFailure(e);
    	}
    }
    
    public static void verifyEquals(boolean actual, boolean expected) {
    	try {
    		
    		assertEquals(actual, expected);
		} catch(Throwable e) {
    		
			addVerificationFailure(e);
		}
    }

    public static void verifyEquals(Object actual, Object expected) {
    	try {
    		
    		assertEquals(actual, expected);
		} catch(Throwable e) {
    		
			addVerificationFailure(e);
		}
    }
    
    public static void verifyEquals(Object[] actual, Object[] expected) {
    	try {
    		
    		assertEquals(actual, expected);
		} catch(Throwable e) {
    		
			addVerificationFailure(e);
		}
    }

    public static void fail(String strMessage) {
    	
    	Log.fatal(strMessage);
    	Assert.fail(strMessage);
    	Log.extentTestLogger.log(LogStatus.FAIL, strMessage);
    }
    
	public static List<Throwable> getVerificationFailures() {
		
		List<Throwable> verificationFailures = verificationFailuresMap.get(Reporter.getCurrentTestResult());
		return verificationFailures == null ? new ArrayList<Throwable>() : verificationFailures;
	}
	
	protected static void addVerificationFailure(Throwable e) {
		
		List<Throwable> verificationFailures = getVerificationFailures();
		verificationFailuresMap.put(Reporter.getCurrentTestResult(), verificationFailures);
		verificationFailures.add(e);
	}
}
