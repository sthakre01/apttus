/* 
==============================================================
 Author     		:	Umesh Kute / Aashish Bhutkar
 Modified By		:	Aashish Bhutkar
 
 Class Name		 	: 	Log
 
 Purpose     		: 	This class contains the reusable function for different types of log levels 
						(e.g. INFO, DEBUG, ERROR, FATAL, WARN) which can be used for reporting purpose.
 
 Date       		:	10/13/2016
 Modified Date		:	11/28/2016
 Version Information:	Version 2.0
 
 PreCondition 		:	None
 Test Steps 		:	1. Enable the appropriate Loggers.
 
 Version Change 1.0	:	1. Initial implementation. 
 Version Change 2.0	:	1. Updated logic for the creation of Extent Graphical Reports.
 
 Copyright notice	:	Copyright(C) 2016 Sungard Availability Services - 
 						All Rights Reserved 
 ======================================================================
 */

package utility;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Reporter;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;


public class Log {

	static Logger Log = Logger.getLogger(Log.class.getName());
	
	// initialize ExtentReports.
	public static ExtentReports extentReports;
	public static ExtentTest extentTestLogger;	
	
	//TODO: Initialize the Logger based on the test case name.
	public static void fn_InitializeExtentLogger(String strTestCaseName) {
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
		Date dateCurrent = new Date();
		
		strTestCaseName = strTestCaseName.replaceAll(" ", "");
		String strFQNReportFilePath = EnvironmentDetails.Path_Reports + System.getProperty("file.separator") 
										+ strTestCaseName + "_" + sdf.format(dateCurrent) + ".html";
		// initialize extent report with the HTML file created based on the Test Case name.
		extentReports = new ExtentReports(strFQNReportFilePath);
		extentReports.loadConfig(new File(EnvironmentDetails.Path_ExtentConfig));
		// initialize extent report logger based on the extent report.
		extentTestLogger = extentReports.startTest(strTestCaseName);
	}
	
	// This is to print log at the beginning of the test case;
	// as we usually run so many test cases as a test suite
	public static void startTestCase(String strTestCaseName) {

		extentTestLogger.log(LogStatus.INFO, "$$$ "+strTestCaseName+ ": STARTED $$$");
		Log.info("$$$ "+strTestCaseName+ ": STARTED $$$");
		
	}
	 
    //This is to print log once the test case is completed
	public static void endTestCase(String strTestCaseName){
	
		extentTestLogger.log(LogStatus.INFO, "$$$ "+strTestCaseName+" COMPLETED $$$");
		Log.info("$$$ "+strTestCaseName+" COMPLETED $$$");
		//extentTestLogger.log(LogStatus.INFO, extentTestLogger.getEndedTime().toString());
		
		extentReports.endTest(extentTestLogger);	// ending the TC and flushing it for final closure.
		extentReports.flush();
	}
	 
	//This is to print log once the test case is completed
	public static void endTestCaseFailure(String strTestCaseName){
	
		extentTestLogger.log(LogStatus.FATAL, "FATAL: Execption recorded for "+strTestCaseName+"...");
		extentReports.endTest(extentTestLogger);	// ending the TC and flushing it for final closure.
		extentReports.flush();
	}
		
	
	//TODO:  Various type of log statements with their respective overrides.  
	public static void info(String strMessage) {
	
		System.out.println(strMessage);
        Log.info(strMessage);
        Reporter.log(strMessage);
        extentTestLogger.log(LogStatus.INFO, strMessage);
	}
	
	public static void warn(String strMessage) {
	
		System.out.println(strMessage);
		Log.warn(strMessage);
		Reporter.log(strMessage);
		extentTestLogger.log(LogStatus.WARNING, strMessage);
	}
	
	public static void error(String strMessage) {
	
		System.out.println(strMessage);
		Log.error(strMessage);
		Reporter.log(strMessage);
		extentTestLogger.log(LogStatus.ERROR, strMessage);		
	}
	
	public static void fatal(String strMessage) {
	
		System.out.println(strMessage);
		Log.fatal(strMessage);
		Reporter.log(strMessage);
		extentTestLogger.log(LogStatus.FATAL, strMessage);
	}
	
	public static void debug(String strMessage) {
	    
		Log.debug(strMessage);
	    System.out.println(strMessage);	    
	}
	
	//TODO: Final statement in case of the logger indicating an exception encountered; to be added
	// to every exception during an Assertion.
	public static void fn_LogExceptionInReport(WebDriver driver, String strTestCaseName) throws Exception {
		
		Utils objUtils = new Utils();		
		
		//extract the information received from takeScreenshot for the exception image.
		Map<String, String> mapException = objUtils.fn_takeScreenshotFullPage(driver);
		String strScreenshotImage = mapException.get("screenshotAbsolutePath").toString().trim();
		String strScreenshotName = mapException.get("screenshotFileName").toString().trim();		
		
		String strFQNExceptionScreenshot = EnvironmentDetails.Path_Reports + System.getProperty("file.separator")
											+ "ExceptionCapture_" + strScreenshotName;
		utility.Log.debug("DEBUG: The FQN for exception snapshot is: "+strFQNExceptionScreenshot);
		//copying the exception file to the report folder to couple the image with report
		//while sharing the report with stake holders when needed.		
		FileUtils.copyFile(new File(strScreenshotImage), new File(strFQNExceptionScreenshot));		
		Thread.sleep(3500);
		//getting the extent image name for logging purpose.
		String strExtentImage = extentTestLogger.addScreenCapture(strFQNExceptionScreenshot);
		
		utility.Log.error("FAIL: Exception has been recorded as captured !");
		extentTestLogger.log(LogStatus.FAIL, "FAIL: Exception recorded as seen in capture.."+strExtentImage);
		endTestCase(strTestCaseName);
	}
}