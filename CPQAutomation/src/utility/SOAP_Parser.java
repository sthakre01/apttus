/* 
============================================================================================================================
 Author     		:	Aashish Bhutkar
 Modified By		:	Aashish Bhutkar
 
 Class Name		 	: 	fn_SOAPParser
 
 Purpose     		: 	Parse XML response generated for the SOAP calls (currently ONLY GET supported).
 
 Date       		:	11/08/2016
 Modified Date		:	12/14/2016
 Version Information:	Version 1.1
 
 PreCondition 		:	Required parameters like the location of file to write response needs to be provided.
 Test Steps 		:	1. Parse the XML response received after the GET call and return the parse response.
 
 Version Change 1.1	:	1. Changed usage of HashMap to Map with generic Object class for it's values.

 Copyright notice	:	Copyright(C) 2016 Sungard Availability Services - 
 						All Rights Reserved 
 ===========================================================================================================================
 */

package utility;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.io.IOUtils;
import org.testng.Assert;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class SOAP_Parser extends VerificationMethods {

/*
	Author: Aashish Bhutkar
	Function fn_SOAPParser: Typical SOAP parser for parsing the SOAP response received.
	Inputs: strResultantXML - The resultant XML after the SOAP response.
			strType - Type of SOAP to be built; honors ONLY "login" & "query" values .
	Outputs: List of Hashmap is the output which actually is the parsed response of the call made.
*/		

	public List<List<Map<String, String>>> fn_SOAPParser (String strResultantXML, String strType) throws Exception {
				
		List<List<Map<String, String>>> lstmapParsedXML = new ArrayList<List<Map<String,String>>>();

		//TODO: If there is no result TAG in the XML, no parsing required, pass the resultant error as response.
		try {
		
			if(!strResultantXML.contains("result")) {
				
				Map<String, String> map = new HashMap<String, String>();
				List<Map<String, String>> lstActualValues = new ArrayList<Map<String, String>>();
				map.put("SOAPFault", strResultantXML);
				lstActualValues.add(map);
				lstmapParsedXML.add(lstActualValues);
			}
			else {
				
				//build the doc for parsing the SOAP resultant XML.
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();
				Document doc = docBuilder.parse(IOUtils.toInputStream(strResultantXML, "UTF-8"));
				
				doc.getDocumentElement().normalize();
				Log.debug("Root element: "+doc.getDocumentElement().getNodeName());
				NodeList nList = doc.getElementsByTagName("result");
				if(nList.getLength() > 1)
					Assert.assertTrue(false, "ERROR: Result Node can't be more than 1 !");
				Node nodeResult = nList.item(0);
				Log.debug("Node List length is: "+nList.getLength());
				
				switch (strType) {
				
					case "login":	//for Login type, parse only for getting the Session ID.					
						try {
							
							Log.debug("\nCurrent Element :" + nodeResult.getNodeName());
							
							if (nodeResult.getNodeType() == Node.ELEMENT_NODE) {
				
								Map<String, String> map = new HashMap<String, String>();
								List<Map<String, String>> lstActualValues = new ArrayList<Map<String, String>>();
								Element eElement = (Element) nodeResult;
								String strValue = eElement.getElementsByTagName("sessionId").item(0).getTextContent();
								Log.debug("Session ID registered is: "+eElement.getElementsByTagName("sessionId").item(0).getTextContent());
								map.put("sessionId", strValue);
								lstActualValues.add(map);
								lstmapParsedXML.add(lstActualValues);
							}						
						}
						catch (Exception exceptionSOAPLogin) {
							
							assertFalse(false, "ERROR: Exception captured while parsing Login SOAP Response as: "+exceptionSOAPLogin);
							exceptionSOAPLogin.printStackTrace();
						}					
						break;
						
					case "query":	//for querry type, parse the entire response for the data.
						try {
						
							Log.debug("\nCurrent Element :" + nodeResult.getNodeName());

							if (nodeResult.getNodeType() == Node.ELEMENT_NODE) {				
				
								Element eElement = (Element) nodeResult;
								NodeList nlRecords = eElement.getElementsByTagName("records");
								Log.debug("Records Nodelist Length: "+nlRecords.getLength());
								//TODO: Traverse the List of Records for reading each record with it's value.
								if(nlRecords.getLength() == 0) {
								
									Map<String, String> map = new HashMap<String, String>();
									List<Map<String, String>> lstActualValues = new ArrayList<Map<String, String>>();
									map.put("SOAP Response", "No result returned for the SOAP response..");
									lstActualValues.add(map);
									lstmapParsedXML.add(lstActualValues);
								}
								else {
									
									for(Integer intRecords = 0; intRecords < nlRecords.getLength(); intRecords++) {
										Map<String, String> map = new HashMap<String, String>();
										NodeList nlRecordsChildren = eElement.getElementsByTagName("records").item(intRecords).getChildNodes();					
										Log.debug("\nRecords Nodelist Children Length: "+nlRecordsChildren.getLength());								
										//TODO: Traverse the Node List to read the Attributes as well as the Content.
										List<Map<String, String>> lstActualValues = new ArrayList<Map<String, String>>();
										for(Integer intNLChild = 0; intNLChild < nlRecordsChildren.getLength(); intNLChild++) {
											
											Element eleChild = (Element)nlRecordsChildren.item(intNLChild);
											String strNodeName = eleChild.getNodeName();
											String strNodeValue = eleChild.getTextContent();
											if(strNodeValue.equals("")) {
												//skip the Node name and don't add it to the resultant list of records.							
											}
											else {
												
												//Map<String, String> map = new HashMap<String, String>();										
												String[] arrNodeNames = strNodeName.split(":");
												strNodeName = arrNodeNames[1];
												Log.debug("Node Name is: "+strNodeName+" with value as: "+strNodeValue);
												map.put(strNodeName, strNodeValue);
												//lstActualValues.add(map);
											}
											//TODO: Read the Attributes for the Child and their values for it.
											NamedNodeMap nmapChild = eleChild.getAttributes();						
											for(Integer intMapChild = 0; intMapChild < nmapChild.getLength(); intMapChild++) {
												
												Node attribute = nmapChild.item(intMapChild);
												Log.debug("Attribute Name is: "+attribute.getNodeName()+" and its Attribute value is: "+attribute.getNodeValue());
											}
										}
										lstActualValues.add(map);
										lstmapParsedXML.add(lstActualValues);
									}
								}					
							}
							Log.debug("\nThe Final List after completion of Parsing is: "+lstmapParsedXML);						
						}
						catch (Exception exceptionSOAPQuery) {
							
							assertFalse(false, "ERROR: Exception captured while parsing Query SOAP Response as: "+exceptionSOAPQuery);
				        	StringWriter stack = new StringWriter();
				        	exceptionSOAPQuery.printStackTrace(new PrintWriter(stack));
							Log.debug("DEBUG: The exception recorded while parsing SOAP request is: "+stack);
						}					
						break;
						
					default:
						break;
				}
			}			
		}
		catch (Exception exceptionSOAPResponse) {
			
			assertFalse(false, "ERROR: Exception captured while parsing Login SOAP Response as: "+exceptionSOAPResponse);
			StringWriter stack = new StringWriter();
			exceptionSOAPResponse.printStackTrace(new PrintWriter(stack));
			Log.debug("DEBUG: The exception recorded @ class level while parsing SOAP request is: "+stack);
		}
		
		return lstmapParsedXML;
	}
}