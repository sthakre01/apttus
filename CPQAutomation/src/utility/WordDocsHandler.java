		
//Author : Madhavi JN
//Purpose: To validate text in the documents of type doc,docx,rtf and PDF files
//Input : Base Excel file from where the Data to compare has to be taken.
//Output : Expected and Actual data will be displayed on console after comparision

package utility;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.*;
import java.util.ArrayList;
import java.util.zip.DataFormatException;

public class WordDocsHandler {
	

	//initiating the Word text Extractor
	public 	static XWPFWordExtractor extractor = null;
	//Initiating the PDF text Extractor
	public  static PDDocument document = null;
	
	//Input parameters filepath-path of the file,TypeofFile-type of file
	public  void fn_validateText(String strDocPath,String strTypeofFile,String strsheetname) throws Throwable, IOException,DataFormatException  
	{

		 EnvironmentDetailsSAS   objEnvDetails = new EnvironmentDetailsSAS();  
		 String strValidationtext=null;
		 
		 try{
		 
		  //TODO: Read the GMSA Validations file for data validation
    	  File fileDocValidations = new File(objEnvDetails.dataSheetPath);	
		  
    	  Log.info("===========================================");	  
    	  Log.info("Validations for the FileName : "+strsheetname);
    	  Log.info("===========================================");
			
		  //TODO : Read Test Excel
		   ArrayList<String> arrlistFileContents = ExcelHandler.readExcelFileAsArray(fileDocValidations,strsheetname);	
		   
			   //reading the word doc/docx/rtf files and retrieving their text for further validation
			   if ((strTypeofFile.equalsIgnoreCase("doc")||strTypeofFile.equalsIgnoreCase("docx")||strTypeofFile.equalsIgnoreCase("rtf"))){
	
				    XWPFDocument docx = new XWPFDocument(new FileInputStream(strDocPath));
				    extractor = new XWPFWordExtractor(docx);			    
				    strValidationtext = extractor.getText();			    
				    extractor.getCoreProperties();			    
				    extractor.close();}
			   
			   //reading the pdf file and retrieving their text for further validation
			   //Note: PDF can be handled until version 1.4 validating above that will be handled in future
			   else{
				
				      FileInputStream file = new FileInputStream(new File(strDocPath));			      
		    	      document = PDDocument.load(file);	    	      
		  	          PDFTextStripper pdfStrip = new PDFTextStripper();	  	          
		  	          strValidationtext = pdfStrip.getText(document);	  	          	  	          
		  	          document.close();}
			   
			   //initialising counters
			   int intRowtraverser=0;
			   int intcounterplus = 0;
		   
			   //Condition for incrementing th eCounter numbers
			   if (strsheetname.contains("GMSA")){
				   intcounterplus=12;}
			   else { intcounterplus=5;}

				
			   //TODO: Now Traverse the ArrayList for Excel File Read to find the item from data table.
				for(int intCnt = 0 ; intCnt < arrlistFileContents.size(); intCnt +=intcounterplus){
					 
					//Validation of data with word/PDF data
					if (strValidationtext.toString().trim().isEmpty()!=true){
		
							 intRowtraverser = intCnt;
							 
							 String strtext = strValidationtext.toString().trim();
								 
							 Log.info("===================Start of verification for row=======================================");
							 						
							 if (!(arrlistFileContents.get(intRowtraverser)).equalsIgnoreCase("n/a")){				 
								   if (strtext.contains(arrlistFileContents.get(intRowtraverser+0).trim()))						
									   Log.info("the text is present and validated for Section#::"+arrlistFileContents.get(intRowtraverser+0));						  
									else								
										Log.info("the text is not present for Section# ::"+arrlistFileContents.get(intRowtraverser+0));}
													 						 							
							  
							 if (!(arrlistFileContents.get(intRowtraverser+1)).equalsIgnoreCase("n/a")){				 
								  if (strtext.contains(arrlistFileContents.get(intRowtraverser+1).trim())) 							
									  Log.info("the text is present and validated for Section Name::"+arrlistFileContents.get(intRowtraverser+1));						 
									else
								      Log.info("the text is not present for Section Name ::"+arrlistFileContents.get(intRowtraverser+1));}
						 
							 if (!(arrlistFileContents.get(intRowtraverser+2)).equalsIgnoreCase("n/a")){										  
								 if (strtext.contains(arrlistFileContents.get(intRowtraverser+2).trim()))
									 Log.info("the text is present and validated for IRL::"+arrlistFileContents.get(intRowtraverser+2));
							   	 else
									 Log.info("the text is not present for IRL ::"+arrlistFileContents.get(intRowtraverser+2));}
						 							
							 Log.info("===================End of verification for end of the row=======================================");}}

		 } 
			 catch (Exception e) {
					// TODO: handle exception
					Log.info("ERROR: problem while Reading and Validating the FunctionalDocs "+strsheetname);
					e.printStackTrace(); }		
	    }  

  }



