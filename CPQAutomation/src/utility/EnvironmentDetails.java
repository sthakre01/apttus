/* 
==============================================================
 Author     		:	Umesh Kute / Aashish Bhutkar
 Modified By		:	Namrata Akarte, Aashish Bhutkar
 
 Class Name		 	: 	EnvironmentDetails
 
 Purpose     		: 	Declaring the environment variables that will be used for WordPress application.
 
 Date       		:	02/06/2015
 Modified Date		:	07/14/2015, 08/17/2016
 Version Information:	Version 1.1
 
 PreCondition 		:	None
 Test Steps 		:	1. Enable the appropriate values for application URL and browser type for application
 
 Version Change 1.1	:	1. Modified the access to the method for extracting the Environment Details by creating Constructors. 
 
 Copyright notice	:	Copyright(C) 2016 Sungard Availability Services - 
 						All Rights Reserved 
 ======================================================================
 */

package utility;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.text.SimpleDateFormat;

import utility.Utils;

public class EnvironmentDetails

{	
	// ****** Environments details ******

	// path details for various driver's or configuration files.
	public static final String Path_ChromeDriver = new File("Brwser_Driverssrc").getAbsolutePath() + System.getProperty("file.separator") 
														+ "chromedriver.exe";
	public static final String Path_IEDriver = new File("Brwser_Driverssrc").getAbsolutePath() + System.getProperty("file.separator")
														+ "IEDriverServer.exe";
	public static final String Path_FirefoxDriver = "";
	public static final String Path_ExtentConfig = System.getProperty("user.dir") + System.getProperty("file.separator")
														 + "TestData" + System.getProperty("file.separator")
														 + "extent-config.xml";
	public static final String Path_Reports = System.getProperty("user.dir") + System.getProperty("file.separator")
													+ "Log" + System.getProperty("file.separator")
													+ "Reports";
	public static final String Path_Results = System.getProperty("user.dir") + System.getProperty("file.separator")
													+ "Log" + System.getProperty("file.separator")
													+ "Results";	
	public static final String Path_Log4j = System.getProperty("user.dir") + System.getProperty("file.separator")
												+ "TestData" + System.getProperty("file.separator")
												+ "log4j.xml";
	
	public static String Path_ExternalFiles = System.getProperty("user.dir") + System.getProperty("file.separator") 
												+ "TestData"; 
	
	// This parameter refers to the path where screenshot of page,
	// during the test case execution is taken.  
	public static SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmssSSS");
	public static Date dateCurrent = new Date();
	public static String strDate = sdf.format(dateCurrent);
	public static final String Path_ScreenShot = System.getProperty("user.dir") + System.getProperty("file.separator") 
													+ "Screenshots" + System.getProperty("file.separator")
													+ "Screenshots"+ "_" +strDate;
	
	Utils objExecEnv = new Utils();
	public static String Path_DataSheet = System.getProperty("user.dir") + System.getProperty("file.separator") 
											+ "TestData"+ System.getProperty("file.separator") 
											+ "TestData.xlsx";
	List<Map<String, String>> lstmapExecEnv = objExecEnv.readexcel_ExecEnvironment(Path_DataSheet, "ExecEnv");
	
	//variable Details 
	public static Map<String, String> mapEnvironmentDetails = new HashMap<String, String>();	
	// Global variable details 
	public static Map<String, Object> mapGlobalVariable = new HashMap<String, Object>();
	//Test case name variable
	public static String strTestCaseName = "";
	
	public static String strFQNFileNameWrite = "";
	
	//Outlook URL 
	public static String strOutlookURL = "https://outlook.office.com/owa/";
	//public  String strOutlookQAMAILURL = "https://outlook.office.com/owa/smb.it.qamail@sungardas.com/";
	
	public String STRUSERNAME = "";
	public String STRPASSWORD = "";
	public String STRURL = "";
	
	//TODO: Default Constructor, which will always return the instance details with "Y" as value for TestCaseNameColumn.
	public EnvironmentDetails() {

		this("Y");
		
		//output data write file name 
		strFQNFileNameWrite = EnvironmentDetails.Path_Results + System.getProperty("file.separator")
																+ "ApttusTest" + "_" + mapEnvironmentDetails.get("Environment").trim()
																+ "_" + EnvironmentDetails.strTestCaseName + "_"
																+ strDate + ".xlsx";
	}

	//TODO: Parameterized Constructor, which will return the Instance details based on Parameterized Instance.	
	public EnvironmentDetails(String strEnv) {
				
		for(Map<String, String> map : lstmapExecEnv) {
			
			if(map.get("Environment").equalsIgnoreCase("INT") && strEnv.equalsIgnoreCase("INT")) {
				
				Log.info("\nEnvironment selected for testing is: "+map.get("Environment"));
				mapEnvironmentDetails = map;
				break;
			}			
			if(map.get("Environment").equalsIgnoreCase("UAT") && strEnv.equalsIgnoreCase("UAT")) {
				
				Log.info("\nEnvironment selected for testing is: "+map.get("Environment"));
				mapEnvironmentDetails = map;
				break;
			}
			if(map.get("Environment").equalsIgnoreCase("DEV") && strEnv.equalsIgnoreCase("DEV")) {
				
				Log.info("\nEnvironment selected for testing is: "+map.get("Environment"));
				mapEnvironmentDetails = map;
				break;
			}
			if(map.get("TestCaseNameColumn").equalsIgnoreCase("Y") && strEnv.equalsIgnoreCase("Y")) {
			
				Log.info("\nDefault Environment selected for testing is: "+map.get("Environment"));
				mapEnvironmentDetails = map;
				break;
			}
		}
	}
	
	
	
	
	
}