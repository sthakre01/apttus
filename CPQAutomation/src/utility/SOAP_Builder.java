/* 
============================================================================================================================
 Author     		:	Aashish Bhutkar
 Modified By		:	Aashish Bhutkar
 
 Class Name		 	: 	fn_SOAPBuilder
 
 Purpose     		: 	Generate XML for the SOAP GET calls.
 
 Date       		:	11/08/2016
 Modified Date		:	03/21/2017, 03/14/2017
 Version Information:	Version 3.0
 
 PreCondition 		:	Required parameters like the Environment details & type of XML to be generated should be provided.
 Test Steps 		:	1. Based on the type of XML generation for GET call, prepare it and return it.
 
 Version Change 2.0	:	1. Created a switch case "update" to handle the POST call as per mapped parameters.
 Version Change 3.0	:	2. Created a switch case "apex" to handle the apex POST call as per mapped parameters.

 Copyright notice	:	Copyright(C) 2016 Sungard Availability Services - 
 						All Rights Reserved 
 ===========================================================================================================================
 */

package utility;

import java.util.Map;
import java.util.Set;

public class SOAP_Builder {


/*
	Author: Aashish Bhutkar
	Function fn_SOAPBuilder: Typical SOAP Builder for ESB API testing.
	Inputs: objEnvironment - Environment Object which governs the execution environment; read from EnvironmentDetails class.
			strType - Type of SOAP to be built; honors ONLY "login" & "query" values .
			strCreds - The credentials required for the SOAP request.
			strQuerry - The Actual Query to be passed in to the SOAP request.
	Outputs: SOAP XML formed as a String to be called as part of the SOAP call.
*/

	public String fn_SOAPBuilder (EnvironmentDetails objEnvironment, String strType, String strCreds, 
									String strQuerry, Map<String, String> mapSOAPRequestBuilder) {
					
		String strSOAP = "";
		
		switch (strType) {
		
			case "login":	//typical GET XML for Login.
				strSOAP = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""+" xmlns:urn=\"urn:"+EnvironmentDetails.mapEnvironmentDetails.get("SOAP_URN")+"\">"
						   +"<soapenv:Header>"
						      +"<urn:LoginScopeHeader>"
						      +"<urn:organizationId/></urn:LoginScopeHeader>"
						   +"</soapenv:Header>"
						   +"<soapenv:Body>"
						      +"<urn:login>"
						         +"<urn:username>"+EnvironmentDetails.mapEnvironmentDetails.get(mapSOAPRequestBuilder.get("UserID"))+"</urn:username>"
						         +"<urn:password>"+EnvironmentDetails.mapEnvironmentDetails.get(mapSOAPRequestBuilder.get("Password"))+"</urn:password>"
						      +"</urn:login>"
						   +"</soapenv:Body>"
						+"</soapenv:Envelope>";
				break;
				
			case "query":	//typical GET XML for Query.
				strSOAP = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""+" xmlns:urn=\"urn:"+EnvironmentDetails.mapEnvironmentDetails.get("SOAP_URN")+"\">"
						   +"<soapenv:Header>"
						   		+"<urn:SessionHeader>"
						   			+"<urn:sessionId>"+strCreds+"</urn:sessionId>"
						   		+"</urn:SessionHeader>"
						   		+"<urn:QueryOptions/>"
						   		+ "<urn:MruHeader>"
						   				+ "<urn:updateMru>true</urn:updateMru>"
						   		+ "</urn:MruHeader>"
						   		+ "<urn:PackageVersionHeader/>"
						   + "</soapenv:Header>"   
						   +"<soapenv:Body>"
						   		+"<urn:query>"
						   			+"<urn:queryString>"+strQuerry+"</urn:queryString>"
						   		+"</urn:query>"
						   +"</soapenv:Body>"
						+"</soapenv:Envelope>";
				break;
				
			case "update":	//typical POST XML for Update: Currently for Approval.
				//TODO: Generate the Update XML based on the inputs.
				
				/******************************************************************************
					Step1: Create the SOAP XML for the fields based on Map inputs.
					Step2: Create the Objects XML tag using the strQuerry keyword which in turn
							is the Object name for which the query is to be composed.
					Step3: Generate the final SOAP XML using the outputs of Step1 & Step2.
				******************************************************************************/
				if(mapSOAPRequestBuilder.isEmpty()) {
					
					Log.error("ERROR: For SOAP Update, there have to be fields passed for upadting them !");
					return "";
				}				
				//Step1:
				String strXMLFieldWithValues = "";
				Set<String> setKeys = mapSOAPRequestBuilder.keySet();
				for(String key: setKeys) {
					if (!(key.equalsIgnoreCase("UserID")||key.equalsIgnoreCase("Password"))) {
						strXMLFieldWithValues = strXMLFieldWithValues + "<urn1:" + key + ">"
								+ mapSOAPRequestBuilder.get(key)
								+ "</urn1:" + key + ">";	
					}
					
				}
				//Step2:				
				String strXMLObject = "";
				strXMLObject =  "<urn:sObjects xsi:type=\"urn1:" + strQuerry + "\">"
								+ strXMLFieldWithValues
								+ "</urn:sObjects>";
				//Step3:				
				strSOAP = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""+" xmlns:urn=\"urn:"+EnvironmentDetails.mapEnvironmentDetails.get("SOAP_URN")+"\""
							+" xmlns:urn1=\"urn:sobject."+EnvironmentDetails.mapEnvironmentDetails.get("SOAP_URN")+"\""
							+" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">"
						   +"<soapenv:Header>"
						   		+"<urn:SessionHeader>"
						   			+"<urn:sessionId>"+strCreds+"</urn:sessionId>"
						   		+"</urn:SessionHeader>"
						   + "</soapenv:Header>"   
						   +"<soapenv:Body>"
							   +"<urn:update>"
						            + strXMLObject						            
						        +"</urn:update>"
						   +"</soapenv:Body>"
						+"</soapenv:Envelope>";
				break;
				
			case "apex":	//typical POST XML for Apex: Currently for Approval.
				//TODO: Generate the Appex XML based on the inputs.
				
				/******************************************************************************
					Step1: Create the SOAP XML for the fields based on Map inputs.
					Step2: Create the Objects XML tag using the Map details.
					Step3: Generate the final SOAP XML using the outputs of Step1 & Step2.
				******************************************************************************/
				if(mapSOAPRequestBuilder.isEmpty()) {
					
					Log.error("ERROR: For SOAP Apex call, please share the pre-requisite values !");
					return "";
				}				
				//Step1:
				String strXMLAttribValues = "";
				Set<String> setKeysApex = mapSOAPRequestBuilder.keySet();
				for(String key: setKeysApex) {
					
					if(key.contains("AttribValue")) {
						
						strXMLAttribValues = strXMLAttribValues + "'" + mapSOAPRequestBuilder.get(key) + "'";
						if(!strXMLAttribValues.isEmpty())	//adding , after every attribute value which has been shared.
							strXMLAttribValues = strXMLAttribValues + ", ";
					}
					
				}
				
				strXMLAttribValues = strXMLAttribValues.substring(0,strXMLAttribValues.lastIndexOf(","));
				
				//Step2:
				String strXMLApexString = mapSOAPRequestBuilder.get("NameSpace") + "." + mapSOAPRequestBuilder.get("ClassName")
											+ "." + mapSOAPRequestBuilder.get("SOAPRequest")
											+ "(" + strXMLAttribValues + ");";
				
				//Step3:				
				strSOAP = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\""								
						+" xmlns:apex=\"http://soap.sforce.com/2006/08/apex\">"
				   +"<soapenv:Header>"
				   		+"<apex:SessionHeader>"
				       		+"<apex:sessionId>"+strCreds+"</apex:sessionId>"
				       +"</apex:SessionHeader>"
				   +"</soapenv:Header>"   
				   +"<soapenv:Body>"
					   +"<apex:executeAnonymous>"
					   		+"<apex:String>"
					   			+ strXMLApexString
					   		+"</apex:String>"						            						            
				        +"</apex:executeAnonymous>"
				   +"</soapenv:Body>"
				+"</soapenv:Envelope>"; 
				break;
				
			default :
				break;
		}
				
		return strSOAP;
	}
}