/* 
==============================================================
 Author     		:	Umesh Kute
 Class Name		 	: 	Utils
 Purpose     		: 	Utility functions to use across the application [Reusable functions]
 						1. Take screenshot during test script execution
 						2. Read data from excel file
 Date       		:	02/06/2015
 Version Information:	Version1.0
 PreCondition 		:	None
 Test Steps 		:	None
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
 #======================================================================
 */

package utility;

import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.imageio.ImageIO;
import javax.swing.JDialog;
import javax.swing.JLabel;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFTable;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.os.WindowsUtils;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;







import atu.ddf.file.ExcelFile;
import atu.ddf.selenium.SeleniumTestNGHelper;
//import com.sun.javafx.collections.MappingChange.Map;

import pageObjects.SalesForce.Configuration;
import pageObjects.SalesForce.LeadCreation;
import pageObjects.SalesForce.OfferingData;
import pageObjects.SalesForce.OrderFlow;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.Screenshot;
import ru.yandex.qatools.ashot.screentaker.ViewportPastingStrategy;



/*
This class contains the reusable function which can be used for 
taking screenshots during test script execution and reading data from excel file
@version: 1.0
@author: Umesh Kute
*/
public class Utils {
		public static WebDriver driver = null;
		
		 public Map<String, String> fn_takeScreenshot(WebDriver driver) throws Exception{		
				
				Map<String, String> mapScreenshot = new HashMap<String, String>();
				String strDateFormat;
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmssSSS");
				
				
				try {
					
					Log.debug(EnvironmentDetails.Path_ScreenShot);
					String strLocalTestCaseName = EnvironmentDetails.strTestCaseName.replaceAll(" ", "");
					Thread.sleep(3500);
					strDateFormat = sdf.format(System.currentTimeMillis());	// this is to be appended to the screenshot file name as a timestamp.
					
					File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
					File targetFile = new File(EnvironmentDetails.Path_ScreenShot, strLocalTestCaseName + "_" + strDateFormat + ".png");
					//String strScreenshotFileName = EnvironmentDetails.Path_ScreenShot + strTestCaseName + "_" + formatDate + ".png"; 
					FileUtils.copyFile(scrFile, targetFile);
					
					//creating the Screenshot's absolute path and get file name into map to be used by caller.
					mapScreenshot.put("screenshotAbsolutePath", targetFile.getAbsolutePath());			
					mapScreenshot.put("screenshotFileName", targetFile.getName());
					  
					return mapScreenshot;
				                
				} catch (Exception e){
				
					Log.error("Class Utils | Method takeScreenshot | Exception occured while capturing ScreenShot : "+e.getMessage());
					e.printStackTrace();
					throw new Exception();
				}
			}
		 
		 public Map<String, String> fn_takeScreenshotFullPage(WebDriver driver) throws Exception{		
				
				Map<String, String> mapScreenshot = new HashMap<String, String>();
				String strDateFormat;
				SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmssSSS");
				BufferedImage imgSnapshot;
				File fileScreenshot ;
				
				try {
					
					Thread.sleep(3500);
					strDateFormat = sdf.format(System.currentTimeMillis());	// this is to be appended to the screenshot file name as a timestamp.
					
					//TODO: Get the current coordinates of the browser content so as to reset to it 
					//		once the screenshot is taken.
					JavascriptExecutor jsExecutor = (JavascriptExecutor)driver;			
					Long longXOffset = (Long) jsExecutor.executeScript("return window.pageXOffset");
					Long longYOffset = (Long) jsExecutor.executeScript("return window.pageYOffset");
					
					//create snapshot file
					String strLocalTestCaseName = EnvironmentDetails.strTestCaseName.replaceAll(" ", "");
					String strScreenshotFileName = EnvironmentDetails.Path_ScreenShot + System.getProperty("file.separator") 
													+ strLocalTestCaseName + "_" + strDateFormat + ".png";
					fileScreenshot = new File(strScreenshotFileName);
					fileScreenshot.createNewFile();
					
					//Capture full page screen shot 
					Screenshot scrSnapshot = new AShot().shootingStrategy(new ViewportPastingStrategy(500)).takeScreenshot(driver);
					imgSnapshot = scrSnapshot.getImage();
					ImageIO.write(imgSnapshot, "png", fileScreenshot);
					//creating the Screenshot's absolute path and get file name into map to be used by caller.
					mapScreenshot.put("screenshotAbsolutePath", fileScreenshot.getAbsolutePath());			
					mapScreenshot.put("screenshotFileName", fileScreenshot.getName());
					  
					//return to initial page position 
					String strScrollPossition = "window.scrollTo("+longXOffset+","+longYOffset+")";
					jsExecutor.executeScript(strScrollPossition);
					Thread.sleep(1500);
					
					return mapScreenshot;
				                
				} catch (Exception e){
				
					Log.error("Class Utils | Method takeScreenshotFullPage | Exception occured while capturing ScreenShot : "+e.getMessage());
					e.printStackTrace();
					throw new Exception();
				}
			}

		 
	 	/// Function to read data from  excel file
		public Object[][] data(String strSheetName,String strTestcaseName) throws atu.ddf.exceptions.DataDrivenFrameworkException 
		{
		String excelResource = System.getProperty("user.dir")
		+ System.getProperty("file.separator") + "src" +System.getProperty("file.separator") + "testData"
		+ System.getProperty("file.separator") + "TestData.xlsx";
		// provide your excel file path here
		ExcelFile excelFile = new ExcelFile(excelResource);
		// provide the Sheet name
		excelFile.setSheetName(strSheetName);
		// provide the Column Name where Test Case names will be given
		excelFile.setTestCaseHeaderName("TestCaseNameColumn");
		// provide the Test Case Name
		List<List<String>> data = excelFile.getDataUsingTestCaseName(strTestcaseName);
		// SeleniumTestNGHelper is a utility class that will help in converting
		// the Test data into TestNG Data Provider supported format
		// One such format here is a 2-Dimensional Object Array
		return SeleniumTestNGHelper.toObjectArray(data);
	}	
		

		public static WebDriver fn_SetChromeDriverProperties()
		{
			//WebDriver driver;

			//EnvironmentDetailsSAS objEnv= new EnvironmentDetailsSAS();
			
			//FirefoxProfile profile = new FirefoxProfile(new File("C:\\Users\\madhavi.jn\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\u5qp8rnq.default"));                  
			//WebDriver driver = new FirefoxDriver(profile);

		  /*  ProfilesIni profile = new ProfilesIni();	 
			FirefoxProfile myprofile = profile.getProfile("default");
			//myprofile.setPreference("network.cookie.alwaysAcceptSessionCookies",true);
			//myprofile.setPreference(key, value);
		
			WebDriver driver = new FirefoxDriver(myprofile);

			 driver.get(objEnv.STRURL);	

			 return driver;*/
			
			
	    	
	    /*	String strFFProfilePath = "C:\\Users\\madhavi.jn\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\s6fkibr3.default";
	    	File fileFFProfileDirectory = new File(strFFProfilePath);
	    	FirefoxProfile ffProfile = new FirefoxProfile(fileFFProfileDirectory);
	    	
	    	driver = new FirefoxDriver(ffProfile);*/

		
			
			DesiredCapabilities chromeCapabilities = DesiredCapabilities.chrome();
            Map<String, Object> chromePrefs = new HashMap<String, Object>();           
            chromePrefs.put("download.prompt_for_download", true);
            
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("prefs", chromePrefs);
            chromeCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
			
           String Totalpath=WindowsUtils.getLocalAppDataPath()+"\\Google\\Chrome\\User Data\\Default";
			options.addArguments("user-data-dir="+Totalpath);
            chromeCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
            
			options.addArguments("--start-maximized");
            chromeCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
            
         /*   options.addArguments("--disable-notifications");
            chromeCapabilities.setCapability(ChromeOptions.CAPABILITY, options);*/
           
          //  options.setBinary("C:\\Users\\suhas.thakre\\AppData\\Local\\Google\\Chrome\\Application\\chrome.exe");
            chromeCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
			
			System.setProperty("webdriver.chrome.driver",EnvironmentDetailsSAS.CHROME_DRIVER_PATH);
			driver = new ChromeDriver(chromeCapabilities);
			return driver;			
		}
		
		public static WebDriver fn_SetChromeDriver2Properties()
		{
			//WebDriver driver;

			//EnvironmentDetailsSAS objEnv= new EnvironmentDetailsSAS();
			
			//FirefoxProfile profile = new FirefoxProfile(new File("C:\\Users\\madhavi.jn\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\u5qp8rnq.default"));                  
			//WebDriver driver = new FirefoxDriver(profile);

		  /*  ProfilesIni profile = new ProfilesIni();	 
			FirefoxProfile myprofile = profile.getProfile("default");
			//myprofile.setPreference("network.cookie.alwaysAcceptSessionCookies",true);
			//myprofile.setPreference(key, value);
		
			WebDriver driver = new FirefoxDriver(myprofile);

			 driver.get(objEnv.STRURL);	

			 return driver;*/
			
			
	    	
	    /*	String strFFProfilePath = "C:\\Users\\madhavi.jn\\AppData\\Roaming\\Mozilla\\Firefox\\Profiles\\s6fkibr3.default";
	    	File fileFFProfileDirectory = new File(strFFProfilePath);
	    	FirefoxProfile ffProfile = new FirefoxProfile(fileFFProfileDirectory);
	    	
	    	driver = new FirefoxDriver(ffProfile);*/

		
			
			DesiredCapabilities chromeCapabilities = DesiredCapabilities.chrome();
            Map<String, Object> chromePrefs = new HashMap<String, Object>();           
            chromePrefs.put("download.prompt_for_download", true);
            
			ChromeOptions options = new ChromeOptions();
			options.setExperimentalOption("prefs", chromePrefs);
            chromeCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
			
         /*  String Totalpath=WindowsUtils.getLocalAppDataPath()+"\\Google\\Chrome\\User Data\\Default";
			options.addArguments("user-data-dir="+Totalpath);
            chromeCapabilities.setCapability(ChromeOptions.CAPABILITY, options);*/
            
			options.addArguments("--start-maximized");
            chromeCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
            
            options.setBinary("C:\\Users\\madhavi.jn\\Desktop\\chrome53folder\\chrome.exe");
            chromeCapabilities.setCapability(ChromeOptions.CAPABILITY, options);
			
			System.setProperty("webdriver.chrome.driver",EnvironmentDetailsSAS.CHROME_DRIVER_PATH);
			driver = new ChromeDriver(chromeCapabilities);
			return driver;
			
			
			
			
			
		}
		
		/// Function to read data from  excel file -- TestData.xls, Sheet - TestCaseDetails
		//This function is included to refer TestData.xls file
		public Object[][] fn_DataProviderTC(String strSheetName,String strTestCaseName) throws Throwable
        {
               String excelResource = System.getProperty("user.dir") +System.getProperty("file.separator")
            		   				  + "testData" + System.getProperty("file.separator") 
            		   				  + "TestData.xlsx";
               // provide your excel file path here
               ExcelFile excelFile = new ExcelFile(excelResource);
               // provide the Sheet name
               excelFile.setSheetName(strSheetName);
               // provide the Column Name where Test Case names will be given
               excelFile.setTestCaseHeaderName("TestCaseNameColumn");
               // provide the Test Case Name
               List<List<String>> data = excelFile.getDataUsingTestCaseName(strTestCaseName);
               //String DataValue[]=data.toArray(new String[data.size()]);
               //String DataValue[]=data.toArray(new String[data.size()]);
               Log.debug("DEBUG: Number of data row available is "+data.size());
              
              
               // List< Map<String, Object> > ListOfMap =    new ArrayList< Map<String, Object> >();
              
               //Read header names from test cases
               ExcelHandler objExcelHandler= new ExcelHandler();
               ArrayList<String> arrReadExcelTCHedaersOnly = objExcelHandler.fn_ReadExcelTCHedaersOnly(new File(excelResource), strSheetName, strTestCaseName);
              
               //Initialize return 2D array size
               Object[][] datareturn = new Object[data.size()][1];
                     for (int i = 0; i < data.size(); i++) {
                            // Global Data parameter details
                            Map<String, Object> mapDataParameter = new HashMap<>();
                            Log.debug("DEBUG: Number of parameter for row " + (i+1)+" is "+data.get(i).size());
                            for (int j=0;j < data.get(i).size();j++)
                            {
                                   //System.out.println(arrReadExcelTCHedaersOnly.get(j)+ "  ==  "+data.get(i).get(j));
                                   mapDataParameter.put(arrReadExcelTCHedaersOnly.get(j), data.get(i).get(j));
                                  
                            }
                            datareturn[i][0] = mapDataParameter;
                            Log.debug("DEBUG: The data read for @Test execution is: "+datareturn[i][0].toString());
                     }
                     
                
                     return datareturn;
               //System.out.println(data);                   
               //return SeleniumTestNGHelper.toObjectArray(lstmapData);
             
        }
		  
		
		
		
		/*
		Author: Suhas Thakre
		Function fn_WriteExcelApttus: Write data in external excel.
		Inputs: strSheetName - Sheet name to write data.
				mapOutputData - Data to write in excel.
		Outputs: Boolean response to check success or failure of function .
		 */
		public void fn_WriteExcelApttus(String strSheetName , Map<String, Object> mapOutputData) {
			try {
				
				WriteExcel objWriteExcel = new WriteExcel();
				List<Map<String, Object>> lisOutputData = new ArrayList<>();
				List<List<Map<String, Object>>> lstmapOutputData = new ArrayList<>();
				//TODO:: Write account name in output file 
				lisOutputData.add(mapOutputData);
				lstmapOutputData.add(lisOutputData);
				objWriteExcel.fn_WriteExcel(lstmapOutputData, EnvironmentDetails.strFQNFileNameWrite, strSheetName);
			}
			catch (Exception ExceptionWriteExcelApttus) {
						Log.error("ERROR: Unable to Write Data in excel with exception reported as: "+ExceptionWriteExcelApttus.getMessage());
			            StringWriter stack = new StringWriter();
			            ExceptionWriteExcelApttus.printStackTrace(new PrintWriter(stack));
			            Log.debug("DEBUG: The exception in fn_WriteExcelApttus method is: "+stack);
			           
			}
		}
		
		
		
		//Function to Read Values from Excel By Condidtion as "Y"
		//Input Parameters:- File path and Sheet Name
		public List<Map<String, String>> readexcel_ExecEnvironment(String destFile,String vSheetName) {

			//TODO: create a List of Map so that multiple instances can be catered.
			List<Map<String, String>> lstExcelMap = new ArrayList<Map<String,String>>();				
			//creating the Try Block			
			try {

				//Creating the new File object by passing the destination file				
				File file = new  File(destFile);
				//passing the created file object as a param to Input stream				
				FileInputStream fileipstream = new FileInputStream(file);
				//creating a workbook object by fileipstream				
				XSSFWorkbook wb = new XSSFWorkbook(fileipstream);
				//creating a sheet object by vSheetName				
				XSSFSheet sheet = wb.getSheet(vSheetName);
				//capturing the initial row header value
				XSSFRow rowheader = sheet.getRow(0);

				//TODO: Get the Maximum Rows till which the loop needs to Iterate.
				int rows = 0;
				for (Row rowExecEnv : sheet)
					for(Cell cellExecEnv : rowExecEnv)
						if (cellExecEnv.getRichStringCellValue().getString().trim().equals("EOR")) {
							
							rows = rowExecEnv.getRowNum() - 1;
							break;
						}						
				
				//TODO: Get the Maximum Columns till which the loop needs to Iterate.
				int cols = 0;
				for(Cell cellExecEnv : rowheader)
					if (cellExecEnv.getRichStringCellValue().getString().trim().equals("EOL")) {
						
						cols = rowheader.getLastCellNum() - 2;
						break;
					}

				//For Loop for iterating from zero to last row of the Excel
				 for(int rowno = 1; rowno <= rows; rowno++) {

					 //Create Excel data map Object
					 Map<String, String> excelDataMap = new HashMap<String, String>();		
                     //capturing the rowno to row variable as rowno cant be used further
					 XSSFRow row = sheet.getRow(rowno);
					 
					 //parsing the column content for respective row	
					 for(int column = 0; column <= cols ; column++) {

						 //for putting all the values fetched into Map 
						 excelDataMap.put(rowheader.getCell(column).getStringCellValue(),row.getCell(column).getStringCellValue());
					 }
					 //add the read map into the list.
					 lstExcelMap.add(excelDataMap);
				 }
				 fileipstream.close();				 
			}
			catch(Exception ioe) {

				ioe.printStackTrace();
			}

			return lstExcelMap;	//returning the read list.
		}
		
		/*
		Author: Aashish Bhutkar
		Function fn_getPreviousDate: Returns the date of previous month(s).
		Inputs:	intDate - which date of the month to be returned.
				intMonthsBack - How many Months Back.
		Outputs: formated date string for the date requested.    
*/
		 public String fn_getFutureDate(Integer intMonthsFuture, String PATTERN)
		 {	
				SimpleDateFormat dateFormat = new SimpleDateFormat();
				dateFormat.applyPattern(PATTERN);

				//TODO: Get current date and then trace it back by 5 days for loading data.
				Calendar cal = Calendar.getInstance();
				cal.add(Calendar.MONTH, +intMonthsFuture);				
				String date1 = dateFormat.format(cal.getTime());
				
				return date1;
		 }
		 
		 
	 //Remove currency code from price  
	 public String fn_CleanCurrencyCode(String strPricingAmount )
	 {	
			try {
				
				if (strPricingAmount.contains("EUR") || strPricingAmount.contains("USD")) {
					
					strPricingAmount = strPricingAmount.replace("EUR", "");
					strPricingAmount = strPricingAmount.replace("USD", "");
				}
				else {
					Log.debug("DEBUG: Currency code not available in price.");
				}
			} catch (Exception ExceptionClnCurrencyCode) {
				
				Log.error("ERROR: Unable to Clean Currency Code with exception reported as: "+ExceptionClnCurrencyCode.getMessage());
	            StringWriter stack = new StringWriter();
	            ExceptionClnCurrencyCode.printStackTrace(new PrintWriter(stack));
	            Log.debug("DEBUG: The exception in fn_CleanCurrencyCode method is: "+stack);
			}
			return strPricingAmount.trim();
	 }
	
	 
	//Purpose :- function to wait for spinner 
		//Input:- driver to be passed
	    public static void fn_waitForSpinner(WebDriver driver)
	    {
	        By bySpinnerLocator = By.className("spinnerImg-tree");
	        WebDriverWait webWait = new WebDriverWait(driver, 120);
	        try{
	            Log.debug("DEBUG: Waiting for spinner to disappear..");
	            webWait.until(ExpectedConditions.invisibilityOfElementLocated(bySpinnerLocator));
	            Log.debug("DEBUG: Finished Waiting..");
	        }
	        catch(TimeoutException exceptionTimeOut)
	        {
		    	StringWriter stack = new StringWriter();
				exceptionTimeOut.printStackTrace(new PrintWriter(stack));
				Log.debug("DEBUG: The exception while waiting for the Spinner is: "+stack);
	        }
	    }
	    
		//Purpose :- function to wait for ProgressBar 
		//Input:- driver to be passed
	    public static void fn_waitForProgressBar(WebDriver driver)
	    {
	    	By byProgressLocator = By.xpath("//*[@id='ngProgress']");
	        WebDriverWait webWait = new WebDriverWait(driver, 120);
	        try{

	        	do{
	        		Log.debug("DEBUG: Waiting for ProgressBar to disappear..");
	                webWait.until(ExpectedConditions.invisibilityOfElementLocated(byProgressLocator));
	                Log.debug("DEBUG: Finished Waiting..");
	        	}while(driver.findElement(byProgressLocator).isDisplayed());

	        }
	        catch(TimeoutException exceptionTimeOut)
	        {
		    	StringWriter stack = new StringWriter();
				exceptionTimeOut.printStackTrace(new PrintWriter(stack));
				Log.debug("DEBUG: The exception while waiting for the Progress Bar is: "+stack);
	        }
	    }
	    
	    	//Purpose :- function to wait for loadingBar 
	  		//Input:- driver to be passed
	  	    public  void fn_waitForloadingBar(WebDriver driver)
	  	    {
	  	    	try{
	  	    		//new WebDriverWait(driver,3).until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='loadingBar']/img")));
	  	        	Log.debug("DEBUG: Waiting for loadingBar to disappear..");
  	        		new WebDriverWait(driver,120).until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@id='loadingBar']/img")));		
  	                Log.debug("DEBUG: Finished Waiting..");
	  	        	
	  	        }
	  	        catch(TimeoutException exceptionTimeOut)
	  	        {
	  		    	StringWriter stack = new StringWriter();
	  				exceptionTimeOut.printStackTrace(new PrintWriter(stack));
	  				Log.debug("DEBUG: The exception while waiting for the loadingBar Bar is: "+stack);
	  	        }
	  	    }
	    
		//Purpose :- function to wait for Loader 
		//Input:- driver to be passed
	    public void fn_waitForLoader(WebDriver driver)
	    {
	       // By bySpinnerLocator = By.className("loading ng-hide");
	       // WebDriverWait webWait = new WebDriverWait(driver, 120);
	        try{
	            Log.debug("DEBUG: Waiting for Loader to disappear..");
	            new WebDriverWait(driver,120).until(ExpectedConditions.invisibilityOfElementLocated(By.className("loading ng-hide")));
	            //webWait.until(ExpectedConditions.invisibilityOfElementLocated(bySpinnerLocator));
	            Log.debug("DEBUG: Finished Waiting..");
	        }
	        catch(TimeoutException exceptionTimeOut)
	        {
		    	StringWriter stack = new StringWriter();
				exceptionTimeOut.printStackTrace(new PrintWriter(stack));
				Log.debug("DEBUG: The exception while waiting for the Loader is: "+stack);
	        }
	    }
	    
	    
		//Purpose :- function to Scroll to an element
		//Input:- driver and webelement to be scrolled should be passed
	    public static void fn_scrollToElement(WebDriver driver,WebElement element) throws InterruptedException
	    {
	        try{
	            Log.debug("DEBUG: Scrolling to the Element...");
				String strScrollYCoord = "window.scrollTo(0,"+ (element.getLocation().y-75)+")";	
				((JavascriptExecutor) driver).executeScript(strScrollYCoord);
				Thread.sleep(3500);
	        }
	        catch(TimeoutException exceptionTimeOut)
	        {
		    	StringWriter stack = new StringWriter();
				exceptionTimeOut.printStackTrace(new PrintWriter(stack));
				Log.debug("DEBUG: The exception while Scrolling to the Element is: "+stack);
	        }
	    } 
	 
   public static void readExcelastable(String destFile) throws FileNotFoundException	
   {
		
	   try {
		   
	    FileInputStream fileipstream = new FileInputStream(new File(destFile));
		XSSFWorkbook workbook = new XSSFWorkbook(fileipstream);
	    int numberOfSheets = workbook.getNumberOfSheets();
	    
	    for (int sheetIdx = 0; sheetIdx < numberOfSheets; sheetIdx++) 
	    {
	        XSSFSheet sheet = workbook.getSheetAt(sheetIdx);
	        List<XSSFTable> tables = sheet.getTables();
	        for (XSSFTable t : tables) {
	            System.out.println(t.getDisplayName());
	            System.out.println(t.getName());
	            System.out.println(t.getNumerOfMappedColumns());

	            int startRow = t.getStartCellReference().getRow();
	            int endRow = t.getEndCellReference().getRow();
	            System.out.println("startRow = " + startRow);
	            System.out.println("endRow = " + endRow);

	            int startColumn = t.getStartCellReference().getCol();
	            int endColumn = t.getEndCellReference().getCol();

	            System.out.println("startColumn = " + startColumn);
	            System.out.println("endColumn = " + endColumn);

	            for (int i = startRow; i <= endRow; i++) {
	                String cellVal = "";

	                for (int j = startColumn; j <= endColumn; j++) {
	                    XSSFCell cell = sheet.getRow(i).getCell(j);
	                    if (cell != null) {
	                        cellVal = cell.getStringCellValue();
	                    }
	                    System.out.print(cellVal + "\t");
	                }
	                
	                System.out.println();
	            }

	        }
	    }

	    //workbook.

		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
   	  }
   
   public static void fn_HighlightElement(WebDriver driver,WebElement element) throws InterruptedException{
       
       //Creating JavaScriptExecuter Interface
       JavascriptExecutor jsHighlightElement = (JavascriptExecutor)driver;
       for (Integer intWaitCounter = 0; intWaitCounter < 2; intWaitCounter++) {
              
              String strElementBorder = element.getCssValue("border").toString().trim();
              
              jsHighlightElement.executeScript("arguments[0].style.border='3px dotted red'", element);
            Thread.sleep(500);
            String strArguementVal = "arguments[0].style.border='"+strElementBorder+"'";
            jsHighlightElement.executeScript(strArguementVal, element);
       }
   }
   
   //Close remainder window 
   public void fn_CloseRemainder(WebDriver driver,WebDriverWait webWait) throws Throwable
	{   
	   String strParentWindow = driver.getWindowHandle();
	   Thread.sleep(1000);
		   try {
			   for (String handle : driver.getWindowHandles()) {
			   		driver.switchTo().window(handle);
	                try {
                		driver.findElement(By.xpath("//*[text()='Reminder']")).isDisplayed();
                		driver.close();
					} catch (Exception e) {
						Log.debug("DEBUG: Not a Remainder window.");// TODO: handle exception
					}
                }
		   driver.switchTo().window(strParentWindow);
		} catch (Exception e) {
			Log.debug("DEBUG: Remainder window not present.");
		}
	}
   
    
   
   public boolean fn_HandleLookUpWindow(WebDriver driver,WebDriverWait webWait,String strWindowName,String strData)
		{
			
			LeadCreation objLeads = new LeadCreation();
			OfferingData objOffering = new OfferingData();
			OrderFlow objOrderflowPO = new OrderFlow();
			Configuration objConfig = new Configuration();
			String strParentWindow = driver.getWindowHandle();

			//Click on LookupWindow
			try 
			{	    
	
				if (strWindowName.contains("Offering"))
			    	objConfig.fn_getOfferWindow(driver, webWait).click();
			    else if(strWindowName.contains("NDA_Address"))
			    	objConfig.fn_getNDAAddressWindow(driver, webWait).click();
				else if(strWindowName.contains("AccountLookupWndw"))
	               objLeads.fn_ClickAcctLookUpwndw(driver,webWait).click();
				else if(strWindowName.contains("Customer Reference Lookup"))
		               objLeads.fn_ClickCustRefLookUpwndw(driver,webWait).click();
				else if(strWindowName.contains("Contact Lookup (New Window)"))
					objOrderflowPO.fn_clickSendToImg(driver, webWait).click();
				else if(strWindowName.contains("Address Lookup (New Window)"))
					objOrderflowPO.fn_clickAddressLukUpWndw(driver, webWait).click();
				else if(strWindowName.contains("Reports To Lookup (New Window)"))
					objOrderflowPO.fn_clickReportLukUpWndw(driver, webWait).click();
				else if(strWindowName.contains("ContactLukUpWindwOnAdCtctEdtPage"))
					objLeads.fn_clickContactLukUpWindwOnAdCtctEdtPage(driver, webWait).click();
				else
				  objOffering.fn_clickLookUpWindow(driver,webWait,strWindowName).click();
			      Thread.sleep(1000);
		      for (String handle : driver.getWindowHandles()) {

	                 driver.switchTo().window(handle);
	                 if (driver.getTitle().contains("Search ~ Salesforce")) {
	                	break;
	                	//Search ~ Salesforce - Unlimited Edition
					}
	               }
		      
		      driver.manage().window().maximize();  
		   //  Thread.sleep(3500);
			//TODO: get the frame for the Child window
		/*	Set<String> strWindowHandles = driver.getWindowHandles();   		
			strWindowHandles.remove(strParentWindow);
			driver.switchTo().window(strchildWindow);	*/// setting the window driver to the newly launched Print Frame Window.     
			//WebElement rootTree = null;
			//rootTree = driver.findElement(By.tagName("FRAMESET"));
		//	WebElement parentFrame = driver.findElement(By.name("searchFrame"));
			driver.switchTo().frame("searchFrame");	 
		//	driver.switchTo().frame("//frame[@name='searchFrame']");
		//	driver.switchTo().frame(parentFrame);
			objLeads.fn_clickSearchBtn(driver, webWait).isDisplayed();
			objLeads.fn_clickSearchBtn(driver, webWait).sendKeys(strData);
			objLeads.fn_clickGoBtn(driver, webWait).isEnabled();
			objLeads.fn_clickGoBtn(driver, webWait).click();
		//	Thread.sleep(5000);//Click on the search item
			driver.switchTo().defaultContent();
			WebElement ResultsFrame = driver.findElement(By.name("resultsFrame"));
			driver.switchTo().frame(ResultsFrame);
			objLeads.fn_findList(driver, webWait).isDisplayed();
			String strLen = objLeads.fn_findList(driver, webWait).getText();
				if (strLen.length()>0)
					Log.debug("DEBUG: Got the text from search window");
				else
					Log.debug("DEBUG: Unable to Get the text from search window");		
				objLeads.fn_findList(driver, webWait).click();
				driver.switchTo().window(strParentWindow);}
			
			catch (InterruptedException exceptionLookUpWin) {
				// TODO Auto-generated catch block
				StringWriter stack = new StringWriter();
				exceptionLookUpWin.printStackTrace(new PrintWriter(stack));
				Log.debug("DEBUG: The exception in Handling the Lookup window is: "+stack);
				return false;}
			
			return true;
		}
   
   /*
   Author: Aashish Bhutkar
   Function fn_DisplayStartMessage: Function to display the message box while execution is ON.
   Inputs: strStartMessage - The start message string to be displayed in the dialog box as per the call.
                 strtStartNote - The start message note to be displayed in the Dialog box.
   Outputs: JDialog - is the handle for the dialog box created while message is being displayed.
*/
   public JDialog fn_DisplayStartMessage(String strStartMessage, String strtStartNote) throws Exception {       
         
          //TODO: Displaying a Message Box to indicate the start of the execution.
         String strPrintFrameStartMsg = "<html><div align='center'><br><br><font size='6' face='SANS_SERIF' color='red'> "+ strStartMessage +"</font>"
                       + "<br><br><br><br><br><br><br><font size='4' face='SANS_SERIF' color='red'>"+ strtStartNote +"</font></div></html>";
         JDialog jdlgPrintFrameBeforeExec = new JDialog();
         jdlgPrintFrameBeforeExec.setTitle("Apttus stand-alone Test says..");
         jdlgPrintFrameBeforeExec.setModal(false);
         jdlgPrintFrameBeforeExec.add(new JLabel(strPrintFrameStartMsg, JLabel.CENTER));
         jdlgPrintFrameBeforeExec.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
         jdlgPrintFrameBeforeExec.setAlwaysOnTop(true);
         jdlgPrintFrameBeforeExec.setLocationRelativeTo(null);
         jdlgPrintFrameBeforeExec.setResizable(false);
         jdlgPrintFrameBeforeExec.pack();
          jdlgPrintFrameBeforeExec.setLocation((Toolkit.getDefaultToolkit().getScreenSize().width)/2 - jdlgPrintFrameBeforeExec.getWidth()/2,
                                                                        (Toolkit.getDefaultToolkit().getScreenSize().height)/2 - jdlgPrintFrameBeforeExec.getHeight()/2);
         jdlgPrintFrameBeforeExec.setVisible(true);
         //Thread.sleep(5000);
         //jdlgPrintFrameBeforeExec.dispose();
        
         return jdlgPrintFrameBeforeExec;
   }

   
   
   
   /* 
   Author: Aashish Bhutkar 
   Function fn_ValidateResponses: This function is associative function to help identify if the API response has any errors.
   Inputs: lstmapActualResponse - This function requires the list hashmap input which is scanned for existing of any error data.                   
   Outputs: Boolean response based on the scanning of the API response data. 
*/      
   public Boolean fn_ValidateResponses(List<List<Map<String, Object>>> lstmapActualResponse, String strCallType) { 
           
           Boolean blnIsResponseErrorFree = true; 
           
           //TODO: Check if the response received is Error / Fault free and return the status accordingly. 
           for(List<Map<String, Object>> lstmap: lstmapActualResponse) 
                   for(Map<String, Object> map: lstmap) { 
                           
                           if(map.containsKey("error")) 
                                   blnIsResponseErrorFree = false; 
                           
                           if(map.containsKey("fault")) 
                                   blnIsResponseErrorFree = false; 
                           
                           if(map.containsKey("message")) 
                                   blnIsResponseErrorFree = false; 
                           
                           if(map.containsKey("error_msg") && !strCallType.equalsIgnoreCase("HTTP")) 
                                   blnIsResponseErrorFree = false; 
                           
                           if(map.containsKey("errorMsg")) 
                                   blnIsResponseErrorFree = false; 
                           
                           if(map.containsKey("SOAPFault")) 
                                   blnIsResponseErrorFree = false;                         
                   } 
                           
           return blnIsResponseErrorFree; 
   }

   
   

/*
   Author: Aashish Bhutkar
   Function fn_DisplayEndMessage: Function to display the message box after execution is completed.
   Inputs: jdlgPrintFrameBeforeExec - has reference to the dialog box which has been created to show message.
                 strEndMessage - The end message string to be displayed in the dialog box as per the call.
                 strtEndNote - The end message note to be displayed in the Dialog box.
*/
   public void fn_DisplayEndMessage(JDialog jdlgPrintFrameBeforeExec, String strEndMessage, String strtEndNote)
                       throws Exception {
         
          //TODO: Displaying a Message Box to indicate the start of the delay.
          jdlgPrintFrameBeforeExec.dispose();
          String strPrintFrameStartMsg = "<html><div align='center'><br><br><font size='6' face='SANS_SERIF'>"+ strEndMessage + "</font>"
                       + "<br><br><br><br><br><br><br><font size='4' face='SANS_SERIF' color='red'>"+ strtEndNote +"</font></div></html>";
          JDialog jdlgPrintFrameAfterExec = new JDialog();
         jdlgPrintFrameAfterExec.setTitle("Apttus stand-alone Test says..");
         jdlgPrintFrameAfterExec.setModal(false);
         jdlgPrintFrameAfterExec.add(new JLabel(strPrintFrameStartMsg, JLabel.CENTER));
         jdlgPrintFrameAfterExec.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
         jdlgPrintFrameAfterExec.setAlwaysOnTop(true);
         jdlgPrintFrameAfterExec.setLocationRelativeTo(null);
         jdlgPrintFrameAfterExec.setResizable(false);
         jdlgPrintFrameAfterExec.pack();
          jdlgPrintFrameAfterExec.setLocation((Toolkit.getDefaultToolkit().getScreenSize().width)/2 - jdlgPrintFrameAfterExec.getWidth()/2,
                               (Toolkit.getDefaultToolkit().getScreenSize().height)/2 - jdlgPrintFrameAfterExec.getHeight()/2);
         jdlgPrintFrameAfterExec.setVisible(true);
         Thread.sleep(5000);
         jdlgPrintFrameAfterExec.dispose();
   }   
   
	}
