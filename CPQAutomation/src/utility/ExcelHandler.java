package utility;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.Reporter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.*;
import java.util.Map.Entry;

/**
 * Created by joe.gannon on 3/3/2016.
 */
public class ExcelHandler
{
    public HashMap<String,ArrayList<HashMap<String,String>>> readDataSheet(String filePath, String sheetName)
    {
        HashMap<String,ArrayList<HashMap<String,String>>> entireMapList = new HashMap<String,ArrayList<HashMap<String,String>>>();
        try
        {
            File excelFile = new File(filePath);
            FileInputStream fis = new FileInputStream(excelFile);
            HSSFWorkbook workbook = new HSSFWorkbook(fis);
            System.out.println("~~~~~~~~~~~~~~~"+excelFile.getName()+"~~~~~~~~~~~~~~~");
            for(int i=0;i < workbook.getNumberOfSheets(); i++)
            {
                ArrayList<HashMap<String,String>> sheetMap = new ArrayList<HashMap<String,String>>();
                String sheetTitle = workbook.getSheetName(i);
//                System.out.println("\tSheet Name: "+workbook.getSheetName(i));
                HSSFSheet sheet = workbook.getSheetAt(i);
                Iterator<Row> rowIterator = sheet.iterator();
                rowIterator.next(); // skip first row
                Row headerRow = rowIterator.next();
                int numHeaders = headerRow.getPhysicalNumberOfCells();
//                System.out.println("numHeaders: "+numHeaders);
                while(rowIterator.hasNext())
                {
                    Row currentRow = rowIterator.next();
                    if(currentRow.getCell(0) != null)
                    {
                        HashMap<String,String> rowMap = new HashMap<String,String>();
                        
                        for(int x = 0; x < numHeaders; x++)
                        {

                            String header = headerRow.getCell(x,currentRow.CREATE_NULL_AS_BLANK).toString();
                            String value = currentRow.getCell(x, currentRow.CREATE_NULL_AS_BLANK).toString();
                            rowMap.put(header,value);
    
                            //System.out.println("\t|--> Header: "+headerRow.getCell(x)+ " || Value: "+currentRow.getCell(x));
                        }
                            
                        if (sheetTitle.contains("Step 3 Setup Bundles") || sheetTitle.contains("Step 3 Setup Base Prices") || sheetTitle.contains("Step 4 Setup Tier Prices")){                           	  
                        	if (rowMap.containsKey("Original CURRENCY") && rowMap.containsValue("EURO")) 
                        		sheetMap.add(rowMap);}
                        else 
                                sheetMap.add(rowMap);
//                        System.out.println("\t~~~~~~~~~~~~~~~~~~~~~~~End Row~~~~~~~~~~~~~~~~~~~~~~~~~");
                    }
                    entireMapList.put(sheetTitle,sheetMap);

                }

//                System.out.println("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
                //map<Sheet, List<map<header,data>>>
            }
//
//            System.out.println("NumSheets: "+entireMapList.size());



        }catch(Exception e)
        {
            System.out.println("File not Found! "+filePath);
            e.printStackTrace();
        }
        return entireMapList;
    }
    
    public static ArrayList<String> readExcelFileAsArray(File file, String strWorksheet) throws FileNotFoundException
    {
        Log.debug("DEBUG: Reading file: "+file.getAbsolutePath()+" and worksheet: "+strWorksheet.toString());
        try
        {
            FileInputStream fis = new FileInputStream(file);
            XSSFWorkbook workbook = new XSSFWorkbook(fis);
            //XSSFWorkbook workbook = new XSSFWorkbook();
            XSSFSheet sheet = workbook.getSheet(strWorksheet);
            //XSSFSheet sheet = workbook.getSheet(strWorksheet);
            Iterator<Row> rowIterator = sheet.iterator();

            //TODO: Create headers and cellValues objects to read the file contents.
            List<String> headers = new ArrayList<String>();
            ArrayList<String> cellValues = new ArrayList<String>();

            //set headers flag
            boolean areHeadersLoaded = false;

            //Iterate through excel and get cells
            while(rowIterator.hasNext())
            {
            	//TODO: Define another objects for row &cell iterators to save read data.
                Row row = rowIterator.next();
                Iterator<Cell> cellIterator = row.cellIterator();

                //TODO: loading the headers first.
                if(!areHeadersLoaded)
                {
                    while(cellIterator.hasNext())
                    {
                        Cell cell = cellIterator.next();
                        headers.add(cell.getStringCellValue());
                    }
                    areHeadersLoaded = true;
                }
                else
                {
                    //TODO: Iterating through rows to retrieve cell data.
                    while(cellIterator.hasNext())
                    {
                        Cell cell = cellIterator.next();
                        String strCellValue;
                        switch(cell.getCellType())
                        {
                            case Cell.CELL_TYPE_STRING:
                                strCellValue = cell.getStringCellValue();
                                cellValues.add(strCellValue);
                                break;
                            case Cell.CELL_TYPE_NUMERIC:
                                strCellValue = Integer.toString((int) cell.getNumericCellValue());
                                cellValues.add(strCellValue);
                                break;
                            case Cell.CELL_TYPE_BOOLEAN:
                                strCellValue = Boolean.toString(cell.getBooleanCellValue());
                                cellValues.add(strCellValue);
                                break;
                            default:
                        }
                    }
                }
            }
            Log.debug("DEBUG: File has been read successfully !");
            return cellValues;
        }
        catch (Exception exception)
        {
            Log.error("ERROR: Error reading excel file.");
            Reporter.log("ERROR: Error reading excel file");
            exception.printStackTrace();
            return null;
        }
    }
    
	/*
    Author: Aashish Bhutkar
    Function fn_ReadTestCases: Read Values from an Test Cases sheet of the excel (ONLY xlsx supported).
    Inputs: fileTCName - The File name for the Test cases workbook.
                  strWorksheetName - The work sheet name to read the data.
                  strTestCaseName - The test case name for which the data is to be read.
    Outputs: List of Hashmap is the output which actually is the read test case data from sheet.
*/
           
    public ArrayList<String> fn_ReadExcelTCHedaersOnly(File fileTCName, String strWorksheetName, String strTestCaseName) throws Exception {
           
           Log.debug("DEBUG: The file to be read is: "+fileTCName.getAbsolutePath()+" and the worksheet name is: "+strWorksheetName+" and TC Name is: "+strTestCaseName);
           FileInputStream fisTCFile = new FileInputStream(fileTCName);
           XSSFWorkbook workbook = new XSSFWorkbook(fisTCFile);
           XSSFSheet sheet = workbook.getSheet(strWorksheetName);
           //Iterator<Row> rowIterator = sheet.iterator();
           
           ArrayList<String> arrHeaders = new ArrayList<String>();
           String strValue = "";
           Integer intRowNumber = 0;
           
          Integer intMaximumRowNum = sheet.getLastRowNum(); 
          
         //TODO: Traverse the file for searching the Row which has the data for TC under test.
          for (int i = 0; i < intMaximumRowNum; i++) {
			if (sheet.getRow(i).getCell(0) !=null) {
				
        		  //System.out.println(" =====================row 0 data======================"+sheet.getRow(i).getCell(0).toString());
        		  if(sheet.getRow(i).getCell(0).toString().equalsIgnoreCase(strTestCaseName)) {
                      
                      intRowNumber = sheet.getRow(i).getRowNum();
                      break;
               }
            	  
			} else {
				//System.out.println(" =====================Null======================");
			}
		}
           
           if(intRowNumber == 0)
                  VerificationMethods.assertTrue(false, "ERROR: There is no Test Case data available as per search data requested for...");     
           
           //TODO: For the row number found, read all the Column Names in an array.
           Row rowExcelFileColumns = sheet.getRow(intRowNumber);
           for(Integer intCol = 1; intCol < rowExcelFileColumns.getLastCellNum(); intCol ++) {
                  
                  Row rowExcelFileData = sheet.getRow(intRowNumber);
                  strValue = rowExcelFileData.getCell(intCol).getStringCellValue().trim();
                  arrHeaders.add(strValue);
           }
           
           fisTCFile.close();
           return arrHeaders;
    }

    
    
    //Read Apttus Query from excel 
    public String fn_ReadApttusQuery(String fileName, String strWorksheetName, String strApttusQuery) throws Exception {
        
    	String excelResource = System.getProperty("user.dir")
				+System.getProperty("file.separator")+ "testData"
				+ System.getProperty("file.separator") + fileName+".xlsx";
        
        File fileworkbookName = new File(excelResource);
        FileInputStream fisTCFile = new FileInputStream(fileworkbookName);
        Log.debug("DEBUG: The file to be read is: "+fileworkbookName.getAbsolutePath()+" and the worksheet name is: "+strWorksheetName+" and TC Name is: "+strApttusQuery);
        XSSFWorkbook workbook = new XSSFWorkbook(fisTCFile);
        XSSFSheet sheet = workbook.getSheet(strWorksheetName);
        
        String strValue = "";
        Integer intRowNumber = 0;
        Integer intMaximumRowNum = sheet.getLastRowNum(); 
       //Traverse the file for searching the Row which has the data for TC under test.
	       for (int i = 0; i <= intMaximumRowNum; i++) {
				if (sheet.getRow(i).getCell(0) !=null) {
					
					//System.out.println(" =====================row 0 data======================"+sheet.getRow(i).getCell(0).toString());
	     		  if(sheet.getRow(i).getCell(0).toString().equalsIgnoreCase(strApttusQuery)) {
	                   
	                   intRowNumber = sheet.getRow(i).getRowNum();
	                   break;
	            }
	         	  
				} else {
					//System.out.println(" =====================Null======================");
				}
			}
        
        if(intRowNumber == 0)
               VerificationMethods.assertTrue(false, "ERROR: There is no Query available as per search data requested for...");     
        
        //For the row number found, read Query.
        Row rowExcelFileData = sheet.getRow(intRowNumber);
        strValue = rowExcelFileData.getCell(1).getStringCellValue().trim();
        
        
        fisTCFile.close();
        return strValue;
    }
    
    
    //Purpose:- To Read Excel File as Map
    //Inputs:- file name and worksheet name
    //Output:- Returns Map which consists of cell data along with header values
	public List<Map<String,String>> fn_ReadExcelAsMap(File file, String strWorksheetName) throws Exception 
    {
    
    FileInputStream fis = new FileInputStream(file);
    XSSFWorkbook workbook = new XSSFWorkbook(fis);
    XSSFSheet sheet = workbook.getSheet(strWorksheetName);
    Iterator<Row> rowIterator = sheet.iterator();  
	
	List<Map<String,String>> sheetMap = new ArrayList<Map<String,String>>();
	
	Row headerRow = rowIterator.next();
	
		  while(rowIterator.hasNext())
		  {
		    Row currentRow = rowIterator.next();
	
	        int numHeaders = headerRow.getPhysicalNumberOfCells();
			
			    if(currentRow.getCell(0) != null)
	            {
			    	HashMap<String,String> rowMap = new HashMap<String,String>();
			    	
				    for(int x = 0; x < numHeaders; x++)
	                {
	                    String header = headerRow.getCell(x,Row.CREATE_NULL_AS_BLANK).toString();
	                    String value = currentRow.getCell(x,Row.CREATE_NULL_AS_BLANK).toString();
	                    rowMap.put(header,value);
	                }					    
				    sheetMap.add(rowMap);
		        }	
			   
		   }
	  
	return sheetMap;
	}
    
    
}
