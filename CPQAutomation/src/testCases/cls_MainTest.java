/*
============================================================================================================================
Author                   :        Aashish Bhutkar
Modified By            :        Aashish Bhutkar
 Class Name                     :        cls_MainTest
 Purpose                  :        Entry point for the stand-alone tool.
 Date                      :        11/08/2016
Modified Date                   :       
Version Information:          Version 1.0
 PreCondition           :        APIProperties & Driver Data Sheet files should be logically populated.
Test Steps              :        1. Be the entry point for stand alone tool.
                                                         2. Call the TestNG class for actual tests.
                                                         3. Close the executable jar once the execution is completed.
 
 Copyright notice      :        Copyright(C) 2016 Sungard Availability Services -
                                                          All Rights Reserved
 ============================================================================================================================
*/
 
package testCases; 
 
import java.io.PrintWriter;
import java.io.StringWriter;
 


import javax.swing.JDialog;

import org.apache.log4j.xml.DOMConfigurator;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;
 


import utility.Log;
import utility.Utils;
 
public class cls_MainTest {
 
/*
          Author: Aashish Bhutkar
          Function main: Main class introduced for testing the API Tool App as a jar.
*/      
         
          public static void main(String[] args) {
        	  
        	  Utils objUtils = new Utils();
                  
                   try {
                  
                             //TODO: Initialize the Log4j mentioning the path for its configuration file.
                             String strLog4jPath = System.getProperty("user.dir") + System.getProperty("file.separator")
                                                   +  "testData" + System.getProperty("file.separator") + "log4j.xml";
                             DOMConfigurator.configure(strLog4jPath);
                            
                             Log.debug("DEBUG: The Main Class has been called !");
                             
                             JDialog jdlgMessageBox =  objUtils.fn_DisplayStartMessage("Started the Apttus_E2E_Flow_API Test", "Completion of Standalone Test will close this dialog box automatically!");
                             
                             
                             TestListenerAdapter tla = new TestListenerAdapter();
                             TestNG testng = new TestNG();
                             testng.setTestClasses(new Class[] {Cls_Apptus_Amendment_Flow_Existing_Acct.class});
                             testng.addListener(tla);
                             testng.run();
                             
                             objUtils.fn_DisplayEndMessage(jdlgMessageBox, "Execution of the Standalone Test is completed!", "This dialog box will be automatically closed in approx 5 seconds!");
                            
                             System.exit(0);        //exit the java program once execution is completed.
                    }
                   catch (Exception exceptionMain) {
                            
                             Log.debug("DEBUG: Exception captured as: "+exceptionMain);
                             StringWriter stack = new StringWriter();
                             exceptionMain.printStackTrace(new PrintWriter(stack));
                             Log.debug("DEBUG: The exception recorded in Main class is: "+stack);
                   }
          }
}