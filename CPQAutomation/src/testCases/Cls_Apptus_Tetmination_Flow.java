/*
 Author     		:	Suhas Thakre
 Class Name			: 	Cls_Apptus_E2E_Flow 
 Purpose     		: 	Purpose of this file is :
						1. Create New Order 

 Date       		:	21/02/2017 
 Modified Date		:		
 Modified By		: 	
 Version Information:	Version 1.0
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/

package testCases;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import appModules.Add_Config_for_MultiProducts;
//import pageObjects.SalesForce.AccountPageObjects;
import appModules.Add_Configurations;
import appModules.Add_Multiple_Products;
import appModules.AfterApprovalProcess;
//import appModules.Add_Configurations;
import appModules.ApprovalValidation;
import appModules.CreateAmendRenew;
import appModules.CreateLeadandConvertToMultipleAcct;
import appModules.MSAGeneration_And_Activation;
import appModules.ApptusLoginLogout;
import appModules.CreateOfferingData;
import appModules.CreateProposal;
import appModules.MSOutlookApproval;
import appModules.OrderFlow_Generation_And_Activation;
import appModules.PricingValidation;
import appModules.Termination;
import utility.EnvironmentDetails;
import utility.Log;
import utility.Utils;
import utility.VerificationMethods;


//E2E flow for New Order Creation
public class Cls_Apptus_Tetmination_Flow extends VerificationMethods{
	
	
		public WebDriver driver;
		public WebDriverWait webWait;
		public EnvironmentDetails objEnv;
	
		 @BeforeClass
		 public void beforeMethod()throws Exception
		 {	
			// Initialize test case variable 
				EnvironmentDetails.strTestCaseName = "Apptus_Tetmination_Flow";
				 
				//TODO: Initialize the Logger.
		    	DOMConfigurator.configure("log4j.xml");
		    	Log.fn_InitializeExtentLogger(EnvironmentDetails.strTestCaseName);
		    	
		    	
		    	
		    	//TODO: Set Chrome driver as the driver to launch the test.
		    	driver = utility.Utils.fn_SetChromeDriverProperties();
		    	    	
				//TODO: initialize the webWait
				webWait = new WebDriverWait(driver,60);

				objEnv = new EnvironmentDetails();
				//Save EnvironmentDetails object to global variable map 
				EnvironmentDetails.mapGlobalVariable.put("objEnv", objEnv);
				driver.get(EnvironmentDetails.mapEnvironmentDetails.get("InstanceURL"));
				
				//Logs in to the Apptus application with the appropriate credentials	
				//ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("InstUserName"), EnvironmentDetails.mapEnvironmentDetails.get("InstPassWord"));
				
				
	            //Name of output data file.
				Log.debug("DEBUG: Output file to be used for this execution is placed @ : "+EnvironmentDetails.strFQNFileNameWrite);          

				//Create directory to save screen shot 
				new File(EnvironmentDetails.Path_ScreenShot).mkdirs();

			
		 }
	 
		 @Test(dataProvider = "objectTestData", description = "Apptus_Tetmination_Flow")
		 public void fn_E2E_Apptus_E2E_Flow(Map<String, Object> mapTcData) throws Throwable 
		 {
			 
			 Termination objTermination= new Termination();
			 Boolean blnTestExecutionStatus = false;
		
			
			 String strAccountName = mapTcData.get("AccountName").toString();
			 String strApttusQueryName = mapTcData.get("ApttusQueryName").toString();
			 String strOppDetails = mapTcData.get("OppDetails").toString();
			 String strOffetingToTerminate = mapTcData.get("NoOffetingToTerminate").toString();
		
			 

			 try { 
				 
				 
				 
				 /********************************************************************************************************************************************************************************************/
				 /*	//TEST DATA TO BE COMMENTED ALWAYS !
				 	Thread.sleep(2000);
					driver.navigate().to("https://sungardas--ebdeva.cs21.my.salesforce.com/001q000000fFB37");
					Map<String,String> mapOfferingNames = new HashMap<String,String>();
					mapOfferingNames.put("GLB - Managed Cloud Recovery", "TestNewOrder1_20170214_0855288-Dublin 1 - Parkwest-OID-1259-V1");
					mapOfferingNames.put("GLB - Backup & Vaulting", "TestNewOrder1_20170214_0855288-Dublin 2 - Clonshaugh-OID-1258-V1");
					EnvironmentDetails.mapGlobalVariable.put("mapOfferingNames", mapOfferingNames);
					EnvironmentDetails.mapGlobalVariable.put("strOpportunityName", "Upg/Dwg - MultipleOfferings");
					EnvironmentDetails.mapGlobalVariable.put("strNewproposalName", "Q-00000702");
					EnvironmentDetails.mapGlobalVariable.put("strMSA_AgrmntNumber", "00006991.0");
					EnvironmentDetails.mapGlobalVariable.put("strMSA_AgrmntName", "TestNewOrder1_20170215_134740815_GMSA");
					EnvironmentDetails.mapGlobalVariable.put("strAgrmntName", "TestNewOrder1_20170220_150715143_Order Form");
					EnvironmentDetails.mapGlobalVariable.put("strAgrmntNum", "00000850.0");
					EnvironmentDetails.mapGlobalVariable.put("arrstrOfferingSel", arrstrOfferingSel);
					Thread.sleep(2000);*/
				 
				 /********************************************************************************************************************************************************************************************/ 
				
				 
				
						
						if (strAccountName.contains("US")) {
							ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("USAUserName"), EnvironmentDetails.mapEnvironmentDetails.get("USAPassWord"));
						}
						else if (strAccountName.contains("CAN")) {
							ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("CANUserName"), EnvironmentDetails.mapEnvironmentDetails.get("CANPassWord"));
						}
						else if (strAccountName.contains("UK")) {
							ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("UKUserName"), EnvironmentDetails.mapEnvironmentDetails.get("UKPassWord"));
						}
						else if (strAccountName.contains("IRE")) {
							ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("IREUserName"), EnvironmentDetails.mapEnvironmentDetails.get("IREPassWord"));
						}
						else if (strAccountName.contains("LUX")) {
							ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("LUXUserName"), EnvironmentDetails.mapEnvironmentDetails.get("LUXPassWord"));
						}
				 
				 
					//Create Opportunity from offering management 
						Boolean blnCreateOpp = objTermination.fn_CreateOpportunity(driver, webWait, strAccountName, strApttusQueryName, strOffetingToTerminate);
						if (blnCreateOpp==true){
							 Log.info("Opportunity has created successfully.");}
						else{
							 Log.error("ERROR: Unable to Create Opportunity.");
							 assertTrue(false, "ERROR: Unable to Opportunity.");
						}
						
					//Perform Early Termination and Auto Renew Off by legal Sales User. 
						Boolean blnEarlyTermination = objTermination.fn_RemoveAutoRenew(driver, webWait, strAccountName, strApttusQueryName);
						if (blnEarlyTermination==true){
							 Log.info("Perform Early Termination successfully.");}
						else{
							 Log.error("ERROR: Unable to Perform Early Termination.");
							 assertTrue(false, "ERROR: Unable to Perform Early Termination.");
						}
						
					//Accept Early Termination Logs. 
						Boolean blnUPDTerminationLogs = objTermination.fn_UPDTerminationLogs(driver, webWait, strAccountName);
						if (blnUPDTerminationLogs==true){
							 Log.info("Early Termination Logs Accepted successfully.");}
						else{
							 Log.error("ERROR: Unable to Accept Early Termination Logs.");
							 assertTrue(false, "ERROR: Unable to Accept Early Termination Logs.");
						}
						
					//To Terminate Opportunity. 
						Boolean blnTerminateOpp = objTermination.fn_TerminateOpp(driver, webWait, strAccountName);
						if (blnTerminateOpp==true){
							 Log.info("Opportunity Terminated successfully.");}
						else{
							 Log.error("ERROR: Unable to Terminate Opportunity.");
							 assertTrue(false, "ERROR: Unable to Terminate Opportunity.");
						}
						
					//Update Opportunity with lost Stage. 
						Boolean blnLostOpp = objTermination.fn_LostOpp(driver, webWait, strAccountName, strOppDetails);
						if (blnLostOpp==true){
							 Log.info("Updated Opportunity with lost Stage successfully.");}
						else{
							 Log.error("ERROR: Unable to Update Opportunity.");
							 assertTrue(false, "ERROR: Unable to Update Opportunity");
						}
						
					//Verify Asset line items status
						Boolean blnVerifyLineItems = objTermination.fn_VerifyLineItems(driver, webWait, strAccountName, strApttusQueryName);
						if (blnVerifyLineItems==true){
							 Log.info("Verified Asset line items status successfully.");}
						else{
							 Log.error("ERROR: Unable to Verify Asset line items status.");
							 assertTrue(false, "ERROR: Unable to Verify Asset line items status");
						}
					 
					blnTestExecutionStatus = true; 
					Log.endTestCase(EnvironmentDetails.strTestCaseName);
					
				
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//Log.fn_LogExceptionInReport(driver, EnvironmentDetails.strTestCaseName);
				//Log.endTestCaseFailure(EnvironmentDetails.strTestCaseName);
				Log.error("ERROR: problem while Creating the Apptus-E2E flow");
				e.printStackTrace();
			}
			 
			 
			 if (blnTestExecutionStatus = false) {
				 assertTrue(false, "ERROR: Test Case Fail");
			}
			  
			 
		 }
		 
		

		//Reading data for the test       
		@DataProvider(name = "objectTestData")
		public Object[][] testExcelData() throws Throwable
		{
		    Utils objUtils=new Utils();
		    return objUtils.fn_DataProviderTC("TestCaseDetails", "Apptus_Tetmination_Flow");
		}
	 
		@AfterTest
		 public void afterMethod() throws Exception
		{			 
		 //TODO: Logs out of the application & quit the driver		
		 //ApptusLoginLogout.appLogout(driver, webWait);
		// driver.quit(); 
		 }

}
