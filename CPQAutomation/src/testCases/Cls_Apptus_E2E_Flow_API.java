/*
 Author     		:	Suhas Thakre
 Class Name			: 	Cls_Apptus_E2E_Flow 
 Purpose     		: 	Purpose of this file is :
						1. Create New Order 

 Date       		:	21/02/2017 
 Modified Date		:		
 Modified By		: 	
 Version Information:	Version 1.0
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/

package testCases;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.sun.jmx.snmp.Timestamp;

import appModules.Add_Multiple_Products;
import appModules.AfterApprovalProcess;
//import appModules.Add_Configurations;
import appModules.ApprovalValidation;
import appModules.ChangeOrder;
import appModules.CreateAmendRenew;
import appModules.CreateLeadandConvertToMultipleAcctNA;
import appModules.MSAGeneration_And_Activation;
import appModules.ApptusLoginLogout;
import appModules.CreateOfferingData;
import appModules.CreateProposal;
import appModules.MSOutlookApproval;
import appModules.OrderFlow_Generation_And_Activation;
import appModules.PricingValidation;
import appModules.Termination;
import utility.EnvironmentDetails;
import utility.ExcelHandler;
import utility.Log;
import utility.Utils;
import utility.VerificationMethods;


//E2E flow for New Order Creation
public class Cls_Apptus_E2E_Flow_API extends VerificationMethods{
	
	
		public WebDriver driver;
		public WebDriverWait webWait;
		public EnvironmentDetails objEnv;
	
		 @BeforeClass
		 public void beforeMethod()throws Exception
		 {	
			// Initialize test case variable 
				EnvironmentDetails.strTestCaseName = "Apptus_E2E_Flow";
				 
				//TODO: Initialize the Logger.
		    	DOMConfigurator.configure("log4j.xml");
		    	Log.fn_InitializeExtentLogger(EnvironmentDetails.strTestCaseName);
		    	
		    	
		    	
		    	//TODO: Set Chrome driver as the driver to launch the test.
		    	driver = utility.Utils.fn_SetChromeDriverProperties();
		    	    	
				//TODO: initialize the webWait
				webWait = new WebDriverWait(driver,60);

				objEnv = new EnvironmentDetails();
				//Save EnvironmentDetails object to global variable map 
				EnvironmentDetails.mapGlobalVariable.put("objEnv", objEnv);
				driver.get(EnvironmentDetails.mapEnvironmentDetails.get("InstanceURL"));
				
				//Logs in to the Apptus application with the appropriate credentials	
				//ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("InstUserName"), EnvironmentDetails.mapEnvironmentDetails.get("InstPassWord"));
				
				
	            //Name of output data file.
				Log.debug("DEBUG: Output file to be used for this execution is placed @ : "+EnvironmentDetails.strFQNFileNameWrite);          

				//Create directory to save screen shot 
				new File(EnvironmentDetails.Path_ScreenShot).mkdirs();

			
		 }
	 
		 @Test(dataProvider = "objectTestData", description = "Apptus_E2E_Flow")
		 public void fn_E2E_Apptus_E2E_Flow(Map<String, Object> mapTcData) throws Throwable 
		 {
			
			 Boolean blnTestExecutionStatus = false;
			 String struniquekey = "";
			 String strCloseOppDetails = mapTcData.get("CloseOppDetails").toString();
			 String strProposalData = mapTcData.get("ProposalData").toString();
			 String strPricingSheet_Name = mapTcData.get("PricingSheet_Name").toString();
			 String strApttusQueryName = mapTcData.get("ApttusQueryName").toString();
			 String strLocCodeSOQL = mapTcData.get("LocCodeSOQL").toString();
			 String strProductInfoSheetName = mapTcData.get("ProductInfoSheet_Name").toString();
			 String strProductInfoWBName =  mapTcData.get("ProductInfoWB_Name").toString();
			 String strCreateOfferingDataSheetName = mapTcData.get("CreateOfferingDataSheet_Name").toString();
			 String strCreateOfferingWBName = mapTcData.get("CreateOfferingWB_Name").toString();
			 String strMulAcctCreationSheet = mapTcData.get("MulAcctCreationSheet_Name").toString();
			 String strMulAcctCreationWB = mapTcData.get("MulAcctCreationWB").toString();
			 String strLeadCreationWB = mapTcData.get("LeadCreationWB").toString();
			 String strLeadToAccountSheet = mapTcData.get("LeadToAccountSheet").toString();
			 String strActivationDate = mapTcData.get("Activation_Date").toString();
			 //String strApprovedFrom = mapTcData.get("ApprovedFrom").toString();
			 Map<String, Object> mapMultipleAcct = new HashMap<String, Object>() ;
			 mapMultipleAcct.put("PathOfFile", mapTcData.get("Path_of_File").toString());
			 mapMultipleAcct.put("strMulAcctCreationSheet", strMulAcctCreationSheet);
			 mapMultipleAcct.put("strMulAcctCreationWB", strMulAcctCreationWB);
			 mapMultipleAcct.put("strLeadToAccountSheet", strLeadToAccountSheet);
			 mapMultipleAcct.put("strLeadCreationWB", strLeadCreationWB);
			 
			 Map<String, Object> mapMultipleProdut = new HashMap<String, Object>() ;
			 mapMultipleProdut.put("strProductInfoSheetName", strProductInfoSheetName);
			 mapMultipleProdut.put("strProductInfoWBName", strProductInfoWBName);
			
			 CreateProposal objProposal = new  CreateProposal();
			 ChangeOrder objChangeOrder = new ChangeOrder();
			 Termination objTermination= new Termination();				
			 ExcelHandler objExcel = new ExcelHandler();
			 CreateOfferingData objOfferingData = new CreateOfferingData();
			 PricingValidation  objPriceValidation = new  PricingValidation();
			 ApprovalValidation objApprovalValidation = new ApprovalValidation();
			 MSAGeneration_And_Activation objMSA = new MSAGeneration_And_Activation();
			 OrderFlow_Generation_And_Activation objOrderFlow = new OrderFlow_Generation_And_Activation();
			 MSOutlookApproval objOutlook = new MSOutlookApproval();
			 AfterApprovalProcess   objAfterApproval = new AfterApprovalProcess();
			 CreateOfferingData objCreateOfferingData= new CreateOfferingData();
			 CreateAmendRenew createUpgDwg = new CreateAmendRenew();
			 //CreateLeadandConvertToMultipleAcct objCreateAcct= new CreateLeadandConvertToMultipleAcct();
			 Add_Multiple_Products objAddprdcts= new Add_Multiple_Products();
			 CreateLeadandConvertToMultipleAcctNA objCreateLeadandConvertToMultipleAcctUS = new CreateLeadandConvertToMultipleAcctNA();
			 //CreateLeadandConvertToMultipleAcctIRL objCreateLeadandConvertToMultipleAcctIRL = new CreateLeadandConvertToMultipleAcctIRL();
			 
			
			 Log.startTestCase(EnvironmentDetails.strTestCaseName);
		
			 try { 
				 
				
				 /********************************************************************************************************************************************************************************************/
				 /*	//TEST DATA TO BE COMMENTED ALWAYS !
				 	Thread.sleep(2000);
					driver.navigate().to("https://sungardas--ebintb.cs44.my.salesforce.com/a4S7A000000A8IU");
					EnvironmentDetails.mapGlobalVariable.put("strAcctAddrID", "LAC-0051022");
					Map<String,String> mapOfferingNames = new HashMap<String,String>();
					//mapOfferingNames.put("GLB - Managed Cloud Recovery", "TestNewOrder1_20170214_0855288-Dublin 1 - Parkwest-OID-1259-V1");
					//mapOfferingNames.put("GLB - Backup & Vaulting", "TestNewOrder1_20170214_0855288-Dublin 2 - Clonshaugh-OID-1258-V1");
					//EnvironmentDetails.mapGlobalVariable.put("mapOfferingNames", mapOfferingNames);
					EnvironmentDetails.mapGlobalVariable.put("strCountry", "Ireland");
					EnvironmentDetails.mapGlobalVariable.put("strAcctName", "IRE0509_1_3118");
					EnvironmentDetails.mapGlobalVariable.put("strNewproposalName", "Q-00001572");
					EnvironmentDetails.mapGlobalVariable.put("struniquekey", "US1104_2");
					EnvironmentDetails.mapGlobalVariable.put("strStartDate", "4/14/2017");
					
					EnvironmentDetails.mapGlobalVariable.put("strMSA_AgrmntNumber", "00006991.0");
					EnvironmentDetails.mapGlobalVariable.put("strMSA_AgrmntName", "API5_3411_GMSA");
					EnvironmentDetails.mapGlobalVariable.put("strAgrmntName", "US20522_1_5947_Order Form");
					EnvironmentDetails.mapGlobalVariable.put("strAgrmntNum", "00002058.0");
					EnvironmentDetails.mapGlobalVariable.put("strOrderUniqueKey", "4f6ad410");
					//objPriceValidation.fn_PricingValidationIRE(driver, webWait);
				 	//objOutlook.fn_WorkbenchUpdate(driver, webWait, "", "");
					//objChangeOrder.fn_ChangeOrder(driver, webWait);
					Thread.sleep(2000);
					
					*/
					
					
				
				 /********************************************************************************************************************************************************************************************/ 
			
					
				 String excelResource = EnvironmentDetails.Path_ExternalFiles+ System.getProperty("file.separator") + strMulAcctCreationWB;
					File fileProductInfo = new File(excelResource);
					List<Map<String, String>> lstAcctCreationDataMap = objExcel.fn_ReadExcelAsMap(fileProductInfo,strMulAcctCreationSheet); 
					
					//TODO: Create account of each data row in MulAcctCreationSheet
					for (int i = 0; i < lstAcctCreationDataMap.size(); i++) {
						
						struniquekey = lstAcctCreationDataMap.get(i).get("Company_Name").toString().trim();
						EnvironmentDetails.mapGlobalVariable.put("struniquekey", struniquekey);
						
						if (struniquekey.contains("US")) {
							ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("USAUserName"), EnvironmentDetails.mapEnvironmentDetails.get("USAPassWord"));
						}
						else if (struniquekey.contains("CAN")) {
							ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("CANUserName"), EnvironmentDetails.mapEnvironmentDetails.get("CANPassWord"));
						}
						else if (struniquekey.contains("UK")) {
							ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("UKUserName"), EnvironmentDetails.mapEnvironmentDetails.get("UKPassWord"));
						}
						else if (struniquekey.contains("IRE")) {
							ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("IREUserName"), EnvironmentDetails.mapEnvironmentDetails.get("IREPassWord"));
						}
						else if (struniquekey.contains("LUX")) {
							ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("LUXUserName"), EnvironmentDetails.mapEnvironmentDetails.get("LUXPassWord"));
						}
						
						
						
						//Create Lead and convert it to Opportunity
						Boolean blnCreateLead = objCreateLeadandConvertToMultipleAcctUS.fn_MultipleAccountCreation(driver, webWait, mapMultipleAcct);
						if (blnCreateLead==true){
							 Log.info("Lead has created successfully and convert to Account");}
						else{
							 Log.error("ERROR: Unable to Create Lead and convt to Account");
								 assertTrue(false, "ERROR: Unable to Create Lead and convt to Account");}	
						
						
						//Select Product family and set product details
						Boolean blnSelProductFamily = objOfferingData.fn_SelectProductFamily(driver, webWait, strApttusQueryName, strProductInfoSheetName
													, strCreateOfferingDataSheetName, strProductInfoWBName, strCreateOfferingWBName);
						if (blnSelProductFamily==true){
							 Log.info("Product family Selected and Data set successfully.");}
						else{
							 Log.error("ERROR: Unable to Select and Set Product family data.");
							 assertTrue(false, "ERROR: Unable to Select and Set Product family data.");} 
									
					    //Click on Create Proposal -Fill out Proposal details -Create it 
						Boolean blnCreateProposal  = objProposal.fn_CreateProposal(driver, webWait, strProposalData);
						if (blnCreateProposal==true){
							 Log.info("Proposal Data has been entered and created successfully");}
						else{
							 Log.error("ERROR: Unable to enter the data and create the Proposal");
							 assertTrue(false, "ERROR: Unable to enter data and create the Proposal");}	
						
						//Capture Offering Details 
						 Boolean blnCaptureOfferingData = objCreateOfferingData.fn_CaptureOfferingData(driver, webWait, strCreateOfferingDataSheetName, strLocCodeSOQL, strCreateOfferingWBName);
						if (blnCaptureOfferingData==true){
							 Log.info("Multiple Offering Created and capture successfully");}
						else{
							 Log.error("ERROR: Unable to Create Multiple Offering.");
							 assertTrue(false, "ERROR: Unable to Create Multiple Offering.");}	
						
						//MSAGeneration_And_Activation
						Boolean blnMSAGenerationAndActivation = objMSA.fn_MSAGenerateActivateAppl(driver, webWait, "API");
						if (blnMSAGenerationAndActivation==true){
							 Log.info("MSA Genaration and Activation Flow generation has been completed successfully");}
						else{
							 Log.error("ERROR: Unable to complete the MSA Genaration and Activation Flow");
							 assertTrue(false, "ERROR: Unable to complete the MSA Genaration and Activation Flow");}
						
						//Add first product Configuring the Services
						Boolean blnAddConfig1 = objAddprdcts.fn_Add_Multiple_Products(driver,webWait,mapMultipleProdut); ;
						if (blnAddConfig1==true){
							 Log.info("Configuration has been created successfully");}
						else{
							 Log.error("ERROR: Unable to add the configuration");
							 assertTrue(false, "ERROR: Unable to add the configuration");}
						
						
						//Add product to offering 
						Boolean blnOfferingManagement = createUpgDwg.fn_OfferingManagementMul(driver, webWait, strProductInfoSheetName, strProductInfoWBName);
						if (blnOfferingManagement==true){
							 Log.info("Product Added successfully to offering");}
						else{
							 Log.error("ERROR: Unable to Add product to offering");
							 assertTrue(false, "ERROR: Unable to Add product to offering");}
						
						
						//Pricing Validation for the Services
						Boolean blnValpricing  = objPriceValidation.fn_PricingValidation(driver, webWait,strPricingSheet_Name);
						if (blnValpricing==true){
							 Log.info("Pricing has been Validated successfully");}
						else{
							 Log.error("ERROR: Unable to validate the pricing of the services");
							 assertTrue(false, "ERROR: Unable to validate the pricing of the services");}	
						
						//Approval Validation for the Proposal created
						Boolean blnValapproval  = objApprovalValidation.fn_ApprovalValidation(driver, webWait);
						if (blnValapproval==true){
							 Log.info("Approvals Submission have been Validated successfully");}
						else{
							 Log.error("ERROR: Unable to validate the Submission of Approvals");
							 assertTrue(false, "ERROR: Unable to validate the Submission of Approvals");}
					
						//Approve Proposal request
						Boolean blnapproval = objOutlook.fn_ApprovalProcessAPI(driver, webWait);
						if (blnapproval==true){
								 Log.info("Approved proposal Successfully.");}
						 else{
								 Log.error("ERROR: Unable to Approved proposal.");
								 assertTrue(false, "ERROR: Unable to Approved proposal.");}  
						
						
						
						//OrderFlowGeneration and Activation
						 Boolean blnOrderFlwGenerationAndActivation = objOrderFlow.fn_OrderFlowGenerateActivateAppl(driver, webWait, "API");
						 if (blnOrderFlwGenerationAndActivation==true){
								 Log.info("Order Flow generation has been completed successfully");}
						 else{
								 Log.error("ERROR: Unable to complete the Order Generation flow");
								 assertTrue(false, "ERROR: Unable to complete the Order Generation flow");}	
						
						 
						//After Approval Process - Review ESignature
				         Boolean blnAfterApprovalprocess = objAfterApproval.fn_OrderFormActivation(driver, webWait, strCloseOppDetails, strActivationDate);
						 if (blnAfterApprovalprocess==true){
								 Log.info("After Approval process of Order Activation has been completed successfully");}
						 else{
								 Log.error("Unable to complet the After Approval process of Order Activation");
								 assertTrue(false, "ERROR: Unable to complete with the After Approval process of Order Activation");}
						 
						  
						/*//Perform Auto Renew Off by legal Sales User. 
							//Step 1: Logout from existing User
							ApptusLoginLogout.appLogout(driver, webWait);
							//Step 2: Login With legal Sales User.
							ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("LegalSalesUserName"), EnvironmentDetails.mapEnvironmentDetails.get("LegalSalesPassWord"));
							//Step 3: Perform Auto Renew Off.
							Boolean blnEarlyTermination = objTermination.fn_RenewOffAndTermination(driver, webWait, EnvironmentDetails.mapGlobalVariable.get("strAcctName").toString(), strApttusQueryName, "Auto Renew Off");
							if (blnEarlyTermination==true){
								 Log.info("Perform Auto Renew Off successfully.");}
							else{
								 Log.error("ERROR: Unable to Perform Auto Renew Off.");
								 assertTrue(false, "ERROR: Unable to Perform Auto Renew Off.");
							}
						 */
						 //ApptusLoginLogout.appLogout(driver, webWait);
						
					}
				 
					
					 
				/*	//After Approval Process - Activate Order
					 Boolean blnAfterApprovalprocess = objAfterApproval.fn_AfterApprovalProcess(driver, webWait, strCloseOppDetails, strActivationDate);
					 if (blnAfterApprovalprocess==true){
							 Log.info("After Approval process of Order Activation has been completed successfully");}
					 else{
							 Log.error("Unable to complet the After Approval process of Order Activation");
							 assertTrue(false, "ERROR: Unable to complete with the After Approval process of Order Activation");}	
					*/
					blnTestExecutionStatus = true;
					Log.endTestCase(EnvironmentDetails.strTestCaseName);

			} catch (Exception e) {
				// TODO Auto-generated catch block
				//Log.fn_LogExceptionInReport(driver, EnvironmentDetails.strTestCaseName);
				//Log.endTestCaseFailure(EnvironmentDetails.strTestCaseName);
				Log.error("ERROR: problem while Creating the Apptus-E2E flow");
				e.printStackTrace();
			}
			 
			 if (blnTestExecutionStatus == false) {
				 assertTrue(false, "ERROR: Test Case Fail");
			}
			 
		 }
		 
		

		//Reading data for the test       
		@DataProvider(name = "objectTestData")
		public Object[][] testExcelData() throws Throwable
		{
		    Utils objUtils=new Utils();
		    return objUtils.fn_DataProviderTC("TestCaseDetails", "Apptus_E2E_Flow");
		}
	 
		@AfterTest
		 public void afterMethod() throws Exception
		{			 
		 //TODO: Logs out of the application & quit the driver		
		// ApptusLoginLogout.appLogout(driver, webWait);
		// driver.quit(); 
		 }

}
