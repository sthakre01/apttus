package testCases;
import java.awt.Robot;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pageObjects.SalesForce.OrderFlow;
import appModules.Add_Configurations;
import appModules.Add_Multiple_Products;
import appModules.AfterApprovalProcess;
import appModules.ApprovalValidation;
import appModules.ChangeOrder;
import appModules.GmailApproval;
import appModules.MSOutlookApproval;
import appModules.MSOutlookLiteApproval;
import appModules.CreateAmendRenew;
import appModules.ApptusLoginLogout;
import appModules.CreateOfferingData;
import appModules.OrderFlow_Generation_And_Activation;
import appModules.PricingValidation;
import appModules.Termination;
import utility.EnvironmentDetails;
import utility.EnvironmentDetailsSAS;
import utility.ExcelHandler;
import utility.Log;
import utility.Utils;
import utility.VerificationMethods;
import utility.WriteExcel;


//E2E flow for Amendment Order Creation
public class Cls_Apttus_Activate_AWS_Order extends VerificationMethods{
	
	
		public WebDriver driver;
		public WebDriverWait webWait;
		
	    public EnvironmentDetails objEnv;
	   
	
		 @BeforeClass
		 public void beforeMethod()throws Exception
		 {		
			 
			// Initialize test case variable 
			EnvironmentDetails.strTestCaseName = "Apptus_Amendment_Flow_Existing_Acct";
			 
			//TODO: Initialize the Logger.
	    	DOMConfigurator.configure("log4j.xml");
	    	Log.fn_InitializeExtentLogger(EnvironmentDetails.strTestCaseName);
	    	
	    	
	    	
	    	//TODO: Set Chrome driver as the driver to launch the test.
	    	driver = utility.Utils.fn_SetChromeDriverProperties();
	    	    	
			//TODO: initialize the webWait
			webWait = new WebDriverWait(driver,60);

			objEnv = new EnvironmentDetails();
			//Save EnvironmentDetails object to global variable map 
			EnvironmentDetails.mapGlobalVariable.put("objEnv", objEnv);
			driver.get(EnvironmentDetails.mapEnvironmentDetails.get("InstanceURL"));
			
			//Logs in to the Apptus application with the appropriate credentials	
			//ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("IREUserName"), EnvironmentDetails.mapEnvironmentDetails.get("IREPassWord"));
			
			
            //Name of output data file.
			Log.debug("DEBUG: Output file to be used for this execution is placed @ : "+EnvironmentDetails.strFQNFileNameWrite);          

			//Create directory to save screen shot 
			new File(EnvironmentDetails.Path_ScreenShot).mkdirs();
			
		 }
	 
		 @Test(dataProvider = "objectTestData", description = "Apptus_Amendment_Flow_Existing_Acct")
		 public void fn_Apttus_Activate_AWS_Order(Map<String, Object> mapTcData) throws Throwable 
		 {
			 AfterApprovalProcess   objAfterApproval = new AfterApprovalProcess();
			
			 //Test Case Data 
			 Map<String, Object> mapMultipleProdut = new HashMap<String, Object>() ;
			 mapMultipleProdut.put("strProductInfoSheetName", "Product_Information");
			 mapMultipleProdut.put("strProductInfoWBName", "TestData.xlsx");
			 String strChangeOrderWB = "TestData.xlsx";
			 String strChangeOrderSheet = "Amend_Renew";
			 String struniquekey = "";
			String strCountry = "";
			 List<Map<String, String>> lstmapChangeOrderData = new ArrayList<Map<String,String>>();
			 ExcelHandler objExcel = new ExcelHandler();
			 
			 
			 //Log Start of test case
			 Log.startTestCase(EnvironmentDetails.strTestCaseName);
		
			 try { 
				 
				 
				 //EnvironmentDetails objEnv1 = (EnvironmentDetails) EnvironmentDetails.mapGlobalVariable.get("objEnv");
				 
				 
				 
				 /********************************************************************************************************************************************************************************************/
				 	//TEST DATA TO BE COMMENTED ALWAYS !
				 				
			 			/*		 Thread.sleep(2000);
				 				 driver.navigate().to("https://sungardas--ebintb.cs44.my.salesforce.com/a4S7A000000A8Vx?srPos=0&srKp=a4S");
								 Thread.sleep(2000);
				 				 ArrayList<String> arrstrOfferingNameSel = new ArrayList<>();
								 arrstrOfferingNameSel.add("IRE0608_1_4304_CR (0CcRE)-Amazon Web Services-OID-1619-V1");
								 EnvironmentDetails.mapGlobalVariable.put("arrstrOfferingNameSel", arrstrOfferingNameSel);
								 System.out.println("The type of the object after retrieval is: "+EnvironmentDetails.mapGlobalVariable.get("arrstrOfferingNameSel").getClass().getSimpleName());
								 ArrayList<String> arrOfferingNameSel = new ArrayList<>();
								 Object objOfferingNameSel = EnvironmentDetails.mapGlobalVariable.get("arrstrOfferingNameSel");
								 EnvironmentDetails.mapGlobalVariable.put("strNewproposalName", "Q-00001756");
								 EnvironmentDetails.mapGlobalVariable.put("strProposalType", "Renewal");
								 */
								
				 
				 
				 
				 /********************************************************************************************************************************************************************************************/ 

			
				//Step : Read Change Order data.
				String excelResource = EnvironmentDetails.Path_ExternalFiles+ System.getProperty("file.separator") + strChangeOrderWB;
				File fileLeadCreationData = new File(excelResource);
				lstmapChangeOrderData = objExcel.fn_ReadExcelAsMap(fileLeadCreationData,strChangeOrderSheet); 
				//TODO: Amend the each account specified in data sheet
				for (int i = 0; i < lstmapChangeOrderData.size(); i++) {
					
					struniquekey = lstmapChangeOrderData.get(i).get("Uniquekey").toString().trim();
					EnvironmentDetails.mapGlobalVariable.put("struniquekey", struniquekey);
					EnvironmentDetails.mapGlobalVariable.put("strAcctName", struniquekey);
					strCountry = lstmapChangeOrderData.get(i).get("Country").toString().trim();
					EnvironmentDetails.mapGlobalVariable.put("strCountry", strCountry);
					
					if (struniquekey.contains("US")) {
						ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("USAUserName"), EnvironmentDetails.mapEnvironmentDetails.get("USAPassWord"));
					}
					else if (struniquekey.contains("CAN")) {
						ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("CANUserName"), EnvironmentDetails.mapEnvironmentDetails.get("CANPassWord"));
					}
					else if (struniquekey.contains("UK")) {
						ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("UKUserName"), EnvironmentDetails.mapEnvironmentDetails.get("UKPassWord"));
					}
					else if (struniquekey.contains("IRE")) {
						ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("IREUserName"), EnvironmentDetails.mapEnvironmentDetails.get("IREPassWord"));
					}
					else if (struniquekey.contains("LUX")) {
						ApptusLoginLogout.appLogin(driver, webWait, EnvironmentDetails.mapEnvironmentDetails.get("LUXUserName"), EnvironmentDetails.mapEnvironmentDetails.get("LUXPassWord"));
					}
					
				
					//After Approval Process - Review ESignature
			         Boolean blnActivateOrderAWS = objAfterApproval.fn_ActivateOrderAWS(driver, webWait);
					 if (blnActivateOrderAWS==true){
							 Log.info("Order Activation has been completed successfully");}
					 else{
							 Log.error("Unable to Activate AWS Order.");
							 assertTrue(false, "ERROR: Unable to Unable to Activate AWS Order.");}
					 	
				}   
				 Log.endTestCase(EnvironmentDetails.strTestCaseName);
				 
				 
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//Log.fn_LogExceptionInReport(driver, EnvironmentDetails.strTestCaseName);
				Log.fn_LogExceptionInReport(driver, EnvironmentDetails.strTestCaseName);
				Log.error("ERROR: problem while Apptus_Amendment_Flow");
				e.printStackTrace();
			}
		 }
		 
		//Reading data for the test    
		@DataProvider(name = "objectTestData")
		public Object[][] testExcelData() throws Throwable
		{
		    Utils objUtils=new Utils();
		    return objUtils.fn_DataProviderTC("TestCaseDetails", "Apptus_Amendment_Flow_Existing_Acct");
		}
	 
		/*@AfterTest
		 public void afterMethod() throws Exception
		{			 
		 //TODO: Logs out of the application & quit the driver		
		// ApptusLoginLogout.appLogout(driver, webWait);
		 driver.quit(); 
		 }*/

}
