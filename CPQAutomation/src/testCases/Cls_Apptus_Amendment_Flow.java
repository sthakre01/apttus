package testCases;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pageObjects.SalesForce.Account;
import pageObjects.SalesForce.Configuration;
import appModules.Add_Configurations;
import appModules.AfterApprovalProcess;
import appModules.ApprovalValidation;
import appModules.GmailApproval;
import appModules.CreateAmendRenew;
import appModules.ApptusLoginLogout;
import appModules.CreateOfferingData;
import appModules.PricingValidation;
import utility.API_SOAP;
import utility.EnvironmentDetails;
import utility.Log;
import utility.Utils;
import utility.VerificationMethods;


//E2E flow for Amendment Order Creation
public class Cls_Apptus_Amendment_Flow extends VerificationMethods{
	
	
		public WebDriver driver;
		public WebDriverWait webWait;
	    public EnvironmentDetails objEnv;
	
		 @BeforeClass
		 public void beforeMethod()throws Exception
		 {			 
			 
			// Initialize test case variable 
			EnvironmentDetails.strTestCaseName = "Apptus_Amendment_Flow";
				
			//TODO: Initialize the Logger.
	    	DOMConfigurator.configure("log4j.xml");
	    	Log.fn_InitializeExtentLogger(EnvironmentDetails.strTestCaseName);
	    	
	    	Log.startTestCase("Login to application");
	    	
	    	//TODO: Set Chrome driver as the driver to launch the test.
	    	driver = utility.Utils.fn_SetChromeDriverProperties();
	    	    	
			//TODO: initialize the webWait
			webWait = new WebDriverWait(driver,60);

			objEnv = new EnvironmentDetails();	
			driver.get(objEnv.mapEnvironmentDetails.get("InstanceURL"));
			
			System.out.println("Login to application");
			Log.startTestCase("Login to application");
			//Logs in to the Apptus application with the appropriate credentials	
			ApptusLoginLogout.appLogin(driver, webWait, objEnv.mapEnvironmentDetails.get("InstUserName"), objEnv.mapEnvironmentDetails.get("InstPassWord"));

			
		 }
	 
		 @Test(dataProvider = "objectTestData", description = "Apptus_Amendment_Flow")
		 public void fn_Apptus_Amendment_Flow(Map<String, Object> mapTcData) throws Throwable 
		 {
		
				
			 CreateAmendRenew createUpgDwg = new CreateAmendRenew();
			 Add_Configurations objConfiguration = new  Add_Configurations();
			 PricingValidation  objPriceValidation = new  PricingValidation();
			 ApprovalValidation objApprovalValidation = new ApprovalValidation();
			 GmailApproval objAtmtGmail = new GmailApproval();
			 AfterApprovalProcess   objAfterApproval = new AfterApprovalProcess();
			 CreateOfferingData objCreateOfferingData= new CreateOfferingData();
			 
			 //Test Case Data 
			 String strAccountName = mapTcData.get("AccountName").toString();
			 String strApttusQueryName = mapTcData.get("ApttusQueryName").toString();
			 Integer strNoOffetingToSelect =   Integer.parseInt(mapTcData.get("NoOffetingToSelect").toString());
			 String strOfferingDataSheet_Name = mapTcData.get("OfferingDataSheet_Name").toString();
			 String strProd1ConfigData = mapTcData.get("Prod1ConfigData").toString();
			 String strProd2ConfigData = mapTcData.get("Prod2ConfigData").toString();
			 String strRepriceProductData = mapTcData.get("RepriceProductData").toString();
			 Integer intProductAssignNewOfferingCount = Integer.parseInt(mapTcData.get("ProductAssignNewOfferingCount").toString());
			 Integer intNoProductToRemove = Integer.parseInt(mapTcData.get("NoProductToRemove").toString());
			 Integer intNewOfferingCount = Integer.parseInt(mapTcData.get("NewOfferingCount").toString());
			 String strEmailDetails = mapTcData.get("EmailDetails").toString();
			 String strPricingSheet_Name = mapTcData.get("PricingSheet_Name").toString();
			 String strActivationDate = mapTcData.get("Activation_Date").toString();
			 
			 
			 
			
			 
			 
			 List<String> arrstrOfferingNameSel = new ArrayList<>();
			 arrstrOfferingNameSel.add("Dublin 1 - Parkwest-OID-1135-V3");
			 arrstrOfferingNameSel.add("TestAutomationAcct2016-12-19 1-Dublin 2 - Clonshaugh-OID-1073-V6");
			 EnvironmentDetails.mapGlobalVariable.put("arrstrOfferingNameSel", arrstrOfferingNameSel);
			 EnvironmentDetails.mapGlobalVariable.put("strProposalType", "Amendment");
			 EnvironmentDetails.mapGlobalVariable.put("strNewOfferingDataName", "TestAutomation_New_Offering_20170113_151716405");
			 
			 
			 Object  objOfferingNameSel =   EnvironmentDetails.mapGlobalVariable.get("arrstrOfferingNameSel");
			 if (objOfferingNameSel instanceof ArrayList) {
				 @SuppressWarnings("unchecked")
				List<String> arrOfferingNameSel = (ArrayList<String>)  objOfferingNameSel;
				 arrOfferingNameSel.add("asf");
			}
			 
			 
			 Log.startTestCase(EnvironmentDetails.strTestCaseName);
		
			 try { 
				 
				 
				    new Account();
				    new Configuration();
				  
				/*objacctpage.fn_setSearchField(driver, webWait).sendKeys("Q-00009737"); 
				objacctpage.fn_clickSearchBtn(driver, webWait).click();
				Thread.sleep(1000);
				driver.findElement(By.partialLinkText("Q-00009737")).click();
				Thread.sleep(3500);*/
				
				
				
				
				//Create proposal from Offering data
				Boolean blnCreateUpgDwg = createUpgDwg.fn_CreateAmendRenewProposal(driver, webWait, strApttusQueryName);
				if (blnCreateUpgDwg==true){
					 Log.info("Proposal has created successfully from Offering data.");}
				else{
					 Log.error("ERROR: Unable to Create proposal from Offering data.");
					 assertTrue(false, "ERROR: Unable to Create proposal from Offering data.");
				}	
				
				//Create New Offering data
				Boolean blnCreateOfferingData = objCreateOfferingData.fn_CreateOfferingData(driver, webWait, strOfferingDataSheet_Name);
				if (blnCreateOfferingData==true){
					 Log.info("New Offering data has created successfully");}
				else{
					 Log.error("ERROR: Unable to Create New Offering data");
					 assertTrue(false, "ERROR: Unable to Create New Offering data");}	
					
						
				//Add first product Configuring the Services
				Boolean blnAddConfig1 = objConfiguration.fn_AddConfigurations(driver, webWait, EnvironmentDetails.strTestCaseName,strProd1ConfigData);
					if (blnAddConfig1==true){
						 Log.info("Configuration has been created successfully");}
					else{
						 Log.error("ERROR: Unable to add the configuration");
						 assertTrue(false, "ERROR: Unable to add the configuration");}

							//Add Second product Configuring the Services
				Boolean blnAddConfig2 = objConfiguration.fn_AddConfigurations(driver, webWait, EnvironmentDetails.strTestCaseName,strProd2ConfigData);
					if (blnAddConfig2==true){
						 Log.info("Configuration has been created successfully");}
					else{
						 Log.error("ERROR: Unable to add the configuration");
						 assertTrue(false, "ERROR: Unable to add the configuration");}	
				
				
				//Add product to offering 
				Boolean blnOfferingManagement = createUpgDwg.fn_OfferingManagement(driver, webWait);
					if (blnOfferingManagement==true){
						 Log.info("Product Added successfully to offering");}
					else{
						 Log.error("ERROR: Unable to Add product to offering");
						 assertTrue(false, "ERROR: Unable to Add product to offering");}	
				
				    
				//RePrice product with discount				 
				Boolean blnRepriceProduct = createUpgDwg.fn_RepriceProduct(driver, webWait, strRepriceProductData);
					if (blnRepriceProduct==true){
						 Log.info("RePrice product successfully");}
					else{
						 Log.error("ERROR: Unable to RePrice product");
						 assertTrue(false, "ERROR: Unable to RePrice product");}	
						 
				
			/*	//Remove product from pricing screen 
				Boolean blnRemoveProduct = createUpgDwg.fn_RemoveProduct(driver, webWait, intNoProductToRemove);
					if (blnRemoveProduct==true){
						 Log.info("product Remove successfully");}
					else{
						 Log.error("ERROR: Unable to Remove product");
						 assertTrue(false, "ERROR: Unable to Remove product");}*/
				
				//Pricing Validation for the Services
				Boolean blnValpricing  = objPriceValidation.fn_PricingValidation(driver, webWait,strPricingSheet_Name);
				if (blnValpricing==true){
					 Log.info("Pricing has been Validated successfully");}
				else{
					 Log.error("ERROR: Unable to validate the pricing of the services");
					 assertTrue(false, "ERROR: Unable to validate the pricing of the services");}
				
				//Approval Validation for the Proposal created
				Boolean blnValapproval  = objApprovalValidation.fn_ApprovalValidation(driver, webWait);
				if (blnValapproval==true){
					 Log.info("Approvals Submission have been Validated successfully");}
				else{
					 Log.error("ERROR: Unable to validate the Submission of Approvals");
					 assertTrue(false, "ERROR: Unable to validate the Submission of Approvals");}
				
				
				 //AutomateGmailApprovalProcess			
		        Boolean blnMailapproval = objAtmtGmail.fn_AutomateGmailApprovalProcess(driver, webWait, strEmailDetails,"Approval");
				 if (blnMailapproval==true){
						 Log.info("Gmail Approval Process has been Validated successfully for Approvals");}
				 else{
						 Log.error("ERROR: Unable to Validate the Gmail Approval Process for Approvals");
						 assertTrue(false, "ERROR: Unable to Validate the Gmail Approval Process for Approvals");}
				
					
				//proposal Flow generation
				Boolean blnGenerateProposal = createUpgDwg.fn_GeneratePresentAcceptProposal(driver, webWait);
				if (blnGenerateProposal==true){
						Log.info("proposal Flow generation has been completed successfully");}
				else{
						Log.error("ERROR: Unable to complete proposal Flow generation ");
					   assertTrue(false, "ERROR: Unable to complete proposal Flow generation");}
				
				
				 //OrderFlowGeneration and Activation
		         Boolean blnOrderFlwGenerationAndActivation = createUpgDwg.fn_OrderFlow_Generate_Activate_Amend(driver, webWait, "suhas.thakre@sungardas.com");
				 if (blnOrderFlwGenerationAndActivation==true){
						 Log.info("Order Flow generation has been completed successfully");}
				 else{
						 Log.error("ERROR: Unable to complete the Order Generation flow");
						 assertTrue(false, "ERROR: Unable to complete the Order Generation flow");}	
				 
				 //AutomateGmailApprovalProcess	
				 Boolean blnAutomateGmailApprovalProcessOrder = objAtmtGmail.fn_AutomateGmailApprovalProcess(driver, webWait, strEmailDetails,"DocuSignProcessForOrder");
				 if (blnAutomateGmailApprovalProcessOrder==true){
						 Log.info("DocuSign process for Order has been completed successfully");}
				 else{
						 Log.error("ERROR: Unable to complete with Docusign process for Order");
						 assertTrue(false, "ERROR: Unable to complete with Docusign process for Order");}
				 
				 //After Approval Process - Review ESignature
		         Boolean blnAfterApprovalprocess = objAfterApproval.fn_OrderFormActivation(driver, webWait, "0.Closed Won#Competitive advantage#Amazon", strActivationDate);
				 if (blnAfterApprovalprocess==true){
						 Log.info("After Approval process of Order Activation has been completed successfully");}
				 else{
						 Log.error("Unable to complet the After Approval process of Order Activation");
						 assertTrue(false, "ERROR: Unable to complete with the After Approval process of Order Activation");}	
				 
				
				 Log.endTestCase(EnvironmentDetails.strTestCaseName);
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				//Log.fn_LogExceptionInReport(driver, EnvironmentDetails.strTestCaseName);
				Log.endTestCaseFailure(EnvironmentDetails.strTestCaseName);
				Log.error("ERROR: problem while Apptus_Amendment_Flow");
				e.printStackTrace();
			}
		 }
		 
		//Reading data for the test    
		@DataProvider(name = "objectTestData")
		public Object[][] testExcelData() throws Throwable
		{
		    Utils objUtils=new Utils();
		    return objUtils.fn_DataProviderTC("TestCaseDetails", "Apptus_Amendment_Flow");
		}
	 
		/*@AfterTest
		 public void afterMethod() throws Exception
		{			 
		 //TODO: Logs out of the application & quit the driver		
		// ApptusLoginLogout.appLogout(driver, webWait);
		 driver.quit(); 
		 }*/

}
