package pageObjects.Apttus;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import pageObjects.BasePageObject;

/**
 * Created by joe.gannon on 2/10/2016.
 */
public class ProposalFormPageObject extends BasePageObject
{
    public static By newPropBtn = By.xpath("//*[@id=\"hotlist\"]/table/tbody/tr/td[2]/input");
    public static By propNameField = By.id("00NP0000000h5bA");
    public static void fillNewProposalForm(WebDriver driver)
    {
        waitForElement(driver,newPropBtn);
        driver.findElement(newPropBtn).click();
        waitForElement(driver,propNameField);
        driver.findElement(propNameField).sendKeys("Joe's Prop");
    }

}
