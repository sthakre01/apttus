package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import pageObjects.BasePageObject;

/**
 * Created by joe.gannon on 2/10/2016.
 */
public class OpportunitesFormPageObject extends BasePageObject
{
	public static WebElement webElement = null;
    private static By oppNameField = By.id("opp3");
    private static By oppAcctName = By.id("opp4");
    private static By oppType = By.id("opp5");
    private static By oppExistingBusiness = By.xpath("//*[@id=\"opp5\"]/option[2]");
    public static void fillOpportunityForm(WebDriver driver)
    {
        driver.findElement(oppNameField).sendKeys("Joe");
        driver.findElement(oppAcctName).sendKeys("Joe's Acct");
        driver.findElement(oppType).click();
        driver.findElement(oppExistingBusiness).click();

    }
    public static boolean isLoaded(WebDriver driver)
    {
        WebElement ele = waitForElement(driver,oppNameField);
        if(ele.isDisplayed())
        {
            return true;
        }
        return false;
    }
    
    
  //To Select Value for lastname
  	public Select fn_selQuoteType(WebDriver driver, WebDriverWait webWait) throws InterruptedException
  	{
  		Select webList = new Select(driver.findElement(By.xpath(("//label[text()='Quote Type']/following::select[1]"))));
  		return webList;
  	}
  	
  //To Select Value for lastname
  	public Select fn_selLeadTactic(WebDriver driver, WebDriverWait webWait) throws InterruptedException
  	{
  		Select webList = new Select(driver.findElement(By.xpath(("//label[text()='Lead Tactic']/following::select[1]"))));
  		return webList;
  	}
  	
  //To Select Value for lastname
  	public Select fn_selOppType(WebDriver driver, WebDriverWait webWait) throws InterruptedException
  	{
  		Select webList = new Select(driver.findElement(By.xpath(("//label[text()='Opportunity Type']/following::select[1]"))));
  		return webList;
  	}
  	
  //Selecting the Competitors on Oppty page	
	public Select fn_selectMultipleOnCompetitors(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath("//select[@title='Competitors - Available']")));
		return webList;
	}
  	
	
	//to click on img add on Oppty edit page
	public WebElement fn_ClickImgAddOnAddressEditPage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@title='Add']")));
		return webElement;
	}
	
	//to click on img add on Oppty edit page
	public WebElement fn_setPriceBook(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Price Book']/following::span/input")));
		return webElement;
	}
	
	//to click on img add on Oppty edit page
	public WebElement fn_setCustRef(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Customer Reference']/following::span[1]/input")));
		return webElement;
	}
  	
}
