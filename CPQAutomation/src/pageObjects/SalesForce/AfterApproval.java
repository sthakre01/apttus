
/*
 Author     		:	Suhas Thakre
 Class Name			: 	Account 
 Purpose     		: 	Purpose of this file is :
						1. To identify Page object related to After Approval page for Apttus application.

 Date       		:	06/012/2017 
 Modified Date		:	06/012/2017	
 Modified By		: 	
 Version Information:	Version 1.0
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/
package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AfterApproval {
	

	public static WebElement webElement = null;
	
		//To identify the Submit with Attachments Buttons on Approval Page
		public WebElement fn_clickActivatedDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Activated Date' and @class='labelCol']//following::td[1]")));
			return webElement;
		}
		
		
		//To identify the Ready for Activation Date
		public WebElement fn_clickReadyForActivationDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Ready For Activation Date']//following::td[1]")));
			return webElement;
		}
		
		
		//To identify the Ready for Activation Date
		public WebElement fn_setReadyForActivationDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Ready For Activation Date']//following::input[1]")));
			return webElement;
		}
		
		//To identify the Submit with Attachments Buttons on Approval Page
		public WebElement fn_setActivatedDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Activated Date' and @class='labelCol']//following::td[1]/div/input]")));
			return webElement;
		}
		
        //click on save button
		public WebElement fn_clickSave(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h2[@class='mainTitle' and text()='Agreement Detail']//following::input[@name='inlineEditSave']")));
			return webElement;
		}
		
        //click on save button
		public WebElement fn_clickOrderDetailSave(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h2[text()='Order Detail']//following::input[1][@title='Save']")));
			return webElement;
		}
		
        //click on save button
		public WebElement fn_clickCalendarTodayLink(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/a[@class='calToday']")));
			return webElement;
		}
		
		//click on save button
		public WebElement fn_setActivationDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Activated Date']/following::input[1]")));
			return webElement;
		}

        //click on btn check e-signature status
		public WebElement fn_clickEsignatureStatus(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@alt='Check eSignature Status']")));
			return webElement;
		}
		
        //click on btn click Activate
		public WebElement fn_clickActivate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@alt='Activate']")));
			return webElement;
		}
		
        //click on Go To Opportunity
		public WebElement fn_clickGoToOpptyBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Go To Opportunity']")));
			return webElement;
		}
		
		//click on Edit btn on Oppty page
		public WebElement fn_clickEditBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h2[text()='Opportunity Detail']//following::input[@title='Edit'][1]")));
			return webElement;
		}
		
		//Selecting the stage on Oppty page	
		public Select fn_selStage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			Select webList = new Select(driver.findElement(By.xpath("//label[text()='Stage']//following::select[1]")));
			return webList;
		}
		
		//Selecting the Opportunity type on Oppty page	
		public Select fn_selOppType(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			Select webList = new Select(driver.findElement(By.xpath("//label[text()='Opportunity Type']//following::select[1]")));
			return webList;
		}
		
		//Selecting the Competitors on Oppty page	
		public Select fn_selectMultipleOnCompetitors(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			Select webList = new Select(driver.findElement(By.xpath("//select[@title='Competitors - Available']")));
			return webList;
		}

		//to click on img add on Oppty edit page
		public WebElement fn_ClickImgAddOnAddressEditPage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@title='Add']")));
			return webElement;
		}

		//Selecting the Primary Win Reason on Oppty page	
		public Select fn_selWinReason(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			Select webList = new Select(driver.findElement(By.xpath("//label[text()='Primary Win reason']//following::select[1]")));
			return webList;
		}
		
		//click on Edit btn on Oppty page
		public WebElement fn_clickOpptySaveBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h2[text()='Opportunity Edit']//following::input[@title='Save'][1]")));
			return webElement;
		}
		
		//Capture Agreement number
		public WebElement fn_getAgrmntNumber(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Agreement Number']//following::div[1]")));
			return webElement;
		}
		
		//Capture Agreement Name
		public WebElement fn_getAgrmntName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Agreement Name']//following::div[1]")));
			return webElement;
		}

		//Capture Agreement Name input box
		public WebElement fn_getAgrmntNameInputBox(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Agreement Name']/following::input[1]")));
			return webElement;
		}

		//Click Agreement number link
		public WebElement fn_clickAgrmntLink(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//th/a[text()='Agreement End Date']//following::a[2]")));
			return webElement;
		}
		
		//Select the document	
		public Select fn_selDocument(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			Select webList = new Select(driver.findElement(By.xpath("//span[text()='Please select the appropriate document(s) and click Next.']//following::select")));
			return webList;
		}
		
		//click on next button on select document form
		public WebElement fn_clickNextButton(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Next']")));
			return webElement;
		}
		
		//click on next button on select document form
		public WebElement fn_clickMSANextButton(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h3[text()='Attachments']//following::input[@value='Next']")));
			return webElement;
		}
		
		//click on Activate button on select document form
		public WebElement fn_clickActivateButton(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Activate']")));
			return webElement;
		}
		
		//click on Activate button on select document form
		public WebElement fn_getStatus(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Status']//following::div")));
			return webElement;
		}
		
        //click on btn click Activate
		public WebElement fn_clickActivateOrderForm(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Activate']")));
			return webElement;
		}
		
		
        //click on GMSA Img click Activate
		public WebElement fn_clickGMSAFrmActivate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@alt='Activate']")));
			return webElement;
		}
		
        //Fetch the order Number
		public WebElement fn_getOrderNumber(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//th[text()='Order Number']//following::a[2]")));
			return webElement;
		}
		
		//Fetch the order Number
		public WebElement fn_getProductFamilyCondition(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Product Family Condition']/following::td[1]/div")));
			return webElement;
		}
		
		//Fetch the order Number
		public WebElement fn_getOrderCount(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Orders']/span")));
			return webElement;
		}
		
        //Fetch the status of Order Numberstatus
		public WebElement fn_getOrderStatus(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Status']//following::div[1]")));
			return webElement;
		}
		
		//click on save button
		public WebElement fn_clickGotoAcct(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Go to Account']")));
			return webElement;
		}
		
		//click on save button
		public WebElement fn_clickAddbtn(WebDriver driver, WebDriverWait webWait, String strLineItemName) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='"+strLineItemName+"']/following-sibling::td/input[@value='Add']")));
			return webElement;
		}
		
		//click on save button
		public WebElement fn_clickEditbtn(WebDriver driver, WebDriverWait webWait, String strLineItemName) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='"+strLineItemName+"']/following-sibling::td/input[@value='Edit']")));
			return webElement;
		}
		
		//click on save button
		public WebElement fn_setInstallDate(WebDriver driver, WebDriverWait webWait, String strLineItemName, String strAWSID) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='"+strLineItemName+"']/following::input[contains(@value,'"+strAWSID+"')]/following::input[1]")));
			return webElement;
		}
		
		//click on save button
		public WebElement fn_setExternalId(WebDriver driver, WebDriverWait webWait, String strLineItemName) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='"+strLineItemName+"']/following::input[1]")));
			return webElement;
		}
		
		//click on save button
		public WebElement fn_clickSaveEntrybtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Save Entry']")));
			return webElement;
		}
		
		//click on save button
		public WebElement fn_clickSaveEditsbtn(WebDriver driver, WebDriverWait webWait, String strAWSID) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[contains(@value,'"+strAWSID+"')]/following::input[@value='Save Edits']")));
			return webElement;
		}
		
		//click on save button
		public WebElement fn_clickSavebtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Save']")));
			return webElement;
		}
		
		//click on save button
		public WebElement fn_clickSendtoPSObtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Send to PSO']")));
			return webElement;
		}
		
		//click on save button
		public WebElement fn_clickSendtoPMbtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Send to Selected PM']")));
			return webElement;
		}
		
		//click on save button
		public WebElement fn_clickActivatebtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Activate']")));
			return webElement;
		}
		
		//Selecting the stage on Oppty page	
		public Select fn_selAssigntoPM(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			Select webList = new Select(driver.findElement(By.xpath("//label[text()='Assign to PM']//following::select[1]")));
			return webList;
		}
		
		
		//click on save button
		public WebElement fn_setSNTCode(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='SNT Code']/following::input[1]")));
			return webElement;
		}
		
		//click on save button
		public WebElement fn_setGUID(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='GUID']/following::input[1]")));
			return webElement;
		}
		
		//click on save button
		public WebElement fn_getSendtoPSOtext(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='Stage Complete - Email Sent to the PSO']")));
			return webElement;
		}
		
		//click on save button
		public WebElement fn_getSendtoPMtext(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='Stage Complete - Email Sent to the PM']")));
			return webElement;
		}
		
		//click on save button
		public WebElement fn_getRecordsSavedtext(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='Records Saved']")));
			return webElement;
		}
		
		//click on save button
		public WebElement fn_getActivatedtext(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='Activated']")));
			return webElement;
		}
		
		
		
		//click on save button
		public WebElement fn_setProductExternalId(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Product External Id']/following::input[1]")));
			return webElement;
		}
		
		//click on save button
		public WebElement fn_setProductInstallDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{	
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Product Install Date']/following::input[1]")));
			return webElement;
		}
		
}
