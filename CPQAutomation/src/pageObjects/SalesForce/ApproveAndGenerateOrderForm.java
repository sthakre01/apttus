
/*
 Author     		:	Suhas Thakre
 Class Name			: 	ApproveAndGenerateOrderForm 
 Purpose     		: 	Purpose of this file is :
						1. To identify Page object related to Approve And Generate Order Form page for Apttus application.

 Date       		:	06/012/2017 
 Modified Date		:	06/012/2017	
 Modified By		: 	
 Version Information:	Version 1.0
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/
package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ApproveAndGenerateOrderForm {
	
	public static WebElement webElement = null;
	
	// To identify the EnterSearchItem
	public Select fn_selectApproval(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//tr/td[contains(text(),'Approval Stage')]//following::td[1]/div/span/select"))));
		return webList;
	}

	// To Click on CreateAgreementGMSA button
	public WebElement fn_clickSendForReviewBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@alt=\"Send For Review\"]")));
		return webElement;
	}
	
	// To Click on Select Generated GMSA
	public WebElement fn_clickAttachGMSA(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//th/div[contains(text(),\"Last Modified Date\")]//following::td/input")));
		return webElement;
	}
	
	// To Click on the Next Button
	public WebElement fn_clickNextBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value=\"Next\"]")));
		return webElement;
	}
	
	// To Click on Select NewOrderFormBtn
	public WebElement fn_clickNewOrderFormBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),\"GMSA/NDA/New Order Form Send For Customer Review\")]//preceding::span/input[@type=\"radio\"]")));
		return webElement;
	}
}
