package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ConstraintRuleVaildations {	

		
	public static WebElement webElement = null;
	
	
		//To identify the Validation Message Displayed
		public WebElement fn_getMessageType(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{		
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='apt-page-header cartTable clearfix']/following::table[1]")));
			return webElement;
		}
		
		//To identify the CR Rule dialog box
		public WebElement fn_getCRRuleMessage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{		
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Constraint Rule Message']/following::div[1]")));
			return webElement;
		}
		
		//To identify the CheckBox of CRRule dialog box
		public WebElement fn_clickCRcheckbox(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{		
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Constraint Rule Message']/following::input[1]")));
			return webElement;
		}
		
	}


