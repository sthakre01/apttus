package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ConfigurationPageObjects {	

		
	public static WebElement webElement = null;
	
	
		//To identify the configure Button
		public WebElement fn_clickConfigureBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			//webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[contains(text(),'Configure Products')]/following::td[1]/div/a/img")));
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[contains(@alt, 'Configure Products')]")));
			return webElement;
		}

		// To identify the ServiceName Field
		public WebElement fn_clickBaseServiceName(WebDriver driver, WebDriverWait webWait,String strServicename) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.partialLinkText(strServicename)));
			return webElement;
		}
		
		
		// To identify the ServiceName Field
		public WebElement fn_clickServiceName(WebDriver driver, WebDriverWait webWait,String strComponentName) throws InterruptedException
		{
			//webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(("//span[@class=\"aptSearchProductName\" and text() ='"+strComponentName+"']/following::div[*]/span/a"))));
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(("//span[@class=\"aptSearchProductName\" and text()='"+strComponentName+"']//following::a[text()=\"Configure\"]"))));
			return webElement;
		}
		
		// To identify the Equipment Reference feild
		public WebElement fn_setEquipReference(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(("//input[@class='Equipment_Reference__c Equipment_Reference__c_input']"))));
			return webElement;
		}
		
		//To identify the Offering Data Window
		public WebElement fn_getNDAAddressWindow(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@title='Address Lookup (New Window)']")));
			return webElement;
		}
		
		//To identify the NDA_Address  Data Window
		public WebElement fn_getOfferWindow(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/span/*/a/img[@alt=\"Offering Data Lookup (New Window)\"]")));
			return webElement;
		}
		
		// To identify the Next Field
		public WebElement fn_clickNext(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(("//*[@id=\"Next\"]/a"))));
			return webElement;
		}
		
		// To identify the SpinnerImage
		public WebElement fn_WaitForSpinner(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(("//img[@class=\"spinnerImg-tree\"]"))));
			return webElement;
		}
		
		
		// To identify the Pricing Button
		public WebElement fn_clickPricingBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(("(//a[text()='Go to Pricing'])[2]"))));
			return webElement;
		}
		
		// To identify the Toggle button of show pricing
		public WebElement fn_clickShowPricing(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.className("showOptionButton")));
			return webElement;
		}		
		
		// To identify the Save button on pricing page
		public WebElement fn_clickSave(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//ul[2]/li[6]/a[contains(text(),'Update Line Items & Exit')]")));
			return webElement;
		}	
		
		// To identify the Save button on pricing page
		public WebElement fn_clickOfferingMgmntBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[text()=\"Offering Management\"]")));
			return webElement;
		}
		
		// To identify the Save button on pricing page
		public WebElement fn_clickDragFrom(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[@class=\"sortable-item\"]")));
			return webElement;
		}
		
		// To identify the Save button on pricing page
		public WebElement fn_clickDragTo(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[@class=\"sortable-item\"]//following::ul[1]")));
			return webElement;
		}
		
		// To identify the Save button on pricing page
		public WebElement fn_clickGoToCart(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value=\"Go To Cart\"]")));
			return webElement;
		}
		
		// To identify the Save button on pricing page
		public WebElement fn_createPropAndSubmitBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[text()=\"Create Proposal & Submit\"]")));
			return webElement;
		}
		
		
		// To identify the Search Btn on ShoppingCart Page
		public WebElement fn_SearchProducts(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@placeholder='Search here']")));
			return webElement;
		}
		
		// To identify the Search Btn on ShoppingCart Page
		public WebElement fn_clicksearchBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Search']")));
			return webElement;
		}
		
		// To click on addmoreproducts btn
		public WebElement fn_clickAddMorePrdctsBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//ul/li/a[text()='Add More Products']")));
			return webElement;
		}
			
		// To identify the configurationtable
		public WebElement fn_getTableCount(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='aptLeafProductsTable']/table")));
			return webElement;
		}
		
		// To identify the Save Button on attributes window page
		public WebElement fn_clickAttSaveBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Save']")));
			return webElement;
		}
		
		
	}


