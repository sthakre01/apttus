
/*
 Author     		:	Suhas Thakre
 Class Name			: 	Login 
 Purpose     		: 	Purpose of this file is :
						1. To identify Page object related to Login page for Apttus application.

 Date       		:	06/012/2017 
 Modified Date		:	06/012/2017	
 Modified By		: 	
 Version Information:	Version 1.0
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/
package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Login {
	
public static WebElement webElement = null;
	
	// To identify the UserName Field
	public WebElement fn_EnterUserName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.id("username")));
		return webElement;
	}
	
	// To identify the Password Field
	public WebElement fn_EnterPassword(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.id("password")));
		return webElement;
	}
	
	// To identify the Login Button
	public WebElement fn_ClickLoginButton(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.id("Login")));
		return webElement;
	}
	
	// To identify the Navigate Arrow Button
	public WebElement fn_clickNavigateArrowBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='userNav-arrow']")));
		return webElement;
	}
	
	// To identify the Logout Button
	public WebElement fn_clickLogOutButton(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/a[@title='Logout']")));
		return webElement;
	}

}
