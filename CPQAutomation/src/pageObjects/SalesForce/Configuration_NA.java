package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Configuration_NA {	

		
public static WebElement webElement = null;
	

	// To identify the Product Options
	public WebElement fn_SearchProducts(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@placeholder='Find Products']")));
		return webElement;
	} 
	
	// To identify the ServiceName Field
	public WebElement fn_clickServiceName(WebDriver driver, WebDriverWait webWait,String strComponentName) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(("//a[text()='"+strComponentName+"']/following::span[text()='Configure...']"))));
		return webElement;
	}
	
	// To identify the Product Options
	public WebElement fn_clickProductOptions(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Product Options']")));
		return webElement;
	}

	// To identify the Product Options
	public WebElement fn_setEquipReference(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Equipment Reference']/following::input[1]")));
		return webElement;
	}
	
	// To Select the Record type of new Record
	public WebElement fn_clickUserOverride(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[normalize-space( ) ='User Override']/following::span[@class='select2-chosen ng-binding']")));
		return webElement;
	}
	
	// To Select the Record type of new Record
	public WebElement fn_setMonthlyCommitment(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[normalize-space( ) ='Monthly Commitment']/following::input[@type='text'][1]")));
		return webElement;
	}
	
	// To Select the Record type of new Record
	public WebElement fn_setattribute(WebDriver driver, WebDriverWait webWait,String strattribute) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[normalize-space( ) ='"+strattribute+"']/following::input[@type='text'][1]")));
		return webElement;
	}
	
	// To Select the Record type of new Record
	public WebElement fn_setLineIteamQty(WebDriver driver, WebDriverWait webWait, String strLineItem) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='"+strLineItem+"']/following::input[1]")));
		return webElement;
	}
	
	// To identify the UserOverRide List Fields
	public WebElement fn_selectOverRide(WebDriver driver, WebDriverWait webWait,String strValue) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(("//div[text()='"+strValue+"' and @class='ng-binding ng-scope']"))));
		return webElement;
	}
	
	// To Click GotoPricing
	public WebElement fn_clickGoToPricing(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		//webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Go to Pricing']")));
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button/i[@class='ss-addcart']")));
		return webElement;
	}
	
	// To identify the Save button on pricing page
	public WebElement fn_clickGotoPricingNABtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Go to Pricing']/..")));
		return webElement;
	}
	
	// To identify the Save button on pricing page
	public WebElement fn_clickRepriceNABtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Reprice']/..")));
		return webElement;
	}
	
	// To identify the Save button on pricing page
	public WebElement fn_clickRemoveItemNABtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Remove Item']/..")));
		return webElement;
	}
	
	// To identify the Save button on pricing page
	public WebElement fn_clickQuickSaveNABtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Quick Save']/..")));
		return webElement;
	}
	
	// To Click Offering Management Button
	public WebElement fn_clickOfferingMgmntBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Offering Management']")));
		return webElement;
	}
	
	// To Click Offering Management Button
	public WebElement fn_clickAddMorePrdctsBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Add More Products']")));
		return webElement;
	}
		
	}


