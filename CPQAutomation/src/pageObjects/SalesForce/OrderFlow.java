
/*
 Author     		:	Suhas Thakre
 Class Name			: 	OrderFlow 
 Purpose     		: 	Purpose of this file is :
						1. To identify Page object related to Order Flow page for Apttus application.

 Date       		:	06/012/2017 
 Modified Date		:	06/012/2017	
 Modified By		: 	
 Version Information:	Version 1.0
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/
package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OrderFlow {
	
public static WebElement webElement = null;
	
	// To Click on Generate Button
	public WebElement fn_clickGenerate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Generate Proposal']/following::img[@alt='Generate']")));
		return webElement;
	}
	
	// To Identify the watermarkcheckbox
	public WebElement fn_clickWaterMark(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/label[contains(text(),'2. Include Watermark')]//following::input")));
		return webElement;
	}
	
	//To identify the radiobtn before proposal to present
	public WebElement fn_clickProposalToPresent(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/span[contains(text(),'Proposal - To Present')]//preceding::td[1]/span")));
		return webElement;
	}
	
	//To identify the radiobtn before proposal to present
	public WebElement fn_clickProposalGenerateBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Generate']")));
		return webElement;
	}
	
	//To identify the radiobtn before proposal to present
	public WebElement fn_clickProposalCancelBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Cancel']")));
		return webElement;
	}
	
	//To identify the radiobtn before proposal to present
	public WebElement fn_clickReturnBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td/input[@value='Return' and @type='submit']")));
		return webElement;
	}
	
	//To identify the radiobtn before proposal to present
	public WebElement fn_getTextApprovalStage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[@class='labelCol' and text()='Approval Stage']//following::td/div")));
		return webElement;
	}
	
	//To identify the radiobtn before proposal to present
	public WebElement fn_getPrimaryContact(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[@class='labelCol' and text()='Primary Contact']//following::a[1]")));
		return webElement;
	}
	
	
	//To identify the status of Status Category
	public WebElement fn_getStatusCategory(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Status Category']//following::div")));
		return webElement;
	}
	
	//To identify the status of Status Category
	public WebElement fn_getStatus(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Status']//following::div")));
		return webElement;
	}
	
	//To click on the present proposal button
	public WebElement fn_clickPresent(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[@class='last labelCol' and text()='Present Proposal']//following::td/div/a/img[@alt='Present']")));
		return webElement;
	}
	
	//To click on the present proposal button
	public WebElement fn_clickSendToImg(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//th/label[text()='Send to:']//following::a/img")));
		return webElement;
	}
	
	//To set text in subject field
	public WebElement fn_setTextinSubject(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//th/label[text()='Subject']//following::input")));
		return webElement;
	}
	
	//To set text in EmailBody field
	public WebElement fn_setTextinEmailBody(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//th/label[text()='Email Body']//following::textarea")));
		return webElement;
	}
	
	//To click on the present proposal button
	public WebElement fn_clickPropAttchmnt(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//th/div[text()='Description']//following::input")));
		return webElement;
	}
	
	//To click on the present proposal button
	public WebElement fn_clickSendEmailBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@value='Send']")));
		return webElement;
	}
	
	//To click on the Accept proposal button
	public WebElement fn_clickAccept(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[@class='labelCol' and text()='Accept']//following::td/div/a/img[1]")));
		return webElement;
	}
	
	//To click on the CreateMSAAgreementBtn
	public WebElement fn_clickMSA_AGRMNT_Btn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[@class='labelCol' and text()='Create GMSA Agreement']//following::div/a/img[@alt='Create_Agreement_GMSA']")));
		return webElement;
	}
	
	//To click on the CreateMSAAgreementBtn
	public WebElement fn_setGMSAAddr(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label[text()='Address']/following::span/input)[1]")));
		return webElement;
	}
	
	//To click on the CreateMSAAgreementBtn
	public WebElement fn_setOrderAddr(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label[text()='Address']/following::span/input)[1]")));
		return webElement;
	}
	
	//To click on the CreateMSAAgreementBtn
	public WebElement fn_clickAddressLukUpWndw(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Address']//following::img[@alt='Address Lookup (New Window)']")));
		return webElement;
	}
	
	//To click on the CreateMSAAgreementBtn
	public WebElement fn_clickReportLukUpWndw(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@alt='Reports To Lookup (New Window)']")));
		return webElement;
	}
	
	//To click on the CreateMSAAgreementBtn
	public WebElement fn_clickMSAStartDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Agreement Start Date']//following::a")));
		return webElement;
	}
	
	//To click on the CreateMSAAgreementBtn
	public WebElement fn_SetMSAStartDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Agreement Start Date']//following::input[1]")));
		return webElement;
	}
	
	//To click on the GMSAFormSaveBtn
	public WebElement fn_clickMSASaveBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h2[text()='New GMSA Agreement Data Entry']//following::input[1][@title='Save']")));
		return webElement;
	}
	
	//To click on the GMSAFormSaveBtn
	public WebElement fn_clickCreateAnthrBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//th/label[text()='Other GMSA agreement exists, create another?']//following::input")));
		return webElement;
	}
	
	//To click on the GMSAFormSaveBtn
	public WebElement fn_captureMSAAgrmntNumber(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Agreement Number']//following::td/div")));
		return webElement;
	}
	
	//To click on the GMSAFormSaveBtn
	public WebElement fn_captureMSAAgrmntName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Agreement Name']//following::td/div")));
		return webElement;
	}
	
	
	//To click on the AgrementLnk
	public WebElement fn_clickAgrementLnk(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//th[text()='Agreement End Date']//following::a[3]")));
		return webElement;
	}
		
	//To click on the GenerateBtn
	public WebElement fn_clickGenerateBtnOnAgrementFrm(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@alt='Generate']")));
		return webElement;
	}
	
	//To click on the CreateAgmntwhLineitems
	public WebElement fn_clickCrtAgmntLinItmsBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/a/img[@alt='Create Agreement with Line Items']")));
		return webElement;
	}
	
	// To Select the Billing Frequency
	public Select fn_selAgreementRecordType(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList =  new Select(driver.findElement(By.xpath("//h2[text()='Select Agreement Record Type']/following::select[1]")));
		return webList;
	}
	
	//To click on the Continue Btn
	public WebElement fn_clickContinueBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@value='Continue']")));
		return webElement;
	}
	
	//To click on the Continue Btn
	public WebElement fn_clickOrderFrmAdressImg(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@value='Continue']")));
		return webElement;
	}
	
	//To click on the Save Btn
	public WebElement fn_clickOrderFrmSaveBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h2[text()='Agreement Edit']//following::input[@name='save']")));
		return webElement;
	}
	
	//To click on the Save Btn
	public WebElement fn_clickGenerateSpprtDocsBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@alt='Generate Supporting Document']")));
		return webElement;
	}
	
	//To click on the select templateBtn
	public WebElement fn_clickSelectTempOnOrderFrm(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//th/div[text()='Subcategory']//following::input[1]")));
		return webElement;
	}
	
	
	//To click on the select templateBtn
	public WebElement fn_clickSpecificTempOnOrderFrm(WebDriver driver, WebDriverWait webWait, String strTemplate) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'"+strTemplate+"')]/../../td[1]//input")));
		return webElement;
	}
	

	//To click on the Generate On OrderFormBtn
	public WebElement fn_clickOrdrFrmGnrtBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Generate']")));
		return webElement;
	}
	
	
	//To identify the radiobtn before proposal to present
	public WebElement fn_clickOrderToPresent(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/span[contains(text(),'Order Form - To Present')]//preceding::td[1]/span")));
		return webElement;
	}
	
	//To click on the Generate On OrderFormBtn
	public WebElement fn_clickOrdrFrmReturnBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Return']")));
		return webElement;
	}
	
	//To click on the Generate On OrderFormBtn
	public WebElement fn_clickSendForReview(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@alt='Send For Review']")));
		return webElement;
	}
	
	//To click on the selection on doc for MSA AGMNT
	public WebElement fn_clickSelectDocForMSA(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Last Modified Date']//following::input")));
		return webElement;
	}
	
	
	//To click on the SelectAttcahments Checkbox on Order Form
	public WebElement fn_clickSelectAttachment(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//th/div[text()='Last Modified Date']//following::input[@type='checkbox']")));
		return webElement;
	}
	
	//To click on the Next button select attchment form
	public WebElement fn_clickNextBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Next']")));
		return webElement;
	}
	
	//To click on Select Email Template Button
	public WebElement fn_clickEmailTemplate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//th/div/a[text()='Description']//following::input[1]")));
		return webElement;
	}
	
	//To click on Next button on Email Template Page
	public WebElement fn_clickNextBtnEmailTemplate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h2[text()='2. Select Email Template (Optional)']//following::input[2][@value='Next']")));
		return webElement;
	}
	
	//To click on Img for selecting the To feild to send email with sungardas template
	public WebElement fn_clickToField(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//th/label[contains(text(),'To')]//following::img[@alt='Reports To Lookup (New Window)']")));
		return webElement;
	}
	
	//To click on Img for selecting the To feild to send email with sungardas template
	public WebElement fn_clickEmailSendBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Send']")));
		return webElement;
	}
	
	//To click on Btn send for esignature
	public WebElement fn_clickESignatureBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@alt='Send For eSignatures']")));
		return webElement;
	}
	
	//To click on AddrecipientsBtn
	public WebElement fn_clickAddRecipientsBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Add Recipient(s)']")));
		return webElement;
	}
	
	//To enter Value for Name field in Add Recipeints page
	public WebElement fn_setNameForRecipient(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@placeholder='Name']")));
		return webElement;
	}
	
	//To enter Value for Email field in Add Recipeints page
	public WebElement fn_setEmailForRecipient(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@placeholder='Email']")));
		return webElement;
	}
	
	//To Click on SendForEsignature Btn 
	public WebElement fn_clickSendForEsignatureBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='recipient_buttons']/input[@value='Send For eSignature']")));
		return webElement;
	}
	
	//To Click on SendForEsignature Btn 
	public WebElement fn_clickFinalizeDocuSignBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='recipient_buttons']/input[@value='Finalize In DocuSign']")));
		return webElement;
	}
	
	//To Click on SendForEsignature Btn 
	public WebElement fn_clickSendBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Send']/..")));
		return webElement;
	}
	
	//To Click on SendForEsignature Btn 
	public WebElement fn_clickWithoutFieldsBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Send Without Fields']")));
		return webElement;
	}
	
	
	//To click on the Amend button 
	public WebElement fn_clickAmendBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//td[ text()='Amend Previous Agreement']//following::div/a/img[@alt='Amend'])[1]")));
		return webElement;
	}
	
	//To click on the Renewal button 
	public WebElement fn_clickRenewalBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//td[ text()='Renew Previous Agreement']//following::div/a/img[@alt='Renew'])[1]")));
		return webElement;
	}
	
	//To click on the Amendments and Renewals check box
	public WebElement fn_clickAmendmentsRenewalscheckbox(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Amendments and Renewals']/ancestor::tr[1]//input")));
		return webElement;
	}
	
	//To click on Select Email Template Button for Amendement
	public WebElement fn_clickEmailTemplateAmendement(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='qOrder Form Renewal/Amendement Send For Customer Review']//preceding::input[1]")));
		return webElement;
	}
	
	//To click on Next button on Email Template Page for Amendement
	public WebElement fn_clickNextBtnEmailTemplateAmendement(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h2[text()='3. Select Email Template (Optional)']//following::input[2][@value='Next']")));
		return webElement;
	}
	
	
	//To click on the Amend button 
	public WebElement fn_clickEditBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.name("edit")));
		return webElement;
	}
	
	//To click on the Amend button 
	public WebElement fn_clickSaveBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.name("save")));
		return webElement;
	}
		
	
	// To Select the Billing Frequency
	public Select fn_selAgreementType(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList =  new Select(driver.findElement(By.xpath("//label[text()='Offline Agreement Type']/following::select[1]")));
		return webList;
	}
	
	// To Select the Billing Frequency
	public Select fn_selSource(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList =  new Select(driver.findElement(By.xpath("//label[text()='Source']/following::select[1]")));
		return webList;
	}
	
	//To click on Btn send for esignature
	public WebElement fn_clickImportDocBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@alt='Import Offline Document']")));
		return webElement;
	}
	
	//To click on Btn send for esignature
	public WebElement fn_setFile(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@type='file']")));
		return webElement;
	}
	
	//To click on AddrecipientsBtn
	public WebElement fn_clickAttachFileBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Attach File']")));
		return webElement;
	}
	
	//To click on AddrecipientsBtn
	public WebElement fn_getDocImportedtext(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//p[text()='Documents Imported:']")));
		return webElement;
	}
	
	
		
}


