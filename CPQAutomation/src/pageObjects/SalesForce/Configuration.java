
/*
 Author     		:	Suhas Thakre
 Class Name			: 	Configuration 
 Purpose     		: 	Purpose of this file is :
						1. To identify Page object related to Configuration page for Apttus application.

 Date       		:	06/012/2017 
 Modified Date		:	06/012/2017	
 Modified By		: 	
 Version Information:	Version 1.0
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/
package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Configuration {	

		
	public static WebElement webElement = null;
	
	
		//To identify the configure Button
		public WebElement fn_clickConfigureBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			//webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[contains(text(),'Configure Products')]/following::td[1]/div/a/img")));
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[contains(@alt, 'Configure Products')]")));
			return webElement;
		}

		// To identify the ServiceName Field
		public WebElement fn_clickBaseServiceName(WebDriver driver, WebDriverWait webWait,String strServicename) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.partialLinkText(strServicename)));
			return webElement;
		}
		
		
		// To identify the ServiceName Field
		public WebElement fn_clickServiceName(WebDriver driver, WebDriverWait webWait,String strComponentName) throws InterruptedException
		{
			//webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(("//span[@class=\"aptSearchProductName\" and text() ='"+strComponentName+"']/following::div[*]/span/a"))));
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(("//span[@class=\"aptSearchProductName\" and text()='"+strComponentName+"']//following::a[text()=\"Configure\"]"))));
			return webElement;
		}
		
		// To identify the Equipment Reference field
		public WebElement fn_setEquipReference(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(("//input[@class='Equipment_Reference__c Equipment_Reference__c_input']"))));
			return webElement;
		}
		
		//To identify the Offering Data Window
		public WebElement fn_getNDAAddressWindow(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@title='Address Lookup (New Window)']")));
			return webElement;
		}
		
		//To identify the NDA_Address  Data Window
		public WebElement fn_getOfferWindow(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/span/*/a/img[@alt=\"Offering Data Lookup (New Window)\"]")));
			return webElement;
		}
		
		// To identify the Next Field
		public WebElement fn_clickNext(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(("//*[@id=\"Next\"]/a"))));
			return webElement;
		}
		
		// To identify the Next Field
		public WebElement fn_clickProductOptionsbtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(("//span[text()='Product Options']"))));
			return webElement;
		}
		
		// To identify the SpinnerImage
		public WebElement fn_WaitForSpinner(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(("//img[@class=\"spinnerImg-tree\"]"))));
			return webElement;
		}
		
		
		// To identify the Pricing Button
		public WebElement fn_clickPricingBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(("(//a[text()='Go to Pricing'])[2]"))));
			return webElement;
		}
		
		// To identify the Pricing Button
		public WebElement fn_clickUpdatePriceBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(("(//a[text()='Update Price'])[1]"))));
			return webElement;
		}
		
		// To identify the Pricing Button
		public WebElement fn_clickValidateBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(("//a[text()='Validate']"))));
			return webElement;
		}
		
		// To identify the Pricing Button
		public WebElement fn_clickHeader(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(("//div[@class='apt-page-header cartTable clearfix']"))));
			return webElement;
		}
		
		// To identify the Toggle button of show pricing
		public WebElement fn_clickShowPricing(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.className("showOptionButton")));
			return webElement;
		}		
		
		// To identify the Save button on pricing page
		public WebElement fn_clickSave(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//ul[2]/li[6]/a[contains(text(),'Update Line Items & Exit')]")));
			return webElement;
		}	
		
		// To identify the Save button on pricing page
		public WebElement fn_clickOfferingMgmntBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[text()=\"Offering Management\"]")));
			return webElement;
		}
		
		// To identify the Save button on pricing page
		public WebElement fn_clickOfferingMgmntNABtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Offering Management']/..")));
			return webElement;
		}
		
		// To identify the Drag from button on pricing page
		public WebElement fn_clickDragFrom(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Products Unassigned to Offering Data Records']/following::ul[1]/span[1]")));
			return webElement;
		}
		
		// To identify the Drag from button on pricing page
		public WebElement fn_clickDragProductFrom(WebDriver driver, WebDriverWait webWait, String strProductName) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Products Unassigned to Offering Data Records']/following::ul[1]/span/li[contains(text(),'"+strProductName+"')]")));
			return webElement;
		}
		
		// To identify the Drag To button on pricing page
		public WebElement fn_clickDragToOffering(WebDriver driver, WebDriverWait webWait, String strOfferingNameOrKey) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'"+strOfferingNameOrKey+"')]//following::ul[1]")));
			return webElement;
		}
		
		// To identify the Drag TO button on pricing page
		public WebElement fn_clickDragTo(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[@class=\"sortable-item\"]//following::ul[1]")));
			return webElement;
		}
		
		// To identify the Save button on pricing page
		public WebElement fn_clickGoToCart(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value=\"Go To Cart\"]")));
			return webElement;
		}
		
		// To identify the Save button on pricing page
		public WebElement fn_clickAddNewOffering(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Add New Offering']")));
			return webElement;
		}
		
		// To identify the Save button on pricing page
		public WebElement fn_clickSaveandReturn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Save and Return']")));
			return webElement;
		}
		
		// To identify the Save button on pricing page
		public WebElement fn_clickSaveConfigbtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@title='Save Configuration']")));
			return webElement;
		}
		
		// To identify the Save button on pricing page
		public WebElement fn_createPropAndSubmitBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[text()=\"Create Proposal & Submit\"]")));
			return webElement;
		}
		
		// To identify the Save button on pricing page
		public WebElement fn_createPropAndSubmitNABtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Create Prop & Submit']/..")));
			return webElement;
		}
		
		// To identify the Search Btn on ShoppingCart Page
		public WebElement fn_SearchProducts(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@placeholder='Search here']")));
			return webElement;
		}
		
		// To identify the Search Btn on ShoppingCart Page
		public WebElement fn_clicksearchBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Search']")));
			return webElement;
		}
		
		//To identify the configure Button
		public WebElement fn_AddMoreProductsBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[text()='Add More Products']")));
			
			return webElement;
		}
		
		//To identify the Change Start Date
		public WebElement fn_SelectChangeStartDatePicker(WebDriver driver, WebDriverWait webWait, String strOfferingName) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[text()='"+strOfferingName+"']/following::td[normalize-space(text())='Change Start Date:']/following::a[1])[1]")));
			
			return webElement;
		}
		
		//To identify the Change Start Date
		public WebElement fn_SelectChangeBillingDatePicker(WebDriver driver, WebDriverWait webWait, String strOfferingName) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[text()='"+strOfferingName+"']/following::td[normalize-space(text())='Change Billing Date:']/following::a[1])[1]")));
			
			return webElement;
		}
		
		// To click on Add more products btn
		public WebElement fn_clickAddMorePrdctsBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//ul/li/a[text()='Add More Products']")));
			return webElement;
		}
		
	}


