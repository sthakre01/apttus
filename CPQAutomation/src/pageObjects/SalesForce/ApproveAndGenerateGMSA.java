
/*
 Author     		:	Suhas Thakre
 Class Name			: 	ApproveAndGenerateGMSA 
 Purpose     		: 	Purpose of this file is :
						1. To identify Page object related to Approve And Generate GMSA page for Apttus application.

 Date       		:	06/012/2017 
 Modified Date		:	06/012/2017	
 Modified By		: 	
 Version Information:	Version 1.0
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/
package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ApproveAndGenerateGMSA {
	
	public static WebElement webElement = null;
	
	// To identify the EnterSearchItem
	public Select fn_selectApproval(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//tr/td[contains(text(),'Approval Stage')]//following::td[1]/div/span/select"))));
		return webList;
	}

	// To Click on CreateAgreementGMSA button
	public WebElement fn_createGMSA(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td[text()=\"Create Agreement GMSA\"]//following::img[1]")));
		return webElement;
	}
	
	//To click on draft element
	public WebElement fn_clickDraft(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td[contains(text(),\"Approval Stage\")]//following::td[1]/div")));
		return webElement;
	}
	
	//To click on Save
	public WebElement fn_clickSave(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td/input[@name=\"inlineEditSave\"]")));
		return webElement;
	}
	
	//To Select the GMSA
	public Select fn_selectDocType(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath("//div/label[contains(text(),\"Record Type of new record\")]//following::select[1]")));
		return webList;
	}
	
	//To click on Continue button
	public WebElement fn_clickContinueBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@value=\"Continue\"]")));
		return webElement;
	}
	
	//To click on Agreement Start Date Link on Agreement Edit Page
	public WebElement fn_clickStartDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/label[text()='Agreement Start Date']//following::a[1]")));
		return webElement;
	}
	
	//To click on Save with warning checkbox on Agreement Edit Page
	public WebElement fn_clickSaveWarning(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/label[text()='Save with Warning']//following::input[1]")));
		return webElement;
	}
	
	//To click on Save Button on Agreement Page
	public WebElement fn_clickSaveBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/h2[text()='Agreement Edit']//following::input[1]")));
		return webElement;
	}
	
	//To click on Generate Button on Agreement Page
	public WebElement fn_clickGenerateBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/span[text()='Generate']//following::img[@alt='Generate']")));
		return webElement;
	}
	
	//To Select the DocumentType 
	public WebElement fn_selDocumentType(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@value=\"doc\"]")));
		return webElement;
	}
	
	//To Select the DocumentType 
	public WebElement fn_selTemplateType(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value=\"Return\"]//following::input[@type=\"radio\"]")));
		return webElement;
	}
	
	//To Click on LInk Click to View the file
	public WebElement fn_clickLinkViewFile(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/a[@title=\"View the document\" and @type=\"application/msword\"]")));
		return webElement;
	}
	
	//To Click on Generate Button on Template page
	public WebElement fn_clickGenerateBtnOnTemplatepg(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[1][@value=\"Generate\"]")));
		return webElement;
	}
}
