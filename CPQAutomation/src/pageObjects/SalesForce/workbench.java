
/*
 Author     		:	Suhas Thakre
 Class Name			: 	Pricing 
 Purpose     		: 	Purpose of this file is :
						1. To identify Page object related to Pricing page for Apttus application.

 Date       		:	06/012/2017 
 Modified Date		:	06/012/2017	
 Modified By		: 	
 Version Information:	Version 1.0
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/
package pageObjects.SalesForce;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class workbench {
	
	
public static WebElement webElement = null;
	
	//To Select Value for lastname
	public Select fn_selEnvironment(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//select[@ name='oauth_host']"))));
		return webList;
	}
	
	//To identify the Updating Price text
	public WebElement fn_clicktermsAccepted(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='termsAccepted']")));		
		return webElement;
	}
	
	
	//To identify the Updating Price text
	public WebElement fn_clickLoginbtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='uiLogin']")));		
		return webElement;
	}
	
	//To identify the Updating Price text
	public WebElement fn_setSOQL(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//textarea")));		
		return webElement;
	}
	
	//To identify the Updating Price text
	public WebElement fn_clickQuerybtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='querySubmit']")));		
		return webElement;
	}
	
	//To identify the Updating Price text
	public WebElement fn_getnorecords(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//p[text()='Sorry, no records returned.']")));		
		return webElement;
	}
	
	//To identify the Updating Price text
	public WebElement fn_getQueryResults(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h2[text()='Query Results']")));		
		return webElement;
	}
	
	//To identify the Updating Price text
	public WebElement fn_QueryResultsTable(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@id='query_results']")));		
		return webElement;
	}
	
	//To identify the Updating Price text
	public WebElement fn_clickDatalink(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='data']")));		
		return webElement;
	}
	
	//To identify the Updating Price text
	public WebElement fn_clickUpdatelink(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[text()='Update']")));		
		return webElement;
	}
	
	//To identify the Updating Price text
	public WebElement fn_clickNextlink(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Next']")));		
		return webElement;
	}
	
	//To Select Value for lastname
	public Select fn_selObjectType(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//td[text()='Object Type']/following::select"))));
		return webList;
	}
	
	//To identify the Updating Price text
	public WebElement fn_clicksingleRecord(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='singleRecord']")));		
		return webElement;
	}
	
	//To identify the Updating Price text
	public WebElement fn_setId(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='singleRecord']/following::input[@name='id']")));		
		return webElement;
	}
	
	//To identify the Updating Price text
	public WebElement fn_clickConfirmUpdatebtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Confirm Update']")));		
		return webElement;
	}
	
	//To identify the Updating Price text
	public WebElement fn_UpdateField(WebDriver driver, WebDriverWait webWait, String strfield) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='"+strfield+"']/following::input[1]")));		
		return webElement;
	}
	
	//To identify the Updating Price text
	public WebElement fn_clickDownload(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Download Full Results']")));		
		return webElement;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		
}