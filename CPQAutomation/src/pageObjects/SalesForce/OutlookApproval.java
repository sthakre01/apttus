
/*
 Author     		:	Suhas Thakre
 Class Name			: 	OutlookApproval 
 Purpose     		: 	Purpose of this file is :
						1. To identify Page object related to Outlook Approval page for Apttus application.

 Date       		:	06/012/2017 
 Modified Date		:	06/012/2017	
 Modified By		: 	
 Version Information:	Version 1.0
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/
package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OutlookApproval {
	
public static WebElement webElement = null;
	
	//To identify the SearchBar on the Outlook Inbox page
	public WebElement fn_clickSearchBar(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@aria-label='Activate Search Textbox']/span[2]")));
		return webElement;
	}
	
	//To identify the SearchBar on the Outlook Inbox page
	public WebElement fn_setSearchBar(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@aria-label='Search mail and people, type your search term then press enter to search.']")));
		return webElement;
	}
	
	
	//To identify the Mail List on the Outlook Inbox page
	public WebElement fn_clickMailListLink(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@aria-label='Mail list']//div[@role='option']")));
		return webElement;
	}
	
	//To identify the Mail List on the Outlook Inbox page
	public WebElement fn_clickSignMailLink(WebDriver driver, WebDriverWait webWait, Integer index) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@aria-label='Mail list']/descendant::span[text()='Request']/..)["+index+"]")));
		return webElement;
	}
	
	
	//To identify the Search button on Outlook Inbox page
	public WebElement fn_clickSearchBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@aria-label='Search mail and people, type your search term then press enter to search.']/following::button[1]")));
		return webElement;
	}
	
	//To identify the Search button on Outlook Inbox page
	public WebElement fn_clickExithBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Exit search']")));
		return webElement;
	}
	
	//To identify the All folders button on Outlook Inbox page
	public WebElement fn_clickAllfoldersBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='All folders' and @tabindex='-1']")));
		return webElement;
	}
	
	//To identify the All folders button on Outlook Inbox page
	public WebElement fn_clickSentItemsBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@title='Sent Items']")));
		return webElement;
	}
	
	//To identify the Reply Button in Inbox table on Gmail Inbox page
	public WebElement fn_clickReplyallBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@title='Reply all']")));
		return webElement;
	}
	
	//To identify the SearchBar on the Gmail Inbox page
	public WebElement fn_setMsgBody(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//textarea[@aria-label='Message body']")));
		return webElement;
	}
	
	//To identify the Reply Button in Inbox table on Gmail Inbox page
	public WebElement fn_clickSendBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@title='Send']")));
		return webElement;
	}
	
	//To identify the Click the Review button inside the email
	public WebElement fn_clickReviewBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@alt='DocuSign']//following::span[contains(text(),'REVIEW')]/../..")));
		return webElement;
	}
	
	//To click on agree and accept checkbox
	public WebElement fn_clickAgreeAndAccept(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='action-bar']/descendant::input[@type='checkbox' and @id='disclosureAccepted']")));
		return webElement;
	}
	
	//To click on continue button after accepting the terms and conditions on Docusign process
	public WebElement fn_clickContinueBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@id='action-bar-btn-continue']")));
		return webElement;
	}
	
	//To click on NavigateBtn for GMSA Docusign process
	public WebElement fn_clickNavigateBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@id='navigate-btn']")));
		return webElement;
	}
	
	//To click on Sign button for GMSA Docusign process
	public WebElement fn_clickSignBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[name()='svg']/*[name()='polygon']")));
		return webElement;
	}
	
	//To click on Sign button for GMSA Docusign process
	public WebElement fn_setPrintName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[contains(@class,'main-text-tab-input tab-form-element required')])[1]")));
		return webElement;
	}
	
	//To click on Sign button for GMSA Docusign process
	public WebElement fn_setPrintTitle(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[contains(@class,'main-text-tab-input tab-form-element required')])[2]")));
		return webElement;
	}
	
	//To click on continue button after accepting the terms and conditions on Docusign process
	public WebElement fn_clickDragFrom(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='btnSignHere']")));
		return webElement;
	}
	
	//To click on continue button after accepting the terms and conditions on Docusign process
	public WebElement fn_clickDragTo(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[starts-with(@id,'ds_hldrBdy_pageimg')]")));
		return webElement;
	}
	
	//To click on Adopt and Sign button after dragging the Docusign to the main page
	public WebElement fn_clickAdoptandSign(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Adopt and Sign' and  @value='signature']")));
		return webElement;
	}
		
	//To click on Finish button
	public WebElement fn_clickFinish(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Finish']")));
		//webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@id='action-bar-bottom-btn-finish']")));
		return webElement;
	}
	
	//To identify the Older button in email list
	public WebElement fn_clickSignoutImg(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@aria-label='User settings']/div[14]/button")));
		return webElement;
	}
	
	//To identify the Older button in email list
	public WebElement fn_clickSignoutBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Sign out']")));
		return webElement;
	}
	
	//To identify the Mail List on the Outlook Inbox page
	public WebElement fn_clickAcctLink(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@alt='Work or school account symbol']")));
		return webElement;
	}
	
	//To identify the Mail List on the Outlook Inbox page
	public WebElement fn_clickAnotherAccountLink(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr[@class='use_another_account ']")));
		return webElement;
	}
	
	//To identify the More actions link 
	public WebElement fn_clickMoreActionsLink(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//button[@title='Post reply'])[2]//following::button[@title='More actions']/span[1]")));
		return webElement;
	}
	
	//To identify the More actions link 
	public WebElement fn_clickReplyBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@aria-label='Reply']//span[text()='Reply']")));
		return webElement;
	}
	
	//To identify the Submit with Attachments Buttons on Approval Page
	public WebElement fn_setEmail(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='cred_userid_inputtext']")));
		return webElement;
	}
	
	//To identify the Submit with Attachments Buttons on Approval Page
	public WebElement fn_setPassword(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='cred_password_inputtext']")));
		return webElement;
	}
	
	//To identify the Submit with Attachments Buttons on Approval Page
	public WebElement fn_clickSignInBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@id='cred_sign_in_button']")));
		return webElement;
	}
	
	//To click on continue button after accepting the terms and conditions on Docusign process
	public WebElement fn_ReviewText(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//p[text()='Request for eSignatures. Please review and sign.']")));
		return webElement;
	}
}
