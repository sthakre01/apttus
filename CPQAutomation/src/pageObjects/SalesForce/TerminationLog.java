
/*
 Author     		:	Suhas Thakre
 Class Name			: 	LeadCreation 
 Purpose     		: 	Purpose of this file is :
						1. To identify Page object related to Lead Creation page for Apttus application.

 Date       		:	06/012/2017 
 Modified Date		:	06/012/2017	
 Modified By		: 	
 Version Information:	Version 1.0
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/
package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TerminationLog {
	
	public static WebElement webElement = null;
		
	// To identify the Edit button
	public WebElement fn_clickEditBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@title='Edit' and @type='button']")));
		return webElement;
	}
   
	// To identify the Save Button
	public WebElement fn_clickSaveBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.name("save")));
		return webElement;
	}
	
	//To Select the Termination Status
	public Select fn_selTerminationStatus(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath("//td/label[text()='Termination Status']//following::td[1]/span/select")));
		return webList;
	}
	
	//To enter Termination Date
	public WebElement fn_setTerminationDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/label[text()='Effective Date of Termination']//following::td[1]//a")));
		return webElement;
	}
	
	// To identify the Save Button
	public WebElement fn_clickPaymentReceiveBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/label[text()='Payment Received']//following::td[1]/input[@type='checkbox']")));
		return webElement;
	}
}


