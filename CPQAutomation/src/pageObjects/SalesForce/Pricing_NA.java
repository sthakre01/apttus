
/*
 Author     		:	Suhas Thakre
 Class Name			: 	Pricing 
 Purpose     		: 	Purpose of this file is :
						1. To identify Page object related to Pricing page for Apttus application.

 Date       		:	06/012/2017 
 Modified Date		:	06/012/2017	
 Modified By		: 	
 Version Information:	Version 1.0
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/
package pageObjects.SalesForce;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Pricing_NA {
	
	
public static WebElement webElement = null;
	
	// To identify the  Rates table
//To identify the Reprice Button
	public List<WebElement> fn_getProdOptions(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		List<WebElement> lsteleProduct = driver.findElements(By.xpath("//a[text()='Show Options']"));
		return lsteleProduct;
	}

	//To identify the Reprice Button
	public List<WebElement> fn_getLineItems(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		List<WebElement> lsteleLineItems = driver.findElements(By.xpath("(//div[@class='option-line-name']/..)"));
		return lsteleLineItems;
		
	}
	
	//To identify the Reprice Button
	public List<WebElement> fn_getChargeType(WebDriver driver, WebDriverWait webWait, int rowindex) throws InterruptedException
	{
		List<WebElement> lsteleLineItems = driver.findElements(By.xpath("(//div[@class='option-line-name']/div/span)["+rowindex+"]/ancestor::li[1]/child::ul/descendant::dynamic-field/div"));
		return lsteleLineItems;
		
	}
	
	//To identify the Updating Price text
	public WebElement fn_getLineItemOptionsTable(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@class='aptLineItemOptionsTable']/tbody")));		
		return webElement;
	}
	
	//To identify the Reprice Button
	public WebElement fn_getLineItem(WebDriver driver, WebDriverWait webWait,int rowindex) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//div[@class='option-line-name']/div/span)["+rowindex+"]")));		
		return webElement;
		
	}
	
	
	//To identify the Reprice Button
	public WebElement fn_getBasePrice(WebDriver driver, WebDriverWait webWait,int rowindex, int chargetypeindex) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//td[4]//li["+rowindex+"]//li["+chargetypeindex+"]//dynamic-field/div)[1]")));		
		return webElement;
		
	}
	
	//To identify the Reprice Button
	public WebElement fn_getQuantity(WebDriver driver, WebDriverWait webWait,int rowindex, int chargetypeindex) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//td[6]//li["+rowindex+"]//li["+chargetypeindex+"]/descendant::div[@class='aptQuantity'])[1]")));		
		return webElement;
		
	}
	
	//To identify the Reprice Button
	public WebElement fn_getQuantityInputbox(WebDriver driver, WebDriverWait webWait,int rowindex, int chargetypeindex) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//td[6]//li["+rowindex+"]//li["+chargetypeindex+"]/descendant::input)[1]")));		
		return webElement;
		
	}
	
	//To identify the Reprice Button
	public WebElement fn_getTerm(WebDriver driver, WebDriverWait webWait,int rowindex, int chargetypeindex) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//td[7]//li["+rowindex+"]//li["+chargetypeindex+"]//dynamic-field/div)[1]")));		
		return webElement;
		
	}
	
	//To identify the Reprice Button
	public WebElement fn_getMRR(WebDriver driver, WebDriverWait webWait,int rowindex, int chargetypeindex) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//td[12]//li["+rowindex+"]//li["+chargetypeindex+"]//dynamic-field/div)[1]")));		
		return webElement;
		
	}
	
	//To identify the Reprice Button
	public WebElement fn_getGCR(WebDriver driver, WebDriverWait webWait,int rowindex, int chargetypeindex) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//td[13]//li["+rowindex+"]//li["+chargetypeindex+"]//dynamic-field/div)[1]")));		
		return webElement;
		
	}
	
	//To identify the Reprice Button
	public WebElement fn_getNetPrice(WebDriver driver, WebDriverWait webWait,int rowindex, int chargetypeindex) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//td[14]//li["+rowindex+"]//li["+chargetypeindex+"]//dynamic-field/div)[1]")));		
		return webElement;
		
	}
	
	//To identify the Reprice Button
	public WebElement fn_getAdjAmt(WebDriver driver, WebDriverWait webWait,int rowindex, int chargetypeindex) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//td[11]//li["+rowindex+"]//li["+chargetypeindex+"]//dynamic-field/descendant::input)[1]")));		
		return webElement;
		
	}
	
	//To identify the Reprice Button
	public WebElement fn_getAdjTypeSel(WebDriver driver, WebDriverWait webWait,int rowindex, int chargetypeindex) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//td[10]//li["+rowindex+"]//li["+chargetypeindex+"]/descendant::span[@class='ng-binding ng-scope'])[1]")));		
		return webElement;
		
	}
	
	//To identify the Reprice Button
	public WebElement fn_getAdjType(WebDriver driver, WebDriverWait webWait,int rowindex, int chargetypeindex) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//td[10]//li["+rowindex+"]//li["+chargetypeindex+"]/descendant::dynamic-field)[1]")));		
		return webElement;
		
	}
	
	
}