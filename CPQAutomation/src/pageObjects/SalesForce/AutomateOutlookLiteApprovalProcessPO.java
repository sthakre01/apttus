package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AutomateOutlookLiteApprovalProcessPO {
	
public static WebElement webElement = null;
	
	//To identify the Mail List on the Outlook Inbox page
	public WebElement fn_clickAcctLink(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@alt='Work or school account symbol']")));
		return webElement;
	}
	
	//To identify the Submit with Attachments Buttons on Approval Page
	public WebElement fn_setEmail(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='cred_userid_inputtext']")));
		return webElement;
	}
	
	//To identify the SearchBar on the Outlook Inbox page
	public WebElement fn_setSearchBar(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@title='Search']")));
		return webElement;
	}
	
	//To identify the Search button on Outlook Inbox page
	public WebElement fn_clickSearchBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@alt='Search']")));
		return webElement;
	}
	
	//To identify the Inbox table
	public WebElement fn_tblInbox(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@class='lvw']/tbody")));
		return webElement;
	}
}
