package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CatalogPageObjects {
	
public static WebElement webElement = null;
	
	// To identify the ProposalName Field
	public WebElement fn_EnterProposalName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//td/label[text()='Proposal Name']/following::td[1]/div/input")));
		return webElement;
	}
	

}
