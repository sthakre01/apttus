
/*
 Author     		:	Suhas Thakre
 Class Name			: 	Pricing 
 Purpose     		: 	Purpose of this file is :
						1. To identify Page object related to Pricing page for Apttus application.

 Date       		:	06/012/2017 
 Modified Date		:	06/012/2017	
 Modified By		: 	
 Version Information:	Version 1.0
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/
package pageObjects.SalesForce;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ChangeOrder {
	
	
public static WebElement webElement = null;
	
	// To identify the  Remove Confirmation Yes button
	public WebElement fn_clickProductConfigureBtn(WebDriver driver, WebDriverWait webWait, String strProductName) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[text()='"+strProductName+"']/ancestor::tr/descendant::img[@title='Configure']")));
		return webElement;
	}
	
	// To identify the  Remove Confirmation Yes button
	public WebElement fn_clickProductConfigureNABtn(WebDriver driver, WebDriverWait webWait, String strProductName) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@title='"+strProductName+"']/following::i[contains(@href,'configure')][1]")));
		return webElement;
	}
	
	// To identify the  Remove Confirmation Yes button
	public WebElement fn_clickProductRemoveBtn(WebDriver driver, WebDriverWait webWait, String strProductName) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[text()='"+strProductName+"']/ancestor::tr/descendant::img[@alt='Remove']")));
		return webElement;
	}
	
	// To identify the  Remove Confirmation Yes button
	public WebElement fn_clickYesBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Are you sure you want to remove this product from current selection ?']/following::input[@value='Yes']")));
		return webElement;
	}
	
	// To identify the  Remove Confirmation Yes button
	public WebElement fn_clickAddLineItem(WebDriver driver, WebDriverWait webWait, String strLineItemName) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[text()='"+strLineItemName+"']/ancestor::tr[1]/descendant::input[@type='radio' or @type='checkbox']")));
		return webElement;
	}
	
	// To identify the  Remove Confirmation Yes button
	public WebElement fn_clickLineItemNA(WebDriver driver, WebDriverWait webWait, String strLineItemName) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='"+strLineItemName+"']")));
		return webElement;
	}
	
	// To identify the  Remove Confirmation Yes button
	public WebElement fn_clickAddLineItemNA(WebDriver driver, WebDriverWait webWait, String strLineItemName) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='"+strLineItemName+"']/preceding::input[@type='checkbox' or @type='radio'][1]/..")));
		return webElement;
	}
	
	// To identify the  Remove Confirmation Yes button
	public WebElement fn_setLineItemAttribute(WebDriver driver, WebDriverWait webWait, String strLineItemName, String strAttribute) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[text()='"+strLineItemName+"']/ancestor::tr[1]/descendant::span[text()='"+strAttribute+"']/following::input[1]")));
		return webElement;
	}
	
	// To identify the  Remove Confirmation Yes button
	public WebElement fn_clickImageAttributes(WebDriver driver, WebDriverWait webWait, String strLineItemName) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[text()='"+strLineItemName+"']/ancestor::tr[1]/descendant::img[contains(@src,'apttus_config2__Image_Attributes')]")));
		return webElement;
	}
	
	// To identify the  Remove Confirmation Yes button
	public WebElement fn_clickQuickSaveBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Quick Save']")));
		return webElement;
	}
	
	//To Select Value for lastname
	public Select fn_selectImgAttribute(WebDriver driver, WebDriverWait webWait, String strAttribute) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[normalize-space(text())='"+strAttribute+"']/following::select"))));
		return webList;
	}
	
	// To identify the  Remove Confirmation Yes button
	public WebElement fn_setLineItemAttributeNA(WebDriver driver, WebDriverWait webWait, String strLineItemName, String strAttribute) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='"+strLineItemName+"']/following::input[1]")));
		return webElement;
	}

	// To Select the Record type of new Record
	public Select fn_selectAdjustmentType(WebDriver driver, WebDriverWait webWait, String strProductName) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("(//a[text()='"+strProductName+"']/ancestor::tr)[1]/td[13]/descendant::select"))));
		return webList;
	}
	
	// To Select the Record type of new Record
	public WebElement fn_clickAdjustmentTypeNA(WebDriver driver, WebDriverWait webWait, String strProductName) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@title='"+strProductName+"']/following::td[9]//ancestor::a[@aria-label='Select box select']")));
		return webElement;
	}
	
	// To Select the Record type of new Record
	public WebElement fn_selectAdjustmentTypeNA(WebDriver driver, WebDriverWait webWait, String strAdjustmentType) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='"+strAdjustmentType+"']")));
		return webElement;
	}
		
	
	
	// To identify the  Remove Confirmation Yes button
	public WebElement fn_setAdjustmentAmount(WebDriver driver, WebDriverWait webWait, String strProductName) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//a[text()='"+strProductName+"']/ancestor::tr)[1]/td[14]/descendant::input")));
		return webElement;
	}
	
	// To identify the  Remove Confirmation Yes button
	public By fn_textUpdatingPrice(WebDriver driver, WebDriverWait webWait, String strProductName) throws InterruptedException
	{
		By bySpinnerLocator = By.xpath("//div[text()='Updating Price, please wait...']");
		return bySpinnerLocator;
	}
	
	// To identify the  Remove Confirmation Yes button
	public By fn_imgloading(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		By bySpinnerLocator = By.xpath("//img[@src='/img/loading.gif']");
		return bySpinnerLocator;
	}
	
	// To identify the  Remove Confirmation Yes button
	public WebElement fn_setAdjustmentAmountNA(WebDriver driver, WebDriverWait webWait, String strProductName) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[@title='"+strProductName+"']/following::td[10]//ancestor::p/input")));
		return webElement;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}