package pageObjects.SalesForce;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import pageObjects.BasePageObject;
import pageObjects.SalesForce.OpportunitesFormPageObject;

/**
 * Created by joe.gannon on 2/10/2016.
 */
public class OpportunitesPageObject extends BasePageObject
{
	public static WebElement webElement = null;
    public static By opportunityTab = By.id("Opportunity_Tab");
    public static By newOpportunityBtn = By.name("new");
    public static By continueBtn = By.xpath("//*[@id=\"bottomButtonRow\"]/input[1]");
    public static void createNewOpportunity(WebDriver driver)
    {
        driver.findElement(opportunityTab).click();
        waitForElement(driver,newOpportunityBtn);
        driver.findElement(newOpportunityBtn).click();
        waitForElement(driver,continueBtn);
        driver.findElement(continueBtn);
        Assert.assertTrue(OpportunitesFormPageObject.isLoaded(driver));

    }
    //to get ProposalNumber
    public WebElement fn_clickProposalNumber(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//th[text()='Proposal ID']/../following::tr[1]/th/a")));
		return webElement;
	}
    
    // To identify the Save Button
 	public WebElement fn_getOpportunityName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
 	{
 		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Opportunity Name']//following::div[1]")));
 		return webElement;
 	}
 	
 	// To identify the Save Button
  	public WebElement fn_clickTerminatebtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
  	{
  		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.name("terminate")));
  		return webElement;
  	}
  	
  	//to Edit the customer reference  
  	public List<WebElement> fn_getEditCRLinks(WebDriver driver, WebDriverWait webWait) throws InterruptedException
  	{
  		List<WebElement> lstwebElement = driver.findElements(By.xpath("//h3[contains(text(),'Products')]/following::tbody[1]//child::a[text()='Edit']"));
  		return lstwebElement;
  	}
  	
  //to click the Tax Exempt Certificate Number
  	public WebElement fn_getEditCRLink(WebDriver driver, WebDriverWait webWait) throws InterruptedException
  	{
  		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h3[contains(text(),'Products')]/following::tbody[1]//child::a[text()='Edit']")));
  		return webElement;
  	}
  	
}
