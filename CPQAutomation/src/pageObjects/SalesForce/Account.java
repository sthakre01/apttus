
/*
 Author     		:	Suhas Thakre
 Class Name			: 	Account 
 Purpose     		: 	Purpose of this file is :
						1. To identify Page object related to account page for Apttus application.

 Date       		:	06/012/2017 
 Modified Date		:	06/012/2017	
 Modified By		: 	
 Version Information:	Version 1.0
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/
package pageObjects.SalesForce;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Account {
	
	
public static WebElement webElement = null;
	
	// To identify the EnterSearchItem
	public WebElement fn_setSearchItem(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.partialLinkText("searchItem")));
		return webElement;
	}

	// To identify the Search Field
	public WebElement fn_setSearchField(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.id("phSearchInput")));
		return webElement;
	}

	// To Click on SearchFeild Button
	public WebElement fn_clickSearchBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.id("phSearchButton")));
		return webElement;
	}

	//To click on CustomerReferences Tab
	public WebElement fn_clickCustomerReferenceTab(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//ul/li/a[@title='Customer References Tab']")));
		return webElement;
	}
	
	//To click on New Button on Customer References tab
	public WebElement fn_clickNewCustRefOnTab(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@title='New']")));
		return webElement;
	}
	
	//To click on NewCustomereReference Button
	public WebElement fn_clickNewCustReferenceBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@value='New Customer Reference']")));
		return webElement;
	}
	
	
	//To Enter Tier Name on Customer Reference page; 0 - Dev Instance, 1 - SandBox Instance.
	public WebElement fn_setCustomerReferenceName(WebDriver driver, WebDriverWait webWait, Integer intCustRefName) throws InterruptedException
	{
		//based on the environment, the XPath will be generated
		String strLabel = "";
		if(intCustRefName == 0)
			strLabel = "Customer Reference Name";
		else
			strLabel = "Tier Name";
		
		String strXPath = "//td/label[text()='"+strLabel+"']//following::td[1]/div/input";
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(strXPath)));
		return webElement;
	}
	
	//To Enter Tier Name on Customer Reference page
	public WebElement fn_clickSaveCustRefPage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@name='save' and @type='submit']")));
		return webElement;
	}
	
	//To Select the Currency Code on Customer Reference page
	public Select fn_selCurrency(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath("//td/label[text()='Currency']//following::td[1]/div/select")));
		return webList;
	}
	
	//To find the Customer Reference Object
	public WebElement fn_getCustomerReferenceTable(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//input[@value='New Customer Reference']//following::table[1]/tbody/tr/th")));
		return webElement;
	}
	
	// To identify the Offering Management Button
	public WebElement fn_clickOfferingManagementBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@title='Offering Management' or @value='Offering Management']")));
		return webElement;
	}
	
	// To identify the Offering Management Button
	public WebElement fn_clickUpgDwgOpptyBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@title='Create Renewal / Upg-Dwg Oppty']")));
		return webElement;
	}
	
	// To identify the Offering Management Button
	public WebElement fn_clickOpportunityName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h3[text()='Opportunities']//following::tbody[1]/tr[2]/th/a")));
		return webElement;
	}
	
	
	// To identify the Offering Management Button
	public WebElement fn_GetCustReference(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h3[text()='Customer References']//following::tbody[1]/tr[2]/th/a")));
		return webElement;
	}
	
	
	// To identify the Offering Management Button
	public WebElement fn_GetActiveContacts(WebDriver driver, WebDriverWait webWait, String strContacts) throws InterruptedException
	{
		//Active Contacts
		//Contacts
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h3[text()='"+strContacts+"']//following::tbody[1]/tr[2]/th/a")));
		return webElement;
	}
	
	// To identify the Offering Management Button
	public WebElement fn_GetOrderAgreement(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h3[text()='Agreements (Account)']//following::a[contains(text(),'Order Form')]")));
		return webElement;
	}
	
	
	// To identify the Offering Management Button
	public List<WebElement> fn_GetLineItems(WebDriver driver, WebDriverWait webWait,String strOrderAgreement) throws InterruptedException
	{
		List<WebElement> webElements = driver.findElements(By.xpath("//h3[text()='Asset Line Items (Sold To)']/following::tbody[1]//child::a[text()='"+strOrderAgreement+"']/../../td[3]"));
		return webElements;
	}
	
	// To identify the Offering Management Button
	public List<WebElement> fn_GetLineItemsOffering(WebDriver driver, WebDriverWait webWait,String strOfferingName) throws InterruptedException
	{
		List<WebElement> webElements = driver.findElements(By.xpath("//h3[text()='Asset Line Items (Sold To)']/following::tbody[1]//child::td[contains(text(),'"+strOfferingName+"')]/../td[2]"));
		return webElements;
	}
	
// To identify the Offering Management Button
	public WebElement fn_ClickShowMoreLineItems(WebDriver driver, WebDriverWait webWait,String strOrderAgreement) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h3[text()='Asset Line Items (Sold To)']//following::a[contains(text(),'Show')]")));
		return webElement;
	}
	
	
}
