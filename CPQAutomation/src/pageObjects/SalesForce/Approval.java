
/*
 Author     		:	Suhas Thakre
 Class Name			: 	Approval 
 Purpose     		: 	Purpose of this file is :
						1. To identify Page object related to Approval page for Apttus application.

 Date       		:	06/012/2017 
 Modified Date		:	06/012/2017	
 Modified By		: 	
 Version Information:	Version 1.0
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/
package pageObjects.SalesForce;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Approval {
	
	public static WebElement webElement = null;
	
	
	//To identify the Submit with Attachments Buttons on Approval Page
	public WebElement fn_clickSubmitWthAttchmentsBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value=\"Submit (With Attachments)\"]")));
		return webElement;
	}
	
	//To select the Attachments and submit them fo further approval process
	public WebElement fn_clickSelect(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//th/div[text()=\"System Modstamp\"]//following::input")));
		return webElement;
	}

	//To Click on Submit on Select Attachments Page
	public WebElement fn_clickSubmit(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value=\"Submit\"]")));
		return webElement;
	}
	
	//To get the message text element
	public WebElement fn_getMsgText(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/div[@class=\"messageText\"]")));
		return webElement;
	}
	
	//To click on the Return Button
	public WebElement fn_clickyesBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Yes']")));
		return webElement;
	}
	
	//To click on the Return Button
	public WebElement fn_clickReturnBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value=\"Return\"]")));
		return webElement;
	}
}
