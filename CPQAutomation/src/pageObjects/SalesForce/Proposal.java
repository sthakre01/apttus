
/*
 Author     		:	Suhas Thakre
 Class Name			: 	Proposal 
 Purpose     		: 	Purpose of this file is :
						1. To identify Page object related to Proposal page for Apttus application.

 Date       		:	06/012/2017 
 Modified Date		:	06/012/2017	
 Modified By		: 	
 Version Information:	Version 1.0
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/
package pageObjects.SalesForce;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Proposal {
	
public static WebElement webElement = null;
	
	

	// To identify the ProposalName Field
	public WebElement fn_clickProposalButton(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Create Quote/Proposal']")));
		return webElement;
	}

    // To identify the ProposalName Field
	public WebElement fn_setProposalName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/label[text()='Proposal Name']/following::td[1]/div/input")));
		return webElement;
	}
	
	// To identify the PrimaryContact Field
	public WebElement fn_setPrimaryContact(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/label[text()='Primary Contact']/following::td[1]/span/input")));
		return webElement;
	}
	
	

	// To identify the PriceList Field
	public WebElement fn_setPriceList(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/label[text()='Price List']/following::td[1]/div/span/input")));
		return webElement;
	}
	
	// To identify the PriceList Field
	public WebElement fn_setEntityName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label[text()='Sungard Entity Name']/following::span/input)[1]")));
		return webElement;
	}
	
	// To identify the Save Button
	public WebElement fn_clickSaveButton(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.name("save")));
		return webElement;
	}
	
	// To identify the Save Button
	public WebElement fn_getProposalNumber(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Proposal ID']//following::div[@id='Name_ileinner']")));
		return webElement;
	}
	
	// To get Proposal Type
	public WebElement fn_getProposalType(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Proposal Type']//following::div[1]")));
		return webElement;
	}
	
	// To identify the New Offering Data Button
	public WebElement fn_clickNewOfferingDataButton(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@title='New Offering Data']")));
		return webElement;
	}
	
	// To identify the New Offering 
	public WebElement fn_getOfferingName(WebDriver driver, WebDriverWait webWait, String strOfferingName) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(" //h3[text()='Offering Data']//following::tbody[1]/tr/th/a[contains(text(),'"+strOfferingName+"')]")));
		return webElement;
	}
	
	// To identify the New Offering 
	public List<WebElement> fn_getOfferingNames(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		List<WebElement> webElements = driver.findElements(By.xpath("//h3[text()='Offering Data']//following::tbody[1]/tr/th/a[1]"));
		return webElements;
	}

	// To identify the New Offering Data Button
	public WebElement fn_clickAgreementsLink(WebDriver driver, WebDriverWait webWait, String strAgreementName) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h3[text()='Agreements']//following::tbody[1]/tr/th/a[contains(text(),'"+strAgreementName+"')]")));
		return webElement;
	}
	
	// To identify the New Offering Data Button
	public WebElement fn_clickMyApprovalsbtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='My Approvals']")));
		return webElement;
	}
	
	// To identify the New Offering Data Button
	public WebElement fn_clickApprovebtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Approve']")));
		return webElement;
	}
	
	// To identify the New Offering Data Button
	public WebElement fn_setApproveComment(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//label[normalize-space(text())='Add a comment (optional)']/following::textarea)[1]")));
		return webElement;
	}
	
	// To identify the Save Button
	public WebElement fn_clickSaveCommentbtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@value='Save'])[1]")));
		return webElement;
	}
	
	
	// To identify the New Offering Data Button
	public WebElement fn_clickAllApprovalsbtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='All Approvals']")));
		return webElement;
	}
	
	// To identify the New Offering Data Button
	public WebElement fn_clickReturnbtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Return']")));
		return webElement;
	}
	
	// To identify the New Offering Data Button
	public List<WebElement> fn_getApprovalscheckbox(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		List<WebElement> webElements = driver.findElements(By.xpath("//input[@type='checkbox']"));
		return webElements;
	}
	
	
	public WebElement fn_verifyApprovalStatus(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Approval Status']//following::div[1]")));
		return webElement;
	}
	
	
	public WebElement fn_verifyStatus(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Status']//following::div[1]")));
		return webElement;
	}
	
	
	
	
	
}
