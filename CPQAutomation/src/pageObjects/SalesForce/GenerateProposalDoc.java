
/*
 Author     		:	Suhas Thakre
 Class Name			: 	GenerateProposalDoc 
 Purpose     		: 	Purpose of this file is :
						1. To identify Page object related to Generate Proposal Doc page for Apttus application.

 Date       		:	06/012/2017 
 Modified Date		:	06/012/2017	
 Modified By		: 	
 Version Information:	Version 1.0
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/
package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class GenerateProposalDoc {
	
	public static WebElement webElement = null;
	
	// To Click on Generate Button on Proposal Page
	public WebElement fn_clickGenerateBdgtPrp(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[contains(@alt, 'Generate Budgetary Proposal')]")));
		return webElement;
	}

	//To Click on the webElemention of Create Budgetary Proposal - To Present Doc
	public WebElement fn_selProposalDocument(WebDriver driver, WebDriverWait webWait,String strDocName) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/span[text()='"+strDocName+"']/preceding::td[1]/span/input")));
		return webElement;
	}
	
	//To Click on Generate Button
	public WebElement fn_clickGenerateBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[@class='pbButton']/span/input[@value='Generate']")));
		return webElement;
	}
	
	//To Click on LInk Click to View the file
	public WebElement fn_clickLinkViewFile(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText("Click here to view this file")));
		return webElement;
	}
	
	//To Click on Return Button
	public WebElement fn_ClickReturnBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@value='Return']")));
		return webElement;
	}
	
	//To Click on Return Button
	public WebElement fn_selDocumentType(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@value=\"DOC\"]")));
		return webElement;
	}
	
	//To select the Proposal data table
	public WebElement fn_getProposalDataTable(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@class=\"detailList\"]")));
		return webElement;
	}
	
	// To Click on Reject Proposal Button on Proposal Page
	public WebElement fn_clickRejectProposal(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[contains(@alt, 'Reject Proposal')]")));
		return webElement;
	}
}
