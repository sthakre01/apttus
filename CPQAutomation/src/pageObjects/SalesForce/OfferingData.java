
/*
 Author     		:	Suhas Thakre
 Class Name			: 	OfferingData 
 Purpose     		: 	Purpose of this file is :
						1. To identify Page object related to Offering Data page for Apttus application.

 Date       		:	06/012/2017 
 Modified Date		:	06/012/2017	
 Modified By		: 	
 Version Information:	Version 1.0
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/
package pageObjects.SalesForce;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class OfferingData {
	
			
	public static WebElement webElement = null;
	
	// To Edit the existing offering data link
	public WebElement fn_clickExistingOfferingDataLink(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//th[text()='Term']//following::a[1]")));
		return webElement;
	}
	
	public WebElement fn_clickExistingMulOfferingLink(WebDriver driver, WebDriverWait webWait, String strOfferingName) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//a[text()='"+strOfferingName+"'])[1]")));
		return webElement;
	}
	
	//Click Edit Button
	public WebElement fn_clickEditbtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//input[@title='Edit'])[1]")));
		return webElement;
	}
		
	// To identify the ServiceName Field
	public WebElement fn_clickNewOfferData(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@title='New Offering Data']")));
		return webElement;
	}

	// To click on the Image ofLook uP window of customer Reference	
	public WebElement fn_clickLookUpWindow(WebDriver driver, WebDriverWait webWait,String WindowName) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='"+WindowName+"']//following::img[1]")));
		return webElement;
	}
	
	// To click on the Go Button
	public WebElement fn_clickGoBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@title=\"Go!\"]")));
		return webElement;
	}
	
	// To identify the OfferingaDataFeild Name
	public WebElement fn_setOfferingDataName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Offering Data Name']//following::input[1]")));
		return webElement;
	}
	
	// To identify the OfferingaDataFeild Name
	public WebElement fn_setCustomerReference(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Customer Reference']/following::span[1]/input")));
		return webElement;
	}
	
	// To click on the Go Button
	public WebElement fn_clickSrchItm(WebDriver driver, WebDriverWait webWait,String srchItem) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.partialLinkText("")));
		return webElement;
	}
	
	// To Enter the Billing Date
	public WebElement fn_setBillingDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Billing Date']//following::input[1]")));
		return webElement;
	}
	
	//To Enter The AutoRenewalTerm
	public WebElement fn_setAutoRenewalTerm(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Auto-Renew Term']//following::input[1]")));
		return webElement;
	}
	
	// To Enter the Term
	public WebElement fn_setTerm(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Offering Data Name']//following::input[1]")));
		return webElement;	}
	
	// To Enter the StartDate
	public WebElement fn_setStartDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Start Date']//following::a[1]")));
		return webElement;
	}
	
	// To Enter the StartDate
	public WebElement fn_setOfferingStartDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Start Date']/following::input[1]")));
		return webElement;
	}
	
	// To Enter the StartDate
	public WebElement fn_setOfferingChangeStartDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Change Start Date']/following::input[1]")));
		return webElement;
	}
	
	// To Enter the StartDate
	public WebElement fn_setOfferingChangeBillDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Change Bill Date']/following::input[1]")));
		return webElement;
	}
	
	// To Enter the FutureDate
	public WebElement fn_setEndDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/th/label[contains(text(),'End Date')]//following::input[1]")));	
		return webElement;
	}
	
	// To Enter the StartDate
	public WebElement fn_setOfferingEndDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='End Date']/following::input[1]")));
		return webElement;
	}
	
	// To Enter the StartDate
	public WebElement fn_setOfferingBillingDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Billing Date']/following::input[1]")));
		return webElement;
	}
	
	// To Enter the StartDate
	public WebElement fn_setChangeRenewTerm(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Auto-Renew Term']/following::input[1]")));
		return webElement;
	}
	
	// To Enter the StartDate
	public WebElement fn_setChangeTerm(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Term']/following::input[1]")));
		return webElement;
	}
	
	
	// To Enter the FutureDate
	public WebElement fn_setExpirationDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[contains(text(),'Expiration')]//following::input[1]")));
		return webElement;
	}

	// To Enter the AnnualPriceIncrease
	public WebElement fn_setAnnualPI(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/th/label[contains(text(),'Annual Price Increase')]//following::td[1]/input")));
		return webElement;
	}
	
	// To Select the Billing Contact
	public Select fn_selBillingContact(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath("//tr/td[contains(text(),'Bill To Contact')]//following::td[1]/select")));
		return webList;
	}
	
	// To Select the Notification Contact
	public Select fn_selNotificationContact(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList =  new Select(driver.findElement(By.xpath("//tr/td[contains(text(),'Notification Contact')]//following::td[1]/select")));
		return webList;
	}
	
	// To Select the Covered Location
	public WebElement fn_selCoveredLocation(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/th/div[contains(text(),'Select')]//following::td[1]/input")));
		return webElement;
	}
	
	// To Click on Save Button
	public WebElement fn_clickSaveBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td/input[@title='Save']")));
		return webElement;
	}
	
	// To Click on Search Button on Popup window
	public WebElement fn_clickWindowSearchBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"lksrch\"]")));
		return webElement;
	}
	
	// To Click on Go Button on Popup window
	public WebElement fn_clickWindowGoBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/input[@title=\"Go!\"]")));
		return webElement;
	}
	
	// To get the link text on popup window
	public WebElement fn_clickWindowLinkText(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@class='list']/tbody/tr[2]/th/a")));
		return webElement;
	}
	
	// To Select the Billing Frequency
	public Select fn_selBillingFrequency(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList =  new Select(driver.findElement(By.xpath("//label[text()='Billing Frequency']//following::select[1]")));
		return webList;
	}
	
	// To Select the Annual Increase PL
	public Select fn_selAnnualIncreasePL(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList =  new Select(driver.findElement(By.xpath("//label[text()='Annual Price Increase PL']//following::select[1]")));
		return webList;
	}
	
	// To Click on Create Upg/Dwg Button
	public WebElement fn_clickEarlyTerminateBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Early Terminate']")));
		return webElement;
	}
	
	// To Click on Create Upg/Dwg Button
	public WebElement fn_clickRemoveAutoRenewBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Remove Auto Renew']")));
		return webElement;
	}
		
		// To Click on Create Upg/Dwg Button
		public WebElement fn_clickCreateUpgDwgBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
		{
			webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Create Upg/Dwg']")));
			return webElement;
		}
	
	// To Click on Create Upg/Dwg Button
	public WebElement fn_clickCreateQuotechkbox(WebDriver driver, WebDriverWait webWait, String strStatus) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='"+strStatus+"' and @type='radio']")));
		return webElement;
	}
	
	public WebElement fn_clickCreateRenewalBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Create Renewal' and @type='button']")));
		return webElement;
	}
	
	public WebElement fn_clickCreateRenewalsbt(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Create Renewal' and @type='submit']")));
		return webElement;
	}
		
	// To Click on ConfirmUpgDwg Button
	public WebElement fn_clickConfirmUpgDwgBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[normalize-space(@value)='Create Upg/Dwg'   and contains(@name,'ConfirmUpgDwg')]")));
		return webElement;
	}
	
	// To Select the Covered Location
	public WebElement fn_clickEditOfferingBtn(WebDriver driver, WebDriverWait webWait, String strOfferingName) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[contains(text(),'"+strOfferingName+"')]/following::input[@value='Edit Offering']")));
		return webElement;
	}
	
	// To Select the Covered Location
	public WebElement fn_clickEditOffBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Edit Offering']")));
		return webElement;
	}
	
	
	// To Select the Covered Location
	public WebElement fn_clickCloneBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@title='Clone']")));
		return webElement;
	}
	
}
