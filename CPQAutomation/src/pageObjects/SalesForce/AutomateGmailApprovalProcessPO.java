package pageObjects.SalesForce;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AutomateGmailApprovalProcessPO {
	
public static WebElement webElement = null;
	
	//To identify the Submit with Attachments Buttons on Approval Page
	public WebElement fn_setEmail(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		//webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='Email']")));
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.id("Email")));
		return webElement;
	}
	
	//To identify the Next button on Gmail login page
	public WebElement fn_clickNext(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		//webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='next']")));
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.id("next")));
		return webElement;
	}

	//To identify the pwd text feild on Gmail login page
	public WebElement fn_setPwd(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		//webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='Passwd']")));
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.id("Passwd")));
		return webElement;
	}
	
	//To identify the SignIn button on Gmail login page
	public WebElement fn_clickSignIn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		//webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='signIn']")));
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.id("signIn")));
		return webElement;
	}
	
	//To identify the SearchBar on the Gmail Inbox page
	public WebElement fn_setSearchBar(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='q']")));
		return webElement;
	}
	
	//To identify the Search button on Gmail Inbox page
	public WebElement fn_clickSearchBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@id='gbqfb']")));
		return webElement;
	}
	
	//To identify the Search button on Gmail Inbox page
	public WebElement fn_clickGoogleApps(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/a[@title='Google apps']")));
		return webElement;
	}
	
	//To identify the Inbox table on Gmail Inbox page
	public WebElement fn_tblInbox(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@role='main']//div[@class='Cp']//tbody")));
		return webElement;
	}
	
	//To identify the Reply Button in Inbox table on Gmail Inbox page
	public WebElement fn_clickReplyBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@data-tooltip='Reply']")));
		return webElement;
	}
	
	//To identify the SearchBar on the Gmail Inbox page
	public WebElement fn_setMsgBody(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@aria-label='Message Body']")));
		return webElement;
	}
	
	//To identify the Reply Button in Inbox table on Gmail Inbox page
	public WebElement fn_clickSendBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[text()='Send']")));
		return webElement;
	}
	
	//To identify the Reply Button in Inbox table on Gmail Inbox page
	public WebElement fn_clickBackTosrchRsltsBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@data-tooltip='Back to Search Results' and @role='button']")));
		return webElement;
	}
	
	//To identify the Reply Button in Inbox table on Gmail Inbox page
	public WebElement fn_getEmailCount(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@data-tooltip='Newer']/preceding-sibling::div[@aria-disabled='false']")));
		return webElement;
	}
	
	//To identify the Older button in email list
	public WebElement fn_clickOlderBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[2][@aria-label='Older']")));
		return webElement;
	}
	
	//To identify the Inbox folder in Gmail
	public WebElement fn_clickInboxLink(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span/a[@target='_top']")));
		return webElement;
	}
	
	//To identify the Older button in email list
	public WebElement fn_clickSignoutImg(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/a/span[@class='gb_8a gbii']")));
		return webElement;
	}
	
	//To identify the Older button in email list
	public WebElement fn_clickSignoutBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/a[text()='Sign out']")));
		return webElement;
	}
	
	//To identify the Click the Review button inside the email
	public WebElement fn_clickReviewBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("(//span[text()='REVIEW'])[last()]")));
		return webElement;
	}
	
	//To identify the Agree for records and esignatures
	public WebElement fn_clickIAcceptBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='disclosureAccepted']")));
		return webElement;
	}
	
	//To click on agree and accept checkbox
	public WebElement fn_clickAgreeAndAccept(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@id='disclosureAccepted']")));
		return webElement;
	}
	
	//To click on NavigateBtn for GMSA Docusign process
	public WebElement fn_clickNavigateBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@id='navigate-btn']")));
		return webElement;
	}
	
	//To click on Sign button for GMSA Docusign process
	public WebElement fn_clickSignBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[name()='svg']/*[name()='polygon']")));
		return webElement;
	}
	
	//To click on continue button after accepting the terms and conditions on Docusign process
	public WebElement fn_clickContinueBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@id='action-bar-btn-continue']")));
		return webElement;
	}
	
	
	//To click on continue button after accepting the terms and conditions on Docusign process
	public WebElement fn_clickDragFrom(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@id='btnSignHere']")));
		return webElement;
	}
	
	//To click on continue button after accepting the terms and conditions on Docusign process
	public WebElement fn_clickDragTo(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[starts-with(@id,'ds_hldrBdy_pageimg')]")));
		return webElement;
	}
	
	
	//To click on Adopt and Sign button after dragging the Docusign to the main page
	public WebElement fn_clickAdoptandSign(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Adopt and Sign' and  @value='signature']")));
		return webElement;
	}
	
	//To click on Finish button
	public WebElement fn_clickFinish(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{	
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[text()='Finish']")));
		//webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@id='action-bar-bottom-btn-finish']")));
		return webElement;
	}
	
}
