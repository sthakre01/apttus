package pageObjects.SalesForce;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LeadCreationObjects {
	
	public static WebElement webElement = null;
		
	// To identify the Leads Tab
	public WebElement fn_selectLeadsTab(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath(("//ul/li/a[@title=\"Leads Tab\"]"))));
		return webElement;
	}
   
	//To click on New Button
	public WebElement fn_clickNew(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td/input[@title=\"New\"]")));
		return webElement;
	}
	
	// To Select the Record type of new Record
	public Select fn_SelectRecord(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[contains(text(),\"Record Type of new record\")]//following::select[1]"))));
		return webList;
	}
   
	//To click on Continue Button
	public WebElement fn_clickContinuebtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td/input[@value=\"Continue\"]")));
		return webElement;
	}
	
	//To Select Value for lastname
	public Select fn_selectFirstName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[contains(text(),'First Name')]//following::select[1]"))));
		return webList;
	}
	
	//To Select Value for lastname
	public Select fn_selQuoteType(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[text()='Quote Type']//following::select[1]"))));
		return webList;
	}
	
	//To enter input for lastname
	public WebElement fn_setFirstName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[contains(text(),'First Name')]//following::input[1]")));
		return webElement;
	}
	
	//To enter input for lastname
	public WebElement fn_setContactAcctName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Account Name']//following::span[1]/input")));
		return webElement;
	}
	
	//To enter input for lastname
	public WebElement fn_setLastName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[contains(text(),'Last Name')]//following::td[1]/div/input")));
		return webElement;
	}
	//To enter input for Company
	public WebElement fn_setCompany(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[contains(text(),'Company')]//following::td[1]/div/input")));
		return webElement;
	}
	//To enter input for Phone
	public WebElement fn_setPhone(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[contains(text(),'Phone')]//following::input[1]")));
		return webElement;
	}
	
	//To Select the Value for Country
	public Select fn_selectCountry(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath("//label[text()=\"Country\"]//following::select[1]")));
		return webList;
	}
	
	// To Select the LeadCurrency
	public Select fn_SelectLeadCurrency(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[contains(text(),'Lead Currency')]//following::td[1]/div/select"))));
		return webList;
	}
	
	// To Select the Lead Status
	public Select fn_selectLeadStatus(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[contains(text(),'Lead Status')]//following::td[1]/div/span/select"))));
		return webList;
	}
	
	// To Select the Lead Status
	public Select fn_selectLeadStage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[contains(text(),'Lead Stage')]//following::select[1]"))));
		return webList;
	}
	
	// To Select the Lead Status
	public Select fn_selectLeadTactic(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[contains(text(),'Lead Tactic')]//following::select[1]"))));
		return webList;
	}
	
	// To Select the Approval Status
	public Select fn_selecApprovalStatus(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[contains(text(),'Approval Status')]//following::select[1]"))));
		return webList;
	}
	
	// To Select the Lead Stage
	public Select fn_LeadStage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[contains(text(),'Lead Stage')]//following::td[1]/span/span/select"))));
		return webList;
	}
	
	// To Select the Account Type
	public Select fn_selectAcctType(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[contains(text(),'Account Type')]//following::td[1]/div/span/select"))));
		return webList;
	}
	
	// To Select the Account Type
	public Select fn_selDataSource(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[text()='Data Source']/following::select[1]"))));
		return webList;
	}
	
	// To Select the Account Type
	public Select fn_selGroupemployee(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[text()='Group employee']/following::select[1]"))));
		return webList;
	}
	
	//to enter the contactStreet Address
	public WebElement fn_setAnnualRevenue(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Annual Revenue']//following::input[1]")));
		return webElement;
	}
	
	// To Select the Industry 
	public Select fn_selectIndustry(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[text()='Industry']/following::select[1]"))));
		return webList;
	}
	
	// To Select the AcctName 
	public Select fn_selectAccountName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//tr/td[text()='Account Name']//following::td[1]/div/select"))));
		return webList;
	}
	
	//To click on Save Button
	public WebElement fn_clickSaveBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@name='save']")));
		return webElement;
	}
	
	//To click on Save Button
	public WebElement fn_clickEditBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@name='edit']")));
		return webElement;
	}
	
	//To click on Lead Convert Button
	public WebElement fn_clickLeadConvertBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td/input[@name=\"convert\"]")));
		return webElement;
	}
	
	//to identify the sungard entity name
	public WebElement fn_getEntityName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Sungard Entity Name']//following::a")));
		return webElement;
	}
	
	//to click the Street Address
	public WebElement fn_clickStreetAdress(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Street Address']//following::td[1]")));
		return webElement;
	}
	
	//to enter the Street Address
	public WebElement fn_setStreetAdress(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Billing Street']/following::textarea[1]")));
		return webElement;
	}
	
	//to enter the Street Address
	public WebElement fn_setStreetAdressIRL(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Street Address']/following::textarea[1]")));
		return webElement;
	}
	
	//to click on OK button on the street/address window
	public WebElement fn_clickOkBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='OK']")));
		return webElement;
	}
	
	//to click the city field
	public WebElement fn_clickcity(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='City / Town']//following::td[1]")));
		return webElement;
	}
	
	//to set the city field
	public WebElement fn_setcity(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Billing City']/following::input[1]")));
		return webElement;
	}
	
	//to set the city field
	public WebElement fn_setcityIRL(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='City / Town']/following::input[1]")));
		return webElement;
	}
	
	//to set the city field
	public WebElement fn_setContactcityIRL(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='City']/following::input[1]")));
		return webElement;
	}
	
	//to set the city field
	public WebElement fn_setState(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Billing State/Province']/following::input[1]")));
		return webElement;
	}
	
	// To To Select the Contact Country
	public Select fn_selStateIRL(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[text()='County']//following::select[1]"))));
		return webList;
	}
	
	
	
	//to click the County field
	public WebElement fn_clickcounty(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='County']//following::td[1]")));
		return webElement;
	}
	
	//to enter the value for County field
	public WebElement fn_setcounty(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Billing Country']/following::input[1]")));
		return webElement;
	}
	
	//to click the PostCode
	public WebElement fn_clickPostCode(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Post Code']//following::td[1]")));
		return webElement;
	}
	
	//to set the value for PostCode
	public WebElement fn_setPostCode(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Billing Zip/Postal Code']/following::input[1]")));
		return webElement;
	}
	
	//to set the value for PostCode
	public WebElement fn_setPostCodeIRL(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Post Code']/following::input[1]")));
		return webElement;
	}
	
	//to enter the contactStreet Address
	public WebElement fn_setCntactStreetAdress(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Mailing Street']//following::textarea[1]")));
		return webElement;
	}
	
	//to enter the contactStreet Address
	public WebElement fn_setLeadStreetAdress(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Street']//following::textarea[1]")));
		return webElement;
	}
	
	//to enter the contactStreet Address
	public WebElement fn_setLeadStreetAdressUK(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Street Address']//following::textarea[1]")));
		return webElement;
	}
	
	//to enter the contactStreet Address
	public WebElement fn_setCntactDecisionRole(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Decision Role']//following::select[1]")));
		return webElement;
	}
	
	//to enter the contact city
	public WebElement fn_setCntactCity(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Mailing City']//following::input[1]")));
		return webElement;
	}
	
	//to enter the contact city
	public WebElement fn_setLeadCity(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='City']//following::input[1]")));
		return webElement;
	}
	
	//to enter the contact city
	public WebElement fn_setLeadState(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='State/Province']//following::input[1]")));
		return webElement;
	}
	
	
	// To To Select the Contact Country
	public Select fn_selLeadStateUK(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[text()='County']//following::select[1]"))));
		return webList;
	}
	
	
	
	//to enter the contact Postal code
	public WebElement fn_setCntactPostalCode(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Mailing Zip/Postal Code']//following::input[1]")));
		return webElement;
	}
	
	//to enter the contact Postal code
	public WebElement fn_setLeadPostalCode(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Zip/Postal Code']//following::input[1]")));
		return webElement;
	}
	
	
	//to enter the contact Postal code
	public WebElement fn_setLeadPostalCodeUK(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Post Code']//following::input[1]")));
		return webElement;
	}
	
	// To Select the Contact County
	public Select fn_selectCntactCounty(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[text()='Mailing Country']//following::select[1]"))));
		return webList;
	}
	
	
	
	// To To Select the Contact Country
	public Select fn_selectCntactCountry(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[text()='Country']//following::select[1]"))));
		return webList;
	}
	
	
	public WebElement fn_setCntactCountry(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Mailing Country']//following::input[1]")));
		return webElement;
	}
	
	public WebElement fn_setLeadCountry(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Country']//following::input[1]")));
		return webElement;
	}
	
	// To To Select the Contact Country
	public Select fn_selLeadCountryUK(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[text()='Country']//following::select[1]"))));
		return webList;
	}

	
	public WebElement fn_setJobTitle(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Job Title']//following::input[1]")));
		return webElement;
	}
	
	//to enter the Payment Method
	public WebElement fn_clickPaymentMethod(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Payment Method']//following::td[1]")));
		return webElement;
	}
	//to Select the Payment Method
	public Select fn_selectPaymentMethod(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[text()='Payment Method']/following::select[1]"))));
		return webList;
	}
	
	//to click the Payment terms
	public WebElement fn_clickPaymentTerm(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Payment Terms']//following::td[1]")));
		return webElement;
	}
	//to select the Payment terms
	public Select fn_selectPaymentTerm(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[text()='Payment Terms']/following::select[1]"))));
		return webList;
	}
	
	
	
	
	//to click the Tax Exempt Level
	public WebElement fn_clickBillingApprovalchk(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Billing Approval']/following::input[@type='checkbox']")));
		return webElement;
	}
	
	//to click the Tax Exempt Level
	public WebElement fn_clickTaxExemptLevel(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Tax Exempt Level']//following::td[1]")));
		return webElement;
	}
	
	//to Select the Tax Exempt Level
	public Select fn_selectTaxExemptLevel(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[text()='Tax Exempt Level']/following::select[1]"))));
		return webList;
	}
	
	//to Select the Tax Exempt Level
	public Select fn_selIndustryRollup(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[text()='Industry Rollup']/following::select[1]"))));
		return webList;
	}
	
	//to Edit the Contact Name 
	public List<WebElement> fn_getEditAddeLinks(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		List<WebElement> lstwebElement = driver.findElements(By.xpath("//h3[text()='Addresses']/following::tbody[1]//child::*[contains(text(),'TestAutoaddress')]/..//child::a[text()='Edit']"));
		return lstwebElement;
	}
	
	
	//to click the Tax Exempt Certificate Number
	public WebElement fn_getAcctAddrID(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h3[text()='Addresses']/following::tbody[1]//child::*[contains(text(),'TestAutoaddress')]/..//child::*/a[contains(text(),'LAC')]")));
		return webElement;
	}
	
	//to click the Tax Exempt Certificate Number
	public WebElement fn_gePrimaryContact(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h3[text()='Contacts']/following::tbody[1]//child::*/a[contains(text(),'FirstName')]")));
		return webElement;
	}
	
	//to click the Tax Exempt Certificate Number
	public WebElement fn_getEditAddeLink(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h3[text()='Addresses']/following::tbody[1]//child::a[text()='Edit']")));
		return webElement;
	}
	
	//to Select the Payment Method
	public Select fn_selAddressStatus(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[text()='Address Status']/following::select[1]"))));
		return webList;
	}
	
	//to click the Tax Exempt Certificate Number
	public WebElement fn_clickTaxExemptCertificateNumber(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Tax Exempt Certificate Number']//following::td[1]")));
		return webElement;
	}
	
	//To set the TaxExemptCertificateNumber
	public WebElement fn_setTaxExemptCertificateNumber(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Tax Exempt Certificate Number']/following::input[1]")));
		return webElement;
	}
	
	//to Click the TaxPayerID
	public WebElement fn_clickTaxPayerID(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Tax Payer ID']//following::td[1]")));
		return webElement;
	}
	
	//To set the TaxPayerID
	public WebElement fn_setTaxPayerID(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Tax Payer ID']/following::input[1]")));
		return webElement;
	}
	
	//to click Exempt End Date
	public WebElement fn_clickTaxPayerEndDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Exempt End Date']//following::td[1]")));
		return webElement;
	}
	
	//To set the TaxPayer End Date
	public WebElement fn_setTaxPayerEndDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Exempt End Date']/following::input[1]")));
		return webElement;
	}
	
	//to Edit the Contact Name 
	public WebElement fn_clickAcctDetailSave(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h2[text()='Account Detail']//following::input[@title='Save']")));
		return webElement;
	}

	//to Edit the Contact Name 
	public WebElement fn_clickEditContactLink(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//th[text()='Job Title']//following::a[text()='Edit']")));
		return webElement;
	}
	
	//to Edit the Contact Name 
	public WebElement fn_clickEditContactDevLink(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//th[text()='Phone']//following::a[text()='Edit']")));
		return webElement;
	}
	
	//to Edit the Contact Name 
	public WebElement fn_clickEditOppLink(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h3[text()='Opportunities']/following::tbody[1]//child::a[text()='Edit']")));
		return webElement;
	}
	
	//to set the email in Edit contact page 
	public WebElement fn_setEmailOnContactPage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Email']//following::input[1]")));
		return webElement;
	}
	
	//to click on Save btn contact page 
	public WebElement fn_clickSaveOnContactPage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h2[text()='Contact Edit']//following::input[@name='save']")));
		return webElement;
	}
	
	//to click on Save btn contact page 
	public WebElement fn_clickSaveIgnoreOnContactPage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h2[text()='Contact Edit']//following::input[@name='save']")));
		return webElement;
	}
	
	//to click on New Address Button
	public WebElement fn_clickNewAddressBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='New Address']")));
		return webElement;
	}
	
	// To Select Adress status
	public Select fn_selectAddressStatus(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[text()='Address Status']//following::select[1]"))));
		return webList;
	}
	
	// To Select County
	public Select fn_selectAddressCounty(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[text()='UK/IE County']//following::select[1]"))));
		return webList;
	}
	
	// To Select County
	public Select fn_selectAddressCountyDev(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[text()='EMEA County']//following::select[1]"))));
		return webList;
	}
	
	//to set address1 on adress edit page
	public WebElement fn_setAddress1(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Address 1']//following::input[1]")));
		return webElement;
	}
	//to set city on adress edit page
	public WebElement fn_setCity(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='City']//following::input[1]")));
		return webElement;
	}
	
	// To Select Country on AddressEdit page
	public Select fn_selectState(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[text()='State / Province']//following::select[1]"))));
		return webList;
	}
	
	// To Select Country on AddressEdit page
	public Select fn_selectCountryOnAddressEditpage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[text()='Country']//following::select[1]"))));
		return webList;
	}
	
	//to set city on adress edit page
	public WebElement fn_clickContactOnAddressBtnOnContactEditPage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='New Address Contact']")));
		return webElement;
	}
	
	//to set city on adress edit page
	public WebElement fn_clickContactLukUpWindwOnAdCtctEdtPage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Contact']//following::img[@alt='Contact Lookup (New Window)']")));
		return webElement;
	}
	
	//to set city on adress edit page
	public WebElement fn_setAddrContact(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Contact']/following::span/input")));
		return webElement;
	}
	
	// To Select Country on AddressEdit page
	public Select fn_selectUsedForOnAddressContactEditpage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//select[@title='Used For - Available']"))));
		return webList;
	}
	
	//To remove some feilds from choosen
	public Select fn_RemoveUsedForOnAddressContactEditpage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//select[@title='Used For - Chosen']"))));
		return webList;
	}
	
	//to set city on adress edit page
	public WebElement fn_setEmailOnAdCtctEdtPage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Email']//following::input[1]")));
		return webElement;
	}
	
	//to set city on adress edit page
	public WebElement fn_clickSaveOnAdCtctEdtPage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h2[text()='Address Contact Edit']//following::input[1][@name='save']")));
		return webElement;
	}
	
	public WebElement fn_clickContactLnkSaveOnAdCtctEdtPage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Contact']//following::a[1]")));
		return webElement;
	}	
	
	// To Select Adress state/Province on Address Edit page
	public Select fn_selectStateOnAddressEditPage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[text()='State / Province']//following::select[1]"))));
		return webList;
	}
	
	//to set Postal code on adress edit page
	public WebElement fn_setPostalCodeOnAddressEditpage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Postal Code']//following::input[1]")));
		return webElement;
	}
	
	// To Select Multiple on AddressEdit page
	public Select fn_selectMultipleOnAddressEditpage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//select[@title='Address Usage Type - Available']"))));
		return webList;
	}
	
	//To remove some feilds from choosen
	public Select fn_RemoveMultipleOnAddressEditpage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//select[@title='Address Usage Type - Chosen']"))));
		return webList;
	}
	
	//to click on img add on adress edit page
	public WebElement fn_clickImgAddOnAddressEditPage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@title='Add']")));
		return webElement;
	}
	
	//to click on img add on adress edit page
	public WebElement fn_ClickImgRemoveOnAddressEditPage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@title='Remove']")));
		return webElement;
	}
	
	//to click on Save Btn on adress edit page
	public WebElement fn_clickSaveAddOnAddressEditPage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h2[text()='Address Edit']//following::input[1][@name='save']")));
		return webElement;
	}
	
	//to click on Save Btn on adress edit page
	public WebElement fn_clickAccountNameLink(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Account Name']//following::a[1]")));
		return webElement;
	}
	
	//to click on Save Btn on adress edit page
	public WebElement fn_clickAccountHierarchyLink(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//th[text()='Account Owner']//following::a[1]")));
		return webElement;
	}
	
	//to click on Acct Link
	public WebElement fn_clickAccountLink(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td[text()='Account']//following::a[1]")));
		return webElement;
	}
	
	//to click on Create New Contact LInk
	public WebElement fn_clickNewContact(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@title='New Contact']")));
		return webElement;
	}
	
	//to click on New Customer ReferenceBtn
	public WebElement fn_clickNewCustomerReferenceBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@title='New Customer Reference']")));
		return webElement;
	}
	
	//to set TierName on Add Customer Reference page
	public WebElement fn_setTierName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Tier Name']//following::input[1]")));
		return webElement;
	}
	
	//to click on Save Btn on 
	public WebElement fn_clickSaveOnCustomerEditPage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h2[text()='Customer Reference Edit']//following::input[1][@name='save']")));
		return webElement;
	}
	
	//to click on Save Btn on 
	public WebElement fn_ClickEditOpttyOnAcctPage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//th[text()='Total Delta GCR']//following::a[3]")));
		return webElement;
	}
	
	//to click on Oppty
	public WebElement fn_ClickOpptyEditOnOpptyPage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//h2[text()='Opportunity Detail']//following::td[1]/input[@name='edit']")));
		return webElement;
	}
	
	//to select oppty Type
	public Select fn_selectOpptyType(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[text()='Opportunity Type']//following::select[1]"))));
		return webList;
	}
		
	//To click on Lookup window
	public WebElement fn_ClickAcctLookUpwndw(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/div/span/a/img[@alt=\" Lookup (New Window)\"]")));
		return webElement;
	}
	
	//To click on CustomerReferenceLookup window
	public WebElement fn_ClickCustRefLookUpwndw(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='One Time Fee']//following::input[11]//following::img[1][@alt='Customer Reference Lookup (New Window)']")));
		return webElement;
	}
	
	//Enter Acct Name
	public WebElement fn_EnterAcctName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td[contains(text(),\"Account Name\")]//following::td[1]/div/select")));
		return webElement;
	}
	
	//Click on Enter opptyName
	public WebElement fn_setOpptyName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td[contains(text(),\"Opportunity Name\")]//following::td[1]/div/input")));
		return webElement;
	}
	
	//Enter Value in subject Field
	public WebElement fn_setSubject(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/label[text()='Subject']//following::td[1]/div/input")));
		return webElement;
	}
	
	
	// To Select the LMRtasktype
	public Select fn_LMRtasktype(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[text()='LMR Task Type']//following::td[1]/div/span/select"))));
		return webList;
	}
	
	
	//Enter value for Duedate
	public WebElement fn_setDuedate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Due Date']//following::td[1]/div/span/span/a")));
		return webElement;
	}
	
	// To Select the Status 
	public Select fn_selectStatus(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[text()=\"Status\"]//following::td[1]/div/span/select"))));
		return webList;
	}
	
	//Select value for active currency
	public Select fn_ActiveCurrency(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//td/label[text()='Activity Currency']//following::td[1]/div/select"))));
		return webList;
	}
	
	//Click on Convert Button
	public WebElement fn_clickConvertBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td/input[@title=\"Convert\"]")));
		return webElement;
	}
	
	//Select the competetors
	public Select fn_selectLeadContact(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//tr/td/label[contains(text(),'Contact Name')]//following::td[1]/div/select"))));
		return webList;
	}
	
	
	//Click on New Oppty Button
	public WebElement fn_clickNewOpptyBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td/input[@name=\"newOpp\"]")));
		return webElement;
	}
	
	//Click on Continue button
	public WebElement fn_clickOpptyContBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//td/input[@title=\"Continue\"]")));
		return webElement;
	}
	
	//Enter Contact LastName
	public WebElement fn_setContactLastName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@name='name_lastcon2']")));
		return webElement;
	}
	
	//Enter the Oppty Name field
	public WebElement fn_setOpptunityName(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[contains(text(),'Opportunity Name')]//following::td[1]/div/input")));
		return webElement;
	}
	
	//Enter the Value in Price book field
	public WebElement fn_setPriceBook(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[contains(text(),'Price Book')]//following::td[1]/div/span/input")));
		return webElement;
	}
	
	//Enter the OpptyType
/*	public Select fn_selectOpptyType(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//span/label[contains(text(),'Opportunity Type')]//following::td[1]/div/span/select"))));
		return webList;
	}*/
	
	//select the Oppty Currency
	public Select fn_selectOpptyCurrency(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[contains(text(),'Opportunity Currency')]//following::td[1]/div/select"))));
		return webList;
	}
	
	//select the Oppty Currency
	public WebElement fn_ClickOpptySaveBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.name("save")));
		return webElement;
	}
	
	//Select the competetors
	public Select fn_SelectOpptyComptrs(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(webWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span/select[@title='Competitors - Available']"))));
		return webList;
	}
	
	//enter the close date
	public WebElement fn_setCloseDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[contains(text(),'Close Date')]//following::td[1]/div/span/input")));
		return webElement;
	}
	
	//select the stage
	public Select fn_selectStage(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//label[contains(text(),'Stage')]//following::td[1]/div/span/select"))));
		return webList;
	}
	
	//Enter the Billing Account
	public WebElement fn_EnterBillingAc(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[contains(text(),'Billing Account')]//following::td[1]/span/input")));
		return webElement;
	}
	
	//Select the PrimaryProductInterest
	public Select fn_SelectPPI(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(webWait.until(ExpectedConditions.elementToBeClickable((By.xpath(("//label[contains(text(),'Primary Product Interest')]//following::td[1]/div/span/select"))))));
		return webList;
	}
	//Click on AddProduct Button
	public WebElement fn_clickAddProductBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td/input[@title='Add Product']")));
		return webElement;
	}
	
	//Select the FeildFilter
	public Select fn_selectFilter(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(webWait.until(ExpectedConditions.elementToBeClickable(By.id("PricebookEntrycol0"))));
		return webList;
	}
	
	//Select the SearchCriteria
	public Select fn_selectSrchCriteria(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(webWait.until(ExpectedConditions.elementToBeClickable(By.id("PricebookEntryoper0"))));
		return webList;
	}
	
	//Enter text in Search feild of Add Product page
	public WebElement fn_setSrchTxt(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.id("PricebookEntryfval0")));
		return webElement;
	}
	
	//Click on Search Button
	public WebElement fn_clickPrdSearchBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.id("save_filter_PricebookEntry")));
		return webElement;
	}
	
	//Select the product after seraching with filter criteria
	public WebElement fn_clickPrdSearched(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//tr/td/div[@title='Price Book Name']//following::td[1]/div/input")));
		return webElement;
	}
	
	//Click on Select Button on product selection page
	public WebElement fn_clickSelectBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/input[@name=\"edit\"]")));
		return webElement;
	}
	
	//Select the SearchCriteria
	public Select fn_selectSite(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		Select webList = new Select(webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Site']//following::select[1]"))));
		return webList;
	}
	
	//To enter Invoice Term
	public WebElement fn_enterInvTerm(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Invoice Term']/following::td/input[contains(@name,'Quantity')]")));
		return webElement;
	}
	
	//To enter StartDate
	public WebElement fn_setStartDate(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Contract Start Date']//following::input[4]")));
		return webElement;
	}
	
	//To enter AnnualFee
	public WebElement fn_setAnnualFee(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='Annual Fee']//following::input[@value='0.00'][1]")));
		return webElement;
	}
	
	//To enter OneTimeFee
	public WebElement fn_setOneTimeFee(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//label[text()='One Time Fee']//following::input[@value='0'][1]")));
		return webElement;
	}
	
	//To click on Save btn
	public WebElement fn_clickPrdctSaveBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@title=\"Save\"]")));
		return webElement;
	}
	
	//To click on CreateQuote/Proposal Button
	public WebElement fn_clickQuoteORProposalBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@alt=\"CreateQuotePoposal\"]")));
		return webElement;
	}
	
	//To click on SearchBtn on Look up window
	public WebElement fn_clickSearchBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id=\"lksrch\"]")));
		return webElement;
	}
	
	//To click on ClickGo Btn on Look up window
	public WebElement fn_clickGoBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div/input[@title=\"Go!\"]")));
		return webElement;
	}
	
	//To click on ClickGo Btn on Look up window
	public WebElement fn_findList(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@class='list']/tbody/tr[2]/th/a")));
		return webElement;
	}
}
