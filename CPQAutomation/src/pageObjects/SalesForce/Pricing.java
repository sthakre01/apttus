
/*
 Author     		:	Suhas Thakre
 Class Name			: 	Pricing 
 Purpose     		: 	Purpose of this file is :
						1. To identify Page object related to Pricing page for Apttus application.

 Date       		:	06/012/2017 
 Modified Date		:	06/012/2017	
 Modified By		: 	
 Version Information:	Version 1.0
  
 Copyright notice	:	Copyright(C) 2015 Sungard Availability Services - 
 						All Rights Reserved 
*/
package pageObjects.SalesForce;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Pricing {
	
	
public static WebElement webElement = null;
	
	// To identify the  Rates table
	public WebElement fn_getRatestbl(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@class='aptLineItemOptionsTable']")));
		return webElement;
	}

	// To identify the  Remove Confirmation Yes button
	public WebElement fn_clickYesBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//span[text()='Remove Confirmation']/following::input[@value='Yes']")));
		return webElement;
	}

	//To identify the Reprice Button
	public WebElement fn_RepriceBtn(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[text()='Reprice']")));		
		return webElement;
	}
	
	//To identify the Updating Price text
	public WebElement fn_UpdPriceTxt(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='Updating Price, please wait...']")));		
		return webElement;
	}

	//To identify the loading spin image
	public WebElement fn_loadingImg(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[@src='/img/loading.gif']")));		
		return webElement;
	}
	
	//To identify the loading Page spin image
	public WebElement fn_loadingPageImg(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//img[contains(@src,'apttus_config2__Image_LoadingPage')]")));		
		return webElement;
	}
	
	//To identify the Reprice Button
	public List<WebElement> fn_getProdOptions(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		List<WebElement> lsteleProduct = driver.findElements(By.xpath("//a[contains(@class,'showOption')]"));
		return lsteleProduct;
	}
	
	//To identify the Reprice Button
	public List<WebElement> fn_getLineItems(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		List<WebElement> lsteleLineItems = driver.findElements(By.xpath("//table[@class='aptLineItemOptionsTable']/tbody/tr/td[2]/span/span/span/a"));
		return lsteleLineItems;
		
	}
	
	//To identify the Reprice Button
	public WebElement fn_getLineItem(WebDriver driver, WebDriverWait webWait,int rowindex) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@class='aptLineItemOptionsTable']/tbody/tr["+rowindex+"]/td[2]/span/span/span/a")));		
		return webElement;
		
	}
	
	//To identify the Reprice Button
	public WebElement fn_geChargeType(WebDriver driver, WebDriverWait webWait,int rowindex) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@class='aptLineItemOptionsTable']/tbody/tr["+rowindex+"]/td[5]/span")));		
		return webElement;
		
	}
	
	//To identify the Reprice Button
	public WebElement fn_getBasePrice(WebDriver driver, WebDriverWait webWait,int rowindex) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@class='aptLineItemOptionsTable']/tbody/tr["+rowindex+"]/td[6]/span")));		
		return webElement;
		
	}
	
	//To identify the Reprice Button
	public WebElement fn_getQuantity(WebDriver driver, WebDriverWait webWait,int rowindex) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@class='aptLineItemOptionsTable']/tbody/tr["+rowindex+"]/td[7]/span")));		
		return webElement;
		
	}
	
	//To identify the Reprice Button
	public WebElement fn_getTerm(WebDriver driver, WebDriverWait webWait,int rowindex) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@class='aptLineItemOptionsTable']/tbody/tr["+rowindex+"]/td[9]/span")));		
		return webElement;
		
	}
	
	//To identify the Reprice Button
	public WebElement fn_getMRR(WebDriver driver, WebDriverWait webWait,int rowindex) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@class='aptLineItemOptionsTable']/tbody/tr["+rowindex+"]/td[14]/span")));		
		return webElement;
		
	}
	
	//To identify the Reprice Button
	public WebElement fn_getGCR(WebDriver driver, WebDriverWait webWait,int rowindex) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@class='aptLineItemOptionsTable']/tbody/tr["+rowindex+"]/td[15]/span")));		
		return webElement;
		
	}
	
	//To identify the Reprice Button
	public WebElement fn_getNetPrice(WebDriver driver, WebDriverWait webWait,int rowindex) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@class='aptLineItemOptionsTable']/tbody/tr["+rowindex+"]/td[16]/span")));		
		return webElement;
		
	}
	
	//To identify the Reprice Button
	public WebElement fn_getAdjAmt(WebDriver driver, WebDriverWait webWait,int rowindex) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@class='aptLineItemOptionsTable']/tbody/tr["+rowindex+"]/td[13]/input")));		
		return webElement;
		
	}
	
	//To Select Value for lastname
	public Select fn_getAdjType(WebDriver driver, WebDriverWait webWait,int rowindex) throws InterruptedException
	{
		Select webList = new Select(driver.findElement(By.xpath(("//table[@class='aptLineItemOptionsTable']/tbody/tr["+rowindex+"]/td[12]/span/select"))));
		return webList;
	}

	//To identify the Updating Price text
	public WebElement fn_getLineItemOptionsTable(WebDriver driver, WebDriverWait webWait) throws InterruptedException
	{
		webElement = webWait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@class='aptLineItemOptionsTable']/tbody")));		
		return webElement;
	}

}